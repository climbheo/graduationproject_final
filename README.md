# GraduationProject
## 한국산업기술대 게임공학과 졸업작품
- 허정민 - 서버
- 권대현 - 이펙트, 모델링
- 박하은 - 클라이언트


# File Structure Conventions
## Top level 레이아웃  
``` 
.
└── GraduationProject
    ├── Documents           # 기획문서, 발표용문서 등
    ├── TheShift_Client     # Client 프로젝트
    ├── TheShift_Server     # Server 프로젝트
    └── README.md           # 현재파일
```
여긴 수정하지 않습니다.  
!! `Documents`폴더 내 문서들 수정하면 commit하기 전까지 언리얼 컴파일 안됩니다. !!  
+ `.docx`, `.pptx` 등과 같은 문서파일 제외시켰음 문서 쓸일 있으면 `.md` 파일로 쓰세요.

## TheShift_Client 레이아웃
```
.
└── TheShift_Client
    ├── ...etc                      # 다른 폴더는 만질일 없을듯 합니다
    ├── Content                     # 권대현씨는 에셋 여기에 추가해주시면 됨
    │   ├── Collections             # Filter같은거 추가...?
    │   ├── Developers              # 쓸일 없을듯 한데 찾아볼게요
    │   ├── (e.g. Architecture)
    │   ├── (e.g. Audio)
    │   ├── (e.g. Blueprints)
    │   ├── (e.g. Maps)
    │   ├── (e.g. Materials)
    │   ├── (e.g. Props)
    │   └── ...etc
    └── Source
        ├── TheShift_Client
        │   ├── > (Create your C++ work folders here)   # e.g. Monsters, Props
        │   ├── TheShift_Client.Build.cs                # 새로운 모듈을 추가할 때 수정 (불확실)
        │   ├── TheShift_ClientGameMode.h
        │   ├── TheShift_ClientGameMode.cpp
        │   └── ...etc                                  # Game mode 등 게임관련 class
        ├── TheShift_Client.Target.cs                   # 빌드 구성을 저장하는데 사용하는 설정파일
        └── TheShift_ClientEditor.Target.cs             # 위와 동일
```
## 작업위치
허정민 (네트워크 코드, 서버, class 제작)
+ TheShift_Client\Source\TheShift_Client
+ TheShift_Server

권대현 (에셋 제작)
+ TheShift_Client\Content  

박하은 (Material 제작, class 제작)
+ TheShift_Client\Source\TheShift_Client  
+ TheShift_Client\Content

# Code Conventions
## Naming conventions
`Unreal engine`의 convention에 따라 `Upper camel case`  
> class UpperCamelCase { }

.clang-format 파일 추가해놨으니 이거에 맞춰서 쓰면 됨

# 문서
## .docx, .pptx 등 문서파일 추가 지양
위와 같은 문서파일 수정 시 클라이언트 에디터 상에서 컴파일이 안되는 현상... 문서파일 `commit` 후 컴파일하면 되지만 `.md` 파일로 대체  


# Map Setting
## Export
+ 서버에서 쓸 맵 정보는 언리얼 에디터에서 세팅할 수 있음
+ 언리얼 에디터에서 맵을 세팅하면 그 정보를 `Protocol/Json/Maps/*json` 으로 저장하게 함  
(Json에 포함 되었으면 하는 actor에 TSNetworked Component와 TSPhysics Info Component를 포함시켜야 한다)  
+ Heightmap의 경우 `Protocol/Heightmap/.r16` 형식으로 따로 저장 해야 함 (자동으로 export하지 않음)  
+ Heightmap이 포함된 맵의 경우 해당 맵 json파일에 Heightmap의 파일 명을 직접 기입해 주어야 한다.
+ Export를 모두 하였다면 json파일과 r16파일을 server로 같은 위치에 옮겨 주면 된다.

## 새로운 맵 추가
1. 위 방식으로 export를 진행 한 후 server의 MapData.cpp에 해당 json파일을 import하도록 하여야 한다. (주석 참조)
2. Types.h에 MapType에도 새로운 맵의 Type을 추가 해 주어야 한다. (클라이언트도 동일)
3. 클라이언트에서 해당 Map Type을 플레이 하겠다 라고 Packet을 보내면 그 Map으로 Session 초기화 (Packet만 보내면 됨)