﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "BowAimUI.generated.h"

/**
 * 
 */
UCLASS()
class THESHIFT_CLIENT_API UBowAimUI : public UUserWidget
{
	GENERATED_BODY()

public:
	void SetDrawSize(float Size);
protected:
	virtual void NativeConstruct() override;

private:
	UPROPERTY()
		class UImage* AimImage;

	UPROPERTY()
		class UCanvasPanel* CanvasPanel;
	
};
