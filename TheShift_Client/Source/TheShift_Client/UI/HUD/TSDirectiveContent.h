﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Protocol/Types.h"
#include "Misc/UActTypes.h"

#include "TSDirectiveContent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THESHIFT_CLIENT_API UTSDirectiveContent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTSDirectiveContent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UTSDirectiveContent* NextDirectiveContent;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UTSDirectiveContent* PrevDirectiveContent;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	EInteractableActionType DirectiveType;

	// Next directive로 넘어갈지 말지 정한다.
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool BlockNext = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FString DirectiveMessage;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int PlayOrder = 1;

	UPROPERTY(BlueprintReadWrite)
	class ATSTriggerBox* ParentTrigger;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TArray<AActor*> Targets;

	UPROPERTY()
	TSubclassOf<class UUserWidget> ChestInventoryWidgetClass;

	UFUNCTION()
	UUserWidget* CreateWidget(TSubclassOf<UUserWidget> NewWidgetClass);

	UFUNCTION(BlueprintCallable)
	void ApplyContent();

	UFUNCTION(BlueprintCallable)
	UTSDirectiveContent* AddNextDirective();

	UFUNCTION(BlueprintCallable)
	void PutNext(UTSDirectiveContent* Prev);

	UFUNCTION(BlueprintCallable)
	void ArrangeOrder(UTSDirectiveContent* Prev);
};
