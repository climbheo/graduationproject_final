﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TSDirectiveContent.h"

#include "TheShift_ClientGameModeBase.h"
#include "Actors/TSTriggerBox.h"
#include "Protocol/Log.h"
#include "Protocol/Serializables.h"
#include "TheShift_GameInstanceBase.h"
#include "Network/Components/TSNetworkedComponent.h"
#include "Item/TSChest.h"
#include "UserWidget.h"
#include "Components/TSDoorComponent.h"
#include "UI/Items/TSChestInventoryWidget.h"

// Sets default values for this component's properties
UTSDirectiveContent::UTSDirectiveContent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
	static ConstructorHelpers::FClassFinder<UUserWidget> DefaultChestInventoryWidget(TEXT("/Game/Graphic/UI/Items/ChestInventory.ChestInventory_C"));
	if(DefaultChestInventoryWidget.Succeeded())
	{
		ChestInventoryWidgetClass = DefaultChestInventoryWidget.Class;
	}
}


// Called when the game starts
void UTSDirectiveContent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UTSDirectiveContent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

// Directive에 의한 widget생성
UUserWidget* UTSDirectiveContent::CreateWidget(TSubclassOf<UUserWidget> NewWidgetClass)
{
	auto ValidWidget = Cast<ATheShift_ClientGameModeBase>(GetWorld()->GetAuthGameMode())->ChangeIngameWidget(NewWidgetClass);

	// Widget이 유효하고 부모 trigger에 종속된 widget이라면 trigger에 추가시켜준다. (
	if (ValidWidget && ParentTrigger)
	{
		ParentTrigger->CurrentActiveWidgets.Emplace(ValidWidget);
	}

	return ValidWidget;
}

void UTSDirectiveContent::ApplyContent()
{
	switch(DirectiveType)
	{
	case EInteractableActionType::OpenDoorRotatePlus:
	{
		if (Targets.Num() != 0)
		{
			if (auto DoorComponent = Cast<UTSDoorComponent>(Targets[0]->GetComponentByClass(UTSDoorComponent::StaticClass())))
			{
				if (DoorComponent->IsLocked)
				{
					// 새로운 directive 생성
					auto NewDirective = AddNextDirective();

					// 새로 만든 directive의 next를 현재 directive의 next로 바꿔주고 다음 directive로 넘어가지 않고 바로 끝나게 만들어준다.
					// NewDirective를 삭제할 때 NewDirective의 Next를 현재 directive의 next로 바꿔주고 삭제해야 한다.
					if (NewDirective)
					{
						NewDirective->BlockNext = true;

						// 새로운 Directive 셋팅
						NewDirective->DirectiveMessage = "You need a key to open this door.Search for a key.";
						NewDirective->PlayOrder = PlayOrder + 1;
					}
					break;
				}

				LOG_INFO("Open door minus");
				TheShift::Serializables::InteractableAct Data;

				// UID를 얻어와서 Data.Value에 넣어준다.
				if (auto NetworkComp = Cast<UTSNetworkedComponent>(Targets[0]->GetComponentByClass(UTSNetworkedComponent::StaticClass())))
				{
					Data.Type = TheShift::InteractableActionType::OpenDoorRotatePlus;
					Data.Tag = TCHAR_TO_UTF8(*NetworkComp->GetActorTag());
					LOG_INFO("Door Id: %s", Data.Tag.c_str());

					SendPacket(Data);

					// 볼일 다 끝났으니 다음 directive는 나오지 않도록 한다.
					BlockNext = true;

					// 한번 열었으면 비활성화한다.
					if (ParentTrigger)
					{
						// 현재 모든 directive가 끝난 후 trigger가 비활성화 되도록 한다.
						ParentTrigger->ScheduleParentTriggerDisable();
						LOG_INFO("Parent trigger scheduled to be disabled");
					}
					break;
				}
			}
		}
	}

	case EInteractableActionType::OpenDoorRotateMinus:
	{
		if (Targets.Num() != 0)
		{
			if (auto DoorComponent = Cast<UTSDoorComponent>(Targets[0]->GetComponentByClass(UTSDoorComponent::StaticClass())))
			{
				if (DoorComponent->IsLocked)
				{
					// 새로운 directive 생성
					auto NewDirective = AddNextDirective();

					// 새로 만든 directive의 next를 현재 directive의 next로 바꿔주고 다음 directive로 넘어가지 않고 바로 끝나게 만들어준다.
					// NewDirective를 삭제할 때 NewDirective의 Next를 현재 directive의 next로 바꿔주고 삭제해야 한다.
					if(NewDirective)
					{
						NewDirective->BlockNext = true;

						// 새로운 Directive 셋팅
						NewDirective->DirectiveMessage = "You need a key to open this door.Search for a key.";
						NewDirective->PlayOrder = PlayOrder + 1;
					}
					break;
				}
				
				LOG_INFO("Open door minus");
				TheShift::Serializables::InteractableAct Data;

				// UID를 얻어와서 Data.Value에 넣어준다.
				if (auto NetworkComp = Cast<UTSNetworkedComponent>(Targets[0]->GetComponentByClass(UTSNetworkedComponent::StaticClass())))
				{
					Data.Type = TheShift::InteractableActionType::OpenDoorRotateMinus;
					Data.Tag = TCHAR_TO_UTF8(*NetworkComp->GetActorTag());
					LOG_INFO("Door Id: %s", Data.Tag.c_str());

					SendPacket(Data);

					// 볼일 다 끝났으니 다음 directive는 나오지 않도록 한다.
					BlockNext = true;

					// 한번 열었으면 비활성화한다.
					if (ParentTrigger)
					{
						// 현재 모든 directive가 끝난 후 trigger가 비활성화 되도록 한다.
						ParentTrigger->ScheduleParentTriggerDisable();
						LOG_INFO("Parent trigger scheduled to be disabled");
					}
					break;
				}
			}
		}
	}
		
	case EInteractableActionType::NextLevel:
	{
		TheShift::Serializables::LevelChangeReqDebug Data;
		Data.Type = Cast<ATheShift_ClientGameModeBase>(GetWorld()->GetAuthGameMode())->GetNextMapType();
		SendPacket(Data);
		break;
	}

	case EInteractableActionType::OpenChest:
	{
		if(Targets.IsValidIndex(0))
		{
			if(ATSChest* Chest = Cast<ATSChest>(Targets[0]))
			{
				if(auto GameMode = Cast<ATheShift_ClientGameModeBase>(GetWorld()->GetAuthGameMode()))
				{
					// 1. Widget 생성
					auto NewWidget = Cast<UTSChestInventoryWidget>(CreateWidget(ChestInventoryWidgetClass));

					// 2. 생성한 widget에 chest actor 전달 Target에 등록되어있음
					if(NewWidget)
					{
						NewWidget->PassChestActor(Chest);
						NewWidget->ChestInitEvent.Broadcast();
						LOG_INFO("Casting widget to UTSChestInventoryWidget successed.");
					}
					else
					{
						LOG_ERROR("Cast widget to UTSChestInventoryWidget failed.");
					}
				}
			}
		}
		break;
	}
		
	default:
		break;
	}
}

UTSDirectiveContent* UTSDirectiveContent::AddNextDirective()
{
	// 새로운 directive 생성
	auto NewDirective = Cast<UTSDirectiveContent>(NewObject<UTSDirectiveContent>(UTSDirectiveContent::StaticClass()));
	NewDirective->RegisterComponent();

	if (NewDirective)
	{
		NewDirective->NextDirectiveContent = NextDirectiveContent;
		// 순서를 바꾸기 전에 next의 prev를 NewDirective로 바꿔준다.
		if(NextDirectiveContent)
		{
			NextDirectiveContent->PlayOrder = NextDirectiveContent->PlayOrder + 1;
			NextDirectiveContent->PrevDirectiveContent = NewDirective;
		}
		NewDirective->PrevDirectiveContent = this;
		NextDirectiveContent = NewDirective;

		NewDirective->PlayOrder = PlayOrder + 1;

		// 밀려난 directive들 order 재정렬
		if(NewDirective->NextDirectiveContent)
		{
			NewDirective->NextDirectiveContent->ArrangeOrder(NewDirective);
		}
	}

	return NewDirective;
}

void UTSDirectiveContent::PutNext(UTSDirectiveContent* Prev)
{
	if(Prev)
	{
		// 내 next를 이전 directive가 가리키던 next로 해주고
		// 이전 directive의 next를 나로 지정한다.
		NextDirectiveContent = Prev->NextDirectiveContent;
		Prev->NextDirectiveContent = this;

		// 다음 directive들 재정렬
		if(NextDirectiveContent)
			NextDirectiveContent->ArrangeOrder(this);
	}
}

void UTSDirectiveContent::ArrangeOrder(UTSDirectiveContent* Prev)
{
	if (Prev)
	{
		PlayOrder = Prev->PlayOrder + 1;
		if (NextDirectiveContent)
			NextDirectiveContent->ArrangeOrder(this);
	}
	else
		LOG_ERROR("Can't arrange directive order.");
}

