﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TSChestInventoryWidget.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnChestContentInit);

class ATSChest;
class UTSChestContent;
/**
 * 
 */
UCLASS()
class THESHIFT_CLIENT_API UTSChestInventoryWidget : public UUserWidget
{
	GENERATED_BODY()
	

public:
	void PassChestActor(ATSChest* Chest);

	//UPROPERTY(BlueprintCallable)
	//void AcquireItemReq();

	UPROPERTY(BlueprintReadOnly)
	ATSChest* ParentChest;

	UPROPERTY(BlueprintAssignable, VisibleAnywhere, BlueprintCallable, Category = "Event")
	FOnChestContentInit ChestInitEvent;

	UPROPERTY(BlueprintReadWrite)
	TMap<int, UTSChestContent*> Contents;
};
