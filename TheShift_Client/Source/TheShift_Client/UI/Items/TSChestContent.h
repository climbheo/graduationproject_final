﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TSChestContent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnItemRemoveFromChest);

/**
 * 
 */
UCLASS()
class THESHIFT_CLIENT_API UTSChestContent : public UUserWidget
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintAssignable, VisibleAnywhere, BlueprintCallable, Category = "Event")
	FOnItemRemoveFromChest ItemRemoveFromChestEvent;
};
