﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TheShift_ClientGameModeBase.h"
#include "Network/Components/TSNetworkedComponent.h"
#include "DrawDebugHelpers.h"
#include "Protocol/Log.h"
#include "Game/System/MapData.h"
#include "Actors/TSPlayer.h"
#include "Actors/TSKnightMonster.h"
#include "Actors/TSWeapon.h"
#include "Actors/TSDestructible.h"
#include "TSPlayerController.h"
#include "TheShift_ClientGameStateBase.h"
#include "Game/Actors/Actor.h"
#include "Actors/TSCharacterAnimInstance.h"
#include "Actors/TSInteractable.h"
#include "Actors/TSWereWolf.h"
#include "TSCameraShake.h"
#include "Runtime/LevelSequence/Public/LevelSequencePlayer.h"

#include "LevelSequenceActor.h"
#include "MovieSceneSequencePlayer.h"
#include "EngineUtils.h"

//For DebugArrow
#include "Kismet/KismetSystemLibrary.h"

//Slice
#include "TheShift_GameInstanceBase.h"
#include "Rendering/SkeletalMeshRenderData.h"
#include "Components/TSSliceable.h"

// Widget
#include "UserWidget.h"
#include "Components/TSDoorComponent.h"

#include "Item/TSPlayerInventoryComponent.h"
#include "Item/TSItemComponent.h"
#include "Item/TSChest.h"
#include "UI/Items/TSChestInventoryWidget.h"
#include "UI/Items/TSChestContent.h"
#include "UI/HUD/TSDirectiveContent.h"


ATheShift_ClientGameModeBase::ATheShift_ClientGameModeBase() : MyId(0)
{
	// GameMode initialization
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bCanEverTick = true;
	
	SetTickGroup(TG_PostUpdateWork);

	// Member variable initialization
	Connected = false;
	Initialized = false;
	GameStarted = false;
	LevelTravelReserved = false;
	InputIndex = 0;
	HostId = INVALID_CLIENTID;

	//Game의 Default 클래스들을 지정해준다(World Setting에 들어간다고 생각하면됨)
	DefaultPawnClass = ATSPlayer::StaticClass();
	GameStateClass = ATheShift_ClientGameStateBase::StaticClass();
	PlayerControllerClass = ATSPlayerController::StaticClass();

	//MapData::Instance();
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> bloodTexture(TEXT("/Game/Effect/Texture/bloodCap_Mat.bloodCap_Mat"));
	if (bloodTexture.Succeeded())
		SlicingMaterialsContainer.Emplace(bloodTexture.Object->GetName(), bloodTexture.Object);

	UE_LOG(LogTemp, Log, TEXT("%s"), *bloodTexture.Object->GetName());
}

// Opening cutscene이후에 level을 초기화 한다.
// (기본 배치 actor spawn)
// 모든 actor에 대해 SetVisibility(true)를 실행한다.
void ATheShift_ClientGameModeBase::PostOpeningCutSceneInitialize()
{
	// Player actor visibility set
	for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; ++It)
	{
		if (auto Player = Cast<ATSPlayer>(*It))
		{
			Player->GetMesh()->SetVisibility(true, true);
			Player->SetWeaponVisibility();
			break;
		}
	}

	// nullptr check, actors visibility set
	for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; ++It)
	{
		if (auto Character = (Cast<ATSCharacter>(*It)))
		{
			if (auto NetWorkComp = Character->FindComponentByClass<UTSNetworkedComponent>())
			{
				if (NetWorkComp->GetUID() == INVALID_UID && !Cast<ATSPlayer>(*It))
					(*It)->Destroy();
				else
				{
					Character->GetMesh()->SetVisibility(true, true);
					if (auto Player = Cast<ATSPlayer>(Character))
						Player->SetWeaponVisibility();
					//TODO: 캐릭터가 들고있는 메쉬에 모두 적용
				}
			}
		}
	}
}

void ATheShift_ClientGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	GameInstance = Cast<UTheShift_GameInstanceBase>(GetGameInstance());
	GameInstance->RegisterPacketProcessingFunc([this](uint8* Buffer) {this->ProcessPacket(Buffer); });

	// MapType을 설정한다.
	InitMapType();
	InitInteractables();

	for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; ++It)
	{
		if (auto Player = Cast<ATSPlayer>(*It))
		{
			Player->GetMesh()->SetVisibility(false, true);
			break;
		}
	}

	for (auto& iter : Effects)
		EffectsContainer.Emplace(iter->GetName(), iter);


	for (auto& iter : Sounds)
		SoundsContainer.Emplace(iter->GetName(), iter);

	for (auto& iter : Sequences)
		SequenceContainer.Emplace(iter->GetName(), iter);

	State.RegisterNetworkComps(&NetworkComps);

	// 내 ClientId 설정
	MyId = GameInstance->GetMyClientId();

	// GameInstance에서 게임 입장이 처리되지 않은 경우
	// Server에게 새로운 session생성, 입장을 요청함
	if (GameInstance->IsInGame() && GameInstance->IsSessionStartedDataStaged())
	{
		ProcessSessionStarted(GameInstance->GetStagedSessionData());

		// 인게임에서 level을 바꿀 때 이 if문이 다시 실행되지 않도록 한다.
		// MainMenu에서 SessionStarted를 수신했을 시 true로 set한다.
		GameInstance->SetSessionStartedDataStaged(false);
	}

	// Editor에서 실행할 때만 작동
	if (GIsEditor)
	{
		if(!GameInstance->IsInGame())
		{
			GameInstance->_Editor_StartWithoutMainMenu();
		}
	}

	Initialized = true;

	LOG_INFO("GameMode init successful.");
	//UE_LOG(LogTemp, Warning, TEXT("GameMode init successful"));
}

void ATheShift_ClientGameModeBase::StartPlay()
{
	Super::StartPlay();

	if (GameInstance->IsInGame() && GameInstance->IsOldPlayersWeaponsDataStaged())
	{
		ProcessToNewPlayerOldPlayerWeapons(GameInstance->GetStagedOldPlayerWeaponsData());

		// 인게임에서 level을 바꿀 때 이 if문이 다시 실행되지 않도록 한다.
		// MainMenu에서 SessionStarted를 수신했을 시 true로 set한다.
		GameInstance->SetOldPlayerWeaponsDataStaged(false);
	}

	if(GameInstance->LastStatStored)
	{
		if(auto PlayerActor = Cast<ATSPlayer>(GetTSActor(MyActorId)))
		{
			PlayerActor->SetCurrentHp(GameInstance->LastStoredHp);
			PlayerActor->CurrentStamina = GameInstance->LastStoredStamina;
			PlayerActor->CurrentTeleportGage = GameInstance->LastStoredTeleportGage;
			PlayerActor->StatChangeEvent.Broadcast();
			GameInstance->LastStatStored = false;
		}
	}

	if(GameInstance->IsInGame() && GameInstance->IsItemDataStaged())
	{
		for(auto& ItemData : GameInstance->GetStagedItemData())
		{
			ProcessItemAcquire(ItemData);
			GameInstance->SetItemDataStaged(false);
		}
	}
}

// Game loop
void ATheShift_ClientGameModeBase::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	
	while (1)
	{
		bool isNoMoreDestroy = true;
		for (auto& iter : ActorsToBeDestroyed)
		{
			if (iter.Value == true)
			{
				GetWorld()->DestroyActor(iter.Key);
				ActorsToBeDestroyed.Remove(iter.Key);
				isNoMoreDestroy = false;
				break;
			}
		};
		if (isNoMoreDestroy)
			break;
	}

	// while(!InputQueue.empty())
	//{
	//    InputInfo Value = InputQueue.front();
	//    InputQueue.pop();

	//    Serializables::Input InputData;
	//    InputData.InputValue = InputType::Test;
	//    Send(InputData);
	//}

	// Game중이 아니라면 데이터를 수신하지 않는다.
	if(GameInstance->IsInGame())
	{
		State.Update(std::chrono::nanoseconds(uint32_t(DeltaSeconds * 1e9)));

		// Data수신, Packet조립 가능 여부 확인
		GameInstance->NetworkTick();
	}
	//uint32 PendingDataSize = 0;
	//int32 BytesTransferred = 0;
	//if (Socket->HasPendingData(PendingDataSize))
	//{
	//	Socket->Recv(Buffer, BUFFERSIZE, BytesTransferred);

	//	AssembleAndProcessPacket(Buffer, BytesTransferred, PacketBuffer);
	//}

	//// TODO: game logic here...
	// if (!ReceivedDataQueue.Empty() && Initialized)
	//{
	//    MessagePtr Data = ReceivedDataQueue.Pop();
	//    if (Data != nullptr)
	//    {
	//        // Read network message
	//        if (ReadMessage(Data))
	//        {
	//            UE_LOG(LogTemp, Warning, TEXT("Data received!!: %d"), static_cast<int>(Data->Data->ClassId()));
	//        }
	//        else
	//        {
	//            UE_LOG(LogTemp, Warning, TEXT("Invalid message received!!: %d"),
	//                   static_cast<int>(Data->Data->ClassId()));
	//        }
	//    }
	//    else
	//    {
	//        UE_LOG(LogTemp, Error, TEXT("Data was nullptr!"));
	//    }
	//}


}

void ATheShift_ClientGameModeBase::ProcessPacket(uint8* Buffer)
{
	// 처음 2bytes는 type
	TheShift::Serializables::Type PacketType = *reinterpret_cast<TheShift::Serializables::Type*>(Buffer);
	
	// 실질적인 data
	InputStream.SetTargetBuffer(reinterpret_cast<char*>(Buffer + sizeof(PacketType)));

	switch (PacketType)
	{
		// Initial connection
		case Serializables::Type::Connected:
		{
			Serializables::Connected Data;
			Data.Read(InputStream);
			ProcessConnected(Data);
			break;
		}

		// Chat message
		case Serializables::Type::Chat:
		{
			Serializables::Chat Data;
			Data.Read(InputStream);
			ProcessChat(Data);
			break;
		}

		case Serializables::Type::LobbyConnected:
		{
			Serializables::LobbyConnected Data;
			Data.Read(InputStream);
			ProcessLobbyConnected(Data);
			break;
		}

		// Lobby info
		case Serializables::Type::LobbyInfo:
		{
			Serializables::LobbyInfo Data;
			Data.Read(InputStream);
			ProcessLobbyInfo(Data);
			break;
		}

		// Connected to session
		case Serializables::Type::SessionConnected:
		{
			Serializables::SessionConnected Data;
			Data.Read(InputStream);
			ProcessSessionConnected(Data);
			break;
		}

		// Game started
		case Serializables::Type::SessionStarted:
		{
			// Level변경 대기중이라면 현재 GameMode에서는 처리하면 안되고
			// 새로 생성될 GameMode가 처리하도록 넘겨야 한다.
			// GameInstance가 저장하도록 하고 여기선 처리하지 않는다.
			if (LevelTravelReserved)
			{
				GameInstance->GetStagedSessionData().Read(InputStream);
				GameInstance->SetSessionStartedDataStaged(true);
			}
			// Level변경이 아니고 게임 시작 직후 받은 data라면 바로 처리한다.
			else
			{
				Serializables::SessionStarted Data;
				Data.Read(InputStream);
				ProcessSessionStarted(Data);
			}

			//SessionStartedReceived = true;
			//SessionStartData.Read(InputStream);
			//컷신 재생 시작 후 끝나면 ProcessSessionStarted(SessionStartData); 호출

			/*auto state = Cast<ATheShift_ClientGameStateBase>(GetWorld()->GetGameState());
			if (state)
				state->SetCutSceneIndex(1);
			else
				UE_LOG(LogTemp, Log, TEXT("ErrorCasting!!"));*/
			break;
		}

		case Serializables::Type::LevelChanged:
		{
			Serializables::LevelChanged Data;
			Data.Read(InputStream);
			ProcessLevelChanged(Data);

			//TODO: Cut scene이 재생중이 아니라면 바로 PostInit, 재생중이라면 아무것도 하지 않음.
			

			break;
		}

		case Serializables::Type::GameStarted:
		{
			Serializables::GameStarted Data;
			Data.Read(InputStream);
			ProcessGameStarted(Data);

			break;
		}

		case Serializables::Type::NewPlayer:
		{
			Serializables::NewPlayer Data;
			Data.Read(InputStream);
			ProcessNewPlayer(Data);
			break;
		}

		case Serializables::Type::PlayerDisconnect:
		{
			Serializables::PlayerDisconnect Data;
			Data.Read(InputStream);
			ProcessPlayerDisconnect(Data);
			break;
		}

		case Serializables::Type::ReadyState:
		{
			Serializables::ReadyState Data;
			Data.Read(InputStream);
			ProcessReadyState(Data);
			break;
		}

		// Actor created
		case Serializables::Type::ActorCreated:
		{
			Serializables::ActorCreated Data;
			Data.Read(InputStream);
			ProcessActorCreated(Data);
			break;
		}

		// Scene info
		case Serializables::Type::SessionStateUpdate:
		{
			Serializables::SessionStateUpdate Data;
			Data.Read(InputStream);
			ProcessSessionStateUpdate(Data);
			break;
		}

		case Serializables::Type::MyCharacterSyncState:
		{
			Serializables::MyCharacterSyncState Data;
			Data.Read(InputStream);
			ProcessMyCharacterSyncState(Data);
			break;
		}

		case Serializables::Type::RotateForTime:
		{
			Serializables::RotateForTime Data;
			Data.Read(InputStream);
			ProcessRotateForTime(Data);
			break;
		}

		case Serializables::Type::InteractableAct:
		{
			Serializables::InteractableAct Data;
			Data.Read(InputStream);
			ProcessInteractableAct(Data);
			break;
		}

		case Serializables::Type::Act:
		{
			Serializables::Act Data;
			Data.Read(InputStream);
			ProcessAct(&Data);
			break;
		}

		case Serializables::Type::ActWithVector:
		{
			Serializables::ActWithVector Data;
			Data.Read(InputStream);
			ProcessAct(&Data);
			//ProcessActWithVector(Data);
			break;
		}

		case Serializables::Type::ActWithTarget:
		{
			Serializables::ActWithTarget Data;
			Data.Read(InputStream);
			ProcessAct(&Data);
			//ProcessActWithTarget(Data);
			break;
		}

		case Serializables::Type::ActWithTargets:
		{
			Serializables::ActWithTargets Data;
			Data.Read(InputStream);
			ProcessAct(&Data);
			break;
		}

		case Serializables::Type::ActWithValue:
		{
			Serializables::ActWithValue Data;
			Data.Read(InputStream);
			ProcessAct(&Data);
			//ProcessActWithValue(Data);
			break;
		}

		case Serializables::Type::Hit:
		{
			Serializables::Hit Data;
			Data.Read(InputStream);
			ProcessHit(Data);
			break;
		}

		case Serializables::Type::DestructibleHit:
		{
			Serializables::DestructibleHit Data;
			Data.Read(InputStream);
			ProcessDestructibleHit(Data);
			break;
		}

		case Serializables::Type::ActorRecognizedAPlayer:
		{
			Serializables::ActorRecognizedAPlayer Data;
			Data.Read(InputStream);
			ProcessRecognize(Data);
			break;
		}

		case Serializables::Type::ToNewPlayerOldPlayerWeapons:
		{
			// Level변경 대기중이라면 현재 GameMode에서는 처리하면 안되고
			// 새로 생성될 GameMode가 처리하도록 넘겨야 한다.
			// GameInstance가 저장하도록 하고 여기선 처리하지 않는다.
			if (LevelTravelReserved)
			{
				GameInstance->GetStagedOldPlayerWeaponsData().Read(InputStream);
				GameInstance->SetOldPlayerWeaponsDataStaged(true);
			}
			// Level변경이 아니고 게임 시작 직후 받은 data라면 바로 처리한다.
			else
			{
				Serializables::ToNewPlayerOldPlayerWeapons Data;
				Data.Read(InputStream);
				ProcessToNewPlayerOldPlayerWeapons(Data);
			}
			break;
		}

		case Serializables::Type::CollisionDebug:
		{
			//Serializables::CollisionDebug Data;
			//Data.Read(InputStream);
			//ProcessCollisionDebug(Data);
			break;
		}

		case Serializables::Type::AttackRangeDebug:
		{
			Serializables::AttackRangeDebug Data;
			Data.Read(InputStream);
			ProcessAttackRangeDebug(Data);
			break;
		}

		case Serializables::Type::TeleportGageChange:
		{
			Serializables::TeleportGageChange Data;
			Data.Read(InputStream);
			ProcessTeleportGage(Data);
			break;
		}

		case Serializables::Type::MaxTeleportGageChange:
		{
			Serializables::MaxTeleportGageChange Data;
			Data.Read(InputStream);
			ProcessMaxTeleportGageChange(Data);
			break;
		}

		case Serializables::Type::StaminaChange:
		{
			Serializables::StaminaChange Data;
			Data.Read(InputStream);
			ProcessStaminaChange(Data);
			break;
		}

		case Serializables::Type::MaxStaminaChange:
		{
			Serializables::MaxStaminaChange Data;
			Data.Read(InputStream);
			ProcessMaxStaminaChange(Data);
			break;
		}

		case Serializables::Type::ItemEquip:
		{
			Serializables::ItemEquip Data;
			Data.Read(InputStream);
			ProcessItemEquip(Data);
			break;
		}

		case Serializables::Type::ItemUnequip:
		{
			Serializables::ItemUnequip Data;
			Data.Read(InputStream);
			ProcessItemUnequip(Data);
			break;
		}

		case Serializables::Type::ItemAcquire:
		{
			Serializables::ItemAcquire Data;
			Data.Read(InputStream);
			ProcessItemAcquire(Data);
			break;
		}

		case Serializables::Type::ItemUnacquire:
		{
			Serializables::ItemUnacquire Data;
			Data.Read(InputStream);
			ProcessItemUnacquire(Data);
			break;
		}

		case Serializables::Type::ItemRemoveFromChest:
		{
			Serializables::ItemRemoveFromChest Data;
			Data.Read(InputStream);
			ProcessItemRemoveFromChest(Data);
			break;
		}

		case Serializables::Type::StatChange:
		{
			Serializables::StatChange Data;
			Data.Read(InputStream);
			ProcessStatChange(Data);
			break;
		}

		case Serializables::Type::Event:
		{
			Serializables::Event Data;
			Data.Read(InputStream);
			ProcessEvent(Data);
			break;
		}

		case Serializables::Type::TriggerNotify:
		{
			Serializables::TriggerNotify Data;
			Data.Read(InputStream);
			ProcessTriggerNotify(Data);

			break;
		}
		// Unhandled message
		case Serializables::Type::Unhandled:
			break;

			// Unhandled message
		default:
			break;
	}
}


void ATheShift_ClientGameModeBase::Logout(AController* Exiting)
{
	//Serializables::Disconnect DisconnectData;
	//Send(DisconnectData);
	//Socket->Close();
	// Clear();
}

ATSPlayer* ATheShift_ClientGameModeBase::GetMyPlayer()
{
	return Cast<ATSPlayer>(GetTSActor(MyActorId));
}

TArray<AActor*> ATheShift_ClientGameModeBase::GetUEActorsByType(TheShift::ActorType Type)
{
	std::list<AActor*> ActorLst;

	TArray<AActor*> ActorArray;
	for (auto& iter : NetworkComps)
	{
		if (iter.Value->GetActorType() == Type)
			ActorArray.Emplace(State.GetUEActor(iter.Key));
	}
	return ActorArray;
}

// MessageQueue<Message>* ATheShift_ClientGameModeBase::GetReceivedDataQueue()
//{
//    return &ReceivedDataQueue;
//}

void ATheShift_ClientGameModeBase::PushInputQueue(Serializables::Input Input)
{
	// Server로 Input data전송
	Serializables::Input InputData;
	InputData = Input;
	InputData.InputId = InputIndex++;
	SendPacket(InputData);

	// Server reconciliation을 위해 queue에 Input정보 쌓아둠
	InputInfo Info;
	Info.InputNum = InputData.InputId;
	Info.InputData = Input;
	Info.Time = std::chrono::high_resolution_clock::now();
	//TODO: 서버로부터 패킷을 받아 큐를 비워줘야 함.
	 InputQueue.push(Info);
}

void ATheShift_ClientGameModeBase::SetupMeshIndexes()
{
	UE_LOG(LogTemp, Log, TEXT("Game Mode SetupSliceMeshIndexes"));
	for (auto& mesh : SlicingMeshes)
	{
		TArray<uint32> indexArray;
		auto RenderData = mesh->GetResourceForRendering();
		auto& bindPoseData = RenderData->LODRenderData.Last(RenderData->LODRenderData.Num() - 1);
		indexArray.Reserve(bindPoseData.GetNumVertices() * 3);
		auto& originIndexVertexBuffer = bindPoseData.MultiSizeIndexContainer;
		originIndexVertexBuffer.GetIndexBuffer(indexArray);

		SlicingMeshIndexes.Emplace(mesh->GetName(), indexArray);
	}
}

// 화면에 directive를 출력한다.
void ATheShift_ClientGameModeBase::DisplayDirective(FString Text)
{
	InstantDirective = Cast<UTSDirectiveContent>(NewObject<UTSDirectiveContent>(UTSDirectiveContent::StaticClass()));
	InstantDirective->RegisterComponent();

	if (InstantDirective)
		InstantDirective->DirectiveMessage = Text;
	
	DirectiveDisplayEvent.Broadcast();
}

// Level의 이름을 읽어와 MapType을 설정한다.
void ATheShift_ClientGameModeBase::InitMapType()
{
	FString LevelName = GetWorld()->GetName();
	LOG_INFO("Level name: %hs", TCHAR_TO_UTF8(*LevelName));

	if (LevelName == TheShift::LevelName::Stage01)
	{
		CurrentMapType = MapType::Stage01;
		CurrentUMapType = UMapType::Stage01;
	}
	else if (LevelName == TheShift::LevelName::Stage02)
	{
		CurrentMapType = MapType::Stage02;
		CurrentUMapType = UMapType::Stage02;
	}
	else if (LevelName == TheShift::LevelName::Stage03)
	{
		CurrentMapType = MapType::Stage03;
		CurrentUMapType = UMapType::Stage03;
	}
	else if (LevelName == TheShift::LevelName::Stage04)
	{
		CurrentMapType = MapType::Stage04;
		CurrentUMapType = UMapType::Stage04;
	}
	else if (LevelName == TheShift::LevelName::TestLandscape)
	{
		CurrentMapType = MapType::Landscape;
		CurrentUMapType = UMapType::Landscape;
	}
	else
	{
		LOG_ERROR("Can't find matching level type: %hs", TCHAR_TO_UTF8(*LevelName));
	}
}

// 현재 level의 MapType을 return한다.
MapType ATheShift_ClientGameModeBase::GetCurrentMapType()
{
	return CurrentMapType;
}

// 현재 level의 UmapType을 return한다.
UMapType ATheShift_ClientGameModeBase::GetCurrentUMapType()
{
	return CurrentUMapType;
}

// 현재 맵에서 1을 더해 return한다.
MapType ATheShift_ClientGameModeBase::GetNextMapType()
{
	switch(CurrentMapType)
	{
	case MapType::Stage01:
		return MapType::Stage02;

	case MapType::Stage02:
		return MapType::Stage03;

	case MapType::Stage03:
		return MapType::Stage04;

	case MapType::Stage04:
		return MapType::MainMenu;

	default:
		return MapType::Unhandled;
	}
}

// 현재 맵에서 1을 더해 return한다.
UMapType ATheShift_ClientGameModeBase::GetNextUMapType()
{
	switch(CurrentMapType)
	{
	case MapType::Stage01:
		return UMapType::Stage02;

	case MapType::Stage02:
		return UMapType::Stage03;

	case MapType::Stage03:
		return UMapType::Stage04;

	case MapType::Stage04:
		return UMapType::MainMenu;

	default:
		return UMapType::MainMenu;
	}
}

void ATheShift_ClientGameModeBase::RegisterNetworkComp(UTSNetworkedComponent* NewComp)
{
	NetworkComps.Add(NewComp->GetUID(), NewComp);
}



bool ATheShift_ClientGameModeBase::Init()
{
	
	
	return true;
}

// Interactable actor들을 등록한다.
void ATheShift_ClientGameModeBase::InitInteractables()
{
	// NetworkedComponent들을 순회하여 태그를 가지고 있는 NetworkedComponent들을 Multimap에 저장한다. (Tag가 key가됨)	
	for (TObjectIterator<UTSNetworkedComponent> It; It; ++It)
	{
		// Tag가 비어있지 않다면 등록한다.
		if(!It->GetActorTag().IsEmpty())
		{
			InteractableNetworkComps.Add((*It)->GetActorTag(), *It);
		}
	}
}

bool ATheShift_ClientGameModeBase::GetConnected() const
{
	return Connected;
}

// 최초 연결 시 처리
bool ATheShift_ClientGameModeBase::ProcessConnected(Serializables::Connected& Data)
{
	UE_LOG(LogTemp, Error, TEXT("Received Connected"));
	// 서버에 접속됨.
	// 로비 화면을 띄우고 데이터를 기다린다.
	// Test용으로 바로 SessionJoinReq를 보낸다.
	MyId = Data.MyId;
	GameInstance->SetMyClientId(MyId);

	//JoinLobby();
	return true;
}

// 채팅을 처리
bool ATheShift_ClientGameModeBase::ProcessChat(Serializables::Chat& Data)
{
	return true;
}

/// <summary>
/// </summary>
bool ATheShift_ClientGameModeBase::ProcessLobbyConnected(Serializables::LobbyConnected& Data)
{
	UE_LOG(LogTemp, Warning, TEXT("Connected to Lobby."));
	return true;
}

// Lobby 업데이트 정보 처리
bool ATheShift_ClientGameModeBase::ProcessLobbyInfo(Serializables::LobbyInfo& Data)
{
	// Lobby의 정보를 주기적으로 받거나 새로 들어갔을때, 게임에서 나왔을때
	// Lobby의 정보를 업데이트하기 위함.
	// TODO: Lobby를 관리하는 class에 정보를 전달해준다.
	// Test용으로 무시

	SessionId TargetSession = NEW_SESSION;
	for (auto& SessionData : Data.Sessions.Array)
	{
		UE_LOG(LogTemp, Warning, TEXT("SeesionId: %d"), SessionData.SessionIndex);
		TargetSession = SessionData.SessionIndex;
	}

	JoinSession(TargetSession);
	return true;
}

// Session최초 연결 시 처리
bool ATheShift_ClientGameModeBase::ProcessSessionConnected(Serializables::SessionConnected& Data)
{
	// 같은 session에 접속된 플레이어 정보를 얻고 출력한다.
	// Test용으로 바로 Ready를 보낸다.
	// 서버에서는 바로 SessionStarted를 보낸다.
	UE_LOG(LogTemp, Error, TEXT("Received SessionConnected"));

	// 맵을 골라서 보내준다. UI랑 같이 작동해야 함.
	Serializables::MapSelect MapSelectData;
	MapSelectData.Map = TheShift::MapType::Stage01;
	//MapSelectData.Map = TheShift::MapType::Landscape;
	SendPacket(MapSelectData);

	// TODO: Session, Lobby 관련 class 추가, Data처리
	HostId = Data.HostId;
	Ready(true);
	return true;
}

/// <summary>
/// Lobby와 Session을 관리하는 class에 해당 data를 넘기고 처리한다.
/// </summary>
bool ATheShift_ClientGameModeBase::ProcessMapSelected(Serializables::MapSelect& Data)
{
	// TODO: Map 선택 정보 업데이트
	// Currently selected map

	return true;
}

// 게임중인 session의 level(map)이 변경될 경우를 처리한다.
bool ATheShift_ClientGameModeBase::ProcessLevelChanged(Serializables::LevelChanged& Data)
{
	// Map초기화, Actor배치
	LoadLevel(Data.Map);

	// Level 변경 대기중, SessionStarted가 바로 와도 처리하지 않고 GameMode가 새로 생성되길 기다린다.
	LevelTravelReserved = true;
	if(auto MyPlayerActor = Cast<ATSPlayer>(GetTSActor(MyActorId)))
	{
		GameInstance->LastStatStored = true;
		GameInstance->LastStoredHp = MyPlayerActor->GetCurrentHp();
		GameInstance->LastStoredStamina = MyPlayerActor->CurrentStamina;
		GameInstance->LastStoredTeleportGage = MyPlayerActor->CurrentTeleportGage;
	}

	return true;
}

// Session 게임 시작 시 처리
bool ATheShift_ClientGameModeBase::ProcessSessionStarted(Serializables::SessionStarted& Data)
{
	// 이 정보를 토대로 World을 초기화한다.
	// map을 불러오고 actor들을 생성하고

	// Timestep 저장.
	ServerUpdateTimestep = Data.ServerUpdateTimestep;
	
	//TODO: Map type validity check해야함. 이 부분에서 많이 터짐.
	State.LoadMap(Data.Map);

	LOG_INFO("Received SessionStarted");
	// TODO: 여기서 새로운 Scene을 생성해야하나?

	TArray<UID> PlayersID;

	for (const auto& Player : Data.Players.Array)
	{
		// Player 추가
		if (Player.Player.ClientId_ > 1)
			PlayersID.Emplace(Player.Player.ActorId_);

		if (Player.Player.ClientId_ == MyId)
			MyActorId = Player.Player.ActorId_;

		// 자신 포함 모든 Player 추가.
		Players.FindOrAdd(Player.Player.ClientId_, std::make_shared<Avatar>(Player.Player));
	}
	// Map초기화, Actor배치
	//LoadLevel(Data.Map);
	StageActors(Data.Actors);

	LOG_INFO("NetworkComps Num: %d", NetworkComps.Num());

	//StageActors(Data.Actors);

	//for(auto& Actor : Data.Actors.Array)
	//{
	//	// Actor들 충 내 Character를 찾는다.
	//	// 있다면 Type을 MyCharacter로 바꿔준다. 컨트롤러를 달아줄 수 있도록
	//	if(Actor.ActorId == MyActor)
	//	{
	//		Actor.Type = ActorType::MyCharacter;
	//	}

	//	SpawnActor(Actor);
	//}


	//TODO: 위 정보대로 초기화가 모두 되었는지 확인해야 함.
	Serializables::LoadCompleted Loaded;
	Loaded.Completed = true;

	SendPacket(Loaded);

	return true;
}

bool ATheShift_ClientGameModeBase::ProcessGameStarted(Serializables::GameStarted& Data)
{
	if (Data.Started)
	{
		// TODO: 게임을 위한 준비가 모두 끝났으므로 Loading화면에서 Game화면으로 넘어간다.
		GameStarted = true;

		// Cut scene이 재생중이 아니라면 바로 PostInit, 재생중이라면 아무것도 하지 않음
		bool IsCutScenePlaying = false;
		for (TActorIterator<ALevelSequenceActor> it(GetWorld()); it; ++it)
		{
			ALevelSequenceActor* SequenceActor = *it;

			if (SequenceActor->GetSequencePlayer()->IsPlaying())
			{
				// 재생중이라면 아무것도 하지 않음.
				// Level blueprint에서 실행할 것.
				IsCutScenePlaying = true;
				LOG_INFO("Sequence is currently playing.");
			}
		}

		// 어떠한 Cut-scene도 재생중이지 않다면 PostOpeningCutSceneInitialize();
		if (!IsCutScenePlaying)
		{
			PostOpeningCutSceneInitialize();
		}
		
		LOG_INFO("Game started.");

		// Cutscene이 재생중인지 확인, 재생중이라면 아무것도 안함
		// 재생중이 아니라면 PostOpenCutSceneInitialize();
		return true;
	}

	return false;
}

// Session에 새로운 Player 접속
bool ATheShift_ClientGameModeBase::ProcessNewPlayer(Serializables::NewPlayer& Data)
{
	LOG_INFO("NewPlayer received.");

	// 새로 들어온 Player 추가
	Players.FindOrAdd(Data.Player.ClientId_, std::make_shared<Avatar>(Data.Player));

	SpawnActor(Data.NewActor);

	return true;
}

// 같은 Session 내 어떤 Player가 연결을 종료했을 때
// Actor를 destroy 한 후 Player도 삭제한다.
bool ATheShift_ClientGameModeBase::ProcessPlayerDisconnect(Serializables::PlayerDisconnect& Data)
{
	auto PtrToDisconnectingPlayer = Players.Find(Data.DisconnectingClientId);
	if (PtrToDisconnectingPlayer != nullptr)
	{
		auto DisconnectingPlayer = *PtrToDisconnectingPlayer;
		auto PtrToNetworkComp = NetworkComps.Find(DisconnectingPlayer->ActorId_);
		if (PtrToNetworkComp != nullptr)
		{
			auto NetworkComp = *PtrToNetworkComp;

			if (NetworkComp != nullptr)
			{
				// Get Actor to destroy
				auto ActorToDestroy = NetworkComp->GetOwner();

				if (ActorToDestroy != nullptr)
				{
					// Remove NetworkComp from container
					NetworkComps.FindAndRemoveChecked(DisconnectingPlayer->ActorId_);
					// Remove Actor

					GetWorld()->DestroyActor(ActorToDestroy);
				}
				else
				{
					LOG_WARNING("Disconnecting player: %d doesn't have any actor to destroy.", Data.DisconnectingClientId);
				}
			}
			else
			{
				LOG_WARNING("Disconnecting player: %d doesn't have any actor to destroy.", Data.DisconnectingClientId);
			}
		}
		else
		{
			LOG_WARNING("Disconnecting player: %d doesn't have any actor to destroy.", Data.DisconnectingClientId);
		}

		// Remove Player from container
		Players.FindAndRemoveChecked(Data.DisconnectingClientId);

		return true;
	}
	else
	{
		LOG_WARNING("Can't find player %d while trying to disconnect player", Data.DisconnectingClientId);
		return false;
	}
}

bool ATheShift_ClientGameModeBase::ProcessReadyState(Serializables::ReadyState& Data)
{
	LOG_INFO("ReadyState received.");

	return true;
}

// 새로운 Actor가 생성되었을때 처리
bool ATheShift_ClientGameModeBase::ProcessActorCreated(Serializables::ActorCreated& Data)
{
	// TODO: 새로운 actor를 생성한다.
	LOG_INFO("ActorCreated received.");
	return true;
}

// Game state 업데이트 처리
bool ATheShift_ClientGameModeBase::ProcessSessionStateUpdate(Serializables::SessionStateUpdate& Data)
{
	// 여기서 actor들을 움직인다.
	for (auto& ActorPosition : Data.ActorPositions.Array)
	{
		// Transform과 Speed를 update해준다.
		if (NetworkComps.Find(ActorPosition.Self) != nullptr)
		{
			std::shared_ptr<TheShift::Actor> CorrespondingActor = State.GetCorrespondingActor(ActorPosition.Self);
			if(CorrespondingActor == nullptr)
			{
				/*LOG_ERROR("CorrespondingActor is nullptr, ActorId: %d", ActorPosition.Self);
				continue;*/
			}

			// TODO: 여기서 보간작업이 일어나야 함?
			AActor* Actor = NetworkComps[ActorPosition.Self]->GetOwner();
			if (Actor != nullptr)
			{
				//UE_LOG(LogTemp, Error, TEXT("Actor Id: %d %f %f %f"), ActorPosition.Self, ActorPosition.Position.X, ActorPosition.Position.Y, ActorPosition.Position.Z);
				//UE_LOG(LogTemp, Error, TEXT("My Id: %d"), MyActor);
				//UE_LOG(LogTemp, Error, TEXT("Actor Location: %f, %f, %f"), Actor->GetTransform().GetLocation().X, Actor->GetTransform().GetLocation().Y, Actor->GetTransform().GetLocation().Z);
				auto PrevTranslation = Actor->GetActorLocation();
				//LOG_INFO("Client Location: %f %f %f", PrevTranslation.X, PrevTranslation.Y, PrevTranslation.Z);
				//LOG_INFO("Server Location: %f %f %f", ActorPosition.Position.X, ActorPosition.Position.Y, ActorPosition.Position.Z);

				//Actor->SetActorLocation(FVector(ActorPosition.Position.X, ActorPosition.Position.Y, ActorPosition.Position.Z));

				if (ActorPosition.Self != MyActorId)
				{
					// 보간
					auto InterpolationDirectionWithMagnitude = FVector(ActorPosition.Position.X, ActorPosition.Position.Y, ActorPosition.Position.Z) - PrevTranslation;
					NetworkComps[ActorPosition.Self]->SetInterpolationDirectionWithMagnitude(InterpolationDirectionWithMagnitude);

					if (auto Character = Cast<ATSCharacter>(Actor))
						Character->SetGoalRotation(ActorPosition.Rotation.Y, ActorPosition.Rotation.Z, ActorPosition.Rotation.X);
					else
					{
						Actor->SetActorRotation(FRotator(ActorPosition.Rotation.Y, ActorPosition.Rotation.Z, ActorPosition.Rotation.X));
						CorrespondingActor->SetRotationWithDegrees(ActorPosition.Rotation);
					}
				}
				////TODO: 내 캐릭터는 다른 방식으로
				//else
				//{
				//	auto newPos = FVector(ActorPosition.Position.X, ActorPosition.Position.Y, ActorPosition.Position.Z);
				//	newPos.Z += 100.f;
				//	if (auto Character = Cast<ATSCharacter>(Actor))
				//		Character->SetActorLocation(FVector(ActorPosition.Position.X, ActorPosition.Position.Y, ActorPosition.Position.Z));
				//	auto directionVec =  FRotator(ActorPosition.Rotation.Y, ActorPosition.Rotation.Z, ActorPosition.Rotation.X).Vector();
				//	
				//	DrawDebugDirectionalArrow(GetWorld(), newPos, newPos + directionVec * 50.f, 2.5f, FColor::Red, false, 0.05f, (uint8)'\000', 3.f);

				//	CorrespondingActor->SetTranslation(ActorPosition.Position.X, ActorPosition.Position.Y, ActorPosition.Position.Z);
				//	CorrespondingActor->SetLastTranslation();
				//}

				FVector NewSpeed(ActorPosition.Speed.X, ActorPosition.Speed.Y, ActorPosition.Speed.Z);
				NetworkComps[ActorPosition.Self]->SetCurrentSpeed(NewSpeed);
			}
			else
			{
				LOG_ERROR("GetOwner returned nullptr.");
			}
		}
		else
		{
			LOG_ERROR("Can't find NetworkedComp %d", static_cast<int>(ActorPosition.Self));
		}
	}

	return true;
}

// 현재 player가 조작하고있는 character의 위치를 서버에서 처리된 위치와 비교하여 보간시킨다.
bool ATheShift_ClientGameModeBase::ProcessMyCharacterSyncState(Serializables::MyCharacterSyncState& Data)
{
	// 내 character가 맞는지 확인
	if(Data.MyActorId != MyActorId)
	{
		return false;
	}

	auto ActorPosition = Data.MyCharacterPosition;	
	if (!NetworkComps[ActorPosition.Self])
		return false;
	AActor* Actor = NetworkComps[ActorPosition.Self]->GetOwner();

	// 보간 먼저
	auto PrevTranslation = Actor->GetActorLocation();
	auto InterpolationDirectionWithMagnitude = FVector(ActorPosition.Position.X, ActorPosition.Position.Y, ActorPosition.Position.Z) - PrevTranslation;
	NetworkComps[ActorPosition.Self]->SetInterpolationDirectionWithMagnitude(InterpolationDirectionWithMagnitude);

	if (auto Character = Cast<ATSCharacter>(Actor))
		Character->SetGoalRotation(ActorPosition.Rotation.Y, ActorPosition.Rotation.Z, ActorPosition.Rotation.X);
	//else
	//{
	//	Actor->SetActorRotation(FRotator(ActorPosition.Rotation.Y, ActorPosition.Rotation.Z, ActorPosition.Rotation.X));
	//	CorrespondingActor->SetRotationWithDegrees(ActorPosition.Rotation);
	//}

	auto newPos = FVector(ActorPosition.Position.X, ActorPosition.Position.Y, ActorPosition.Position.Z);
	newPos.Z += 100.f;
	//if (auto Character = Cast<ATSCharacter>(Actor))
		//Character->SetActorLocation(FVector(ActorPosition.Position.X, ActorPosition.Position.Y, ActorPosition.Position.Z));
	auto directionVec = FRotator(ActorPosition.Rotation.Y, ActorPosition.Rotation.Z, ActorPosition.Rotation.X).Vector();

	DrawDebugDirectionalArrow(GetWorld(), newPos, newPos + directionVec * 50.f, 2.5f, FColor::Red, false, 0.05f, (uint8)'\000', 3.f);

	//std::shared_ptr<TheShift::Actor> CorrespondingActor = State.GetCorrespondingActor(ActorPosition.Self);
	//CorrespondingActor->SetTranslation(ActorPosition.Position.X, ActorPosition.Position.Y, ActorPosition.Position.Z);
	//CorrespondingActor->SetLastTranslation();

	// 모든 처리된 input을 버리고 처리되지 않은 input을 처리한다.
	TimePoint LastProcessedTimePoint;
	std::vector<InputInfo> Inputs;
	//while (!InputQueue.empty())
	for(int i=0; i<InputQueue.size(); ++i)
	{
		auto InputData = InputQueue.front();

		// 서버에서 처리된 input은 버린다.
		if (InputData.InputNum <= Data.LastProcessedInputId)
		{
			InputQueue.pop();
			LastProcessedTimePoint = InputData.Time;
		}
		// 서버에서 처리되지 않은 input들은 game state로 넘겨서 보간한다.
		else
		{
			Inputs.emplace_back(InputData);
		}
		// State에서 server상 위치와 현재 위치를 보간시킨다.

	}
	if(!Inputs.empty())
		State.ApplyUnprocessedInput(Data.MyCharacterPosition, Inputs, MyActorId, LastProcessedTimePoint);

	return true;
}

// 주어진 시간동안 회전하도록 한다.
bool ATheShift_ClientGameModeBase::ProcessRotateForTime(Serializables::RotateForTime& Data)
{
	if(std::shared_ptr<TheShift::Actor> TargetActor = GetCorrespondingActor(Data.Id))
	{
		TargetActor->RotateForTimeWithDegrees(Data.RotateAmount, Data.DurationMilliseconds);
		return true;
	}

	return false;
}

// Interactable act를 처리한다.
bool ATheShift_ClientGameModeBase::ProcessInteractableAct(Serializables::InteractableAct& Data)
{
	LOG_INFO("InteractableAct");
	
	TArray<UTSNetworkedComponent*> TaggedComps;
	InteractableNetworkComps.MultiFind(FString(Data.Tag.c_str()), TaggedComps);

	for(auto& Iter : TaggedComps)
	{
		switch(Data.Type)
		{
		case TheShift::InteractableActionType::ActivateActors:
		{
			
			break;
		}

		case TheShift::InteractableActionType::DeactivateActors:
		{
			
			break;
		}

		case TheShift::InteractableActionType::OpenDoorRotateMinus:
		{
			if(ATSInteractable* Actor = Cast<ATSInteractable>(Iter->GetOwner()))
			{
				if(auto DoorComponent = Cast<UTSDoorComponent>(Actor->GetComponentByClass(UTSDoorComponent::StaticClass())))
				{
					DoorComponent->Open = true;
				}
				FRotator Rotator(0.f, -30.f, 0.f);
				Actor->RotateForTime(Rotator, 1.f);
				//TODO: GameState에서도 똑같이 해줘야 함.
				LOG_INFO("Door rotated minus");
			}
			else
			{
				LOG_WARNING("Interactable is nullptr");
			}
			break;
		}

		case TheShift::InteractableActionType::OpenDoorRotatePlus:
		{
			if(ATSInteractable* Actor = Cast<ATSInteractable>(Iter->GetOwner()))
			{
				if (auto DoorComponent = Cast<UTSDoorComponent>(Actor->GetComponentByClass(UTSDoorComponent::StaticClass())))
				{
					DoorComponent->Open = true;
				}
				FRotator Rotator(0.f, 30.f, 0.f);
				Actor->RotateForTime(Rotator, 1.f);
				//TODO: GameState에서도 똑같이 해줘야 함.
				LOG_INFO("Door rotated plus");
			}
			else
			{
				LOG_WARNING("Interactable is nullptr");
			}
			break;
		}

		//case TheShift::InteractableActionType::NextLevel:
			//break;

		default:
			return false;
		}
	}

	return true;
}

// 원래 존재하는 session에 입장했을 때 이미 있는 player들의 무기 정보를 저장해놓는다.
bool ATheShift_ClientGameModeBase::ProcessToNewPlayerOldPlayerWeapons(Serializables::ToNewPlayerOldPlayerWeapons& Data)
{
	for(auto& WeaponData : Data.SwapWeapons.Array)
	{
		ProcessAct(&WeaponData);
	}

	return true;
}

/// <summary>
/// 서버로부터 받은 BoundingVolume들의 실시간 위치 정보를 처리한다.
/// </summary>
bool ATheShift_ClientGameModeBase::ProcessCollisionDebug(Serializables::CollisionDebug& Data)
{
	UE_LOG(LogTemp, Warning, TEXT("Collision Debug!"));
	for (auto& ActorVolumeDebugInfo : Data.Info.Array)
	{
		FVector Location;
		Location.X = ActorVolumeDebugInfo.Position.X;
		Location.Y = ActorVolumeDebugInfo.Position.Y;
		Location.Z = ActorVolumeDebugInfo.Position.Z;
		//UE_LOG(LogTemp, Warning, TEXT("X: %f Y: %f, Z: %f Radius: %f"), Location.X, Location.Y, Location.Z, ActorVolumeDebugInfo.Radius);

		DrawDebugSphere(
			GetWorld(),
			Location,
			ActorVolumeDebugInfo.Radius,
			32,
			FColor(255, 0, 0),
			false,
			0.025f
		);
		//TODO: 이 정보들을 가지고 화면에 출력해줘야 함.
		switch (ActorVolumeDebugInfo.BoundingVolumeType_)
		{
			case BoundingVolumeType::Sphere:
				break;
			default:
				break;
		}
		//VolumeDebugInfo.RelativeLocation;
		//VolumeDebugInfo.RelativeRotation;
		//VolumeDebugInfo.ScaledExtent;
		//VolumeDebugInfo.HalfHeight;
		//VolumeDebugInfo.Radius;
	}

	return true;
}

// 단순한 Act들을 처리한다.
bool ATheShift_ClientGameModeBase::ProcessAct(Serializables::Act* Data)
{
	// 입력이 있을때 바로 처리하기 때문에 내 Act는 무시한다.
	//if (Data->Self == MyId)
	//	return true;


	// 공격, 스킬 등과 같은 Actor들의 행동을 처리해준다.
	if (NetworkComps.Find(Data->Self) != nullptr)
	{
		AActor* Actor = NetworkComps[Data->Self]->GetOwner();
		if (Actor != nullptr)
		{
			if (auto Character = Cast<ATSCharacter>(Actor))
			{
				Character->Act(Data);
			}
			else
			{
				LOG_INFO("Character is nullptr");
			}
		}
		else
		{
			LOG_ERROR("GetOwner returned nullptr");
			return false;
		}
	}
	else
	{
		LOG_ERROR("Can't find NetworkedComp %d", (int)(Data->Self));
		return false;
	}

	return true;
}

//// Vector를 가지는 Act들을 처리한다.
//// ex) 특정 방향으로 공격
//bool ATheShift_ClientGameModeBase::ProcessActWithVector(Serializables::ActWithVector& Data)
//{
//	// 입력이 있을때 바로 처리하기 때문에 내 Act는 무시한다.
//	if(Data.Self == MyId)
//		return true;
//
//	// 공격, 스킬 등과 같은 Actor들의 행동을 처리해준다.
//	if(NetworkComps.Find(Data.Self) != nullptr)
//	{
//		AActor* Actor = NetworkComps[Data.Self]->GetOwner();
//		if(Actor != nullptr)
//		{
//			if(auto Character = Cast<ATSCharacter>(Actor))
//			{
//				//Player의 Act를 처리한다.
//				FVector Vector(Data.Vector.X, Data.Vector.Y, Data.Vector.Z);
//				Character->Act(Data.Type, Vector);
//			}
//		}
//		else
//		{
//			LOG_ERROR("GetOwner returned nullptr");
//			return false;
//		}
//	}
//	else
//	{
//		LOG_ERROR("Can't find NetworkedComp %d", (int)(Data.Self));
//		return false;
//	}
//
//	return true;
//}
//
//// 특정 Actor를 목표로 한 Act를 처리한다.
//bool ATheShift_ClientGameModeBase::ProcessActWithTarget(Serializables::ActWithTarget& Data)
//{
//	// 입력이 있을때 바로 처리하기 때문에 내 Act는 무시한다.
//	if(Data.Self == MyId)
//		return true;
//
//	// 공격, 스킬 등과 같은 Actor들의 행동을 처리해준다.
//	if(NetworkComps.Find(Data.Self) != nullptr)
//	{
//		AActor* Actor = NetworkComps[Data.Self]->GetOwner();
//		if(Actor != nullptr)
//		{
//			if(auto Character = Cast<ATSCharacter>(Actor))
//			{
//				//Player의 Act를 처리한다.
//				Character->Act(Data.Type, Data.TargetId);
//			}
//		}
//		else
//		{
//			LOG_ERROR("GetOwner returned nullptr");
//			return false;
//		}
//	}
//	else
//	{
//		LOG_ERROR("Can't find NetworkedComp %d", (int)(Data.Self));
//		return false;
//	}
//
//	return true;
//}
//// 4byte의 값을 가지고 있는 Act를 처리한다.
//bool ATheShift_ClientGameModeBase::ProcessActWithValue(Serializables::ActWithValue& Data)
//{
//
//	return false;
//}

// Actor의 피격을 처리한다.
bool ATheShift_ClientGameModeBase::ProcessHit(Serializables::Hit& Data)
{
	auto NetworkCompPtr = NetworkComps.Find(Data.Self);
	auto OtherNetworkCompPtr = NetworkComps.Find(Data.AttackerId);
	if (!NetworkCompPtr)
	{
		LOG_WARNING("Can't find network comp %d", Data.Self);
		return false;
	}
	if(!OtherNetworkCompPtr)
	{
		LOG_WARNING("Can't find network comp %d", Data.AttackerId);
		return false;
	}

	// Destructible actor hit 처리
	if((*NetworkCompPtr)->GetActorType() == ActorType::Destructible)
	{
		LOG_INFO("Hit on destructible.");
		auto DestructibleActor = Cast<ATSDestructible>((*NetworkCompPtr)->GetOwner());
		DestructibleActor->OnHit(Data.Hp, Data.Damage, Data.AttackType, Data.AttackerId, Data.IsDead);

		// Actor 삭제 준비
		if(Data.IsDead)
		{
			// Actor 삭제
			DestroyCorrespondingActor(Data.Self);

			// AActor는 바로 삭제하지 않고 삭제될 준비가 되면 삭제
			ActorsToBeDestroyed.Add(DestructibleActor, false);
		}

		return true;
	}

	ATSCharacter* Actor = Cast<ATSCharacter>((*NetworkCompPtr)->GetOwner());
	ATSCharacter* damager = Cast<ATSCharacter>((*OtherNetworkCompPtr)->GetOwner());
	//Weapon 타입과 Actor찾아오기
	WeaponType DamageWeaponType = damager->GetCurrentWeaponType();
	TArray<AActor*> AttachedActors;
	damager->GetAttachedActors(AttachedActors);
	AActor* Weapon = nullptr;
	for (AActor* actor : AttachedActors)
	{
		if (actor->ActorHasTag("Sword"))
		{
			Weapon = actor;
			break;
		}
	}
	//
	if (Actor != nullptr)
	{
		if (auto Character = Cast<ATSCharacter>(Actor))
		{
			auto CharacterAnimInstance = Character->GetAnimInstance();
			auto PlayerPointer = Cast<ATSPlayer>(damager);
			bool IsBowTeleportFinalAttack = (DamageWeaponType == WeaponType::BOW && PlayerPointer && PlayerPointer->IsTeleporting() && Data.IsDead);

			if (CharacterAnimInstance)
			{
				//플레이어가 타격시 잠깐 플레이어 경직 처리
				if (PlayerPointer)
				{
					if (auto PlayerAnim = Cast<UTSCharacterAnimInstance>(PlayerPointer->GetAnimInstance()))
						PlayerAnim->StopAnim();
				}
				auto CurrentClientController = GetWorld()->GetFirstPlayerController();
				if (damager->GetController() == CurrentClientController || Actor->GetController() == CurrentClientController)
					CurrentClientController->PlayerCameraManager->PlayCameraShake(UTSCameraShake::StaticClass(), 0.5f);

				if (DamageWeaponType == WeaponType::SWORD && Data.IsDead)
					LOG_INFO("Sword Dead!!!");

				//검으로 죽일시에는 Death 애니메이션 출력이 아닌 Slice로 죽임. Death Anim Play X.
				if (!IsBowTeleportFinalAttack)
					Character->OnHit(Data.Hp, Data.Damage, Data.AttackType, Data.AttackerId, Data.IsDead);

				//피 파티클은 어떻게 치든 항상 발생
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), EffectsContainer["BloodParticle"], Actor->FindComponentByClass<UCapsuleComponent>()->GetComponentLocation(), FRotator::ZeroRotator, FVector(0.35f, 0.35f, 0.35f), true, EPSCPoolMethod::None);

				if (DamageWeaponType == WeaponType::HAND)
				{
					UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), EffectsContainer["P_EnergyBlast_Impact"], Actor->FindComponentByClass<UCapsuleComponent>()->GetComponentLocation(), Actor->GetActorRotation(), FVector(0.5f, 0.5f, 0.5f), true, EPSCPoolMethod::None);
					UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), EffectsContainer["P_Grappler_ImpactSpark"], Actor->FindComponentByClass<UCapsuleComponent>()->GetComponentLocation(), Actor->GetActorRotation(), FVector(0.5f, 0.5f, 0.5f), true, EPSCPoolMethod::None);
					UE_LOG(LogTemp, Log, TEXT("EMITTER!!!!!"));
				}


				//검을 껴도 플레이어만 찢어지는이펙트 내기
				//혹은 BOW Teleport Attack으로 적을 죽일 시에
				
				if (DamageWeaponType == WeaponType::BOW && PlayerPointer)
				{
					UGameplayStatics::PlaySoundAtLocation(GetWorld(), SoundsContainer["Arhcer_Hit"], Actor->FindComponentByClass<UCapsuleComponent>()->GetComponentLocation(), 1.3f);
					UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), EffectsContainer["TearingEffect"], Actor->FindComponentByClass<UCapsuleComponent>()->GetComponentLocation(), Weapon->GetActorRotation(),FVector(0.75f, 0.75f, 0.75f));

					float Random = FMath::FRandRange(90.f, -90.f);
					UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), EffectsContainer["TearingEffect"], Actor->FindComponentByClass<UCapsuleComponent>()->GetComponentLocation(), Weapon->GetActorRotation()+FRotator(Random, Random, Random), FVector(1.f, 1.f, 1.f));

					if (PlayerPointer->IsMyPlayer())
						CurrentClientController->PlayerCameraManager->PlayCameraShake(UTSCameraShake::StaticClass(), 0.6f);

				}
				if ((DamageWeaponType == WeaponType::SWORD && PlayerPointer))
				{
					UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), EffectsContainer["TearingEffect"], Actor->FindComponentByClass<UCapsuleComponent>()->GetComponentLocation(), Weapon->GetActorRotation());
					UGameplayStatics::PlaySoundAtLocation(GetWorld(), SoundsContainer["SwordHit"], Actor->FindComponentByClass<UCapsuleComponent>()->GetComponentLocation(), 0.6f);
				}
			}
			// 사망 처리
			if (Data.IsDead)
			{
				auto Player = Cast<ATSPlayer>(Actor);
				//플레이어는 죽이지 않는다-> 죽는 애님 재생 후 다른 플레이어가 와서 살려주면 살아나는 식.
				if (!Player)
				{
					// 절단하는 경우 Actor를 바로 삭제해주므로 사망 애니메이션을 기다리지 않고
					// 바로 삭제함을 표시한다.
					bool ShouldDestroyInstantly = false;

					//절단 처리
					if (/*DamageWeaponType == WeaponType::SWORD || */IsBowTeleportFinalAttack)
					{
						UTSSliceable* SliceComponent = Actor->FindComponentByClass<UTSSliceable>();
						if (!SliceComponent)
							LOG_INFO("This actor doesn't have slicable component.");
						USkeletalMeshComponent* SliceMesh = Actor->FindComponentByClass<USkeletalMeshComponent>();
						if (!SliceMesh)
							LOG_INFO("This actor doesn't have USkeletalMeshComponent.");

						if (SliceComponent && SliceMesh)
						{
							if (Weapon)
							{
								auto SwordMesh = Cast<USkeletalMeshComponent>(Weapon->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
								DrawDebugBox(GetWorld(), SwordMesh->GetComponentLocation(), FVector(30.f, 30.f, 30.f), FColor::Red, false, 10.f);
								DrawDebugLine(GetWorld(), SwordMesh->GetComponentLocation(), SwordMesh->GetComponentLocation() + SwordMesh->GetComponentTransform().GetUnitAxis(EAxis::Y) * 100.f, FColor::Blue, false, 10.f);
								DrawDebugLine(GetWorld(), SwordMesh->GetComponentLocation(), SwordMesh->GetComponentLocation() + SwordMesh->GetComponentTransform().GetUnitAxis(EAxis::X) * 100.f, FColor::Green, false, 10.f);

								SliceComponent->SliceSkeletalMesh(SliceMesh, SwordMesh->GetComponentLocation(), SwordMesh->GetComponentTransform().GetUnitAxis(EAxis::Y),
																  SwordMesh->GetComponentTransform().GetUnitAxis(EAxis::X), GetSlicingMaterial("bloodCap_Mat"));
								ShouldDestroyInstantly = true;
								//MarkActorAsGoodToBeDestroyed(Actor);
							}
							else
								LOG_INFO("Damager doesn't have weapon.");
						}
					}
					else
						LOG_INFO("This weapon type does not slice actors.");

					LOG_INFO("Actor %d is dead.", Data.Self);

					// Actor 삭제
					DestroyCorrespondingActor(Data.Self);

					// AActor는 바로 삭제하지 않고 삭제될 준비가 되면 삭제
					ActorsToBeDestroyed.Add(Character, ShouldDestroyInstantly);
				}
				else
				{
					//플레이어 Dead시 처리
					UE_LOG(LogTemp, Log, TEXT("Player Dead"));
					Player->SetHit(false);
					Player->SetDead();
				}
			}
		}
		else
			UE_LOG(LogTemp, Log, TEXT("Damage Getter is not Acharacter"));
	}
	else
	{
		LOG_ERROR("GetOwner returned nullptr");
		return false;
	}

	return true;
}

bool ATheShift_ClientGameModeBase::ProcessDestructibleHit(Serializables::DestructibleHit& Data)
{
	if(auto NetworkCompPtr = InteractableNetworkComps.Find(FString(Data.Tag.c_str())))
	{
		if (auto DestructibleActor = Cast<ATSDestructible>((*NetworkCompPtr)->GetOwner()))
		{
			DestructibleActor->OnHit(Data.Hp, Data.Damage, Data.AttackType, Data.AttackerId, Data.IsDead);
		}
		else
		{
			LOG_ERROR("Can't find destructible actor %s", Data.Tag.c_str());
		}
	}
	else
	{
		LOG_ERROR("Can't find network component. %s", Data.Tag.c_str());
	}

	return true;
}
// 특정 Actor를 발견했을 때의 행동을 처리한다.
// ex) Monster가 Player 발견 시 특정 소리 재생
bool ATheShift_ClientGameModeBase::ProcessRecognize(Serializables::ActorRecognizedAPlayer& Data)
{
	auto Actor = GetTSActor(Data.Self);
	if (Actor != nullptr)
	{
		LOG_INFO("%d recognized %d!", Data.Self, Data.TargetId);
		auto Character = Cast<ATSCharacter>(Actor);
		Character->OnRecognize(Data.TargetId);
		return true;
	}

	return false;
}

// Debug용 공격 범위를 그린다.
bool ATheShift_ClientGameModeBase::ProcessAttackRangeDebug(Serializables::AttackRangeDebug& Data)
{
	for(auto& DebugInfo : Data.Info.Array)
	{
		FQuat Rotation;
		FRotator Rotator(DebugInfo.Rotation.Y, DebugInfo.Rotation.Z, DebugInfo.Rotation.X);
		Rotation = Rotator.Quaternion();
		
		switch(DebugInfo.BoundingVolumeType_)
		{
		case BoundingVolumeType::AABB:
			DrawDebugBox(
				GetWorld(),
				FVector(DebugInfo.Position.X, DebugInfo.Position.Y, DebugInfo.Position.Z),
				FVector(DebugInfo.ScaledExtent.X, DebugInfo.ScaledExtent.Y, DebugInfo.ScaledExtent.Z),
				Rotation,
				FColor(255, 0, 0),
				false, 3);
			break;

		case BoundingVolumeType::OBB:
			DrawDebugBox(
				GetWorld(),
				FVector(DebugInfo.Position.X, DebugInfo.Position.Y, DebugInfo.Position.Z),
				FVector(DebugInfo.ScaledExtent.X, DebugInfo.ScaledExtent.Y, DebugInfo.ScaledExtent.Z),
				Rotation,
				FColor(255, 0, 0),
				false, 3);
			break;

		case BoundingVolumeType::Capsule:
			break;

		case BoundingVolumeType::Sphere:
			break;

		case BoundingVolumeType::Unhandled:
			break;

		default:
			return false;
		}
	}	

	return true;
}

// 서버로부터 갱신된 teleport gage를 받고 적용시킨다.
bool ATheShift_ClientGameModeBase::ProcessTeleportGage(Serializables::TeleportGageChange& Data)
{
	if (auto TSActor = GetTSActor(Data.Id))
	{
		if (auto Player = Cast<ATSPlayer>(TSActor))
		{
			Player->CurrentTeleportGage = Data.Gage;
			LOG_INFO("Teleport Gage: %d", Player->CurrentTeleportGage);
			if (Data.Id == MyActorId)
				Player->TeleportGageChangeEvent.Broadcast();
			return true;
		}
		else
			return false;

		return true;
	}
	else
		return false;
}

// 서버로부터 갱신된 teleport max gage를 적용시킨다.
bool ATheShift_ClientGameModeBase::ProcessMaxTeleportGageChange(Serializables::MaxTeleportGageChange& Data)
{
	if (auto TSActor = GetTSActor(Data.Id))
	{
		if (auto Player = Cast<ATSPlayer>(TSActor))
		{
			Player->MaxTeleportGage = Data.NewMaxValue;
			LOG_INFO("Max teleport gage changed to %d", Player->MaxTeleportGage);
		}
		else
			return false;
	}
	else
		return false;

	return true;
}

// 서버로부터 캐릭터의 Stamina를 받아 적용시킨다. 
bool ATheShift_ClientGameModeBase::ProcessStaminaChange(Serializables::StaminaChange& Data)
{
	if (auto TSActor = GetTSActor(Data.Id))
	{
		if (auto Player = Cast<ATSPlayer>(TSActor))
		{
			Player->CurrentStamina = Data.NewStaminaValue;
			LOG_INFO("Stamina changed to %d", Player->CurrentStamina);
			if (Data.Id == MyActorId)
				Player->StaminaChangeEvent.Broadcast();
			return true;
		}
		else
			return false;
	}
	else
		return false;

	return true;
}

// 서버로부터 캐릭터의 Max Stamina를 받아 적용시킨다.
bool ATheShift_ClientGameModeBase::ProcessMaxStaminaChange(Serializables::MaxStaminaChange& Data)
{
	if (auto TSActor = GetTSActor(Data.Id))
	{
		if (auto Player = Cast<ATSPlayer>(TSActor))
		{
			Player->MaxStamina = Data.NewMaxStaminaValue;
			LOG_INFO("Max stamina changed to %d", Player->MaxStamina);
			return true;
		}
		else
			return false;
	}
	else
		return false;

	return true;
}

bool ATheShift_ClientGameModeBase::ProcessItemEquip(Serializables::ItemEquip& Data)
{
	return true;
}

bool ATheShift_ClientGameModeBase::ProcessItemUnequip(Serializables::ItemUnequip& Data)
{
	return true;
}

bool ATheShift_ClientGameModeBase::ProcessItemAcquire(Serializables::ItemAcquire& Data)
{
	if(LevelTravelReserved)
	{
		GameInstance->GetStagedItemData().Emplace(Data);
		GameInstance->SetItemDataStaged(true);
		return true;
	}
	if(auto MyActor = Cast<ATSPlayer>(GetTSActor(MyActorId)))
	{
		if (Data.ItemCategory == static_cast<int>(UItemType::Weapon))
			LOG_INFO("Item acquire: ItemType: Weapon");
		if(Data.ItemCategory == static_cast<int>(UItemType::Armor))
			LOG_INFO("Item acquire: ItemType: Armor");
		MyActor->InventoryComp->AcquireItem(Data);
		auto NewItemPtr = MyActor->InventoryComp->Items.Find(Data.Index);
		ItemToAddOnInventoryWidget = *NewItemPtr;
		ItemAcquireEvent.Broadcast();
	}
	return true;
}

bool ATheShift_ClientGameModeBase::ProcessItemUnacquire(Serializables::ItemUnacquire& Data)
{
	return true;
}

bool ATheShift_ClientGameModeBase::ProcessItemRemoveFromChest(Serializables::ItemRemoveFromChest& Data)
{
	LOG_INFO("ProcessItemRemoveFromChest");

	if(auto NetworkCompPtr = InteractableNetworkComps.Find(Data.ChestTag.c_str()))
	{
		auto NetworkComp = *NetworkCompPtr;
		if (auto Chest = Cast<ATSChest>(NetworkComp->GetOwner()))
		{
			if (auto ChestWidget = Chest->ChestInventoryWidget)
			{
				if (auto ChestItemPtr = Chest->Items.Find(Data.Index))
				{
					(*ChestItemPtr)->DestroyComponent();
					Chest->Items.Remove(Data.Index);
					LOG_INFO("Item removed from chest. Index: %d", Data.Index);
				}
				else
				{
					LOG_ERROR("Can't find chest item. Index: %d", Data.Index);
				}
				if (auto ContentPtr = ChestWidget->Contents.Find(Data.Index))
				{
					LOG_INFO("Item acquired from chest.");

					(*ContentPtr)->ItemRemoveFromChestEvent.Broadcast();
				}
				else
					LOG_ERROR("Can't find Chest inventory content. Index: %d", Data.Index);
			}
			else
				LOG_ERROR("Can't find chest widget.");
		}
		else
		{
			LOG_ERROR("Can't find Chest: %s", Data.ChestTag.c_str());
		}
	}
	else
	{
		LOG_ERROR("Can't find interactable: %s", Data.ChestTag.c_str());
	}
	return true;
}

bool ATheShift_ClientGameModeBase::ProcessStatChange(Serializables::StatChange& Data)
{
	if (Data.Id != MyActorId)
		return false;

	// 위젯 percentage set
	// 위젯은 어떻게 얻어옴 ?
	// Player를 가져오고 스탯을 설정해줌
	// 애니메이션 재생하는 event부르지 말고 바로 set해주는 event를 부른다. (모든 stat에 대해)
	if(auto MyPlayerActor = Cast<ATSPlayer>(GetTSActor(MyActorId)))
	{
		MyPlayerActor->SetCurrentHp(Data.Hp);
		MyPlayerActor->CurrentStamina = Data.Stamina;
		MyPlayerActor->CurrentTeleportGage = Data.TeleportGage;
		MyPlayerActor->StatChangeEvent.Broadcast();
		LOG_INFO("Stat init.");
	}

	return true;
}

bool ATheShift_ClientGameModeBase::ProcessEvent(Serializables::Event& Data)
{
	LOG_INFO("Process Event");
	switch(Data.Type)
	{
	case EventType::Stage01_CellDoor3Unlock:
	{
		if (auto InteractableIter = InteractableNetworkComps.Find(FString("CellDoor3")))
		{
			if (auto OwnerActor = (*InteractableIter)->GetOwner())
			{
				if (auto DoorComponent = Cast<UTSDoorComponent>(OwnerActor->GetComponentByClass(UTSDoorComponent::StaticClass())))
				{
					LOG_INFO("Door unlocked.");
					DoorComponent->IsLocked = false;
					DisplayDirective("You found a key. Use it to unlock door.");
				}
				else
					LOG_ERROR("Can't find door component.");
			}
			else
				LOG_ERROR("Can't find Door actor");
		}
		else
			LOG_ERROR("Can't find Tag: %s", "CellDoor3");
		
		break;
	}

	case EventType::Stage01_CellDoor4Unlock:
	{
		if (auto InteractableIter = InteractableNetworkComps.Find(FString("CellDoor4")))
		{
			if (auto OwnerActor = (*InteractableIter)->GetOwner())
			{
				if (auto DoorComponent = Cast<UTSDoorComponent>(OwnerActor->GetComponentByClass(UTSDoorComponent::StaticClass())))
				{
					LOG_INFO("Door unlocked.");
					DoorComponent->IsLocked = false;
				}
				else
					LOG_ERROR("Can't find door component.");
			}
			else
				LOG_ERROR("Can't find Door actor");
		}
		else
			LOG_ERROR("Can't find Tag: %s", "CellDoor4");

		break;
	}

	default:
		return false;
	}

	return true;
}

bool ATheShift_ClientGameModeBase::ProcessTriggerNotify(Serializables::TriggerNotify& Data)
{
	LOG_INFO("Trigger notify received.");
	ALevelSequenceActor* Outer = nullptr;
	switch(static_cast<TriggerActionType>(Data.Act))
	{
	case TriggerActionType::Stage01_PlayBossCutscene:
	{
		ULevelSequencePlayer::CreateLevelSequencePlayer(GetWorld(), SequenceContainer["BossCutScene1"], FMovieSceneSequencePlaybackSettings(), Outer);
		Outer->SequencePlayer->Play();
	
		break;
	} 

	case TriggerActionType::Stage02_PlayBossCutscene:
	{

		break;
	}

	case TriggerActionType::Stage03_PlayBossCutscene:
	{
		break;
	}
	}

	auto NetworkComp = InteractableNetworkComps.Find(Data.Tag.c_str());
	if (NetworkComp)
		(*NetworkComp)->GetOwner()->Destroy();
	return true;
}

/// <summary>
/// 게임 시작 시, 서버로부터 생성 명령 수신 시
/// 해당 Actor를 생성한다.
/// </summary>
AActor* ATheShift_ClientGameModeBase::SpawnActor(Serializables::ActorInfo& Actor)
{
	UE_LOG(LogTemp, Warning, TEXT("SpawnActor"));
	FVector Location(Actor.Position.X, Actor.Position.Y, Actor.Position.Z);
	AActor* NewActor = nullptr;
	bool MyCharacter = false;

	switch (Actor.Type)
	{
		case ActorType::MyCharacter:
		{
			MyActorId = Actor.ActorId;
			State.SetMyActorId(MyActorId);

			UE_LOG(LogTemp, Warning, TEXT("Spawn My Character"));
			// GetWorld()->SpawnActor<ATSPlayer>(Location, FRotator::ZeroRotator);
			APlayerController* Controller = GetWorld()->GetFirstPlayerController();
			if (Cast<ATSPlayerController>(Controller))
				UE_LOG(LogTemp, Log, TEXT("Casting Succeded"));
			if (Controller != nullptr)
			{
				NewActor = Controller->GetPawn();
				MyPlayerActor = Cast<ATSPlayer>(NewActor);
				if (NewActor != nullptr)
				{
					MyCharacter = true;
					NewActor->SetActorLocation(Location);
					UE_LOG(LogTemp, Warning, TEXT("Spawned"));
				}
				else
				{
					UE_LOG(LogTemp, Error, TEXT("Character is nullptr"));
				}
			}

			Actor.Type = ActorType::Player;
			break;
		}

		case ActorType::Player:
		{
			UClass* SpawnActor = StaticLoadClass(AActor::StaticClass(), NULL, TEXT("/Game/Game/Actors/TSPlayerBP.TSPlayerBP_C"));
			if (SpawnActor)
			{
				UE_LOG(LogTemp, Warning, TEXT("Player spawn"));
				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				FRotator TempRotator = FRotator::ZeroRotator;
				NewActor = GetWorld()->SpawnActor(SpawnActor, &Location, &TempRotator, SpawnParams);
			}

			break;
		}

		case ActorType::KnightMonster:
		{
			UClass* SpawnActor = StaticLoadClass(AActor::StaticClass(), NULL, TEXT("/Game/Game/Actors/TSKnightMonsterBP.TSKnightMonsterBP_C"));
			if (SpawnActor)
			{
				LOG_INFO("Knight monster spawn");
				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				FRotator TempRotator = FRotator::ZeroRotator;
				NewActor = GetWorld()->SpawnActor(SpawnActor, &Location, &TempRotator, SpawnParams);
			}
			break;
		}
		case ActorType::FatMonster:
		{
			UClass* SpawnActor = StaticLoadClass(AActor::StaticClass(), NULL, TEXT("/Game/Game/Actors/TSFatMonster_BP.TSFatMonster_BP_C"));
			if (SpawnActor)
			{
				LOG_INFO("Fat monster spawn");
				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				FRotator TempRotator = FRotator::ZeroRotator;
				NewActor = GetWorld()->SpawnActor(SpawnActor, &Location, &TempRotator, SpawnParams);
			}
			break;
		}
		case ActorType::Spider:
		{
			UClass* SpawnActor = StaticLoadClass(AActor::StaticClass(), NULL, TEXT("/Game/Graphic/DynamicMesh/Monster/Spider/BP_TSSpider.BP_TSSpider_C"));
			if (SpawnActor)
			{
				LOG_INFO("Spider monster spawn");
				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				FRotator TempRotator = FRotator::ZeroRotator;
				NewActor = GetWorld()->SpawnActor(SpawnActor, &Location, &TempRotator, SpawnParams);
			}
			break;
		}
		case ActorType::SwordMonster:
		{
			UClass* SpawnActor = StaticLoadClass(AActor::StaticClass(), NULL, TEXT("/Game/Graphic/DynamicMesh/Monster/SwordMonster/TSSwordMonster_BP.TSSwordMonster_BP_C"));
			if (SpawnActor)
			{
				LOG_INFO("SwordMonster spawn");
				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				FRotator TempRotator = FRotator::ZeroRotator;
				NewActor = GetWorld()->SpawnActor(SpawnActor, &Location, &TempRotator, SpawnParams);
			}
			break;
		}
		case ActorType::BlackWareWolf:
		case ActorType::BrownWareWolf:
		case ActorType::WhiteWareWolf:
		{
			UClass* SpawnActor = StaticLoadClass(AActor::StaticClass(), NULL, TEXT("/Game/Graphic/DynamicMesh/Monster/WereWolf/TSWereWolf_BP.TSWereWolf_BP_C"));
			if (SpawnActor)
			{
				LOG_INFO("BlackWareWolf spawn");
				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				FRotator TempRotator = FRotator::ZeroRotator;
				NewActor = GetWorld()->SpawnActor(SpawnActor, &Location, &TempRotator, SpawnParams);
				if (auto Wolf = Cast<ATSWereWolf>(NewActor))
					Wolf->SetWolfType(static_cast<int>(Actor.Type) - 10);
				else
					LOG_INFO("Casting Failed");
			}
			break;
		}

		case ActorType::TestActor:

			break;

		default:
			break;
	}

	if (NewActor != nullptr)
	{
		auto NetworkComp = Cast<UTSNetworkedComponent>(NewActor->GetComponentByClass(UTSNetworkedComponent::StaticClass()));
		if (NetworkComp != nullptr)
		{
			NetworkComp->SetUID(Actor.ActorId);
			NetworkComps.FindOrAdd(Actor.ActorId, NetworkComp);

			// GameState에도 만들어준다.
			if(MyCharacter)
			{
				LOG_INFO("My character spawn! Id: %d", MyActorId);
				State.CreateActor(Actor.Type, Eigen::Vector3f(Location.X, Location.Y, Location.Z), Actor.ActorId);
			}

			LOG_WARNING("Found NetworkedComponent: %d", (int)(Actor.ActorId));
		}
		else
			UE_LOG(LogTemp, Warning, TEXT("Can't find NetworkedComponent"));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("NewActor is nullptr"));
	}

	return NewActor;
}

// Level load 시 원래 level에서 화면 전화 효과를 재생한다.
void ATheShift_ClientGameModeBase::PlayPreLevelTransition()
{
	//TODO: 현재 Level이 stage인지 메인 메뉴 등 인지 확인 후 그에 따른 효과를 재생한다.
	FString LevelName = UGameplayStatics::GetCurrentLevelName(GetWorld());

	if (LevelName.Contains("Stage") == true)
	{

	}
	else if (LevelName == "MainMenu")
	{

	}
	// Test Stage
	else if (LevelName == "Landscape")
	{
		// None	
	}
	else
	{
		LOG_ERROR("Invalid level name: %hs", TCHAR_TO_UTF8(*LevelName));
	}
}

// Level load 시 변경 완료 된 level에서 화면 전환 효과를 재생한다.
void ATheShift_ClientGameModeBase::PlayPostLevelTransition()
{
	//TODO: 현재 Level이 stage인지 메인 메뉴 등 인지 확인 후 그에 따른 효과를 재생한다.
	FString LevelName = UGameplayStatics::GetCurrentLevelName(GetWorld());

	if (LevelName.Contains("Stage") == true)
	{

	}
	else if (LevelName == "MainMenu")
	{

	}
	// Test Stage
	else if (LevelName == "Landscape")
	{
		// None	
	}
	else
	{
		LOG_ERROR("Invalid level name: %hs", TCHAR_TO_UTF8(*LevelName));
	}
}

void ATheShift_ClientGameModeBase::JoinLobby()
{
	Serializables::LobbyJoinReq Data;
	Data.MyId = MyId;
	Data.Name = TCHAR_TO_UTF8(*MyName);
	SendPacket(Data);
}

void ATheShift_ClientGameModeBase::JoinSession(SessionId Id)
{
	Serializables::SessionJoinReq Data;
	Data.SessionIndex = Id;
	SendPacket(Data);
}

void ATheShift_ClientGameModeBase::Ready(bool IsReady)
{
	Serializables::Ready Data;
	Data.IsReady = IsReady;
	SendPacket(Data);
}

void ATheShift_ClientGameModeBase::MapSelect(MapType Map)
{
	Serializables::MapSelect Data;
	Data.Map = Map;
	Data.SessionIndex = CurrentSessionId;
	SendPacket(Data);
}

void ATheShift_ClientGameModeBase::ExitSession()
{
	Serializables::SessionExit Data;
	Data.CurrentSessionId = CurrentSessionId;
	SendPacket(Data);
}

// GameState에서 사용하는 Actor가 필요할때 사용한다. TheShift::Actor에 대한 shared_ptr을 return.
std::shared_ptr<TheShift::Actor> ATheShift_ClientGameModeBase::GetCorrespondingActor(UID Id)
{
	return State.GetCorrespondingActor(Id);
}

void ATheShift_ClientGameModeBase::DestroyCorrespondingActor(UID Id)
{
	State.DestroyActor(Id);
	if (NetworkComps.Find(Id) != nullptr)
		NetworkComps.FindAndRemoveChecked(Id);
}

AActor* ATheShift_ClientGameModeBase::GetTSActor(UID Id)
{
	auto Comp = NetworkComps.Find(Id);
	if (Comp != nullptr)
	{
		return (*Comp)->GetOwner();
	}
	else
	{
		LOG_WARNING("Can't find actor. Id: %d", Id);
	}
	return nullptr;
}

float ATheShift_ClientGameModeBase::GetServerUpdateTimestep() const
{
	return ServerUpdateTimestep;
}

// Serialized actor info를 읽고 배치 후 visibility를 false로 set한다.
void ATheShift_ClientGameModeBase::StageActors(Serializables::SerializableVector<Serializables::ActorInfo>& SerializedActors)
{
	for (auto& Actor : SerializedActors.Array)
	{
		// Actor들 충 내 Character를 찾는다.
		// 있다면 Type을 MyCharacter로 바꿔준다. 컨트롤러를 달아줄 수 있도록
		if (Actor.ActorId == MyActorId)
		{
			Actor.Type = ActorType::MyCharacter;
		}

		// 초기 load에선 보이지 않도록 한다. (Cut-scene에서 보이면 안되는 actor들이 보이는 경우와 같은 상황을 방지)
		if (ATSCharacter* SpawnedActor = Cast<ATSCharacter>(SpawnActor(Actor)))
		{
			SpawnedActor->GetMesh()->SetVisibility(false, true);
		}
	}

	LOG_INFO("StageActors successful.");
}

// Level을 불러온다.
//TODO: 현재 Level에 따라 transition효과를 달리 해야함. ex) in-game중에선 fade-out
void ATheShift_ClientGameModeBase::LoadLevel(MapType Type)
{
	//if(CurrentMapType == Type)
	//{
	//	return;
	//}
	
	//TODO: 오류체크. MapType이 오류값이 넘어온다면 Session에서 아예 나오고 함수 종료하기.
	// 기존의 Map이 load돼있었다면 초기화 한 후 넘겨받은 MapType을 load한다.
	//State.LoadMap(Type);

	switch (Type)
	{
		case MapType::Landscape:
			// PlayPreLevelTransition();
			UGameplayStatics::OpenLevel(GetWorld(), UTF8_TO_TCHAR(LevelName::TestLandscape), false);
			break;

		case MapType::Stage01:
			// PlayPreLevelTransition();
			UGameplayStatics::OpenLevel(GetWorld(), UTF8_TO_TCHAR(LevelName::Stage01), false);
			break;

		case MapType::Stage02:
			// PlayPreLevelTransition();
			UGameplayStatics::OpenLevel(GetWorld(), UTF8_TO_TCHAR(LevelName::Stage02), false);
			break;

		case MapType::Stage03:
			// PlayPreLevelTransition();
			UGameplayStatics::OpenLevel(GetWorld(), UTF8_TO_TCHAR(LevelName::Stage03), false);
			break;

		case MapType::Stage04:
			// PlayPreLevelTransition();
			UGameplayStatics::OpenLevel(GetWorld(), UTF8_TO_TCHAR(LevelName::Stage04), false);
			break;

		default:
			LOG_ERROR("Can't open invalid level type, MapType: %d", Type);
			break;
	}

	// 새로운 level에서 변경효과 재생
	// PlayPostLevelTransition();
}

void ATheShift_ClientGameModeBase::MarkActorAsGoodToBeDestroyed(AActor* Actor)
{
	bool* Value = ActorsToBeDestroyed.Find(Actor);
	if (Value != nullptr)
	{
		*Value = true;
	}
	else
	{
		LOG_WARNING("This Actor is not registered as ToBeDestroyed.");
	}
}

TArray<uint32>* ATheShift_ClientGameModeBase::GetSlicingMeshIndexes(FString MeshName)
{
	/*UE_LOG(LogTemp, Log, TEXT("There is %d Num MeshIndexes"), SlicingMeshIndexes.Num());*/

	auto iter = SlicingMeshIndexes.Find(MeshName);
	if (iter == nullptr)
		return nullptr;
	else
		return iter;
}

UMaterialInterface* ATheShift_ClientGameModeBase::GetSlicingMaterial(FString MaterialName)
{
	/*for (auto& iter : SlicingMaterialsContainer)
		UE_LOG(LogTemp, Log, TEXT("%s"), *iter.Key);
	UE_LOG(LogTemp, Log, TEXT("There is %d Num SlicingMaterial"), SlicingMaterialsContainer.Num());*/
	auto iter = SlicingMaterialsContainer.Find(MaterialName);
	if (iter == nullptr)
		return nullptr;
	else
		return *iter;
}

// 게임 중 띄울 Widget을 변경한다. (Inventory, 메뉴 등, HUD 제외)
UUserWidget* ATheShift_ClientGameModeBase::ChangeIngameWidget(TSubclassOf<UUserWidget> WidgetClass)
{
	if(CurrentPopUpWidget != nullptr)
	{
		CurrentPopUpWidget->RemoveFromViewport();
		CurrentPopUpWidget = nullptr;
	}
	if(WidgetClass != nullptr)
	{
		CurrentPopUpWidget = CreateWidget<UUserWidget>(GetWorld(), WidgetClass);
		if(CurrentPopUpWidget != nullptr)
		{
			// 새로 등록한 widget을 화면에 띄운다.
			CurrentPopUpWidget->AddToViewport();
			return CurrentPopUpWidget;
		}
	}

	return nullptr;
}

void ATheShift_ClientGameModeBase::SendItemAcquireFromChestReq(UTSItemComponent* Item)
{
	Serializables::ItemAcquireFromChestReq Data;
	Data.Index = Item->Index;
	if(auto Chest = Cast<ATSChest>(Item->GetOwner()))
	{
		if(auto NetworkComp = Cast<UTSNetworkedComponent>(Chest->GetComponentByClass(UTSNetworkedComponent::StaticClass())))
		{
			Data.ChestTag = TCHAR_TO_UTF8(*NetworkComp->GetActorTag());
			SendPacket(Data);
		}
	}
}
