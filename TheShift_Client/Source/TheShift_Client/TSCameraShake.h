// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraShake.h"
#include "TSCameraShake.generated.h"

/**
 *
 */
UCLASS()
class THESHIFT_CLIENT_API UTSCameraShake : public UCameraShake
{
	GENERATED_BODY()

	UTSCameraShake();

};
