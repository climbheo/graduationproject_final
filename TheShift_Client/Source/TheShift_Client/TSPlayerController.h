﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TSPlayerController.generated.h"

/**
 *
 */
UCLASS()
class THESHIFT_CLIENT_API ATSPlayerController : public APlayerController
{
	GENERATED_BODY()


public:
	void ChangeInputMode(bool isGameMode = true);

protected:
	virtual void BeginPlay() override;
	virtual void BeginDestroy() override;


	FInputModeUIOnly UIOnlyInputMode;
	FInputModeGameOnly GameOnlyInputMode;
};
