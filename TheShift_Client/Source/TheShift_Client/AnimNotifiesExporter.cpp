﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "AnimNotifiesExporter.h"
#include "Animation/AnimSequence.h"

void UAnimNotifiesExporter::ExportAnimMontageNotifies(std::vector<TheShift::AnimNotifyInfo>* pOut, UAnimMontage* pMontage, std::wstring AdditionalName)
{
	if (pMontage)
	{
		auto& sections = pMontage->CompositeSections;
		int sectionNum = sections.Num();

		//섹션 한개가 곧 한개의 SlotAnim
		//섹션의 이름은 곧 그 AnimNotify의 이름
		//섹션이름이 같을땐 인자로 오는 AdditionalName으로 각 Montage를 구분
		auto& notifies = pMontage->Notifies;

		for (int i = 0; i < sectionNum; ++i)
		{
			float start = 0.f;
			float end = 0.f;
			pMontage->GetSectionStartAndEndTime(i, start, end);
			
			TheShift::AnimNotifyInfo newNotifyInfo;
			//이름을 먼저 넣어준다
			std::wstring animNotfiName_W = std::wstring(AdditionalName + (*sections[i].SectionName.ToString()));
			newNotifyInfo.AnimName.assign(animNotfiName_W.begin(), animNotfiName_W.end());

			//노티파이들 중 현재 Section과 Segement가 일치하는 Notify들을 넣어준다.
			//서버로 넘어가야 할 Notify는 HitCheck(피격판정), NextAttackCheck(콤보체크), 등 여타 노티파이들임(계속 추가될것).
			for (auto& notify : notifies)
			{
				//현재 Section과 노티파이의 Segemnt가 일치한다면 넣어준다.
				if (notify.GetSegmentIndex() == i || sectionNum == 1)
				{
					FString curNotifyName = notify.NotifyName.ToString();
					//Editor상의 TriggerTime에서 현재 Section의 StartTime을 빼준다.
					float fixedTime = notify.GetTriggerTime() - start;
					if (notify.NotifyName.ToString().Contains("HitCheck"))
						newNotifyInfo.AnimNotifies.emplace_back("HitCheck", fixedTime);
					else if (notify.NotifyName.ToString().Contains("NextAttackCheck"))
						newNotifyInfo.AnimNotifies.emplace_back("NextAttackCheck", fixedTime);
					else if (notify.NotifyName.ToString().Contains("TeleportMove"))
						newNotifyInfo.AnimNotifies.emplace_back("TeleportMove", fixedTime);
					else if (notify.NotifyName.ToString().Contains("AttackEnd"))
						newNotifyInfo.AnimNotifies.emplace_back("AttackEnd", fixedTime);
					else if (notify.NotifyName.ToString().Contains("MoveStart"))
						newNotifyInfo.AnimNotifies.emplace_back("MoveStart", fixedTime);
					else if (notify.NotifyName.ToString().Contains("ChasingStop"))
						newNotifyInfo.AnimNotifies.emplace_back("ChasingStop", fixedTime);
				}
				else
				{
					if (AdditionalName == L"")
					{
						UE_LOG(LogTemp, Log, TEXT("%d Section Index's Notify : %s is Missing"), notify.GetSegmentIndex(), *notify.NotifyName.ToString());
						UE_LOG(LogTemp, Log, TEXT("Section Num : %d"), sectionNum);
					}
				}
			}
			pOut->emplace_back(newNotifyInfo);
		}
	}
}

void UAnimNotifiesExporter::ExportSlotAnimNotifies(std::vector<TheShift::AnimNotifyInfo>* pOut, UAnimSequence* pSlotAnim, float PlayRate)
{
	if(pSlotAnim)
	{
		//Montage와 다르게 SlotAnim의 이름을 바로 Anim이름으로 넣어준다.
		FName animName = pSlotAnim->GetFName();
		auto& notifies = pSlotAnim->Notifies;
		float originEndTime = pSlotAnim->GetPlayLength();
		float newEndTime = originEndTime / PlayRate;

		TheShift::AnimNotifyInfo newNotifyInfo;
		//이름을 먼저 넣어준다
		std::wstring animNotfiName_W = std::wstring(*animName.ToString());
		newNotifyInfo.AnimName.assign(animNotfiName_W.begin(), animNotfiName_W.end());

		//노티파이들 중 현재 Section과 Segement가 일치하는 Notify들을 넣어준다.
		//서버로 넘어가야 할 Notify는 HitCheck(피격판정), NextAttackCheck(콤보체크), (이하 추가)임.
		for (auto& notify : notifies)
		{
			FString curNotifyName = notify.NotifyName.ToString();
			//Editor상의 TriggerTime에서 현재 Section의 StartTime을 빼준다.
			//SlotAnim은 PlayRate를 조절해준다.
			float fixedTime = newEndTime * notify.GetTriggerTime() / originEndTime;
			if (notify.NotifyName.ToString().Contains("HitCheck"))
				newNotifyInfo.AnimNotifies.emplace_back("HitCheck", fixedTime);
			else if (notify.NotifyName.ToString().Contains("NextAttackCheck"))
				newNotifyInfo.AnimNotifies.emplace_back( "NextAttackCheck", fixedTime);
			else if (notify.NotifyName.ToString().Contains("DashEnd"))
				newNotifyInfo.AnimNotifies.emplace_back("DashEnd", fixedTime);
			else if (notify.NotifyName.ToString().Contains("HitReset"))
				newNotifyInfo.AnimNotifies.emplace_back("HitReset", fixedTime);
			else if (notify.NotifyName.ToString().Contains("AttackEnd"))
				newNotifyInfo.AnimNotifies.emplace_back("AttackEnd", fixedTime);
			else if (notify.NotifyName.ToString().Contains("MoveStart"))
				newNotifyInfo.AnimNotifies.emplace_back("MoveStart", fixedTime);
			else if (notify.NotifyName.ToString().Contains("ChasingStop"))
				newNotifyInfo.AnimNotifies.emplace_back("ChasingStop", fixedTime);
		}
		pOut->emplace_back(newNotifyInfo);
	}
}
