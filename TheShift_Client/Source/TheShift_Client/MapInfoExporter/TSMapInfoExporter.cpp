﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TSMapInfoExporter.h"
#include "EngineUtils.h"
#include "Network/Components/TSNetworkedComponent.h"
#include "Network/Components/TSBoxCollider.h"
#include "Network/Components/TSSphereCollider.h"
#include "Network/Components/TSCapsuleCollider.h"
#include "Components/BoxComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SphereComponent.h"
#include "LandscapeComponent.h"
#include "Landscape.h"
#include "Actors/TSWereWolf.h"
#include "Item/TSArmorItemComponent.h"
#include "Item/TSItemComponent.h"
#include "Item/TSWeaponItemComponent.h"
#include "Misc/Paths.h"

// Sets default values
ATSMapInfoExporter::ATSMapInfoExporter()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	SetTickGroup(TG_PostUpdateWork);


}

// Called when the game starts or when spawned
void ATSMapInfoExporter::BeginPlay()
{
	Super::BeginPlay();

	UE_LOG(LogTemp, Log, TEXT("EXPORTER!!!!!!!!!!!!!!!!!!!!!!"));

	TArray<AActor*> Wolves;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATSWereWolf::StaticClass(), Wolves);
	for (auto iter : Wolves)
	{
		if (auto Wolf = Cast<ATSWereWolf>(iter))
		{
			Wolf->ApplyMeshAndActorType();
		}
		else
			UE_LOG(LogTemp, Log, TEXT("Casting Failed"));
	}
	UE_LOG(LogTemp, Log, TEXT("Wolf Mesh Num : %d"), Wolves.Num());
	ExportMapInfo();

}

// Called every frame
void ATSMapInfoExporter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATSMapInfoExporter::ExportMapInfo()
{
	MapInfo Info;
	Info.Type = GetMapType(TCHAR_TO_ANSI(*GetWorld()->GetName()));
	UE_LOG(LogTemp, Warning, TEXT("Map Name: %s"), *GetWorld()->GetName());

	// Landscapes info
	// Heightmap을 위한 정보
	for(TActorIterator<ALandscape> It(GetWorld()); It; ++It)
	{
		HeightmapInfo Heightmap;

		It->GetSimpleCollisionHalfHeight();

		Heightmap.FileName = "";
		auto HeightmapLocation = It->GetActorLocation();
		auto HeightmapRotation = It->GetActorRotation();
		auto HeightmapScale = It->GetActorScale3D();
		Heightmap.Position.X = HeightmapLocation.X;
		Heightmap.Position.Y = HeightmapLocation.Y;
		Heightmap.Position.Z = HeightmapLocation.Z;
		Heightmap.Rotation.X = HeightmapRotation.Roll;
		Heightmap.Rotation.Y = HeightmapRotation.Pitch;
		Heightmap.Rotation.Z = HeightmapRotation.Yaw;
		Heightmap.Scale.X = HeightmapScale.X;
		Heightmap.Scale.Y = HeightmapScale.Y;
		Heightmap.Scale.Z = HeightmapScale.Z;

		Info.Heightmaps.emplace_back(Heightmap);
	}

	// Actor들의 위치 정보, Bounding volume 정보
	for(TObjectIterator<UTSNetworkedComponent> It; It; ++It)
	{
		// 현재 world에 없거나 Export속성이 false라면 export하지 않는다.
		if(It->GetWorld() != GetWorld() || !It->GetWhetherExport())
			continue;

		ActorPlacementInfo PlacementInfo;
		ActorInfo ActorTypeInfo;

		auto OwnerActor = It->GetOwner();

		auto NetworkComp = Cast<UTSNetworkedComponent>(OwnerActor->GetComponentByClass(UTSNetworkedComponent::StaticClass()));
		auto ActorTag = NetworkComp->GetActorTag();

		if(ActorTag == FString("CutScene"))
			continue;
		PlacementInfo.Tag = TCHAR_TO_UTF8(*ActorTag);

		PlacementInfo.Type = NetworkComp->GetActorType();
		auto ActorLocation = OwnerActor->GetActorLocation();
		auto ActorScale = OwnerActor->GetActorScale();
		PlacementInfo.ActorPosition.X = ActorLocation.X;
		PlacementInfo.ActorPosition.Y = ActorLocation.Y;
		PlacementInfo.ActorPosition.Z = ActorLocation.Z;

		auto ActorRotation = OwnerActor->GetActorRotation();
		PlacementInfo.ActorRotation.X = ActorRotation.Roll;
		PlacementInfo.ActorRotation.Y = ActorRotation.Pitch;
		PlacementInfo.ActorRotation.Z = ActorRotation.Yaw;
		//if(ActorScale.X < 0.f)
		//	PlacementInfo.ActorRotation.X *= -1.f;
		//if(ActorScale.Y < 0.f)
		//	PlacementInfo.ActorRotation.Y *= -1.f;
		//if(ActorScale.Z < 0.f)
		//	PlacementInfo.ActorRotation.Z *= -1.f;

		////////////////// Bounding Volume //////////////////
		auto Boxes = OwnerActor->GetComponentsByClass(UBoxComponent::StaticClass());
		auto Capsules = OwnerActor->GetComponentsByClass(UCapsuleComponent::StaticClass());
		auto Spheres = OwnerActor->GetComponentsByClass(USphereComponent::StaticClass());
		auto Items = OwnerActor->GetComponentsByClass(UTSItemComponent::StaticClass());

		// OBB
		for(auto& Element : Boxes)
		{
			auto Box = Cast<UBoxComponent>(Element);

			// Export 속성이 꺼져있으면 export하지 않는다.
			if(auto TSBox = Cast<UTSBoxCollider>(Element))
			{
				if (!TSBox->Export)
					continue;
			}
			//if (!Box->GetCollisionEnabled())
			//	continue;

			BoundingVolumeInfo Info;
			Info.BoundingVolumeType_ = BoundingVolumeType::OBB;
			auto Extent = Box->GetScaledBoxExtent();
			auto Location = Box->GetRelativeTransform().GetLocation();
			auto Rotation = Box->GetRelativeTransform().GetRotation();
			
			//Info.ScaledExtent.X = Extent.X;
			//Info.ScaledExtent.Y = Extent.Y;
			//Info.ScaledExtent.Z = Extent.Z;
			Info.ScaledExtent.X = Extent.X < 0.f ? -Extent.X : Extent.X;
			Info.ScaledExtent.Y = Extent.Y < 0.f ? -Extent.Y : Extent.Y;
			Info.ScaledExtent.Z = Extent.Z < 0.f ? -Extent.Z : Extent.Z;
			Info.RelativeLocation.X = Location.X * ActorScale.X;
			Info.RelativeLocation.Y = Location.Y * ActorScale.Y;
			Info.RelativeLocation.Z = Location.Z * ActorScale.Z;
			Info.RelativeRotation.X = Rotation.Rotator().Roll;
			Info.RelativeRotation.Y = Rotation.Rotator().Pitch;
			Info.RelativeRotation.Z = Rotation.Rotator().Yaw;
			//if(ActorScale.X < 0.f)
			//	Info.RelativeRotation.X *= -1.f;
			//if(ActorScale.Y < 0.f)
			//	Info.RelativeRotation.Y *= -1.f;
			//if(ActorScale.Z < 0.f)
			//	Info.RelativeRotation.Z *= -1.f;

			PlacementInfo.BoundInfo.emplace_back(Info);
		}

		// Capsule
		for(auto& Element : Capsules)
		{
			auto Capsule = Cast<UCapsuleComponent>(Element);

			// Export 속성이 꺼져있으면 export하지 않는다.
			if(auto TSCapsule = Cast<UTSCapsuleCollider>(Element))
			{
				if (!TSCapsule->Export)
					continue;
			}
			//if (!Capsule->GetCollisionEnabled())
			//	continue;

			BoundingVolumeInfo Info;
			Info.BoundingVolumeType_ = BoundingVolumeType::Capsule;
			auto HalfHeight = Capsule->GetScaledCapsuleHalfHeight();
			auto Radius = Capsule->GetScaledCapsuleRadius();
			auto Location = Capsule->GetRelativeTransform().GetLocation();
			auto Rotation = Capsule->GetRelativeTransform().GetRotation();
			Info.HalfHeight = HalfHeight;
			Info.Radius = Radius;

			Info.RelativeLocation.X = Location.X * ActorScale.X;
			Info.RelativeLocation.Y = Location.Y * ActorScale.Y;
			Info.RelativeLocation.Z = Location.Z * ActorScale.Z;
			Info.RelativeRotation.X = Rotation.Rotator().Roll;
			Info.RelativeRotation.Y = Rotation.Rotator().Pitch;
			Info.RelativeRotation.Z = Rotation.Rotator().Yaw;
			//if(ActorScale.X < 0.f)
				//Info.RelativeRotation.X *= -1.f;
			//if(ActorScale.Y < 0.f)
				//Info.RelativeRotation.Y *= -1.f;
			//if(ActorScale.Z < 0.f)
				//Info.RelativeRotation.Z *= -1.f;

			PlacementInfo.BoundInfo.emplace_back(Info);
		}

		// Sphere
		for(auto& Element : Spheres)
		{
			auto Sphere = Cast<USphereComponent>(Element);

			// Export 속성이 꺼져있으면 export하지 않는다.
			if(auto TSSphere = Cast<UTSSphereCollider>(Element))
			{
				if (!TSSphere->Export)
					continue;
			}
			//if (!Sphere->GetCollisionEnabled())
			//	continue;

			BoundingVolumeInfo Info;
			Info.BoundingVolumeType_ = BoundingVolumeType::Sphere;
			auto Radius = Sphere->GetScaledSphereRadius();
			auto Location = Sphere->GetRelativeTransform().GetLocation();
			auto Rotation = Sphere->GetRelativeTransform().GetRotation();
			Info.Radius = Radius;
			Info.RelativeLocation.X = Location.X * ActorScale.X;
			Info.RelativeLocation.Y = Location.Y * ActorScale.Y;
			Info.RelativeLocation.Z = Location.Z * ActorScale.Z;

			PlacementInfo.BoundInfo.emplace_back(Info);
		}
		////////////////// Bounding Volume //////////////////

		for(auto& Element : Items)
		{
			ItemInfo ItemData;
			if (auto Item = Cast<UTSItemComponent>(Element))
			{
				ItemData.Index = Item->Index;
				ItemData.ItemCategory = static_cast<int>(Item->Type);
				switch (Item->Type)
				{
				case UItemType::Weapon:
				{
					if (auto WeaponItem = Cast<UTSWeaponItemComponent>(Item))
					{
						ItemData.ItemType = static_cast<int>(WeaponItem->WeaponType);
					}
					else
						continue;

					break;
				}

				case UItemType::Armor:
				{
					if (auto ArmorItem = Cast<UTSArmorItemComponent>(Item))
					{
						ItemData.ItemType = static_cast<int>(ArmorItem->ArmorType);
					}
					else
						continue;
					break;
				}

				//case UItemType::Expendable:
				//{
					//break;
				//}

				default:
					continue;
				}
			}
			else
				continue;

			PlacementInfo.InventoryInfo.emplace_back(ItemData);
		}

		PlacementInfo.IsMovable = OwnerActor->IsRootComponentMovable();

		Info.ActorPlacements.emplace_back(PlacementInfo);

		// 중복 체크
		bool AlreadyRegistered = false;
		for(auto& Info : Info.ActorTypeInfo)
		{
			if(Info.Type == PlacementInfo.Type)
				AlreadyRegistered = true;
		}

		// 이미 등록된 Actor의 정보는 추가 할 필요가 없음
		if(AlreadyRegistered == false)
		{
			ActorTypeInfo.Type = PlacementInfo.Type;
			ActorTypeInfo.Stat = NetworkComp->GetStat();

			ActorTypeInfo.BoundInfo = PlacementInfo.BoundInfo;
			// Is Movable
			ActorTypeInfo.IsMovable = PlacementInfo.IsMovable;
			Info.ActorTypeInfo.emplace_back(ActorTypeInfo);
		}

		//UE_LOG(LogTemp, Warning, TEXT("Physics Actor Added"));

	}

	FString TargetPath = FPaths::ProjectContentDir() + TEXT("Protocol/Json/Maps/") + GetWorld()->GetName() + TEXT(".json");
	JsonExporter.ExportMapInfo(TCHAR_TO_ANSI(*TargetPath), Info);
	JsonExporter.Close();
}