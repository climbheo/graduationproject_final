﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TheShift_MainMenuGameModeBase.h"

#include "TheShift_GameInstanceBase.h"
#include "Protocol/Log.h"
#include "UserWidget.h"

ATheShift_MainMenuGameModeBase::ATheShift_MainMenuGameModeBase()
{
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bCanEverTick = true;
}

void ATheShift_MainMenuGameModeBase::BeginPlay()
{
	GameInstance = Cast<UTheShift_GameInstanceBase>(GetGameInstance());
	GameInstance->RegisterPacketProcessingFunc([this](uint8* Buffer) {this->ProcessPacket(Buffer); });

	ChangeMenuWidget(StartingWidgetClass);
}

void ATheShift_MainMenuGameModeBase::Tick(float DeltaSeconds)
{
	// Data수신, Packet조립 가능 여부 확인
	GameInstance->NetworkTick();
	//if (GameInstance->NetworkTick())
	//{
	//	// Packet 처리
	//	ProcessPacket(GameInstance->GetPacketData());
	//}
}

void ATheShift_MainMenuGameModeBase::ProcessPacket(uint8* Buffer)
{
	TheShift::Serializables::Type PacketType = *reinterpret_cast<TheShift::Serializables::Type*>(Buffer);
	uint8* PacketBuffer = Buffer + sizeof(PacketType);
	InputStream.SetTargetBuffer(reinterpret_cast<char*>(PacketBuffer));

	LOG_INFO("%d", PacketType);
	
	switch(PacketType)
	{
	case TheShift::Serializables::Type::Connected:
	{
		TheShift::Serializables::Connected Data;
		Data.Read(InputStream);
		ProcessConnected(Data);
		break;
	}

	case TheShift::Serializables::Type::LobbyConnected:
	{
		TheShift::Serializables::LobbyConnected Data;
		Data.Read(InputStream);
		ProcessLobbyConnected(Data);
		break;
	}

	case TheShift::Serializables::Type::LobbyInfo:
	{
		TheShift::Serializables::LobbyInfo Data;
		Data.Read(InputStream);
		ProcessLobbyInfo(Data);
		break;
	}

	case TheShift::Serializables::Type::ReadyState:
	{
		TheShift::Serializables::ReadyState Data;
		Data.Read(InputStream);
		ProcessReadyState(Data);
		break;
	}

	case TheShift::Serializables::Type::SessionConnected:
	{
		TheShift::Serializables::SessionConnected Data;
		Data.Read(InputStream);
		ProcessSessionConnected(Data);
		break;
	}
		
	case TheShift::Serializables::Type::MapSelect:
	{
		TheShift::Serializables::MapSelect Data;
		Data.Read(InputStream);
		ProcessMapSelect(Data);
		break;
	}
		
	case TheShift::Serializables::Type::SessionStarted:
	{
		GameInstance->GetStagedSessionData().Read(InputStream);
		GameInstance->SetSessionStartedDataStaged(true);
		LOG_INFO("Actors: %d", GameInstance->GetStagedSessionData().Actors.Array.size());
		ProcessSessionStarted(GameInstance->GetStagedSessionData());
		break;
	}

	case TheShift::Serializables::Type::ToNewPlayerOldPlayerWeapons:
	{
		GameInstance->GetStagedOldPlayerWeaponsData().Read(InputStream);
		GameInstance->SetOldPlayerWeaponsDataStaged(true);
		LOG_INFO("Weapons Data received. Weapons num: %d", GameInstance->GetStagedOldPlayerWeaponsData().SwapWeapons.Array.size());
		break;
	}

	default:
		LOG_WARNING("Can't read packet data.");
		return;
	}
}

void ATheShift_MainMenuGameModeBase::ProcessConnected(TheShift::Serializables::Connected& Data)
{
	LOG_INFO("Connected");
	GameInstance->SetMyClientId(Data.MyId);
}

void ATheShift_MainMenuGameModeBase::ProcessLobbyConnected(TheShift::Serializables::LobbyConnected& Data)
{
	LOG_INFO("LobbyConnected");
}

// LobbyInfo를 처리한다. 현재 widget이 lobby가 아니라면 아무런 처리도 하지 않는다.
void ATheShift_MainMenuGameModeBase::ProcessLobbyInfo(TheShift::Serializables::LobbyInfo& Data)
{
	LOG_INFO("LobbyInfo received. Sessions: %d Players: %d", Data.Sessions.Array.size(), Data.Players.Array.size());

	bool DataNotEmpty = false;

	SessionsInLobby.Empty();
	PlayersInLobby.Empty();
	for(auto& Element : Data.Sessions.Array)
	{
		// Session list를 갱신한다.
		USessionInfo* NewSessionInfo = NewObject<USessionInfo>(this, USessionInfo::StaticClass());
		NewSessionInfo->SessionIndex = Element.SessionIndex;
		NewSessionInfo->Map = static_cast<UMapType>(Element.Map);
		NewSessionInfo->IsInGame = Element.IsInGame;
		NewSessionInfo->SessionName = Element.SessionName.c_str();
		for(auto& Id : Element.ClientIds)
		{
			NewSessionInfo->ClientIds.Add(Id);
		}

		SessionsInLobby.Emplace(NewSessionInfo);
		DataNotEmpty = true;
	}
	
	for(auto& Element : Data.Players.Array)
	{
		// Player list를 갱신한다.
		UPlayerInfo* NewPlayerInfo = NewObject<UPlayerInfo>(this, UPlayerInfo::StaticClass());
		NewPlayerInfo->Id = Element.Id;
		NewPlayerInfo->Name = Element.Name.c_str();

		PlayersInLobby.Emplace(NewPlayerInfo);
		DataNotEmpty = true;
	}

	if(DataNotEmpty)
		LobbyCanUpdate = true;
}

void ATheShift_MainMenuGameModeBase::ProcessReadyState(TheShift::Serializables::ReadyState& Data)
{
	LOG_INFO("ReadyState");
}

void ATheShift_MainMenuGameModeBase::ProcessSessionConnected(TheShift::Serializables::SessionConnected& Data)
{
	LOG_INFO("SessionConnected");
}

void ATheShift_MainMenuGameModeBase::ProcessMapSelect(TheShift::Serializables::MapSelect& Data)
{
	LOG_INFO("MapSelect");
}

void ATheShift_MainMenuGameModeBase::ProcessSessionStarted(TheShift::Serializables::SessionStarted& Data)
{
	LOG_INFO("SessionStarted");
	GameInstance->SetInGame(true);
	UGameplayStatics::OpenLevel(GetWorld(), GameInstance->GetLevelName(Data.Map));
}

void ATheShift_MainMenuGameModeBase::ProcessChat(TheShift::Serializables::Chat& Data)
{
	LOG_INFO("Chat");
}

// Server에게 lobby info를 요청한다. 
void ATheShift_MainMenuGameModeBase::LobbyInfoRequest()
{
	TheShift::Serializables::LobbyInfoReq Data;
	if(GameInstance != nullptr)
	{
		Data.MyId = GameInstance->GetMyClientId();
		GameInstance->Send(Data);
	}
}

// Lobby에 접속 요청을 한다.
void ATheShift_MainMenuGameModeBase::LobbyJoinRequest()
{
	TheShift::Serializables::LobbyJoinReq Data;
	Data.Name = "TestClient";
	Data.MyId = GameInstance->GetMyClientId();
	SendPacket(Data);
}

// Server에게 선택한 session에 대해 접속을 요청한다.
void ATheShift_MainMenuGameModeBase::SessionJoinRequest(int SessionId)
{
	TheShift::Serializables::SessionJoinReq Data;
	Data.SessionIndex = SessionId;
	SendPacket(Data);
}

void ATheShift_MainMenuGameModeBase::ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass)
{
	if(CurrentWidget != nullptr)
	{
		CurrentWidget->RemoveFromViewport();
		CurrentWidget->RemoveFromParent();
		CurrentWidget = nullptr;
	}
	if(NewWidgetClass != nullptr)
	{
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), NewWidgetClass);
		if(CurrentWidget != nullptr)
		{
			// 새로 등록한 widget을 화면에 띄운다.
			CurrentWidget->AddToViewport();
		}
	}
}
