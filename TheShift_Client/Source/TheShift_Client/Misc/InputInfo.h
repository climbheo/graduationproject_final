﻿#pragma once
#include <chrono>

#include "Protocol/Serializables.h"

namespace TheShift
{
using TimePoint = std::chrono::time_point<std::chrono::high_resolution_clock>;
struct InputInfo
{
	uint32_t InputNum;
	TimePoint Time;
	Serializables::Input InputData;
};
}
