﻿#pragma once

#include "EngineMinimal.h"
#include "Protocol/Types.h"

UENUM()
enum class EInteractableActionType : uint8
{
	ActivateActors = static_cast<uint8>(TheShift::InteractableActionType::ActivateActors),
	DeactivateActors = static_cast<uint8>(TheShift::InteractableActionType::DeactivateActors),
	OpenDoorRotateMinus = static_cast<uint8>(TheShift::InteractableActionType::OpenDoorRotateMinus),
	OpenDoorRotatePlus = static_cast<uint8>(TheShift::InteractableActionType::OpenDoorRotatePlus),
	NextLevel = static_cast<uint8>(TheShift::InteractableActionType::NextLevel),
	OpenChest = static_cast<uint8>(TheShift::InteractableActionType::OpenChest),

	None,
};

UENUM()
enum class ETriggerActionType : uint8
{
	None = static_cast<uint8>(TheShift::TriggerActionType::None),
	Stage01_PlayBossCutscene = static_cast<uint8>(TheShift::TriggerActionType::Stage01_PlayBossCutscene),
	Stage02_PlayBossCutscene = static_cast<uint8>(TheShift::TriggerActionType::Stage02_PlayBossCutscene),
	Stage03_PlayBossCutscene = static_cast<uint8>(TheShift::TriggerActionType::Stage03_PlayBossCutscene)
};
