#pragma once

#include "EngineMinimal.h"

UENUM(BlueprintType)
enum class UMapType : uint8
{
	MainMenu,
	Stage01,
	Stage02,
	Stage03,
	Stage04,
	Landscape,
};
