﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <queue>
#include <mutex>
#include "Protocol/MessageQueue.h"
#include "Protocol/Serializables.h"
#include "Protocol/MessageBuilder.h"
#include "Protocol/Protocol.h"
#include "Protocol/Types.h"
#include "Protocol/Json_Formatter/JsonReader.h"
#include "Protocol/Json_Formatter/JsonWriter.h"
#include "Game/System/GameState.h"

#include <chrono>

#include "Networking.h"
#include "Sockets.h"

#include "GameFramework/Actor.h"
#include "EngineMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Misc/UMapTypes.h"

#include "TheShift_ClientGameModeBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnItemAcquire);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDirectiveDisplay);

using namespace TheShift;


class UTSNetworkedComponent;
class ATSPlayer;
class ATSKnightMonster;
class UTSItemComponent;
class UTSDirectiveContent;

/**
 *
 */
UCLASS()
class THESHIFT_CLIENT_API ATheShift_ClientGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATheShift_ClientGameModeBase();

	UFUNCTION(BlueprintCallable, Category = "Server")
	void PostOpeningCutSceneInitialize();

	virtual void BeginPlay() override;
	virtual void StartPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	virtual void Logout(AController* Exiting) override;

	UPROPERTY(BlueprintAssignable, VisibleAnywhere, BlueprintCallable, Category = "Event")
	FOnItemAcquire ItemAcquireEvent;

	UPROPERTY(BlueprintAssignable, VisibleAnywhere, BlueprintCallable, Category = "Event")
	FOnDirectiveDisplay DirectiveDisplayEvent;

	UPROPERTY(BlueprintReadOnly)
	UTSItemComponent* ItemToAddOnInventoryWidget;

	class ATSPlayer* GetMyPlayer();

	TArray<AActor*> GetUEActorsByType(TheShift::ActorType Type);

	//MessageQueue<Message>* GetReceivedDataQueue();
	void PushInputQueue(Serializables::Input Input);

	//Slice
	UFUNCTION(BlueprintCallable, Category = "Slice")
	void SetupMeshIndexes();

	UFUNCTION(BlueprintCallable, Category = "Widget")
	void DisplayDirective(FString DirectiveMessage);

	UPROPERTY(BlueprintReadWrite)
	UTSDirectiveContent* InstantDirective;

	UPROPERTY(BlueprintReadWrite)
	UUserWidget* CurrentDirectiveWidget;

	UPROPERTY(BlueprintReadWrite)
	UTSDirectiveContent* CurrentDirectiveContent;

	UPROPERTY(BlueprintReadWrite)
	bool CurrentlyDirectiveOnScreen = false;

	void InitMapType();
	MapType GetCurrentMapType();
	UFUNCTION(BlueprintCallable, Category = Map)
	UMapType GetCurrentUMapType();
	MapType GetNextMapType();
	UMapType GetNextUMapType();
	void RegisterNetworkComp(UTSNetworkedComponent* NewComp);

	ClientId MyId;
	UID MyActorId;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MyProperty)
	FString MyName;
	SessionId CurrentSessionId;
	MapType CurrentMapType = MapType::Unhandled;
	UMapType CurrentUMapType;

	void SetDrawDebug(bool Draw) { IsDrawDebug = Draw; }

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = DEBUG)
	bool IsDrawDebug = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Rendering)
	bool IsWitcherSenseVisible = false;


	//UFUNCTION(BlueprintCallable, Category = BGM)
	//	virtual void PlayBGMOnBP(bool IsBoss, UMapType MapType);

	// SerializableData의 자식 클래스가 와야한다. 
	//template <typename T>
	//typename std::enable_if<std::is_base_of<TheShift::Serializables::SerializableData, T>::value>::type Send(
	//	T& Data)
	//{
	//	size_t Size = 0;
	//	int32 TotalSentBytes = 0;
	//	int32 SentBytes = 0;
	//	MsgBuilder.BuildSerializedBuffer(Data, reinterpret_cast<char*>(SendBuffer), Size);
	//	while(TotalSentBytes < Size)
	//	{
	//		//UE_LOG(LogTemp, Error, TEXT("Data send size: %d"), Size);
	//		Socket->Send(SendBuffer + TotalSentBytes, Size - TotalSentBytes, SentBytes);
	//		TotalSentBytes += SentBytes;
	//	}
	//}

private:
	InputMemoryStream InputStream;

	class UTheShift_GameInstanceBase* GameInstance = nullptr;

	float ServerUpdateTimestep;

	TheShift::GameState State;

	void ProcessPacket(uint8* Buffer);

	std::queue<InputInfo> InputQueue;
	uint32_t InputIndex;

	bool Connected = false;
	bool Initialized = false;
	bool GameStarted = false;
	// Level이동이 예정되어있는지 확인용
	// ProcessLevelChanged에서 true로 설정해주며 true라면 ProcessSessionStarted가 실행되지 않도록 한다.
	bool LevelTravelReserved = false;
	//TODO: UI class? 를 만들어서 Session과 Lobby정보를 담도록 한다.
	ClientId HostId;

	TMap<UID, UTSNetworkedComponent*> NetworkComps;
	TMap<ClientId, std::shared_ptr<Avatar>> Players;
	TMap<AActor*, bool> ActorsToBeDestroyed;
	TMultiMap<FString, UTSNetworkedComponent*> InteractableNetworkComps;

	uint8 SendBuffer[BUFFERSIZE];

	JsonFormatter::JsonReader JsonReader_;
	JsonFormatter::JsonWriter JsonWriter_;
	std::vector<ActorInfo> ActorInfoArray;

	UPROPERTY()
	ATSPlayer* MyPlayerActor;

	//void Clear();
	bool Init();
	void InitInteractables();
	//bool CreateReceiver();
	bool GetConnected() const;

	//bool ReadData(Serializables::SerializableData& Data);

	bool ProcessConnected(Serializables::Connected& Data);
	bool ProcessChat(Serializables::Chat& Data);
	bool ProcessLobbyConnected(Serializables::LobbyConnected& Data);
	bool ProcessLobbyInfo(Serializables::LobbyInfo& Data);
	bool ProcessSessionConnected(Serializables::SessionConnected& Data);
	bool ProcessMapSelected(Serializables::MapSelect& Data);
	bool ProcessLevelChanged(Serializables::LevelChanged& Data);
	bool ProcessSessionStarted(Serializables::SessionStarted& Data);
	bool ProcessGameStarted(Serializables::GameStarted& Data);
	bool ProcessNewPlayer(Serializables::NewPlayer& Data);
	bool ProcessPlayerDisconnect(Serializables::PlayerDisconnect& Data);
	bool ProcessReadyState(Serializables::ReadyState& Data);
	bool ProcessActorCreated(Serializables::ActorCreated& Data);
	bool ProcessSessionStateUpdate(Serializables::SessionStateUpdate& Data);
	bool ProcessMyCharacterSyncState(Serializables::MyCharacterSyncState& Data);
	bool ProcessRotateForTime(Serializables::RotateForTime& Data);
	bool ProcessInteractableAct(Serializables::InteractableAct& Data);
	bool ProcessToNewPlayerOldPlayerWeapons(Serializables::ToNewPlayerOldPlayerWeapons& Data);
	bool ProcessCollisionDebug(Serializables::CollisionDebug& Data);
	bool ProcessAttackRangeDebug(Serializables::AttackRangeDebug& Data);
	bool ProcessTeleportGage(Serializables::TeleportGageChange& Data);
	bool ProcessMaxTeleportGageChange(Serializables::MaxTeleportGageChange& Data);
	bool ProcessStaminaChange(Serializables::StaminaChange& Data);
	bool ProcessMaxStaminaChange(Serializables::MaxStaminaChange& Data);
	bool ProcessItemEquip(Serializables::ItemEquip& Data);
	bool ProcessItemUnequip(Serializables::ItemUnequip& Data);
	bool ProcessItemAcquire(Serializables::ItemAcquire& Data);
	bool ProcessItemUnacquire(Serializables::ItemUnacquire& Data);
	bool ProcessItemRemoveFromChest(Serializables::ItemRemoveFromChest& Data);
	bool ProcessStatChange(Serializables::StatChange& Data);
	bool ProcessEvent(Serializables::Event& Data);
	bool ProcessTriggerNotify(Serializables::TriggerNotify& Data);

	// Process Acts
	bool ProcessAct(Serializables::Act* Data);
	bool ProcessHit(Serializables::Hit& Data);
	bool ProcessDestructibleHit(Serializables::DestructibleHit& Data);
	bool ProcessRecognize(Serializables::ActorRecognizedAPlayer& Data);

	AActor* SpawnActor(Serializables::ActorInfo& Actor);

	// Level변경 효과 재생
	void PlayPreLevelTransition();
	void PlayPostLevelTransition();

public:
	void JoinLobby();
	void JoinSession(SessionId Id);
	void Ready(bool IsReady);
	void MapSelect(MapType Map);
	void ExitSession();
	std::shared_ptr<TheShift::Actor> GetCorrespondingActor(UID Id);
	void DestroyCorrespondingActor(UID Id);
	AActor* GetTSActor(UID Id);
	float GetServerUpdateTimestep() const;

	void StageActors(Serializables::SerializableVector<Serializables::ActorInfo>& SerializedActors);
	void LoadLevel(MapType Type);

	void MarkActorAsGoodToBeDestroyed(AActor* Actor);

	//HaEun
	TArray<uint32>* GetSlicingMeshIndexes(FString MeshName);
	UMaterialInterface* GetSlicingMaterial(FString MaterialName);
	TheShift::GameState* GetGameState() { return &State; }

	TMap<FString, class UParticleSystem*> EffectsContainer;
	TMap<FString, class USoundWave*> SoundsContainer;
	TMap<FString, class ULevelSequence*> SequenceContainer;
	
private:
	TMap<ActorType, ActorInfo> ActorStats;

	TMap<FString, TArray<uint32>> SlicingMeshIndexes;
	UPROPERTY(EditAnywhere, Category = "Slice", Meta = (AllowPrivateAccess = true))
	TArray<USkeletalMesh*> SlicingMeshes;

	TMap<FString, UMaterialInterface*> SlicingMaterialsContainer;
	UPROPERTY(EditAnywhere, Category = "Slice", Meta = (AllowPrivateAccess = true))
	TArray<UMaterialInterface*> SlicingMaterials;

	UPROPERTY(EditAnywhere, Category = "Effect", Meta = (AllowPrivateAccess = true))
	TArray<class UParticleSystem*> Effects;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound", Meta = (AllowPrivateAccess = true))
		TArray<class USoundWave*> Sounds;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sequence", Meta = (AllowPrivateAccess = true))
		TArray<class ULevelSequence*> Sequences;
public:
	// Widgets
	UPROPERTY()
	UUserWidget* CurrentPopUpWidget;

	UFUNCTION(BlueprintCallable)
	UUserWidget* ChangeIngameWidget(TSubclassOf<UUserWidget> WidgetClass);

	UFUNCTION(BlueprintCallable)
	void SendItemAcquireFromChestReq(UTSItemComponent* Item);
};

