﻿#pragma once

#include "MemoryStream.h"
#include "Protocol.h"
#include "Types.h"


namespace TheShift
{
namespace Serializables
{
// 모든 Message는 이 구조체를 상속하면 됨.
class SerializableData
{
public:
	virtual ~SerializableData() = default;
	// 직렬화 함수.
	virtual void Push(OutputMemoryStream& OutputStream) const = 0;
	virtual void Read(InputMemoryStream& InputStream) = 0;

	// static Type GetType(SerializableData* Data);
	virtual uint16_t ClassId() const = 0;
};

struct SerializableElement
{
	virtual ~SerializableElement() = default;
	virtual void Push(OutputMemoryStream& OutputStream) const = 0;
	virtual void Read(InputMemoryStream& InputStream) = 0;
};

template <typename T>
struct SerializableVector
{
	static_assert(std::is_base_of<SerializableElement, T>::value || std::is_base_of<SerializableData, T>::value,
		"Only SerializableElements and SerializableData can be stored in SerializableVector.");

	// Read this only when you interpret serializable
	uint32_t Size;
	std::vector<T> Array;

	~SerializableVector();
	void Push(OutputMemoryStream& OutputStream) const;
	void Read(InputMemoryStream& InputStream);
};

template <typename T>
void SerializableVector<T>::Read(InputMemoryStream& InputStream)
{
	InputStream.Read(Size);
	Array.reserve(Size);
	for (uint32_t i = 0; i < Size; ++i)
	{
		T Element;
		Element.Read(InputStream);
		Array.push_back(Element);
	}
}

template <typename T>
SerializableVector<T>::~SerializableVector()
{
	Array.clear();
}

template <typename T>
void SerializableVector<T>::Push(OutputMemoryStream& OutputStream) const
{
	OutputStream.Push(static_cast<uint32_t>(Array.size()));
	for (uint32_t i = 0; i < Array.size(); ++i)
	{
		Array[i].Push(OutputStream);
	}
}


void Add(const Vector3f& Vector, OutputMemoryStream& OutputStream);
void Get(Vector3f& Vector, InputMemoryStream& InputStream);
void Add(const std::string& Str, OutputMemoryStream& OutputStream);
void Get(std::string& Str, InputMemoryStream& InputStream);
void Add(const Avatar& avatar, OutputMemoryStream& OutputStream);
void Get(Avatar& avatar, InputMemoryStream& InputStream);


template <typename T>
typename std::enable_if<std::is_arithmetic<T>::value || std::is_enum<T>::value, void>::type Add(
	const std::vector<T>& Data, OutputMemoryStream& OutputStream)
{
	OutputStream.Push(Data);
}

template <typename T>
typename std::enable_if<std::is_arithmetic<T>::value || std::is_enum<T>::value, void>::type Get(
	std::vector<T>& Data, InputMemoryStream& InputStream)
{
	InputStream.Read(Data);
}

template <typename T>
typename std::enable_if<std::is_arithmetic<T>::value || std::is_enum<T>::value, void>::type Add(
	T Data, OutputMemoryStream& OutputStream)
{
	OutputStream.Push(Data);
}

template <typename T>
typename std::enable_if<std::is_arithmetic<T>::value || std::is_enum<T>::value, void>::type Get(
	T& Data, InputMemoryStream& InputStream)
{
	InputStream.Read(Data);
}

template <typename T>
void Add(const SerializableVector<T>& Data, OutputMemoryStream& OutputStream)
{
	Data.Push(OutputStream);
}

template <typename T>
void Get(SerializableVector<T>& Data, InputMemoryStream& InputStream)
{
	Data.Read(InputStream);
}

enum class Type : uint16_t
{
	Unhandled = 0,

	// Client -> Server
	// InitialConnection = SerializableData::ConstHash("InitialConnection"),
	// 주기적으로 lobby의 방 정보를 새로 요청하기 위함.
	// Lobby 접속은 어차피 처음 접속 했을때 해줌.
	// session에서 나왔을때도 바로 보내줄 것임.
	Disconnect = ConstHash("Disconnect"),
	LobbyJoinReq = ConstHash("LobbyJoinReq"),
	LobbyInfoReq = ConstHash("LobbyInfoReq"),
	SessionCreateReq = ConstHash("SessionCreateReq"),
	SessionJoinReq = ConstHash("SessionJoinReq"),
	CreateAndStartSessionReq = ConstHash("CreateAndStartSessionReq"),
	Chat = ConstHash("Chat"),
	Ready = ConstHash("Ready"),
	LoadCompleted = ConstHash("LoadCompleted"),
	Input = ConstHash("Input"),
	DirectiveAct = ConstHash("DirectiveAct"),
	InteractableAct = ConstHash("InteractableAct"),
	MountWeapon = ConstHash("MountWeapon"),
	ItemAcquireFromChestReq = ConstHash("ItemAcquireFromChestReq"),
	TriggerNotify = ConstHash("TriggerNotify"),
	Cheat = ConstHash("Cheat"),

	ChangeUpdateTimestep = ConstHash("ChangeUpdateTimestep"),
	// Session에서 준비한다.
	// 서버에서 같은 세션의 플레이어들이 모두 ready한것을 확인했으면 SessionStarted를 보낸다.
	// 두번오면 ready 해제

	// Server -> Client
	Connected = ConstHash("Connected"),
	LobbyConnected = ConstHash("LobbyConnected"),
	// 새로고침이나 새로운 방 정보를 업데이트 하기 위함
	LobbyInfo = ConstHash("LobbyInfo"),
	ReadyState = ConstHash("ReadyState"),
	SessionConnected = ConstHash("SessionConnected"),
	MapSelect = ConstHash("MapSelect"),
	SessionStarted = ConstHash("SessionStarted"),
	LevelChanged = ConstHash("LevelChanged"),
	GameStarted = ConstHash("GameStarted"),
	NewPlayer = ConstHash("NewPlayer"),
	PlayerDisconnect = ConstHash("PlayerDisconnect"),
	ActorCreated = ConstHash("ActorCreated"),
	SessionStateUpdate = ConstHash("SessionStateUpdate"),
	MyCharacterSyncState = ConstHash("MyCharacterSyncState"),
	SessionExit = ConstHash("SessionExit"),
	RotateForTime = ConstHash("RotateForTime"),
	ToNewPlayerOldPlayerWeapons = ConstHash("ToNewPlayerOldPlayerWeapons"),
	TeleportGageChange = ConstHash("TeleportGage"),
	MaxTeleportGageChange = ConstHash("MaxTeleportGageChange"),
	StaminaChange = ConstHash("StaminaChange"),
	MaxStaminaChange = ConstHash("MaxStaminaChange"),
	ItemAcquire = ConstHash("ItemAcquire"),
	ItemUnacquire = ConstHash("ItemUnacquire"),
	ItemEquip = ConstHash("ItemEquip"),
	ItemUnequip = ConstHash("ItemUnequip"),
	ItemRemoveFromChest = ConstHash("ItemRemoveFromChest"),
	StatChange = ConstHash("StatChange"),
	Event = ConstHash("Event"),


	// Server <-> Client
	SwapWeapon = ConstHash("SwapWeapon"),
	SwapArmor = ConstHash("SwapArmor"),

	UpdateTimestepChanged = ConstHash("UpdateTimestepChanged"),

	// Act class type
	Act = ConstHash("Act"),
	ActWithVector = ConstHash("ActWithVector"),
	ActWithTarget = ConstHash("ActWithTarget"),
	ActWithTargets = ConstHash("ActWithTargets"),
	ActWithValue = ConstHash("ActWithValue"),
	Hit = ConstHash("Hit"),
	DestructibleHit = ConstHash("DestructibleHit"),

	// Game event
	ActorRecognizedAPlayer = ConstHash("RecognizedAPlayer"),

	// Debug
	CollisionDebug = ConstHash("CollisionDebug"),
	AttackRangeDebug = ConstHash("AttackRangeDebug"),
	ToSword = ConstHash("ToSword"),
	LevelChangeReqDebug = ConstHash("LevelChangeReqDebug"),
};

//////////////////////////SerializableElments//////////////////////////
struct PlayerInfo final : SerializableElement
{
	ClientId Id;
	std::string Name;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
};

struct SessionInfo final : SerializableElement
{
	SessionId SessionIndex;
	std::string SessionName;
	std::vector<ClientId> ClientIds;
	bool IsInGame;
	MapType Map;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
};

struct AvatarInfo final : SerializableElement
{
	Avatar Player;
	// Class 정보?
	// Player의 캐릭터 정보

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
};

/// <summary>
/// 맵에 배치된 actor들의 정보,
/// /// </summary>
struct ActorInfo final : SerializableElement
{
	UID ActorId;
	ActorType Type;
	Vector3f Position;
	uint32_t Hp;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
};

void Add(const ActorInfo& Actor, OutputMemoryStream& OutputStream);
void Get(ActorInfo& Actor, InputMemoryStream& InputStream);

/// <summary>
/// Actor의 위치정보, 회전, 속도를 저장한다.
/// </summary>
struct ActorPosition final : SerializableElement
{
	UID Self;
	Vector3f Position;
	Vector3f Rotation;
	Vector3f Speed;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
};

struct BoundingVolumeDebug final : SerializableElement
{
	BoundingVolumeType BoundingVolumeType_;
	Vector3f Position;
	Vector3f Rotation;
	Vector3f ScaledExtent;
	float HalfHeight;
	float Radius;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
};

///////////////////////////////////////////////////////////////////////

////////////////////////////////Packets////////////////////////////////
// Client가 server에서 접속을 끊을 때 보내는 packet
struct Disconnect final : SerializableData
{
	UID ActorId;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// Lobby에 접속을 요청할 때 보내는 packet
struct LobbyJoinReq final : SerializableData
{
	ClientId MyId;
	std::string Name;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// Lobby에 접속이 되었을 경우 보내는 packet
struct LobbyConnected final : SerializableData
{
	bool Connected;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// Lobby정보를 요청하는 packet
struct LobbyInfoReq final : SerializableData
{
	ClientId MyId;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// Lobby에 존재하는 session들의 정보와 player들의 정보를 보내는 packet
struct LobbyInfo final : SerializableData
{
	SerializableVector<PlayerInfo> Players;
	SerializableVector<SessionInfo> Sessions;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// Server에 연결이 되었을 때 보내는 packet
struct Connected final : SerializableData
{
	ClientId MyId;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// Session 생성 요청을 할때 보내는 packet
struct SessionCreateReq final : SerializableData
{
	ClientId MyId;
	MapType Map;
	std::string SessionName;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// Session에 접속 요청을 할때 보내는 packet
struct SessionJoinReq final : SerializableData
{
	SessionId SessionIndex;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// 즉시 Session을 생성하고 시작할때 보내는 packet
struct CreateAndStartSessionReq final : SerializableData
{
	MapType Type;
	std::string SessionName;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// Lobby에서 session에 연결이 되었을 때 보내는 packet
struct SessionConnected final : SerializableData
{
	SessionId SessionIndex;
	std::string SessionName;
	SerializableVector<PlayerInfo> PlayersInfo;
	ClientId HostId;
	float ServerUpdateTimestep;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// 대기중인 session에서 map을 정할때 보내는 packet
struct MapSelect final : SerializableData
{
	SessionId SessionIndex;
	MapType Map;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// 게임중인 session의 level(map)이 변경될 경우 해당 session내 모든 player들에게 전송한다.
struct LevelChanged final : SerializableData
{
	MapType Map;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};


// Chatting packet
struct Chat final : SerializableData
{
	SessionId To;
	ClientId From;
	std::string Data;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// 새로운 actor가 생성되었을 때 client들에게 해당 actor를 spawn해야함을 알리는 packet
struct ActorCreated final : SerializableData
{
	UID ActorId;
	ActorType Type;
	Vector3f Position;
	// Eigen::Vector3f Rotation;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// 대기중인 Session에서 시작되었을 때 보내는 packet
// GameStarted는 모든 player가 load가 완료되고 loading창에서 game화면으로 넘어가게 하는 packet
struct SessionStarted final : SerializableData
{
	MapType Map;
	SerializableVector<ActorInfo> Actors;
	SerializableVector<AvatarInfo> Players;
	float ServerUpdateTimestep;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// 새로운 player가 진행중인 session에 접속했을 경우 해당 session의 다른 player들에게 보내는 packet
struct NewPlayer final : SerializableData
{
	Avatar Player;
	ActorInfo NewActor;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// Session에 접속중이던 player가 접속을 끊었을 때 접속해있는 다른 player들에게 보내는 packet
struct PlayerDisconnect final : SerializableData
{
	ClientId DisconnectingClientId;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// 아직 시작하지 않은 session에서 시작 준비를 했을 때 server로 보내는 packet
struct Ready final : SerializableData
{
	bool IsReady;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// 아직 시작하지 않은 session에서 player가 준비를 했을때 보내는 packet
struct ReadyState final : SerializableData
{
	ClientId Id;
	bool IsReady;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// Game시작 전 player가 game load가 모두 완료되었을때 보내는 packet
struct LoadCompleted final : SerializableData
{
	bool Completed;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// 모든 player가 load를 끝냈을 때 loading 화면에서 game화면으로 넘어가도 됨을 알리는 packet
struct GameStarted final : SerializableData
{
	bool Started;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// Player가 input을 입력했을 경우 server로 보내는 packet
// Player와 관련되지 않은 정보는 보내지 않는다. (Monster관련 input X)
struct Input final : SerializableData
{
	InputType InputValue;
	uint32_t InputId;
	Vector3f CamRotation;
	Vector3f ControlRotation;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// Directive관련 act를 전송한다.
// 아이템 획득, 문 열기
struct DirectiveAct final : SerializableData
{
	InteractableActionType Type;
	int Value;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

struct InteractableAct final : SerializableData
{
	std::string Tag;
	InteractableActionType Type;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// 무기 획득
struct MountWeapon final : SerializableData
{
	uint8_t Weapon;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

/// <summary>
/// 매 서버 업데이트 시 보내게 되는 정보
/// 이동, 이벤트 등 이전과 달라진 부분들을 보낸다.
/// 모든 데이터를 보내지 않음.
/// </summary>
struct SessionStateUpdate final : SerializableData
{
	// Actor의 위치정보에 변화가 있을때만 저장한다.
	SerializableVector<ActorPosition> ActorPositions;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// 내 Character의 위치 동기화 관련 처리를 위한 data
// 클라이언트 측에서 player가 조작중인 character는 SessionStateUpdate대신 MyCharacterSyncState를 이용해 위치를 동기화시킨다.
struct MyCharacterSyncState final : SerializableData
{
	UID MyActorId;
	uint32_t LastProcessedInputId;
	ActorPosition MyCharacterPosition;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// Player가 session에서 나갔을 경우에 server로 보내는 packet
struct SessionExit final : SerializableData
{
	SessionId CurrentSessionId;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// 액터를 주어진 시간동안 천천히 회전시킬때 보내는 packet
struct RotateForTime final : SerializableData
{
	UID Id;
	int DurationMilliseconds;
	Vector3f RotateAmount;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// 아무런 목표 없이 하는 act를 보내는 packet
struct Act : SerializableData
{
	UID Self;
	ActType Type;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// 특정 목표 vector를 향해 하는 act를 보내는 packet
struct ActWithVector final : Act
{
	Vector3f Vector;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// 특정 actor를 목표로 하는 act를 보내는 packet
struct ActWithTarget final : Act
{
	UID TargetId;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// 여러 actor들을 목표로 하는 act를 보내는 packet (활 범위공격)
struct ActWithTargets final : Act
{
	std::vector<UID> IdsOfTargets;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// 4Byte의 값을 정보로 가지고 있는 act를 보내는 packet
struct ActWithValue final : Act
{
	int Value;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// Actor가 피격되면 보내는 packet
struct Hit final : SerializableData
{
	UID Self;
	float Hp;
	float Damage;
	bool IsDead;
	UID AttackerId;
	ActType AttackType;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

struct DestructibleHit final : SerializableData
{
	std::string Tag;
	float Hp = Hp;
	float Damage;
	bool IsDead;
	UID AttackerId;
	ActType AttackType;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// Monster가 player를 인식했을 때 보내는 packet
struct ActorRecognizedAPlayer final : SerializableData
{
	UID Self;
	UID TargetId;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

//TODO: 추후에 사용해야 될 packet
// struct Login final : SerializableData
//{
//    // Login정보. 이거로 데이터베이스에서 Player정보 가져오기?
//    // 일단 사용하지 않음.
//    std::string Id;
//    std::string Password;
//
//    void Push(OutputMemoryStream& OutputStream) const override;
//    void Read(InputMemoryStream& InputStream) override;
//    uint16_t ClassId() const override;
//};

//DEBUG: BoundingVolume을 client측에서 그려주기 위해 보내는 packet
// 현재 그리는 속도 너무 느림, 사용하지 못함.
struct CollisionDebug final : SerializableData
{
	SerializableVector<BoundingVolumeDebug> Info;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

struct AttackRangeDebug final : SerializableData
{
	SerializableVector<BoundingVolumeDebug> Info;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

//DEBUG: Client측에서 server의 update tickrate 조정을 원할 때 보내는 packet
struct ChangeUpdateTimestep final : SerializableData
{
	float NewTimestep;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// Server의 tickrate가 변경되었음을 client로 알릴 때 보내는 packet
struct UpdateTimestepChanged final : SerializableData
{
	float NewTimestep;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

//DEPRECATED: Test용으로 제작한 임시 packet임 삭제되어야 함.
struct ToSword final : SerializableData
{
	UID Id;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};


// 무기를 바꿀 경우 주고받는 packet
struct SwapWeapon final : SerializableData
{
	UID Id;
	// Client측에선 UENUM을 사용하므로 WeaponType으로 저장하지 않고 uint8_t로 저장한다.
	// UENUM과 WeaponType의 원소 순서는 동일함이 보장되어야 함.
	uint8_t SwapType;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// 방어구를 바꿀 경우 주고받는 packet
struct SwapArmor final : SerializableData
{
	UID Id;
	uint8_t SwapType;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

struct ToNewPlayerOldPlayerWeapons final : SerializableData
{
	SerializableVector<ActWithValue> SwapWeapons;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

struct StatChange final : SerializableData
{
	UID Id;
	float Hp;
	int TeleportGage;
	int Stamina;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

struct Event final : SerializableData
{
	EventType Type;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

struct TeleportGageChange final : SerializableData
{
	UID Id;
	int Gage;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

struct MaxTeleportGageChange final : SerializableData
{
	UID Id;
	int NewMaxValue;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

struct StaminaChange final : SerializableData
{
	UID Id;
	int NewStaminaValue;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

struct MaxStaminaChange final : SerializableData
{
	UID Id;
	int NewMaxStaminaValue;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

struct ItemAcquire final : SerializableData
{
	int Index;
	int ItemCategory;
	int ItemType;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

struct ItemUnacquire final : SerializableData
{
	UID OwnerId;
	int Index;
	
	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

struct ItemEquip final : SerializableData
{
	UID OwnerActorId;
	int Index;
	int ItemCategory;
	int ItemType;
	
	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

struct ItemUnequip final : SerializableData
{
	UID OwnerActorId;
	int Index;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// 상자에서 아이템을 획득할때 클라에서 서버로 보내는 packet
struct ItemAcquireFromChestReq final : SerializableData
{
	std::string ChestTag;
	int Index;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

struct TriggerNotify final : SerializableData
{
	std::string Tag;
	TriggerActionType Act;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

struct Cheat final : SerializableData
{
	UID Id;
	CheatType Type;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

// 어떤 플레이어가 상자에서 아이템을 획득해서 상자에서 해당 아이템을 제거하고자 할때 서버에서 클라로 보내는 packet
struct ItemRemoveFromChest final : SerializableData
{
	std::string ChestTag;
	int Index;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

struct LevelChangeReqDebug final : SerializableData
{
	MapType Type;

	void Push(OutputMemoryStream& OutputStream) const override;
	void Read(InputMemoryStream& InputStream) override;
	uint16_t ClassId() const override;
};

///////////////////////////////////////////////////////////////////////
} // namespace Serializables
} // namespace TheShift
