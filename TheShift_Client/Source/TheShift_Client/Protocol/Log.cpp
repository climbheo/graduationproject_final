#include "Log.h"

namespace TheShift
{
std::string ConvertLogString(const char* String)

{
	std::string LogString(String);
	std::string Result;
	std::string::iterator StartIter = LogString.begin();
	int Count = 0;
	size_t Pos = 0;
	for(int i = 0; i < LogString.size(); ++i)
	{
		char CurrentChar = LogString[i];
		if(CurrentChar == '%')
		{
			std::string ConvertedString = "{" + std::to_string(Count++) + "}";
			LogString.replace(Pos, 2, ConvertedString);
		}
		++Pos;
	}
	return LogString;
}
}
