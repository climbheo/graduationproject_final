#pragma once

#include "Protocol.h"

#ifdef TSSERVER
// Server log macro functions
#ifndef SPDLOG_INCLUDED
#define SPDLOG_INCLUDED
#include "Vendor/include/spdlog/spdlog.h"
#define LOG_INFO(STRING, ...) spdlog::info(TheShift::ConvertLogString(STRING), __VA_ARGS__)
#define LOG_WARNING(STRING, ...) spdlog::warn(TheShift::ConvertLogString(STRING), __VA_ARGS__)
#define LOG_ERROR(STRING, ...) spdlog::error(TheShift::ConvertLogString(STRING), __VA_ARGS__)
#define LOG_DEBUG(STRING, ...) spdlog::debug(TheShift::ConvertLogString(STRING), __VA_ARGS__)
#endif

#elif defined(TSCLIENT)
// Client log macro functions
#ifndef ENGINEMINIMAL_INCLUDED
#define ENGINEMINIMAL_INCLUDED
#include "EngineMinimal.h"
#define LOG_INFO(STRING, ...) UE_LOG(LogTemp, Log, TEXT(STRING), __VA_ARGS__)
#define LOG_WARNING(STRING, ...) UE_LOG(LogTemp, Warning, TEXT(STRING), __VA_ARGS__)
#define LOG_ERROR(STRING, ...) UE_LOG(LogTemp, Error, TEXT(STRING), __VA_ARGS__)
#define LOG_DEBUG(STRING, ...) UE_LOG(LogTemp, Log, TEXT(STRING), __VA_ARGS__)
#endif

#endif

namespace TheShift
{
std::string ConvertLogString(const char* String);
}

