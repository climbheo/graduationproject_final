﻿#pragma once

#include <numeric>
#include <atomic>
#include <cassert>
#include <mutex>
#include <vector>
#include <chrono>

template <std::size_t N>
struct is_power_of_two
{
	static const bool value = (N & (N - 1)) == 0;
};

template <std::size_t N>
constexpr auto IsPowerOfTwo = is_power_of_two<N>::value;

// 사이즈는 2의n승만 가능. 마스크할때 필요하기 때문
// PoolSize가 2의n승인지 체크 해주자. (2 4 8 16 32 64...)
// 실제 버퍼의 크기는 PoolSize - 1
// 값 하나는 reserve 해둠.
template <class T, size_t PoolSize = 1024>
class MemoryPool
{
	static_assert(IsPowerOfTwo<PoolSize>&& PoolSize > 2, "MemoryPool size needs to be power of 2.(At least 4)");
private:
	std::vector<T> mBuffer;
	std::vector<uint16_t> mFreeList; // 0부터 쭉 채워놔야 함.
	std::atomic_uint16_t mTail, mHead; // 초기값 tail = 0, head = 1 ? 0xffff로 마스크할건데 0xffff를 reserve 해놓을거니 index하나가 비어있게됨.
	std::atomic_int32_t mCount; // 초기값 PoolSize - 1
	MemoryPool<T, PoolSize>* mNextPool;
	size_t mPoolNumber;
	std::mutex mExtendLock;
	static const uint16_t mMask = PoolSize - 1; // 2의 n승 - 1

	int mMaxDelay = 64;
	int mMinDelay = 1;
	int mLimit = mMinDelay;

public:
	MemoryPool(size_t poolNumber = 0) :
		mTail(0),
		mHead(1),
		mCount(PoolSize - 1),
		mNextPool(nullptr),
		mPoolNumber(poolNumber)
	{
		mBuffer.resize(PoolSize - 1);
		mFreeList.resize(PoolSize);
		// mFreeList는 0부터 쭉 채워놓자.
		std::iota(&mFreeList[1], &mFreeList[PoolSize - 1] + 1, 0);
		mFreeList[0] = 0xffff;

	}

	uint16_t getHead() const
	{
		return mHead;
	}
	uint16_t getTail() const
	{
		return mTail;
	}

	// 해당 메모리블럭의 인덱스가 필요 없는경우 사용.
	T* alloc()
	{
		while(true)
		{
			uint16_t expected = mHead;
			uint16_t index = mFreeList[expected];
			// 다른 thread에서 선점했는지 check
			if(index != 0xffff)
			{
				// 채워지지 않은 index면 선점을 시도한다.
				if(std::atomic_compare_exchange_strong(reinterpret_cast<std::atomic_uint16_t*>(&mFreeList[expected]), &index, 0xffff))
				{
					// Head를 옮겨준다. 실패하면 다른 Thread가 처리하도록 내버려둔다.
					std::atomic_compare_exchange_strong(&mHead, &expected, (expected + 1) & mMask);

					// 선점 했으므로 head와 상관 없이 mCount를 줄여준다.
					--mCount;

					// 선점에 성공한 메모리 주소를 return해준다.
					return &mBuffer[index];
				}
				// 선점에 실패한다면 잠시 쉰다.
				interruptException();
			}
			else
			{
				// 선점에 실패했다.
				// 가용 메모리가 없다면 새로운 pool을 할당한다.
				if(mCount <= 0)
				{
					if(mNextPool)
						return mNextPool->alloc();
					return allocExtend();
				}

				// 이미 선점된 index면 대신 head를 옮겨주길 시도한다.
				std::atomic_compare_exchange_strong(&mHead, &expected, (expected + 1) & mMask);
			}
		}
	}

	// 해당 메모리블럭의 인덱스가 필요한 경우에 사용.
	size_t alloc(T*& memory)
	{
		while(true)
		{
			uint16_t expected = mHead;
			uint16_t index = mFreeList[expected];
			// 다른 thread에서 선점했는지 check
			if(index != 0xffff)
			{
				// 채워지지 않은 index면 선점을 시도한다.
				if(std::atomic_compare_exchange_strong(reinterpret_cast<std::atomic_uint16_t*>(&mFreeList[expected]), &index, 0xffff))
				{
					// Head를 옮겨준다. 실패하면 다른 Thread가 처리하도록 내버려둔다.
					std::atomic_compare_exchange_strong(reinterpret_cast<std::atomic_uint16_t*>(&mHead), &expected, (expected + 1) & mMask);

					// 선점 했으므로 head와 상관 없이 mCount를 줄여준다.
					--mCount;

					// 선점에 성공한 index를 return해준다.
					memory = &mBuffer[index];
					return index + (PoolSize - 1) * mPoolNumber;
				}

				// 선점에 실패한다면 잠시 쉰다.
				interruptException();
			}
			else
			{
				// 선점에 실패했다.
				// 가용 메모리가 없다면 새로운 pool을 할당한다.
				if(mCount <= 0)
				{
					if(mNextPool)
						return mNextPool->alloc(memory);
					return allocExtend(memory);
				}

				// 이미 선점된 index면 대신 head를 옮겨주길 시도한다.
				std::atomic_compare_exchange_strong(&mHead, &expected, (expected + 1) & mMask);
			}
		}
	}

	void dealloc(T* mem)
	{
		assert(mem != nullptr);

		// 해당 메모리가 담긴 풀을 찾고 거기서 해제해준다.
		if(mem < &mBuffer.front() || mem > &mBuffer.back())
		{
			if(mNextPool)
			{
				mNextPool->dealloc(mem);
			}
			return;
		}
		// 버퍼 시작 위치와의 차로 index를 구할 수 있음.	
		uint16_t memIndex = static_cast<uint16_t>(mem - &mBuffer.front());
		while(true)
		{
			uint16_t expected = mTail;
			uint16_t index = mFreeList[expected];
			// mFreeList[expected] 가 0xffff라면 반환 가능
			if(index == 0xffff)
			{
				// 반환 가능한 index 선점을 시도한다.
				if(std::atomic_compare_exchange_strong(reinterpret_cast<std::atomic_uint16_t*>(&mFreeList[expected]), &index, memIndex))
				{
					// Head 변경과 같은 방식. tail += 1, 실패 시 다른 Thread가 처리하도록 내버려둔다.
					std::atomic_compare_exchange_strong(&mTail, &expected, (expected + 1) & mMask);

					// 가용 메모리 수를 반영시켜준다.
					++mCount;
					return;
				}
				// 선점에 실패한다면 잠시 쉰다.
				interruptException();
			}
			else
			{
				// 다른 Thread에서 처리되지 않은 tail 이동을 처리해준다.
				std::atomic_compare_exchange_strong(&mTail, &expected, (expected + 1) & mMask);
			}
		}
	}

	T* unsafeAt(size_t index)
	{
		// index가 범위를 넘어섰는데 mNextPool이 nullptr이라면 access violation.
		if(index > PoolSize - 2)
		{
			if(mNextPool == nullptr)
			{
				return nullptr;
			}
			return unsafeAtExtend(index);
		}


		return &mBuffer[index];
	}

	void clear()
	{
		if(mNextPool != nullptr)
		{
			mNextPool->clear();
		}
		mTail = 0;
		mHead = 1;
		mCount = PoolSize - 1;

		mNextPool = nullptr;
		mPoolNumber = 0;
		std::iota(&mFreeList[1], &mFreeList[PoolSize - 1], 0);
	}

private:
	T* allocExtend()
	{
		// 추가로 생성된 풀이 있으면 그 풀에서 할당받는다.
		if(mNextPool == nullptr)
		{
			// 없으면 락걸고 새로 만들고 거기서 할당받는다.
			{
				// 공간이 부족할때 새로운 풀이 여러번 생기는걸 방지하기 위해 락을 사용한다.
				mExtendLock.lock();
				if(mNextPool == nullptr)
				{
					mNextPool = new MemoryPool<T, PoolSize>(mPoolNumber + 1);
					//spdlog::warn("New memory pool created. Consider modifying pool size.");
				}
				mExtendLock.unlock();
			}
			if(mNextPool == nullptr)
			{
				return nullptr;
			}
		}
		return mNextPool->alloc();
	}

	size_t allocExtend(T*& memory)
	{
		// 추가로 생성된 풀이 있으면 그 풀에서 할당받는다.
		if(mNextPool == nullptr)
		{
			// 없으면 락걸고 새로 만들고 거기서 할당받는다.
			{
				// 공간이 부족할때 새로운 풀이 여러번 생기는걸 방지하기 위해 락을 사용한다.
				mExtendLock.lock();
				if(mNextPool == nullptr)
				{
					mNextPool = new MemoryPool<T, PoolSize>(mPoolNumber + 1);
					//spdlog::warn("New memory pool created. Consider modifying pool size.");
				}
				mExtendLock.unlock();
			}
			assert(mNextPool != nullptr);
			if(mNextPool == nullptr)
			{
				memory = nullptr;
				return 0;
			}
		}
		return mNextPool->alloc(memory);
	}

	T* unsafeAtExtend(size_t index)
	{
		return mNextPool->unsafeAt(index - (PoolSize - 1));
	}

	uint16_t atomicIncr(uint16_t& index)
	{
		volatile uint16_t returnVal = index;
		volatile uint16_t expected = returnVal;
		volatile uint16_t desired = (returnVal + 1) & mMask;
		while(!std::atomic_compare_exchange_strong(reinterpret_cast<std::atomic_uint16_t*>(&index), const_cast<uint16_t*>(&expected), desired))
		{
			returnVal = index;
			expected = returnVal;
			desired = (returnVal + 1) & mMask;
		}

		return returnVal;
	}

	int32_t atomicIncr(int32_t& count)
	{
		int expected = count;
		int desired = expected + 1;
		while(!std::atomic_compare_exchange_strong(reinterpret_cast<std::atomic_int32_t*>(&count), &expected, desired))
		{
			expected = count;
			desired = expected + 1;
		}
		//int32_t returnVal = count.fetch_add(1);
		return desired;
	}

	int32_t atomicDecr(int32_t& count)
	{
		int expected = count;
		int desired = expected - 1;
		while(!std::atomic_compare_exchange_strong(reinterpret_cast<std::atomic_int32_t*>(&count), &expected, desired))
		{
			expected = count;
			desired = expected - 1;
		}
		//int32_t returnVal = count.fetch_sub(1);
		return desired;
	}

	void interruptException()
	{
		int delay;
		if(mLimit != 0)
			delay = rand() & mLimit;
		else
			mLimit = 1;
		if(mLimit < mMaxDelay)
			mLimit *= 2;

		//std::this_thread::sleep_for(std::chrono::microseconds(delay));
		std::this_thread::sleep_for(std::chrono::nanoseconds(delay));
	}
};