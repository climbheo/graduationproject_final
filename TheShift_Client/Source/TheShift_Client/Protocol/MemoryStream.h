#pragma once

// MemoryStream은 기본적인 역할만 하도록 하고
// NetworkStream이라던지 MemoryStream을 상속받는 식으로
// 구현하는게 맞는거 같다 확장성을 고려하면.
// 지금은 시간상 문제로, 용도 문제로 따로 작업하진 않지만
// 다음번부터는 이 부분에 대해 충분한 고민이 필요할 것 같다.

//#include "Protocol.h"
#include <algorithm>
#include <memory>
#include <string>
#include <vector>

// enum class PacketType
//{
//	Float4,
//};

// constexpr size_t StringHeaderSize = 4;
// constexpr size_t ChatHeaderSize = sizeof(Protocol::Chat::To) + sizeof(Protocol::Chat::From) + StringHeaderSize;

template <class T, size_t ElementSize, size_t DataSize>
struct NetworkVector
{
	size_t Size = ElementSize;
	char Data[DataSize];
};

// consider as incoming packet.
class InputMemoryStream
{
private:
	char* Buffer;
	uint32_t BitHead;
	uint32_t BitCapacity;

public:
	InputMemoryStream();
	InputMemoryStream(uint32_t BitCount);

	char* GetBufferPtr();
	void SetTargetBuffer(char* TargetBuffer);
	uint32_t GetBitLength() const;
	uint32_t GetByteLength() const;

	void Reset();

	// it only matters when you swap the bytes
	// I'm not gonna bother swapping bytes for now.
	template <typename T>
	typename std::enable_if<(std::is_arithmetic<T>::value || std::is_enum<T>::value), void>::type Read(
		T& Data, uint32_t BitCount = sizeof(T) * 8)
	{
		static_assert(std::is_arithmetic<T>::value || std::is_enum<T>::value,
					  "Generic read only supports primitive types.");
		ReadBits(&Data, BitCount);
	}

	template <typename T>
	typename std::enable_if<(std::is_arithmetic<T>::value || std::is_enum<T>::value), void>::type Read(
		std::shared_ptr<T> Data, uint32_t BitCount = sizeof(T) * 8)
	{
		static_assert(std::is_arithmetic<T>::value || std::is_enum<T>::value,
					  "Generic read only supports primitive types.");
		ReadBits(&(*Data.get()), BitCount);
	}

private:
	void ReadBits(uint8_t& Data, uint32_t BitCount);
	void ReadBits(void* Data, uint32_t BitCount);

public:
	//void Read(bool& Data);
	void Read(std::string& Data);

	template <typename T>
	void Read(std::vector<T>& vector)
	{
		uint32_t Size;
		Read(Size);
		vector.resize(Size);
		for(uint32_t i = 0; i < Size; ++i)
		{
			Read(vector[i]);
		}
	}

	// void ReadChat(Protocol::Chat& ChatMessage);
};

// consider as outgoing packet.
class OutputMemoryStream
{
private:
	std::vector<char> Buffer;
	uint32_t BitHead;
	uint32_t BitCapacity;

	void ResizeBuffer(uint32_t inBitCapacity);

public:
	OutputMemoryStream();

	const char* GetBufferPtr() const;
	uint32_t GetBitLength() const;
	uint32_t GetByteLength() const;

	void Reset();

private:
	void WriteBits(uint8_t inData, uint32_t inBitCount);
	void WriteBits(const void* inData, uint32_t inBitCount);

	template <typename T>
	void Write(T inData, uint32_t inBitCount = sizeof(T) * 8)
	{
		// check if the data type is primitive during compile time
		static_assert(std::is_arithmetic<T>::value || std::is_enum<T>::value,
					  "Generic write only supports primitive data types.");
		WriteBits(&inData, inBitCount);
	}

	//template <typename T>
	//typename std::enable_if<std::is_same<T, bool>::value, void>::type Write(bool inData)
	//{
	//    WriteBits(&inData, 1);
	//}

public:
	template <typename T>
	typename std::enable_if<std::is_same<T, std::string>::value, void>::type Push(const T& inData)
	{
		// need to count elements.
		auto elementCount = static_cast<uint32_t>(inData.size());
		Push(elementCount);

		for(const auto& element : inData)
		{
			Push(element);
		}
	}

	//template <typename T>
	//typename std::enable_if<std::is_same<T, bool>::value, void>::type Push(T inData)
	//{
	//    // TODO: 이게 맞는거야??
	//    Write(inData);
	//    //*reinterpret_cast<uint32_t*>(Buffer.data() + 4) = (BitHead - 32 + 7) >> 3;
	//    *reinterpret_cast<uint32_t*>(Buffer.data()) = (BitHead + 7) >> 3;
	//}


	template <typename T>
	typename std::enable_if<(std::is_arithmetic<T>::value || std::is_enum<T>::value) &&
		!std::is_same<T, std::string>::value>::type
		Push(T inData, uint32_t inBitCount = sizeof(T) * 8)
	{
		// check if the data type is primitive during compile time
		Write(inData, inBitCount);

		// update Data size field in stream.
		*reinterpret_cast<uint32_t*>(Buffer.data()) = (BitHead + 7) >> 3;
	}

	template <typename T>
	void Push(const std::vector<T>& vector)
	{
		Push(static_cast<uint32_t>(vector.size()));
		for(const T& element : vector)
		{
			Push(element);
		}
	}

	template <typename T>
	void Push(const std::vector<T*>& vector)
	{
		Push(static_cast<uint32_t>(vector.size()));
		for(T* element : vector)
		{
			Push(*element);
		}
	}

	template <typename T>
	void Push(const std::vector<std::shared_ptr<T>>& vector)
	{
		Push(static_cast<uint32_t>(vector.size()));
		for(std::shared_ptr<T> element : vector)
		{
			Push(*(element.get()));
		}
	}

	// void WriteHeader(Protocol::MessageType messageType);
	// void writeHeader(Protocol::ConnectionType connectionType);
	// void PushChat(const Protocol::Chat& message);
};
