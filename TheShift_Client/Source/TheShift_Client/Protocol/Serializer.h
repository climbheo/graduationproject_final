#pragma once

#include "MemoryStream.h"
#include "Serializables.h"


namespace TheShift
{
class Serializer
{
public:
	const char* GetBuffer() const;

	template <typename T>
	void Serialize(T* SrcData, size_t& size)
	{
		static_assert(std::is_base_of<Serializables::SerializableData, T>::value, "Only can serialize serializable data"
					  );

		SrcData->Push(OutputStream);
		size = OutputStream.GetByteLength();
		OutputStream.Reset();
	}

	template <typename T>
	typename std::enable_if<std::is_arithmetic<T>::value || std::is_enum<T>::value>::type Push(T Data)
	{
		OutputStream.Push(Data);
	}

	template <typename T>
	typename std::enable_if<std::is_base_of<Serializables::SerializableData, T>::value>::type Push(const T& data)
	{
		OutputStream.Push(data);
	}

	void Reset()
	{
		OutputStream.Reset();
	}

private:
	OutputMemoryStream OutputStream;
};

class Deserializer
{
public:
	char* GetBuffer();

	template <typename T>
	typename std::enable_if<std::is_base_of<Serializables::SerializableData, T>::value, void>::type Deserialize(
		T& Dest, char* SrcData)
	{
		// static_assert(std::is_base_of<Serializables::SerializableData, T>::value, "Only can deserialize serializable data.");
		InputStream.SetTargetBuffer(SrcData);
		Dest.Read(InputStream);
		InputStream.Reset();
	}

	template <typename T>
	void Read(T& Dest)
	{
		InputStream.Read(Dest);
	}

	void SetTarget(char* Data)
	{
		InputStream.SetTargetBuffer(Data);
	}

private:
	InputMemoryStream InputStream;
};
} // namespace TheShift
