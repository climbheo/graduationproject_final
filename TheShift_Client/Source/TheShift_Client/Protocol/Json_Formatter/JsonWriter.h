#pragma once

#include "Vendor/include/rapidjson/document.h"
#include "Vendor/include/rapidjson/ostreamwrapper.h"
#include "Vendor/include/rapidjson/writer.h"
#include <fstream>
#include <vector>
#include "Protocol/Types.h"

namespace TheShift
{
namespace JsonFormatter
{
class JsonWriter
{
public:
	JsonWriter();
	JsonWriter(const char* path);

	bool Open(const char* path);
	bool Close();

	void ExportActorInfo(const char* path, std::vector<ActorInfo>& info);
	void ExportMapInfo(const char* path, MapInfo& info);
	void ExportAnimNotifyInfo(const char* path, std::vector<AnimNotifyInfo>& info);

private:
	std::ofstream Ofs;
	rapidjson::OStreamWrapper Osw;
	rapidjson::Writer<rapidjson::OStreamWrapper> Writer;
	rapidjson::Document Doc;

	void WriteActorInfo(std::vector<ActorInfo>& info);
	void WriteMapInfo(MapInfo& info);
	void WriteAnimNotifyInfo(std::vector<AnimNotifyInfo>& info);
};
}
}

