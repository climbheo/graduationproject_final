#pragma once

#include <cstdint>

//TODO: Queue를 내 용도에 맞게 바꿔줘야 함.
#include <queue>
#include <mutex>
#include "MessageBuilder.h"

template <typename MessageT>
class MessageQueue
{
public:
	//void SetIndex(int8_t QueueIndex);
	std::shared_ptr<MessageT> Pop();
	void Push(std::shared_ptr<MessageT> NewMessage);
	bool Empty();


private:
	// 연결된 Game thread와 동일한 index.
	//int8_t Index;

	std::queue<std::shared_ptr<MessageT>> Queue;
	std::mutex Mutex;
};

template <typename MessageT>
bool MessageQueue<MessageT>::Empty()
{
	//	//TODO lock 없애는게...
	Mutex.lock();
	bool ReturnVal = Queue.empty();
	Mutex.unlock();
	return ReturnVal;
}

template <typename MessageT>
void MessageQueue<MessageT>::Push(std::shared_ptr<MessageT> NewMessage)
{
	Mutex.lock();
	Queue.push(NewMessage);
	Mutex.unlock();
}

template <typename MessageT>
std::shared_ptr<MessageT> MessageQueue<MessageT>::Pop()
{
	Mutex.lock();
	std::shared_ptr<MessageT> Msg = Queue.front();
	Queue.pop();
	Mutex.unlock();

	return Msg;
}
