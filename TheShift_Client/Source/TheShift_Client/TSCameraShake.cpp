// Fill out your copyright notice in the Description page of Project Settings.


#include "TSCameraShake.h"

UTSCameraShake::UTSCameraShake()
{
	OscillationDuration = 0.25f;
	OscillationBlendInTime = 0.1f;
	OscillationBlendOutTime = 0.2f;

	//RotOscillation.Pitch.Amplitude = FMath::RandRange(1.f, 3.f);
	//RotOscillation.Pitch.Frequency = FMath::RandRange(5.f, 10.f);

	//RotOscillation.Yaw.Amplitude = FMath::RandRange(1.f, 3.f);
	//RotOscillation.Yaw.Frequency = FMath::RandRange(5.f, 10.f);

	LocOscillation.Y.Amplitude = FMath::RandRange(-15.f, 15.f);
	LocOscillation.Y.Frequency = FMath::RandRange(60.f, 80.f);

	LocOscillation.Z.Amplitude = FMath::RandRange(-15.f, 15.f);
	LocOscillation.Z.Frequency = FMath::RandRange(60.f, 80.f);
	bSingleInstance = false;
}

