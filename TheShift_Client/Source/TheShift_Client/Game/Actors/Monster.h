#pragma once

#include "Game/Actors/Actor.h"
#ifdef TSCLIENT
#include "../../Actors/TSWeapon.h"
#endif

namespace TheShift
{
class Monster : public Actor
{
public:
	Monster(UID Id);

	virtual void Update(float DeltaTime) override;

#ifdef TSSERVER
	// 주위에 플레이어가 있는지 찾는다.
	virtual void LookForPlayer();

	//!! Client에선 ActorRecognizedAPlyer를 수신 시 RecognizedActors에 Actor를 추가해준다.
	// RecognizeRange보다 멀어질 경우 알아서 제거

	// LookForPlayer()가 INVALID_UID외의 UID를 return할 시 해당 Player를 따라간다.
	virtual void OnRecognizePlayer(UID PlayerId);

	// FollowPlayer중 Player와 일정 범위 내에 위치할 경우 행동을 정의한다.
	virtual void OnPlayerContact(UID PlayerId);
	void SetContactRange(float Range);
#endif


protected:
#ifdef TSSERVER
	// Attack 또는 Skill을 사용하게 되는 거리
	// ex) Player와 이 거리보다 가까워지게 되면 공격
	// Player와 이 거리보다 가까워지게 되면 OnPlayerContact()가 불린다.
	float ContactRange;
	float SquaredContactRange;
#endif
};
}
