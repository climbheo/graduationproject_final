#pragma once

#include "Game/Actors/Monster.h"


namespace TheShift
{
class KnightMonster : public Monster
{
public:
	KnightMonster(UID Id);

#ifdef TSSERVER
	void OnPlayerContact(UID PlayerId) override;
#endif

private:
#ifdef TSSERVER
	bool AttackCoolDown = true;
#endif
};
}
