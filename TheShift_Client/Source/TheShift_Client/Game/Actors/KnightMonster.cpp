#include "Game/Actors/KnightMonster.h"
#include "Game/System/GameState.h"
#include "Game/Actors/Trigger.h"
#ifdef TSSERVER
#include "Game/System/TimerCallee.h"
#endif

using namespace std::chrono_literals;

namespace TheShift
{

KnightMonster::KnightMonster(UID Id) : Monster(Id)
{
	Type = ActorType::KnightMonster;
#ifdef TSSERVER
	SetRecognizeRange(500.f);
	SetContactRange(150.f);
#endif
}

#ifdef TSSERVER
void KnightMonster::OnPlayerContact(UID PlayerId)
{
	// Cool-Down check
	if(AttackCoolDown == true)
	{
		//AttackCoolDown = false;
		//std::shared_ptr<CoolDown> AttackCoolDownTimer = std::make_shared<CoolDown>();
		//AttackCoolDownTimer->SetTimer(3s);
		//AttackCoolDownTimer->Register(&AttackCoolDown);
		//CurrentGameState->AddToTimer(AttackCoolDownTimer);

		//// Trigger 등록 및 세팅
		//UID TriggerId = CurrentGameState->SpawnActor(ActorType::Trigger, WorldTransform.GetWorldTranslation());
		//auto AttackTrigger = std::reinterpret_pointer_cast<Trigger>(CurrentGameState->GetActor(TriggerId));
		//AttackTrigger->Rotate(WorldTransform.GetWorldRotation());
		//auto TriggerVolume = AttackTrigger->AddTriggerVolume<BoundingSphere>();
		//TriggerVolume->SetRadius(75.f);
		//TriggerVolume->GetTransform().Translate(100.f, 0.f, 100.f);
		//
		//// 0.5초 후에 100의 데미지를 입히는것으로 등록
		//AttackTrigger->SetAsAttack(100.f, 0.5f, ActType::BaseAttack_1, CurrentGameState->GetActor(Id));

		//// 공격 함을 알림
		//Serializables::ActWithTarget AttackMessage;
		//AttackMessage.Self = Id;
		//AttackMessage.TargetId = PlayerId;
		//AttackMessage.Type = ActType::BaseAttack_1;
		//CurrentGameState->BroadcastPacket(AttackMessage);
	}
}
#endif

}
