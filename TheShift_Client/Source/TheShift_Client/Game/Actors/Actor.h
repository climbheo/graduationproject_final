﻿#pragma once

#include "Game/Components/BoundingVolume.h"
#include "Protocol/Types.h"
#include "Protocol/Protocol.h"
#include "Protocol/Serializables.h"
#include "../Components/Movement.h"
#ifdef TSSERVER
#include "../System/TimerCallee.h"
#endif


/// <summary>
/// Actor의 스탯
/// 이동 속도, 공격력, 공격력 등을 저장한다.
/// 실시간으로 변화하는 값이 아닌 기본값을 저장한다.
/// </summary>
namespace TheShift
{
class GameState;

class Actor
{
public:
	Actor(UID id);
	Actor(UID id, ActorType type);
	
	virtual ~Actor();
	virtual void OnInput(Serializables::Input inputValue);
#ifdef TSSERVER
	virtual void OnHit(ActType AttackType);
	virtual void SwapWeapon(WeaponType Type);
#endif
	virtual void MovementCheck(Serializables::Input inputValue);
	virtual void Update(float DeltaTime);

	void Init(const ActorInfo& Info);

	UID GetId() const;
	bool GetHit() const;
	ActorType GetType() const;
	Transform& GetTransform();
	std::shared_ptr<Movement> GetMovementComp() const;
	uint32_t GetHp() const;
	//const Eigen::Vector3f& GetDirection() const;
	Eigen::Vector3f GetVelocity() const;
	Eigen::Vector3f GetAcceleration() const;
	bool GetIsMovable() const;
#ifdef TSSERVER
	bool GetCanAct() const;
#endif
	std::vector<BoundingVolume*>& GetBoundingVolumes();
	//std::vector<BoundingVolume*>& GetBoundingVolumes();

	void SetTranslation(float X, float Y, float Z);
	void SetRotation(const Eigen::Quaternionf& Rotation);
	void SetRotationWithDegrees(float X, float Y, float Z);
	void SetRotationWithDegrees(const TheShift::Vector3f& Degrees);
	void SetLastTranslation();
	void SetScale(float value);
	void SetIsMovable(bool Movable);
	//void SetHit(bool Hit);
	bool ApplyDamage(float Damage, ActType AttackType = ActType::None, UID AttackerId = INVALID_UID);

	void Translate(float x, float y, float z);
	void Translate(Vector3f value);
	void Translate(Eigen::Vector3f value);
	void Rotate(const Eigen::Quaternionf& Rotation);
	void RotateWithDegrees(float x, float y, float z);
	void RotateWithDegrees(Vector3f value);
	void Scale(float value);

#ifdef TSCLIENT
	void RotateForTimeWithDegrees(Vector3f Rotation, float DurationMilliseconds);
	bool RotateForTimeOn = false;
	Vector3f AngularVelocity;
	float RotationDuration;
#endif
	
	void RegisterGameState(TheShift::GameState* BasedGameState);
	void RegisterBoundingVolume(BoundingVolume* NewVolume);

	// Platform 충돌 관련
	void SetStandingPlatform(std::shared_ptr<BoundingVolume> Platform);
	std::shared_ptr<BoundingVolume> GetStandingPlatform();
	void SetPlatformNormal(Eigen::Vector3f& PlatformSurfaceNormalVector);
	const Eigen::Vector3f& GetPlatformSurfaceNormal() const;
	bool IsStandingOnPlatform() const;
	void SetActorStandingOnPlatform(bool Value);

#ifdef TSSERVER
	//void RegisterAddActFunc(std::function<void(Serializables::Act&)> Func);
	void SetRecognizeRange(float Range);
	void CheckRecognizeRangeAndAdd(UID OtherActorId, double SquaredDist);
	void SetCanAct(bool Value);
#endif

protected:
	UID Id;
	ActorType Type;
	StatusType CurrentStatus;
	int Hp;
	//Eigen::Vector3f CurrentMoveDirection;

	// Actor의 기본 능력치
	Stats Stat;

	bool IsMovable = true;

	Transform WorldTransform;
	Eigen::Vector3f LastTranslation;

	TheShift::GameState* CurrentGameState = nullptr;
	std::shared_ptr<Movement> MovementComponent = nullptr;
#ifdef TSSERVER
	float RecognizeRange;
	float SquaredRecognizeRange;

	// whether or not this actor can act
	bool CanAct = true;
	// For AnimNotify Activators
	std::vector<std::shared_ptr<CheckActivator>> CurrentActivators;
#endif
	// whether or not this actor is hit at the moment
	bool IsHit = false;
	std::map<UID, double> RecognizedActors;

	std::vector<BoundingVolume*> BoundingVolumes;
	std::shared_ptr<BoundingVolume> PlatformVolume = nullptr;
	Eigen::Vector3f PlatformSurfaceNormal;
	bool StandingOnPlatform = false;
};
}
