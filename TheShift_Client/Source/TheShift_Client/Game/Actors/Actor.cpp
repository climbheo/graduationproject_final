﻿#include "Game/Actors/Actor.h"
#include "../System/GameState.h"
#include "../Components/Movement.h"
#include "Protocol/Log.h"

#ifdef TSSERVER
#include "Game/System/Network/SendRequirements.h"
using namespace std::chrono_literals;
#endif

#ifdef TSCLIENT
#include "EngineMinimal.h"
#endif

namespace TheShift
{
Actor::Actor(UID id, ActorType type) : Id(id), Type(type), Hp(500.f)
{
	Stat.ForwardRunSpeed = 200.f;
}

Actor::Actor(UID id) : Id(id), Type(ActorType::Prop), Hp(500.f)
{
	Stat.ForwardRunSpeed = 200.f;
}

Actor::~Actor()
{}

void Actor::OnInput(Serializables::Input inputValue)
{

}

#ifdef TSSERVER
// AttackType의 공격에 피격되었을 때 ApplyDamage에서 불린다.
void Actor::OnHit(TheShift::ActType AttackType)
{
	// 공격에 맞았으므로 IsHit을 true로 set해준다.
	IsHit = true;
	CanAct = false;

	// 맞는 중엔 스스로 움직일 수 없도록 한다.
	if(MovementComponent != nullptr)
		MovementComponent->LockInput();

	std::shared_ptr<Activator> HitReset = std::make_shared<Activator>();

	// IsHit을 false로 바꿔주는 lambda함수를 등록한다.
	HitReset->Register([this]() {
		IsHit = false;
		CanAct = true;

		// 다시 스스로 움직일 수 있도록 한다.
		if(MovementComponent != nullptr)
			MovementComponent->UnlockInput();
					   });

	// 0.75초 이후 위의 lambda함수가 불리도록 설정한다.
	HitReset->SetTimer(750ms);
	CurrentGameState->AddToTimer(HitReset);
}
#endif

void Actor::MovementCheck(Serializables::Input inputValue)
{
	if(inputValue.InputValue <= InputType::Camera_Rotation)
		MovementComponent->ApplyInput(inputValue);
}

void Actor::Update(float DeltaTime)
{
	if(MovementComponent != nullptr)
	{
		MovementComponent->Update(DeltaTime);
		//#ifdef TSCLIENT
		//	// UE Actor를 가져와서 바뀐 위치를 설정해준다.
		//	AActor* CorrespondingActor = CurrentGameState->GetUEActor(Id);
		//	Eigen::Vector3f Translation = WorldTransform.GetWorldTranslation();
		//	FVector UETranslation(Translation.x(), Translation.y(), Translation.z());
		//	CorrespondingActor->SetActorLocation(UETranslation);
		//#endif
	}

#ifdef TSCLIENT
	if (RotateForTimeOn)
	{
		RotationDuration -= DeltaTime;
		if (RotationDuration < 0.f)
		{
			RotateForTimeOn = false;
			RotateWithDegrees(AngularVelocity * RotationDuration);
		}
		else
		{
			RotateWithDegrees(AngularVelocity * DeltaTime);
		}

		// UE actor에도 적용을 시켜줘야 하는데 UE Actor의 id가 일치를 안하네...?
		// Tag가 있다면 tag로 찾아서?
	}
#endif
}

// Actor를 초기화 한다.(Bounding volume, stat 등)
void Actor::Init(const ActorInfo& Info)
{
	Info.BoundInfo;
	Info.Stat;
	Info.Type;

	Stat = Info.Stat;
	Type = Info.Type;
}

UID Actor::GetId() const
{
	return Id;
}

bool Actor::GetHit() const
{
	return IsHit;
}

ActorType Actor::GetType() const
{
	return Type;
}

Transform& Actor::GetTransform()
{
	return WorldTransform;
}

std::shared_ptr<Movement> Actor::GetMovementComp() const
{
	return MovementComponent;
}

uint32_t Actor::GetHp() const
{
	return Hp;
}

//const Eigen::Vector3f& Actor::GetDirection() const
//{
//    //return CurrentMoveDirection;
//}

Eigen::Vector3f Actor::GetVelocity() const
{
	if(MovementComponent != nullptr)
		return MovementComponent->GetCurrentVelocity();
	return Eigen::Vector3f::Zero();
}

Eigen::Vector3f Actor::GetAcceleration() const
{
	if(MovementComponent != nullptr)
		return MovementComponent->GetCurrentAcceleration();
	return Eigen::Vector3f::Zero();
}

// 이 Actor가 움직일 수 있는지 여부를 return한다.
bool Actor::GetIsMovable() const
{
	return IsMovable;
}

#ifdef TSSERVER
// 이 Actor가 현재 어떠한 Act를 할 수 있는지 여부를 return한다.
bool Actor::GetCanAct() const
{
	return CanAct;
}
#endif

std::vector<BoundingVolume*>& Actor::GetBoundingVolumes()
{
	return BoundingVolumes;
}

void Actor::SetTranslation(float X, float Y, float Z)
{
	WorldTransform.SetTranslation(X, Y, Z);
}

void Actor::SetRotation(const Eigen::Quaternionf& Rotation)
{
	WorldTransform.SetRotation(Rotation);
}

void Actor::SetRotationWithDegrees(float X, float Y, float Z)
{
	WorldTransform.SetRotationWithDegrees(X, Y, Z);
}

void Actor::SetRotationWithDegrees(const TheShift::Vector3f& Degrees)
{
	WorldTransform.SetRotationWithDegrees(Degrees.X, Degrees.Y, Degrees.Z);
}

void Actor::SetLastTranslation()
{
	LastTranslation = WorldTransform.GetWorldTranslation();
	for(auto& Volume : BoundingVolumes)
	{
		Volume->SetLastTranslation();
	}
}

void Actor::SetScale(float value)
{
	WorldTransform.SetScale(value);
}

// 이 Actor가 움직일 수 있는지 set한다.
void Actor::SetIsMovable(bool Movable)
{
	IsMovable = Movable;
}

//void Actor::SetHit(bool Hit)
//{
//	IsHit = Hit;
//}

// Damage를 적용시키고 사망 여부를 return한다.
bool Actor::ApplyDamage(float Damage, ActType AttackType, UID AttackerId)
{
	//LOG_DEBUG("Applying damage: %f", Damage);
	bool IsDead = false;
	Hp -= Damage;

#ifdef TSSERVER
	if(Hp < 0.f)
	{
		// 죽음
		IsDead = true;
	}
	else
		// 경직 설정 및 피격시 AI설정 등등
		OnHit(AttackType);

	// 공격이 적중했음을 알림
	Serializables::Hit HitMessage;
	HitMessage.Self = Id;
	HitMessage.Damage = Damage;
	HitMessage.IsDead = IsDead;
	HitMessage.AttackerId = AttackerId;
	HitMessage.AttackType = AttackType;

	CurrentGameState->BroadcastPacket(HitMessage);
#endif

	// 생존
	return IsDead;
}

#ifdef TSSERVER
void Actor::SwapWeapon(WeaponType Type)
{

}
#endif

void Actor::Translate(float x, float y, float z)
{
	WorldTransform.Translate(x, y, z);
}

void Actor::Translate(Vector3f value)
{
	WorldTransform.Translate(value);
}

void Actor::Translate(Eigen::Vector3f value)
{
	WorldTransform.Translate(value);
}

void Actor::Rotate(const Eigen::Quaternionf& Rotation)
{
	WorldTransform.Rotate(Rotation);
}

void Actor::RotateWithDegrees(float x, float y, float z)
{
	WorldTransform.RotateWithDegrees(x, y, z);
}

void Actor::RotateWithDegrees(Vector3f value)
{
	WorldTransform.RotateWithDegrees(value);
}

#ifdef TSCLIENT
// 주어진 시간동안 Rotation만큼 회전한다.
void Actor::RotateForTimeWithDegrees(Vector3f Rotation, float DurationMilliseconds)
{
	// 움직일 수 없는 액터들에 대해서만 실행한다.
	if (IsMovable)
		return;

	RotateForTimeOn = true;
	RotationDuration = DurationMilliseconds / 1000.f;
	AngularVelocity = Rotation * (1.f / RotationDuration);
}
#endif

void Actor::Scale(float value)
{
	WorldTransform.Scale(value);
}

void Actor::RegisterGameState(GameState* BasedGameState)
{
	CurrentGameState = BasedGameState;
}

#ifdef TSSERVER
//void Actor::RegisterAddActFunc(std::function<void(Serializables::Act&)> Func)
//{
//	AddAct = Func;
//}

// 이 Actor가 다른 Actor를 발견하게 되는 거리를 set한다.
// Monster	: 와 Player의 거리가 Range보다 가까워지게 되면 OnRecognizePlayer()가 불린다.
// Player	: 이 범위 외에 있는 Actor의 Update는 해당 Player에게 보내지 않는다. 
void Actor::SetRecognizeRange(float Range)
{
	RecognizeRange = Range;
	SquaredRecognizeRange = Range * Range;
}

// RecognizeRange보다 가까운 거리에 있는 Actor들을 추가한다.
void Actor::CheckRecognizeRangeAndAdd(UID OtherActorId, double SquaredDist)
{
	// 없으면 거리 검사
	if(RecognizedActors.find(OtherActorId) == RecognizedActors.end())
	{
		// 거리가 범위 안이라면 추가
		if(SquaredRecognizeRange > SquaredDist)
		{
			RecognizedActors.insert(std::make_pair(OtherActorId, SquaredDist));
			auto OtherActor = CurrentGameState->GetActor(OtherActorId);
			if(OtherActor == nullptr)
				return;

			// 이 Actor가 Player가 아니고 상대 Actor가 Player일때만 Packet을 전송한다.
			if(OtherActor->GetType() == ActorType::Player &&
			   Type != ActorType::Player)
			{
				// 발견했음을 Player들에게 알려준다.
				Serializables::ActorRecognizedAPlayer RecognizeMessage;
				RecognizeMessage.Self = Id;
				RecognizeMessage.TargetId = OtherActorId;

				CurrentGameState->BroadcastPacket(RecognizeMessage);
			}
		}
	}
	else
	{
		// 이미 있는데 거리가 더 멀면 삭제
		if(RecognizeRange * RecognizeRange < SquaredDist)
		{
			RecognizedActors.erase(OtherActorId);
		}
		// 아직 범위 내에 존재한다면 거리 갱신
		else
		{
			RecognizedActors.insert_or_assign(OtherActorId, SquaredDist);
		}
	}
}

// true if this actor can act at this moment
// false if this actor can't act at this moment
void Actor::SetCanAct(bool Value)
{
	CanAct = Value;
}

#endif

// Bounding Volume을 추가한다.
// Bounding Volume을 부모로 하는 자식 Volume들은 부모 Volume이 관리한다.
void Actor::RegisterBoundingVolume(BoundingVolume* NewVolume)
{
	BoundingVolumes.emplace_back(NewVolume);
}

// Actor가 현재 올라서 있는 Platform volume을 저장한다.
// Platform에 올라서 있음도 저장한다.
void Actor::SetStandingPlatform(std::shared_ptr<BoundingVolume> Platform)
{
	PlatformVolume = Platform;
}

// Actor가 현재 올라서있는 platform volume의 pointer를 return한다.
std::shared_ptr<BoundingVolume> Actor::GetStandingPlatform()
{
	return PlatformVolume;
}

// Actor가 현재 올라서있는 platform의 충돌면의 normal vector를 저장한다.
void Actor::SetPlatformNormal(Eigen::Vector3f& PlatformSurfaceNormalVector)
{
	PlatformSurfaceNormal = PlatformSurfaceNormalVector;
}

// Actor가 현재 올라서있는 platform의 충돌면의 normal vector를 return한다.
const Eigen::Vector3f& Actor::GetPlatformSurfaceNormal() const
{
	return PlatformSurfaceNormal;
}

// Actor가 현재 platform volume위에 서있는지 여부를 return한다.
bool Actor::IsStandingOnPlatform() const
{
	return StandingOnPlatform;
}

// Actor가 현재 platform volume위에 서있는지 여부를 저장한다.
void Actor::SetActorStandingOnPlatform(bool Value)
{
	StandingOnPlatform = Value;
}
}

