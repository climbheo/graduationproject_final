#include "Game/Actors/Monster.h"
#include "Game/System/GameState.h"


namespace TheShift
{

Monster::Monster(UID Id) : Actor(Id)
{
	MovementComponent = std::make_shared<Movement>(&WorldTransform, &CurrentStatus, Stat);
}

void Monster::Update(float DeltaTime)
{
#ifdef TSSERVER
	// 행동을 할 수 있을때만 실행한다.
	if(CanAct)
		LookForPlayer();
#endif
	Actor::Update(DeltaTime);
}

#ifdef TSSERVER
// 일정 범위 안에 플레이어가 들어와있는지 확인하고 거리에 따른 행동을 한다.
// RecognizeRange	: Player를 발견함 (ex Player를 따라감)
// ContactRange		: Player와 접촉함 (ex Player를 공격함)
void Monster::LookForPlayer()
{
	// 거리비교
	for(auto ActorDistPair : RecognizedActors)
	{
		// Player와 contact
		if(ActorDistPair.second < SquaredContactRange)
		{
			// Client는 Contact처리는 Packet을 수신해서 한다.
			// 알아서 계산해서 실행하지 않음.
			OnPlayerContact(ActorDistPair.first);
		}
		// Player를 발견
		else if(ActorDistPair.second < SquaredRecognizeRange)
		{
			OnRecognizePlayer(ActorDistPair.first);
		}
	}
}
#endif

#ifdef TSSERVER
// 이 Monster가 Player에게 공격과 같은 행동을 할 수 있게되는 거리를 set한다.
// Monster와 Player의 거리가 Range보다 가까워지게 되면 OnPlayerContact()가 불린다.
void Monster::SetContactRange(float Range)
{
	ContactRange = Range;
	SquaredContactRange = Range * Range;
}
#endif

#ifdef TSSERVER
// UID가 PlayerId인 Player를 발견했을때 하는 행동을 정의.
// Player와 이 거리보다 가까워지게 되면 불린다.
void Monster::OnRecognizePlayer(UID PlayerId)
{
	auto MyLocation = GetTransform().GetWorldTranslation();
	Eigen::Vector3f PlayerLocation;
	auto PlayerActor = CurrentGameState->GetActor(PlayerId);

	if(PlayerActor == nullptr)
	{
		RecognizedActors.erase(PlayerId);
		return;
	}

	// Player를 향해 이동하게 한다.
	PlayerLocation = PlayerActor->GetTransform().GetWorldTranslation();
	MovementComponent->SetDestination(PlayerLocation);
}
#endif

#ifdef TSSERVER
// 어떤 Player와 일정 범위 안에 위치하게 되면 하는 행동을 정의한다.
// Player와 이 거리보다 가까워지게 되면 불린다.
// Client가 몬스터가 공격했다는 Packet수신 시 실행한다.
void Monster::OnPlayerContact(UID PlayerId)
{

}
#endif

}

