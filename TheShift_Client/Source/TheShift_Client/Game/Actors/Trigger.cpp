﻿#include "Game/Actors/Trigger.h"
#include "Game/System/GameState.h"
#include "Game/Components/BoundingVolume.h"
#include "Game/System/CollisionResolver.h"
#include "Game/Components/Movement.h"
#include "Game/Actors/Character.h"


namespace TheShift
{
Trigger::Trigger(UID Id) : Actor(Id)
{}

// Template function instances
template std::shared_ptr<AABB> TheShift::Trigger::AddTriggerVolume();
template std::shared_ptr<OBB> TheShift::Trigger::AddTriggerVolume();
template std::shared_ptr<BoundingSphere> TheShift::Trigger::AddTriggerVolume();
template std::shared_ptr<BoundingCapsule> TheShift::Trigger::AddTriggerVolume();

template<typename VolumeT>
typename std::shared_ptr<VolumeT> TheShift::Trigger::AddTriggerVolume()
{
	// Volume 생성
	auto MyId = GetId();
	std::shared_ptr<VolumeT> Volume = CurrentGameState->GetCollisionResolver()->CreateTrigger<VolumeT>();
	if((*CurrentGameState->GetActors()).find(MyId) != CurrentGameState->GetActors()->end())
	{
		Volume->SetParent((*CurrentGameState->GetActors())[MyId]);
	}

	// OnTriggerOverlap 설정
	Volume->SetOnTriggerOverlap([this](std::shared_ptr<BoundingVolume> Other) {
		// Overlap 이벤트는 매 프레임 불린다. 
		// 없던 Actor가 범위 안에 Enter하면
		if(std::find_if(OverlappedActors.begin(), OverlappedActors.end(),
						[Other](std::pair<std::shared_ptr<Actor>, bool> a) {
							return a.first == Other->GetParentActor();
						}) == OverlappedActors.end())
		{
			OverlappedActors.emplace_back(std::make_pair(Other->GetParentActor(), true));
			// OnTriggerEnter 실행
			if(OnTriggerEnterFunc)
			{
				OnTriggerEnterFunc(Other->GetParentActor());
			}
		}
						// 원래 범위 내에 있던 Actor이면
		else
		{
			auto Iter = std::find_if(OverlappedActors.begin(), OverlappedActors.end(), [Other](std::pair<std::shared_ptr<Actor>, bool> a) {
				return Other->GetParentActor() == a.first;
									 });
			// 해당 Actor가 Enter했음을 표기한다.
			// 나중에 Leave를 판별하기 위함. (true였는데 false면 Leave)
			// 해당 bool값은 충돌체크 완료 시 false로 set, overlap시 true로 set
			Iter->second = true;
		}
		// Overlap Func call
		if(OnTriggerOverlapFunc)
			OnTriggerOverlapFunc(Other);
								});

	// Volume 추가
	TriggerVolumes.emplace_back(Volume);

	return Volume;
}

void Trigger::SetOnTriggerEnterFunc(std::function<void(std::shared_ptr<Actor>)> Func)
{
	OnTriggerEnterFunc = Func;
}

void Trigger::SetOnTriggerLeaveFunc(std::function<void(std::shared_ptr<Actor>)> Func)
{
	OnTriggerLeaveFunc = Func;
}

void Trigger::SetOnTriggerOverlapFunc(std::function<void(std::shared_ptr<BoundingVolume>)> Func)
{
	OnTriggerOverlapFunc = Func;
}

// 이 Trigger에 Overlap
void Trigger::SetAsDamager(float Damage)
{
	SetOnTriggerEnterFunc([Damage, this](std::shared_ptr<Actor> Other) {
		// 데미지를 준 액터에 중복으로 데미지가 적용되는것을 막음
		if(Other->ApplyDamage(Damage))
		{

		}
						  });
	LifeTime = 0.01f;
}

// Trigger volume에 intersect하면 hp를 깎는다.
// 다른 actor의 공격에 의해 hp를 깎는 경우 이 함수 대신 SetAsAttack을 사용한다.
void Trigger::SetAsDamager(float Damage, float Timer)
{
	SetOnTriggerEnterFunc([Damage, this](std::shared_ptr<Actor> Other) {
		// 데미지를 준 액터에 중복으로 데미지가 적용되는것을 막음
		Other->ApplyDamage(Damage);
						  });
	LifeTime = 0.01f;
	TimerTime = Timer;
}

// Trigger volume에 intersect하면 hp를 깎는다.
// 공격자, 공격 종류에 대한 정보를 받아 클라이언트들에게 이를 알려준다.
void Trigger::SetAsAttack(float Damage, float Timer, ActType AttackType, std::shared_ptr<Actor> Attacker)
{
	// 데미지를 준 액터에 중복으로 데미지가 적용되는것을 막음
	SetOnTriggerEnterFunc([Damage, AttackType, Attacker, this](std::shared_ptr<Actor> Other) {

		ActorType AttackerType = Attacker->GetType();
		ActorType OtherType = Other->GetType();

		// 둘중 하나가 Player고 나머지 하나가 Player가 아니라면 데미지 적용
		if((AttackerType == ActorType::Player && OtherType != ActorType::Player) ||
			(AttackerType != ActorType::Player && OtherType == ActorType::Player) && !Other->GetHit())
		{

			// Damage 적용
			bool IsDead = Other->ApplyDamage(Damage, AttackType, Attacker->GetId());

			auto HitLocation = GetTransform().GetTranslation();
			auto OtherLocation = Other->GetTransform().GetTranslation();

			OtherLocation.x() = HitLocation.x() - OtherLocation.x();
			OtherLocation.y() = HitLocation.y() - OtherLocation.y();
			OtherLocation.z() = HitLocation.z() - OtherLocation.z();

			OtherLocation.normalize();

			OtherLocation.x() *= -150.f;
			OtherLocation.y() *= -150.f;
			OtherLocation.z() *= -150.f;

		#ifdef TSSERVER



			Other->SetHit(true);
			if(OtherType == ActorType::Player)
			{
				auto player = std::static_pointer_cast<Character>(Other);
				if(player)
					player->InitAttackState();
			}
		#endif // TSSERVER
			Other->GetMovementComp()->AddImpulse(OtherLocation, 1.f);

			// 사망했다면 Destroy해준다.
			if(IsDead == true)
			{
				CurrentGameState->DestroyActor(Other);
			}
		}
						  });
	LifeTime = 0.01f;
	TimerTime = Timer;
}

// 해당 Trigger에 Player가 Enter할 경우 Tag를 가진 monster들을 spawn한다.
void Trigger::SetAsSpawner(const char* Tag)
{
	// Spawner의 수명은 무한
	// 한번 Enter하면 소멸한다.
	LifeTime = -1.f;

	SetOnTriggerEnterFunc([Tag, this](std::shared_ptr<Actor> Player) {
		// Player가 enter했을때만 발동한다.
		if(Player->GetType() == ActorType::Player)
		{

		#ifdef TSSERVER



			// 해당 Tag를 가진 Actor들을 모두 spawn하고 player들에게 broadcast한다.
			CurrentGameState->SpawnActorsWithTagAndBroadcast(Tag);
		#endif // TSSERVER
			// 발동되었으니 소멸되도록 한다.
			LifeTime = 0.f;
		}
						  });
}

// Trigger의 LifeTime을 설정한다.
// -값이면 무한지속
void Trigger::SetLifeTime(float Duration)
{
	LifeTime = Duration;
}

// Duration만큼 지난 후에 발동할수 있도록 한다.
void Trigger::SetTimer(float Duration)
{
	TimerTime = Duration;
}

float Trigger::GetLifeTime()
{
	return LifeTime;
}

// 발동까지 남은 시간을 return한다.
float Trigger::GetTimerLeftTime()
{
	return TimerTime;
}

void Trigger::ResetOverlapMark()
{
	for(auto& OverlappedActor : OverlappedActors)
	{
		OverlappedActor.second = false;
	}
}

// Trigger범위 내에 있다가 밖으로 나간 Actor가 있는지 check하고 
// OnTriggerLeave()가 valid하다면 실행하고 Actor list에서 삭제한다.
void Trigger::TriggerLeaveCheck()
{
	for(auto Iter = OverlappedActors.begin(); Iter != OverlappedActors.end();)
	{
		if(Iter->second == false)
		{
			if(OnTriggerLeaveFunc)
				OnTriggerLeaveFunc(Iter->first);

			Iter = OverlappedActors.erase(Iter);
			continue;
		}
		Iter++;

	}
}
}