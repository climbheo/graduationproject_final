﻿#include "GameState.h"
#include "Game/System/MapData.h"
#include <functional>
#include "Protocol/Log.h"

#include "Game/Components/BoundingVolume.h"
#include "Game/Components/Heightmap.h"
#include "Game/Actors/Character.h"
#include "Game/Actors/Trigger.h"
#include "Game/Actors/KnightMonster.h"
#include "Game/System/CollisionResolver.h"

#ifdef TSCLIENT
#include "Network/Components/TSNetworkedComponent.h"
#include "EngineMinimal.h"
#endif

using namespace std::chrono_literals;

namespace TheShift
{

GameState::GameState() : TimePassed(0)
{
	CollisionResolver_ = new CollisionResolver(this);
#ifdef TSCLIENT
	DummyActor = std::make_shared<Character>(INVALID_UID);
	ReconciliationDuration = 50ms;
#endif
}

/// <summary>
/// Game state를 update한다.
/// Player가 있으면 true를 return하고 없으면 false를 return한다.
/// </summary>
bool GameState::Update(std::chrono::nanoseconds deltaTime)
{
#ifdef TSSERVER
	// 남아있는 Player가 아무도 없다면 종료한다.
	if(Players.size() == 0)
	{
		return false;
	}

	if(AllPlayerLoaded)
	#endif

	#ifdef TSCLIENT
		if(MapLoaded)
		#endif

			if(true)
			{
				TimePassed += deltaTime;


			#ifdef TSSERVER
				auto UpdateData = std::make_shared<Serializables::SessionStateUpdate>();
				//auto CollisionDebugData = std::make_shared<Serializables::CollisionDebug>();
				bool Changed = false;
				JobTimer.Poll();
			#endif

				//bool SendCollisionDebug = false;
				//CollisionDebugDuration += deltaTime;
				//
				//if(CollisionDebugDuration > 16ms)
				//{
				//	CollisionDebugDuration = 0ns;
				//	SendCollisionDebug = true;
				//}
				//else
				//{
				//	SendCollisionDebug = false;
				//}

				for(auto& ActorPair : Actors)
				{
					ActorPair.second->Update(deltaTime.count() * 1e-9f);
				}

#ifdef TSCLIENT
				// Character위치 보간
				if(ReconciliationAvailable)
				{
					float ReconciliationAmount = deltaTime / ReconciliationDuration;
					if(ReconciliationAmount > 1.f)
					{
						ReconciliationAmount = 1.f;
					}
					ReconciliationDelta += deltaTime;
					Eigen::Vector3f ReconciliationTranslation = ReconciliationDirection * (ReconciliationLength * ReconciliationAmount);
					auto MyActor = Actors[MyActorId];

					// MyActor를 DummyActor에 대해 보간시켜준다.
					if(MyActor != nullptr)
					{
						MyActor->Translate(ReconciliationTranslation);
					}
					else
					{
						LOG_WARNING("Can't do reconciliation. Can't find my actor %d", MyActorId);
					}
					
					if(ReconciliationDelta > ReconciliationDuration)
					{
						ReconciliationAvailable = false;
					}
				}
#endif

				//auto CollisionStartTime = std::chrono::high_resolution_clock::now();
				// 충돌처리
				CollisionResolver_->ResolveCollisions(deltaTime.count() * 1e-9f);

				//auto CollisionDuration = std::chrono::high_resolution_clock::now() - CollisionStartTime;

				for(auto& ActorPair : Actors)
				{
					if(ActorPair.second->GetType() == ActorType::Prop)
						continue;

				#ifdef TSCLIENT
					// 내 Character만 UE Actor의 Location을 set해준다.
					if(ActorPair.first == MyActorId)
					{
						auto Translation = ActorPair.second->GetTransform().GetWorldTranslation();
						auto UEActor = GetUEActor(ActorPair.first);
						if(UEActor != nullptr)
							UEActor->SetActorLocation(FVector(Translation.x(), Translation.y(), Translation.z()));
					}
				#endif

				#ifdef TSSERVER
					Serializables::ActorPosition ActorPos;

					//Eigen::Vector3f Speed = ActorPair.second->GetVelocity();
					//if (!Speed.isZero())
					//    ActorPair.second->Translate(ActorPair.second->GetVelocity() * (deltaTime.count() / 1000.f));
					//TODO: 변화가 있는 Actor만 보내야 함.
					ActorPos.Self = ActorPair.first;
					ActorPos.Position = ActorPair.second->GetTransform().GetWorldTranslation();
					ActorPos.Rotation = ActorPair.second->GetTransform().GetWorldDegrees();
					ActorPos.Speed = ActorPair.second->GetVelocity();
					UpdateData->ActorPositions.Array.emplace_back(ActorPos);


					//// Collsion Debug
					//auto Volumes = ActorPair.second->GetBoundingVolumes();
					//for(auto Volume : Volumes)
					//{
					//	Serializables::BoundingVolumeDebug VolumeInfoForDebug;
					//	VolumeInfoForDebug.Position = Volume->GetTransform().GetWorldTranslation();
					//	VolumeInfoForDebug.Rotation = Volume->GetTransform().GetWorldDegrees();;
					//	VolumeInfoForDebug.BoundingVolumeType_ = Volume->GetVolumeType();
					//	VolumeInfoForDebug.ScaledExtent = Volume->GetRearrangedExtent();
					//	VolumeInfoForDebug.HalfHeight = Volume->GetHalfHeight();
					//	VolumeInfoForDebug.Radius = Volume->GetRadius();
					//	CollisionDebugData->Info.Array.push_back(VolumeInfoForDebug);

					//	//spdlog::debug("Volume Position: {0}, {1}, {2}", VolumeInfoForDebug.Position.X, VolumeInfoForDebug.Position.Y, VolumeInfoForDebug.Position.Z);
					//}

					//spdlog::info("Id: {0}", ActorPos.Self);
					//spdlog::info("Locataion: X: {0}, Y: {1}, Z: {2}", ActorPos.Position.X, ActorPos.Position.Y, ActorPos.Position.Z);
					//spdlog::info("Rotation: X: {0}, Y: {1}, Z: {2}", ActorPos.Rotation.X, ActorPos.Rotation.Y, ActorPos.Rotation.Z);
					//spdlog::info("Speed: X: {0}, Y: {1}, Z: {2}", ActorPos.Speed.X, ActorPos.Speed.Y, ActorPos.Speed.Z);
					Changed = true;
				#endif
				}

				//if(!Acts.empty())
				//	Changed = true;

				//while(!Acts.empty())
				//{
				//	Serializables::Act& Act = Acts.front();
				//	UpdateData->Acts.Array.emplace_back(Act);

				//	Acts.pop();
				//}

			#ifdef TSSERVER
				if(Changed)
				{
					DataToBeSent.emplace_back(UpdateData);

					//if(SendCollisionDebug)
					//{
					//	DataToBeSent.emplace_back(CollisionDebugData);
					//	//spdlog::debug("Collision Debug send");
					//}
				}

				//LOG_INFO("Collision Duration: \t%d", CollisionDuration.count());
			#endif
			}
	return true;
}

/// <summary>
/// 받은 Message를 처리한다.
/// </summary>
/// TODO: base class에서 ClassId를 불러서 type을 구분할 수 있으면 TypeOfMessage를 넣을 필요가 없잖아... Test해보기
#ifdef TSSERVER
void GameState::ApplyMessage(Message* message)
{
	switch(static_cast<Serializables::Type>(message->Data->ClassId()))
	{
	case Serializables::Type::SessionJoinReq:
		ProcessSessionJoinReq(message);
		break;

	case Serializables::Type::SessionExit:
		ProcessSessionExit(message);
		break;

	case Serializables::Type::LoadCompleted:
		ProcessLoadCompleted(message);
		break;

	case Serializables::Type::Input:
		ProcessInput(message);
		break;

	case Serializables::Type::SwapWeapon:
		ProcessSwapWeapon(message);
		break;

	case Serializables::Type::Unhandled:
		LOG_WARNING("GameState: Unhandled message from %d", message->From);
		break;

	default:
		LOG_WARNING("GameState: Unhandled message from %d", message->From);
		break;
	}
}
#endif

#ifdef TSSERVER
std::vector<std::shared_ptr<Serializables::SerializableData>>* GameState::GetDataToBeSent()
{
	return &DataToBeSent;
}
#endif

#ifdef TSSERVER
void GameState::ClearDataToBeSent()
{
	DataToBeSent.clear();
}
#endif

#ifdef TSSERVER
void GameState::AddNewPlayer(std::shared_ptr<Avatar> player)
{
	Players.insert(std::make_pair(player->ClientId_, player));
}
#endif

#ifdef TSSERVER
std::map<ClientId, std::shared_ptr<Avatar>>* GameState::GetPlayers()
{
	return &Players;
}
#endif

std::map<UID, std::shared_ptr<Actor>>* GameState::GetActors()
{
	return &Actors;
}

/// <summary>
/// 저장된 Actor들의 map pointer를 return한다.
/// operator[] 대신 .at(keyType) 을 사용해주세요... const라 operator[] 안됨
/// </summary>
const std::map<UID, std::shared_ptr<Actor>>* GameState::GetActors() const
{
	return &Actors;
}

std::list<std::shared_ptr<Actor>> GameState::GetActorsByType(ActorType Type)
{
	std::list< std::shared_ptr<Actor>> ActorLst;

	for (auto& iter : Actors)
	{
		if (iter.second->GetType() == Type)
			ActorLst.emplace_back(iter.second);
	}

	return ActorLst;
}

std::list<std::shared_ptr<Actor>> GameState::GetActorsByTypes(ActorType* Types, int TypeNum)
{
	std::list< std::shared_ptr<Actor>> ActorLst;

	for (auto& iter : Actors)
	{
		for (int i = 0; i < TypeNum; ++i)
		{
			if (iter.second->GetType() == Types[i])
				ActorLst.emplace_back(iter.second);
		}
	}

	return ActorLst;
}

// 해당 Id의 Actor를 return한다.
std::shared_ptr<TheShift::Actor> GameState::GetActor(UID Id)
{
	if(Actors.find(Id) == Actors.end())
	{
		LOG_WARNING("Actor #%d does not exist.", Id);
		return nullptr;
	}
	else
		return Actors.at(Id);
}

const std::vector<std::shared_ptr<TheShift::Heightmap>>& GameState::GetHeightmaps() const
{
	return Heightmaps;
}

#ifdef TSSERVER
TheShift::MapType GameState::GetMapType() const
{
	return Map;
}
#endif

//#ifdef TSSERVER
//void GameState::AddAct(Serializables::Act& act)
//{
//	Acts.push(act);
//}
//#endif

/// <summary>
/// Timer에 Job을 추가합니다.
/// Callable 객체인 Callee는 시간이 member인 TimePoint를 지난 경우 자동으로 실행됩니다.
/// </summary>
#ifdef TSSERVER
void GameState::AddToTimer(std::shared_ptr<TimerCallee> Callee)
{
	JobTimer.Push(Callee);
}

void GameState::BroadcastPacket(Serializables::SerializableData& Data)
{
	for(auto PlayerPair : Players)
	{
		Network::Send(PlayerPair.first, Data);
	}
}

// 해당 Tag를 가진 Actor들을 spawn한다.
void GameState::SpawnActorsWithTagAndBroadcast(const char* Tag)
{
	auto RangePair = PlacementInfo.equal_range(Tag);
	for(auto Iter = RangePair.first; Iter != RangePair.second; ++Iter)
	{
		//TODO: 패킷 하나로 줄이는게 좋음. 한꺼번에 보내도록.
		Serializables::ActorCreated Packet;
		Packet.ActorId = SpawnActor(Iter->second.Type, Iter->second.ActorPosition);
		Packet.Position = Iter->second.ActorPosition;
		Packet.Type = Iter->second.Type;
		BroadcastPacket(Packet);
	}
}

void GameState::SetUpdateTimestep(float NewTimestep)
{
	UpdateTimestep = NewTimestep;
}

#endif

TheShift::CollisionResolver* GameState::GetCollisionResolver()
{
	return CollisionResolver_;
}

/// <summary>
/// 해당 map을 불러온다.
/// </summary>
void GameState::Init(MapType map)
{
	Map = map;
	if(map != MapType::Unhandled)
		LoadMap(map);
	else
		LOG_ERROR("Map type is not valid. Can't Init.");

	// Unhandled이면 아무것도 하지 않음.
	// 나중에 게임이 시작 되었을 때 map이 전달되면 그때 초기화 함.
	// LoadMap을 부르면 됨
}

void GameState::LoadMap(MapType map)
{
#ifdef TSSERVER
	for(int i = 0; i < Acts.size(); ++i)
		Acts.pop();
#endif

	CollisionResolver_->Reset();
	PlacementInfo.clear();
	ActorStat.clear();
	Heightmaps.clear();
	StartPointIndices.clear();
	Actors.clear();
	Interactables.clear();

	Map = map;

	// MapInfo를 읽어온다.
	auto& CurrentMapInfo = MapData::GetInstance().GetMapInfo(map);

	// Heightmap들을 읽어온다.
	for(const auto& Heightmap : CurrentMapInfo.Heightmaps)
	{
		Heightmaps.emplace_back(std::make_shared<TheShift::Heightmap>(Heightmap.Verts));
	}

	// Actor들의 정보를 읽어온다.
	for(auto& ActorTypeInfo : CurrentMapInfo.ActorTypeInfo)
	{
		ActorStat.insert(std::make_pair(ActorTypeInfo.Type, ActorTypeInfo));
	}

	// Map의 배치 정보를 저장한다. 
	for(auto& ActorPlacement : CurrentMapInfo.ActorPlacements)
	{
		PlacementInfo.insert(std::make_pair(ActorPlacement.Tag, ActorPlacement));
	}

	if(PlacementInfo.empty())
	{
		LOG_ERROR("MapData is empty.");
	}
	else
	{
		LOG_INFO("MapData is not empty.");
	}

	// Start Point index들을 추가한다.
	for(int i = 0; i < PlacementInfo.count("Start Point"); ++i)
	{
		StartPointIndices.insert(std::make_pair(INVALID_CLIENTID, i));
	}

	// 처음 로드되어야 할 Actor들 CreateActor 하기
	// Tag(Key)가 ""이면 맵 로드할때 모두 생성한다.
	//auto ObjectsIter = PlacementInfo.lower_bound("");
	//auto ObjectsEnd = PlacementInfo.upper_bound("");
	//for(; ObjectsIter != ObjectsEnd; ObjectsIter++)
	for (auto& ObjectsIter : PlacementInfo)
	{
		// Tag가 비어있지 않고 Interactable이 아니라면 넘긴다.
		// 문과 같이 Tag가 있고 Interactable인 애들만 처음에 초기화 할 때 생성한다.
		//if (ObjectsIter.second.Type != ActorType::Interactable && !ObjectsIter.second.Tag.empty())
		//	continue;
		if (ObjectsIter.second.Type == ActorType::Player)
			continue;

#ifdef TSCLIENT
		if (ObjectsIter.second.Type != ActorType::Prop && ObjectsIter.second.Type != ActorType::Player)
		{
			continue;
		}
#endif

		UID NewActorId = SpawnActor(ObjectsIter.second.Type, ObjectsIter.second.ActorPosition);
		if (NewActorId != INVALID_UID)
		{
			if (Actors.find(NewActorId) == Actors.end())
			{
				LOG_WARNING("Can't find actor. UID: %d", NewActorId);
				break;
			}
			else
			{
				Actors[NewActorId]->SetRotationWithDegrees(ObjectsIter.second.ActorRotation);
				if (ObjectsIter.second.Type == ActorType::Prop
					|| ObjectsIter.second.Type == ActorType::Interactable
					|| ObjectsIter.second.Type == ActorType::Destructible
					|| ObjectsIter.second.Type == ActorType::Trigger)
				{
					// Is this actor Movable?
					Actors[NewActorId]->SetIsMovable(ObjectsIter.second.IsMovable);
					CollisionResolver_->CreateBoundingVolumes(Actors[NewActorId], ObjectsIter.second.BoundInfo);
				}
				if (ObjectsIter.second.Type == ActorType::Prop)
				{
					Actors[NewActorId]->SetIsMovable(ObjectsIter.second.IsMovable);
					//CollisionResolver_->CreateBoundingVolumes(Actors[NewActorId], ObjectsIter.second.BoundInfo);
				}

				// Tag가 있는 actor라면 Interactables에 추가한다.
				if (!ObjectsIter.first.empty())
				{
					Interactables.insert(std::make_pair(ObjectsIter.first, NewActorId));
				}

				//#ifdef TSSERVER
				//	if(ObjectsIter->second.Type != ActorType::Prop && ObjectsIter->second.Type != ActorType::Player)
				//	{
				//		Serializables::ActorCreated Data;
				//		Data.ActorId = NewActorId;
				//		Data.Position = ObjectsIter->second.ActorPosition;
				//		Data.Type = ObjectsIter->second.Type;
				//		BroadcastPacket(Data);
				//	}
				//#endif
			}
		}
	}

	switch (map)
	{
	case MapType::TestMap:
		break;

	default:
		break;
	}

#ifdef TSSERVER
	// 마지막으로 Player들 초기화
	for (auto& PlayerPair : Players)
	{
		InitPlayer(ActorType::Player, PlayerPair.first);
	}
#endif

	// Map이 바뀌었으면 시간은 0으로 set해준다.
	TimePassed = std::chrono::nanoseconds::zero();

#ifdef TSCLIENT
	MapLoaded = true;
#endif

	LOG_INFO("Map: %d loaded.", map);
}

/// <summary> 
/// Game 시작에 대한 처리를 한다.
/// </summary>
//TODO: 다른 일도 동반해야 할 것...?
#ifdef TSSERVER
void GameState::StartGame()
{
	auto Data = BuildSessionStarted();
	//spdlog::info("Players Size: {0}", Data.Players.Array.size());
	LOG_INFO("Players Size: %d", Data.Players.Array.size());

	for(auto& PlayerPair : Players)
	{
		Network::Send(PlayerPair.first, Data);
	}
}
#endif

#ifdef TSCLIENT
// GameState에서 UE Actor를 접근할 수 있도록 등록한다.
void GameState::RegisterNetworkComps(TMap<UID, UTSNetworkedComponent*>* Container)
{
	NetworkComps = Container;
	LOG_INFO("NetworkComps registered.");
}

// GameState에서 UE용 Actor에 접근해야할 때 사용.
// AActor의 pointer를 return한다.
AActor* GameState::GetUEActor(UID Id)
{
	auto Comp = NetworkComps->Find(Id);
	if(Comp != nullptr)
	{
		return (*Comp)->GetOwner();
	}
	return nullptr;
}

TheShift::UID GameState::GetMyId() const
{
	return MyActorId;
}

// UE에서 Actor의 정보가 필요할 경우 TheShift::Actor의 pointer를 return한다.
std::shared_ptr<TheShift::Actor> GameState::GetCorrespondingActor(UID Id)
{
	auto RetVal = Actors.find(Id);
	if(RetVal != Actors.end())
		return RetVal->second;
	return nullptr;
}

void GameState::SetMyActorId(UID Id)
{
	MyActorId = Id;
}


// Server측에서 처리가 되지 않은 Input들을 처리해 현재 위치와 보간시켜준다.
void GameState::ApplyUnprocessedInput(const Serializables::ActorPosition& PositionInfo,
									  const std::vector<InputInfo>& UnprocessedInputs,
									  UID MyCharacterId,
									  TimePoint LastProcessedTimePoint)
{
	// Actor Id 확인 
	if (MyActorId != MyCharacterId)
	{
		return;
	}

	auto Iter = Actors.find(MyActorId);
	if (Iter == Actors.end())
	{
		return;
	}

	auto MyCharacter = Iter->second;

	// Server에서 처리된 결과를 DummyActor에 적용시켜준다.
	DummyActor->SetTranslation(PositionInfo.Position.X, PositionInfo.Position.Y, PositionInfo.Position.Z);
	DummyActor->SetRotationWithDegrees(PositionInfo.Rotation.X, PositionInfo.Rotation.Y, PositionInfo.Rotation.Z);

	// 현재 movement component의 상태를 보간하려는 actor와 동일하게 해야한다.
	*DummyActor->GetMovementComp() = *MyCharacter->GetMovementComp();

	// 처리되지 않은 inpu 처리
	for(auto iter = UnprocessedInputs.begin(); iter != UnprocessedInputs.end(); ++iter)
	{
		// 처리되지 않은 input의 Duration을 구하고 Update한다.
		TimePoint nextInputTimePoint;
		if(std::next(iter, 1) != UnprocessedInputs.end())
			nextInputTimePoint = std::next(iter, 1)->Time;
		else
			nextInputTimePoint = std::chrono::high_resolution_clock::now();

		// 이후 실제 Actor를 DummyActor의 위치로 보간시킨다.
		DummyActor->GetMovementComp()->Update(
			std::chrono::duration_cast<std::chrono::nanoseconds>(nextInputTimePoint - iter->Time).count() * 1e-9f
		);
	}

	// DummyActor의 위치를 조정했으니 Update()에서 실제 Actor를 DummyActor에 대해 보간시킨다.
	ReconciliationAvailable = true;
	const Eigen::Vector3f ReconciliationVector = 
		DummyActor->GetTransform().GetWorldTranslation()
		- MyCharacter->GetTransform().GetWorldTranslation();
	ReconciliationDirection = ReconciliationVector.normalized();
	ReconciliationLength = ReconciliationVector.norm();
}

// Reconciliation을 몇초동안 진행할 것인지(보간) set한다.
void GameState::SetReconciliationDuration(float DurationInSeconds)
{
	ReconciliationDuration = std::chrono::nanoseconds(static_cast<long long>(DurationInSeconds * 1e+9));
}
#endif

#ifdef TSSERVER
void GameState::ProcessSessionExit(Message* message)
{
	if(Players.find(message->From) != Players.end())
	{
		UID ActorId = Players.at(message->From)->ActorId_;
		Players.erase(message->From);
		//UID ActorId = std::reinterpret_pointer_cast<Serializables::Disconnect>(message->Data)->ActorId;
		DestroyActor(ActorId);

		// 남아있는 Player들에게 접속을 종료한 Player가 있음을 알린다.
		Serializables::PlayerDisconnect ToRemainings;
		ToRemainings.DisconnectingClientId = message->From;

		for(auto& Player : Players)
		{
			Network::Send(Player.first, ToRemainings);
		}

		if(StartPointIndices.find(message->From) != StartPointIndices.end())
		{
			int StartPointNum = StartPointIndices[message->From];
			StartPointIndices.erase(message->From);
			StartPointIndices.insert(std::make_pair(INVALID_CLIENTID, StartPointNum));
		}
		//spdlog::info("GameState: Player destroyed {0}", message->From);
		LOG_INFO("GameState: Player destroyed %d", message->From);
	}
}
#endif

/// <summary>
/// Player들로부터 Load가 완료되었다는 정보를 수신하면 Game update를 시작한다.
/// </summary>
#ifdef TSSERVER
void GameState::ProcessLoadCompleted(Message* message)
{
	if(!AllPlayerLoaded)
	{
		auto Data = std::reinterpret_pointer_cast<Serializables::LoadCompleted>(message->Data);

		if(Players.find(message->From) != Players.end())
		{
			Players[message->From]->IsLoaded_ = true;
		}

		for(auto& PlayerPair : Players)
		{
			if(!PlayerPair.second->IsLoaded_)
			{
				return;
			}
		}

		AllPlayerLoaded = true;

		//TODO: Update시작한다고 클라들한테 알리기
	}
}
#endif

#ifdef TSSERVER
void GameState::ProcessInput(Message* message)
{
	auto InputData = std::reinterpret_pointer_cast<Serializables::Input>(message->Data);
	UID ActorId = 0;
	if(Players.find(message->From) != Players.end())
	{
		ActorId = Players[message->From]->ActorId_;
	}
	else
		return;
	std::shared_ptr<Actor> TargetActor = nullptr;
	if(Actors.find(ActorId) != Actors.end())
	{
		TargetActor = Actors[ActorId];
		//Actors[ActorId]->OnInput(InputData->InputValue);
	}
	else
		return;

	// Input 실행 (CanAct가 true일 경우에만 실행한다.)
	//TODO: Client측에서도 처리해주어야 한다.
	if(TargetActor->GetCanAct())
		TargetActor->OnInput(*InputData);
}

// Player가 무기변경을 할 경우
void GameState::ProcessSwapWeapon(Message* message)
{
	// Player validity check
	if(Players.find(message->From) == Players.end())
		return;

	auto Data = std::reinterpret_pointer_cast<Serializables::SwapWeapon>(message->Data);

	// Actor id validity check
	UID ActorId = Players[message->From]->ActorId_;
	if(ActorId != Data->Id)
		return;

	// Actor validity check
	auto TargetActor = Actors.find(ActorId);
	if(TargetActor == Actors.end())
		return;

	// Swap 처리
	std::reinterpret_pointer_cast<Character>(TargetActor->second)->SwapWeapon(static_cast<WeaponType>(Data->SwapType));

	//TODO: 무기 변경에 성공 했다면 packet을 보낸다. 무기 변경을 할 수 없는 상황이라면 보내지 말아야 함.

	// 무기를 변경했음을 모든 client들에게 알린다.
	Serializables::ActWithValue SwapData;
	SwapData.Self = ActorId;
	SwapData.Type = ActType::WeaponSwap;
	SwapData.Value = Data->SwapType;
	BroadcastPacket(SwapData);

	LOG_INFO("Swap data sent.");
}

#endif


#ifdef TSSERVER
void GameState::ProcessSessionJoinReq(Message* message)
{
	if(Players.find(message->From) == Players.end())
		return;

	// Character 생성 ( 생성 위치중 한 곳에 )
	UID NewPlayerActorUID = InitPlayer(ActorType::Player, message->From);
	if(NewPlayerActorUID == INVALID_UID)
		return;

	auto JoinReq = std::reinterpret_pointer_cast<Serializables::SessionJoinReq>(message->Data);


	// 새로 들어온 Player에게 현재 Game의 상태를 저장한다.
	Serializables::SessionStarted ToNewPlayer;
	for(auto& PlayerPair : Players)
	{
		Serializables::AvatarInfo Info;
		Info.Player = *PlayerPair.second;

		ToNewPlayer.Players.Array.emplace_back(Info);
	}
	ToNewPlayer.Map = Map;
	ToNewPlayer.ServerUpdateTimestep = UpdateTimestep;

	// 현재 맵에 배치된 Actor들의 정보를 저장한다.
	for(auto& ActorPair : Actors)
	{
		// Map에 배치되는 Prop들은 보내지 않는다.
		if(ActorPair.second->GetType() != ActorType::Prop /*&& ActorPair.second->GetType() != ActorType::Player*/)
		{
			Serializables::ActorInfo Info;

			Info.Type = ActorPair.second->GetType();
			Info.ActorId = ActorPair.second->GetId();
			Info.Hp = ActorPair.second->GetHp();
			Info.Position = ActorPair.second->GetTransform().GetTranslation();

			ToNewPlayer.Actors.Array.emplace_back(Info);
		}
	}

	// 새로 들어온 Player에게 Packet을 보낸다.
	Network::Send(message->From, ToNewPlayer);

	// 기존에 있던 Player들에겐 새로 들어온 Player의 상태를 보내준다.
	Serializables::NewPlayer ToOldPlayers;

	ToOldPlayers.Player = *Players[message->From];

	Serializables::ActorInfo NewActorInfo;
	NewActorInfo.ActorId = NewPlayerActorUID;
	NewActorInfo.Hp = Actors[NewPlayerActorUID]->GetHp();
	NewActorInfo.Position = Actors[NewPlayerActorUID]->GetTransform().GetTranslation();
	NewActorInfo.Type = Actors[NewPlayerActorUID]->GetType();
	ToOldPlayers.NewActor = NewActorInfo;

	for(auto& PlayerPair : Players)
	{
		if(PlayerPair.second->ClientId_ != message->From)
			Network::Send(PlayerPair.second->ClientId_, ToOldPlayers);
	}
}
#endif

#ifdef TSSERVER
TheShift::Serializables::SessionStarted GameState::BuildSessionStarted() const
{
	Serializables::SessionStarted Data;
	Data.Map = Map;
	Data.ServerUpdateTimestep = UpdateTimestep;

	// Player정보 입력
	for(auto& PlayerPair : Players)
	{
		Serializables::AvatarInfo PlayerInfo;
		PlayerInfo.Player = *PlayerPair.second;
		Data.Players.Array.emplace_back(PlayerInfo);
	}

	// Actor정보 입력
	for(auto& ActorPair : Actors)
	{
		// 맵에 배치된 Prop들은 보내지 않는다. JSON에 어차피 다 저장되어 있음.
		// 생성되어있는 몬스터나 플레이어 액터만 보내주면 됨.
		auto Type = ActorPair.second->GetType();
		if(Type == ActorType::Prop
		   || Type == ActorType::Trigger)
			continue;
		Serializables::ActorInfo ObjectInfo;
		ObjectInfo.Type = ActorPair.second->GetType();
		ObjectInfo.Hp = ActorPair.second->GetHp();
		ObjectInfo.ActorId = ActorPair.second->GetId();
		auto Position = ActorPair.second->GetTransform().GetTranslation();
		ObjectInfo.Position = Vector3f(Position.x(), Position.y(), Position.z());
		Data.Actors.Array.emplace_back(ObjectInfo);
	}

	return Data;
}
#endif

/// <summary>
/// Player가 Session에 최초 접속 시 초기화를 해준다. 
/// </summary>
#ifdef TSSERVER
UID GameState::InitPlayer(ActorType Type, ClientId Id)
{
	//TODO: Player 초기화 작업
	return CreateCharacter(Type, Id);
}
#endif

TheShift::UID GameState::SpawnActor(ActorType Type, const Vector3f& Position)
{
	UID FreeUID = INVALID_UID;
	if (Type == ActorType::Prop)
		FreeUID = GetAvailableUID(true);
	else
		FreeUID = GetAvailableUID(false);

	if (FreeUID == INVALID_UID)
		return INVALID_UID;
	return CreateActor(Type, Position, FreeUID)->GetId();
}

TheShift::UID GameState::SpawnActor(ActorType Type, const Eigen::Vector3f& Position)
{
	UID FreeUID = INVALID_UID;
	if (Type == ActorType::Prop)
		FreeUID = GetAvailableUID(true);
	else
		FreeUID = GetAvailableUID(false);

	if (FreeUID == INVALID_UID)
		return INVALID_UID;
	return CreateActor(Type, Position, FreeUID)->GetId();
}

std::shared_ptr<Actor> GameState::CreateActor(ActorType Type, const Vector3f& Position, UID Id)
{
	std::shared_ptr<Actor> NewActor;

	switch(Type)
	{
	case ActorType::Player:
		NewActor = std::make_shared<Character>(Id);

		break;

	case ActorType::Prop:
	case ActorType::Interactable:
		NewActor = std::make_shared<Actor>(Id);
		break;

	case ActorType::Trigger:
		NewActor = std::make_shared<Trigger>(Id);
		break;

	case ActorType::KnightMonster:
		LOG_ERROR("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		NewActor = std::make_shared<KnightMonster>(Id);
		break;

		//case ActorType::OgerMonster:
		//	NewActor = std::make_shared<OgerMonster>(Id);
		//	break;

			// EX) Add case state for every actor type like so
			//case ActorType::"ActorType":
			//	NewActor = std::make_shared<"ActorType">(FreeUID);
			//	break;

	default:
		return nullptr;
	}

	// return INVALID_UID if NewActor is nullptr
	if(NewActor == nullptr)
		return nullptr;


	// Init BoundingVolumes
	// Tag가 없는 Actor들은 Init에서 초기화
	if (Type != ActorType::Prop
		&& Type != ActorType::Interactable
		&& Type != ActorType::Destructible
		&& Type != ActorType::Trigger)
	{
		// Is this actor Movable?
		NewActor->SetIsMovable(ActorStat[Type].IsMovable);
		CollisionResolver_->CreateBoundingVolumes(NewActor, ActorStat[Type].BoundInfo);
	}


	Actors.insert(std::make_pair(Id, NewActor));


	// Init Actor
	Actors[Id]->RegisterGameState(this);
	//#ifdef TSSERVER
	//	// Register func
	//	Actors[Id]->RegisterAddActFunc([this](Serializables::Act& act) {this->AddAct(act); });
	//#endif
		// Set Location
	Actors[Id]->SetTranslation(Position.X, Position.Y, Position.Z);
	Actors[Id]->SetLastTranslation();

	return NewActor;
}

std::shared_ptr<Actor> GameState::CreateActor(ActorType Type, const Eigen::Vector3f& Position, UID Id)
{
	if(Type != ActorType::Player)
	{
		LOG_ERROR("Error!!! Not supposed to create actor type other than player type.");
	}
	return CreateActor(Type, TheShift::Vector3f(Position.x(), Position.y(), Position.z()), Id);
}

/// <summary>
/// 시작 포인트들 중 한 곳에 Character를 생성시킨다.
/// </summary>
//TODO: Character의 Type에 따라 초기화 해준다. (무기 종류? 클래스 종류?)
#ifdef TSSERVER
UID GameState::CreateCharacter(ActorType Type, ClientId id)
{
	if(Players.find(id) == Players.end())
		return INVALID_UID;

	bool NoAvailablePoints = false;

	// Player시작위치를 불러온다.
	// 빈 자리를 찾고 불가능하다면 마지막 위치에 추가한다.
	int StartPointNum = PlacementInfo.count("Start Point");
	auto It = PlacementInfo.lower_bound("Start Point");

	// 가능한 Start point가 있으면
	if(StartPointIndices.count(INVALID_CLIENTID) != 0)
	{
		// Key가 INVALID_CLIENTID이면 비어있는 Start point
		auto AvailableStartPointNumPair = StartPointIndices.lower_bound(INVALID_CLIENTID);
		for(int i = 0; i < AvailableStartPointNumPair->second; ++i)
		{
			It++;
		}

		// 해당 Start point에 해당 Player가 spawn됐으므로 Key를 해당 Player의 Id로 바꿔준다.
		StartPointIndices.insert(std::make_pair(id, AvailableStartPointNumPair->second));
		StartPointIndices.erase(AvailableStartPointNumPair);
	}
	// 가능한 Start point가 없으면 그냥 뺑뺑이 돌린다.
	else
	{
		It = PlacementInfo.lower_bound("Start Point");
		// 1, 2, 3번 자리가 있으면 123123 이런식으로 캐릭터를 배치시킨다.
		int Max = (Players.size() % StartPointNum) - 1;
		for(int i = 0; i < Max; ++i)
		{
			It++;
		}
	}

	UID CharacterId = SpawnActor(ActorType::Player, It->second.ActorPosition);
	if(CharacterId == INVALID_UID)
	{
		//spdlog::error("INVALID_UID while creating character. PlayerId: {0}", id);
		LOG_ERROR("INVALID_UID while creating character. PlayerId: %d", id);
	}
	else
	{
		Players[id]->ActorId_ = CharacterId;
	}

	return CharacterId;
}
#endif

/// <summary>
/// Import한 Actor의 정보에 따라 Bounding volume을 초기화 시켜준다.
/// </summary>
bool GameState::SetBoundingVolume(std::shared_ptr<Actor> TargetActor)
{
	// Import한 정보를 읽는다.
	ActorType Type = TargetActor->GetType();
	if(ActorStat.find(Type) == ActorStat.end())
	{
		//spdlog::warn("Actor Type: {0}, Can't find Actor Stat from imported data", Type);
		LOG_WARNING("Actor Type: %d, Can't find Actor Stat from imported data", static_cast<int>(Type));
		return false;
	}

	ActorInfo Info = ActorStat[Type];

	// 해당 정보대로 초기화 한다.
	TargetActor->Init(Info);
	return true;
}

void GameState::DestroyActor(UID actorId)
{
	if(Actors.find(actorId) != Actors.end())
	{
		// Actor가 가지고 있던 Volume들 일괄 삭제
		for(auto VolumePointer : Actors[actorId]->GetBoundingVolumes())
		{
			CollisionResolver_->RemoveBoundingVolume(VolumePointer->GetId());
		}
		Actors.erase(actorId);
	}
}

void GameState::DestroyActor(std::shared_ptr<Actor> ActorToDestroy)
{
	DestroyActor(ActorToDestroy->GetId());
}

UID GameState::GetAvailableUID(bool Reverse/* = false*/)
{
	UID FreeUID = INVALID_UID;
	if (Reverse)
	{
		FreeUID = MAX_UID;
	}
	bool Found = false;
	while (!Found)
	{
		if (Reverse)
		{
			--FreeUID;
			if (FreeUID == INVALID_UID)
				return INVALID_UID;
		}
		else
		{
			++FreeUID;
			if (FreeUID >= MAX_UID)
				return INVALID_UID;
		}

		if (Actors.find(FreeUID) == Actors.end())
		{
			break;
		}

	}

	return FreeUID;
}
}

