﻿#pragma once

//#include "Common.h"
#include <chrono>
#include <vector>
#include <set>
#include "Protocol/Types.h"
#include "Protocol/Log.h"
#include "../../Vendor/include/Eigen/Core"
//#include <Eigen/Core>
#include <functional>

#include "Game/Components/Heightmap.h"
#include "Game/Components/BoundingVolume.h"

namespace TheShift
{
class GameState;
class Actor;

// SAP에서 최소 또는 최대값을 저장하면서 BoundingVolume의 ptr을 저장한다.
struct EndPoint
{
	int mData; // Owner box ID | MinMax flag
	float mValue; // Min or max value
};

// 액터 목록, 맵 정보 참조할 수 있어야 함.
class CollisionResolver
{
public:
	CollisionResolver(GameState* state);

	void ResolveCollisions(float deltaTime);
	template<class VolumeT>
	typename std::enable_if<std::is_base_of<BoundingVolume, VolumeT>::value,
		std::shared_ptr<VolumeT>>::type CreateTrigger();
	template<class VolumeT>
	typename std::enable_if<std::is_base_of<BoundingVolume, VolumeT>::value,
		std::shared_ptr<VolumeT>>::type CreateCollider();

	void CreateBoundingVolumes(std::shared_ptr<Actor> OwnerActor, std::vector<BoundingVolumeInfo>& BoundInfo);

	void RegisterTrigger(std::shared_ptr<BoundingVolume> NewTrigger);
	void RegisterCollider(std::shared_ptr<BoundingVolume> NewCollider);

	std::shared_ptr<BoundingVolume> GetBoundingVolume(UID Id);

	void RemoveBoundingVolume(UID Id);

	void Reset();

private:
	std::vector<std::shared_ptr<BoundingVolume>> VolumesToBeInitialized;
	std::map<UID, std::shared_ptr<BoundingVolume>> BoundingVolumes;

	// Volume의 Minimum 좌표값을 토대로 정렬시켜서 저장한다.
	// 여기에 저장되는 uint32_t값은 BoundingVolumes에 key이다.
	//std::vector<std::shared_ptr<BoundingVolume>> SortedByAxisX;
	//std::vector<std::shared_ptr<BoundingVolume>> SortedByAxisY;
	//std::vector<std::shared_ptr<BoundingVolume>> SortedByAxisZ;

	// 사이즈가 제일 작은 vector를 순회하면서 해당 set에 있는 원소를 두개씩 뽑아 
	// 다른 vector들이 이 두 원소를 가지고 있는 셋을 가지고 있는지 확인한다.
	//std::vector<std::set<BoundingVolume*>> SAPAxisXSets;
	//std::vector<std::set<BoundingVolume*>> SAPAxisYSets;
	//std::vector<std::set<BoundingVolume*>> SAPAxisZSets;

	//std::map<std::shared_ptr<BoundingVolume>, std::set<std::shared_ptr<BoundingVolume>>> SAPAxisXSets;
	//std::map<std::shared_ptr<BoundingVolume>, std::set<std::shared_ptr<BoundingVolume>>> SAPAxisYSets;
	//std::map<std::shared_ptr<BoundingVolume>, std::set<std::shared_ptr<BoundingVolume>>> SAPAxisZSets;
	// std::set<std::pair<std::shared_ptr<BoundingVolume>, std::shared_ptr<BoundingVolume>>> AxisXPairs;
	// std::set<std::pair<std::shared_ptr<BoundingVolume>, std::shared_ptr<BoundingVolume>>> AxisYPairs;
	// std::set<std::pair<std::shared_ptr<BoundingVolume>, std::shared_ptr<BoundingVolume>>> AxisZPairs;

	uint32_t IndexCount = 0;

	// 각 IndicesSortedByAxis
	//void RearrangeByAxisX();
	//void RearrangeByAxisY();
	//void RearrangeByAxisZ();

	// Transform이 변경될 때 child 볼륨들을 재정렬 하기 위해 이 함수를 부른다.
	void RearrangeVolume(BoundingVolume* TargetVolume);
	void RearrangeVolumeInAxis(int Axis, BoundingVolume* TargetVolume);

	float CalcHeightOfLandscape(const Eigen::Vector2f& Coord, std::shared_ptr<Heightmap> Map);
	float CalcHeightOfLandscape(const Eigen::Vector3f& Coord, std::shared_ptr<Heightmap> Map);
	float BarryCentric(const Eigen::Vector3f& Vert1, const Eigen::Vector3f& Vert2, const Eigen::Vector3f& Vert3, const Eigen::Vector2f& Coord);
	//float BarryCentric(const TheShift::Vector3f& Vert1, const TheShift::Vector3f& Vert2, const TheShift::Vector3f& const Vert3, const Vector2f& Coord);
	const std::shared_ptr<Heightmap> GetHeightmapCurrentlyOn(const Eigen::Vector3f& Position) const;
	void Resolve(std::shared_ptr<Actor> Target, float deltaTime);
	void ResolveHeightmap(std::shared_ptr<Actor> Target);
	void ResolveBoundingVolumes(float DeltaTime);
	void InitDerivedSpheres(std::vector<std::shared_ptr<BoundingSphere>>& Spheres, float HalfHeight, float Radius);

	UID GetAvailableUID();
private:
	GameState* State;
};

// Trigger를 생성한다.
// Trigger는 Overlapped된 Bounding volume들의 정보를 가지고 사용자가 정의한 
// OnTriggerEnter, OnTriggerLeave 함수를 실행하는데 사용된다.
template<typename VolumeT>
typename std::enable_if<std::is_base_of<BoundingVolume, VolumeT>::value,
	std::shared_ptr<VolumeT>>::type TheShift::CollisionResolver::CreateTrigger()
{
	std::shared_ptr<VolumeT> ReturnValue = std::make_shared<VolumeT>();
	RegisterTrigger(ReturnValue);
	VolumesToBeInitialized.emplace_back(ReturnValue);

#ifdef _DEBUG
	//if(std::is_same<AABB, VolumeT>::value)
	//	LOG_DEBUG("AABB Trigger created.");
	//else if(std::is_same<OBB, VolumeT>::value)
	//	LOG_DEBUG("OBB Trigger created.");
	//else if(std::is_same<BoundingCapsule, VolumeT>::value)
	//	LOG_DEBUG("Capsule Trigger created.");
	//else if(std::is_same<BoundingSphere, VolumeT>::value)
	//	LOG_DEBUG("Sphere Trigger created.");
#endif // DEBUG

#ifdef TSCLIENT
	//if(std::is_same<AABB, VolumeT>::value)
	//{
	//	LOG_DEBUG("AABB Trigger created.");
	//}
	//else if(std::is_same<OBB, VolumeT>::value)
	//{
	//	LOG_DEBUG("OBB Trigger created.");
	//}
	//else if(std::is_same<BoundingCapsule, VolumeT>::value)
	//{
	//	LOG_DEBUG("Capsule Trigger created.");
	//}
	//else if(std::is_same<BoundingSphere, VolumeT>::value)
	//{
	//	LOG_DEBUG("Sphere Trigger created.");
	//}
#endif

	return ReturnValue;
}

// Collider를 생성한다.
// Collider는 Actor들의 물리적 충돌 처리에 사용된다. 
// 생성한 후 Parent와 Extent등 을 초기화해야 한다.
template<typename VolumeT>
typename std::enable_if<std::is_base_of<BoundingVolume, VolumeT>::value,
	std::shared_ptr<VolumeT>>::type TheShift::CollisionResolver::CreateCollider()
{
	std::shared_ptr<VolumeT> ReturnValue = std::make_shared<VolumeT>();
	RegisterCollider(ReturnValue);
	VolumesToBeInitialized.emplace_back(ReturnValue);

#ifdef _DEBUG
	//if(std::is_same<AABB, VolumeT>::value)
	//	LOG_DEBUG("AABB Collider created.");
	//else if(std::is_same<OBB, VolumeT>::value)
	//	LOG_DEBUG("OBB Collider created.");
	//else if(std::is_same<BoundingCapsule, VolumeT>::value)
	//	LOG_DEBUG("Capsule Collider created.");
	//else if(std::is_same<BoundingSphere, VolumeT>::value)
	//	LOG_DEBUG("Sphere Collider created.");
#endif // DEBUG

#ifdef TSCLIENT
	//if(std::is_same<AABB, VolumeT>::value)
	//{
	//	LOG_DEBUG("AABB Collider created.");
	//}
	//else if(std::is_same<OBB, VolumeT>::value)
	//{
	//	LOG_DEBUG("OBB Collider created.");
	//}
	//else if(std::is_same<BoundingCapsule, VolumeT>::value)
	//{
	//	LOG_DEBUG("Capsule Collider created.");
	//}
	//else if(std::is_same<BoundingSphere, VolumeT>::value)
	//{
	//	LOG_DEBUG("Sphere Collider created.");
	//}
#endif

	return ReturnValue;
}
}

