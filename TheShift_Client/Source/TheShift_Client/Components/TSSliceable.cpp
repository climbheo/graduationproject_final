﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TSSliceable.h"
#include "Components/SkeletalMeshComponent.h"
#include "Engine/StaticMesh.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SkinnedMeshComponent.h"
#include "Rendering/SkeletalMeshRenderData.h"
#include "GeomTools.h"
#include "DrawDebugHelpers.h"
#include "Components/PoseableMeshComponent.h"
#include "SkeletalRenderPublic.h"
#include "RenderingThread.h"
#include "SkinWeightVertexBuffer.h"
#include "RenderResource.h"
#include "SkeletalMeshMerge.h"
#include "TheShift_ClientGameModeBase.h"

bool operator==(const FFinalSkinVertex& a, const FFinalSkinVertex& b)
{

	return a.Position.Equals(b.Position);
}

bool operator==(const TPair<FFinalSkinVertex, FFinalSkinVertex>& a, const TPair<FFinalSkinVertex, FFinalSkinVertex>& b)
{
	return (a.Key == b.Key) && (a.Value == b.Value);
}



// Sets default values for this component's properties
UTSSliceable::UTSSliceable()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTSSliceable::BeginPlay()
{
	Super::BeginPlay();

	// ...

}


// Called every frame
void UTSSliceable::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}
//
void UTSSliceable::SliceSkeletalMesh(USkeletalMeshComponent* pSkeletalMesh, FVector vPlanePos, FVector vPlaneNormal, FVector vPlaneTangent, UMaterialInterface* pCapMaterial)
{
	if(IsValid(pSkeletalMesh))
	{
		FPlane slicePlane(vPlanePos, vPlaneNormal);

		auto RenderData = pSkeletalMesh->GetSkeletalMeshRenderData();

		auto& bindPoseData = RenderData->LODRenderData.Last(RenderData->LODRenderData.Num() - 1);

		//LOD 0 T-Pose PositionBuffer
		auto& originPositionVertexBuffer = bindPoseData.StaticVertexBuffers.PositionVertexBuffer;
		//LOD 0 T-Pose IndexBuffer
		auto& originIndexVertexBuffer = bindPoseData.MultiSizeIndexContainer;
		//LOD 0 T-Pose TangentAndUVBuffer
		auto& originUVAndTangentBuffer = bindPoseData.StaticVertexBuffers.StaticMeshVertexBuffer;
		//LOD 0 T-Pose SkinWeightBuffer
		auto& originSkinWeightBuffer = bindPoseData.SkinWeightVertexBuffer;

		ATheShift_ClientGameModeBase* gameMode = Cast<ATheShift_ClientGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
		TArray<uint32>* IndexArray;
		UE_LOG(LogTemp, Log, TEXT("Mesh Name : %s"), *pSkeletalMesh->SkeletalMesh->GetName());

		if(gameMode)
			IndexArray = gameMode->GetSlicingMeshIndexes(pSkeletalMesh->SkeletalMesh->GetName());
		else
			return;

		if(!IndexArray)
		{
			UE_LOG(LogTemp, Log, TEXT("There is no IndexArray for SlicingSkeletalMesh"));
			return;
		}
		TArray<FFinalSkinVertex> originSkinnedVertices;
		originSkinnedVertices.Reserve(bindPoseData.GetNumVertices());
		for(int i = 0; i < 2; ++i)
		{
			newSlicedVertexes[i].Reserve(bindPoseData.GetNumVertices());
			newSkinWeightArray[i].Reserve(bindPoseData.GetNumVertices());
			newIndexArray[i].Reserve(bindPoseData.GetNumVertices() * 2);
		}
		pSkeletalMesh->GetCPUSkinnedVertices(originSkinnedVertices, 0);
		auto pActor = pSkeletalMesh->GetOwner();
		auto ActorMatrix = pSkeletalMesh->GetComponentTransform().ToMatrixWithScale();

		//교차를 검사한다.
		for(int i = 0; i < (IndexArray->Num() / 3); ++i)
		{
			FVector Positions[3];
			bool isCheck[3] = { false, false, false };
			bool isPositive[3] = { false, false, false };

			for(int j = 0; j < 3; ++j)
				Positions[j] = ActorMatrix.TransformPosition(originSkinnedVertices[(*IndexArray)[i * 3 + j]].Position);
			FVector directionVec[3];
			float	denominator[3];
			float	multiplier[3];
			FVector betweenVec[3];
			FVector intersectionPoint[3];
			directionVec[0] = Positions[0] - Positions[1];
			directionVec[1] = Positions[1] - Positions[2];
			directionVec[2] = Positions[2] - Positions[0];

			//실질적 교차 검사코드
			for(int j = 0; j < 3; ++j)
			{
				directionVec[j].Normalize();
				denominator[j] = FVector::DotProduct(vPlaneNormal, directionVec[j]);
				betweenVec[j] = vPlanePos - Positions[j];
				multiplier[j] = FVector::DotProduct(betweenVec[j], vPlaneNormal) / denominator[j];
				intersectionPoint[j] = Positions[j] + directionVec[j] * multiplier[j];
				FVector A, B;
				if(j != 2)
					A = Positions[j + 1] - Positions[j];
				else
					A = Positions[0] - Positions[2];
				B = intersectionPoint[j] - Positions[j];
				if(FVector::DotProduct(A, B) > 0 && A.Size() >= B.Size())
					isCheck[j] = true;

				//각 정점이 양, 음의방향인지 검사
				if(slicePlane.PlaneDot(Positions[j]) >= 0.f)
					isPositive[j] = true;
				else
					isPositive[j] = false;

			}
			//


	//삼각형의 어느 선분도 교차되지 않았다면 이 삼각형을 구성하는 정점은 모두 DIR_POSITIVE or DIR_NEGATIVE
			if(!isCheck[0] && !isCheck[1] && !isCheck[2])
			{
				bool isAlreadyInSet[DIR_END][3] = { false, false, false, false, false, false };
				//먼저 교차점이아닌 원래 존재했던 정점들이 이미 각 Set에 들어가있는 인덱스인지 체크한다.
				for(int j = 0; j < 3; ++j)
				{
					if(checkIndexMap[DIR_POSITIVE].Find((*IndexArray)[i * 3 + j]))
						isAlreadyInSet[DIR_POSITIVE][j] = true;
					if(checkIndexMap[DIR_NEGATIVE].Find((*IndexArray)[i * 3 + j]))
						isAlreadyInSet[DIR_NEGATIVE][j] = true;
				}

				//한 점이라도 양의 방향이면 모두 양의 방향
				if(isPositive[0] || isPositive[1] || isPositive[2])
				{
					for(int j = 0; j < 3; ++j)
					{
						//정점 인덱스가 아직 할당이안된놈이면 정점정보,인덱스 모두 채운다.
						//그렇지 않으면 인덱스만 채운다.
						if(!isAlreadyInSet[DIR_POSITIVE][j])
						{
							int newIndex = newSlicedVertexes[DIR::DIR_POSITIVE].Emplace(originSkinnedVertices[(*IndexArray)[i * 3 + j]]);
							newSlicedVertexes[DIR::DIR_POSITIVE][newIndex].Position = ActorMatrix.TransformPosition(newSlicedVertexes[DIR::DIR_POSITIVE][newIndex].Position);
							newIndexArray[DIR_POSITIVE].Emplace(newIndex);
							checkIndexMap[DIR_POSITIVE].Add((*IndexArray)[i * 3 + j], newIndex);
							newSkinWeightArray[DIR_POSITIVE].Emplace(originSkinWeightBuffer.GetSkinWeightPtr<false>((*IndexArray)[i * 3 + j]));
						}
						else
							newIndexArray[DIR_POSITIVE].Emplace(checkIndexMap[DIR::DIR_POSITIVE][(*IndexArray)[i * 3 + j]]);
					}
				}
				//그렇지 않다면 모두 음의 방향
				else
				{
					for(int j = 0; j < 3; ++j)
					{
						if(!isAlreadyInSet[DIR_NEGATIVE][j])
						{
							int newIndex = newSlicedVertexes[DIR::DIR_NEGATIVE].Emplace(originSkinnedVertices[(*IndexArray)[i * 3 + j]]);
							newSlicedVertexes[DIR::DIR_NEGATIVE][newIndex].Position = ActorMatrix.TransformPosition(newSlicedVertexes[DIR::DIR_NEGATIVE][newIndex].Position);
							newIndexArray[DIR_NEGATIVE].Emplace(newIndex);
							checkIndexMap[DIR_NEGATIVE].Add((*IndexArray)[i * 3 + j], newIndex);
							newSkinWeightArray[DIR_NEGATIVE].Emplace(originSkinWeightBuffer.GetSkinWeightPtr<false>((*IndexArray)[i * 3 + j]));
						}
						else
							newIndexArray[DIR_NEGATIVE].Emplace(checkIndexMap[DIR::DIR_NEGATIVE][(*IndexArray)[i * 3 + j]]);
					}
				}

			}
			//그렇지 않다면 교차 케이스에 대한 처리를 한다.
			else
			{
				//평면이 삼각형에 교차되는 케이스는 세가지가있음.
				//각 케이스에 따라 새로 생성되는 삼각형들의 정점위치정보, 정점의 UV, Tangent, SkingWeight를 저장해둔다.
				//함수로 넘겨줘야 할 정보, 교차점, 현재 인덱스, 각 정점의 교차 여부, 각 정점의 양, 음의방향 여부
				//스키닝된 정점 정보, 인덱스 정보, UV&Tangent정보, 본 가중치 정보
				FMatrix identity = FMatrix::Identity;
				SliceAndTriangulate(ActorMatrix, intersectionPoint, i, isCheck, isPositive,
									originSkinnedVertices, (*IndexArray), originSkinWeightBuffer);
			}
		}
		pSlicedActors[0] = GetWorld()->SpawnActor<AActor>();
		pSlicedActors[1] = GetWorld()->SpawnActor<AActor>();
		//UE_LOG(LogTemp, Log, TEXT("ID : %d"),FPlatformTLS::GetCurrentThreadId());
		for(int i = 0; i < newIntersectionPoints.Num(); ++i)
		{
			for(int j = i + 1; j < newIntersectionPoints.Num(); ++j)
			{
				if(newIntersectionPoints[i] == newIntersectionPoints[j])
				{
					newIntersectionPoints.RemoveAt(j);
					j--;
					continue;
				}
			}
		}

		while(newIntersectionPoints.Num() != 0)
		{
			auto curVertex = newIntersectionPoints.Pop();
			if(capQueue.empty())
			{
				deque<FFinalSkinVertex> firstQueue;
				firstQueue.emplace_back(curVertex.Key);
				firstQueue.emplace_back(curVertex.Value);
				capQueue.emplace_back(firstQueue);
			}
			else
			{
				bool isChained = false;
				for(auto& existQueue : capQueue)
				{
					//만약 이번에 넣으려는 Edge의 시작점이(Key) 원래 존재하는 Edge의 끝점(back)과 같다면?-> 연결
					if(existQueue.back() == (curVertex.Key))
					{
						existQueue.emplace_back(curVertex.Value);
						isChained = true;
						break;
					}
					//아니면 이번에 넣으려는Edge의 끝점이(Value) 원래 존재하는 Edge의 시작점(front)와 같다면?->연결)
					else if(existQueue.front() == (curVertex.Value))
					{
						existQueue.emplace_front(curVertex.Key);
						isChained = true;
						break;
					}

				}

				//아무 Edge하고도 연결되지 않는다면 그냥 queue에 넣는다
				if(!isChained)
				{
					deque<FFinalSkinVertex> tempQueue;
					tempQueue.emplace_back(curVertex.Key);
					tempQueue.emplace_back(curVertex.Value);
					capQueue.emplace_back(tempQueue);
				}
				//하나라도 연결되었다면 덱끼리 서로 비교해 서로의 첫점, 끝점관계를 분석해 붙여야함.
				//여러 케이스가있음
				else
				{
					bool isMergeCompleted = false;
					if(capQueue.size() > 1)
					{
						/*do
						{
							bool isCheck = false;*/
						for(int index = 0; index < capQueue.size(); ++index)
						{
							for(int index_2 = index + 1; index_2 < capQueue.size(); ++index_2)
							{
								if(capQueue[index].front() == (capQueue[index_2].back()))
								{
									for(int i = capQueue[index_2].size() - 2; i >= 0; --i)
										capQueue[index].emplace_front(capQueue[index_2][i]);

									capQueue.erase(capQueue.begin() + index_2);
									//index_2--;
									//isCheck = true;
									index = -1;
									break;
								}
								//만약 이번 덱의 끝점이 비교하는 덱의 시작과 같다면 연결
								else if(capQueue[index].back() == (capQueue[index_2].front()))
								{
									for(int i = 1; i < capQueue[index_2].size(); ++i)
										capQueue[index].emplace_back(capQueue[index_2][i]);

									capQueue.erase(capQueue.begin() + index_2);
									//index_2--;
									//isCheck = true;
									index = -1;
									break;
								}
								//만약 이번 덱과 비교하는 덱의 시작점끼리 같다면 연결
								else if(capQueue[index].front() == (capQueue[index_2].front()))
								{
									for(int i = 1; i < capQueue[index_2].size(); ++i)
										capQueue[index].emplace_front(capQueue[index_2][i]);

									capQueue.erase(capQueue.begin() + index_2);
									//index_2--;
									//isCheck = true;
									index = -1;
									break;
								}
								else if(capQueue[index].back() == (capQueue[index_2].back()))
								{
									for(int i = capQueue[index_2].size() - 2; i >= 0; --i)
										capQueue[index].emplace_back(capQueue[index_2][i]);

									capQueue.erase(capQueue.begin() + index_2);
									//index_2--;
									//isCheck = true;
									index = -1;
									break;
								}
							}
						}
						/*	if (!isCheck)
								isMergeCompleted = true;

						} while (!isMergeCompleted);*/
					}
				}
			}
		}


		TArray<TArray<int32>>	capIndexes;
		TArray<TArray<int32>>	capIndexes_p;
		FVector capNormal;


		if(capQueue.size() > 0)
		{
			for(int i = 0; i < capQueue.size(); ++i)
			{

				TArray<int32> newIndexes;
				TArray<int32> newIndexes_p;
				newIndexes.Reserve(capQueue[i].size() * 2);
				newIndexes_p.Reserve(capQueue[i].size() * 2);

				for(int index = 0; index < capQueue[i].size() - 2; ++index)
				{
					bool isFlip = false;


					FPlane curcapPlane(capQueue[i][0].Position, capQueue[i][index + 1].Position, capQueue[i][index + 2].Position);
					capNormal = curcapPlane;

					if(FVector::DotProduct(capNormal, vPlaneNormal) < 0.f)
						isFlip = true;

					//IndexBuffer
					if(!isFlip)
					{
						newIndexes_p.Emplace(0);
						newIndexes_p.Emplace(index + 1);
						newIndexes_p.Emplace(index + 2);

						newIndexes.Emplace(index + 2);
						newIndexes.Emplace(index + 1);
						newIndexes.Emplace(0);
					}
					else
					{
						newIndexes.Emplace(0);
						newIndexes.Emplace(index + 1);
						newIndexes.Emplace(index + 2);

						newIndexes_p.Emplace(index + 2);
						newIndexes_p.Emplace(index + 1);
						newIndexes_p.Emplace(0);
					}
				}
				capIndexes.Emplace(newIndexes);
				capIndexes_p.Emplace(newIndexes_p);
			}

		}


		newCapPosition.Reserve(capQueue.size());
		for(int i = 0; i < 2; ++i)
		{
			newCapNormal[i].Reserve(capQueue.size());
			newCapTangent[i].Reserve(capQueue.size());
		}
		newCapUV.Reserve(capQueue.size());

		FVector U = FVector::VectorPlaneProject(-vPlaneTangent, vPlaneNormal);
		FVector V = FVector::CrossProduct(U, vPlaneNormal);

		V.Normalize();

		FVector BasisX, BasisY, BasisZ;
		BasisZ = slicePlane;
		BasisZ.FindBestAxisVectors(BasisX, BasisY);
		FMatrix ToWorld = FMatrix(BasisX, BasisY, slicePlane, BasisZ * slicePlane.W);

		for(int i = 0; i < capQueue.size(); ++i)
		{
			for(auto& vertex : capQueue[i])
			{
				FVector P = ToWorld.InverseTransformPosition(vertex.Position);

				vertex.U = P.X / 64.f;
				vertex.V = P.Y / 64.f;

				/*	 vertex.U = FVector::DotProduct(vertex.Position, U);
					 vertex.V = FVector::DotProduct(vertex.Position, V);
					 vertex.U = vertex.U - (int)vertex.U;
					 vertex.V = vertex.V - (int)vertex.V;*/

			}
		}

		for(int i = 0; i < 2; ++i)
		{
			for(int capIndex = 0; capIndex < capQueue.size(); ++capIndex)
			{
				TArray<FVector> newPosition;
				TArray<FVector> newNormal;
				TArray<FProcMeshTangent> newTangent;
				TArray<FVector2D> newUV;
				if(i == 0)
					newPosition.Reserve(capQueue[capIndex].size());
				newNormal.Reserve(capQueue[capIndex].size());
				if(i == 0)
					newUV.Reserve(capQueue[capIndex].size());

				newTangent.Reserve(capQueue[capIndex].size());

				for(auto& finalSkinVertex : capQueue[capIndex])
				{
					if(i == 0)
						newPosition.Emplace(finalSkinVertex.Position);

					//For Negative
					if(i == 1)
						newNormal.Emplace(vPlaneNormal);
					//For Positive
					else
						newNormal.Emplace(-vPlaneNormal);
					//For Negative
					if(i == 1)
						newTangent.Emplace(FProcMeshTangent(vPlaneTangent, false));
					//For Positive
					else
						newTangent.Emplace(FProcMeshTangent(-vPlaneTangent, false));
					if(i == 0)
						newUV.Emplace(FVector2D(finalSkinVertex.U, finalSkinVertex.V));
				}
				if(i == 0)
					newCapPosition.Emplace(newPosition);

				//For Negative
				if(i == 1)
					newCapNormal[1].Emplace(newNormal);
				//For Positive
				else
					newCapNormal[0].Emplace(newNormal);
				//For Negative
				if(i == 1)
					newCapTangent[1].Emplace(newTangent);
				//For Positive
				else
					newCapTangent[0].Emplace(newTangent);
				if(i == 0)
					newCapUV.Emplace(newUV);

			}
		}


		//

		TArray<FVector> newPosition;
		TArray<FVector> newNormal;
		TArray<FProcMeshTangent> newTangent;
		TArray<FVector2D> newUV;
		TArray<FColor> newColor;

		for(int dir = 0; dir < 2; ++dir)
		{
			newPosition.Empty();
			newNormal.Empty();
			newTangent.Empty();
			newUV.Empty();

			newPosition.Reserve(newSlicedVertexes[dir].Num());
			newNormal.Reserve(newSlicedVertexes[dir].Num());
			newUV.Reserve(newSlicedVertexes[dir].Num());
			newTangent.Reserve(newSlicedVertexes[dir].Num());

			FVector normalizedNormal;
			FVector normalizedTangent;
			for(int index = 0; index < newSlicedVertexes[dir].Num(); ++index)
			{
				newPosition.Emplace(newSlicedVertexes[dir][index].Position);
				normalizedNormal = newSlicedVertexes[dir][index].TangentZ.ToFVector();
				normalizedNormal.Normalize();
				normalizedTangent = newSlicedVertexes[dir][index].TangentX.ToFVector();
				normalizedTangent.Normalize();
				newNormal.Emplace(normalizedNormal);
				newTangent.Emplace(FProcMeshTangent(normalizedTangent, false));
				newUV.Emplace(newSlicedVertexes[dir][index].U, newSlicedVertexes[dir][index].V);
			}

			pSlicedActors[dir]->SetActorEnableCollision(true);
			pSlicedActors[dir]->SetActorLocation(GetOwner()->GetActorLocation());
			UProceduralMeshComponent* pNewProceduralMesh = NewObject<UProceduralMeshComponent>(pSlicedActors[dir]);
			pSlicedActors[dir]->SetRootComponent(pNewProceduralMesh);


			pNewProceduralMesh->SetCollisionProfileName("SlicedMesh");
			pNewProceduralMesh->bUseComplexAsSimpleCollision = false;
			pNewProceduralMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
			pNewProceduralMesh->CreateMeshSection(0, newPosition, newIndexArray[dir], newNormal, newUV, newColor, newTangent, false);
			pNewProceduralMesh->SetSimulatePhysics(true);
			pNewProceduralMesh->SetMaterial(0, pSkeletalMesh->GetMaterial(0));
			pNewProceduralMesh->SetVisibility(true);
			pNewProceduralMesh->Activate(true);

			//cap
			for(int capIndex = 0; capIndex < capQueue.size(); ++capIndex)
			{
				if(dir == 1)
					pNewProceduralMesh->CreateMeshSection(capIndex + 1, newCapPosition[capIndex], capIndexes[capIndex], newCapNormal[1][capIndex], newCapUV[capIndex], newColor, newCapTangent[1][capIndex], false);
				else
					pNewProceduralMesh->CreateMeshSection(capIndex + 1, newCapPosition[capIndex], capIndexes_p[capIndex], newCapNormal[0][capIndex], newCapUV[capIndex], newColor, newCapTangent[0][capIndex], false);

				pNewProceduralMesh->SetMaterial(capIndex + 1, pCapMaterial);
			}

			FBox localBox = pNewProceduralMesh->GetProcMeshSection(0)->SectionLocalBox;

			TArray<FVector> localBoxPoints;
			localBoxPoints.Reserve(8);
			localBoxPoints.Emplace(localBox.Min.X, localBox.Min.Y, localBox.Min.Z);
			localBoxPoints.Emplace(localBox.Min.X, localBox.Max.Y, localBox.Min.Z);
			localBoxPoints.Emplace(localBox.Max.X, localBox.Min.Y, localBox.Min.Z);
			localBoxPoints.Emplace(localBox.Max.X, localBox.Max.Y, localBox.Min.Z);

			localBoxPoints.Emplace(localBox.Min.X, localBox.Min.Y, localBox.Max.Z);
			localBoxPoints.Emplace(localBox.Min.X, localBox.Max.Y, localBox.Max.Z);
			localBoxPoints.Emplace(localBox.Max.X, localBox.Min.Y, localBox.Max.Z);
			localBoxPoints.Emplace(localBox.Max.X, localBox.Max.Y, localBox.Max.Z);

			pNewProceduralMesh->AddCollisionConvexMesh(localBoxPoints);
			pNewProceduralMesh->RegisterComponentWithWorld(GetWorld());
			pNewProceduralMesh->AddRadialImpulse(vPlanePos, 500.f, 500.f, ERadialImpulseFalloff::RIF_Linear, true);

		}
	}
}

void UTSSliceable::SliceAndTriangulate(FMatrix& actorMatrix, FVector* pIntersectionPoints, uint32 curIndex, bool* pIsIntersected, bool* pIsPositiveForPlane, TArray<FFinalSkinVertex>& skinnedVertexes, TArray<uint32>& indexes, FSkinWeightVertexBuffer& skinWeights)
{
	//삼각형이 교차되는 케이스는 세 케이스임
	//각 케이스 안에서 어떤쪽이 양,음의방향인지도 구분해야한다.
	//이때 각 양, 음의 방향에 추가되는 정점의 인덱스는 어떤식으로 쌓을 것인가
	//제일 간단하게 한다면 이미 Set에 한번 넣었던 정점인덱스라면 인덱스 배열에 순회할 인덱스만 추가한다.
	//Set에 넣지 않은 정점인덱스라면 새로운 인덱스와 정점정보를 채워준다.

	bool isAlreadyInSet[DIR_END][3] = { false, false, false, false, false, false };

	//먼저 교차점이아닌 원래 존재했던 정점들이 이미 각 Set에 들어가있는 인덱스인지 체크한다.
	for(int i = 0; i < 3; ++i)
	{
		if(checkIndexMap[DIR_POSITIVE].Find(indexes[curIndex * 3 + i]))
			isAlreadyInSet[DIR_POSITIVE][i] = true;
		if(checkIndexMap[DIR_NEGATIVE].Find(indexes[curIndex * 3 + i]))
			isAlreadyInSet[DIR_NEGATIVE][i] = true;
	}

	//인덱스 1이 tip triangle일때 -> 0,1번째 선분이 교차할 때
	if(pIsIntersected[0] && pIsIntersected[1])
	{
		//tip triangle이 +방향이면 tip쪽의 한 정점과 두 교차점을 positive방향에 담는다.
		if(pIsPositiveForPlane[1])
		{
			//DIR_POSITIVE_Start
			// tip 삼각형을 그리는 나머지 두 교차점을 순서대로 배열에 넣어준다.

			// 새로운 정점은 교차점의 위치정보를 빼고는 tip 정점과 모든 데이터가 동일하다.
			// 또한 새로운 정점들은 무조건 인덱스가 새로 생성된다.(무조건 정점의 정보를 채워줘야하는가?)
			// 아니다! 교차점을 공유하는 삼각형이 있다면 이 또한 문제가 된다.
			// 이러한 인덱스와 정점정보 만큼의 메모리 낭비를 허용할것인가? => 허용해도 상관은 없지만 메모리의 낭비가 커진다..
			// 인덱스의 낭비를 허용하지 않는 방법은 새로운 정점 정보가 정점에 있는지를 체크하는 것.
			// 일일히 정점들의 컨테이너를 순회하면서 정점정보가 있는지 순회하는 것은 낭비. 그냥 일단 허용해보자.
			// 속도가 느리다면 여기서 채워지는 Array들의 reserve를 호출하는 것을 고려할 것.
			FFinalSkinVertex newVertex;
			memcpy(&newVertex, &skinnedVertexes[indexes[curIndex * 3 + 1]], sizeof(FFinalSkinVertex));
			newVertex.Position = pIntersectionPoints[0];

			//3
			// 정점을 넣을때 반환되는 리턴값은 새로운 정점의 인덱스
			uint32 newIndex = newSlicedVertexes[DIR::DIR_POSITIVE].Emplace(newVertex);
			// 정점의 새로운 인덱스를 인덱스배열에 넣어준다.
			newIndexArray[DIR_POSITIVE].Emplace(newIndex);
			// 정점의 스킨정보를 받아 넣어준다.
			newSkinWeightArray[DIR_POSITIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3 + 1]));
			TPair<FFinalSkinVertex, FFinalSkinVertex> curEdge;
			curEdge.Key = newVertex;

			//tip(1) -> 만약 원래 있던 정점이 아직 Set에 들어가지 않았다면 새로 정점정보와 인덱스를 채워준다.
			//			그렇지 않다면 정점정보는 채우지 않고 인덱스만 채워준다. 이때 인덱스는 이미 추가되어있던 이 정점의 인덱스는 어떻게 얻을수 있는가?
			if(!isAlreadyInSet[DIR::DIR_POSITIVE][1])
			{
				newIndex = newSlicedVertexes[DIR::DIR_POSITIVE].Emplace(skinnedVertexes[indexes[curIndex * 3 + 1]]);
				newSlicedVertexes[DIR::DIR_POSITIVE][newIndex].Position = actorMatrix.TransformPosition(newSlicedVertexes[DIR::DIR_POSITIVE][newIndex].Position);
				newIndexArray[DIR_POSITIVE].Emplace(newIndex);
				checkIndexMap[DIR_POSITIVE].Add(indexes[curIndex * 3 + 1], newIndex);
				newSkinWeightArray[DIR_POSITIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3 + 1]));
			}
			else
				newIndexArray[DIR_POSITIVE].Emplace(checkIndexMap[DIR::DIR_POSITIVE][indexes[curIndex * 3 + 1]]);
			//4
			newVertex.Position = pIntersectionPoints[1];
			newIndex = newSlicedVertexes[DIR::DIR_POSITIVE].Emplace(newVertex);
			newIndexArray[DIR_POSITIVE].Emplace(newIndex);
			newSkinWeightArray[DIR_POSITIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3 + 1]));
			curEdge.Value = newVertex;
			newIntersectionPoints.Emplace(curEdge);
			// 이 모든 값들은 반드시 삼각형을 그리는 순서대로 (시계방향으로) 쌓여야만 한다.

			//DIR_POSITIVE_End
			//DIR_NEGATIVE_Start

			//0
			if(!isAlreadyInSet[DIR::DIR_NEGATIVE][0])
			{
				newIndex = newSlicedVertexes[DIR::DIR_NEGATIVE].Emplace(skinnedVertexes[indexes[curIndex * 3]]);
				newSlicedVertexes[DIR::DIR_NEGATIVE][newIndex].Position = actorMatrix.TransformPosition(newSlicedVertexes[DIR::DIR_NEGATIVE][newIndex].Position);
				newIndexArray[DIR_NEGATIVE].Emplace(newIndex);
				checkIndexMap[DIR_NEGATIVE].Add(indexes[curIndex * 3], newIndex);
				newSkinWeightArray[DIR_NEGATIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3]));
			}
			else
				newIndexArray[DIR_NEGATIVE].Emplace(checkIndexMap[DIR_NEGATIVE][indexes[curIndex * 3]]);

			//5
			int intersectionIndex = 0;
			memcpy(&newVertex, &skinnedVertexes[indexes[curIndex * 3]], sizeof(FFinalSkinVertex));
			newVertex.Position = pIntersectionPoints[0];
			newIndex = newSlicedVertexes[DIR::DIR_NEGATIVE].Emplace(newVertex);
			intersectionIndex = newIndex;
			newIndexArray[DIR_NEGATIVE].Emplace(newIndex);
			newSkinWeightArray[DIR_NEGATIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3]));

			//2
			if(!isAlreadyInSet[DIR::DIR_NEGATIVE][2])
			{
				newIndex = newSlicedVertexes[DIR::DIR_NEGATIVE].Emplace(skinnedVertexes[indexes[curIndex * 3 + 2]]);
				newSlicedVertexes[DIR::DIR_NEGATIVE][newIndex].Position = actorMatrix.TransformPosition(newSlicedVertexes[DIR::DIR_NEGATIVE][newIndex].Position);
				newIndexArray[DIR_NEGATIVE].Emplace(newIndex);
				checkIndexMap[DIR_NEGATIVE].Add(indexes[curIndex * 3 + 2], newIndex);
				newSkinWeightArray[DIR_NEGATIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3 + 2]));
			}
			else
				newIndexArray[DIR_NEGATIVE].Emplace(checkIndexMap[DIR_NEGATIVE][indexes[curIndex * 3 + 2]]);
			//5
			newIndexArray[DIR_NEGATIVE].Emplace(intersectionIndex);
			//6
			memcpy(&newVertex, &skinnedVertexes[indexes[curIndex * 3 + 2]], sizeof(FFinalSkinVertex));
			newVertex.Position = pIntersectionPoints[1];
			newIndex = newSlicedVertexes[DIR::DIR_NEGATIVE].Emplace(newVertex);
			newIndexArray[DIR_NEGATIVE].Emplace(newIndex);
			newSkinWeightArray[DIR_NEGATIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3 + 2]));

			//2
			newIndexArray[DIR_NEGATIVE].Emplace(checkIndexMap[DIR_NEGATIVE][indexes[curIndex * 3 + 2]]);

		}
		else
			//그렇지 않다면 나머지 두 정점을 positive방향에 담는다.
		{
			FFinalSkinVertex newVertex;
			memcpy(&newVertex, &skinnedVertexes[indexes[curIndex * 3 + 1]], sizeof(FFinalSkinVertex));
			newVertex.Position = pIntersectionPoints[0];

			//3
			uint32 newIndex = newSlicedVertexes[DIR::DIR_NEGATIVE].Emplace(newVertex);
			newIndexArray[DIR_NEGATIVE].Emplace(newIndex);
			newSkinWeightArray[DIR_NEGATIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3 + 1]));
			TPair<FFinalSkinVertex, FFinalSkinVertex> curEdge;
			curEdge.Key = newVertex;

			//tip(1) 
			if(!isAlreadyInSet[DIR::DIR_NEGATIVE][1])
			{
				newIndex = newSlicedVertexes[DIR::DIR_NEGATIVE].Emplace(skinnedVertexes[indexes[curIndex * 3 + 1]]);
				newSlicedVertexes[DIR::DIR_NEGATIVE][newIndex].Position = actorMatrix.TransformPosition(newSlicedVertexes[DIR::DIR_NEGATIVE][newIndex].Position);
				newIndexArray[DIR_NEGATIVE].Emplace(newIndex);
				checkIndexMap[DIR_NEGATIVE].Add(indexes[curIndex * 3 + 1], newIndex);
				newSkinWeightArray[DIR_NEGATIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3 + 1]));
			}
			else
				newIndexArray[DIR_NEGATIVE].Emplace(checkIndexMap[DIR_NEGATIVE][indexes[curIndex * 3 + 1]]);
			//4
			newVertex.Position = pIntersectionPoints[1];
			newIndex = newSlicedVertexes[DIR::DIR_NEGATIVE].Emplace(newVertex);
			newIndexArray[DIR_NEGATIVE].Emplace(newIndex);
			newSkinWeightArray[DIR_NEGATIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3 + 1]));

			curEdge.Value = newVertex;
			newIntersectionPoints.Emplace(curEdge);
			//DIR_POSITIVE_End
			//DIR_NEGATIVE_Start

			//0
			if(!isAlreadyInSet[DIR::DIR_POSITIVE][0])
			{
				newIndex = newSlicedVertexes[DIR::DIR_POSITIVE].Emplace(skinnedVertexes[indexes[curIndex * 3]]);
				newSlicedVertexes[DIR::DIR_POSITIVE][newIndex].Position = actorMatrix.TransformPosition(newSlicedVertexes[DIR::DIR_POSITIVE][newIndex].Position);
				newIndexArray[DIR_POSITIVE].Emplace(newIndex);
				checkIndexMap[DIR_POSITIVE].Add(indexes[curIndex * 3], newIndex);
				newSkinWeightArray[DIR_POSITIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3]));
			}
			else
				newIndexArray[DIR_POSITIVE].Emplace(checkIndexMap[DIR_POSITIVE][indexes[curIndex * 3]]);

			//5
			int intersectionIndex = 0;
			memcpy(&newVertex, &skinnedVertexes[indexes[curIndex * 3]], sizeof(FFinalSkinVertex));
			newVertex.Position = pIntersectionPoints[0];
			newIndex = newSlicedVertexes[DIR::DIR_POSITIVE].Emplace(newVertex);
			intersectionIndex = newIndex;
			newIndexArray[DIR_POSITIVE].Emplace(newIndex);
			newSkinWeightArray[DIR_POSITIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3]));

			//2
			if(!isAlreadyInSet[DIR::DIR_POSITIVE][2])
			{
				newIndex = newSlicedVertexes[DIR::DIR_POSITIVE].Emplace(skinnedVertexes[indexes[curIndex * 3 + 2]]);
				newSlicedVertexes[DIR::DIR_POSITIVE][newIndex].Position = actorMatrix.TransformPosition(newSlicedVertexes[DIR::DIR_POSITIVE][newIndex].Position);
				newIndexArray[DIR_POSITIVE].Emplace(newIndex);
				checkIndexMap[DIR_POSITIVE].Add(indexes[curIndex * 3 + 2], newIndex);
				newSkinWeightArray[DIR_POSITIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3 + 2]));
			}
			else
				newIndexArray[DIR_POSITIVE].Emplace(checkIndexMap[DIR_POSITIVE][indexes[curIndex * 3 + 2]]);


			//5
			newIndexArray[DIR_POSITIVE].Emplace(intersectionIndex);
			//6
			memcpy(&newVertex, &skinnedVertexes[indexes[curIndex * 3 + 2]], sizeof(FFinalSkinVertex));
			newVertex.Position = pIntersectionPoints[1];
			newIndex = newSlicedVertexes[DIR::DIR_POSITIVE].Emplace(newVertex);
			newIndexArray[DIR_POSITIVE].Emplace(newIndex);
			newSkinWeightArray[DIR_POSITIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3 + 2]));
			//2
			newIndexArray[DIR_POSITIVE].Emplace(checkIndexMap[DIR_POSITIVE][indexes[curIndex * 3 + 2]]);
		}
	}
	//인덱스 2가 tip triangle일때 -> 1,2번째 선분이 교차할 때
	else if(pIsIntersected[1] && pIsIntersected[2])
	{
		//tip triangle이 +방향이면
		if(pIsPositiveForPlane[2])
		{
			//DIR_POSITIVE_Start

			FFinalSkinVertex newVertex;
			memcpy(&newVertex, &skinnedVertexes[indexes[curIndex * 3 + 2]], sizeof(FFinalSkinVertex));
			newVertex.Position = pIntersectionPoints[1];

			//3
			uint32 newIndex = newSlicedVertexes[DIR::DIR_POSITIVE].Emplace(newVertex);
			newIndexArray[DIR_POSITIVE].Emplace(newIndex);
			newSkinWeightArray[DIR_POSITIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3 + 2]));
			TPair<FFinalSkinVertex, FFinalSkinVertex> curEdge;
			curEdge.Key = newVertex;

			//tip(2)
			if(!isAlreadyInSet[DIR::DIR_POSITIVE][2])
			{
				newIndex = newSlicedVertexes[DIR::DIR_POSITIVE].Emplace(skinnedVertexes[indexes[curIndex * 3 + 2]]);
				newSlicedVertexes[DIR::DIR_POSITIVE][newIndex].Position = actorMatrix.TransformPosition(newSlicedVertexes[DIR::DIR_POSITIVE][newIndex].Position);
				newIndexArray[DIR_POSITIVE].Emplace(newIndex);
				checkIndexMap[DIR_POSITIVE].Add(indexes[curIndex * 3 + 2], newIndex);
				newSkinWeightArray[DIR_POSITIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3 + 2]));
			}
			else
				newIndexArray[DIR_POSITIVE].Emplace(checkIndexMap[DIR_POSITIVE][indexes[curIndex * 3 + 2]]);
			//4
			newVertex.Position = pIntersectionPoints[2];
			newIndex = newSlicedVertexes[DIR::DIR_POSITIVE].Emplace(newVertex);
			newIndexArray[DIR_POSITIVE].Emplace(newIndex);
			newSkinWeightArray[DIR_POSITIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3 + 2]));

			curEdge.Value = newVertex;
			newIntersectionPoints.Emplace(curEdge);
			//DIR_POSITIVE_End
			//DIR_NEGATIVE_Start

			//1
			if(!isAlreadyInSet[DIR::DIR_NEGATIVE][1])
			{
				newIndex = newSlicedVertexes[DIR::DIR_NEGATIVE].Emplace(skinnedVertexes[indexes[curIndex * 3 + 1]]);
				newSlicedVertexes[DIR::DIR_NEGATIVE][newIndex].Position = actorMatrix.TransformPosition(newSlicedVertexes[DIR::DIR_NEGATIVE][newIndex].Position);
				newIndexArray[DIR_NEGATIVE].Emplace(newIndex);
				checkIndexMap[DIR_NEGATIVE].Add(indexes[curIndex * 3 + 1], newIndex);
				newSkinWeightArray[DIR_NEGATIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3 + 1]));
			}
			else
				newIndexArray[DIR_NEGATIVE].Emplace(checkIndexMap[DIR_NEGATIVE][indexes[curIndex * 3 + 1]]);

			//5
			int intersectionIndex = 0;
			memcpy(&newVertex, &skinnedVertexes[indexes[curIndex * 3 + 1]], sizeof(FFinalSkinVertex));
			newVertex.Position = pIntersectionPoints[1];
			newIndex = newSlicedVertexes[DIR::DIR_NEGATIVE].Emplace(newVertex);
			intersectionIndex = newIndex;
			newIndexArray[DIR_NEGATIVE].Emplace(newIndex);
			newSkinWeightArray[DIR_NEGATIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3 + 1]));

			//0
			if(!isAlreadyInSet[DIR::DIR_NEGATIVE][0])
			{
				newIndex = newSlicedVertexes[DIR::DIR_NEGATIVE].Emplace(skinnedVertexes[indexes[curIndex * 3]]);
				newSlicedVertexes[DIR::DIR_NEGATIVE][newIndex].Position = actorMatrix.TransformPosition(newSlicedVertexes[DIR::DIR_NEGATIVE][newIndex].Position);
				newIndexArray[DIR_NEGATIVE].Emplace(newIndex);
				checkIndexMap[DIR_NEGATIVE].Add(indexes[curIndex * 3], newIndex);
				newSkinWeightArray[DIR_NEGATIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3]));
			}
			else
				newIndexArray[DIR_NEGATIVE].Emplace(checkIndexMap[DIR_NEGATIVE][indexes[curIndex * 3]]);
			//5
			newIndexArray[DIR_NEGATIVE].Emplace(intersectionIndex);

			//6
			memcpy(&newVertex, &skinnedVertexes[indexes[curIndex * 3]], sizeof(FFinalSkinVertex));
			newVertex.Position = pIntersectionPoints[2];
			newIndex = newSlicedVertexes[DIR::DIR_NEGATIVE].Emplace(newVertex);
			newIndexArray[DIR_NEGATIVE].Emplace(newIndex);
			newSkinWeightArray[DIR_NEGATIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3]));
			//0
			newIndexArray[DIR_NEGATIVE].Emplace(checkIndexMap[DIR_NEGATIVE][indexes[curIndex * 3]]);
		}
		//그렇지 않다면
		else
		{
			//DIR_POSITIVE_Start

			FFinalSkinVertex newVertex;
			memcpy(&newVertex, &skinnedVertexes[indexes[curIndex * 3 + 2]], sizeof(FFinalSkinVertex));
			newVertex.Position = pIntersectionPoints[1];

			//3
			uint32 newIndex = newSlicedVertexes[DIR::DIR_NEGATIVE].Emplace(newVertex);
			newIndexArray[DIR_NEGATIVE].Emplace(newIndex);
			newSkinWeightArray[DIR_NEGATIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3 + 2]));
			TPair<FFinalSkinVertex, FFinalSkinVertex> curEdge;
			curEdge.Key = newVertex;


			//tip(2)
			if(!isAlreadyInSet[DIR::DIR_NEGATIVE][2])
			{
				newIndex = newSlicedVertexes[DIR::DIR_NEGATIVE].Emplace(skinnedVertexes[indexes[curIndex * 3 + 2]]);
				newSlicedVertexes[DIR::DIR_NEGATIVE][newIndex].Position = actorMatrix.TransformPosition(newSlicedVertexes[DIR::DIR_NEGATIVE][newIndex].Position);
				newIndexArray[DIR_NEGATIVE].Emplace(newIndex);
				checkIndexMap[DIR_NEGATIVE].Add(indexes[curIndex * 3 + 2], newIndex);
				newSkinWeightArray[DIR_NEGATIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3 + 2]));
			}
			else
				newIndexArray[DIR_NEGATIVE].Emplace(checkIndexMap[DIR_NEGATIVE][indexes[curIndex * 3 + 2]]);
			//4
			newVertex.Position = pIntersectionPoints[2];
			newIndex = newSlicedVertexes[DIR::DIR_NEGATIVE].Emplace(newVertex);
			newIndexArray[DIR_NEGATIVE].Emplace(newIndex);
			newSkinWeightArray[DIR_NEGATIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3 + 2]));

			curEdge.Value = newVertex;
			newIntersectionPoints.Emplace(curEdge);

			//DIR_POSITIVE_End
			//DIR_NEGATIVE_Start

			//1
			if(!isAlreadyInSet[DIR::DIR_POSITIVE][1])
			{
				newIndex = newSlicedVertexes[DIR::DIR_POSITIVE].Emplace(skinnedVertexes[indexes[curIndex * 3 + 1]]);
				newSlicedVertexes[DIR::DIR_POSITIVE][newIndex].Position = actorMatrix.TransformPosition(newSlicedVertexes[DIR::DIR_POSITIVE][newIndex].Position);
				newIndexArray[DIR_POSITIVE].Emplace(newIndex);
				checkIndexMap[DIR_POSITIVE].Add(indexes[curIndex * 3 + 1], newIndex);
				newSkinWeightArray[DIR_POSITIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3 + 1]));
			}
			else
				newIndexArray[DIR_POSITIVE].Emplace(checkIndexMap[DIR_POSITIVE][indexes[curIndex * 3 + 1]]);

			//5
			int intersectionIndex = 0;
			memcpy(&newVertex, &skinnedVertexes[indexes[curIndex * 3 + 1]], sizeof(FFinalSkinVertex));
			newVertex.Position = pIntersectionPoints[1];
			newIndex = newSlicedVertexes[DIR::DIR_POSITIVE].Emplace(newVertex);
			intersectionIndex = newIndex;
			newIndexArray[DIR_POSITIVE].Emplace(newIndex);
			newSkinWeightArray[DIR_POSITIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3 + 1]));

			//0
			if(!isAlreadyInSet[DIR::DIR_POSITIVE][0])
			{
				newIndex = newSlicedVertexes[DIR::DIR_POSITIVE].Emplace(skinnedVertexes[indexes[curIndex * 3]]);
				newSlicedVertexes[DIR::DIR_POSITIVE][newIndex].Position = actorMatrix.TransformPosition(newSlicedVertexes[DIR::DIR_POSITIVE][newIndex].Position);
				newIndexArray[DIR_POSITIVE].Emplace(newIndex);
				checkIndexMap[DIR_POSITIVE].Add(indexes[curIndex * 3], newIndex);
				newSkinWeightArray[DIR_POSITIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3]));
			}
			else
				newIndexArray[DIR_POSITIVE].Emplace(checkIndexMap[DIR_POSITIVE][indexes[curIndex * 3]]);
			//5
			newIndexArray[DIR_POSITIVE].Emplace(intersectionIndex);

			//6
			memcpy(&newVertex, &skinnedVertexes[indexes[curIndex * 3]], sizeof(FFinalSkinVertex));
			newVertex.Position = pIntersectionPoints[2];
			newIndex = newSlicedVertexes[DIR::DIR_POSITIVE].Emplace(newVertex);
			newIndexArray[DIR_POSITIVE].Emplace(newIndex);
			newSkinWeightArray[DIR_POSITIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3]));
			//0
			newIndexArray[DIR_POSITIVE].Emplace(checkIndexMap[DIR_POSITIVE][indexes[curIndex * 3]]);
		}
	}
	//인덱스 0이 tip triangle일때 -> 0,2번째 선분이 교차할 때
	else if(pIsIntersected[0] && pIsIntersected[2])
	{
		//tip triangle이 +방향이면
		if(pIsPositiveForPlane[0])
		{
			//DIR_POSITIVE_Start
			FFinalSkinVertex newVertex;
			memcpy(&newVertex, &skinnedVertexes[indexes[curIndex * 3]], sizeof(FFinalSkinVertex));
			newVertex.Position = pIntersectionPoints[2];

			//3
			uint32 newIndex = newSlicedVertexes[DIR::DIR_POSITIVE].Emplace(newVertex);
			newIndexArray[DIR_POSITIVE].Emplace(newIndex);
			newSkinWeightArray[DIR_POSITIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3]));
			TPair<FFinalSkinVertex, FFinalSkinVertex> curEdge;
			curEdge.Key = newVertex;

			//tip(0)
			if(!isAlreadyInSet[DIR::DIR_POSITIVE][0])
			{
				newIndex = newSlicedVertexes[DIR::DIR_POSITIVE].Emplace(skinnedVertexes[indexes[curIndex * 3]]);
				newSlicedVertexes[DIR::DIR_POSITIVE][newIndex].Position = actorMatrix.TransformPosition(newSlicedVertexes[DIR::DIR_POSITIVE][newIndex].Position);
				newIndexArray[DIR_POSITIVE].Emplace(newIndex);
				checkIndexMap[DIR_POSITIVE].Add(indexes[curIndex * 3], newIndex);
				newSkinWeightArray[DIR_POSITIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3]));
			}
			else
				newIndexArray[DIR_POSITIVE].Emplace(checkIndexMap[DIR_POSITIVE][indexes[curIndex * 3]]);
			//4
			newVertex.Position = pIntersectionPoints[0];
			newIndex = newSlicedVertexes[DIR::DIR_POSITIVE].Emplace(newVertex);
			newIndexArray[DIR_POSITIVE].Emplace(newIndex);
			newSkinWeightArray[DIR_POSITIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3]));

			curEdge.Value = newVertex;
			newIntersectionPoints.Emplace(curEdge);
			//DIR_POSITIVE_End
			//DIR_NEGATIVE_Start

			//2
			if(!isAlreadyInSet[DIR::DIR_NEGATIVE][2])
			{
				newIndex = newSlicedVertexes[DIR::DIR_NEGATIVE].Emplace(skinnedVertexes[indexes[curIndex * 3 + 2]]);
				newSlicedVertexes[DIR::DIR_NEGATIVE][newIndex].Position = actorMatrix.TransformPosition(newSlicedVertexes[DIR::DIR_NEGATIVE][newIndex].Position);
				newIndexArray[DIR_NEGATIVE].Emplace(newIndex);
				checkIndexMap[DIR_NEGATIVE].Add(indexes[curIndex * 3 + 2], newIndex);
				newSkinWeightArray[DIR_NEGATIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3 + 2]));
			}
			else
				newIndexArray[DIR_NEGATIVE].Emplace(checkIndexMap[DIR_NEGATIVE][indexes[curIndex * 3 + 2]]);

			//5
			int intersectionIndex = 0;
			memcpy(&newVertex, &skinnedVertexes[indexes[curIndex * 3 + 2]], sizeof(FFinalSkinVertex));
			newVertex.Position = pIntersectionPoints[2];
			newIndex = newSlicedVertexes[DIR::DIR_NEGATIVE].Emplace(newVertex);
			intersectionIndex = newIndex;
			newIndexArray[DIR_NEGATIVE].Emplace(newIndex);
			newSkinWeightArray[DIR_NEGATIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3 + 2]));

			//1
			if(!isAlreadyInSet[DIR::DIR_NEGATIVE][1])
			{
				newIndex = newSlicedVertexes[DIR::DIR_NEGATIVE].Emplace(skinnedVertexes[indexes[curIndex * 3 + 1]]);
				newSlicedVertexes[DIR::DIR_NEGATIVE][newIndex].Position = actorMatrix.TransformPosition(newSlicedVertexes[DIR::DIR_NEGATIVE][newIndex].Position);
				newIndexArray[DIR_NEGATIVE].Emplace(newIndex);
				checkIndexMap[DIR_NEGATIVE].Add(indexes[curIndex * 3 + 1], newIndex);
				newSkinWeightArray[DIR_NEGATIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3 + 1]));
			}
			else
				newIndexArray[DIR_NEGATIVE].Emplace(checkIndexMap[DIR_NEGATIVE][indexes[curIndex * 3 + 1]]);
			//5
			newIndexArray[DIR_NEGATIVE].Emplace(intersectionIndex);

			//6
			memcpy(&newVertex, &skinnedVertexes[indexes[curIndex * 3 + 1]], sizeof(FFinalSkinVertex));
			newVertex.Position = pIntersectionPoints[0];
			newIndex = newSlicedVertexes[DIR::DIR_NEGATIVE].Emplace(newVertex);
			newIndexArray[DIR_NEGATIVE].Emplace(newIndex);
			newSkinWeightArray[DIR_NEGATIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3 + 1]));
			//1
			newIndexArray[DIR_NEGATIVE].Emplace(checkIndexMap[DIR_NEGATIVE][indexes[curIndex * 3 + 1]]);
		}
		//그렇지 않다면
		else
		{
			FFinalSkinVertex newVertex;
			memcpy(&newVertex, &skinnedVertexes[indexes[curIndex * 3]], sizeof(FFinalSkinVertex));
			newVertex.Position = pIntersectionPoints[2];

			//3
			uint32 newIndex = newSlicedVertexes[DIR::DIR_NEGATIVE].Emplace(newVertex);
			newIndexArray[DIR_NEGATIVE].Emplace(newIndex);
			newSkinWeightArray[DIR_NEGATIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3]));
			TPair<FFinalSkinVertex, FFinalSkinVertex> curEdge;
			curEdge.Key = newVertex;

			//tip(0)
			if(!isAlreadyInSet[DIR::DIR_NEGATIVE][0])
			{
				newIndex = newSlicedVertexes[DIR::DIR_NEGATIVE].Emplace(skinnedVertexes[indexes[curIndex * 3]]);
				newSlicedVertexes[DIR::DIR_NEGATIVE][newIndex].Position = actorMatrix.TransformPosition(newSlicedVertexes[DIR::DIR_NEGATIVE][newIndex].Position);
				newIndexArray[DIR_NEGATIVE].Emplace(newIndex);
				checkIndexMap[DIR_NEGATIVE].Add(indexes[curIndex * 3], newIndex);
				newSkinWeightArray[DIR_NEGATIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3]));
			}
			else
				newIndexArray[DIR_NEGATIVE].Emplace(checkIndexMap[DIR_NEGATIVE][indexes[curIndex * 3]]);
			//4
			newVertex.Position = pIntersectionPoints[0];
			newIndex = newSlicedVertexes[DIR::DIR_NEGATIVE].Emplace(newVertex);
			newIndexArray[DIR_NEGATIVE].Emplace(newIndex);
			newSkinWeightArray[DIR_NEGATIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3]));

			curEdge.Value = newVertex;
			newIntersectionPoints.Emplace(curEdge);
			//DIR_POSITIVE_End
			//DIR_NEGATIVE_Start

			//2
			if(!isAlreadyInSet[DIR::DIR_POSITIVE][2])
			{
				newIndex = newSlicedVertexes[DIR::DIR_POSITIVE].Emplace(skinnedVertexes[indexes[curIndex * 3 + 2]]);
				newSlicedVertexes[DIR::DIR_POSITIVE][newIndex].Position = actorMatrix.TransformPosition(newSlicedVertexes[DIR::DIR_POSITIVE][newIndex].Position);
				newIndexArray[DIR_POSITIVE].Emplace(newIndex);
				checkIndexMap[DIR_POSITIVE].Add(indexes[curIndex * 3 + 2], newIndex);
				newSkinWeightArray[DIR_POSITIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3 + 2]));
			}
			else
				newIndexArray[DIR_POSITIVE].Emplace(checkIndexMap[DIR_POSITIVE][indexes[curIndex * 3 + 2]]);

			//5
			int intersectionIndex = 0;
			memcpy(&newVertex, &skinnedVertexes[indexes[curIndex * 3 + 2]], sizeof(FFinalSkinVertex));
			newVertex.Position = pIntersectionPoints[2];
			newIndex = newSlicedVertexes[DIR::DIR_POSITIVE].Emplace(newVertex);
			intersectionIndex = newIndex;
			newIndexArray[DIR_POSITIVE].Emplace(newIndex);
			newSkinWeightArray[DIR_POSITIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3 + 2]));

			//1
			if(!isAlreadyInSet[DIR::DIR_POSITIVE][1])
			{
				newIndex = newSlicedVertexes[DIR::DIR_POSITIVE].Emplace(skinnedVertexes[indexes[curIndex * 3 + 1]]);
				newSlicedVertexes[DIR::DIR_POSITIVE][newIndex].Position = actorMatrix.TransformPosition(newSlicedVertexes[DIR::DIR_POSITIVE][newIndex].Position);
				newIndexArray[DIR_POSITIVE].Emplace(newIndex);
				checkIndexMap[DIR_POSITIVE].Add(indexes[curIndex * 3 + 1], newIndex);
				newSkinWeightArray[DIR_POSITIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3 + 1]));
			}
			else
				newIndexArray[DIR_POSITIVE].Emplace(checkIndexMap[DIR_POSITIVE][indexes[curIndex * 3 + 1]]);
			//5
			newIndexArray[DIR_POSITIVE].Emplace(intersectionIndex);

			//6
			memcpy(&newVertex, &skinnedVertexes[indexes[curIndex * 3 + 1]], sizeof(FFinalSkinVertex));
			newVertex.Position = pIntersectionPoints[0];
			newIndex = newSlicedVertexes[DIR::DIR_POSITIVE].Emplace(newVertex);
			newIndexArray[DIR_POSITIVE].Emplace(newIndex);
			newSkinWeightArray[DIR_POSITIVE].Emplace(skinWeights.GetSkinWeightPtr<false>(indexes[curIndex * 3 + 1]));
			//1
			newIndexArray[DIR_POSITIVE].Emplace(checkIndexMap[DIR_POSITIVE][indexes[curIndex * 3 + 1]]);
		}
	}
}

