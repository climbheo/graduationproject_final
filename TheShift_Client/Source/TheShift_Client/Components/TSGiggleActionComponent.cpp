﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TSGiggleActionComponent.h"

// Sets default values for this component's properties
UTSGiggleActionComponent::UTSGiggleActionComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTSGiggleActionComponent::BeginPlay()
{
	Super::BeginPlay();
	InitialLocation = GetOwner()->GetActorLocation();
	// ...

	
	
	
}


// Called every frame
void UTSGiggleActionComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...

	if (IsGiggle)
	{
		UE_LOG(LogTemp, Log, TEXT("GIGGLING"));
		float randomFloat = FMath::FRandRange(-2.f, 2.f);
		GetOwner()->AddActorLocalOffset(FVector(randomFloat, randomFloat, randomFloat));
	}
}

void UTSGiggleActionComponent::StartGiggle()
{
	IsGiggle = true;
	GetOwner()->SetActorLocation(InitialLocation);

	if (ResetGiggleTimerHandle.IsValid())
		ResetGiggleTimerHandle.Invalidate();

	GetWorld()->GetTimerManager().SetTimer(ResetGiggleTimerHandle, FTimerDelegate::CreateLambda([this]()->void {
		IsGiggle = false;

		}), 0.5f, false);
}

