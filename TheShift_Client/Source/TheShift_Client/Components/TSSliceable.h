// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "EngineMinimal.h"
#include "Components/ActorComponent.h"

//Slice
#include <deque>
#include "SkeletalRenderPublic.h"
#include "ProceduralMeshComponent.h"
using namespace std;
//

#include "TSSliceable.generated.h"

class USkeletalMeshComponent;
class UStaticMeshComponent;
class UProceduralMeshComponent;
class UMaterialInterface;


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class THESHIFT_CLIENT_API UTSSliceable : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UTSSliceable();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	//Skeletal Mesh를 자르는 함수
	void SliceSkeletalMesh(USkeletalMeshComponent* pSkeletalMesh, FVector vPlanePos, FVector vPlaneNormal, FVector vPlaneTangent, UMaterialInterface* pCapMaterial);

private:
	//교차되는 삼각형을 세개의 삼각형으로 만드는 함수 -> 절단면을 위한 삼각형이 아님
	void SliceAndTriangulate(FMatrix& actorMatrix, FVector* pIntersectionPoints, uint32 curIndex, bool* pIsIntersected, bool* pIsPositiveForPlane,
							 TArray<FFinalSkinVertex>& skinnedVertexes, TArray<uint32>& indexes, class FSkinWeightVertexBuffer& skinWeights);

private:
	enum DIR
	{
		DIR_POSITIVE = 0,
		DIR_NEGATIVE = 1,
		DIR_END
	};
	//양,음 방향의 정점들을 나눠 저장해 연산 -> DIR_POSITIVE or DIR_NEGATIVE

	//이 Map의 배열은 각 양, 음의 방향에 담겼던 인덱스들의 새로운 인덱스를 저장하는 변수
	//Key는 원래 인덱스이며 Value는 나중에 담긴 새로운 인덱스
	TMap<uint32, int32>				checkIndexMap[DIR::DIR_END];

	//For New IndexBuffers
	TArray<int32>						newIndexArray[DIR::DIR_END];

	//For New SkingWeightBuffers
	TArray<TSkinWeightInfo<false>*>		newSkinWeightArray[DIR::DIR_END];

	//For New SlicedVertex
	TArray<FFinalSkinVertex>			newSlicedVertexes[DIR::DIR_END];

	//For Intersection
	TArray<TPair<FFinalSkinVertex, FFinalSkinVertex>>			newIntersectionPoints;

	//For Cap
	deque<deque<FFinalSkinVertex>> capQueue;

	AActor* pSlicedActors[2] = {};

	TArray<TArray<FVector>> newCapPosition;

	TArray<TArray<FVector>> newCapNormal[DIR_END];

	TArray<TArray<FProcMeshTangent>> newCapTangent[DIR_END];

	TArray<TArray<FVector2D>> newCapUV;

};
