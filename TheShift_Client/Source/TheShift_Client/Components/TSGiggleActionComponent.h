﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "EngineMinimal.h"
#include "Components/ActorComponent.h"
#include "TSGiggleActionComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THESHIFT_CLIENT_API UTSGiggleActionComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTSGiggleActionComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	void StartGiggle();

private:
	UPROPERTY()
		FVector InitialLocation;

	bool IsGiggle = false;

	UPROPERTY()
	FTimerHandle ResetGiggleTimerHandle = {};
		
	
};
