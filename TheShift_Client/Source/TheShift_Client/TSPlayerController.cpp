﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TSPlayerController.h"


void ATSPlayerController::BeginPlay()
{
	Super::BeginPlay();
}

void ATSPlayerController::BeginDestroy()
{
	Super::BeginDestroy();
}

void ATSPlayerController::ChangeInputMode(bool isGameMode)
{
	if(isGameMode)
	{
		bShowMouseCursor = false;
		SetInputMode(GameOnlyInputMode);
	}
	else
	{
		bShowMouseCursor = true;
		SetInputMode(UIOnlyInputMode);
	}

}
