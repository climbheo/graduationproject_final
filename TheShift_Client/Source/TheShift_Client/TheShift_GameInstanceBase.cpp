﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TheShift_GameInstanceBase.h"

#include "IPv4Address.h"
#include "SocketSubsystem.h"
#include "Game/System/MapData.h"
#include "Protocol/Log.h"
#include "TheShift_ClientGameModeBase.h"

UTheShift_GameInstanceBase::UTheShift_GameInstanceBase()
{
	LOG_DEBUG("GameInstance Constructor()");
}

void UTheShift_GameInstanceBase::Init()
{
	Super::Init();

	// Memory initialize
	memset(Buffer, 0, BUFFERSIZE);
	memset(PacketBuffer, 0, BUFFERSIZE);

	// Network initialize
	FIPv4Address Ip;
	FIPv4Address::Parse(ServerAddr, Ip);
	InternetAddress = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateInternetAddr();
	InternetAddress->SetIp(Ip.Value);
	InternetAddress->SetPort(Port);

	// Server연결 시도
	if (!ConnectToServer())
	{
		// 연결 실패
		InitialConnectionFailed = true;
		return;
	}

	MapData::GetInstance();

	LOG_DEBUG("TheShift_GameInstance initialized");
}

void UTheShift_GameInstanceBase::Shutdown()
{
	Super::Shutdown();

	TheShift::Serializables::Disconnect DisconnectData;
	Send(DisconnectData);
	Socket->Close();
	LOG_DEBUG("GameInstance Shutdown()");
}

// TheShift_GameModeBase의 Tick()에서 매번 불릴 Network packet처리 함수.
// Packet조립 가능여부를 return한다.
// true를 return한다면 PacketBuffer주소를 통해 packet읽기 가능
// Loop로 감싸서 false를 return할때까지 반복해야함.
void UTheShift_GameInstanceBase::NetworkTick()
{
	uint32 PendingDataSize = 0;
	int32 BytesTransferred = 0;
	if (Socket->HasPendingData(PendingDataSize))
	{
		Socket->Recv(Buffer, TheShift::BUFFERSIZE, BytesTransferred);
		AssembleAndProcessPacket(Buffer, BytesTransferred, PacketBuffer);
	}
}

// 조립이 다 된 packet을 처리 할 함수를 등록한다.
void UTheShift_GameInstanceBase::RegisterPacketProcessingFunc(std::function<void(uint8*)> NewProcessPacketFunc)
{
	ProcessPacketFunc = NewProcessPacketFunc;
}

// Pending data가 있다면 recv하고 읽은 길이를 return한다.
int32 UTheShift_GameInstanceBase::Recv()
{
	uint32 PendingDataSize = 0;
	int32 BytesTransferred = 0;
	if (Socket->HasPendingData(PendingDataSize))
	{
		Socket->Recv(Buffer, TheShift::BUFFERSIZE, BytesTransferred);
	}

	return BytesTransferred;
}

// Packet의 주소를 return한다.
// 첫 2byte는 TheShift::Serializables::Type, 이후로는 data
// 주의: 조립이 완료되었는지 여부는 보장하지 않음, NetworkTick()의 return값으로 해당 여부 확인 가능
uint8* UTheShift_GameInstanceBase::GetPacketData()
{
	// 앞 4bytes(packet의 크기)가 제외된 buffer의 pointer를 return
	return PacketBuffer + sizeof(uint32);
}

bool UTheShift_GameInstanceBase::TryConnectToServer(FString StringAddress)
{
	// Network initialize
	FIPv4Address Ip;
	FIPv4Address::Parse(StringAddress, Ip);
	InternetAddress = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateInternetAddr();
	InternetAddress->SetIp(Ip.Value);
	InternetAddress->SetPort(Port);

	// Server연결 시도
	if (!ConnectToServer())
	{
		// 연결 실패
		return false;
	}

	return true;
}

// Server와의 연결 상태를 return한다.
bool UTheShift_GameInstanceBase::IsConnectedToServer() const
{
	return ConnectedToServer;
}

// 현재 게임이 진행중인지 여부를 return한다.
bool UTheShift_GameInstanceBase::IsInGame() const
{
	return InGame;
}

// 현재 게임의 진행여부를 변경한다.
void UTheShift_GameInstanceBase::SetInGame(bool Value)
{
	InGame = Value;
}

// Server에게 Session 생성을 요청한다.
void UTheShift_GameInstanceBase::CreateSessionRequest(UMapType LevelType, FString SessionName)
{
	TheShift::MapType Type;
	switch (LevelType)
	{
	case UMapType::MainMenu:
		Type = TheShift::MapType::MainMenu;
		break;
	case UMapType::Stage01:
		Type = TheShift::MapType::Stage01;
		break;
	case UMapType::Stage02:
		Type = TheShift::MapType::Stage02;
		break;
	case UMapType::Stage03:
		Type = TheShift::MapType::Stage03;
		break;
	case UMapType::Stage04:
		Type = TheShift::MapType::Stage04;
		break;
	case UMapType::Landscape:
		Type = TheShift::MapType::Landscape;
		break;
	default:
		LOG_ERROR("No matching types, please add a corresponding UMapType or TheShift::LevelName.");
		return;
	}

	LOG_INFO("Requesting to create session with level: %hs", TCHAR_TO_UTF8(*GetLevelName(Type).GetPlainNameString()));
	// 새로운 session을 생성하고 바로 시작해달라고 요청한다.
	TheShift::Serializables::CreateAndStartSessionReq Data;
	Data.Type = Type;
	Data.SessionName = std::string(TCHAR_TO_UTF8(*SessionName));
	Send(Data);
}

// MapType을 받아 해당 Map의 이름을 FName으로 return한다.
FName UTheShift_GameInstanceBase::GetLevelName(TheShift::MapType Type)
{
	switch (Type)
	{
	case TheShift::MapType::MainMenu:
		return TheShift::LevelName::MainMenu;
	case TheShift::MapType::Stage01:
		return TheShift::LevelName::Stage01;
	case TheShift::MapType::Stage02:
		return TheShift::LevelName::Stage02;
	case TheShift::MapType::Stage03:
		return TheShift::LevelName::Stage03;
	case TheShift::MapType::Stage04:
		return TheShift::LevelName::Stage04;
	case TheShift::MapType::Landscape:
		return TheShift::LevelName::TestLandscape;
	default:
		return FName();
	}
}

void UTheShift_GameInstanceBase::OnStart()
{
	Super::OnStart();
	LOG_DEBUG("GameInstance OnStart()");
}

// Server에 접속한다.
bool UTheShift_GameInstanceBase::ConnectToServer()
{
	Socket = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateSocket(NAME_Stream, TEXT("default"), false);
	if (!Socket->Connect(*InternetAddress))
	{
		LOG_ERROR("Can't connect to server!!");
		ConnectedToServer = false;
		return false;
	}

	else
	{
		LOG_INFO("Connected to server.");
		ConnectedToServer = true;
		return true;
	}
}

void UTheShift_GameInstanceBase::AssembleAndProcessPacket(uint8* SrcReceiveBuffer, int32 BytesTransferred,
	uint8* DestPacketBuffer)
{
	uint32 Rest = BytesTransferred;
	uint32 PacketSize = 0;

	// 전에 받아놓은 패킷의 제일 앞 사이즈를 읽는다.
	if (PrevSize > sizeof(uint32))
	{
		PacketSize = *reinterpret_cast<uint32*>(&PacketBuffer[0]);
	}
	// 데이터 길이도 안왔을 수 있음.
	else
	{
		uint32 AdditionalDataSizeLength = sizeof(uint32) - PrevSize;

		// 새로 온 데이터로도 덜 온 데이터 길이를 만들 수 없다면 온 만큼 복사해주고 패킷을 만들 수 없으니 return한다.
		if (Rest < AdditionalDataSizeLength)
		{
			memcpy(&PacketBuffer[PrevSize], SrcReceiveBuffer, Rest);
			PrevSize += Rest;
			return;
		}

		// 만약 데이터 길이를 만들 수 있다면 PrevSize부터 4바이트 까지만 복사해준다.
		memcpy(&PacketBuffer[PrevSize], SrcReceiveBuffer, AdditionalDataSizeLength);
		// 데이터 길이를 복사해준 만큼 ptr을 옮겨준다.
		SrcReceiveBuffer += AdditionalDataSizeLength;
		PrevSize += AdditionalDataSizeLength;
		Rest -= AdditionalDataSizeLength;
		PacketSize = *reinterpret_cast<uint32*>(&PacketBuffer[0]);
	}

	// 패킷을 만든다.
	while (Rest > 0)
	{
		if (PacketSize == 0)
		{
			// 패킷 처리하고 남아있는 데이터 처리할 때 데이터 사이즈를 읽을 수 있으면 계속 처리.
			if (Rest >= sizeof(uint32))
			{
				PacketSize = *reinterpret_cast<uint32*>(&SrcReceiveBuffer[0]);
			}
			// 데이터 사이즈를 읽을 수 없으면 복사해주고 break;
			else
			{
				memcpy(PacketBuffer + PrevSize, SrcReceiveBuffer, Rest);
				PrevSize = Rest;
				break;
			}
		}
		uint32 Required = PacketSize - PrevSize;
		if (Rest >= Required)
		{
			//패킷 생성 가능.
			memcpy(PacketBuffer + PrevSize, SrcReceiveBuffer, Required);
			// 패킷 처리.
			//TheShift::Serializables::Type DataType = *reinterpret_cast<TheShift::Serializables::Type*>(PacketBuffer + sizeof(uint32));
			//ProcessPacket(PacketBuffer + sizeof(uint32) + sizeof(TheShift::Serializables::Type), DataType);
			Rest -= Required;
			SrcReceiveBuffer += Required;
			PacketSize = 0;
			BytesTransferred -= PacketSize;
			PrevSize = 0;

			// Packet 처리
			ProcessPacketFunc(PacketBuffer + sizeof(uint32));
			// 조립 가능
		}
		else
		{
			// 패킷 생성 불가능.
			// 복사해둔다.
			memcpy(PacketBuffer + PrevSize, SrcReceiveBuffer, Rest);
			PrevSize += Rest;
			Rest = 0;
		}
	}

	// 조립할수 없음
}

// ClientId 설정
void UTheShift_GameInstanceBase::SetMyClientId(TheShift::ClientId Id)
{
	MyClientId = Id;
}

// ClientId를 return
TheShift::ClientId UTheShift_GameInstanceBase::GetMyClientId() const
{
	return MyClientId;
}

TheShift::Serializables::SessionStarted& UTheShift_GameInstanceBase::GetStagedSessionData()
{
	return StagedSessionData;
}

TheShift::Serializables::ToNewPlayerOldPlayerWeapons& UTheShift_GameInstanceBase::GetStagedOldPlayerWeaponsData()
{
	return StagedOldPlayerWeaponsData;
}

TArray<TheShift::Serializables::ItemAcquire>& UTheShift_GameInstanceBase::GetStagedItemData()
{
	return StagedItemData;
}

// 사용할 수 있는 SessionStarted data가 있음을 설정한다.
void UTheShift_GameInstanceBase::SetSessionStartedDataStaged(bool Value)
{
	SessionStartedDataStaged = Value;

	// false가 들어올 경우 data를 처리했다는 뜻이기 때문에 정리해준다.
	if(!Value)
	{
		StagedSessionData.Actors.Array.clear();
		StagedSessionData.Players.Array.clear();
	}
}

// 사용할 수 있는 ToNewPlayerWeaponsData가 있음을 설정한다.
void UTheShift_GameInstanceBase::SetOldPlayerWeaponsDataStaged(bool Value)
{
	OldPlayerWeaponsDataStaged = Value;

	// false가 들어올 경우 data를 처리했다는 뜻이기 때문에 정리해준다.
	if(!Value)
	{
		StagedOldPlayerWeaponsData.SwapWeapons.Array.clear();
	}
}

void UTheShift_GameInstanceBase::SetItemDataStaged(bool Value)
{
	ItemDataStaged = Value;

	if(!Value)
	{
		StagedItemData.Empty();
	}
}

// 사용할 수 있는 SessionStarted가 저장되어있는지 확인한다.
bool UTheShift_GameInstanceBase::IsSessionStartedDataStaged() const
{
	return SessionStartedDataStaged;
}

// 사용ㅎ알 수 있는 ToNewPlayerOldPlayerWeapons가 저장되어있는지 확인한다.
bool UTheShift_GameInstanceBase::IsOldPlayersWeaponsDataStaged() const
{
	return OldPlayerWeaponsDataStaged;
}

bool UTheShift_GameInstanceBase::IsItemDataStaged() const
{
	return ItemDataStaged;
}

// Editor상에서 MainMenu를 건너뛰고 바로 다른 level에서 시작할 때 Session을 만들고 접속하는 일련의 과정을
// 한번에 처리하도록 한다. Debug용이하게 하기 위함
void UTheShift_GameInstanceBase::_Editor_StartWithoutMainMenu()
{
	CreateSessionRequest(Cast<ATheShift_ClientGameModeBase>(GetWorld()->GetAuthGameMode())->GetCurrentUMapType(), "Debug");
	InGame = true;
}
