﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TSTeleportEffectActor.h"

// Sets default values
ATSTeleportEffectActor::ATSTeleportEffectActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PoseableMeshComponent = CreateDefaultSubobject<UPoseableMeshComponent>(TEXT("SrpingArm"));
	RootComponent = PoseableMeshComponent;

}

// Called when the game starts or when spawned
void ATSTeleportEffectActor::BeginPlay()
{
	//UE_LOG(LogTemp, Log, TEXT("BEGINPLAY!!"));
	Super::BeginPlay();
	
}

// Called every frame
void ATSTeleportEffectActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (IsMove)
	{
		AddActorWorldOffset(DirectionVector * 50.f * DeltaTime);
	}

}

void ATSTeleportEffectActor::SetCharacterToCopy(ACharacter* Character)
{
	//UE_LOG(LogTemp, Log, TEXT("SET!!"));
	CharacterToCopy = Character; 
	PoseableMeshComponent->SetSkeletalMesh(CharacterToCopy->GetMesh()->SkeletalMesh);
}

