﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "IPAddress.h"
#include "Sockets.h"
#include "Engine/GameInstance.h"
#include "Protocol/MemoryStream.h"
#include "Protocol/MessageBuilder.h"
#include "Protocol/MessageQueue.h"
#include "Protocol/Protocol.h"
#include "Misc/UMapTypes.h"

#include "TheShift_GameInstanceBase.generated.h"


/**
 * 
 */
UCLASS()
class THESHIFT_CLIENT_API UTheShift_GameInstanceBase : public UGameInstance
{
	GENERATED_BODY()

private:
	FSocket* Socket;
	TSharedPtr<FInternetAddr> InternetAddress;
	MessageQueue<TheShift::Message>* ReceivedDataQueue;
	TheShift::MessageBuilder MsgBuilder;
	uint8 Buffer[TheShift::BUFFERSIZE];
	uint8 PacketBuffer[TheShift::BUFFERSIZE];
	uint8 SendBuffer[TheShift::BUFFERSIZE];
	int32 PrevSize;
	InputMemoryStream InputStream;
	bool ConnectedToServer = false;
	bool InGame = false;

	// SessionStart 종류
	// MainMenu에서 시작인가? -> GameInstance가 가지고있는 SessionStartedData를 사용
	bool MenuStarted = false;
	// 인게임 내에서 level 변경인가? ->
	// 1. SessionStarted를 이전 GameMode가 수신했을 경우
	//		GameInstance가 저장하고 있도록 하고 새로 만들어질 GameMode가 처리하도록 한다.
	// 2. SessionStarted를 새 GameMode가 수신했을 경우
	//		현재 GameMode에서 SessionStarted를 처리한다.
	// GameMode는 LevelChange 대기중인지 여부를 저장하고 있고 이를 기준으로 위 두가지 경우를 판단한다.
	bool LevelTravel = false;

	TheShift::ClientId MyClientId = TheShift::INVALID_CLIENTID;
	TheShift::UID MyActor = TheShift::INVALID_UID;
	TheShift::SessionId CurrentSessionId = TheShift::INVALID_SESSIONID;

public:
	UTheShift_GameInstanceBase();

	virtual void Init() override;
	virtual void Shutdown() override;
	virtual void NetworkTick();
	void RegisterPacketProcessingFunc(std::function<void(uint8*)> NewProcessPacketFunc);
	int32 Recv();
	uint8* GetPacketData();

	std::function<void(uint8*)> ProcessPacketFunc;

	// Server address connecting to
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Network)
	FString ServerAddr = "127.0.0.1";
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Network)
	int32 Port = 54000;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MyProperty)
	FString MyName;
	UPROPERTY(BlueprintReadWrite, Category = Network)
	bool InitialConnectionFailed = false;
	UFUNCTION(BlueprintCallable, Category = Network)
	bool TryConnectToServer(FString Ip);

	bool LastStatStored = false;
	float LastStoredHp;
	int LastStoredStamina;
	int LastStoredTeleportGage;

	bool IsConnectedToServer() const;
	bool IsInGame() const;
	void SetInGame(bool Value);

	UFUNCTION(BlueprintCallable, Category = "Server Request")
	void CreateSessionRequest(UMapType LevelType, FString SessionName);

	FName GetLevelName(TheShift::MapType Type);

	// SerializableData의 자식 클래스가 와야한다. 
	template <typename T>
	typename std::enable_if<std::is_base_of<TheShift::Serializables::SerializableData, T>::value>::type Send(
		T& Data)
	{
		size_t Size = 0;
		int32 TotalSentBytes = 0;
		int32 SentBytes = 0;
		MsgBuilder.BuildSerializedBuffer(Data, reinterpret_cast<char*>(SendBuffer), Size);
		while (TotalSentBytes < Size)
		{
			//UE_LOG(LogTemp, Error, TEXT("Data send size: %d"), Size);
			Socket->Send(SendBuffer + TotalSentBytes, Size - TotalSentBytes, SentBytes);
			TotalSentBytes += SentBytes;
		}
	}
	
protected:
	virtual void OnStart() override;

private:
	bool ConnectToServer();

	// Packet조립, 조립 가능 여부를 return한다.
	void AssembleAndProcessPacket(uint8* SrcReceiveBuffer, int32 BytesTransferred, uint8* DestPacketBuffer);

public:
	void SetMyClientId(TheShift::ClientId Id);
	TheShift::ClientId GetMyClientId() const;

private:
	TheShift::Serializables::SessionStarted StagedSessionData;
	TheShift::Serializables::ToNewPlayerOldPlayerWeapons StagedOldPlayerWeaponsData;
	TArray<TheShift::Serializables::ItemAcquire> StagedItemData;

	bool SessionStartedDataStaged = false;
	bool OldPlayerWeaponsDataStaged = false;
	bool ItemDataStaged = false;

public:
	TheShift::Serializables::SessionStarted& GetStagedSessionData();
	TheShift::Serializables::ToNewPlayerOldPlayerWeapons& GetStagedOldPlayerWeaponsData();
	TArray<TheShift::Serializables::ItemAcquire>& GetStagedItemData();
	void SetSessionStartedDataStaged(bool Value);
	void SetOldPlayerWeaponsDataStaged(bool Value);
	void SetItemDataStaged(bool Value);
	bool IsSessionStartedDataStaged() const;
	bool IsOldPlayersWeaponsDataStaged() const;
	bool IsItemDataStaged() const;
	void _Editor_StartWithoutMainMenu();
};

#define SendPacket(DATA) Cast<UTheShift_GameInstanceBase>(GetWorld()->GetGameInstance())->Send(DATA)
