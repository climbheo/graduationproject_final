﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Protocol/Serializables.h"
#include "TSPlayerInventoryComponent.generated.h"

class UTSItemComponent;
class ATSPlayer;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class THESHIFT_CLIENT_API UTSPlayerInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UTSPlayerInventoryComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// 현재 inventory에 등록되어 있는 item들
	// Key: slot index
	// Value: ItemComponent
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	TMap<int, UTSItemComponent*> EquipedItems;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	TMap<int, UTSItemComponent*> Items;

	// Inventory의 소유 플레이어
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	ATSPlayer* OwningPlayer;

	UFUNCTION(BlueprintCallable)
	void AcquireItem(UTSItemComponent* Item);

	void AcquireItem(TheShift::Serializables::ItemAcquire& Data);

	UFUNCTION(BlueprintCallable)
	void UnacquireItem(UTSItemComponent* Item);

	UFUNCTION(BlueprintCallable)
	void EquipItem(UTSItemComponent* Item);

	UFUNCTION(BlueprintCallable)
	void UnequipItem(UTSItemComponent* Item);

	UFUNCTION(BlueprintCallable)
	void UnequipItemByIndex(int Index);

	UFUNCTION(BlueprintCallable)
	void RemoveItem(UTSItemComponent* Item);
};
