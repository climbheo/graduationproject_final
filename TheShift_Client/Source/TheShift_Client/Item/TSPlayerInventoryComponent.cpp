﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TSPlayerInventoryComponent.h"


#include "TSArmorItemComponent.h"
#include "TSWeaponItemComponent.h"
#include "Actors/TSPlayer.h"
#include "Item/TSItemComponent.h"

// Sets default values for this component's properties
UTSPlayerInventoryComponent::UTSPlayerInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTSPlayerInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...

}


// Called every frame
void UTSPlayerInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

// Item을 획득한다.
void UTSPlayerInventoryComponent::AcquireItem(UTSItemComponent* Item)
{
	Items.Emplace(Item->Index, Item);
	OwningPlayer->AcquireItem(Item);
}

void UTSPlayerInventoryComponent::AcquireItem(TheShift::Serializables::ItemAcquire& Data)
{
	UTSItemComponent* NewItem;
	switch(static_cast<UItemType>(Data.ItemCategory))
	{
	case UItemType::Weapon:
		NewItem = NewObject<UTSWeaponItemComponent>(this, UTSWeaponItemComponent::StaticClass());
		NewItem->Type = UItemType::Weapon;
		Cast<UTSWeaponItemComponent>(NewItem)->WeaponType = static_cast<UWeaponItemType>(Data.ItemType);
		break;

	case UItemType::Armor:
		NewItem = NewObject<UTSArmorItemComponent>(this, UTSArmorItemComponent::StaticClass());
		NewItem->Type = UItemType::Armor;
		Cast<UTSArmorItemComponent>(NewItem)->ArmorType = static_cast<UArmorItemType>(Data.ItemType);
		break;

	default:
		return;
	}

	NewItem->Index = Data.Index;
	NewItem->RegisterComponent();

	// 아이템 컨테이너에 추가.
	Items.Emplace(NewItem->Index, NewItem);
}

// Inventory에서 아이템을 제거한다.
void UTSPlayerInventoryComponent::UnacquireItem(UTSItemComponent* Item)
{
	UnequipItem(Item);
	//Items.Remove(Item);
	OwningPlayer->UnacquireItem(Item);
	RemoveItem(Item);
}

// 아이템을 장착한다.
void UTSPlayerInventoryComponent::EquipItem(UTSItemComponent* Item)
{
	switch(Item->Type)
	{
	case UItemType::Weapon:
	{
		// 장착
		if(auto WeaponItem = Cast<UTSWeaponItemComponent>(Item))
		{
			OwningPlayer->EquipWeaponFromInventory(WeaponItem->WeaponType);
			EquipedItems.Emplace(1, Item);
			//NotEquipedItems.Remove(Item);
		}
		break;
	}

	case UItemType::Armor:
	{
		// 장착
		if(auto ArmorItem = Cast<UTSArmorItemComponent>(Item))
		{
			OwningPlayer->EquipArmorFromInventory(ArmorItem->ArmorType);
			EquipedItems.Emplace(3, Item);
			//NotEquipedItems.Remove(Item);
		}
		break;
	}

	default:
		return;
	}
}

// 장착된 아이템을 해제한다.
void UTSPlayerInventoryComponent::UnequipItem(UTSItemComponent* Item)
{
	int ShouldBeRemovedKey = -1;
	for(auto& ItemPair : EquipedItems)
	{
		if(ItemPair.Value == Item)
		{
			// 삭제할 아이템의 슬롯 index를 넘겨준다.
			OwningPlayer->UnequipItem(ItemPair.Key);
			ShouldBeRemovedKey = ItemPair.Key;
		}
	}

	if(ShouldBeRemovedKey != -1)
	{
		EquipedItems.Remove(ShouldBeRemovedKey);
		//NotEquipedItems.Emplace(Item);
	}
}

// 해당 index에 장착된 아이템을 장착 해제한다.
void UTSPlayerInventoryComponent::UnequipItemByIndex(int Index)
{
	OwningPlayer->UnequipItem(Index);
}

// 장착된 아이템과 소지중인 아이템 중 동일한 것을 찾아 삭제한다.
void UTSPlayerInventoryComponent::RemoveItem(UTSItemComponent* Item)
{
	UnequipItem(Item);

	int ShouldBeRemovedKey = -1;
	for(auto& ItemPair : Items)
	{
		if(ItemPair.Value == Item)
		{
			ShouldBeRemovedKey = ItemPair.Key;
		}
	}

	if(ShouldBeRemovedKey != -1)
		Items.Remove(ShouldBeRemovedKey);
}

