﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Item/TSItemComponent.h"
#include "Actors/TSWeapon.h"
#include "TSWeaponItemComponent.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class THESHIFT_CLIENT_API UTSWeaponItemComponent : public UTSItemComponent
{
	GENERATED_BODY()

public:
	UTSWeaponItemComponent();
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UWeaponItemType WeaponType;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TSubclassOf<ATSWeapon> WeaponClass;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float Damage;
};
