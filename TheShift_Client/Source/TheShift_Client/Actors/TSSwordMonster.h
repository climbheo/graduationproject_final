﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Actors/TSMonster.h"
#include "TSSwordMonster.generated.h"

/**
 * 
 */
UCLASS()
class THESHIFT_CLIENT_API ATSSwordMonster : public ATSMonster
{
	GENERATED_BODY()
public:
	ATSSwordMonster();

protected:
	virtual void BeginPlay() override;
	virtual void PostInitializeComponents() override;

public:
	virtual void Tick(float DeltaTime) override;


public:
	void Act(TheShift::ActType ActEnum) override;
	void Act(TheShift::ActType ActEnum, FVector Vector) override;
	void Act(TheShift::ActType ActEnum, TheShift::UID TargetActorId) override;
	void Act(TheShift::ActType ActEnum, int Value) override;
	void OnHit(float Hp, float Damage, TheShift::ActType AttackType, TheShift::UID AttackerId, bool IsDead) override;

	virtual void ExportAnimData(FString FileName) override;

	UFUNCTION(BlueprintCallable, Category = Anim)
		void BindNotify();

private:
	UPROPERTY()
		class UAnimSequence* DefaultHitAnim;

	UPROPERTY()
		class UAnimMontage* LockOnMontage;

	UPROPERTY()
		class UAnimSequence* Combo2Montage;

	UPROPERTY()
		class UAnimMontage* Combo3Montage;

	bool IsCutSceneEnded = false;
};
