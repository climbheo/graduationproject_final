﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TSCharacter.h"
#include "Components/TSNullComponent.h"
#include "Network/Components/TSNetworkedComponent.h"
#include "TheShift_ClientGameModeBase.h"
#include "GameFrameWork/WorldSettings.h"
#include "TSDamagable.h"
#include "TSCharacterAnimInstance.h"
#include "TSWeapon.h"
#include "Game/Actors/Actor.h"
#include "Protocol/Log.h"
#include "TSPlayer.h"
#include "Game/Actors/Character.h"
#include "Kismet/KismetMathLibrary.h"


// Sets default values
ATSCharacter::ATSCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Root 재설정
	USceneComponent* OldRoot = RootComponent;
	NullComp = CreateDefaultSubobject<USceneComponent>(TEXT("Null Component"));
	RootComponent = NullComp;
	OldRoot->SetupAttachment(RootComponent);

	NetworkComp = CreateDefaultSubobject<UTSNetworkedComponent>(TEXT("Networked Component"));
	DamagableComp = CreateDefaultSubobject<UTSDamagable>(TEXT("Damagable"));

	// Capsule 위치 조정
	auto Capsule = GetCapsuleComponent();
	Capsule->AddLocalOffset(FVector(0.f, 0.f, Capsule->GetScaledCapsuleHalfHeight()));

	//캐릭터 속도제한
	//TODO: import한 stat에 따라야 함.
	GetCharacterMovement()->MaxWalkSpeed = 600.f;

	AttackEndComboState();
	//CurrentWeaponType = WeaponType::HAND;

	OnDestroyed.AddDynamic(this, &ATSCharacter::OnCharacterDestroyed);

	static ConstructorHelpers::FClassFinder<AActor> teleportEffectActor(TEXT("/Game/Effect/TeleportEffectBP.TeleportEffectBP_C"));
	if (teleportEffectActor.Succeeded())
		TeleportEffectBPClass = teleportEffectActor.Class;

}

// Called when the game starts or when spawned
void ATSCharacter::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ATSCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// 삭제 check
	if(GoodToBeDestroyed == true)
	{
		auto GameMode = Cast<ATheShift_ClientGameModeBase>(GetWorld()->GetAuthGameMode());
		GameMode->MarkActorAsGoodToBeDestroyed(this);
		GameMode->DestroyCorrespondingActor(NetworkComp->GetUID());
	}

	////스프링 암 거리 보간
	//SpringArmComp->TargetArmLength = FMath::FInterpTo(SpringArmComp->TargetArmLength, ArmLengthTo, DeltaTime, ArmLengthSpeed);

	//움직이는 방향에 따라 캐릭터의 회전을 부드럽게 보간한다.
	if(NetworkComp->GetUID() != INVALID_UID)
	{
		FVector DirectionNormal = DirectionToMove.GetSafeNormal();
		FRotator FinalRotator = GetActorRotation();
		FinalRotator = FMath::Lerp(FinalRotator, DirectionNormal.Rotation(), RotationSpeed);

		SetActorRotation(FinalRotator);
	}
}

const WeaponType ATSCharacter::GetCurrentWeaponType() const
{
	return CurrentWeaponType;
}



void ATSCharacter::PlayAttackMontage(UAnimMontage* AnimMontage, int32 SectionNumber)
{
	if(CurrentCombo != SectionNumber)
	{
		CurrentCombo = SectionNumber;
		CurrentAnimInstance->JumpToAttackMontageSection(AnimMontage, CurrentCombo);

	}
}

void ATSCharacter::SetGoalRotation(float Pitch, float Yaw, float Roll)
{
	//UE_LOG(LogTemp, Log, TEXT("Goal!!!!!!!!!"));
	DirectionToMove = FRotator(Pitch, Yaw, Roll).Vector();
}

void ATSCharacter::Act(TheShift::ActType ActEnum)
{
}

void ATSCharacter::Act(TheShift::ActType ActEnum, FVector Vector)
{
}

void ATSCharacter::Act(TheShift::ActType ActEnum, TheShift::UID TargetActorId)
{
}

void ATSCharacter::Act(TheShift::ActType ActEnum, std::vector<TheShift::UID>& IdsOfTargets)
{
}

void ATSCharacter::Act(TheShift::ActType ActEnum, int32 Value)
{
	switch(ActEnum)
	{
	case ActType::WeaponSwap:
	{
		WeaponType Type = static_cast<WeaponType>(Value);
		auto Element = WeaponsOnSlot.Find(Type);

		// Player doesn't have this type of weapon.
		// Hand와 Equip_None은 Element가 nullptr인게 맞음.
		if(Element == nullptr && Type != WeaponType::HAND && Type != WeaponType::EQUIP_NONE)
		{
			LOG_ERROR("Trying to swap weapon to WeaponType: %d. Can't find weapon.", Type);
			return;
		}

		// Swap weapon
		else
			SwapWeapon(Type);

		break;
	}

	default:
		return;
	}
}

void ATSCharacter::Act(TheShift::Serializables::Act* Data)
{
	switch(static_cast<TheShift::Serializables::Type>(Data->ClassId()))
	{
	case TheShift::Serializables::Type::Act:
	{
		Act(Data->Type);
		break;
	}

	case TheShift::Serializables::Type::ActWithValue:
	{
		Act(Data->Type, reinterpret_cast<TheShift::Serializables::ActWithValue*>(Data)->Value);
		break;
	}

	case TheShift::Serializables::Type::ActWithTarget:
	{
		Act(Data->Type, reinterpret_cast<TheShift::Serializables::ActWithTarget*>(Data)->TargetId);
		break;
	}

	case TheShift::Serializables::Type::ActWithTargets:
	{
		Act(Data->Type, reinterpret_cast<TheShift::Serializables::ActWithTargets*>(Data)->IdsOfTargets);
		break;
	}

	case TheShift::Serializables::Type::ActWithVector:
	{
		auto Vector = reinterpret_cast<TheShift::Serializables::ActWithVector*>(Data)->Vector;
		Act(Data->Type, FVector(Vector.X, Vector.Y, Vector.Z));
		break;
	}

	default:
	{

		return;
	}
	}
}

void ATSCharacter::OnHit(float Hp, float Damage, TheShift::ActType AttackType, TheShift::UID AttackerId, bool IsDead)
{
	PlayHitEffect(FLinearColor::Red);
}

void ATSCharacter::OnRecognize(TheShift::UID OtherActorId)
{

}

float ATSCharacter::GetCurrentHp() const
{
	return CurrentHp;
}

void ATSCharacter::SetCurrentHp(float NewHp)
{
	CurrentHp = NewHp;
}

void ATSCharacter::PlayHitEffect(FLinearColor HitColor)
{
	auto Mesh = GetMesh();
	if (Mesh)
	{
		Mesh->SetVectorParameterValueOnMaterials("EffectColor", UKismetMathLibrary::Conv_LinearColorToVector(HitColor));
		Mesh->SetScalarParameterValueOnMaterials("StartTime", UGameplayStatics::GetTimeSeconds(GetWorld()));
	}
}
	
	// Player Input처리
	void ATSCharacter::PushInputQueue(TheShift::Serializables::Input Input)
	{
		auto GameMode = Cast<ATheShift_ClientGameModeBase>(GetWorld()->GetAuthGameMode());

		// Client의 Input이 GameState에 직접 반영됨.
		std::shared_ptr<TheShift::Actor> Actor = GameMode->GetCorrespondingActor(NetworkComp->GetUID());
		if(Actor != nullptr)
		{
			if (auto Player = Cast<ATSPlayer>(this))
			{
				bool Dash = false;
				int DashValue = 0;
				Player->GetDashInfo(&Dash, &DashValue);

				// Input에 대한 결과를 기다리지 않고 바로 처리함.
				if(!Dash && !Player->IsTeleporting())
					Actor->OnInput(Input);
			}
			GameMode->PushInputQueue(Input);
		}
	}

// Weapon Slot에 장착된 무기로 변경한다.
void ATSCharacter::SwapWeapon(WeaponType Type)
{
	ATSWeapon** WeaponPtr = WeaponsOnSlot.Find(Type);
	if(WeaponPtr == nullptr)
	{
		LOG_ERROR("Trying to swap weapon: This character doesn't have this type of weapon %d.", Type);
		return;
	}

	else
	{
		switch (Type)
		{
		case WeaponType::SWORD:
			MaxCombo = 4;
			break;

		case WeaponType::HAND:
			MaxCombo = 3;
			break;

		case WeaponType::BOW:
			MaxCombo = 3;
			break;

		}


		LOG_INFO("SwapWeapon()");
		// 기존에 손에 들고 있던 무기는 보이지 않게 함
		if(CurrentWeapon != nullptr)
			CurrentWeapon->SetActorHiddenInGame(true);

		// 무기정보 수정
		CurrentWeaponType = Type;
		CurrentWeapon = *WeaponPtr;

		// 새로 손에 든 Hidden 풀어주기
		if(CurrentWeapon != nullptr)
			CurrentWeapon->SetActorHiddenInGame(false);
		//TODO: Mesh도 변경해줘야...? 그냥 바뀌진 않을텐데
	}
}

void ATSCharacter::ExportAnimData(FString FileName)
{
	std::vector<AnimNotifyInfo> NotifyInfoContainer;

	//SlotAnims
	if (ExportAnims.Num() == AnimPlayRates.Num())
	{
		for (int i = 0; i < ExportAnims.Num(); ++i)
			UAnimNotifiesExporter::ExportSlotAnimNotifies(&NotifyInfoContainer, ExportAnims[i], AnimPlayRates[i]);
	}
	//Json에 애님Notify정보 기입
	JsonFormatter::JsonWriter JsonExporter;
	FString TargetPath = FPaths::ProjectContentDir() + TEXT("Protocol/Json/Notifies/") + FileName + TEXT(".json");
	JsonExporter.ExportAnimNotifyInfo(TCHAR_TO_UTF8(*TargetPath), NotifyInfoContainer);
	JsonExporter.Close();
}

UTSCharacterAnimInstance* ATSCharacter::GetAnimInstance()
{
	return CurrentAnimInstance;
}

void ATSCharacter::SetDead(bool IsPlayer)
{
	IsDead = true;

	if (!IsPlayer)
	{
		SetActorEnableCollision(false);
		GetMesh()->SetHiddenInGame(false);
		bCanBeDamaged = false;
	}
	else
	{
		//플레이어액터는 그 자리에 두고 죽는 애니메이션 재생만한다.
		auto GameMode = Cast<ATheShift_ClientGameModeBase>(GetWorld()->GetAuthGameMode());
		auto ServerPlayer = std::reinterpret_pointer_cast<Character>(GameMode->GetGameState()->GetActor(NetworkComp->GetUID()));
		//클라에서 작동하는 이동로직도 잠군다.
		if(ServerPlayer)
			ServerPlayer->LockInput();
	}
	//언렬 Input도아예잠군다.
	//이건 생각을좀 해볼게 죽엇어도 캠도누르고 키를누를상황이 꽤나올수잇음 일단 보류!
	/*if(IsPlayerControlled())
		DisableInput(Cast<APlayerController>(GetController()));*/
	//else
	//	ABAIController->StopAI();
}

void ATSCharacter::OnCharacterDestroyed(AActor* DestroyedActor)
{
	if(CurrentWeapon)
	{
		CurrentWeapon->Destroy();
	}
}

// Slot에 해당 type의 weapon이 있는지 확인하고 장착한다.
// Slot에 새로운 무기를 추가하기 위해서는 ATSCharacter::AddWeaponOnSlot()을 사용해야 함.
void ATSCharacter::EquipWeapon(WeaponType TypeOfWeapon)
{
	if(WeaponsOnSlot.Contains(TypeOfWeapon))
	{
		CurrentWeaponType = TypeOfWeapon;
		CurrentWeapon = WeaponsOnSlot[TypeOfWeapon];
	}
	else
	{
		LOG_ERROR("Trying to equip weapon type %d. Can't find weapon.", TypeOfWeapon);
	}
}

// Slot에 해당 무기를 추가한다.
// 무기를 장착(손에 들기)하기 위해서는 ATSCharacter::EquipWeapon()을 사용해야 함.
void ATSCharacter::AddWeaponOnSlot(ATSWeapon* WeaponToAdd)
{
	WeaponType TypeOfWeapon = WeaponToAdd->GetWeaponType();

	// 무기를 추가할 수 있는지 먼저 확인한다.
	if(!ATSWeapon::WeaponSockets.Contains(TypeOfWeapon))
	{
		LOG_ERROR("Trying to add weapon on slot. Can't find socket name.");
		return;
	}
	if(!GetMesh()->DoesSocketExist(ATSWeapon::WeaponSockets[TypeOfWeapon]))
	{
		LOG_ERROR("Trying to add weapon on slot. Can't find slot on mesh.");
		return;
	}

	// WeaponsOnSlot에 무기를 추가함
	auto Element = WeaponsOnSlot.Find(TypeOfWeapon);
	if(Element != nullptr)
	{
		//TODO: 이미 원소가 있다면 변경해주어야 한다. (여기서 말고 이 함수를 부르는 곳에서)
		// 이미 있다면 덮어씌운다.
		WeaponsOnSlot[TypeOfWeapon] = WeaponToAdd;

	}
	else
	{
		// 같은 타입이 없는데 개수가 최대를 초과하지 않을때만 추가한다.
		if(WeaponsOnSlot.Num() >= MaxWeaponSlot)
		{
			// 더이상 slot에 추가할 수 없으므로 return
			LOG_INFO("Weapon slot is full. Can't add anymore.");
			return;
		}
		else
			WeaponsOnSlot.Add(TypeOfWeapon, WeaponToAdd);
	}

	if(GetMesh()->DoesSocketExist(ATSWeapon::WeaponSockets[TypeOfWeapon]))
	{
		// 무기를 붙임
		WeaponsOnSlot[TypeOfWeapon]->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, ATSWeapon::WeaponSockets[TypeOfWeapon]);
		// 무기를 장착하기 전까지는 보이지 않게 함.
		WeaponsOnSlot[TypeOfWeapon]->SetActorHiddenInGame(true);
	}
}

void ATSCharacter::AttackStartComboState()
{
	CanNextCombo = true;
	IsComboInputOn = false;
	CurrentCombo = FMath::Clamp<int32>(CurrentCombo + 1, 1, MaxCombo);
}

void ATSCharacter::AttackEndComboState()
{
	IsComboInputOn = false;
	CanNextCombo = false;
	CurrentCombo = 0;
	IsAttacking = false;
}
