﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Actors/TSCharacter.h"
#include "Misc/UItemTypes.h"
#include "TSPlayer.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnHpChange);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnStaminaChange);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnTeleportGageChange);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnStatChange);

class UTSItemComponent;
/**
 *
 */
UCLASS()
class THESHIFT_CLIENT_API ATSPlayer : public ATSCharacter
{
	GENERATED_BODY()

public:
	ATSPlayer();

protected:
	virtual void BeginPlay() override;
	virtual void PostInitializeComponents() override;
	virtual void BeginDestroy() override;
	virtual void Destroyed() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(BlueprintAssignable, VisibleAnywhere, BlueprintCallable, Category = "Event")
	FOnHpChange HpChangeEvent;
	UPROPERTY(BlueprintAssignable, VisibleAnywhere, BlueprintCallable, Category = "Event")
	FOnStaminaChange StaminaChangeEvent;
	UPROPERTY(BlueprintAssignable, VisibleAnywhere, BlueprintCallable, Category = "Event")
	FOnTeleportGageChange TeleportGageChangeEvent;
	UPROPERTY(BlueprintAssignable, VisibleAnywhere, BlueprintCallable, Category = "Event")
	FOnStatChange StatChangeEvent;

	void Act(TheShift::ActType ActEnum) override;
	void Act(TheShift::ActType ActEnum, FVector Vector) override;
	void Act(TheShift::ActType ActEnum, TheShift::UID TargetActorId) override;
	void Act(TheShift::ActType ActEnum, std::vector<TheShift::UID>& IdsOfTargets) override;
	void Act(TheShift::ActType ActEnum, int Value) override;
	void OnHit(float Hp, float Damage, TheShift::ActType AttackType, TheShift::UID AttackerId, bool IsDead) override;

	//방향 input Init
	void InitDirection();

	class UTSCharacterAnimInstance* GetCurrentAnimInstance();

public:
	void GetDashInfo(bool* IsDash, int* DashDirection);
	int GetComboCnt() { return CurrentCombo; }
	bool IsTeleporting() { return Teleport; }
	bool IsAiming() { return Aiming; }
	bool IsReleasedAim() { return ReleasedAim; }
	bool IsPlayerAttacking() { return IsAttacking; }

	void SetWeaponVisibility(bool IsRender = true);

	UFUNCTION(BlueprintCallable, Category = "Armor")
	void SwapArmor(UArmorItemType NewArmorType);


	//이 Client가 직접조종하는 Player인가?
	bool IsMyPlayer();


protected:

	void LoadMontages();

	//Action -> 몇번이고 명심하자!! 이 함수들은 자신의 캐릭터만 호출한다./////////
	//Axis
	void ForwardBack(float AxisValue);
	void LeftRight(float AxisValue);
	void Turn(float AxisValue);
	void LookUp(float AxisValue);

	void OnForwardPressed();
	void OnForwardReleased();
	void OnBackPressed();
	void OnBackReleased();
	void OnRightPressed();
	void OnRightReleased();
	void OnLeftPressed();
	void OnLeftReleased();
	void OnBaseAttackPressed();
	void OnBaseAttackReleased();
	void OnSmashPressed();
	void OnTeleportPressed();
	void OnShiftPressed();
	void OnShiftReleased();
	void OnWeaponSwapPressed(WeaponType TypeValue);
	void OnCheat();
	template<WeaponType WeaponInput>
	void OnWeaponSwapPressed() { OnWeaponSwapPressed(WeaponInput); }
	void OnDashPressed();
	void OnEscape();
	void OnLevelChange_Level01();
	void OnLevelChange_Level02();
	void OnLevelChange_Level03();
	void OnLevelChange_Level04();
	void OnLevelChange_Landscape();
	void OnMoveToStart();
	void OnMoveToMiddle();
	void OnMoveToBoss();
	void OnMoveToEnd();
	void OnGodMode();
	void SendBowTargets();
	void LockServerActorInput(bool IsLock);

	//WitcherSense Mesh Visibility Function
	void SetWitcherSenseMeshVisibility();
	
	//Bow Target UI On/Off Function
	void RenderBowTargetUI(bool IsRender = true);

	//캐릭터 Control관련 Function
	void InitControlMode(bool bRotateInit = false);

	//애님몽타주 끝날시 실행되는 함수
	UFUNCTION()
	void OnAttackMontageEnded(UAnimMontage* Montage, bool bInterrupted);



	virtual void ExportAnimData(FString FileName) override;

	//회전 계산 함수
	FVector CalculateGoalRotation(bool bAttacking = false);
	void CalculateRoationAfterAttack();
	void ApplyNonAccumulatedInput();

	//Bow Cam Move
	void AttachCameraToBowCamSpot();

	//Bow Teleport Target
	void FindBowTeleportTarget();

	//BindNotify
	void BindNotify();


public:
	//Direction Variables -> 가끔 누적되는상황이있음(Input이 잠겨있을때 회전 반영 X)
	bool Forward = false;
	bool Back = false;
	bool Left = false;
	bool Right = false;

	//Non Accumulated Direction Variables -> 누적되지 않는 
	bool NonAccumulatedForward = false;
	bool NonAccumulatedBack = false;
	bool NonAccumulatedLeft = false;
	bool NonAccumulatedRight = false;


	//TODO: 임시로 Stamina랑 TeleportGage 2000으로 많이 올려놨음. 적정치를 찾아서 수정해야 함.
	// Teleport gage 최대치
	UPROPERTY(BlueprintReadOnly)
	int MaxTeleportGage = 2000;

	// Teleport 기술의 gage 소모량?
	UPROPERTY(BlueprintReadOnly)
	int TeleportGageConsumption = 50;
	
	// UI에 표시될 현재 teleport gage 
	UPROPERTY(BlueprintReadOnly)
	int CurrentTeleportGage = 2000;

	// UI에 표시될 현재 stamina
	UPROPERTY(BlueprintReadOnly)
	int CurrentStamina = 2000;

	// Stamina 최대치
	UPROPERTY(BlueprintReadOnly)
	int MaxStamina = 2000;

	//캐릭터에 붙어 회전하는 스프링 암 컴포넌트
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite, Category = Camera)
	class USpringArmComponent* SpringArmComp;

	//스프링 암의 끝에 붙어있는 카메라 컴포넌트 (스프링 암의 자식)
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera)
	class UCameraComponent* CameraComp;

	//화살통 렌더링하는 스태틱 메쉬 컴포넌트 (메쉬의 자식)
	UPROPERTY(VisibleAnywhere, Category = Camera)
	class UStaticMeshComponent* QuiverMeshComp;

	//화살 렌더링하는 스태틱 메쉬 컴포넌트 (메쉬의 자식)
	UPROPERTY(VisibleAnywhere, Category = Camera)
	class UStaticMeshComponent* ArrowMeshComp;

	// Inventory
	UPROPERTY(BlueprintReadWrite, Category = Inventory)
	class UTSPlayerInventoryComponent* InventoryComp;

	UPROPERTY()
	class UAnimSequence* SwordHitAnim;

	UPROPERTY()
	class UAnimSequence* GrapplerHitAnim;

	UPROPERTY()
	class UAnimSequence* BigHitAnim;

	UPROPERTY()
	class UStaticMesh* QuiverMesh;

	UPROPERTY()
	class UStaticMesh* ArrowMesh;

	// Items, Inventory
	UFUNCTION(BlueprintCallable)
	void EquipWeaponFromInventory(UWeaponItemType Type);

	UFUNCTION(BlueprintCallable)
	void EquipArmorFromInventory(UArmorItemType Type);

	UFUNCTION(BlueprintCallable)
	void UnequipItem(int SlotIndex);

	void AcquireItem(UTSItemComponent* Item);
	void UnacquireItem(UTSItemComponent* Item);

	UFUNCTION(BlueprintCallable)
	bool CheckItemEquiped(UTSItemComponent* Item);

	UFUNCTION(BlueprintCallable)
	int GetEquipedItemSlot(UTSItemComponent* Item);
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attack, Meta = (AllowPrivateAccess = true))
	TArray<class UAnimSequence*> SwordSmashAnims;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attack, Meta = (AllowPrivateAccess = true))
	TArray<class UAnimSequence*> GrapplerSmashAnims;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Armor, Meta = (AllowPrivateAccess = true))
	TArray<class USkeletalMesh*> PlayerMeshes;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Armor, Meta = (AllowPrivateAccess = true))
	TArray<TSubclassOf<class UAnimInstance>> PlayerAnimInstances;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Armor, Meta = (AllowPirvateAccess = true))
	TArray<class UAnimMontage*> HandAttackMontage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Armor, Meta = (AllowPirvateAccess = true))
	TArray<class UAnimMontage*> HandTeleportMontage;

	//SpringArm 거리 관련 변수
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = Camera, Meta = (AllowPrivateAccess = true))
	float ArmLengthTo = 0.f;
	UPROPERTY()
	float ArmLengthSpeed = 0.f;

	//대쉬 여부와 방향 변수
	UPROPERTY()
	bool Dash = false;
	UPROPERTY()
	int DashValue = 0;

	//순간이동기술 중인가
	UPROPERTY()
	bool Teleport = false;

	//에이밍 중인가
	UPROPERTY()
	bool Aiming = false;

	//에임을 풀었는가
	UPROPERTY()
	bool ReleasedAim = false;

	//Smash Anim Play Rates
	float SmashAnimPlayRates[5][20] = {};

	UPROPERTY()
	class UParticleSystem* DustParticle;

	//공격 중에 들어온 Input에 대한 회전값 계산을 위한 변수
	float AttackRotateXValue = 0.f;
	float AttackRotateYValue = 0.f;


	//Item쪽으로 넘길 임시변수
	UArmorItemType ArmorType = UArmorItemType::None;
};
