﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TSWeapon.h"
#include "Protocol/Log.h"
#include "Animation/AnimSequence.h"

TMap<FName, class USkeletalMesh*> ATSWeapon::Meshes;
TMap<WeaponType, FName> ATSWeapon::WeaponSockets;

// Sets default values
ATSWeapon::ATSWeapon()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	static bool SocketsInitialized = false;
	if(!SocketsInitialized)
	{
		// Socket name 추가.
		WeaponSockets.Emplace(WeaponType::SWORD, FName("SwordHandlingSocket"));
		WeaponSockets.Emplace(WeaponType::DAGGER, FName("DaggerHandlingSocket"));
		WeaponSockets.Emplace(WeaponType::BOW, FName("BowHandlingSocket"));
		SocketsInitialized = true;
	}

	MeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));

	//기본 Sword 메쉬
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> DefaultSwordMesh(TEXT("/Game/Graphic/Weapon/NewGreatSword/GreatSword_.GreatSword_"));
	if(DefaultSwordMesh.Succeeded())
	{
		Meshes.Emplace(FName("GreatSword"), DefaultSwordMesh.Object);
	}

	//기본 활 메쉬
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> DefaultBowMesh(TEXT("/Game/Graphic/Weapon/Bow/Bow.Bow"));
	if (DefaultBowMesh.Succeeded())
	{
		Meshes.Emplace(FName("Bow"), DefaultBowMesh.Object);
	}

	//swordHarmed
	static ConstructorHelpers::FObjectFinder <UAnimSequence> DefaultBowAnim(TEXT("/Game/Graphic/Weapon/Bow/Bow_Ani.Bow_Ani"));
	if (DefaultBowAnim.Succeeded())
		BowAnim = DefaultBowAnim.Object;


	RootComponent = MeshComponent;

	Tags.Add("Weapon");
}

// Called when the game starts or when spawned
void ATSWeapon::BeginPlay()
{
	Super::BeginPlay();
	SetActorEnableCollision(false);
}

// Called every frame
void ATSWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

const WeaponType ATSWeapon::GetWeaponType() const
{
	return TypeOfWeapon;
}

// 무기의 type을 정하고 mesh를 정해준다.
void ATSWeapon::InitWeapon(FName Name, WeaponType Type)
{
	auto Mesh = Meshes.Find(Name);
	if(Mesh != nullptr)
	{
		WeaponName = Name;
		MeshComponent->SetSkeletalMesh(*Mesh);
		TypeOfWeapon = Type;
		if (Type == WeaponType::SWORD)
			Tags.Add("Sword");
	}
	else
		LOG_ERROR("Can't find weapon mesh: %s", TCHAR_TO_ANSI(*Name.ToString()));
}

void ATSWeapon::PlayBowAnim()
{
	if (TypeOfWeapon == WeaponType::BOW)
	{
		if (MeshComponent)
			MeshComponent->PlayAnimation(BowAnim, false);
	}
}

