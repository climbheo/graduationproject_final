// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TSDamagable.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class THESHIFT_CLIENT_API UTSDamagable : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UTSDamagable();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	const bool IsActorDamagable() const;
	void SetDamagable(bool Input = true);

	const bool IsActorHit() const;
	void SetHit(bool Input = true);

private:
	//현재 대미지를 줄 수 있는가?
	UPROPERTY(EditAnywhere, Category = Damage)
	bool IsDamagable = false;

	//현재 Hit 상태인가?
	UPROPERTY(EditAnywhere, Category = Damage)
	bool IsHit = false;
};
