﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "TSWeapon.h"
#include "TSCharacterAnimInstance.generated.h"

DECLARE_MULTICAST_DELEGATE(FOnNextAttackCheckDelegate);
DECLARE_MULTICAST_DELEGATE(FOnHitCheckDelegate);
DECLARE_MULTICAST_DELEGATE(FOnHitResetDelegate);
DECLARE_MULTICAST_DELEGATE(FOnZoomInDelegate);
DECLARE_MULTICAST_DELEGATE(FOnZoomOutDelegate);
DECLARE_MULTICAST_DELEGATE(FOnTeleportEffectDelegate);
DECLARE_MULTICAST_DELEGATE(FOnDestroyDelegate);
DECLARE_MULTICAST_DELEGATE(FOnCameraShakeDelegate);
DECLARE_MULTICAST_DELEGATE(FOnCameraDragBackDelegate);
/**
 *
 */
UCLASS()
class THESHIFT_CLIENT_API UTSCharacterAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:
	UTSCharacterAnimInstance();
	virtual void NativeUpdateAnimation(float DeltaSeconds) override;

public:
	void PlayAttackMontage(UAnimMontage* Montage);
	void JumpToAttackMontageSection(UAnimMontage* Montage, int32 NewSection);
	//void SetNormalAttackMontage(TCHAR* Name);
	void RestartAnimation(class UAnimSequence* Anim, FName SlotName = FName("DefaultSlot"), float PlayRate = 1.f,int LoopCount = 1);
	void StopAnim();



public:
	//다음 Combo로 넘어갈때 호출되는 Delegate
	FOnNextAttackCheckDelegate OnNextAttackCheck;

	//공격(충돌) 체크를 해야할때 호출되는 Delegate
	FOnHitCheckDelegate OnHitCheck;

	//피격애니메이션이 끝나면 호출되는 Delegate
	FOnHitResetDelegate OnHitReset;

	//ZoomIn
	FOnZoomInDelegate OnZoomInWork;

	//ZoomOut
	FOnZoomOutDelegate OnZoomOutWork;

	//Destroy
	FOnDestroyDelegate OnDestroyWork;

	//TeleportEffect
	FOnTeleportEffectDelegate OnTeleportEffectWork;

	//CamShake
	FOnCameraShakeDelegate OnCameraShakeWork;
	//CamDragBack;
	FOnCameraDragBackDelegate OnCameraDragBackWork;

	//////////////// 활 조준중에 재생되는 BlendSpace에 쓰일 변수->애님BP와 연동 X 보간용임.
	UPROPERTY(Transient)
		float GoalHorizonMove = 0.f;
	UPROPERTY(Transient)
		float GoalVerticalMove = 0.f;
	//Controller의 회전->Bow모드일때 반영
	UPROPERTY(Transient)
		float VerticalRotation;
	///////////////

private:
	UFUNCTION()
	void AnimNotify_NextAttackCheck();

	UFUNCTION()
	void AnimNotify_HitCheck();

	UFUNCTION()
	void AnimNotify_HitReset();

	UFUNCTION()
	void AnimNotify_ZoomIn();

	UFUNCTION()
	void AnimNotify_ZoomOut();

	UFUNCTION()
	void AnimNotify_Destroy();

	UFUNCTION()
		void AnimNotify_TeleportEffect();

	UFUNCTION()
		void AnimNotify_CameraShake();

	UFUNCTION()
		void AnimNotify_CameraDragBack();


protected:
	// NetWorkComp의 Speed를 BlendSpace에 사용하기 위해 받아오는 변수
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Pawn, Meta = (AllowPrivateAccess = true))
	FVector CurrentSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	bool IsHit = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
		bool IsBigHit = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Pawn, Meta = (AllowPrivateAccess = true))
	WeaponType CurrentWeaponType;

	//장비 교체중인가?
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Pawn, Meta = (AllowPrivateAccess = true))
	bool IsSwapping = false;

	//액터가 죽는중인가?
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Pawn, Meta = (AllowPrivateAccess = true))
	bool IsDead = false;

	//대쉬 여부와 대쉬 방향 관련 변수
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Pawn, Meta = (AllowPrivateAccess = true))
		bool IsDash = false;

	//캐스팅 중인가
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Pawn, Meta = (AllowPrivateAccess = true))
		bool IsTeleporting = false;

	//에이밍 중인가
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Pawn, Meta = (AllowPrivateAccess = true))
		bool IsAiming = false;

	//에이밍을 풀었는가
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Pawn, Meta = (AllowPrivateAccess = true))
		bool IsReleasedAim = false;

	//공격중인가
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Pawn, Meta = (AllowPrivateAccess = true))
		bool IsAttacking = false;

	//콤보 카운트
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Pawn, Meta = (AllowPrivateAccess = true))
		int CurrentCombo = false;

	//애님과 연동되는 활 BlendSpace 변수
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Pawn, Meta = (AllowPrivateAccess = true))
		float HorizonMove;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Pawn, Meta = (AllowPrivateAccess = true))
		float VerticalMove;
	//활 Aiming상태에서 회전을 컨트롤하기 위한 변수
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Pawn, Meta = (AllowPrivateAccess = true))
		FRotator BowControlRotation;

	/////////////////////////////////////

	//대쉬 방향을 나타내는 변수
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Pawn, Meta = (AllowPrivateAccess = true))
		int DashValue = 0;

	// 일반 공격몽타주
	//UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Attack, Meta = (AllowPrivateAccess = true))
	//UAnimMontage* NormalAttackMontage;

	TWeakObjectPtr<class ATSCharacter> Character;

	//Hit시 잠깐 애님 멈추는 용의 변수들
	UPROPERTY()
	UAnimMontage* CurMontage;
	FTimerHandle ResetAnimTimerHandle = {};
};
