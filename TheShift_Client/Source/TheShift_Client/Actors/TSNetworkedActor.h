﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Network/Components/TSNetworkedComponent.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TSNetworkedActor.generated.h"

UCLASS()
class THESHIFT_CLIENT_API ATSNetworkedActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATSNetworkedActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY()
	UTSNetworkedComponent* NetworkedComponent;

public:
	FVector CurrentSpeed;

};
