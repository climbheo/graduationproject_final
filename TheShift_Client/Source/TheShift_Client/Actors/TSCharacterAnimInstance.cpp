﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TSCharacterAnimInstance.h"
#include "TSCharacter.h"
#include "TSPlayer.h"
#include "Animation/AnimInstanceProxy.h"
#include "Animation/AnimNode_SequencePlayer.h"
#include "Animation/AnimNode_StateMachine.h"
#include "Network/Components/TSNetworkedComponent.h"
#include "Animation/AnimSequence.h"

UTSCharacterAnimInstance::UTSCharacterAnimInstance()
{

}

void UTSCharacterAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	auto Pawn = TryGetPawnOwner();
	if(!::IsValid(Pawn))
		return;

	if(!Character.IsValid())
		Character = Cast<ATSCharacter>(Pawn);

	if(Character.IsValid())
	{
		CurrentWeaponType = Character->GetCurrentWeaponType();
		IsDead = Character->GetDead();
		IsHit = Character->IsOnlySmallHit();
		IsBigHit = Character->IsBigHit();
		if (auto Player = Cast<ATSPlayer>(Character))
		{
			//여기서 플레이어의 Anim관련변수들을 갱신
			Player->GetDashInfo(&IsDash, &DashValue);
			IsTeleporting = Player->IsTeleporting();
			IsAiming = Player->IsAiming();
			IsReleasedAim = Player->IsReleasedAim();
			IsAttacking = Player->IsPlayerAttacking();
			
			CurrentCombo = Player->GetComboCnt();

			//활 상체 회전관련 구문
			BowControlRotation = Player->GetControlRotation();
			BowControlRotation.Roll = 0.f;

			FVector CurControllerRotation = FRotator(0.f, BowControlRotation.Yaw, 0.f).Vector();
			FVector CurPlayerRotation = FRotator(0.f, Player->GetActorRotation().Yaw, 0.f).Vector();
			FVector CurPlayerRightRotation = FRotator(0.f, Player->GetActorRotation().Yaw+90.f, 0.f).Vector();

			float DegreeDiff = FMath::RadiansToDegrees(acosf(CurControllerRotation.CosineAngle2D(CurPlayerRotation)));
			if (FVector::DotProduct(CurControllerRotation, CurPlayerRightRotation) < 0.f)
				DegreeDiff *= -1.f;

			//UE_LOG(LogTemp, Log, TEXT("yaw : %f"), DegreeDiff);

		/*	if (BowControlRotation.Yaw <= 90.f && BowControlRotation.Yaw >= 45.f)
				BowControlRotation.Yaw = 45.f;

			if (BowControlRotation.Yaw >= 270.f && BowControlRotation.Yaw <= 315.f)
				BowControlRotation.Yaw = 315.f;*/

			if (BowControlRotation.Pitch <= 90.f && BowControlRotation.Pitch >= 45.f)
				BowControlRotation.Pitch = 45.f;

			if (BowControlRotation.Pitch >= 270.f && BowControlRotation.Pitch <= 315.f)
				BowControlRotation.Pitch = 315.f;

			/*if (!IsTeleporting)
				BowControlRotation.Pitch = 360.f - BowControlRotation.Pitch;
			else
			{*/
				if (DegreeDiff >= 45.f)
					DegreeDiff = 45.f;
				else if(DegreeDiff <=-45.f)
					DegreeDiff = -45.f;
			/*
				UE_LOG(LogTemp, Log, TEXT("DegreeDiff : %f"), DegreeDiff);
				UE_LOG(LogTemp, Log, TEXT("Pitch : %f"), BowControlRotation.Pitch);*/
				BowControlRotation.Yaw = -DegreeDiff;

			
			/*	
			/*if (!IsTeleporting)
				BowControlRotation.Yaw = 360.f - BowControlRotation.Yaw;*/

			///////////////
		}

		
	}

	HorizonMove = FMath::Lerp(HorizonMove, GoalHorizonMove+VerticalRotation, 0.1f);
	VerticalMove = FMath::Lerp(VerticalMove, GoalVerticalMove, 0.1f);

	auto NetWorkComponent = Pawn->GetComponentByClass(UTSNetworkedComponent::StaticClass());
	if(NetWorkComponent != nullptr)
	{
		auto Component = Cast<UTSNetworkedComponent>(NetWorkComponent);
		if(Component != nullptr)
			CurrentSpeed = Component->GetCurrentSpeed();
	}
}

void UTSCharacterAnimInstance::PlayAttackMontage(UAnimMontage* Montage)
{
	if(Character.IsValid())
	{
		if(!Montage_IsPlaying(Montage))
			Montage_Play(Montage, 1.f);
	}
}

void UTSCharacterAnimInstance::JumpToAttackMontageSection(UAnimMontage* Montage, int32 NewSection)
{
	if (Montage_IsPlaying(Montage))
	{
		Montage_JumpToSection(FName(*FString::Printf(TEXT("Attack%d"), NewSection)), Montage);
		UE_LOG(LogTemp, Log, TEXT("Already Playing : %s"), *FName(*FString::Printf(TEXT("Attack%d"), NewSection)).ToString());
	}
	else
	{
		Montage_Play(Montage);
		Montage_JumpToSection(FName(*FString::Printf(TEXT("Attack%d"), NewSection)), Montage);
		UE_LOG(LogTemp, Log, TEXT("Start Playing : %s"), *FName(*FString::Printf(TEXT("Attack%d"), NewSection)).ToString());
	}

}

void UTSCharacterAnimInstance::RestartAnimation(UAnimSequence* Anim, FName SlotName, float PlayRate, int LoopCount)
{
	//UE_LOG(LogTemp, Log, TEXT("Anim Call!!!!!!!!!!!!!!!!!!!!"));
	StopAllMontages(0.25f);
	//PlaySlotAnimationAsDynamicMontage(Anim, FName("DefaultSlot"), 0.25f, 0.25f, PlayRate);
	PlaySlotAnimation(Anim, SlotName, 0.25f, 0.25f, PlayRate, LoopCount);
	//UE_LOG(LogTemp, Log, TEXT("Play Rate : %f"), PlayRate);
}

void UTSCharacterAnimInstance::StopAnim()
{
	CurMontage = GetCurrentActiveMontage();
	Montage_Pause(CurMontage);
	auto Pawn = TryGetPawnOwner();
	if (!::IsValid(Pawn))
		return;

	float StopTime = 0.1f;

	auto Player = Cast<ATSPlayer>(Character);
	if (Player)
		StopTime = Player->GetCurrentWeaponType() == WeaponType::SWORD ? 0.15f : 0.1f;


	GetWorld()->GetTimerManager().SetTimer(ResetAnimTimerHandle, FTimerDelegate::CreateLambda([this]()->void {
		Montage_Resume(CurMontage);

	 }), StopTime, false);

}

//void UTSCharacterAnimInstance::SetNormalAttackMontage(TCHAR* Name)
//{
	//static ConstructorHelpers::FObjectFinder<UAnimMontage> DefaultNormalAttackMontage(Name);
	//if(DefaultNormalAttackMontage.Succeeded())
		//Montage = DefaultNormalAttackMontage.Object;
//}

void UTSCharacterAnimInstance::AnimNotify_NextAttackCheck()
{
	OnNextAttackCheck.Broadcast();
}

void UTSCharacterAnimInstance::AnimNotify_HitCheck()
{
	OnHitCheck.Broadcast();
}

void UTSCharacterAnimInstance::AnimNotify_HitReset()
{
	//UE_LOG(LogTemp, Log, TEXT("ClientHitReset"));
	OnHitReset.Broadcast();
}

void UTSCharacterAnimInstance::AnimNotify_ZoomIn()
{
	OnZoomInWork.Broadcast();
}

void UTSCharacterAnimInstance::AnimNotify_ZoomOut()
{
	//UE_LOG(LogTemp, Log, TEXT("AnimInstance ZoomOutCall"));
	OnZoomOutWork.Broadcast();
}

void UTSCharacterAnimInstance::AnimNotify_Destroy()
{
	OnDestroyWork.Broadcast();
}

void UTSCharacterAnimInstance::AnimNotify_TeleportEffect()
{
	OnTeleportEffectWork.Broadcast();
}

void UTSCharacterAnimInstance::AnimNotify_CameraShake()
{
	OnCameraShakeWork.Broadcast();
}

void UTSCharacterAnimInstance::AnimNotify_CameraDragBack()
{
	OnCameraDragBackWork.Broadcast();
}
