﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TSSpider.h"
#include "Network/Components/TSNetworkedComponent.h"
#include "TSCharacterAnimInstance.h"
#include "TSWeapon.h"
#include "Components/TSNullComponent.h"
#include "Protocol/Log.h"
#include "Components/TSSliceable.h"
#include "Protocol/Serializables.h"
#include "Animation/AnimSequence.h"
#include "Actors/TSProjectile.h"
#include "Components/WidgetComponent.h"
#include "TheShift_ClientGameModeBase.h"

ATSSpider::ATSSpider()
{
	// NetworkComp setting
	NetworkComp->SetActorType(ActorType::Spider);
	//NetworkComp->SetActorTag("Character");

	GetMesh()->SetRelativeLocationAndRotation(FVector(0.f, 0.f, -89.f), FRotator(0.f, -90.f, 0.f));

	//Default Mesh
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> skeletalMesh(TEXT("/Game/Graphic/DynamicMesh/Monster/Spider/Spider.Spider"));
	if (skeletalMesh.Succeeded())
		GetMesh()->SetSkeletalMesh(skeletalMesh.Object);

	GetMesh()->SetRelativeScale3D(FVector(2.5f, 2.5f, 2.5f));

	GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);
	static ConstructorHelpers::FClassFinder<UAnimInstance> defaultAnimInstance(TEXT("/Game/Graphic/DynamicMesh/Monster/Spider/AnimBP_Spider.AnimBP_Spider_C"));
	if (defaultAnimInstance.Succeeded())
		GetMesh()->SetAnimInstanceClass(defaultAnimInstance.Class);

	static ConstructorHelpers::FObjectFinder<UAnimSequence> attack1(TEXT("/Game/Graphic/DynamicMesh/Monster/Spider/Animation/Spider_Attack01.Spider_Attack01"));
	if (attack1.Succeeded())
		AttackAnim[0] = attack1.Object;
	static ConstructorHelpers::FObjectFinder<UAnimSequence> attack2(TEXT("/Game/Graphic/DynamicMesh/Monster/Spider/Animation/Spider_Attack02.Spider_Attack02"));
	if (attack2.Succeeded())
		AttackAnim[1] = attack2.Object;

	CurrentWeaponType = WeaponType::SWORD;

	bSimGravityDisabled = true;
	GetMesh()->SetEnableGravity(false);
	GetCapsuleComponent()->SetEnableGravity(false);
	GetCharacterMovement()->GravityScale = 0.f;
}

void ATSSpider::BeginPlay()
{
	Super::BeginPlay();
}

void ATSSpider::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	CurrentAnimInstance = Cast<UTSCharacterAnimInstance>(GetMesh()->GetAnimInstance());

	CurrentAnimInstance->OnDestroyWork.AddLambda([this]()->void {
		auto gameMode = Cast<ATheShift_ClientGameModeBase>((GetWorld()->GetAuthGameMode()));
		if (gameMode)
			gameMode->MarkActorAsGoodToBeDestroyed(this);

		});
}

void ATSSpider::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ATSSpider::Act(TheShift::ActType ActEnum)
{
}

void ATSSpider::Act(TheShift::ActType ActEnum, FVector Vector)
{
	switch (ActEnum)
	{
	case ActType::Hit_Reset:
		IsHit = false;
		BigHit = false;
		UE_LOG(LogTemp, Log, TEXT("Monster hitReset"));
		break;
	}
}

void ATSSpider::Act(TheShift::ActType ActEnum, TheShift::UID TargetActorId)
{
	if (!IsDead && !IsHit && !BigHit)
		Cast<UTSCharacterAnimInstance>(GetAnimInstance())->RestartAnimation(AttackAnim[static_cast<int>(ActEnum) - 1], FName("DefaultSlot"), 1.f);
}

void ATSSpider::OnHit(float Hp, float Damage, TheShift::ActType AttackType, TheShift::UID AttackerId, bool IsDead)
{
	if (IsDead)
	{
		CurrentAnimInstance->StopAllMontages(0.25f);
		BigHit = false;
		IsHit = false;
		this->IsDead = true;
	
		BowAimUIComponent->SetVisibility(false);
		LOG_INFO("Monster Dead!!");

		UClass* SpawnActor = StaticLoadClass(AActor::StaticClass(), NULL, TEXT("/Game/BP_SoulItem.BP_SoulItem_C"));
		if (SpawnActor)
		{
			LOG_INFO("Soul spawn");
			FActorSpawnParameters SpawnParams;
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			FRotator TempRotator = FRotator::ZeroRotator;
			auto Location = GetCapsuleComponent()->GetComponentLocation();
			auto Actor = GetWorld()->SpawnActor(SpawnActor, &Location, &TempRotator, SpawnParams);

			auto GameMode = Cast<ATheShift_ClientGameModeBase>(GetWorld()->GetAuthGameMode());

			if (auto Soul = Cast<ATSProjectile>(Actor))
				Soul->SetTargetActor(GameMode->GetTSActor(AttackerId));
			//텔레포트게이지는 바로올려야함 밑에 코드로 작성.
		}
	}
	else
	{
		UE_LOG(LogTemp, Log, TEXT("Monster Hit!!!"));
		CurrentAnimInstance->StopAllMontages(0.25f);
		if (AttackType >= ActType::Smash_1 && AttackType <= ActType::Smash_4)
		{
			if (!BigHit)
			{
				BigHit = true;
				IsHit = false;
				UE_LOG(LogTemp, Log, TEXT("Monster BigHit!!!"));
			}
		}
		else
			IsHit = true;
	}
}
