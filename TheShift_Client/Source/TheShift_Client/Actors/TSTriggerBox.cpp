﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TSTriggerBox.h"
#include "TheShift_GameInstanceBase.h"
#include "TheShift_ClientGameModeBase.h"
#include "Protocol/Serializables.h"
#include "UI/HUD/TSDirectiveContent.h"
#include "Network/Components/TSNetworkedComponent.h"
#include "UserWidget.h"

void ATSTriggerBox::NotifyServer()
{
	UID MyUID = INVALID_UID;
	if (auto GameMode = Cast<ATheShift_ClientGameModeBase>(GetWorld()->GetAuthGameMode()))
		MyUID = GameMode->MyActorId;
	else
		return;

	TheShift::Serializables::TriggerNotify Data;
	auto NetworkComp = Cast<UTSNetworkedComponent>(GetComponentByClass(UTSNetworkedComponent::StaticClass()));
	if (NetworkComp)
		Data.Tag = TCHAR_TO_UTF8(*NetworkComp->GetActorTag());
	Data.Act = static_cast<TriggerActionType>(TriggerType);
	SendPacket(Data);
}

// 현재 Trigger에 종속된 widget들을 정리한다.
void ATSTriggerBox::RemoveDerivedWidgets()
{
	for (auto& Widget : CurrentActiveWidgets) 
	{
		if(Widget)
		{
			Widget->RemoveFromParent();
		}
	}
}

void ATSTriggerBox::ScheduleParentTriggerDisable()
{
	DisableScheduled = true;
}
