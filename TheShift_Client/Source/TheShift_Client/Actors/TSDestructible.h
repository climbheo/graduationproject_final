﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Protocol/Protocol.h"
#include "Protocol/Types.h"
#include "Network/Components/TSNetworkedComponent.h"
#include "TSDestructible.generated.h"

UCLASS()
class THESHIFT_CLIENT_API ATSDestructible : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATSDestructible();

	void OnHit(float Hp, float Damage, TheShift::ActType AttackType, TheShift::UID AttackerId, bool IsDead);

	// 파괴 시 함께 파괴될 자식 actor들
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TArray<AActor*> DestructibleChildren;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	UTSNetworkedComponent* NetworkComp;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
