﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TSInteractable.h"
#include "Network/Components/TSNetworkedComponent.h"

// Sets default values
ATSInteractable::ATSInteractable()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	NetworkComp = CreateDefaultSubobject<UTSNetworkedComponent>(TEXT("Networked Component"));
}

// Called when the game starts or when spawned
void ATSInteractable::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATSInteractable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (RotateForTimeOn) 
	{
		float DurationLeft = RotationDuration - DeltaTime;
		if (DurationLeft > 0.f)
		{
			AddActorWorldRotation(AngularVelocity * DeltaTime);
			RotationDuration -= DeltaTime;
		}
		else
		{
			AddActorWorldRotation(AngularVelocity * RotationDuration);
			RotateForTimeOn = false;
		}
	}
}

void ATSInteractable::RotateForTime(FRotator& GoalRotationOffset, float Duration)
{
	RotateForTimeOn = true;
	AngularVelocity = GoalRotationOffset * (1.f / Duration);
	RotationDuration = Duration;
}

