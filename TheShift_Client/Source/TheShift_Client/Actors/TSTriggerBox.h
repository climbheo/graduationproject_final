﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "Protocol/Types.h"
#include "Misc/UActTypes.h"
#include "TSTriggerBox.generated.h"


/**
 * 
 */
UCLASS()
class THESHIFT_CLIENT_API ATSTriggerBox : public ATriggerBox
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TArray<AActor*> Targets;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TArray<class UTSDirectiveContent*> DirectiveContents;

	UPROPERTY(BlueprintReadOnly)
	bool DisableScheduled = false;

	UPROPERTY(BlueprintReadOnly)
	TArray<UUserWidget*> CurrentActiveWidgets;

	UPROPERTY(EditAnywhere)
	ETriggerActionType TriggerType;

	// UID를 찾아 하려는 act enum을 보낸다
	UFUNCTION(BlueprintCallable)
	void NotifyServer();

	UFUNCTION(BlueprintCallable)
	void RemoveDerivedWidgets();

	void ScheduleParentTriggerDisable();
};
