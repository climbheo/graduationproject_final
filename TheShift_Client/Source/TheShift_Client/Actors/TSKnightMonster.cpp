﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TSKnightMonster.h"
#include "Network/Components/TSNetworkedComponent.h"
#include "TSCharacterAnimInstance.h"
#include "TSWeapon.h"
#include "Components/TSNullComponent.h"
#include "Protocol/Log.h"
#include "Components/TSSliceable.h"
#include "Protocol/Serializables.h"
#include "Animation/AnimSequence.h"
#include "Components/WidgetComponent.h"
#include "TheShift_ClientGameModeBase.h"
#include "Actors/TSProjectile.h"
ATSKnightMonster::ATSKnightMonster()
{
	// NetworkComp setting
	NetworkComp->SetActorType(ActorType::KnightMonster);
	//NetworkComp->SetActorTag("Character");

	GetMesh()->SetRelativeLocationAndRotation(FVector(0.f, 0.f, -89.f), FRotator(0.f, -90.f, 0.f));

	//Default Mesh
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> skeletalMesh(TEXT("/Game/Graphic/DynamicMesh/Monster/KnightMonster/KnightMonster.KnightMonster"));
	if(skeletalMesh.Succeeded())
		GetMesh()->SetSkeletalMesh(skeletalMesh.Object);

	GetMesh()->SetRelativeScale3D(FVector(2.5f, 2.5f, 2.5f));

	GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);
	static ConstructorHelpers::FClassFinder<UAnimInstance> defaultAnimInstance(TEXT("/Game/Graphic/DynamicMesh/Monster/KnightMonster/AnimBP_KnightMonster.AnimBP_KnightMonster_C"));
	if(defaultAnimInstance.Succeeded())
		GetMesh()->SetAnimInstanceClass(defaultAnimInstance.Class);

	static ConstructorHelpers::FObjectFinder<UAnimSequence> attack1(TEXT("/Game/Graphic/DynamicMesh/Monster/KnightMonster/Animation/KnightMonster_Attack01.KnightMonster_Attack01"));
	if(attack1.Succeeded())
		AttackAnim[0] = attack1.Object;
	static ConstructorHelpers::FObjectFinder<UAnimSequence> attack2(TEXT("/Game/Graphic/DynamicMesh/Monster/KnightMonster/Animation/KnightMonster_Attack02.KnightMonster_Attack02"));
	if(attack2.Succeeded())
		AttackAnim[1] = attack2.Object;

	//GrapplerHit
	static ConstructorHelpers::FObjectFinder <UAnimSequence> defaultHitAnim(TEXT("/Game/Graphic/DynamicMesh/Monster/KnightMonster/Animation/KnightMonster_Hit.KnightMonster_Hit"));
	if(defaultHitAnim.Succeeded())
		DefaultHitAnim = defaultHitAnim.Object;

	// Anim Montage 로드
	//static ConstructorHelpers::FObjectFinder<UAnimMontage> DefaultNormalAttackMontage(TEXT("/Game/Graphic/MainPlayer/RetargetedAnim/TempAttackMontage.TempAttackMontage"));
	//if(DefaultNormalAttackMontage.Succeeded())
	//	NormalAttackMontage = DefaultNormalAttackMontage.Object;

	CurrentWeaponType = WeaponType::SWORD;

	//Have To Modify to Collision Profile Code.
	//SetActorEnableCollision(false);
	//GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);
	bSimGravityDisabled = true;
	GetMesh()->SetEnableGravity(false);
	GetCapsuleComponent()->SetEnableGravity(false);
	GetCharacterMovement()->GravityScale = 0.f;
}

void ATSKnightMonster::BeginPlay()
{
	Super::BeginPlay();
}

void ATSKnightMonster::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	CurrentAnimInstance = Cast<UTSCharacterAnimInstance>(GetMesh()->GetAnimInstance());

	// Hit check를 하는 타이밍에 서버에 hit check를 해달라고 보내게 함.
	CurrentAnimInstance->OnHitReset.AddLambda([this]()->void {

											  });

	CurrentAnimInstance->OnDestroyWork.AddLambda([this]()->void {
		auto gameMode = Cast<ATheShift_ClientGameModeBase>((GetWorld()->GetAuthGameMode()));
		if(gameMode)
			gameMode->MarkActorAsGoodToBeDestroyed(this);

												 });
}

void ATSKnightMonster::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	/*if(!BowAimUIComponent->bHiddenInGame)
		UE_LOG(LogTemp, Log, TEXT("%s"), *BowAimUIComponent->GetDrawSize().ToString());*/
}

void ATSKnightMonster::Act(TheShift::ActType ActEnum)
{

}

void ATSKnightMonster::Act(TheShift::ActType ActEnum, FVector Vector)
{
	switch (ActEnum)
	{
	case ActType::Hit_Reset:
		IsHit = false;
		BigHit = false;
		UE_LOG(LogTemp, Log, TEXT("Monster hitReset"));
		break;
	}

}

void ATSKnightMonster::Act(TheShift::ActType ActEnum, TheShift::UID TargetActorId)
{
	//UE_LOG(LogTemp, Log, TEXT("Monster ATTACK!!"));

	if(!IsDead && !IsHit && !BigHit)
		Cast<UTSCharacterAnimInstance>(GetAnimInstance())->RestartAnimation(AttackAnim[static_cast<int>(ActEnum)-1], FName("DefaultSlot"), 1.7f);

	/*auto TeleportEffectActor = Cast<ATSTeleportEffectActor>(GetWorld()->SpawnActor<AActor>(TeleportEffectBPClass,
		GetMesh()->GetComponentLocation(), GetMesh()->GetComponentRotation()));

	if (TeleportEffectActor)
		TeleportEffectActor->SetCharacterToCopy(this);*/
;

}

void ATSKnightMonster::OnHit(float Hp, float Damage, TheShift::ActType AttackType, TheShift::UID AttackerId, bool IsDead)
{
	/*ATSCharacter::OnHit(Hp, Damage, AttackType, AttackerId, IsDead);*/

	// 데미지 적용, 피격 애니메이션 재생
	//LOG_INFO("Actor: %d took damage of %f from Actor: %d", NetworkComp->GetUID(), Damage, AttackerId);
	//...
	if (IsDead)
	{
		// 사망 애니메이션 재생? 사망 처리
		// 다 끝나고 Actor 삭제해줘야 함.
		// ActorsToBeDestroyed 에 추가되어있는 value를 true로 변경하면 다음 프레임에 삭제됨
		CurrentAnimInstance->StopAllMontages(0.25f);
		BigHit = false;
		IsHit = false;
		this->IsDead = true;
		BowAimUIComponent->SetVisibility(false);
		LOG_INFO("Monster Dead!!");

		UClass* SpawnActor = StaticLoadClass(AActor::StaticClass(), NULL, TEXT("/Game/BP_SoulItem.BP_SoulItem_C"));
		if (SpawnActor)
		{
			LOG_INFO("Soul spawn");
			FActorSpawnParameters SpawnParams;
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			FRotator TempRotator = FRotator::ZeroRotator;
			auto Location = GetCapsuleComponent()->GetComponentLocation();
			auto Actor = GetWorld()->SpawnActor(SpawnActor, &Location, &TempRotator, SpawnParams);

			auto GameMode = Cast<ATheShift_ClientGameModeBase>(GetWorld()->GetAuthGameMode());

			if (auto Soul = Cast<ATSProjectile>(Actor))
				Soul->SetTargetActor(GameMode->GetTSActor(AttackerId));
			//텔레포트게이지는 바로올려야함 밑에 코드로 작성.
		}
	}
	else
	{
		UE_LOG(LogTemp, Log, TEXT("Monster Hit!!!"));
		CurrentAnimInstance->StopAllMontages(0.25f);
		if (AttackType >= ActType::Smash_1 && AttackType <= ActType::Smash_4)
		{
			if (!BigHit)
			{
				BigHit = true;
				IsHit = false;
				UE_LOG(LogTemp, Log, TEXT("Monster BigHit!!!"));
			}
		}
		else
		{
			Cast<UTSCharacterAnimInstance>(GetAnimInstance())->RestartAnimation(ExportAnims[0], FName("DefaultSlot"), 1.f);
			IsHit = true;
		}
		//CurrentAnimInstance->RestartAnimation(DefaultHitAnim);
	}
}
