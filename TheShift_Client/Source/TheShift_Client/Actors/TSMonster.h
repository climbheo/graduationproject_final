﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Actors/TSCharacter.h"
#include "TSMonster.generated.h"

/**
 *
 */
UCLASS()
class THESHIFT_CLIENT_API ATSMonster : public ATSCharacter
{
	GENERATED_BODY()

public:
	ATSMonster();

protected:
	//	virtual void BeginPlay() override;
	//	virtual void PostInitializeComponents() override;
	//
	//public:
	//	// Called every frame
	//	virtual void Tick(float DeltaTime) override;
	//
	//	// Called to bind functionality to input
	//	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

public:
	void SetAimUIDraw(bool IsDraw, float Distance);

	
public:
	UPROPERTY(VisibleAnywhere, Category = UI)
	class UWidgetComponent* BowAimUIComponent;

	UPROPERTY()
	TSubclassOf<class UUserWidget> AimWidgetClass;

	TheShift::ActType RecentActType = TheShift::ActType::None;
};
