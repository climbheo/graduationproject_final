// Fill out your copyright notice in the Description page of Project Settings.


#include "TSDamagable.h"

// Sets default values for this component's properties
UTSDamagable::UTSDamagable()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTSDamagable::BeginPlay()
{
	Super::BeginPlay();

	// ...

}


// Called every frame
void UTSDamagable::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

const bool UTSDamagable::IsActorDamagable() const
{
	return IsDamagable;
}

void UTSDamagable::SetDamagable(bool Input)
{
	IsDamagable = Input;
}

const bool UTSDamagable::IsActorHit() const
{
	return IsHit;
}

void UTSDamagable::SetHit(bool Input)
{
	IsHit = Input;
}

