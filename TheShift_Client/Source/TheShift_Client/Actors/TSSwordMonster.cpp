﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TSSwordMonster.h"
#include "Network/Components/TSNetworkedComponent.h"
#include "TSCharacterAnimInstance.h"
#include "TSWeapon.h"
#include "Components/TSNullComponent.h"
#include "Protocol/Log.h"
#include "Protocol/Serializables.h"
#include "Animation/AnimSequence.h"
#include "Components/WidgetComponent.h"
#include "TheShift_ClientGameModeBase.h"
#include "TSCameraShake.h"
#include "Actors/TSProjectile.h"

ATSSwordMonster::ATSSwordMonster()
{
	// NetworkComp setting
	NetworkComp->SetActorType(ActorType::SwordMonster);
	//NetworkComp->SetActorTag("Character");

	GetMesh()->SetRelativeLocationAndRotation(FVector(0.f, 0.f, -89.f), FRotator(0.f, -90.f, 0.f));

	//Default Mesh
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> skeletalMesh(TEXT("/Game/Graphic/DynamicMesh/Monster/SwordMonster/SwordMonster.SwordMonster"));
	if (skeletalMesh.Succeeded())
		GetMesh()->SetSkeletalMesh(skeletalMesh.Object);

	GetMesh()->SetRelativeScale3D(FVector(1.4f, 1.4f, 1.4f));

	GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);
	static ConstructorHelpers::FClassFinder<UAnimInstance> defaultAnimInstance(TEXT("/Game/Graphic/DynamicMesh/Monster/SwordMonster/AnimBP_SwordMonster.AnimBP_SwordMonster_C"));
	if (defaultAnimInstance.Succeeded())
		GetMesh()->SetAnimInstanceClass(defaultAnimInstance.Class);

	static ConstructorHelpers::FObjectFinder<UAnimMontage> lockOnMontage(TEXT("/Game/Graphic/DynamicMesh/Monster/SwordMonster/Animation/ThrustMontage.ThrustMontage"));
	if (lockOnMontage.Succeeded())
		LockOnMontage = lockOnMontage.Object;

	static ConstructorHelpers::FObjectFinder<UAnimSequence> combo2Montage(TEXT("/Game/Graphic/DynamicMesh/Monster/SwordMonster/Animation/2Combo.2Combo"));
	if (combo2Montage.Succeeded())
		Combo2Montage = combo2Montage.Object;

	static ConstructorHelpers::FObjectFinder<UAnimMontage> combo3Montage(TEXT("/Game/Graphic/DynamicMesh/Monster/SwordMonster/Animation/3ComboMontage.3ComboMontage"));
	if (combo3Montage.Succeeded())
		Combo3Montage = combo3Montage.Object;

	//GrapplerHit
	static ConstructorHelpers::FObjectFinder <UAnimSequence> defaultHitAnim(TEXT("/Game/Graphic/DynamicMesh/Monster/FatMonster/Animation/FatMonster_Hit.FatMonster_Hit"));
	if (defaultHitAnim.Succeeded())
		DefaultHitAnim = defaultHitAnim.Object;

	CurrentWeaponType = WeaponType::SWORD;

	bSimGravityDisabled = true;
	GetMesh()->SetEnableGravity(false);
	GetCapsuleComponent()->SetEnableGravity(false);
	GetCharacterMovement()->GravityScale = 0.f;
}

void ATSSwordMonster::BeginPlay()
{
	Super::BeginPlay();
}

void ATSSwordMonster::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	CurrentAnimInstance = Cast<UTSCharacterAnimInstance>(GetMesh()->GetAnimInstance());

	
}

void ATSSwordMonster::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ATSSwordMonster::Act(TheShift::ActType ActEnum)
{
	RecentActType = ActEnum;
	CurrentAnimInstance->StopAllMontages(0.25f);
	switch (ActEnum)
	{
	case ActType::BaseAttack_2:

		Cast<UTSCharacterAnimInstance>(GetAnimInstance())->RestartAnimation(ExportAnims[0], FName("DefaultSlot"), 1.f);
		break;
	case ActType::BaseAttack_3:

		Cast<UTSCharacterAnimInstance>(GetAnimInstance())->PlayAttackMontage(Combo3Montage);

		break;
	case ActType::Smash_1:
		Cast<UTSCharacterAnimInstance>(GetAnimInstance())->PlayAttackMontage(LockOnMontage);
	
		break;
	case ActType::Smash_2:
		Cast<UTSCharacterAnimInstance>(GetAnimInstance())->RestartAnimation(ExportAnims[3], FName("DefaultSlot"), 1.f);
		break;
	}
}

void ATSSwordMonster::Act(TheShift::ActType ActEnum, FVector Vector)
{
}

void ATSSwordMonster::Act(TheShift::ActType ActEnum, TheShift::UID TargetActorId)
{
}

void ATSSwordMonster::Act(TheShift::ActType ActEnum, int Value)
{
	switch (ActEnum)
	{
	case ActType::Dash:
		if (Value == 1)//왼쪽
		{
			CurrentAnimInstance->StopAllMontages(0.25f);
			Cast<UTSCharacterAnimInstance>(GetAnimInstance())->RestartAnimation(ExportAnims[4], FName("DefaultSlot"), AnimPlayRates[3]);
		}
		else
		{
			CurrentAnimInstance->StopAllMontages(0.25f);
			Cast<UTSCharacterAnimInstance>(GetAnimInstance())->RestartAnimation(ExportAnims[5], FName("DefaultSlot"), AnimPlayRates[4]);
		}
		break;
	case ActType::Cam_Rotation:
		if (Value == 1)//왼쪽
		{
			CurrentAnimInstance->StopAllMontages(0.25f);
			Cast<UTSCharacterAnimInstance>(GetAnimInstance())->RestartAnimation(ExportAnims[1], FName("DefaultSlot"), AnimPlayRates[1], 10);
		}
		else
		{
			CurrentAnimInstance->StopAllMontages(0.25f);
			Cast<UTSCharacterAnimInstance>(GetAnimInstance())->RestartAnimation(ExportAnims[2], FName("DefaultSlot"), AnimPlayRates[2], 10);
		}
		break;
	}
}

void ATSSwordMonster::OnHit(float Hp, float Damage, TheShift::ActType AttackType, TheShift::UID AttackerId, bool IsDead)
{
	// 데미지 적용, 피격 애니메이션 재생
	//LOG_INFO("Actor: %d took damage of %f from Actor: %d", NetworkComp->GetUID(), Damage, AttackerId);
	//...
	if (IsDead)
	{
		// 사망 애니메이션 재생? 사망 처리
		// 다 끝나고 Actor 삭제해줘야 함.
		// ActorsToBeDestroyed 에 추가되어있는 value를 true로 변경하면 다음 프레임에 삭제됨
		CurrentAnimInstance->StopAllMontages(0.25f);
		BigHit = false;
		IsHit = false;
		this->IsDead = true;
	
		BowAimUIComponent->SetVisibility(false);
		LOG_INFO("SwordMonster Dead!!");
	}
	else
	{
		UE_LOG(LogTemp, Log, TEXT("SwordMonster Hit!!!"));
		//CurrentAnimInstance->StopAllMontages(0.25f);
		//IsHit = true;
	}
}

void ATSSwordMonster::ExportAnimData(FString FileName)
{
	std::vector<AnimNotifyInfo> NotifyInfoContainer;

	//SlotAnims
	if (ExportAnims.Num() == AnimPlayRates.Num())
	{
		for (int i = 0; i < ExportAnims.Num(); ++i)
			UAnimNotifiesExporter::ExportSlotAnimNotifies(&NotifyInfoContainer, ExportAnims[i], AnimPlayRates[i]);
	}

	//SwordAttackMontage
	UAnimNotifiesExporter::ExportAnimMontageNotifies(&NotifyInfoContainer, LockOnMontage);

	//SwordAttackMontage
	UAnimNotifiesExporter::ExportAnimMontageNotifies(&NotifyInfoContainer, Combo3Montage);


	//Json에 애님Notify정보 기입
	JsonFormatter::JsonWriter JsonExporter;
	FString TargetPath = FPaths::ProjectContentDir() + TEXT("Protocol/Json/Notifies/") + FileName + TEXT(".json");
	JsonExporter.ExportAnimNotifyInfo(TCHAR_TO_UTF8(*TargetPath), NotifyInfoContainer);
	JsonExporter.Close();
}

void ATSSwordMonster::BindNotify()
{
	CurrentAnimInstance->OnHitCheck.AddLambda([this]()->void {

		//모든 카메라 쉐이킹으로바꿔야함.
		float Power = 0.2f;
		if (RecentActType == TheShift::ActType::Smash_2)
			Power = 1.f;
		else if (RecentActType == TheShift::ActType::Smash_1)
			Power = 1.f;

		auto CurrentClientController = GetWorld()->GetFirstPlayerController();
		CurrentClientController->PlayerCameraManager->PlayCameraShake(UTSCameraShake::StaticClass(), Power);
		});


	CurrentAnimInstance->OnDestroyWork.AddLambda([this]()->void {
		auto gameMode = Cast<ATheShift_ClientGameModeBase>((GetWorld()->GetAuthGameMode()));
		if (gameMode)
			gameMode->MarkActorAsGoodToBeDestroyed(this);

		});
}