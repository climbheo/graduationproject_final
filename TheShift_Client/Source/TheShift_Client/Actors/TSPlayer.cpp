﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TSPlayer.h"
#include "Components/TSNullComponent.h"
#include "Network/Components/TSNetworkedComponent.h"
#include "TSCharacterAnimInstance.h"
#include "TSWeapon.h"
#include "TheShift_ClientGameModeBase.h"
#include "TheShift_GameInstanceBase.h"
#include "Game/Actors/Actor.h"
#include "TSMonster.h"
#include "Protocol/Log.h"
#include "DrawDebugHelpers.h"
#include "Components/WidgetComponent.h"
#include "UserWidget.h"
#include "WidgetLayoutLibrary.h"
#include "TSCameraShake.h"
#include "Game/Actors/Character.h"
#include "Animation/AnimNode_StateMachine.h"
#include "Item/TSPlayerInventoryComponent.h"
#include "Item/TSItemComponent.h"
#include "Item/TSWeaponItemComponent.h"
#include "Item/TSArmorItemComponent.h"



ATSPlayer::ATSPlayer()
{
	// Component 추가
	SpringArmComp = CreateDefaultSubobject<USpringArmComponent>(TEXT("SrpingArm"));
	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	QuiverMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Quiver"));
	ArrowMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ArrowMeshComponent"));
	InventoryComp = CreateDefaultSubobject<UTSPlayerInventoryComponent>(TEXT("Inventory"));

	// NetworkComp setting
	NetworkComp->SetActorType(ActorType::Player);
	NetworkComp->SetActorTag("Character");

	//스프링 암은 캡슐에, 카메라는 스프링 암에 붙인다.
	SpringArmComp->SetupAttachment(GetCapsuleComponent());
	CameraComp->SetupAttachment(SpringArmComp);


	//메쉬 세팅 및, 스프링 암 위치, 회전, 보간속도 조정
	GetMesh()->SetRelativeLocationAndRotation(FVector(0.f, 0.f, -89.f), FRotator(0.f, -90.f, 0.f));
	SpringArmComp->TargetArmLength = 350.f;
	SpringArmComp->SetRelativeRotation(FRotator(-20.f, 0.f, 0.f));
	SpringArmComp->bEnableCameraLag = true;
	SpringArmComp->bEnableCameraRotationLag = true;
	SpringArmComp->CameraLagSpeed = 2.f;
	ArmLengthSpeed = 3.f;
	ArmLengthTo = 350.f;

	// Player Mesh
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> skeletalMesh(TEXT("/Game/Graphic/DynamicMesh/GreatSword_Player/GreatSword_Player.GreatSword_Player"));
	if (skeletalMesh.Succeeded())
		GetMesh()->SetSkeletalMesh(skeletalMesh.Object);

	GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);
	static ConstructorHelpers::FClassFinder<UAnimInstance> defaultAnimInstance(TEXT("/Game/Graphic/DynamicMesh/GreatSword_Player/AnimBP_Player.AnimBP_Player_C"));
	if (defaultAnimInstance.Succeeded())
		GetMesh()->SetAnimInstanceClass(defaultAnimInstance.Class);

	//Montage Asset Load
	LoadMontages();

	//기본 화살통 메쉬
	static ConstructorHelpers::FObjectFinder<UStaticMesh> DefaultQuiverMesh(TEXT("/Game/Graphic/Weapon/Bow/Quiver_Arrow_.Quiver_Arrow_"));
	if (DefaultQuiverMesh.Succeeded())
	{
		QuiverMesh = DefaultQuiverMesh.Object;
		QuiverMeshComp->SetStaticMesh(QuiverMesh);
	}
	//기본 화살 메쉬
	static ConstructorHelpers::FObjectFinder<UStaticMesh> DefaultArrowMesh(TEXT("/Game/Graphic/Weapon/Bow/Arrow.Arrow"));
	if (DefaultArrowMesh.Succeeded())
	{
		ArrowMesh = DefaultArrowMesh.Object;
		ArrowMeshComp->SetStaticMesh(ArrowMesh);
	}

	//먼지 파티클
	static ConstructorHelpers::FObjectFinder<UParticleSystem> Dust(TEXT("/Game/Graphic/Particles/P_Dust01.P_Dust01"));
	if (Dust.Succeeded())
	{
		DustParticle = Dust.Object;
	}
	

	//초기 웨펀 타입 설정
	CurrentWeaponType = WeaponType::HAND;

	// 최대 무기 슬롯
	MaxWeaponSlot = 4;

	//중력 받지 않게 하기 -> 서버에서 컨트롤
	bSimGravityDisabled = true;
	GetMesh()->SetEnableGravity(false);
	GetCapsuleComponent()->SetEnableGravity(false);
	GetCapsuleComponent()->SetCollisionProfileName("Player");
	GetCharacterMovement()->GravityScale = 0.f;

	ArrowMeshComp->SetVisibility(false);
	QuiverMeshComp->SetVisibility(false);
}

void ATSPlayer::BeginPlay()
{
	Super::BeginPlay();

	//컨트롤 모드 초기화.
	InitControlMode();

	SpringArmComp->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, FName("CamMoveSocket"));

	// 기본 무기 추가
	// slot에 hand추가
	WeaponsOnSlot.Add(WeaponType::HAND, nullptr);

	// Sword 추가
	ATSWeapon* Sword = GetWorld()->SpawnActor<ATSWeapon>(FVector::ZeroVector, FRotator::ZeroRotator);
	Sword->InitWeapon(FName("GreatSword"), WeaponType::SWORD);
	AddWeaponOnSlot(Sword);

	// Bow 추가
	ATSWeapon* Bow = GetWorld()->SpawnActor<ATSWeapon>(FVector::ZeroVector, FRotator::ZeroRotator);
	Bow->InitWeapon(FName("Bow"), WeaponType::BOW);
	AddWeaponOnSlot(Bow);

	// WeaponType::HAND로 시작
	CurrentWeaponType = WeaponType::HAND;
	CurrentWeapon = WeaponsOnSlot[WeaponType::HAND];

	//화살, 화살통 메쉬 소켓에 붙이기
	QuiverMeshComp->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, FName("QuiverSocket"));
	ArrowMeshComp->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, FName("ArrowSocket"));

	InventoryComp->OwningPlayer = this;
	
	LOG_INFO("Weapon init. Player Id: %d", NetworkComp->GetUID());
}

void ATSPlayer::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	CurrentAnimInstance = Cast<UTSCharacterAnimInstance>(GetMesh()->GetAnimInstance());
	BindNotify();

	
}

void ATSPlayer::BeginDestroy()
{
	Super::BeginDestroy();
	LOG_ERROR("Player destroyed!!!!!!!!!!!!!!!!");
}

void ATSPlayer::Destroyed()
{
	Super::Destroyed();
	LOG_ERROR("Player destroyed!!!!!!!!!!!!!!!!");
}

void ATSPlayer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if(IsDead)
		LockServerActorInput(true);

	auto SwordStaeMachineIndex = CurrentAnimInstance->GetStateMachineIndex(FName("SwordStateMachine"));
	if (CurrentAnimInstance->GetStateMachineInstance(SwordStaeMachineIndex)->GetCurrentStateName() == "CrouchIdle")
	{
		auto CurrentClientController = GetWorld()->GetFirstPlayerController();
		CurrentClientController->PlayerCameraManager->PlayCameraShake(UTSCameraShake::StaticClass(), 0.04f);
	}

	auto BowStaeMachineIndex = CurrentAnimInstance->GetStateMachineIndex(FName("BowStateMachine"));
	if (CurrentAnimInstance->GetStateMachineInstance(BowStaeMachineIndex)->GetCurrentStateName() == "SitAttack2")
	{
		auto CurrentClientController = GetWorld()->GetFirstPlayerController();
		CurrentClientController->PlayerCameraManager->PlayCameraShake(UTSCameraShake::StaticClass(), 0.04f);
	}
	if (GetHit())
	{
		AttackEndComboState();
		ReleasedAim = false;
		Aiming = false;
	}
	//if (Aiming)
	//	UE_LOG(LogTemp, Log, TEXT("Aiming"));

	//if (ReleasedAim)
	//	UE_LOG(LogTemp, Log, TEXT("ReleaseAiming"));

	//if (IsAttacking)
	//	UE_LOG(LogTemp, Log, TEXT("Attacking"));

	//if(Teleport)
	//	UE_LOG(LogTemp, Log, TEXT("Teleporting"));
	//if (IsHit)
	//	UE_LOG(LogTemp, Log, TEXT("ISHIT!!"));
	//if (BigHit)
	//	UE_LOG(LogTemp, Log, TEXT("BIGHITT!!"));

		//스프링 암 거리 보간
	SpringArmComp->TargetArmLength = FMath::FInterpTo(SpringArmComp->TargetArmLength, ArmLengthTo * 1.5f, DeltaTime, ArmLengthSpeed);

	if (CurrentWeaponType == WeaponType::BOW)
	{
		if (((IsAttacking && !ReleasedAim) || (Teleport && !ReleasedAim)) && CurrentCombo < MaxCombo)
		{
			ArrowMeshComp->SetVisibility(true);
			if (Teleport)
			{
				auto BowStaeMachineIndex = CurrentAnimInstance->GetStateMachineIndex(FName("BowStateMachine"));
				if (CurrentAnimInstance->GetStateMachineInstance(BowStaeMachineIndex)->GetCurrentStateName() == "TeleportAttack")
					ArrowMeshComp->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, FName("BowTeleportAttackArrowSpot"));
				else
					ArrowMeshComp->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, FName("ArrowSocket_Teleport"));
				
			}
			else
				ArrowMeshComp->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, FName("ArrowSocket"));
			//UE_LOG(LogTemp, Log, TEXT("Arrow Render O"));
		}
		else
		{
			ArrowMeshComp->SetVisibility(false);
			//UE_LOG(LogTemp, Log, TEXT("Arrow Render X"));
		}
	}

	if (IsMyPlayer())
	{
		//내 플레이어는 위쳐센스에 대한 On/Off를 검사해야함.
		SetWitcherSenseMeshVisibility();
		if ((IsAttacking | Aiming | ReleasedAim) && CurrentWeaponType == WeaponType::BOW && CurrentCombo < MaxCombo)
		{
			RenderBowTargetUI();
			//UE_LOG(LogTemp, Log, TEXT("Aim On"));
		}
		else
		{
			RenderBowTargetUI(false);
			//UE_LOG(LogTemp, Log, TEXT("Aim Off"));
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	
}

void ATSPlayer::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	//Actions
	PlayerInputComponent->BindAction(FName("Forward"), IE_Pressed, this, &ATSPlayer::OnForwardPressed);
	PlayerInputComponent->BindAction(FName("Forward"), IE_Released, this, &ATSPlayer::OnForwardReleased);
	PlayerInputComponent->BindAction(FName("Back"), IE_Pressed, this, &ATSPlayer::OnBackPressed);
	PlayerInputComponent->BindAction(FName("Back"), IE_Released, this, &ATSPlayer::OnBackReleased);
	PlayerInputComponent->BindAction(FName("Left"), IE_Pressed, this, &ATSPlayer::OnLeftPressed);
	PlayerInputComponent->BindAction(FName("Left"), IE_Released, this, &ATSPlayer::OnLeftReleased);
	PlayerInputComponent->BindAction(FName("Right"), IE_Pressed, this, &ATSPlayer::OnRightPressed);
	PlayerInputComponent->BindAction(FName("Right"), IE_Released, this, &ATSPlayer::OnRightReleased);
	PlayerInputComponent->BindAction(FName("BaseAttack"), IE_Pressed, this, &ATSPlayer::OnBaseAttackPressed);
	PlayerInputComponent->BindAction(FName("BaseAttack"), IE_Released, this, &ATSPlayer::OnBaseAttackReleased);
	PlayerInputComponent->BindAction(FName("Shift"), IE_Pressed, this, &ATSPlayer::OnShiftPressed);
	PlayerInputComponent->BindAction(FName("Shift"), IE_Released, this, &ATSPlayer::OnShiftReleased);
	PlayerInputComponent->BindAction(FName("Smash"), IE_Pressed, this, &ATSPlayer::OnSmashPressed);
	PlayerInputComponent->BindAction(FName("Teleport"), IE_Pressed, this, &ATSPlayer::OnTeleportPressed);
	PlayerInputComponent->BindAction(FName("WeaponSlotSword"), IE_Pressed, this, &ATSPlayer::OnWeaponSwapPressed<WeaponType::SWORD>);
	PlayerInputComponent->BindAction(FName("WeaponSlotDagger"), IE_Pressed, this, &ATSPlayer::OnWeaponSwapPressed<WeaponType::DAGGER>);
	PlayerInputComponent->BindAction(FName("WeaponSlotBow"), IE_Pressed, this, &ATSPlayer::OnWeaponSwapPressed<WeaponType::BOW>);
	PlayerInputComponent->BindAction(FName("WeaponSlotNone"), IE_Pressed, this, &ATSPlayer::OnWeaponSwapPressed<WeaponType::HAND>);
	PlayerInputComponent->BindAction(FName("Dash"), IE_Pressed, this, &ATSPlayer::OnDashPressed);
	PlayerInputComponent->BindAction(FName("Level01"), IE_Pressed, this, &ATSPlayer::OnLevelChange_Level01);
	PlayerInputComponent->BindAction(FName("Level02"), IE_Pressed, this, &ATSPlayer::OnLevelChange_Level02);
	PlayerInputComponent->BindAction(FName("Level03"), IE_Pressed, this, &ATSPlayer::OnLevelChange_Level03);
	PlayerInputComponent->BindAction(FName("Level04"), IE_Pressed, this, &ATSPlayer::OnLevelChange_Level04);
	PlayerInputComponent->BindAction(FName("Landscape"), IE_Pressed, this, &ATSPlayer::OnLevelChange_Landscape);
	PlayerInputComponent->BindAction(FName("MoveToStart"), IE_Pressed, this, &ATSPlayer::OnMoveToStart);
	PlayerInputComponent->BindAction(FName("MoveToMiddle"), IE_Pressed, this, &ATSPlayer::OnMoveToMiddle);
	PlayerInputComponent->BindAction(FName("MoveToBoss"), IE_Pressed, this, &ATSPlayer::OnMoveToBoss);
	PlayerInputComponent->BindAction(FName("MoveToEnd"), IE_Pressed, this, &ATSPlayer::OnMoveToEnd);
	PlayerInputComponent->BindAction(FName("GodMode"), IE_Pressed, this, &ATSPlayer::OnGodMode);
	PlayerInputComponent->BindAction(FName("ResetCharacterState"), IE_Pressed, this, &ATSPlayer::OnCheat);

	//Axis

	PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &ATSPlayer::LookUp);
	PlayerInputComponent->BindAxis(TEXT("ForwardBack"), this, &ATSPlayer::ForwardBack);
	PlayerInputComponent->BindAxis(TEXT("LeftRight"), this, &ATSPlayer::LeftRight);
	PlayerInputComponent->BindAxis(TEXT("Turn"), this, &ATSPlayer::Turn);
	//...
}

void ATSPlayer::Act(TheShift::ActType ActEnum)
{
	//UE_LOG(LogTemp, Log, TEXT("Act : %d !!!!!!!!!!!!!!!!!!!!!!!!!1"), static_cast<int>(ActEnum));
	// Base attack
	//UE_LOG(LogTemp, Log, TEXT("Player Act : %d"), static_cast<int>(ActEnum));

	//Teleport Attack
	if (ActEnum == ActType::Teleport_Casting)
	{
		if (!Teleport)
		{
			UE_LOG(LogTemp, Log, TEXT("Teleport Casting Start!!!"));
			Teleport = true;
			if(CurrentWeaponType==WeaponType::HAND)
				CurrentAnimInstance->PlayAttackMontage(HandTeleportMontage[static_cast<int>(ArmorType)]);
			else if (CurrentWeaponType == WeaponType::BOW)
			{
				
			}
			else if (CurrentWeaponType == WeaponType::SWORD)
			{

			}

		}
	}
}

void ATSPlayer::Act(TheShift::ActType ActEnum, FVector Vector)
{
	int ServerSendedCombo = static_cast<int>(ActEnum);
	//LOG_INFO("ACT!!");

	if (ActEnum >= ActType::BaseAttack_1 && ActEnum <= ActType::BaseAttack_4)
	{
		//UE_LOG(LogTemp, Log, TEXT("Base Attack : %d"), static_cast<int>(ActEnum));
		if (CurrentWeaponType != WeaponType::BOW)
		{
			SetGoalRotation(Vector.Y, Vector.Z, Vector.X);
			if (ServerSendedCombo != CurrentCombo)
			{
				switch (CurrentWeaponType)
				{
				case WeaponType::HAND:
					PlayAttackMontage(HandAttackMontage[static_cast<int>(ArmorType)], static_cast<int32>(ActEnum));
					break;
				case WeaponType::SWORD:
					PlayAttackMontage(NormalAttackMontage[static_cast<int>(ArmorType)], static_cast<int32>(ActEnum));
					break;
				}
				//각 공격 모션이 시작될때마다 누적된방향키는 다시 초기화
				InitDirection();
			}
		}
		else
		{
			if (!Teleport)
			{
				if (ActEnum == ActType::BaseAttack_1 || ActEnum == ActType::BaseAttack_4)
				{
					AttachCameraToBowCamSpot();
					if (ActEnum == ActType::BaseAttack_4)
						UE_LOG(LogTemp, Log, TEXT("Bow Combo!!"));

					if (!Teleport)
					{
						IsAttacking = true;
						Aiming = true;
					}
					ReleasedAim = false;
				}
				else
				{
					auto CurrentClientController = GetWorld()->GetFirstPlayerController();
					CurrentClientController->PlayerCameraManager->PlayCameraShake(UTSCameraShake::StaticClass(), 0.1f);
					ArmLengthTo -= 100.f;
					SpringArmComp->CameraLagSpeed = 30.f;
					if (ArmLengthTo <= 75.f)
						ArmLengthTo = 75.f;
					auto GameMode = Cast<ATheShift_ClientGameModeBase>(GetWorld()->GetAuthGameMode());
					UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), GameMode->EffectsContainer["P_EnergyBlast_Impact"], GetMesh()->GetBoneLocation(FName("Bip001-L-Hand")), FRotator::ZeroRotator, FVector(0.3f, 0.3f, 0.3f), true, EPSCPoolMethod::None);

				}
			}
			else
			{
				auto CurrentClientController = GetWorld()->GetFirstPlayerController();
				CurrentClientController->PlayerCameraManager->PlayCameraShake(UTSCameraShake::StaticClass(), 0.2f);
				auto GameMode = Cast<ATheShift_ClientGameModeBase>(GetWorld()->GetAuthGameMode());
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), GameMode->EffectsContainer["BloodParticle"], GetMesh()->GetBoneLocation(FName("Bip001-R-Hand")), FRotator::ZeroRotator, FVector(0.3f, 0.3f, 0.3f), true, EPSCPoolMethod::None);
			}

		}
	}
	//Smash
	else if (ActEnum > ActType::BaseAttack_4 && ActEnum <= ActType::Smash_4)
	{
		if (CurrentWeaponType != WeaponType::BOW)
		{
			if (ServerSendedCombo != CurrentCombo)
			{
				SetGoalRotation(Vector.Y, Vector.Z, Vector.X);
				int AnimNum = static_cast<int>(ActEnum) - static_cast<int>(ActType::Smash_1);
				switch (CurrentWeaponType)
				{
				case WeaponType::HAND:
				{
					auto& curAnim = GrapplerSmashAnims[static_cast<int>(ArmorType) * 3 + AnimNum];
					CurrentAnimInstance->RestartAnimation(curAnim, FName("PlayerHandSlot"), SmashAnimPlayRates[0][AnimNum]);
					break;
				}
				case WeaponType::SWORD:
				{
					auto& curAnim = SwordSmashAnims[static_cast<int>(ArmorType) * 4 + AnimNum];
					CurrentAnimInstance->RestartAnimation(curAnim, FName("DefaultSlot"), SmashAnimPlayRates[1][AnimNum]);
					break;
				}
				}
				//각 공격 모션이 시작될때마다 누적된방향키는 다시 초기화
				InitDirection();
			}
		}
		else
		{
			//Bow Smash 발동
			CurrentCombo = static_cast<int>(ActEnum);
			SetGoalRotation(Vector.Y, Vector.Z, Vector.X);
			InitDirection();
		}
		
	}
	else if (ActEnum == ActType::Attack_End)
	{
		SetGoalRotation(Vector.Y, Vector.Z, Vector.X);
		UE_LOG(LogTemp, Log, TEXT("ActWithVector AttackEnd"));

		ApplyNonAccumulatedInput();
	
		//활시위에서 손을 떼는순간임.
		if (CurrentWeaponType == WeaponType::BOW)
		{
			//활 스매쉬가  아니라면
			if (CurrentCombo < static_cast<int>(ActType::Smash_1))
			{
				if (!ReleasedAim)
				{
					if (!Dash)
					{
						UE_LOG(LogTemp, Log, TEXT("ReleasedAim_Bow"));
						Aiming = false;
						ReleasedAim = true;
						if (Teleport)
						{
							auto CurrentClientController = GetWorld()->GetFirstPlayerController();
							CurrentClientController->PlayerCameraManager->PlayCameraShake(UTSCameraShake::StaticClass(), 1.f);
						}
						CurrentCombo = 0;// 차지 0
						//CurrentWeapon->PlayBowAnim();

						//내 플레이어가 활시위에서 손을 뗏을때만 서버로 플레이어의 타겟을 보낸다.
						//이 메시지를 받은 서버에서는 몬스터가 화살에 피격당하는 내용의 Packet을 BroadCast한다.->클라에서는 몬스터가 맞는 모션 및 이펙트 출력(화살잔상 출력은어떻게?)
						if (IsMyPlayer() && !Teleport)
							SendBowTargets();
						else if (IsMyPlayer() && Teleport)
							FindBowTeleportTarget();
					}
					ArmLengthTo = 250.f;
					SpringArmComp->CameraLagSpeed = 12.f;
				}
				else
				{
					UE_LOG(LogTemp, Log, TEXT("AttackEnd_Bow"));
					if (Teleport && ReleasedAim)
					{
						Teleport = false;
						ReleasedAim = false;
					}
					else
					{
						IsAttacking = false;
						ReleasedAim = false;
					}
				}
			}
			//활 스매쉬를 끝내라.
			else
			{
				UE_LOG(LogTemp, Log, TEXT("SmashEnd_Bow"));
				IsAttacking = false;
				CurrentCombo = 0;
			}
		}
		else
		{
			if (CurrentWeaponType == WeaponType::SWORD && Teleport)
			{
				Teleport = false;
				SpringArmComp->CameraLagSpeed = 2.f;
				ArmLengthTo = 350.f;
				SpringArmComp->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, FName("CamMoveSocket"));
				UE_LOG(LogTemp, Log, TEXT("Sword Teleport Attack End"));
			}
			AttackEndComboState();
		}
	}
	else if (ActEnum == ActType::Dash_End)
	{
		UE_LOG(LogTemp, Log, TEXT("DASHEND"));
		Dash = false;
		DashValue = 0;
		SetGoalRotation(Vector.Y, Vector.Z, Vector.X);
		ApplyNonAccumulatedInput();
		AttackEndComboState();
	}	
	else if (ActEnum >= ActType::Teleport_Attack_1 && ActEnum <= ActType::Teleport_Attack_4)
	{
		if (ServerSendedCombo != CurrentCombo)
		{
			SetGoalRotation(Vector.Y, Vector.Z, Vector.X);
			int AnimNum = static_cast<int>(ActEnum) - static_cast<int>(ActType::Teleport_Casting);
			switch (CurrentWeaponType)
			{
			case WeaponType::HAND:
			{
				PlayAttackMontage(HandTeleportMontage[static_cast<int>(ArmorType)], AnimNum);
				break;
			}
			case WeaponType::SWORD:
			{
				//PlayAttackMontage(HandTeleportMontage, static_cast<int32>(ActEnum));
				break;
			}
			}
		}
	}
	else if (ActEnum == ActType::Teleport_End)
	{
		UE_LOG(LogTemp, Log, TEXT("Teleport_End"));
		Teleport = false;
		CurrentCombo = 0;

		CurrentAnimInstance->StopAllMontages(0.1f);
		SetGoalRotation(Vector.Y, Vector.Z, Vector.X);
	}
	else if (ActEnum == ActType::Cam_Rotation)
	{
		CurrentAnimInstance->VerticalRotation = (FMath::DegreesToRadians(acosf(GetActorRotation().Vector().CosineAngle2D(DirectionToMove))))/360.f;
		//UE_LOG(LogTemp, Log, TEXT("Camera_Rotation With Bow : %f"), CurrentAnimInstance->VerticalRotation);
		//SetGoalRotation(Vector.Y, Vector.Z, Vector.X);
	}
	else if (ActEnum == ActType::Hit_Reset)
	{
		LockServerActorInput(false);
		IsHit = false;
		if (BigHit)
			CurrentAnimInstance->OnZoomOutWork.Broadcast();
		BigHit = false;
		Dash = false;
		Aiming = false;
		ReleasedAim = false;
		AttackEndComboState();
		UE_LOG(LogTemp, Log, TEXT("Player hitReset"));
	}
}

void ATSPlayer::Act(TheShift::ActType ActEnum, TheShift::UID TargetActorId)
{

}

void ATSPlayer::Act(TheShift::ActType ActEnum, std::vector<TheShift::UID>& IdsOfTargets)
{
	////////////////////////////////////
	// Id를 이용해 actor get하는 방법 //
	////////////////////////////////////
	//
	//for(auto& Id : IdsOfTargets)
	//{
	//	// UE AActor getter
	//	Cast<ATheShift_ClientGameModeBase>(GetWorld()->GetAuthGameMode())->GetTSActor(Id);
	//
	//	// TheShift::Actor getter
	//	Cast<ATheShift_ClientGameModeBase>(GetWorld()->GetAuthGameMode())->GetCorrespondingActor(Id);
	//}
	////////////////////////////////////
}

void ATSPlayer::Act(TheShift::ActType ActEnum, int Value)
{
	switch (ActEnum)
	{
	case ActType::WeaponSwap:
	{
		WeaponType Type = static_cast<WeaponType>(Value);
		auto Element = WeaponsOnSlot.Find(Type);

		static bool DrawDebugLine = false;
		DrawDebugLine = !DrawDebugLine;
		Cast<ATheShift_ClientGameModeBase>(GetWorld()->GetAuthGameMode())->SetDrawDebug(DrawDebugLine);

		// Player doesn't have this type of weapon.
		if (Element == nullptr)
		{
			LOG_ERROR("Trying to swap weapon to WeaponType: %d. Can't find weapon.", Type);
			return;
		}
		
		// Swap weapon
		else
		{
			SwapWeapon(Type);
			if (CurrentWeaponType == WeaponType::BOW)
			{
				RotationSpeed = 0.15f;

				QuiverMeshComp->SetVisibility(true);
				
			}
			else
			{
				RotationSpeed = 0.2f;
				QuiverMeshComp->SetVisibility(false);
			}
		}
		break;
	}

	case ActType::ArmorSwap:
	{
		SwapArmor(static_cast<UArmorItemType>(Value));
		break;
	}
		
	case ActType::Dash:
	{
		CurrentAnimInstance->StopAllMontages(0.1f);
		Dash = true;
		DashValue = Value;

		IsAttacking = false;
		CurrentCombo = 0;
		Aiming = false;
		ReleasedAim = false;
		CanNextCombo = false;
		IsComboInputOn = false;

		break;
	}
	default:
		return;
	}
}

void ATSPlayer::OnHit(float Hp, float Damage, TheShift::ActType AttackType, TheShift::UID AttackerId, bool IsDead)
{
	ATSCharacter::OnHit(Hp, Damage, AttackType, AttackerId, IsDead);

	CurrentHp = Hp;
	HpChangeEvent.Broadcast();
	
	if (IsDead)
	{

	}
	else
	{
		LockServerActorInput(true);

		UE_LOG(LogTemp, Log, TEXT("Player Hit!!"));
		AttackEndComboState();

	
		CurrentAnimInstance->StopAllMontages(0.25f);
		if (AttackType >= ActType::Smash_1 && AttackType <= ActType::Smash_4)
		{
			if (!BigHit)
			{
				SpringArmComp->AttachToComponent(GetCapsuleComponent(), FAttachmentTransformRules::KeepRelativeTransform);
				BigHit = true;
				IsHit = false;
				UE_LOG(LogTemp, Log, TEXT("Player BigHit!!!"));
			}
		}
		else
		{
			int WeaponAnimNum = 0;

			switch (CurrentWeaponType)
			{
			case WeaponType::HAND:
				WeaponAnimNum = 0;
				break;
			case WeaponType::SWORD:
				WeaponAnimNum = 1;
				break;
			case WeaponType::BOW:
				WeaponAnimNum = 2;
				break;
			}
			int FinalHitAnimNum = 11 + static_cast<int>(ArmorType) * 3 + WeaponAnimNum;
			Cast<UTSCharacterAnimInstance>(GetAnimInstance())->RestartAnimation(ExportAnims[FinalHitAnimNum], FName("DefaultSlot"), AnimPlayRates[FinalHitAnimNum]);
			IsHit = true;
		}
		Aiming = false;
		ReleasedAim = false;
		Dash = false;
		DashValue = 0;
	}
}

void ATSPlayer::ForwardBack(float AxisValue)
{
	if (IsAttacking || Aiming)
	{
		AttackRotateXValue = AxisValue;
		//UE_LOG(LogTemp, Log, TEXT("AttackRotateXValue : %f"), AttackRotateXValue);
	}
	CurrentAnimInstance->GoalVerticalMove = AxisValue;
}

void ATSPlayer::LeftRight(float AxisValue)
{
	if (IsAttacking || Aiming)
	{
		AttackRotateYValue = AxisValue;
		//UE_LOG(LogTemp, Log, TEXT("AttackRotateYValue : %f"), AttackRotateYValue);
	}
	CurrentAnimInstance->GoalHorizonMove = AxisValue;
	//UE_LOG(LogTemp, Log, TEXT("Time : %f, HorizonMove : %f"), GetWorld()->GetTimeSeconds(), CurrentAnimInstance->GoalHorizonMove);

}

void ATSPlayer::Turn(float AxisValue)
{
	auto BowStateMachineIndex = CurrentAnimInstance->GetStateMachineIndex(FName("BowStateMachine"));
	auto CurrentStateName = CurrentAnimInstance->GetStateMachineInstance(BowStateMachineIndex)->GetCurrentStateName();

	if (fabs(AxisValue) > FLT_EPSILON )
	{
		//카메라를 돌렸을 때 만약 Input이 Pressed상태인게 하나라도 있다면 Packet을 보내야함!
		//공격중이면 보내지않음
		if (!(CurrentWeaponType == WeaponType::BOW && (IsAttacking || ReleasedAim)) && !Teleport)
		{
			if ((Right || Left || Back || Forward))
			{
				//캐릭터의 회전 다시 계산
				CalculateGoalRotation(IsAttacking);
				FRotator MoveRotator = DirectionToMove.GetSafeNormal().Rotation();
				Serializables::Input InputData;
				InputData.ControlRotation = TheShift::Vector3f(MoveRotator.Roll, MoveRotator.Pitch, MoveRotator.Yaw);
				FRotator CamRotation = GetControlRotation();
				InputData.CamRotation = TheShift::Vector3f(CamRotation.Roll, CamRotation.Pitch, CamRotation.Yaw);
				InputData.InputValue = TheShift::InputType::Camera_Rotation;
				PushInputQueue(InputData);

				//UE_LOG(LogTemp, Log, TEXT("Sending"));

				// GameState의 Actor도 Rotation을 갱신해준다.
				Cast<ATheShift_ClientGameModeBase>(GetWorld()->GetAuthGameMode())->GetCorrespondingActor(NetworkComp->GetUID())->SetRotationWithDegrees(InputData.ControlRotation);
			}
		}
		else if((CurrentStateName!= "SitAttack3" && CurrentStateName!="TeleportAttack"))
		{
			Serializables::Input InputData;
			auto ControlRotation = GetControlRotation();

			FRotator NewRotator(0.f, ControlRotation.Yaw, 0.f);
			DirectionToMove = NewRotator.Vector().GetSafeNormal();

			FRotator MoveRotator = DirectionToMove.GetSafeNormal().Rotation();
			InputData.ControlRotation = TheShift::Vector3f(MoveRotator.Roll, MoveRotator.Pitch, MoveRotator.Yaw);

			FRotator CamRotation = ControlRotation;
			InputData.CamRotation = TheShift::Vector3f(CamRotation.Roll, CamRotation.Pitch, CamRotation.Yaw);
			InputData.InputValue = TheShift::InputType::Camera_Rotation;

			PushInputQueue(InputData);

			//UE_LOG(LogTemp, Log, TEXT("Bow Cam Rotation Sending"));

			// GameState의 Actor도 Rotation을 갱신해준다.
			Cast<ATheShift_ClientGameModeBase>(GetWorld()->GetAuthGameMode())->GetCorrespondingActor(NetworkComp->GetUID())->SetRotationWithDegrees(InputData.ControlRotation);

		}
	}
	//만약 이동량이 없는상태(정지된 상태(활을 조준하고 있을떄))에서 카메라를 돌린다면 발을 굴리는 모션을 재생해야함.
	//그떄 실행되는 구문
	if (CurrentWeaponType == WeaponType::BOW)
	{
		//UE_LOG(LogTemp, Log, TEXT("TURNBOW"));
		/*if (Aiming | ReleasedAim | IsAttacking)
			UE_LOG(LogTemp, Log, TEXT("TURNBOW2"));*/
		//UE_LOG(LogTemp, Log, TEXT("Size : %f"), NetworkComp->GetCurrentInterpolationDirectionWithMagnitude().Size());
		if (Aiming | ReleasedAim | IsAttacking)
			CurrentAnimInstance->VerticalRotation = AxisValue / 2.f;
			//UE_LOG(LogTemp, Log, TEXT("TurnTime : %f, HorizonMove : %f"), GetWorld()->GetTimeSeconds(), CurrentAnimInstance->GoalHorizonMove);
	}

	AddControllerYawInput(AxisValue*0.5f);
}

void ATSPlayer::LookUp(float AxisValue)
{
	AddControllerPitchInput(AxisValue);
}

void ATSPlayer::OnForwardPressed()
{
	/*if (Dash)
		return;*/
	Forward = true;
	NonAccumulatedForward = true;
	UE_LOG(LogTemp, Log, TEXT("Forward"));
	
	if (!IsAttacking && !Teleport)
	{
		if (!Back || Left || Right)
			CalculateGoalRotation(IsAttacking | ReleasedAim | Aiming);

	/*	if (IsAttacking | ReleasedAim | Aiming)
		{
			UE_LOG(LogTemp, Log, TEXT("TRUEEEEEE"));
		}
		else
			UE_LOG(LogTemp, Log, TEXT("FALSE"));*/
	}
	else 
	{
		//클라에서도 공격 중 Input은 누적하는상황-> Pressed는 모두 일단 서버로 보낸다.
		if (Back)
			Back = false;
	}
	Serializables::Input InputData;
	FRotator MoveRotator = DirectionToMove.GetSafeNormal().Rotation();
	InputData.ControlRotation = TheShift::Vector3f(MoveRotator.Roll, MoveRotator.Pitch, MoveRotator.Yaw);
	FRotator CamRotation = GetControlRotation();
	InputData.CamRotation = TheShift::Vector3f(CamRotation.Roll, CamRotation.Pitch, CamRotation.Yaw);
	if (!IsAttacking && !Dash)
	{
		if (!Back)
			InputData.InputValue = TheShift::InputType::Forward_Pressed;
		else
			InputData.InputValue = TheShift::InputType::Back_Released;
	}
	else
		InputData.InputValue = TheShift::InputType::Forward_Pressed;
	PushInputQueue(InputData);
}

void ATSPlayer::OnForwardReleased()
{
	if (!IsAttacking)
	{
		Forward = false;
		UE_LOG(LogTemp, Log, TEXT("Forward Released"));
	}
	NonAccumulatedForward = false;
	if(!Teleport)
	CalculateGoalRotation(IsAttacking | ReleasedAim | Aiming);
	
	Serializables::Input InputData;
	FRotator MoveRotator = DirectionToMove.GetSafeNormal().Rotation();
	InputData.ControlRotation = TheShift::Vector3f(MoveRotator.Roll, MoveRotator.Pitch, MoveRotator.Yaw);
	FRotator CamRotation = GetControlRotation();
	InputData.CamRotation = TheShift::Vector3f(CamRotation.Roll, CamRotation.Pitch, CamRotation.Yaw);
	if (!IsAttacking && !Dash)
	{
		if (!Back)
			InputData.InputValue = TheShift::InputType::Forward_Released;
		else
			InputData.InputValue = TheShift::InputType::Back_Pressed;
	}
	else
		InputData.InputValue = TheShift::InputType::Forward_Released;
	PushInputQueue(InputData);
}

void ATSPlayer::OnBackPressed()
{
	/*if (Dash)
		return;
*/
	NonAccumulatedBack = true;

	UE_LOG(LogTemp, Log, TEXT("Back"));
	Back = true;
	if (!IsAttacking && !Teleport)
	{
		if (!Forward || Left || Right)
			CalculateGoalRotation(IsAttacking | ReleasedAim | Aiming);
	}
	else
	{
		if (Forward)
			Forward = false;
	}
	Serializables::Input InputData;
	FRotator MoveRotator = DirectionToMove.GetSafeNormal().Rotation();

	InputData.ControlRotation = TheShift::Vector3f(MoveRotator.Roll, MoveRotator.Pitch, MoveRotator.Yaw);
	FRotator CamRotation = GetControlRotation();
	InputData.CamRotation = TheShift::Vector3f(CamRotation.Roll, CamRotation.Pitch, CamRotation.Yaw);
	if (!IsAttacking && !Dash)
	{
		if (!Forward)
			InputData.InputValue = TheShift::InputType::Back_Pressed;
		else
			InputData.InputValue = TheShift::InputType::Forward_Released;
	}
	else
		InputData.InputValue = TheShift::InputType::Back_Pressed;
	PushInputQueue(InputData);
}

void ATSPlayer::OnBackReleased()
{
	if (!IsAttacking)
	{
		UE_LOG(LogTemp, Log, TEXT("Back Released"));
		Back = false;
	}

	NonAccumulatedBack = false;
	if (!Teleport)
	CalculateGoalRotation(IsAttacking | ReleasedAim | Aiming);

	Serializables::Input InputData;
	FRotator MoveRotator = DirectionToMove.GetSafeNormal().Rotation();

	InputData.ControlRotation = TheShift::Vector3f(MoveRotator.Roll, MoveRotator.Pitch, MoveRotator.Yaw);
	FRotator CamRotation = GetControlRotation();
	InputData.CamRotation = TheShift::Vector3f(CamRotation.Roll, CamRotation.Pitch, CamRotation.Yaw);
	if (!IsAttacking && !Dash)
	{
		if (!Forward)
			InputData.InputValue = TheShift::InputType::Back_Released;
		else
			InputData.InputValue = TheShift::InputType::Forward_Pressed;
	}
	else
		InputData.InputValue = TheShift::InputType::Back_Released;

	PushInputQueue(InputData);

}

void ATSPlayer::OnRightPressed()
{
	/*if (Dash)
		return;
	*/
	NonAccumulatedRight = true;

	UE_LOG(LogTemp, Log, TEXT("Right"));
	Right = true;

	if (!IsAttacking && !Teleport)
	{
		if (!Left || Forward || Back)
		{
			CalculateGoalRotation(IsAttacking | ReleasedAim | Aiming);
			UE_LOG(LogTemp, Log, TEXT("Rotate Right"));
		}
	}
	else
	{
		if (Left)
			Left = false;
	}

	Serializables::Input InputData;
	FRotator MoveRotator = DirectionToMove.GetSafeNormal().Rotation();

	InputData.ControlRotation = TheShift::Vector3f(MoveRotator.Roll, MoveRotator.Pitch, MoveRotator.Yaw);
	FRotator CamRotation = GetControlRotation();
	InputData.CamRotation = TheShift::Vector3f(CamRotation.Roll, CamRotation.Pitch, CamRotation.Yaw);
	if (!IsAttacking && !Dash)
	{
		if (!Left)
		{
			InputData.InputValue = TheShift::InputType::Right_Pressed;
			UE_LOG(LogTemp, Log, TEXT("RIGHT INPUT SEND"));
		}
		else
			InputData.InputValue = TheShift::InputType::Left_Released;
	}
	else
		InputData.InputValue = TheShift::InputType::Right_Pressed;
	PushInputQueue(InputData);

}

void ATSPlayer::OnRightReleased()
{	
	if (!IsAttacking)
	{
		UE_LOG(LogTemp, Log, TEXT("Right Released"));
		Right = false;
	}

	NonAccumulatedRight = false;
	if (!Teleport)
	CalculateGoalRotation(IsAttacking | ReleasedAim | Aiming);

	Serializables::Input InputData;
	FRotator MoveRotator = DirectionToMove.GetSafeNormal().Rotation();

	InputData.ControlRotation = TheShift::Vector3f(MoveRotator.Roll, MoveRotator.Pitch, MoveRotator.Yaw);
	FRotator CamRotation = GetControlRotation();
	InputData.CamRotation = TheShift::Vector3f(CamRotation.Roll, CamRotation.Pitch, CamRotation.Yaw);
	if (!IsAttacking && !Dash)
	{
		if (!Left)
			InputData.InputValue = TheShift::InputType::Right_Released;
		else
			InputData.InputValue = TheShift::InputType::Left_Pressed;
	}
	else
		InputData.InputValue = TheShift::InputType::Right_Released;

	PushInputQueue(InputData);
}

void ATSPlayer::OnLeftPressed()
{
	/*if (Dash)
		return;*/
	UE_LOG(LogTemp, Log, TEXT("Left"));
	Left = true;
	
	NonAccumulatedLeft = true;

	if (!IsAttacking && !Teleport)
	{
		if (!Right || Forward || Back)
		{
			CalculateGoalRotation(IsAttacking | ReleasedAim | Aiming);
			UE_LOG(LogTemp, Log, TEXT("Rotate Left"));
		}
	}
	else
	{
		if (Right)
			Right = false;
	}
	Serializables::Input InputData;
	FRotator MoveRotator = DirectionToMove.GetSafeNormal().Rotation();
	InputData.ControlRotation = TheShift::Vector3f(MoveRotator.Roll, MoveRotator.Pitch, MoveRotator.Yaw);
	FRotator CamRotation = GetControlRotation();
	InputData.CamRotation = TheShift::Vector3f(CamRotation.Roll, CamRotation.Pitch, CamRotation.Yaw);
	if (!IsAttacking && !Dash)
	{
		if (!Right)
		{
			InputData.InputValue = TheShift::InputType::Left_Pressed;
			UE_LOG(LogTemp, Log, TEXT("LEFT INPUT SEND"));
		}
		else
			InputData.InputValue = TheShift::InputType::Right_Released;
	}
	else
		InputData.InputValue = TheShift::InputType::Left_Pressed;
	PushInputQueue(InputData);
}

void ATSPlayer::OnLeftReleased()
{
	if (!IsAttacking)
	{
		UE_LOG(LogTemp, Log, TEXT("Left Released"));
		Left = false;
	}

	NonAccumulatedLeft = false;
	if (!Teleport)
		CalculateGoalRotation(IsAttacking | ReleasedAim | Aiming);
	
	Serializables::Input InputData;
	FRotator MoveRotator = DirectionToMove.GetSafeNormal().Rotation();
	InputData.ControlRotation = TheShift::Vector3f(MoveRotator.Roll, MoveRotator.Pitch, MoveRotator.Yaw);
	FRotator CamRotation = GetControlRotation();
	InputData.CamRotation = TheShift::Vector3f(CamRotation.Roll, CamRotation.Pitch, CamRotation.Yaw);
	if (!IsAttacking && !Dash)
	{
		if (!Right)
			InputData.InputValue = TheShift::InputType::Left_Released;
		else
			InputData.InputValue = TheShift::InputType::Right_Pressed;
	}
	else
		InputData.InputValue = TheShift::InputType::Left_Released;

	if (InputData.InputValue == TheShift::InputType::Left_Released)
		LOG_INFO("Left released.");
	PushInputQueue(InputData);
	
}

void ATSPlayer::OnBaseAttackPressed()
{
	if (GetHit())
		return;
	//공격중이 아니라면?
	Serializables::Input InputData;
	InputData.InputValue = TheShift::InputType::MouseLeft_Pressed;
	if (CurrentWeaponType != WeaponType::BOW)
	{
		FVector NewDirection = CalculateGoalRotation(true);
		FRotator MoveRotator = NewDirection.GetSafeNormal().Rotation();
		InputData.ControlRotation = TheShift::Vector3f(MoveRotator.Roll, MoveRotator.Pitch, MoveRotator.Yaw);
	}
	else
	{
		auto ControlRotation = GetControlRotation();
		FRotator NewRotator(0.f, ControlRotation.Yaw, 0.f);
		DirectionToMove = NewRotator.Vector().GetSafeNormal();
		FRotator MoveRotator = DirectionToMove.GetSafeNormal().Rotation();
		InputData.ControlRotation = TheShift::Vector3f(MoveRotator.Roll, MoveRotator.Pitch, MoveRotator.Yaw);
	}
	FRotator CamRotation = GetControlRotation();
	InputData.CamRotation = TheShift::Vector3f(CamRotation.Roll, CamRotation.Pitch, CamRotation.Yaw);
	PushInputQueue(InputData);

	if (Back)
		UE_LOG(LogTemp, Log, TEXT("Attack Back"));
	if (Forward)
		UE_LOG(LogTemp, Log, TEXT("Attack Forward"));
	if (Left)
		UE_LOG(LogTemp, Log, TEXT("Attack Left"));
	if (Right)
		UE_LOG(LogTemp, Log, TEXT("Attack Right"));

	if (!IsAttacking &&!Dash && !Teleport)
	{
		if (CurrentWeaponType != WeaponType::BOW)
		{
			IsAttacking = true;
			switch (CurrentWeaponType)
			{
			case WeaponType::HAND:
				PlayAttackMontage(HandAttackMontage[static_cast<int>(ArmorType)], 1);
				break;
			case WeaponType::SWORD:
				PlayAttackMontage(NormalAttackMontage[static_cast<int>(ArmorType)], 1);
				break;
			}//AttackStartComboState();
		}
		else
		{
			if (!Aiming && !ReleasedAim && CurrentCombo < MaxCombo)
			{
				Aiming = true;
				ReleasedAim = false;
			}
		}
		UE_LOG(LogTemp, Log, TEXT("First Attack Input On Client"));
	/*	UE_LOG(LogTemp, Log, TEXT("Attack Input While Attacking"));
		UE_LOG(LogTemp, Log, TEXT("RootComp Relatvie  RotationZ : %f"), RootComponent->RelativeRotation.Yaw);
		UE_LOG(LogTemp, Log, TEXT("RootComp RotationZ : %f"), RootComponent->GetComponentRotation().Yaw);
		UE_LOG(LogTemp, Log, TEXT("Actor  RotationZ : %f"), GetActorRotation().Yaw);
		UE_LOG(LogTemp, Log, TEXT("ControlRotationZ : %f"), GetControlRotation().Yaw);
		UE_LOG(LogTemp, Log, TEXT("--------------"));*/

		//UE_LOG(LogTemp, Log, TEXT("Attack Start"));
	}
	//공격중이라면
	else if(IsAttacking)
	{
		
	}

	/*}*/
}

void ATSPlayer::OnBaseAttackReleased()
{
	if (GetHit())
		return;
	if ((Aiming || IsAttacking || Teleport || ReleasedAim) && CurrentCombo < MaxCombo)
	{
		Serializables::Input InputData;
		InputData.InputValue = TheShift::InputType::MouseLeft_Released;
		FVector NewDirection = CalculateGoalRotation(true);
		FRotator MoveRotator = NewDirection.GetSafeNormal().Rotation();

		InputData.ControlRotation = TheShift::Vector3f(MoveRotator.Roll, MoveRotator.Pitch, MoveRotator.Yaw);
		FRotator CamRotation = GetControlRotation();
		InputData.CamRotation = TheShift::Vector3f(CamRotation.Roll, CamRotation.Pitch, CamRotation.Yaw);
		PushInputQueue(InputData);
	}

}

void ATSPlayer::OnSmashPressed()
{
	if (GetHit() || Teleport)
		return;
	Serializables::Input InputData;
	InputData.InputValue = TheShift::InputType::MouseRight_Pressed;
	FVector NewDirection = CalculateGoalRotation(true);
	FRotator MoveRotator = NewDirection.GetSafeNormal().Rotation();

	InputData.ControlRotation = TheShift::Vector3f(MoveRotator.Roll, MoveRotator.Pitch, MoveRotator.Yaw);
	FRotator CamRotation = GetControlRotation();
	InputData.CamRotation = TheShift::Vector3f(CamRotation.Roll, CamRotation.Pitch, CamRotation.Yaw);
	PushInputQueue(InputData);

	if ((CurrentWeaponType == WeaponType::BOW && CurrentCombo < MaxCombo) && !Aiming && !ReleasedAim && !IsAttacking)
	{
		IsAttacking = true;
		CurrentCombo = static_cast<int>(ActType::Smash_1);
	}
}

void ATSPlayer::OnTeleportPressed()
{
	if (GetHit())
		return;

	// Teleport를 할수있는 상태가 아니라면 return한다.
	if (CurrentTeleportGage < TeleportGageConsumption)
		return;
	
	Serializables::Input InputData;
	InputData.InputValue = TheShift::InputType::V_Pressed;

	auto ControlRotation = GetControlRotation();
	FRotator NewRotator(0.f, ControlRotation.Yaw, 0.f);
	DirectionToMove = NewRotator.Vector().GetSafeNormal();
	FRotator MoveRotator = DirectionToMove.GetSafeNormal().Rotation();
	InputData.ControlRotation = TheShift::Vector3f(MoveRotator.Roll, MoveRotator.Pitch, MoveRotator.Yaw);

	FRotator CamRotation = GetControlRotation();
	InputData.CamRotation = TheShift::Vector3f(CamRotation.Roll, CamRotation.Pitch, CamRotation.Yaw);
	PushInputQueue(InputData);

	//Teleport = true;

	UE_LOG(LogTemp, Log, TEXT("V_Pressed Send"));
}

void ATSPlayer::OnShiftPressed()
{
	Serializables::Input InputData;
	InputData.InputValue = TheShift::InputType::Shift_Pressed;
	FVector NewDirection = CalculateGoalRotation(true);
	FRotator MoveRotator = NewDirection.GetSafeNormal().Rotation();

	InputData.ControlRotation = TheShift::Vector3f(MoveRotator.Roll, MoveRotator.Pitch, MoveRotator.Yaw);
	FRotator CamRotation = GetControlRotation();
	InputData.CamRotation = TheShift::Vector3f(CamRotation.Roll, CamRotation.Pitch, CamRotation.Yaw);
	PushInputQueue(InputData);
}

void ATSPlayer::OnShiftReleased()
{
	Serializables::Input InputData;
	InputData.InputValue = TheShift::InputType::Shift_Released;
	FVector NewDirection = CalculateGoalRotation(true);
	FRotator MoveRotator = NewDirection.GetSafeNormal().Rotation();

	InputData.ControlRotation = TheShift::Vector3f(MoveRotator.Roll, MoveRotator.Pitch, MoveRotator.Yaw);
	FRotator CamRotation = GetControlRotation();
	InputData.CamRotation = TheShift::Vector3f(CamRotation.Roll, CamRotation.Pitch, CamRotation.Yaw);
	PushInputQueue(InputData);
}

// WeaponSwap 버튼을 눌렀을 때 불린다.
void ATSPlayer::OnWeaponSwapPressed(WeaponType TypeValue)
{
	Serializables::SwapWeapon Data;
	Data.Id = NetworkComp->GetUID();
	//TODO: 야매코드
	auto GameMode = Cast<ATheShift_ClientGameModeBase>(GetWorld()->GetAuthGameMode());
	Data.Id = GameMode->MyActorId;
	Data.SwapType = static_cast<uint8_t>(TypeValue);
	//Serializables::Input InputData;
	//InputData.InputValue = TypeValue;

	// GameMode를 통해 server로 패킷 전송하기.
	SendPacket(Data);
}

void ATSPlayer::OnCheat()
{
	AttackEndComboState();
	IsHit = false;
	BigHit = false;
	Aiming = false;
	ReleasedAim = false;
	Dash = false;
	Teleport = false;

	Forward = false;
	Back = false;
	Left = false;
	Right = false;

	NonAccumulatedForward = false;
	NonAccumulatedBack = false;
	NonAccumulatedLeft = false;
	NonAccumulatedRight = false;

	Serializables::Input InputData;
	FRotator ControlRotation;

	//현재 액터의 회전
	ControlRotation = GetActorRotation();
	InputData.ControlRotation = TheShift::Vector3f(ControlRotation.Roll, ControlRotation.Pitch, ControlRotation.Yaw);
	FRotator CamRotation = GetControlRotation();
	InputData.CamRotation = TheShift::Vector3f(CamRotation.Roll, CamRotation.Pitch, CamRotation.Yaw);
	InputData.InputValue = InputType::Cheat_Pressed;

	PushInputQueue(InputData);

}

void ATSPlayer::OnDashPressed()
{
	if (GetHit())
		return;
	if (!Dash && !Teleport)
	{
		CurrentAnimInstance->StopAllMontages(0.1f);

		AttackEndComboState();
		
		Aiming = false;
		ReleasedAim = false;

		Dash = true;
		
		Serializables::Input InputData;
		FRotator ControlRotation;

		//현재 액터의 회전
		ControlRotation = GetActorRotation();
		InputData.ControlRotation = TheShift::Vector3f(ControlRotation.Roll, ControlRotation.Pitch, ControlRotation.Yaw);
		FRotator CamRotation = GetControlRotation();
		InputData.CamRotation = TheShift::Vector3f(CamRotation.Roll, CamRotation.Pitch, CamRotation.Yaw);
		InputData.InputValue = InputType::SpaceBar_Pressed;

		auto TeleportEffectActor = Cast<ATSTeleportEffectActor>(GetWorld()->SpawnActor<AActor>(TeleportEffectBPClass,
			GetMesh()->GetComponentLocation(), GetMesh()->GetComponentRotation()));

		if(TeleportEffectActor)
			TeleportEffectActor->SetCharacterToCopy(this);

		PushInputQueue(InputData);
		UE_LOG(LogTemp, Log, TEXT("Dash"));
		//Dash구현
		//이 때 클라에서는 잔상 액터를 남기고 현재 액터의 렌더링을 꺼야함.(애님은 재생됨.)-> 서버에서 Input기반으로 순간이동시킨다.

		//움직임 방향벡터에 기반해 플레이어가 움직이지 "못하게" 한다.
		//대쉬중에는 회전 잠구기
		
	}
}

// ESC를 눌렀을때 Menu를 연다.
void ATSPlayer::OnEscape()
{

}

// Stage01로 이동한다.
void ATSPlayer::OnLevelChange_Level01()
{
	Serializables::LevelChangeReqDebug Data;
	Data.Type = MapType::Stage01;
	SendPacket(Data);
}

// Stage02로 이동한다.
void ATSPlayer::OnLevelChange_Level02()
{
	Serializables::LevelChangeReqDebug Data;
	Data.Type = MapType::Stage02;
	SendPacket(Data);
}

// Stage03로 이동한다.
void ATSPlayer::OnLevelChange_Level03()
{
	Serializables::LevelChangeReqDebug Data;
	Data.Type = MapType::Stage03;
	SendPacket(Data);
}

// Stage04로 이동한다.
void ATSPlayer::OnLevelChange_Level04()
{
	Serializables::LevelChangeReqDebug Data;
	Data.Type = MapType::Stage04;
	SendPacket(Data);
}

// Landscape로 이동한다.(Debug용)
void ATSPlayer::OnLevelChange_Landscape()
{
	Serializables::LevelChangeReqDebug Data;
	Data.Type = MapType::Landscape;
	SendPacket(Data);
}

void ATSPlayer::OnMoveToStart()
{
	Serializables::Cheat Data;
	Data.Id = NetworkComp->GetUID();
	Data.Type = CheatType::MoveToStart;
	SendPacket(Data);
}

void ATSPlayer::OnMoveToMiddle()
{
	Serializables::Cheat Data;
	Data.Id = NetworkComp->GetUID();
	Data.Type = CheatType::MoveToMiddle;
	SendPacket(Data);
}

void ATSPlayer::OnMoveToBoss()
{
	Serializables::Cheat Data;
	Data.Id = NetworkComp->GetUID();
	Data.Type = CheatType::MoveToBoss;
	SendPacket(Data);
}

void ATSPlayer::OnMoveToEnd()
{
	Serializables::Cheat Data;
	Data.Id = NetworkComp->GetUID();
	Data.Type = CheatType::MoveToEnd;
	SendPacket(Data);
}

void ATSPlayer::OnGodMode()
{
	Serializables::Cheat Data;
	Data.Id = NetworkComp->GetUID();
	Data.Type = CheatType::GodMode;
	SendPacket(Data);
}

void ATSPlayer::SendBowTargets()
{
	std::vector<UID> Targets;
	//손을 떼는순간 플레이어와 적의 사이에 장애물이 없고 보이는 사정거리 안의 적들의 ID를 전송한다.
	Serializables::ActWithTargets Data;
	Data.Self = NetworkComp->GetUID();

	//이 밑줄코드는 나중에 ActorsByTypes로 바껴야함(모든 감지가능한 몬스터들)
	auto GameMode = Cast<ATheShift_ClientGameModeBase>(GetWorld()->GetAuthGameMode());
	auto CapsuleLocation = GetCapsuleComponent()->GetComponentLocation();

	FCollisionQueryParams Parameter;
	TArray<AActor*> MonsterArray;

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATSMonster::StaticClass(), MonsterArray);
	Parameter.AddIgnoredActors(MonsterArray);
	Parameter.AddIgnoredActor(this);

	auto PlayerLookVector = GetActorForwardVector();

	for (auto& Monster : MonsterArray)
	{
		auto MonsterLocation = Cast<ACharacter>(Monster)->GetCapsuleComponent()->GetComponentLocation();
		//사정거리(1000->임시, 나중에 조정할 값) 
		auto DistanceVector = MonsterLocation - CapsuleLocation;

		float degree = FMath::RadiansToDegrees(acosf(DistanceVector.CosineAngle2D(PlayerLookVector)));
	
		if (DistanceVector.Size() <= 1200.f && DistanceVector.Size() >= 100.f && degree <= 20.f)
		{
			FHitResult OutHit;

			DrawDebugLine(GetWorld(), CapsuleLocation, MonsterLocation, FColor::Green, false, 0.1f);
			//만약 레이로 쐇을때 사정거리 안에 있고 플레이어와 몬스터 사이에 아무것도 없다면 이 녀석은 조준당해야함.
			if (!GetWorld()->LineTraceSingleByChannel(OutHit, CapsuleLocation, MonsterLocation, ECC_Visibility, Parameter))
			{
				Targets.emplace_back(Cast<UTSNetworkedComponent>(Monster->GetComponentByClass(UTSNetworkedComponent::StaticClass()))->GetUID());
				UE_LOG(LogTemp, Log, TEXT("Detected Enemy: %s"), *Monster->GetName());
			}
			else
			{
				UE_LOG(LogTemp, Log, TEXT("Enemy %s Was Blocked Blocked By %s"), *Monster->GetName(),*OutHit.Actor->GetName());
			}
		}
	}

	Data.IdsOfTargets = Targets;

	// GameMode를 통해 server로 패킷 전송하기.
	SendPacket(Data);
}

void ATSPlayer::LockServerActorInput(bool IsLock)
{
	auto GameMode = Cast<ATheShift_ClientGameModeBase>(GetWorld()->GetAuthGameMode());
	auto ServerPlayer = std::reinterpret_pointer_cast<Character>(GameMode->GetGameState()->GetActor(NetworkComp->GetUID()));
	//클라에서 작동하는 이동로직도 잠군다.
	if (ServerPlayer)
	{
		if (!IsLock)
			ServerPlayer->UnLockInput();
		else
			ServerPlayer->LockInput();
	}
}

bool ATSPlayer::IsMyPlayer()
{
	auto MyPlayer = Cast<ATSPlayer>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetPawn());

	auto Player = Cast<ATSPlayer>(this);

	return Player == MyPlayer;
}

void ATSPlayer::SetWitcherSenseMeshVisibility()
{
	//Witcher Sense On/Off Function////////////////////
	auto GameMode = Cast<ATheShift_ClientGameModeBase>(GetWorld()->GetAuthGameMode());

	if (GameMode->IsWitcherSenseVisible)
	{
		//이 밑줄코드는 나중에 ActorsByTypes로 바껴야함(모든 감지가능한 오브젝트들)
		auto CapsuleLocation = GetCapsuleComponent()->GetComponentLocation();
		
		FCollisionQueryParams Parameter;
		TArray<AActor*> MonsterArray;

	/*	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATSMonster::StaticClass(), MonsterArray);*/
		UGameplayStatics::GetAllActorsWithTag(GetWorld(), "Sensable", MonsterArray);
		Parameter.AddIgnoredActors(MonsterArray);
		Parameter.AddIgnoredActor(this);

		UE_LOG(LogTemp, Log, TEXT("MonsterAndPlayerNum : %d"), MonsterArray.Num() + 1);

		for (auto& Monster : MonsterArray)
		{
			auto MonsterLocation = Monster->GetActorLocation();
			if (auto Character = Cast<ACharacter>(Monster))
				MonsterLocation = Character->GetCapsuleComponent()->GetComponentLocation();
			else
			{
				if(auto ChildActorComponent = Monster->GetComponentByClass(UChildActorComponent::StaticClass()))
					MonsterLocation = Cast<UChildActorComponent>(ChildActorComponent)->GetComponentLocation();
				else
					UE_LOG(LogTemp, Log, TEXT("%s Has No ChildActor"), *Monster->GetName());
			}
			FHitResult OutHit;
			DrawDebugLine(GetWorld(), CapsuleLocation, MonsterLocation, FColor::Green, false, 0.1f);
			//3000보다 가까우면 일단 WitcherSense를 킬지말지 검사해야함.
			if ((CapsuleLocation - MonsterLocation).Size() <= 3000.f)
			{
				////만약 플레이어와 몬스터 사이에 걸리는 물체가 없다면
				if (!GetWorld()->LineTraceSingleByChannel(OutHit, CapsuleLocation, MonsterLocation, ECC_Visibility, Parameter))
				{
					//if (OutHit.Actor != Monster && !Cast<ACharacter>(OutHit.Actor))
					//{
					//	//UE_LOG(LogTemp, Log, TEXT("Monster Blocked By %s"), *OutHit.Actor->GetName());
					//	//몬스터의 컴포넌트 중 Sensable태그를 가진 컴포넌트를 가져온다.
					auto Components = Monster->GetComponentsByTag(UActorComponent::StaticClass(), "Sensable");
					UE_LOG(LogTemp, Log, TEXT("Monster Name : %s"), *Monster->GetName());
					for (auto Component : Components)
					{
						if (Component->GetName() == "WSMesh_NoZTest")
						{
							Cast<UPrimitiveComponent>(Component)->SetVisibility(false);
							UE_LOG(LogTemp, Log, TEXT("Off"));
							break;
						}
					}

				}
				else
				{
					auto Components = Monster->GetComponentsByTag(UActorComponent::StaticClass(), "Sensable");
					for (auto Component : Components)
					{
						if (Component->GetName() == "WSMesh_NoZTest")
						{
							UE_LOG(LogTemp, Log, TEXT("%s Was Blocked by %s"), *Monster->GetName(), *OutHit.Actor->GetName());
							Cast<UPrimitiveComponent>(Component)->SetVisibility(true);
							UE_LOG(LogTemp, Log, TEXT("On"));
							break;
						}
					}
				}
			}
			//3000보다멀면 ZTest를 하지않는 메쉬 렌더링을 끈다.
			else
			{
				auto Components = Monster->GetComponentsByTag(UActorComponent::StaticClass(), "Sensable");
				for (auto Component : Components)
				{
					if (Component->GetName() == "WSMesh_NoZTest")
					{
						Cast<UPrimitiveComponent>(Component)->SetVisibility(false);
						//UE_LOG(LogTemp, Log, TEXT("Off"));
						break;
					}
				}
			}

		}
	}
}

void ATSPlayer::RenderBowTargetUI(bool IsRender)
{
	auto GameMode = Cast<ATheShift_ClientGameModeBase>(GetWorld()->GetAuthGameMode());
	//이 밑줄코드는 나중에 ActorsByTypes로 바껴야함(모든 감지가능한 "몬스터"들)
	auto CapsuleLocation = GetCapsuleComponent()->GetComponentLocation();

	FCollisionQueryParams Parameter;
	TArray<AActor*> MonsterArray;

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATSMonster::StaticClass(), MonsterArray);
	Parameter.AddIgnoredActors(MonsterArray);
	Parameter.AddIgnoredActor(this);

	auto PlayerLookVector = GetActorForwardVector();

	if (IsRender)
	{
		for (auto& Monster : MonsterArray)
		{
			auto CurMonster = Cast<ATSMonster>(Monster);
			auto MonsterLocation = Cast<ACharacter>(Monster)->GetCapsuleComponent()->GetComponentLocation();

			auto DistanceVector = MonsterLocation - CapsuleLocation;
			float Distance = DistanceVector.Size();
			float degree = FMath::RadiansToDegrees(acosf(DistanceVector.CosineAngle2D(PlayerLookVector)));
		
			FHitResult OutHit;
			//1000.f 보다 가까우면 조준대상인지 검사하자.
		
			if (Distance <= 1200.f && Distance >= 100.f && degree <= 20.f)
			{
				//사이에 가리는 오브젝트가없으면 UI 키기
				if (!GetWorld()->LineTraceSingleByChannel(OutHit, CapsuleLocation, MonsterLocation, ECC_Visibility, Parameter))
				{
					//UE_LOG(LogTemp, Log, TEXT("AimMonster Name : %s"), *Monster->GetName());
					CurMonster->SetAimUIDraw(true, Distance);
				}
				else
					CurMonster->SetAimUIDraw(false, 0.f);
			}
			else
				CurMonster->SetAimUIDraw(false,0.f);
		/*	if (CurMonster->GetDead())
				UE_LOG(LogTemp, Log, TEXT("Monster Is Dead"));*/
		}
	}
	else
	{
		for (auto& Monster : MonsterArray)
			Cast<ATSMonster>(Monster)->SetAimUIDraw(false, 0.f);
	}


}

void ATSPlayer::InitControlMode(bool bRotateInit)
{
	//SpringArm의 Default 거리	
	//ArmLengthTo = 300.f;
	if (bRotateInit)
		SpringArmComp->SetRelativeRotation(FRotator::ZeroRotator);

	//SpringArm과 Camera는 Pawn의 회전컨트롤에 영향을 받으며 다른 액터들과 충돌한다.
	SpringArmComp->bUsePawnControlRotation = true;
	SpringArmComp->bInheritPitch = true;
	SpringArmComp->bInheritYaw = true;
	SpringArmComp->bInheritRoll = true;
	SpringArmComp->bDoCollisionTest = true;

	//컨트롤러의 회전에 따라 캐릭터가 회전하게 하지 않는다.(이걸 true로 하면 카메라 회전시, 캐릭터도 동일한 회전값을 가져버림)
	bUseControllerRotationYaw = false;

	//움직임 방향벡터에 기반해 플레이어가 회전하게 한다.
	GetCharacterMovement()->bOrientRotationToMovement = false;
	//플레이어의 회전 속도
	GetCharacterMovement()->RotationRate = FRotator(0.f, 720.f, 0.f);
}

void ATSPlayer::OnAttackMontageEnded(UAnimMontage* Montage, bool bInterrupted)
{
	//if (!bInterrupted)
	//{
	//	IsAttacking = false;
	//	AttackEndComboState();

	//	UE_LOG(LogTemp, Log, TEXT("ComboEnded"));

	//	Serializables::Input InputData;
	//	InputData.InputValue = TheShift::InputType::Combo_End;
	//	FRotator ControlRotation = RootComponent->GetComponentRotation();
	//	InputData.ControlRotation = TheShift::Vector3f(ControlRotation.Roll, ControlRotation.Pitch, ControlRotation.Yaw);
	//	FRotator CamRotation = GetControlRotation();
	//	InputData.CamRotation = TheShift::Vector3f(CamRotation.Roll, CamRotation.Pitch, CamRotation.Yaw);
	//	PushInputQueue(InputData);

	//	//방향 input Init
	//	InitDirection();
	//}

	//OnAttackEnd.Broadcast();
}

void ATSPlayer::ExportAnimData(FString FileName)
{
	// slot에 hand추가
	std::vector<AnimNotifyInfo> PlayerNotifies;

	//Export전에 PlayRate 기입.
	//Grappler Smash PlayRate
	SmashAnimPlayRates[0][0] = 2.5f;
	SmashAnimPlayRates[0][1] = 1.7f;
	SmashAnimPlayRates[0][2] = 1.7f;
	//Sword Smash PlayRate
	SmashAnimPlayRates[1][0] = 1.7f;
	SmashAnimPlayRates[1][1] = 1.7f;
	SmashAnimPlayRates[1][2] = 1.7f;
	SmashAnimPlayRates[1][3] = 2.5f;

	//HandAttackMontage
	UAnimNotifiesExporter::ExportAnimMontageNotifies(&PlayerNotifies, HandAttackMontage[static_cast<int>(ArmorType)], L"Grappler_");

	UE_LOG(LogTemp, Log, TEXT("SmashNums :  %d"), GrapplerSmashAnims.Num());
	//HandSmashSlotAnims
	for (int i = 0; i < 3; ++i)
		UAnimNotifiesExporter::ExportSlotAnimNotifies(&PlayerNotifies, GrapplerSmashAnims[i], SmashAnimPlayRates[0][i]);
	
	//HandTeleportAnims
	UAnimNotifiesExporter::ExportAnimMontageNotifies(&PlayerNotifies, HandTeleportMontage[0], L"Grappler_T_");

	//SwordAttackMontage
	UAnimNotifiesExporter::ExportAnimMontageNotifies(&PlayerNotifies, NormalAttackMontage[0], L"GreatSword_");
	//SwordSmashSlotAnims
	for (int i = 0; i < 4; ++i)
		UAnimNotifiesExporter::ExportSlotAnimNotifies(&PlayerNotifies, SwordSmashAnims[i], SmashAnimPlayRates[1][i]);

	//SlotAnims
	if (ExportAnims.Num() == AnimPlayRates.Num())
	{
		for (int i = 0; i < ExportAnims.Num(); ++i)
			UAnimNotifiesExporter::ExportSlotAnimNotifies(&PlayerNotifies, ExportAnims[i], AnimPlayRates[i]);
	}
	//Json에 애님Notify정보 기입
	JsonFormatter::JsonWriter JsonExporter;
	FString TargetPath = FPaths::ProjectContentDir() + TEXT("Protocol/Json/Notifies/") + "PlayerNotifies" + TEXT(".json");
	JsonExporter.ExportAnimNotifyInfo(TCHAR_TO_UTF8(*TargetPath), PlayerNotifies);
	JsonExporter.Close();
}

FVector ATSPlayer::CalculateGoalRotation(bool bAttacking)
{
	if (GetHit() || IsDead)
		return DirectionToMove;
	UE_LOG(LogTemp, Log, TEXT("Rotation"));
	FRotator ControlRotation;
	FVector NewDirectionVector = DirectionToMove;

	ControlRotation = GetControlRotation();

	if (Forward)
	{
		if (Right)
			NewDirectionVector = FVector::ForwardVector.RotateAngleAxis(ControlRotation.Yaw + 45.f, FVector::UpVector);
		else if (Left)
			NewDirectionVector = FVector::ForwardVector.RotateAngleAxis(ControlRotation.Yaw - 45.f, FVector::UpVector);
		else
			NewDirectionVector = FVector::ForwardVector.RotateAngleAxis(ControlRotation.Yaw, FVector::UpVector);
	}
	else if (Back)
	{
		if (Right)
			NewDirectionVector = FVector::ForwardVector.RotateAngleAxis(ControlRotation.Yaw + 135.f, FVector::UpVector);
		else if (Left)
			NewDirectionVector = FVector::ForwardVector.RotateAngleAxis(ControlRotation.Yaw - 135.f, FVector::UpVector);
		else
			NewDirectionVector = FVector::ForwardVector.RotateAngleAxis(ControlRotation.Yaw - 180.f, FVector::UpVector);
	}
	else
	{
		if (Right)
			NewDirectionVector = FVector::ForwardVector.RotateAngleAxis(ControlRotation.Yaw + 90.f, FVector::UpVector);
		else if (Left)
			NewDirectionVector = FVector::ForwardVector.RotateAngleAxis(ControlRotation.Yaw - 90.f, FVector::UpVector);
	}
	if (!bAttacking && !Dash)
		DirectionToMove = NewDirectionVector;



	return NewDirectionVector;
}

void ATSPlayer::CalculateRoationAfterAttack()
{
	//공격의 다음 스텝은 카메라와 키입력, 캐릭터를 기준으로함.
	//Step 1-> 공격 중 키입력을 체크한다.
	//Step 2-> 다음 공격 시 전 공격중 들어온 키입력과 카메라의 시선을 기준으로 회전해야 할 방향을 정한다.
	//Step 3-> 현재 방향과 회전해야 할 방향의 차이값만큼 목표회전값을 정한다.
	//키입력이 없을땐 더하지 안흔ㄴ다.

	FVector GoalVector = FVector::ForwardVector.RotateAngleAxis(GetControlRotation().Yaw, FVector::UpVector);
	GoalVector = GoalVector.GetSafeNormal();

	FVector ActorForwardVector = GetCapsuleComponent()->GetForwardVector();
	ActorForwardVector.Z = 0.f;
	ActorForwardVector = ActorForwardVector.GetSafeNormal();

	FVector ActorRightVector = GetCapsuleComponent()->GetRightVector();
	ActorRightVector.Z = 0.f;
	ActorRightVector = ActorRightVector.GetSafeNormal();

	//UE_LOG(LogTemp, Log, TEXT("CameraVector : %s"), *GoalVector.ToString());
	//UE_LOG(LogTemp, Log, TEXT("ActorVector : %s"), *ActorForwardVector.ToString());

	if (AttackRotateXValue > 0.f)
	{
		if (AttackRotateYValue > 0.f)
			GoalVector = GoalVector.RotateAngleAxis(45.f, FVector::UpVector);
		else if (AttackRotateYValue < 0.f)
			GoalVector = GoalVector.RotateAngleAxis(-45.f, FVector::UpVector);
		else
			GoalVector = GoalVector;
	}
	else if (AttackRotateXValue < 0.f)
	{
		if (AttackRotateYValue > 0.f)
			GoalVector = GoalVector.RotateAngleAxis(135.f, FVector::UpVector);
		else if (AttackRotateYValue < 0.f)
			GoalVector = GoalVector.RotateAngleAxis(-135.f, FVector::UpVector);
		else
			GoalVector = -GoalVector;
	}
	else
	{
		if (AttackRotateYValue > 0.f)
			GoalVector = GoalVector.RotateAngleAxis(90.f, FVector::UpVector);
		else if (AttackRotateYValue < 0.f)
			GoalVector = GoalVector.RotateAngleAxis(-90.f, FVector::UpVector);
		else
			GoalVector = ActorForwardVector;
	}

	UE_LOG(LogTemp, Log, TEXT("RightVector : %s"), *ActorRightVector.ToString());

	float DegreeDiff = FMath::RadiansToDegrees(FMath::Acos(FVector::DotProduct(GoalVector, ActorForwardVector)));

	if (FVector::DotProduct(GoalVector, ActorRightVector) > 0.f)
		DegreeDiff *= -1.f;

	UE_LOG(LogTemp, Log, TEXT("Degree : %f"), DegreeDiff);

	float GoalRotationYaw = DegreeDiff;

	AddActorLocalRotation(FRotator(0.f, -GoalRotationYaw, 0.f));

	AttackRotateYValue = 0.f;
	AttackRotateXValue = 0.f;
}

void ATSPlayer::ApplyNonAccumulatedInput()
{
	if (NonAccumulatedForward)
		Forward = true;
	else
		Forward = false;

	if (NonAccumulatedBack)
		Back = true;
	else
		Back = false;
	if (NonAccumulatedRight)
		Right = true;
	else
		Right = false;

	if (NonAccumulatedLeft)
		Left = true;
	else
		Left = false;
}

void ATSPlayer::AttachCameraToBowCamSpot()
{
	if (!Dash)
	{
		ArmLengthTo = 220.f;
		SpringArmComp->CameraLagSpeed = 12.f;

		auto CamSpots = GetComponentsByTag(UChildActorComponent::StaticClass(), FName("BowCamera"));

		if (CamSpots.Num() != 0)
			SpringArmComp->AttachToComponent(Cast<UChildActorComponent>(CamSpots[0]), FAttachmentTransformRules::KeepRelativeTransform);
	}
}


void ATSPlayer::FindBowTeleportTarget()
{
	//Ray를 발사하는 Screen좌표.
	FVector2D ScreenPos = UWidgetLayoutLibrary::GetViewportSize(GetWorld())/2.f;
	FVector RayPosition;
	FVector RayDirection;
	UGameplayStatics::DeprojectScreenToWorld(Cast<APlayerController>(GetController()), ScreenPos, RayPosition, RayDirection);
	FHitResult HitInfo;
	std::vector<UID> Targets;
	Serializables::ActWithTargets Data;
	//손을 떼는순간 플레이어와 적의 사이에 장애물이 없고 보이는 사정거리 안의 적들의 ID를 전송한다.
	Data.Self = NetworkComp->GetUID();
	if (GetWorld()->LineTraceSingleByChannel(HitInfo, RayPosition, RayPosition + RayDirection * 10000.f, ECollisionChannel::ECC_Visibility))
	{
		//만약 레이에 맞은 가장가까운Actor가 Monster라면
		if (auto Monster = Cast<ATSMonster>(HitInfo.Actor))
		{
			Targets.emplace_back(Cast<UTSNetworkedComponent>(Monster->GetComponentByClass(UTSNetworkedComponent::StaticClass()))->GetUID());
			Data.IdsOfTargets = Targets;
			// GameMode를 통해 server로 패킷 전송하기.
		
		}
	}
	SendPacket(Data);

	UE_LOG(LogTemp, Log, TEXT("Detected Bow Teleport Attack Target!"));
	UE_LOG(LogTemp, Log, TEXT("Target Num : %d"), Data.IdsOfTargets.size());

	if (Data.IdsOfTargets.empty())
		Teleport = false;
}

void ATSPlayer::BindNotify()
{
	if (CurrentAnimInstance)
	{
		// Attack montage가 끝날 때 Combo check을 보내게 함.
		CurrentAnimInstance->OnMontageEnded.AddDynamic(this, &ATSPlayer::OnAttackMontageEnded);
		CurrentAnimInstance->OnNextAttackCheck.AddLambda([this]()->void {
			if (!ReleasedAim)
				Aiming = false;
			});
		CurrentAnimInstance->OnTeleportEffectWork.AddLambda([this]()->void {

			auto TeleportEffectActor = Cast<ATSTeleportEffectActor>(GetWorld()->SpawnActor<AActor>(TeleportEffectBPClass,
				GetMesh()->GetComponentLocation(), GetMesh()->GetComponentRotation()));

			if (TeleportEffectActor)
				TeleportEffectActor->SetCharacterToCopy(this);

			if (CurrentWeaponType == WeaponType::SWORD && Teleport)
			{
				TeleportEffectActor->SetDirection(-GetActorForwardVector());
			}
			});
		CurrentAnimInstance->OnCameraDragBackWork.AddLambda([this]()->void
		{
			ArmLengthTo = 450.f;
		}
		);
		CurrentAnimInstance->OnCameraShakeWork.AddLambda([this]()->void
			{
				auto CurrentClientController = GetWorld()->GetFirstPlayerController();
				CurrentClientController->PlayerCameraManager->PlayCameraShake(UTSCameraShake::StaticClass(), 1.5f);
			}
		);

		CurrentAnimInstance->OnZoomInWork.AddLambda([this]() {

			if (CurrentWeaponType != WeaponType::BOW || Dash)
			{
				if (CurrentWeaponType == WeaponType::SWORD && Teleport)
				{
					GetMesh()->SetVisibility(true);
					CurrentWeapon->SetActorHiddenInGame(false);
					SpringArmComp->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, FName("CamMoveSocket"));
				}
				if (CurrentWeaponType == WeaponType::BOW)
				{
					SpringArmComp->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, FName("CamMoveSocket"));
					ArmLengthTo = 350.f;
				}

				SpringArmComp->CameraLagSpeed = 12.f;
				ArmLengthTo -= 70.f;

				if (ArmLengthTo <= 250.f)
					ArmLengthTo = 200.f;

			}
			else
			{
				if (!Teleport)
				{
					if (CurrentCombo < MaxCombo)
						AttachCameraToBowCamSpot();
					else
						ArmLengthTo -= 150.f;
				}
				else
				{
					auto BowStaeMachineIndex = CurrentAnimInstance->GetStateMachineIndex(FName("BowStateMachine"));
					if (CurrentAnimInstance->GetStateMachineInstance(BowStaeMachineIndex)->GetCurrentStateName() == "TeleportAttack")
					{
						ArmLengthTo = 225.f;
						SpringArmComp->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, FName("BowTeleportAttackSpot"));
						SpringArmComp->CameraLagSpeed = 4.f;

						AddControllerPitchInput(5.f);
					}
					else
					{
						ArmLengthTo = 150.f;
						auto CamSpots = GetComponentsByTag(UChildActorComponent::StaticClass(), FName("BowTeleportSpot"));

						if (CamSpots.Num() != 0)
							SpringArmComp->AttachToComponent(Cast<UChildActorComponent>(CamSpots[0]), FAttachmentTransformRules::KeepRelativeTransform);
					}
				}
			}});

		CurrentAnimInstance->OnZoomOutWork.AddLambda([this]() {
			auto GameMode = Cast<ATheShift_ClientGameModeBase>(GetWorld()->GetAuthGameMode());

			if (CurrentWeaponType != WeaponType::BOW)
			{
				UE_LOG(LogTemp, Log, TEXT("ZOOMOUT!"));
				SpringArmComp->CameraLagSpeed = 2.f;
				ArmLengthTo = 350.f;
				if (CurrentWeaponType == WeaponType::SWORD && Teleport)
				{
					GetMesh()->SetVisibility(false);
					CurrentWeapon->SetActorHiddenInGame(true);
					SpringArmComp->AttachToComponent(GetCapsuleComponent(), FAttachmentTransformRules::KeepRelativeTransform);
				}
			}
			else
			{
				auto BowStateMachineIndex = CurrentAnimInstance->GetStateMachineIndex(FName("BowStateMachine"));
				auto CurrentStateName = CurrentAnimInstance->GetStateMachineInstance(BowStateMachineIndex)->GetCurrentStateName();

				UE_LOG(LogTemp, Log, TEXT("ZoomOut of Bow"));


				if ((!Aiming && !IsAttacking && !Teleport) ||
					(CurrentWeaponType == WeaponType::BOW && IsAttacking && CurrentCombo == static_cast<int>(ActType::Smash_1)))
				{
					if (GameMode->IsWitcherSenseVisible && CurrentStateName == "Ground" && Teleport)
						return;

					SpringArmComp->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, FName("CamMoveSocket"));
					ArmLengthTo = 350.f;
					SpringArmComp->CameraLagSpeed = 2.f;
				}
				if (CurrentStateName == "TeleportAttack" && !ReleasedAim)
				{
					Teleport = false;
					SpringArmComp->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, FName("CamMoveSocket"));
					UE_LOG(LogTemp, Log, TEXT("TelportAttack End!!"));
				}
				if (CurrentStateName == "SitAttack3" && ReleasedAim)
				{
					ReleasedAim = false;
					if (Teleport)
						SpringArmComp->CameraLagSpeed = 0.5f;

					UE_LOG(LogTemp, Log, TEXT("Reset Teleport Aim Do Teleport"));
				}

				/*if (Teleport && !ReleasedAim)
				{
					Teleport = false;
					UE_LOG(LogTemp, Log, TEXT("Bow Teleport Attack End"));
				}*/
				/*if (IsComboInputOn)
				{
					IsAttacking = false;
					IsComboInputOn = false;
				}
				else
				{
					Aiming = false;
					SpringArmComp->CameraLagSpeed = 2.f;
					ArmLengthTo = 350.f;

					SpringArmComp->DetachFromParent();
					SpringArmComp->AttachToComponent(GetCapsuleComponent(), FAttachmentTransformRules::KeepRelativeTransform);
				}*/
			}
			});


		// Hit check를 하는 타이밍에 서버에 hit check를 해달라고 보내게 함.
		CurrentAnimInstance->OnHitCheck.AddLambda([this]()->void {

			//칼 내려칠떄 먼지 파티클 재생
			if (CurrentWeaponType == WeaponType::SWORD && Teleport)
			{
				if (DustParticle)
				{
					FVector RotationVector(300.f, 0.f, 0.f);
					FVector UpAxis = FVector::UpVector;
					FVector SocketLocation = CurrentWeapon->FindComponentByClass<USkeletalMeshComponent>()->GetSocketLocation("GreatSwordSocket");
					SocketLocation.Z = GetActorLocation().Z + 50.f;
					for (int i = 0; i < 36; i++)
					{
						UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), DustParticle, SocketLocation + RotationVector.RotateAngleAxis(10.f * float(i), UpAxis), FRotator::ZeroRotator, FVector(1.f, 1.f, 1.f), true, EPSCPoolMethod::None);
					}
					auto CurrentClientController = GetWorld()->GetFirstPlayerController();
					CurrentClientController->PlayerCameraManager->PlayCameraShake(UTSCameraShake::StaticClass(), 3.f);
				}
			}

			});

		CurrentAnimInstance->OnHitReset.AddLambda([this]()->void {
			/*	Serializables::Input InputData;
				InputData.InputValue = TheShift::InputType::Hit_Reset;
				FRotator ControlRotation = GetControlRotation();
				InputData.ControlRotation = TheShift::Vector3f(ControlRotation.Roll, ControlRotation.Pitch, ControlRotation.Yaw);
				FRotator CamRotation = GetControlRotation();
				InputData.CamRotation = TheShift::Vector3f(CamRotation.Roll, CamRotation.Pitch, CamRotation.Yaw);
				PushInputQueue(InputData);

				CurrentAnimInstance->SetHit(false);*/
			});

	}
}

// Inventory에 있는 'Type'의 무기를 장착한다.
void ATSPlayer::EquipWeaponFromInventory(UWeaponItemType Type)
{
	// 1. 해당 무기가 어떤 종류의 무기인지 확인하고 해당 무기가 들어갈 슬롯에 무기가 장착되어있는지 확인
	switch(Type)
	{
	case UWeaponItemType::GreatSword:
	{
		//SwapWeapon(WeaponType::SWORD);
		OnWeaponSwapPressed(WeaponType::SWORD);
		break;
	}

	case UWeaponItemType::Bow:
	{
		//SwapWeapon(WeaponType::BOW);
		OnWeaponSwapPressed(WeaponType::BOW);
		break;
	}

	default:
		return;
	}
	// 2. 장착되어있다면 해당 무기를 해제하고 inventory로 옮겨줌
	// 3. 해제된 후 새로운 무기를 슬롯에 장착시켜준다. 
	// 4. 장착 완료했다면 해당 무기를 Inventory의 NotEquipedItems에서 제외시켜준다.
}

// Inventory에 있는 'Type'의 방어구를 장착한다.
void ATSPlayer::EquipArmorFromInventory(UArmorItemType Type)
{
	// 1. 해당 방어구가 어떤 종류의 방어구인지 확인하고 해당 방어구가 들어갈 슬롯에 방어구가 장착되어있는지 확인
	// 2. 장착되어있다면 해당 방어구를 해제하고 inventory로 옮겨줌
	// 3. 해제된 후 새로운 방어구를 슬롯에 장착시켜준다. 
	// 4. 장착 완료했다면 해당 방어구를 Inventory의 NotEquipedItems에서 제외시켜준다.
	//SwapArmor(Type);

	Serializables::SwapArmor Data;
	Data.Id = NetworkComp->GetUID();
	Data.SwapType = static_cast<uint8>(Type);
	SendPacket(Data);
}

// 'SlotIndex'에 장착되어있는 아이템을 장착 해제한다.
void ATSPlayer::UnequipItem(int SlotIndex)
{
	// 1. 해당 슬롯이 무기 슬롯인지 방어구 슬롯인지 확인
	// 2. 아이템 제거
}

// 아이템 획득 시 Inventory component에 의해 불리는 함수
void ATSPlayer::AcquireItem(UTSItemComponent* Item)
{
	Serializables::ItemAcquire Data;
	//Data.Index = Data.;
	Data.ItemCategory = static_cast<uint8_t>(Item->Type);
	switch(Item->Type)
	{
	case UItemType::Weapon:
	{
		if(auto WeaponItem = Cast<UTSWeaponItemComponent>(Item))
		{
			Data.ItemType = static_cast<int>(WeaponItem->WeaponType);
		}
		break;
	}

	case UItemType::Armor:
	{
		if(auto ArmorItem = Cast<UTSArmorItemComponent>(Item))
		{
			Data.ItemType = static_cast<int>(ArmorItem->ArmorType);
		}
		break;
	}
	default:
		return;
	}

	SendPacket(Data);
}

// 아이템을 inventory에서 제거할 때 inventory component에 의해 불리는 함수
void ATSPlayer::UnacquireItem(UTSItemComponent* Item)
{
	//Serializables::Item
}

// 아이템이 장착중인지 여부를 return한다.
bool ATSPlayer::CheckItemEquiped(UTSItemComponent* Item)
{
	// 어차피 크기 작은 컨테이너니까 순회해서 살펴보고 bool값 return하자.
	//InventoryComp->EquipedItems.
	for(auto& ItemPair : InventoryComp->EquipedItems)
	{
		// 장착중이라면 바로 return true
		if(ItemPair.Value == Item)
		{
			return true;
		}
	}

	// 순회를 마칠때까지 동일한 item을 찾지 못했다면 장착중인 item이 아니므로 return false
	return false;
}

// 장착된 아이템의 slot번호를 return한다.
int ATSPlayer::GetEquipedItemSlot(UTSItemComponent* Item)
{
	for(auto& ItemPair : InventoryComp->EquipedItems)
	{
		// 장착중이라면 바로 return
		if(ItemPair.Value == Item)
		{
			return ItemPair.Key;
		}
	}

	return -1;
}

void ATSPlayer::InitDirection()
{
	Forward = false;
	Back = false;
	Left = false;
	Right = false;

	UE_LOG(LogTemp, Log, TEXT("InitDirection!!"));
}

UTSCharacterAnimInstance* ATSPlayer::GetCurrentAnimInstance()
{
	return CurrentAnimInstance;
}

void ATSPlayer::GetDashInfo(bool* IsDash, int* DashDirection)
{
	*IsDash = this->Dash;
	*DashDirection = this->DashValue;
}

void ATSPlayer::SetWeaponVisibility(bool IsRender)
{
	auto Weapon = WeaponsOnSlot.Find(CurrentWeaponType);
	if (*Weapon)
		(*Weapon)->SetActorHiddenInGame(!IsRender);
	QuiverMeshComp->SetVisibility(IsRender && CurrentWeaponType == WeaponType::BOW);
	ArrowMeshComp->SetVisibility(false);
}

void ATSPlayer::SwapArmor(UArmorItemType NewArmorType)
{
	ArmorType = NewArmorType;

	GetMesh()->SetSkeletalMesh(PlayerMeshes[static_cast<int>(ArmorType)]);
	GetMesh()->SetAnimInstanceClass(PlayerAnimInstances[static_cast<int>(ArmorType)]);
	CurrentAnimInstance = Cast<UTSCharacterAnimInstance>(GetMesh()->GetAnimInstance());
	BindNotify();

	SpringArmComp->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, FName("CamMoveSocket"));


	//화살, 화살통 메쉬 소켓에 붙이기
	QuiverMeshComp->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, FName("QuiverSocket"));
	ArrowMeshComp->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, FName("ArrowSocket"));
	SetWeaponVisibility();
}


void ATSPlayer::LoadMontages()
{
	
}
