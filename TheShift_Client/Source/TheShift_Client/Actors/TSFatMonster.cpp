﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TSFatMonster.h"
#include "Network/Components/TSNetworkedComponent.h"
#include "TSCharacterAnimInstance.h"
#include "TSWeapon.h"
#include "Components/TSNullComponent.h"
#include "Protocol/Log.h"
#include "Protocol/Serializables.h"
#include "Animation/AnimSequence.h"
#include "Components/WidgetComponent.h"
#include "TheShift_ClientGameModeBase.h"
#include "TSCameraShake.h"
#include "Actors/TSProjectile.h"
ATSFatMonster::ATSFatMonster()
{
	// NetworkComp setting
	NetworkComp->SetActorType(ActorType::FatMonster);
	//NetworkComp->SetActorTag("Character");

	GetMesh()->SetRelativeLocationAndRotation(FVector(0.f, 0.f, -89.f), FRotator(0.f, -90.f, 0.f));

	//Default Mesh
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> skeletalMesh(TEXT("/Game/Graphic/DynamicMesh/Monster/FatMonster/FatMonster.FatMonster"));
	if (skeletalMesh.Succeeded())
		GetMesh()->SetSkeletalMesh(skeletalMesh.Object);

	GetMesh()->SetRelativeScale3D(FVector(1.4f, 1.4f, 1.4f));

	GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);
	static ConstructorHelpers::FClassFinder<UAnimInstance> defaultAnimInstance(TEXT("/Game/Graphic/DynamicMesh/Monster/FatMonster/Animation/AnimBP_FatMonster.AnimBP_FatMonster_C"));
	if (defaultAnimInstance.Succeeded())
		GetMesh()->SetAnimInstanceClass(defaultAnimInstance.Class);

	static ConstructorHelpers::FObjectFinder<UAnimSequence> attack1(TEXT("/Game/Graphic/DynamicMesh/Monster/FatMonster/Animation/FatMonster_Attack01.FatMonster_Attack01"));
	if (attack1.Succeeded())
		AttackAnim[0] = attack1.Object;
	static ConstructorHelpers::FObjectFinder<UAnimSequence> attack2(TEXT("/Game/Graphic/DynamicMesh/Monster/FatMonster/Animation/FatMonster_Attack02.FatMonster_Attack02"));
	if (attack2.Succeeded())
		AttackAnim[1] = attack2.Object;
	static ConstructorHelpers::FObjectFinder<UAnimSequence> attack3(TEXT("/Game/Graphic/DynamicMesh/Monster/FatMonster/Animation/FatMonster_Attack03.FatMonster_Attack03"));
	if (attack3.Succeeded())
		AttackAnim[2] = attack3.Object;

	static ConstructorHelpers::FObjectFinder<UAnimMontage> lockOnMontage(TEXT("/Game/Graphic/DynamicMesh/Monster/FatMonster/Animation/2_LockOn.2_LockOn"));
	if (lockOnMontage.Succeeded())
		LockOnMontage = lockOnMontage.Object;

	static ConstructorHelpers::FObjectFinder<UAnimMontage> combo2Montage(TEXT("/Game/Graphic/DynamicMesh/Monster/FatMonster/Animation/TwoComboMontage.TwoComboMontage"));
	if (combo2Montage.Succeeded())
		Combo2Montage = combo2Montage.Object;

	static ConstructorHelpers::FObjectFinder<UAnimMontage> combo3Montage(TEXT("/Game/Graphic/DynamicMesh/Monster/FatMonster/Animation/ThreeComboMontage.ThreeComboMontage"));
	if (combo3Montage.Succeeded())
		Combo3Montage = combo3Montage.Object;

	//GrapplerHit
	static ConstructorHelpers::FObjectFinder <UAnimSequence> defaultHitAnim(TEXT("/Game/Graphic/DynamicMesh/Monster/FatMonster/Animation/FatMonster_Hit.FatMonster_Hit"));
	if (defaultHitAnim.Succeeded())
		DefaultHitAnim = defaultHitAnim.Object;

	CurrentWeaponType = WeaponType::SWORD;

	bSimGravityDisabled = true;
	GetMesh()->SetEnableGravity(false);
	GetCapsuleComponent()->SetEnableGravity(false);
	GetCharacterMovement()->GravityScale = 0.f;
}

void ATSFatMonster::BeginPlay()
{
	Super::BeginPlay();
}

void ATSFatMonster::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	CurrentAnimInstance = Cast<UTSCharacterAnimInstance>(GetMesh()->GetAnimInstance());

	CurrentAnimInstance->OnHitCheck.AddLambda([this]()->void {

		//모든 카메라 쉐이킹으로바꿔야함.
		float Power = 0.2f;
		if (RecentActType == TheShift::ActType::Smash_2)
			Power = 1.f;
		else if (RecentActType == TheShift::ActType::Smash_1)
			Power = 1.f;

		auto CurrentClientController = GetWorld()->GetFirstPlayerController();
		CurrentClientController->PlayerCameraManager->PlayCameraShake(UTSCameraShake::StaticClass(), Power);
		});


	CurrentAnimInstance->OnDestroyWork.AddLambda([this]()->void {
		auto gameMode = Cast<ATheShift_ClientGameModeBase>((GetWorld()->GetAuthGameMode()));
		if (gameMode)
			gameMode->MarkActorAsGoodToBeDestroyed(this);

		});
}

void ATSFatMonster::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ATSFatMonster::Act(TheShift::ActType ActEnum)
{
	RecentActType = ActEnum;
	CurrentAnimInstance->StopAllMontages(0.25f);
	switch (ActEnum)
	{
	case ActType::BaseAttack_2:
	
		Cast<UTSCharacterAnimInstance>(GetAnimInstance())->PlayAttackMontage(Combo2Montage);
		break;
	case ActType::BaseAttack_3:
		
		Cast<UTSCharacterAnimInstance>(GetAnimInstance())->PlayAttackMontage(Combo3Montage);

		break;
	case ActType::Smash_1:
		Cast<UTSCharacterAnimInstance>(GetAnimInstance())->RestartAnimation(ExportAnims[4], FName("DefaultSlot"), AnimPlayRates[4]);
		break;
	case ActType::Smash_2:
	
		Cast<UTSCharacterAnimInstance>(GetAnimInstance())->PlayAttackMontage(LockOnMontage);
		break;
	}
}

void ATSFatMonster::Act(TheShift::ActType ActEnum, FVector Vector)
{
	switch (ActEnum)
	{
	case ActType::Hit_Reset:
		IsHit = false;
		UE_LOG(LogTemp, Log, TEXT("Monster hitReset"));
		break;
	}
}

void ATSFatMonster::Act(TheShift::ActType ActEnum, TheShift::UID TargetActorId)
{
	if (!IsDead && !IsHit)
		Cast<UTSCharacterAnimInstance>(GetAnimInstance())->RestartAnimation(AttackAnim[static_cast<int>(ActEnum) - 1], FName("DefaultSlot"), 1.7f);
}

void ATSFatMonster::Act(TheShift::ActType ActEnum, int Value)
{
	switch (ActEnum)
	{
	case ActType::Dash:
		if (Value == 1)//왼쪽
		{
			CurrentAnimInstance->StopAllMontages(0.25f);
			Cast<UTSCharacterAnimInstance>(GetAnimInstance())->RestartAnimation(ExportAnims[5], FName("DefaultSlot"), AnimPlayRates[5]);
		}
		else
		{
			CurrentAnimInstance->StopAllMontages(0.25f);
			Cast<UTSCharacterAnimInstance>(GetAnimInstance())->RestartAnimation(ExportAnims[6], FName("DefaultSlot"), AnimPlayRates[6]);
		}
		break;
	case ActType::Cam_Rotation:
		if (Value == 1)//왼쪽
		{
			CurrentAnimInstance->StopAllMontages(0.25f);
			Cast<UTSCharacterAnimInstance>(GetAnimInstance())->RestartAnimation(ExportAnims[7], FName("DefaultSlot"), AnimPlayRates[7], 10);
		}
		else
		{
			CurrentAnimInstance->StopAllMontages(0.25f);
			Cast<UTSCharacterAnimInstance>(GetAnimInstance())->RestartAnimation(ExportAnims[8], FName("DefaultSlot"), AnimPlayRates[8], 10);
		}
		break;
	}
}

void ATSFatMonster::OnHit(float Hp, float Damage, TheShift::ActType AttackType, TheShift::UID AttackerId, bool IsDead)
{
	ATSCharacter::OnHit(Hp, Damage, AttackType, AttackerId, IsDead);

	// 데미지 적용, 피격 애니메이션 재생
	//LOG_INFO("Actor: %d took damage of %f from Actor: %d", NetworkComp->GetUID(), Damage, AttackerId);
	//...
	if (IsDead)
	{
		// 사망 애니메이션 재생? 사망 처리
		// 다 끝나고 Actor 삭제해줘야 함.
		// ActorsToBeDestroyed 에 추가되어있는 value를 true로 변경하면 다음 프레임에 삭제됨
		CurrentAnimInstance->StopAllMontages(0.25f);
		BigHit = false;
		IsHit = false;
		this->IsDead = true;
		BowAimUIComponent->SetVisibility(false);
		LOG_INFO("FatMonster Dead!!");

		UClass* SpawnActor = StaticLoadClass(AActor::StaticClass(), NULL, TEXT("/Game/BP_SoulItem.BP_SoulItem_C"));
		if (SpawnActor)
		{
			LOG_INFO("Soul spawn");
			FActorSpawnParameters SpawnParams;
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			FRotator TempRotator = FRotator::ZeroRotator;
			auto Location = GetCapsuleComponent()->GetComponentLocation();
			auto Actor = GetWorld()->SpawnActor(SpawnActor, &Location, &TempRotator, SpawnParams);

			auto GameMode = Cast<ATheShift_ClientGameModeBase>(GetWorld()->GetAuthGameMode());

			if (auto Soul = Cast<ATSProjectile>(Actor))
				Soul->SetTargetActor(GameMode->GetTSActor(AttackerId));
			//텔레포트게이지는 바로올려야함 밑에 코드로 작성.
		}
	}
	else
	{
		UE_LOG(LogTemp, Log, TEXT("FatMonster Hit!!!"));
		//CurrentAnimInstance->StopAllMontages(0.25f);
		//IsHit = true;
	}
}

void ATSFatMonster::ExportAnimData(FString FileName)
{
	std::vector<AnimNotifyInfo> NotifyInfoContainer;

	//SlotAnims
	if (ExportAnims.Num() == AnimPlayRates.Num())
	{
		for (int i = 0; i < ExportAnims.Num(); ++i)
			UAnimNotifiesExporter::ExportSlotAnimNotifies(&NotifyInfoContainer, ExportAnims[i], AnimPlayRates[i]);
	}

	//SwordAttackMontage
	UAnimNotifiesExporter::ExportAnimMontageNotifies(&NotifyInfoContainer, LockOnMontage);

	//SwordAttackMontage
	UAnimNotifiesExporter::ExportAnimMontageNotifies(&NotifyInfoContainer, Combo2Montage);

	//SwordAttackMontage
	UAnimNotifiesExporter::ExportAnimMontageNotifies(&NotifyInfoContainer, Combo3Montage);


	//Json에 애님Notify정보 기입
	JsonFormatter::JsonWriter JsonExporter;
	FString TargetPath = FPaths::ProjectContentDir() + TEXT("Protocol/Json/Notifies/") + FileName + TEXT(".json");
	JsonExporter.ExportAnimNotifyInfo(TCHAR_TO_UTF8(*TargetPath), NotifyInfoContainer);
	JsonExporter.Close();
}
