﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "EngineMinimal.h"
#include "TSWeapon.generated.h"

UENUM(BlueprintType)
enum class WeaponType : uint8
{
	EQUIP_NONE = 0,
	HAND,
	SWORD,
	DAGGER,
	BOW,
	WEAPON_END
};

UCLASS()
class THESHIFT_CLIENT_API ATSWeapon : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATSWeapon();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//이 무기의 타입
	const WeaponType GetWeaponType() const;
	void ChangeWeaponMesh(FName Name);
	void InitWeapon(FName Name, WeaponType Type);

	void PlayBowAnim();

public:
	UPROPERTY(VisibleAnywhere, Category = Mesh)
	class USkeletalMeshComponent* MeshComponent;

	UPROPERTY(EditAnywhere, Category = Type)
	WeaponType TypeOfWeapon;

	// WeaponType별 Socket name
	static TMap<WeaponType, FName> WeaponSockets;

	UPROPERTY()
		class UAnimSequence* BowAnim;

private:
	//로딩될 메쉬의 컨테이너
	static TMap<FName, class USkeletalMesh*> Meshes;

	//현재 메쉬의 이름
	UPROPERTY(EditAnywhere, Category = Name, Meta = (AllowPrivateAccess = true))
	FName WeaponName;

	UPROPERTY(EditAnywhere, Category = Name, Meta = (AllowPrivateAccess = true))
	FName SocketName;
};
