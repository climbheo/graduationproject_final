﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TSProjectile.generated.h"

UCLASS()
class THESHIFT_CLIENT_API ATSProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATSProjectile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
public:
	void SetTargetActor(AActor* TargetActor) { this->TaretActor = TargetActor; }

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Target, Meta = (AllowPrivateAccess = true))
	AActor* TaretActor = nullptr;

};
