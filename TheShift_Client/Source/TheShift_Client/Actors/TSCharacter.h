﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "EngineMinimal.h"
#include "GameFramework/Character.h"
#include "TSWeapon.h"
#include "Protocol/Types.h"
#include "Protocol/Serializables.h"
#include "Components/PoseableMeshComponent.h"
#include "TSTeleportEffectActor.h"
#include "AnimNotifiesExporter.h"
#include "TSCharacter.generated.h"

//UBlueprint* GenerateBP(const TCHAR* BlueprintPath);

UCLASS()
class THESHIFT_CLIENT_API ATSCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ATSCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	//virtual void PostInitializeComponents() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	//virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

public://Server와 맞닿는 부분에서 보통 호출될 함수들

	//Play Montage Func
	void PlayAttackMontage(class UAnimMontage* AnimMontage, int32 SectionNumber);

	//Set GoalRotation
	void SetGoalRotation(float Pitch, float Yaw, float Roll);

	void Act(TheShift::Serializables::Act* Data);
	virtual void Act(TheShift::ActType ActEnum);
	virtual void Act(TheShift::ActType ActEnum, FVector Vector);
	virtual void Act(TheShift::ActType ActEnum, TheShift::UID TargetActorId);
	virtual void Act(TheShift::ActType ActEnum, std::vector<TheShift::UID>& IdsOfTargets);
	virtual void Act(TheShift::ActType ActEnum, int32 Value);
	virtual void OnHit(float Hp, float Damage, TheShift::ActType AttackType, TheShift::UID AttackerId, bool IsDead);
	virtual void OnRecognize(TheShift::UID OtherActorId);

	//Get CurrentWeaponType
	const WeaponType GetCurrentWeaponType() const;
	bool  GetSwapping() const { return IsSwapping; }
	class UTSCharacterAnimInstance* GetAnimInstance();
	bool  GetDead() const { return IsDead; }

	//나중에 Damagable쪽으로 뺄 함수 (생명주기관련함수)
	void SetDead(bool IsPlayer = false);
	void SetSwapping(bool Swapping) { IsSwapping = Swapping; }

	//Hit Info
	bool GetHit() { return IsHit || BigHit; }
	bool IsOnlySmallHit(){ return IsHit; }
	void SetHit(bool Hit) { IsHit = Hit; }
	bool IsBigHit() { return BigHit; }

	float GetCurrentHp() const;
	void SetCurrentHp(float NewHp);

	UFUNCTION(BlueprintCallable, Category = "Rendering")
	void PlayHitEffect(FLinearColor HitColor);
protected:
	//Input 관련 Function
	void PushInputQueue(TheShift::Serializables::Input Input);
	void SwapWeapon(WeaponType Type);

	//Anim Export Function
	UFUNCTION(BlueprintCallable, Category = "Anim")
	virtual void ExportAnimData(FString FileName);

	//OnDestroyed()
	UFUNCTION()
	void OnCharacterDestroyed(AActor* DestroyedActor);

	// Weapons
	// 무기는 Type별로 하나씩 장착 가능
	TMap<WeaponType, ATSWeapon*> WeaponsOnSlot;
	void EquipWeapon(WeaponType TypeOfWeapon);
	void AddWeaponOnSlot(ATSWeapon* WeaponToAdd);

public:
	//Direction Variables
	//bool Forward = false;
	//bool Back = false;
	//bool Left = false;
	//bool Right = false;

	//Location을 발바닥으로 옮겨주기 위한 새로운 Root component
	UPROPERTY(VisibleAnywhere)
	class USceneComponent* NullComp;

	//서버와 관련된 컴포넌트
	UPROPERTY(VisibleAnywhere)
	class UTSNetworkedComponent* NetworkComp;

	//액터가 데미지를 받는 방식을 관리하는 컴포넌트
	UPROPERTY(VisibleAnywhere, Category = Damage)
	class UTSDamagable* DamagableComp;

	// 삭제 할 Actor에 등록되어있을 때 값이 true면 다음 frame에 삭제된다.
	UPROPERTY(EditAnywhere)
	bool GoodToBeDestroyed = false;

	// 무기 슬롯 최대 크기
	UPROPERTY(EditAnywhere)
	int MaxWeaponSlot = 4;

	//현재 장착한 무기 액터(컴포넌트 X)
	UPROPERTY(VisibleAnywhere, Category = Weapon)
	class ATSWeapon* CurrentWeapon;

	UPROPERTY(VisibleAnywhere, Category = Temp)
	class UCapsuleComponent* NewRoot;

protected:
	//SpringArm 거리 관련 변수
	float ArmLengthTo = 0.f;
	float ArmLengthSpeed = 0.f;

protected:
	////회전 계산 함수
	//void CaculateGoalRotation();

	//콤보어택 관련 함수
	void AttackStartComboState();
	void AttackEndComboState();

protected:
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	float CurrentHp;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	float MaxHp;
	
	// 기본 공격 Montage
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Armor, Meta = (AllowPirvateAccess = true))
	TArray<class UAnimMontage*> NormalAttackMontage;

	//현재 애님 인스턴스(AnimBP가 이 클래스를 상속)
	UPROPERTY()
	class UTSCharacterAnimInstance* CurrentAnimInstance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon, Meta = (AllowPrivateAccess = true))
	WeaponType CurrentWeaponType;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attack, Meta = (AllowPrivateAccess = true))
	bool IsAttacking;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attack, Meta = (AllowPrivateAccess = true))
	bool CanNextCombo;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attack, Meta = (AllowPrivateAccess = true))
	bool IsComboInputOn;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attack, Meta = (AllowPrivateAccess = true))
	int32 CurrentCombo;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attack, Meta = (AllowPrivateAccess = true))
	int32 MaxCombo;

	//캐릭터가 죽었는가?
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Pawn, Meta = (AllowPrivateAccess = true))
	bool IsDead = false;

	//장비 교체중인가?
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Pawn, Meta = (AllowPrivateAccess = true))
	bool IsSwapping = false;

	//타격애님이 재생중인가?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	bool IsHit = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	//아예 처맞고 밀려났는가
		bool BigHit = false;

	//Player의 움직임의 방향과 회전값
	UPROPERTY(Transient)
	FVector DirectionToMove = FVector::ZeroVector;

	UPROPERTY(Transient)
	FRotator GoalRotator = FRotator::ZeroRotator;

	UPROPERTY(Transient)
	float RotationSpeed = 0.2f;

	//Export Anims
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attack, Meta = (AllowPrivateAccess = true))
	TArray<class UAnimSequence*> ExportAnims;

	//Slot Anim Play Rates
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attack, Meta = (AllowPrivateAccess = true))
	TArray<float> AnimPlayRates;

	UPROPERTY()
	TSubclassOf<AActor> TeleportEffectBPClass;
};
