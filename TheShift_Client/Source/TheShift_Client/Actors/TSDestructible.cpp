﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TSDestructible.h"
#include "DestructibleComponent.h"
#include "Protocol/Log.h"
#include "PxMaterial.h"
#include "PhysicsInterfaceDeclaresCore.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/TSGiggleActionComponent.h"
#include "TSCameraShake.h"
#include "TheShift_ClientGameModeBase.h"
#include "TSPlayer.h"
// Sets default values
ATSDestructible::ATSDestructible()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Default Root Component"));
	NetworkComp = CreateDefaultSubobject<UTSNetworkedComponent>(TEXT("Networked Component"));
}

// 
void ATSDestructible::OnHit(float Hp, float Damage, TheShift::ActType AttackType, TheShift::UID AttackerId, bool IsDead)
{
	if (IsDead)
	{
		LOG_INFO("Destruct!");
		// 파괴 처리
		for (auto Child : DestructibleChildren)
		{
			auto DestructibleComponent = Cast<UDestructibleComponent>(Child->GetComponentByClass(UDestructibleComponent::StaticClass()));
			if (DestructibleComponent)
			{
				Child->SetActorEnableCollision(true);
				DestructibleComponent->WakeAllRigidBodies();
				DestructibleComponent->SetCollisionProfileName("Destructible");
				DestructibleComponent->RecreatePhysicsState();
				DestructibleComponent->UpdatePhysicsVolume(true);
				DestructibleComponent->SetSimulatePhysics(true);
				float RandomFloat = FMath::FRandRange(-100.f, 100.f);
				DestructibleComponent->AddRadialImpulse(GetActorLocation(), 500.f, 500.f, ERadialImpulseFalloff::RIF_Linear, true);
			}
			else
			{
				LOG_ERROR("Can't find destructible component.");
			}

			auto SensableActors = Child->GetComponentsByTag(UActorComponent::StaticClass(),"Sensable");
			for (auto& iter : SensableActors)
				iter->DestroyComponent();
		}
		
	}
	else
	{
		for (auto Child : DestructibleChildren)
		{
			auto GiggleComponent = Cast<UTSGiggleActionComponent>(Child->GetComponentByClass(UTSGiggleActionComponent::StaticClass()));
			if (GiggleComponent)
			{
				GiggleComponent->StartGiggle();
				LOG_INFO("destructible HiT!!!");
			}
			if (auto GameMode = Cast<ATheShift_ClientGameModeBase>(GetWorld()->GetAuthGameMode()))
			{
				if (GameMode)
				{
					auto AttackerActor = GameMode->GetTSActor(AttackerId);
					ATSPlayer* AttackPlayer = nullptr;
					if (AttackerActor)
						AttackPlayer = Cast<ATSPlayer>(AttackerActor);
					if (AttackPlayer == GameMode->GetMyPlayer())
					{
						auto CurrentClientController = GetWorld()->GetFirstPlayerController();
						CurrentClientController->PlayerCameraManager->PlayCameraShake(UTSCameraShake::StaticClass(), 1.f);
					}
					else
						UE_LOG(LogTemp, Log, TEXT("NOt my Player"));
				}
			}

		
		}
		
	}
	SetActorEnableCollision(false);
}

// Called when the game starts or when spawned
void ATSDestructible::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATSDestructible::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

