﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TSInteractable.generated.h"

UCLASS()
class THESHIFT_CLIENT_API ATSInteractable : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATSInteractable();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	bool RotateForTimeOn = false;
	FRotator AngularVelocity;
	float RotationDuration;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//서버와 관련된 컴포넌트
	UPROPERTY(VisibleAnywhere)
	class UTSNetworkedComponent* NetworkComp;

	void RotateForTime(FRotator& GoalRotationOffset, float Duration);
};
