﻿#include "Game/Components/BoundingVolume.h"
#include "Game/Actors/Actor.h"
#include "Game/System/CollisionResolver.h"
#include "Protocol/Log.h"


namespace TheShift
{
// Local Transform을 설정해준다.
TheShift::BoundingVolume::BoundingVolume(Eigen::Vector3f& RelativeLocation, Eigen::Vector3f& RelativeRotation)
{
	LocalTransform.SetTranslation(RelativeLocation);
	LocalTransform.SetRotationWithDegrees(RelativeRotation);
	CollisionPlaneNormalVector.setZero();
	IntersectedLength = 0.f;

	LocalTransform.RegisterBoundingVolume(this);
}

TheShift::BoundingVolume::BoundingVolume()
{
	CollisionPlaneNormalVector.setZero();
	IntersectedLength = 0.f;

	LocalTransform.RegisterBoundingVolume(this);
}

// Child Volume들을 같이 삭제한다.
BoundingVolume::~BoundingVolume()
{
	//auto BoundingVolumes = GetParentActor()->GetBoundingVolumes();
	//std::remove(BoundingVolumes.begin(), BoundingVolumes.end(), this);
}

// 이 Volume을 사용하고있는 Resolver를 등록한다.
void BoundingVolume::SetCollisionResolver(CollisionResolver* Owner)
{
	Resolver = Owner;
}

BoundingVolumeType BoundingVolume::GetVolumeType() const
{
	return VolumeType;
}

CollisionType BoundingVolume::GetCollisionBehavior() const
{
	return CollisionBehavior;
}

// 현재 위치를 이 Volume의 이전 위치로 저장한다.
void BoundingVolume::SetLastTranslation()
{
	LastTranslation = LocalTransform.GetWorldTranslation();
}

void BoundingVolume::SetId(UID NewId)
{
	Id = NewId;
}

bool BoundingVolume::Intersect(BoundingVolume& other)
{
	bool RetVal = false;
	CHECK_VOLUMETYPE_AND_DO_COLLIDE(RetVal, other);
	return RetVal;
}

void BoundingVolume::CalculateAlignedExtent()
{

}

// Transform이 변경되었으므로 Extent가 다시 계산되어야 함을 알린다.
// ExtentUpdateNeeded가 true면 GetAlignedExtent()를 부를때 Extent를 다시 계산하고 false로 값을 변경한다.
void BoundingVolume::NotifyExtentUpdateNeeded()
{
	ExtentUpdateNeeded = true;
}

float BoundingVolume::GetCollisionRange() const
{
	return CollisionRange;
}

std::shared_ptr<TheShift::BoundingVolume> BoundingVolume::GetParentVolume() const
{
	return ParentVolume;
}

TheShift::UID BoundingVolume::GetId() const
{
	return Id;
}

void BoundingVolume::SetCollisionBehavior(CollisionType type)
{
	CollisionBehavior = type;
}

void BoundingVolume::SetParent(std::shared_ptr<Actor> Parent)
{
	ParentActor = Parent;
	LocalTransform.SetParent(&Parent->GetTransform());
	Parent->GetTransform().AddChild(&LocalTransform);
	Parent->RegisterBoundingVolume(this);
	IsMovable = Parent->GetIsMovable();
}

void BoundingVolume::SetParent(std::shared_ptr<BoundingVolume> Parent)
{
	ParentVolume = Parent;
	LocalTransform.SetParent(&Parent->GetTransform());
	Parent->GetTransform().AddChild(&LocalTransform);
	auto ParentActor = GetParentActor();
	ParentActor->RegisterBoundingVolume(this);
	IsMovable = ParentActor->GetIsMovable();
}

//// World Transform을 업데이트한다.
//void TheShift::BoundingVolume::UpdateWorldTransform()
//{
//    if(ParentActor != nullptr)
//        WorldTransform.SetTransform(LocalTransform.GetData() * ParentActor->GetTransform().GetData());
//    else
//        WorldTransform.SetTransform(LocalTransform.GetData() * ParentVolume->GetWorldTransform().GetData());
//    
//    // 내가 바뀌었으니 자식들도 모두 update해준다.
//    for(auto& Volume : ChildVolumes)
//    {
//        Volume->UpdateWorldTransform();
//    }
//}

Transform& TheShift::BoundingVolume::GetTransform()
{
	return LocalTransform;
}

Eigen::Vector3f TheShift::BoundingVolume::GetLastTranslation() const
{
	return LastTranslation;
}

Eigen::Vector3f TheShift::BoundingVolume::GetLastDegrees() const
{
	return LastDegrees;
}

void TheShift::BoundingVolume::SetLastTranslation(const Eigen::Vector3f& LastValue)
{
	LastTranslation = LastValue;
}

void TheShift::BoundingVolume::SetLastDegrees(const Eigen::Vector3f& LastValue)
{
	LastDegrees = LastValue;
}

// 충돌 면의 정보를 return한다.
// Intersect() 가 불리기 전에 불리면 안된다.
// Intersect() 가 true를 return한 경우에 값이 갱신된다.
// Intersect() 가 false를 return한 경우에 둘다 0.f, 0.f, 0.f을 return한다.
void TheShift::BoundingVolume::GetLastCollisionPlaneInfo(Eigen::Vector3f& PlaneNormalVector, Eigen::Vector3f& PlaneLocation) const
{
	PlaneNormalVector = CollisionPlaneNormalVector;
	PlaneLocation = CollisionPlaneLocation;
}

// 충돌 한 면의 정보를 set해준다.
// 충돌체크 시 자신의 값만 수정하지 않고 상대 Volume도 수정해주기 위함.
void TheShift::BoundingVolume::SetLastCollisionPlaneInfo(Eigen::Vector3f& PlaneNormalVector, Eigen::Vector3f& PlaneLocation)
{
	CollisionPlaneNormalVector = PlaneNormalVector;
	CollisionPlaneLocation = PlaneLocation;
}

// 바로 직전에 발생한 충돌에서 겹쳐진 길이를 return한다.
// Intersect() 가 불리기 전에 불리면 안된다.
// Intersect() 가 true를 return한 경우에 값이 갱신된다.
// Intersect() 가 false를 return한 경우에 0.f을 return한다.
float TheShift::BoundingVolume::GetLastIntersectedLength() const
{
	return IntersectedLength;
}

// 겹쳐진 길이를 set해준다.
// 충돌체크 시 자신의 값만 수정하지 않고 상대 Volume도 수정해주기 위함.
void TheShift::BoundingVolume::SetLastIntersectedLength(float NewLength)
{
	IntersectedLength = NewLength;
}

//// World Transform을 return한다. 계산이 되어있지 않을 수 있다.
//const Transform& TheShift::BoundingVolume::GetWorldTransform() const
//{
//    return LocalTransform.GetWorld;
//}

// 이 Volume을 소유한 Actor를 return한다.
std::shared_ptr<TheShift::Actor> TheShift::BoundingVolume::GetParentActor() const
{
	if (ParentActor != nullptr)
		return ParentActor;
	else
		return ParentVolume->GetParentActor();
}

Eigen::Vector3f TheShift::BoundingVolume::GetRelativeLocation() const
{
	return LocalTransform.GetTranslation();
}

Eigen::Vector3f TheShift::BoundingVolume::GetRelativeRotation() const
{
	return LocalTransform.GetDegrees();
}

Eigen::Vector3f TheShift::BoundingVolume::GetExtent() const
{
	return Eigen::Vector3f(0.f, 0.f, 0.f);
}

float TheShift::BoundingVolume::GetHalfHeight() const
{
	return 0.f;
}

float TheShift::BoundingVolume::GetRadius() const
{
	return 0.f;
}

bool TheShift::BoundingVolume::GetIsMovable() const
{
	return IsMovable;
}

// Set this volume as a Trigger
void TheShift::BoundingVolume::SetAsTrigger()
{
	IsCollider_ = false;
	IsTrigger_ = true;
}

// Set this volume as a Collider
void TheShift::BoundingVolume::SetAsCollider()
{
	IsCollider_ = true;
	IsTrigger_ = false;
}

//// Trigger에 다른 Bounding Volume이 들어왔을 때 실행되는 함수를 설정한다.
//void TheShift::BoundingVolume::SetOnTriggerEnter(std::function<void(std::shared_ptr<BoundingVolume>)> NewOnTriggerEnterFunc)
//{
//    OnTriggerEnterFunc = NewOnTriggerEnterFunc;
//}
//
//// Trigger에 있던 Bounding Volume이 나갔을 때 실행되는 함수를 설정한다.
//void TheShift::BoundingVolume::SetOnTriggerLeave(std::function<void(std::shared_ptr<BoundingVolume>)> NewOnTriggerLeaveFunc)
//{
//    OnTriggerLeaveFunc = NewOnTriggerLeaveFunc;
//}

void TheShift::BoundingVolume::SetOnTriggerOverlap(std::function<void(std::shared_ptr<BoundingVolume>)> NewOnTriggerOverlapFunc)
{
	OnTriggerOverlapFunc = NewOnTriggerOverlapFunc;
}

void TheShift::BoundingVolume::SetRelativeLocation(const Eigen::Vector3f& NewRelativeLocation)
{
	LocalTransform.SetTranslation(NewRelativeLocation);
}

void TheShift::BoundingVolume::SetRelativeRotation(const Eigen::Vector3f& NewRelativeRotation)
{
	LocalTransform.SetRotationWithDegrees(NewRelativeRotation);
}

// 충돌처리 과정에서 움직이는 객체인지 정한다.
void TheShift::BoundingVolume::SetIsMovable(bool Movable)
{
	IsMovable = Movable;
}

void TheShift::BoundingVolume::SetRelativeLocation(const TheShift::Vector3f& NewRelativeLocation)
{
	LocalTransform.SetTranslation(NewRelativeLocation.X, NewRelativeLocation.Y, NewRelativeLocation.Z);
}

void TheShift::BoundingVolume::SetRelativeRotation(const TheShift::Vector3f& NewRelativeRotation)
{
	LocalTransform.SetRotationWithDegrees(NewRelativeRotation.X, NewRelativeRotation.Y, NewRelativeRotation.Z);
}

// Returns true if this volume is a trigger
bool TheShift::BoundingVolume::IsTrigger() const
{
	return IsTrigger_;
}

// Returns true if this volume is a collider
bool TheShift::BoundingVolume::IsCollider() const
{
	return IsCollider_;
}

//// Calls OnTriggerEnterFunc
//// Does nothing if any functions are registered.
//void TheShift::BoundingVolume::OnTriggerEnter(std::shared_ptr<BoundingVolume> Other)
//{
//    if(OnTriggerEnterFunc != nullptr)
//        OnTriggerEnterFunc(Other);
//    else
//        spdlog::warn("OnTriggerEnter called but there's no OnTriggerEnterFunc registered.");
//}
//
//// Calls OnTriggerLeaveFunc
//// Does nothing if any functions are registered.
//void TheShift::BoundingVolume::OnTriggerLeave(std::shared_ptr<BoundingVolume> Other)
//{
//    if(OnTriggerLeaveFunc != nullptr)
//        OnTriggerLeaveFunc(Other);
//    else
//        spdlog::warn("OnTriggerLeave called but there's no OnTriggerLeaveFunc registered.");
//}

void TheShift::BoundingVolume::OnTriggerOverlap(std::shared_ptr<BoundingVolume> Other)
{
	if(Other->GetParentActor()->GetType() == ActorType::Destructible)
	{
		LOG_INFO("Trigger on destructible");
	}
	if (OnTriggerOverlapFunc != nullptr)
		OnTriggerOverlapFunc(Other);
	else
		//spdlog::warn("OnTriggerOverlap called but there's no OnTriggerOverlapFunc registered.");
		LOG_WARNING("OnTriggerOverlap called but there's no OnTriggerOverlapFunc registered.");
}

Eigen::Vector3f TheShift::BoundingVolume::GetMaxPoint()
{
	// 여기서 WorldTransform이 update되니까 GetWorldTransform()을 부를 필요 없음.
	auto ArrangedExtent = GetAlignedExtent();

	return ArrangedExtent + LocalTransform.GetWorldTranslation();
}

Eigen::Vector3f TheShift::BoundingVolume::GetMinPoint()
{
	// 여기서 WorldTransform이 update되니까 GetWorldTransform()을 부를 필요 없음.
	auto ArrangedExtent = GetAlignedExtent();

	return -ArrangedExtent + LocalTransform.GetWorldTranslation();
}

Eigen::Vector3f TheShift::BoundingVolume::GetAlignedExtent()
{
	// Extent가 다시 계산되어야 한다면 다시 계산하고 return한다.
	if (ExtentUpdateNeeded == true)
		CalculateAlignedExtent();
	return CalculatedAlignedExtent;
}

void TheShift::BoundingVolume::RegisterBoundingVolumeRearranger(std::function<void(BoundingVolume*)> Rearranger)
{
	LocalTransform.RegisterBoundingVolumeRearranger(Rearranger);
}

OBB::OBB(Eigen::Vector3f& RelativeLocation, Eigen::Vector3f& RelativeRotation, Eigen::Vector3f& NewExtent) :
	BoundingVolume(RelativeLocation, RelativeRotation), Extent(NewExtent)
{
	VolumeType = BoundingVolumeType::OBB;
}

bool OBB::Intersect(BoundingVolume& Other)
{
	bool RetVal = false;
	CHECK_VOLUMETYPE_AND_DO_COLLIDE(RetVal, Other);
	return RetVal;
}

bool OBB::Intersect(AABB& Other)
{
	LOG_INFO("OBB VS AABB");
	// 상대를 OBB로 만들고 충돌체크한다.
	OBB NewOBB;
	NewOBB.SetExtent(Other.GetExtent());
	NewOBB.GetTransform().SetRotation(Other.GetTransform().GetWorldRotation());
	NewOBB.GetTransform().SetTranslation(Other.GetTransform().GetWorldTranslation());

	return Intersect(NewOBB);
}

bool OBB::Intersect(OBB& Other)
{
	LOG_INFO("OBB VS OBB");
	// Separating axis검사 15번 하면 된다.
	Eigen::Quaternionf RotationA = GetTransform().GetWorldRotation();
	Eigen::Quaternionf RotationB = Other.GetTransform().GetWorldRotation();

	// 각 박스의 축
	Eigen::Vector3f AxisX1 = RotationA * Eigen::Vector3f::UnitX();
	Eigen::Vector3f AxisY1 = RotationA * Eigen::Vector3f::UnitY();
	Eigen::Vector3f AxisZ1 = RotationA * Eigen::Vector3f::UnitZ();
	Eigen::Vector3f AxisX2 = RotationB * Eigen::Vector3f::UnitX();
	Eigen::Vector3f AxisY2 = RotationB * Eigen::Vector3f::UnitY();
	Eigen::Vector3f AxisZ2 = RotationB * Eigen::Vector3f::UnitZ();

	// Other A 축 검사
	if (Separated(Other, AxisX1))
		return false;
	if (Separated(Other, AxisY1))
		return false;
	if (Separated(Other, AxisZ1))
		return false;

	// Other B 축 검사
	if (Separated(Other, AxisX2))
		return false;
	if (Separated(Other, AxisY2))
		return false;
	if (Separated(Other, AxisZ2))
		return false;

	// A X B 축 검사
	if (Separated(Other, AxisX1.cross(AxisX2)))
		return false;
	if (Separated(Other, AxisX1.cross(AxisY2)))
		return false;
	if (Separated(Other, AxisX1.cross(AxisZ2)))
		return false;

	if (Separated(Other, AxisY1.cross(AxisX2)))
		return false;
	if (Separated(Other, AxisY1.cross(AxisY2)))
		return false;
	if (Separated(Other, AxisY1.cross(AxisZ2)))
		return false;

	if (Separated(Other, AxisZ1.cross(AxisX2)))
		return false;
	if (Separated(Other, AxisZ1.cross(AxisY2)))
		return false;
	if (Separated(Other, AxisZ1.cross(AxisZ2)))
		return false;

	return true;
}

// OBB를 AABB로 만들고
// 상대를 재 정렬된 축 기준으로 회전시킨다.
// 그리고 AABB - Sphere 충돌계산
bool OBB::Intersect(BoundingSphere& Other)
{
	auto MyWorldTranslation = LocalTransform.GetWorldTranslation();
	auto OtherWorldTranslation = Other.GetTransform().GetWorldTranslation();
	Eigen::Vector3f OtherTranslationRelativeToMine = OtherWorldTranslation - MyWorldTranslation;

	//  Eigen::Vector3f LocalSphereLocation = LocalTransform.GetWorldTransformData().inverse()* OtherWorldTranslation;
	//  Eigen::Vector3f Closest;
	//  Closest.x() = std::max(-Extent.x(), std::min(LocalSphereLocation.x(), Extent.x()));
	//  Closest.y() = std::max(-Extent.y(), std::min(LocalSphereLocation.y(), Extent.y()));
	//  Closest.z() = std::max(-Extent.z(), std::min(LocalSphereLocation.z(), Extent.z()));

	//  Eigen::Vector3f OtherToMineOffset = Closest - LocalSphereLocation;
	//  bool DoesIntersect;
	//  if((Closest - LocalSphereLocation).squaredNorm() < Other.GetRadius() * Other.GetRadius())
	//      DoesIntersect = true;
	//  else
	//      DoesIntersect = false;

	//  if(DoesIntersect)
	//  {
		  //CollisionPlaneNormalVector = OtherToMineOffset.normalized();
		  //CollisionPlaneLocation = Closest;
	//      CollisionPlaneNormalVector = LocalTransform.GetWorldTransformData().rotation() * CollisionPlaneNormalVector;
	//      CollisionPlaneLocation = LocalTransform.GetWorldTransformData() * CollisionPlaneLocation;

	//      IntersectedLength = OtherToMineOffset.norm() - Other.GetRadius();
	//      
	//      Eigen::Vector3f OtherCollisionNormal = -CollisionPlaneNormalVector;
	//      Other.SetLastIntersectedLength(IntersectedLength);
	//      Other.SetLastCollisionPlaneInfo(OtherCollisionNormal, Closest);
	//  }


	  // OBB를 축에 정렬된 상태인 AABB로 만든다.
	AABB ConvertedAABB;
	ConvertedAABB.SetExtent(Extent);
	ConvertedAABB.SetAlignedExtent(Extent);
	ConvertedAABB.SetIsMovable(IsMovable);
	Eigen::Quaternionf MyRotation = LocalTransform.GetWorldRotation();
	Eigen::Quaternionf InversedRotation = MyRotation.inverse();

	// 회전된 OBB에 따라 회전된 Sphere를 새로 만든다.
	BoundingSphere ConvertedSphere;
	ConvertedSphere.SetRadius(Other.GetRadius());
	ConvertedSphere.SetIsMovable(Other.GetIsMovable());
	Transform& NewSphereTransform = ConvertedSphere.GetTransform();
	//NewSphereTransform.SetRotation(InversedRotation);
	//NewSphereTransform.SetRotationWithDegrees(-MyRotationInDegrees.x(), -MyRotationInDegrees.y(), -MyRotationInDegrees.z());
	NewSphereTransform.Rotate(InversedRotation);
	NewSphereTransform.Translate(OtherTranslationRelativeToMine);


	// 충돌 검사 실행
	bool DoesIntersect = ConvertedAABB.Intersect(ConvertedSphere);

	if (DoesIntersect == true)
	{
		//////////////////////////////////////////////////////////

		// 나온 결과를 다시 회전시켜줘야 함.
		Eigen::Vector3f ConvertedCollisionPlaneNormalVector;
		Eigen::Vector3f ConvertedCollisionPlaneLocation;
		ConvertedAABB.GetLastCollisionPlaneInfo(ConvertedCollisionPlaneNormalVector, ConvertedCollisionPlaneLocation);
		IntersectedLength = ConvertedAABB.GetLastIntersectedLength();

		// 제 위치로 다시 회전, 이동
		Transform ResultPlaneNormalVectorTransform;
		ResultPlaneNormalVectorTransform.Rotate(MyRotation);
		ResultPlaneNormalVectorTransform.Translate(ConvertedCollisionPlaneNormalVector);
		Eigen::Vector3f ResultPlaneNormalVector = ResultPlaneNormalVectorTransform.GetWorldTranslation();
		//Eigen::Vector3f ResultPlaneNormalVector = MyRotation * ConvertedCollisionPlaneNormalVector;

		Transform ResultPlaneLocationTransform;
		ResultPlaneLocationTransform.Rotate(MyRotation);
		ResultPlaneLocationTransform.Translate(ConvertedCollisionPlaneLocation);
		Eigen::Vector3f ResultPlaneLocation = ResultPlaneLocationTransform.GetWorldTranslation();
		//Eigen::Vector3f ResultPlaneLocation = MyRotation * ConvertedCollisionPlaneLocation;

		ResultPlaneLocation += MyWorldTranslation;

		// OBB 결과 저장
		CollisionPlaneNormalVector = ResultPlaneNormalVector;
		CollisionPlaneLocation = ResultPlaneLocation;

		// Sphere 결과 저장
		ResultPlaneNormalVector *= -1.f;
		Other.SetLastCollisionPlaneInfo(ResultPlaneNormalVector, ResultPlaneLocation);
		Other.SetLastIntersectedLength(IntersectedLength);
	}

	return DoesIntersect;
}

bool OBB::Intersect(BoundingCapsule& Other)
{
	auto MyWorldTranslation = LocalTransform.GetWorldTranslation();
	auto OtherWorldTranslation = Other.GetTransform().GetWorldTranslation();
	Eigen::Vector3f OtherTranslationRelativeToMine = OtherWorldTranslation - MyWorldTranslation;

	AABB ConvertedAABB;
	ConvertedAABB.SetExtent(Extent);
	ConvertedAABB.SetAlignedExtent(Extent);
	ConvertedAABB.SetIsMovable(IsMovable);
	Eigen::Quaternionf MyRotation = LocalTransform.GetWorldRotation();
	Eigen::Quaternionf InversedRotation = MyRotation.inverse();

	// 회전된 OBB에 따라 회전된 Sphere를 새로 만든다.
	BoundingCapsule ConvertedCapsule;
	ConvertedCapsule.SetRadius(Other.GetRadius());
	ConvertedCapsule.SetHalfHeight(Other.GetHalfHeight());
	ConvertedCapsule.SetIsMovable(Other.GetIsMovable());
	Transform& NewSphereTransform = ConvertedCapsule.GetTransform();
	//NewSphereTransform.SetRotation(InversedRotation);
	//NewSphereTransform.SetRotationWithDegrees(-MyRotationInDegrees.x(), -MyRotationInDegrees.y(), -MyRotationInDegrees.z());
	NewSphereTransform.Rotate(InversedRotation);
	NewSphereTransform.Translate(OtherTranslationRelativeToMine);

	// 충돌 검사 실행
	bool DoesIntersect = ConvertedAABB.Intersect(ConvertedCapsule);

	if (DoesIntersect == true)
	{
		//////////////////////////////////////////////////////////

		// 나온 결과를 다시 회전시켜줘야 함.
		Eigen::Vector3f ConvertedCollisionPlaneNormalVector;
		Eigen::Vector3f ConvertedCollisionPlaneLocation;
		ConvertedAABB.GetLastCollisionPlaneInfo(ConvertedCollisionPlaneNormalVector, ConvertedCollisionPlaneLocation);
		IntersectedLength = ConvertedAABB.GetLastIntersectedLength();

		// 제 위치로 다시 회전, 이동
		Transform ResultPlaneNormalVectorTransform;
		ResultPlaneNormalVectorTransform.Rotate(MyRotation);
		ResultPlaneNormalVectorTransform.Translate(ConvertedCollisionPlaneNormalVector);
		Eigen::Vector3f ResultPlaneNormalVector = ResultPlaneNormalVectorTransform.GetWorldTranslation();
		//Eigen::Vector3f ResultPlaneNormalVector = MyRotation * ConvertedCollisionPlaneNormalVector;

		Transform ResultPlaneLocationTransform;
		ResultPlaneLocationTransform.Rotate(MyRotation);
		ResultPlaneLocationTransform.Translate(ConvertedCollisionPlaneLocation);
		Eigen::Vector3f ResultPlaneLocation = ResultPlaneLocationTransform.GetWorldTranslation();
		//Eigen::Vector3f ResultPlaneLocation = MyRotation * ConvertedCollisionPlaneLocation;

		ResultPlaneLocation += MyWorldTranslation;

		// OBB 결과 저장
		CollisionPlaneNormalVector = ResultPlaneNormalVector;
		CollisionPlaneLocation = ResultPlaneLocation;

		// Sphere 결과 저장
		ResultPlaneNormalVector *= -1.f;
		Other.SetLastCollisionPlaneInfo(ResultPlaneNormalVector, ResultPlaneLocation);
		Other.SetLastIntersectedLength(IntersectedLength);
	}

	return DoesIntersect;
}

bool OBB::Intersect(Ray& Other, float* ContactDist /*= nullptr*/, float RayRadius /*= 0.f*/)
{
	return Other.Intersect(*this, ContactDist, RayRadius);
}

void OBB::Resolve(float deltaTime)
{

}

TheShift::OBB::OBB() : BoundingVolume(), Extent(0.f, 0.f, 0.f)
{
	VolumeType = BoundingVolumeType::OBB;
}

Eigen::Vector3f TheShift::OBB::GetExtent() const
{
	return Extent;
}

void TheShift::OBB::SetExtent(const Eigen::Vector3f& NewExtent)
{
	Extent = NewExtent;
	CollisionRange = Extent.norm();
}

void TheShift::OBB::SetExtent(const TheShift::Vector3f& NewExtent)
{
	Extent.x() = NewExtent.X;
	Extent.y() = NewExtent.Y;
	Extent.z() = NewExtent.Z;
	CollisionRange = Extent.norm();
}

bool TheShift::OBB::Separated(OBB& Other, const Eigen::Vector3f& Axis)
{
	// 1. 모든 vertex를 axis에 대해서 투영한다
	// 2. box의 모든점의 투영점을 비교하여 minA maxA, minB maxB를 구한다.
	// 3. if minA < maxB or maxA > minB -> return true (이 axis에 대해선 겹쳐진것.
	float minA = std::numeric_limits<float>::max();
	float maxA = std::numeric_limits<float>::min();
	float minB = minA;
	float maxB = maxA;
	Eigen::Vector3f cornersA[8];
	GetCorners(cornersA);
	Eigen::Vector3f cornersB[8];
	Other.GetCorners(cornersB);

	for (int i = 0; i < 8; ++i) {
		float ProjA = cornersA[i].dot(Axis);
		if (minA > ProjA)
			minA = ProjA;
		if (maxA < ProjA)
			maxA = ProjA;

		float ProjB = cornersB[i].dot(Axis);
		if (minB > ProjB)
			minB = ProjB;
		if (maxB < ProjB)
			maxB = ProjB;
	}

	return std::max(maxA, maxB) - std::min(minA, minB) > (maxA - minA) + (maxB - minB);
}

void OBB::GetCorners(Eigen::Vector3f* Corners)
{
	Corners[0] = GetCorner(Eigen::Vector3f{ 1.f, 1.f, 1.f });
	Corners[1] = GetCorner(Eigen::Vector3f{ 1.f, 1.f, -1.f });
	Corners[2] = GetCorner(Eigen::Vector3f{ 1.f, -1.f, 1.f });
	Corners[3] = GetCorner(Eigen::Vector3f{ -1.f, 1.f, 1.f });
	Corners[4] = GetCorner(Eigen::Vector3f{ 1.f, -1.f, -1.f });
	Corners[5] = GetCorner(Eigen::Vector3f{ -1.f, 1.f, -1.f });
	Corners[6] = GetCorner(Eigen::Vector3f{ -1.f, -1.f, 1.f });
	Corners[7] = GetCorner(Eigen::Vector3f{ -1.f, -1.f, -1.f });
}

Eigen::Vector3f OBB::GetCorner(const Eigen::Vector3f& Dir)
{
	Eigen::Vector3f Center = GetTransform().GetTranslation();
	Eigen::Vector3f Corner;
	Eigen::Vector3f CalculatedExtent = GetTransform().GetWorldRotation() * Extent;
	Corner.x() = Center.x() + CalculatedExtent.x() * Dir.x();
	Corner.y() = Center.y() + CalculatedExtent.y() * Dir.y();
	Corner.z() = Center.z() + CalculatedExtent.z() * Dir.z();
	return Corner;
}

void TheShift::OBB::CalculateAlignedExtent()
{
	CalculatedAlignedExtent = AABB::CalculateExtent(Extent, LocalTransform.GetWorldRotation());
	ExtentUpdateNeeded = false;
}

AABB::AABB(Eigen::Vector3f& RelativeLocation, Eigen::Vector3f& RelativeRotation, Eigen::Vector3f& NewExtent) :
	BoundingVolume(RelativeLocation, RelativeRotation), Extent(NewExtent)
{
	VolumeType = BoundingVolumeType::AABB;
}

bool AABB::Intersect(BoundingVolume& Other)
{
	bool RetVal = false;
	CHECK_VOLUMETYPE_AND_DO_COLLIDE(RetVal, Other);
	return RetVal;
}

bool AABB::Intersect(AABB& other)
{
	Eigen::Vector3f Center = LocalTransform.GetWorldTranslation();
	const auto Distance = Center - other.GetTransform().GetWorldTranslation();
	const auto Length = CalculatedAlignedExtent + other.GetAlignedExtent();
	return std::abs(Distance.x()) <= Length.x() &&
		std::abs(Distance.y()) <= Length.y() &&
		std::abs(Distance.z()) <= Length.z();
}

bool AABB::Intersect(OBB& Other)
{
	return Other.Intersect(*this);
}

bool AABB::Intersect(BoundingSphere& Other)
{
	auto MyTranslation = LocalTransform.GetWorldTranslation();
	//auto CalculatedExtent = CalculateExtent(Extent, LocalTransform.GetWorldRotation());

	Eigen::Vector3f BoxMin = MyTranslation - CalculatedAlignedExtent;
	Eigen::Vector3f BoxMax = MyTranslation + CalculatedAlignedExtent;
	Eigen::Vector3f Closest;
	Eigen::Vector3f OtherTranslation = Other.GetTransform().GetWorldTranslation();

	// Intersect여부 체크
	float Radius = Other.GetRadius();
	if (OtherTranslation.x() < MyTranslation.x() - CalculatedAlignedExtent.x() - Radius)
		return false;
	if (OtherTranslation.x() > MyTranslation.x() + CalculatedAlignedExtent.x() + Radius)
		return false;
	if (OtherTranslation.y() < MyTranslation.y() - CalculatedAlignedExtent.y() - Radius)
		return false;
	if (OtherTranslation.y() > MyTranslation.y() + CalculatedAlignedExtent.y() + Radius)
		return false;
	if (OtherTranslation.z() < MyTranslation.z() - CalculatedAlignedExtent.z() - Radius)
		return false;
	if (OtherTranslation.z() > MyTranslation.z() + CalculatedAlignedExtent.z() + Radius)
		return false;

	bool DoesIntersect = true;

	// Calculate CollisionNormVector
	if (DoesIntersect)
	{
		// 어느 Volume을 기준으로 충돌면을 만들 것인지 먼저 정해야 함.
		// AABB - Sphere 충돌의 경우 Sphere translation의 축이 몇개가 AABB의 범위 안에 위치하는지로
		// 판별한다. 
		// 3개 모두 범위 안               - AABB 기준
		// 2개가 범위 안, 1개가 범위 밖   - AABB 기준
		// 1개가 범위 안, 2개가 범위 밖   - Sphere 기준
		// 3개 모두 범위 밖               - Sphere 기준

		int Count = 0;
		// Sphere의 위치가 AABB Range 안에 있는가?
		bool SphereAxisXInAABBRange = false;
		bool SphereAxisYInAABBRange = false;
		bool SphereAxisZInAABBRange = false;

		if (OtherTranslation.x() > MyTranslation.x() - CalculatedAlignedExtent.x() && OtherTranslation.x() < MyTranslation.x() + CalculatedAlignedExtent.x())
		{
			++Count;
			SphereAxisXInAABBRange = true;
		}
		if (OtherTranslation.y() > MyTranslation.y() - CalculatedAlignedExtent.y() && OtherTranslation.y() < MyTranslation.y() + CalculatedAlignedExtent.y())
		{
			++Count;
			SphereAxisYInAABBRange = true;
		}
		if (OtherTranslation.z() > MyTranslation.z() - CalculatedAlignedExtent.z() && OtherTranslation.z() < MyTranslation.z() + CalculatedAlignedExtent.z())
		{
			++Count;
			SphereAxisZInAABBRange = true;
		}

		if (Count == 1)
			int a = 0;

		// OnAABB가 true이면 충돌면이 AABB기준임.
		bool OnAABB = false;

		// 2개보다 적은 수의 축이 범위 안이라면 Sphere 기준
		// 0개면 꼭지점 충돌
		// 1개면 모서리 충돌
		if (Count < 2)
			OnAABB = false;

		// 2개 이상의 축이 범위 안이라면 AABB 기준
		else
		{
			OnAABB = true;
			//// Sphere의 상대적 위치에서 가장 큰 원소의 축에 투영시킨 vector가 AABB의 충돌면의 normal vector임

			//CollisionPlaneNormalVector.x() = 1.f;
			//CollisionPlaneNormalVector.y() = 1.f;
			//CollisionPlaneNormalVector.z() = 1.f;
			//
			//// 가장 값이 큰 축만 값을 남긴다.
			//// AABB는 이미 축에 정렬된 상태이므로 그 값이 충돌면의 Normal vector가 됨.
			//if(std::abs(OtherTranslationRelativeToMine.x()) > std::abs(OtherTranslationRelativeToMine.y()))
			//    CollisionPlaneNormalVector.y() = 0.f;
			//else
			//    CollisionPlaneNormalVector.x() = 0.f;
			//if(std::abs(OtherTranslationRelativeToMine.x()) > std::abs(OtherTranslationRelativeToMine.z()))
			//    CollisionPlaneNormalVector.z() = 0.f;
			//else
			//    CollisionPlaneNormalVector.x() = 0.f;
			//if(std::abs(OtherTranslationRelativeToMine.y()) > std::abs(OtherTranslationRelativeToMine.z()))
			//    CollisionPlaneNormalVector.z() = 0.f;
			//else
			//    CollisionPlaneNormalVector.y() = 0.f;

			//if(CollisionPlaneNormalVector.x() > FLT_EPSILON)
			//{
			//    // 음수면 부호를 바꿔준다.
			//    if(OtherTranslationRelativeToMine.x() < -FLT_EPSILON)
			//        CollisionPlaneNormalVector.x() = -1.f;
			//}
			//else if(CollisionPlaneNormalVector.y() > FLT_EPSILON)
			//{
			//    // 음수면 부호를 바꿔준다.
			//    if(OtherTranslationRelativeToMine.y() < -FLT_EPSILON)
			//        CollisionPlaneNormalVector.y() = -1.f;
			//}
			//else if(CollisionPlaneNormalVector.z() > FLT_EPSILON)
			//{
			//    // 음수면 부호를 바꿔준다.
			//    if(OtherTranslationRelativeToMine.z() < -FLT_EPSILON)
			//        CollisionPlaneNormalVector.z() = -1.f;
			//}
		}

		// Sphere의 Relative Translation
		Eigen::Vector3f OtherTranslationRelativeToMine = (OtherTranslation - MyTranslation);
		Eigen::Vector3f ClosestExtent = CalculatedAlignedExtent;
		Eigen::Vector3f ContactPoint = Eigen::Vector3f::Zero();

		if (OtherTranslationRelativeToMine.x() < -FLT_EPSILON)
			ClosestExtent.x() *= -1.f;
		if (OtherTranslationRelativeToMine.y() < -FLT_EPSILON)
			ClosestExtent.y() *= -1.f;
		if (OtherTranslationRelativeToMine.z() < -FLT_EPSILON)
			ClosestExtent.z() *= -1.f;

		ContactPoint = ClosestExtent;

		// 맞닿은 점 찾기
		// AABB 범위 안에 있다면 Sphere 좌표로 바꿔줌.
		if (SphereAxisXInAABBRange)
			ContactPoint.x() = OtherTranslationRelativeToMine.x();
		if (SphereAxisYInAABBRange)
			ContactPoint.y() = OtherTranslationRelativeToMine.y();
		if (SphereAxisZInAABBRange)
			ContactPoint.z() = OtherTranslationRelativeToMine.z();

		// 충돌면의 법선벡터
		// 이 Volume과 충돌한 면의 법선벡터를 구한다.
		Eigen::Vector3f Temp = (MyTranslation + ContactPoint) - OtherTranslation;
		Temp.normalize();
		CollisionPlaneNormalVector = Temp;

		// Static       vs      Static      : 아무것도 안함
		// Static       vs      Dynamic     : Static은 움직이지 않고 Dynamic이 겹친만큼 이동시켜줌.
		// Dynamic      vs      Dynamic     : 겹친만큼의 반만큼 동일하게 이동시켜줌.

		// IsMovable이 true면 Dynamic, false면 Static

		bool OtherIsMovable = Other.GetIsMovable();

		Eigen::Vector3f ContactPointToOther = (OtherTranslationRelativeToMine - ContactPoint);
		IntersectedLength = Other.GetRadius() - ContactPointToOther.norm();
		if (IntersectedLength < 0.f)
			return false;

		// Dynamic vs Dynamic
		if (IsMovable == true && OtherIsMovable == true)
		{
			// 중간지점
			CollisionPlaneLocation = MyTranslation + ContactPoint + CollisionPlaneNormalVector * (IntersectedLength * 0.5f);
		}

		// Dynamic vs Static
		else if (IsMovable == true && OtherIsMovable == false)
		{
			// Other의 접점
			CollisionPlaneLocation = MyTranslation + ContactPoint + CollisionPlaneNormalVector * IntersectedLength;
		}

		// Static vs Dynamic
		else if (IsMovable == false && OtherIsMovable == true)
		{
			// this의 접점
			// OtherTranslation - MyTranslation과 this의 교차점을 찾는다. (길이를 OtherRadius로 하면 됨)
			CollisionPlaneLocation = MyTranslation + ContactPoint;
		}

		// Static vs Static
		else if (IsMovable == false && OtherIsMovable == false)
		{
			// Do nothing
		}

		Eigen::Vector3f OtherCollisionPlaneNormalVector = -CollisionPlaneNormalVector;
		Other.SetLastIntersectedLength(IntersectedLength);
		Other.SetLastCollisionPlaneInfo(OtherCollisionPlaneNormalVector, CollisionPlaneLocation);
	}

	return DoesIntersect;
}

bool AABB::Intersect(BoundingCapsule& Other)
{
	Eigen::Quaternionf Rotation = Other.GetTransform().GetWorldRotation();
	Transform RayTransform;
	RayTransform.Rotate(Rotation);
	RayTransform.Translate(0.f, 0.f, 1.f);
	Eigen::Vector3f Ray = RayTransform.GetWorldTranslation();
	float Radius = Other.GetRadius();
	float HalfHeight = Other.GetHalfHeight();
	Eigen::Vector3f OtherLocation = Other.GetTransform().GetWorldTranslation();
	Eigen::Vector3f StartPoint = OtherLocation - Ray * (HalfHeight - Radius);
	Eigen::Vector3f EndPoint = OtherLocation + Ray * (HalfHeight - Radius);

	float Dist;
	if (Intersect(Ray, StartPoint, EndPoint, Dist, Radius))
	{
		if (Dist > (HalfHeight - Radius) * 2.f)
			return false;
		Eigen::Vector3f SphereLocation(StartPoint + (Ray * Dist));

		BoundingSphere NewSphere;
		NewSphere.SetRadius(Radius);
		NewSphere.GetTransform().Translate(SphereLocation);

		bool ReturnValue = Intersect(NewSphere);
		if (ReturnValue == true)
		{
			Eigen::Vector3f OtherCollisionNormal;
			Eigen::Vector3f OtherCollisionLocation;
			NewSphere.GetLastCollisionPlaneInfo(OtherCollisionNormal, OtherCollisionLocation);
			Other.SetLastCollisionPlaneInfo(OtherCollisionNormal, OtherCollisionLocation);
			Other.SetLastIntersectedLength(IntersectedLength);
			return true;
		}
	}

	return false;
}

bool AABB::Intersect(Eigen::Vector3f& Ray, Eigen::Vector3f& StartPoint, Eigen::Vector3f& EndPoint, float& Dist, float Radius)
{
	Eigen::Vector3f BoxLocation = LocalTransform.GetWorldTranslation();
	Eigen::Vector3f Min = BoxLocation - GetAlignedExtent() - Eigen::Vector3f(Radius, Radius, Radius);
	Eigen::Vector3f Max = BoxLocation + GetAlignedExtent() + Eigen::Vector3f(Radius, Radius, Radius);

	if (StartPoint.x() > Min.x() && StartPoint.x() < Max.x())
		if (StartPoint.y() > Min.y() && StartPoint.y() < Max.y())
			if (StartPoint.z() > Min.z() && StartPoint.z() < Max.z())
			{
				Dist = 0.f;
				return true;
			}

	if (EndPoint.x() > Min.x() && EndPoint.x() < Max.x())
		if (EndPoint.y() > Min.y() && EndPoint.y() < Max.y())
			if (EndPoint.z() > Min.z() && EndPoint.z() < Max.z())
			{
				Dist = (EndPoint - StartPoint).norm();
				return true;
			}

	//   if(Contains(StartPoint))
	//   {
	//       Dist = 0.f;
	//       return true;
	//   }
	//   
	//   if(Contains(EndPoint))
	//   {
	//       Dist = (EndPoint - StartPoint).norm();
	//       return true;
	//   }

	Eigen::Vector3f NormalizedRay(Ray.normalized());

	// r.dir is unit direction vector of ray
	Eigen::Vector3f DirFrac;
	DirFrac.x() = 1.f / NormalizedRay.x();
	DirFrac.y() = 1.f / NormalizedRay.y();
	DirFrac.z() = 1.f / NormalizedRay.z();

	// lb is the corner of AABB with minimal coordinates - left bottom, rt is maximal corner
	// r.org is origin of ray
	float t1 = (Min.x() - StartPoint.x()) * DirFrac.x();
	float t2 = (Max.x() - StartPoint.x()) * DirFrac.x();
	float t3 = (Min.y() - StartPoint.y()) * DirFrac.y();
	float t4 = (Max.y() - StartPoint.y()) * DirFrac.y();
	float t5 = (Min.z() - StartPoint.z()) * DirFrac.z();
	float t6 = (Max.z() - StartPoint.z()) * DirFrac.z();

	float tmin = std::max(std::max(std::min(t1, t2), std::min(t3, t4)), std::min(t5, t6));
	float tmax = std::min(std::min(std::max(t1, t2), std::max(t3, t4)), std::max(t5, t6));

	// if tmax < 0, ray (line) is intersecting AABB, but the whole AABB is behind us
	if (tmax < 0)
	{
		Dist = tmax;
		return false;
	}

	// if tmin > tmax, ray doesn't intersect AABB
	if (tmin > tmax)
	{
		Dist = tmax;
		return false;
	}

	if (tmin < 0)
	{
		Dist = tmax;
	}
	else
	{
		Dist = tmin;
	}
	return true;
}

bool AABB::Intersect(Ray& Other, float* ContactDist /*= nullptr*/, float RayRadius /*= 0.f*/)
{
	return Other.Intersect(*this, ContactDist, RayRadius);
}

void AABB::Resolve(float deltaTime)
{

}

// Point가 AABB안에 위치하고 있는지 여부를 return한다.
bool AABB::Contains(Eigen::Vector3f& Point)
{
	Eigen::Vector3f BoxLocation = LocalTransform.GetWorldTranslation();
	Eigen::Vector3f Min = BoxLocation - GetAlignedExtent();
	Eigen::Vector3f Max = BoxLocation + GetAlignedExtent();

	if (Point.x() < Min.x() || Point.x() > Max.x())
		return false;
	if (Point.y() < Min.y() || Point.y() > Max.y())
		return false;
	if (Point.z() < Min.z() || Point.z() > Max.z())
		return false;

	return true;
}

void AABB::SetAlignedExtent(const Eigen::Vector3f& NewExtent)
{
	CalculatedAlignedExtent = NewExtent;
}

void AABB::SetAlignedExtent(const TheShift::Vector3f& NewExtent)
{
	CalculatedAlignedExtent.x() = NewExtent.X;
	CalculatedAlignedExtent.y() = NewExtent.Y;
	CalculatedAlignedExtent.z() = NewExtent.Z;
}

TheShift::AABB::AABB() : BoundingVolume(), Extent(0.f, 0.f, 0.f)
{
	VolumeType = BoundingVolumeType::AABB;
}

// Transform에 의해 변환된 Extent를 축에 재정렬 시킨다.
// World Transform을 가지고 변환된 Extent를 return한다.
Eigen::Vector3f TheShift::AABB::CalculateExtent(Eigen::Vector3f& Extent, const Eigen::Quaternionf& WorldRotation)
{
	Eigen::Vector3f Extent1(Extent.x(), Extent.y(), Extent.z());
	Eigen::Vector3f RotatedExtent1 = WorldRotation * Extent1;
	Eigen::Vector3f Extent2(-Extent.x(), Extent.y(), Extent.z());
	Eigen::Vector3f RotatedExtent2 = WorldRotation * Extent2;
	Eigen::Vector3f Extent3(Extent.x(), -Extent.y(), Extent.z());
	Eigen::Vector3f RotatedExtent3 = WorldRotation * Extent3;
	Eigen::Vector3f Extent4(Extent.x(), Extent.y(), -Extent.z());
	Eigen::Vector3f RotatedExtent4 = WorldRotation * Extent4;
	Eigen::Vector3f Extent5(-Extent.x(), -Extent.y(), Extent.z());
	Eigen::Vector3f RotatedExtent5 = WorldRotation * Extent5;
	Eigen::Vector3f Extent6(Extent.x(), -Extent.y(), -Extent.z());
	Eigen::Vector3f RotatedExtent6 = WorldRotation * Extent6;
	Eigen::Vector3f Extent7(-Extent.x(), Extent.y(), -Extent.z());
	Eigen::Vector3f RotatedExtent7 = WorldRotation * Extent7;
	Eigen::Vector3f Extent8(-Extent.x(), -Extent.y(), -Extent.z());
	Eigen::Vector3f RotatedExtent8 = WorldRotation * Extent8;

	Eigen::Vector3f CalculatedExtent = RotatedExtent1;
	if (CalculatedExtent.x() < RotatedExtent2.x())
		CalculatedExtent.x() = RotatedExtent2.x();
	if (CalculatedExtent.x() < RotatedExtent3.x())
		CalculatedExtent.x() = RotatedExtent3.x();
	if (CalculatedExtent.x() < RotatedExtent4.x())
		CalculatedExtent.x() = RotatedExtent4.x();
	if (CalculatedExtent.x() < RotatedExtent5.x())
		CalculatedExtent.x() = RotatedExtent5.x();
	if (CalculatedExtent.x() < RotatedExtent6.x())
		CalculatedExtent.x() = RotatedExtent6.x();
	if (CalculatedExtent.x() < RotatedExtent7.x())
		CalculatedExtent.x() = RotatedExtent7.x();
	if (CalculatedExtent.x() < RotatedExtent8.x())
		CalculatedExtent.x() = RotatedExtent8.x();

	if (CalculatedExtent.y() < RotatedExtent2.y())
		CalculatedExtent.y() = RotatedExtent2.y();
	if (CalculatedExtent.y() < RotatedExtent3.y())
		CalculatedExtent.y() = RotatedExtent3.y();
	if (CalculatedExtent.y() < RotatedExtent4.y())
		CalculatedExtent.y() = RotatedExtent4.y();
	if (CalculatedExtent.y() < RotatedExtent5.y())
		CalculatedExtent.y() = RotatedExtent5.y();
	if (CalculatedExtent.y() < RotatedExtent6.y())
		CalculatedExtent.y() = RotatedExtent6.y();
	if (CalculatedExtent.y() < RotatedExtent7.y())
		CalculatedExtent.y() = RotatedExtent7.y();
	if (CalculatedExtent.y() < RotatedExtent8.y())
		CalculatedExtent.y() = RotatedExtent8.y();

	if (CalculatedExtent.z() < RotatedExtent2.z())
		CalculatedExtent.z() = RotatedExtent2.z();
	if (CalculatedExtent.z() < RotatedExtent3.z())
		CalculatedExtent.z() = RotatedExtent3.z();
	if (CalculatedExtent.z() < RotatedExtent4.z())
		CalculatedExtent.z() = RotatedExtent4.z();
	if (CalculatedExtent.z() < RotatedExtent5.z())
		CalculatedExtent.z() = RotatedExtent5.z();
	if (CalculatedExtent.z() < RotatedExtent6.z())
		CalculatedExtent.z() = RotatedExtent6.z();
	if (CalculatedExtent.z() < RotatedExtent7.z())
		CalculatedExtent.z() = RotatedExtent7.z();
	if (CalculatedExtent.z() < RotatedExtent8.z())
		CalculatedExtent.z() = RotatedExtent8.z();

	return CalculatedExtent;
}

Eigen::Vector3f TheShift::AABB::GetExtent() const
{
	return Extent;
}

void TheShift::AABB::SetExtent(const Eigen::Vector3f& NewExtent)
{
	Extent = NewExtent;
	CollisionRange = Extent.norm();
}

void TheShift::AABB::SetExtent(const TheShift::Vector3f& NewExtent)
{
	Extent.x() = NewExtent.X;
	Extent.y() = NewExtent.Y;
	Extent.z() = NewExtent.Z;
	CollisionRange = Extent.norm();
}

void TheShift::AABB::CalculateAlignedExtent()
{
	CalculatedAlignedExtent = CalculateExtent(Extent, LocalTransform.GetWorldRotation());
	ExtentUpdateNeeded = false;
}

BoundingCapsule::BoundingCapsule(Eigen::Vector3f& RelativeLocation, Eigen::Vector3f& RelativeRotation, float NewHalfHeight, float NewRadius) :
	BoundingVolume(RelativeLocation, RelativeRotation), HalfHeight(NewHalfHeight), Radius(NewRadius)
{
	VolumeType = BoundingVolumeType::Capsule;
}

//BoundingCapsule::BoundingCapsule(BoundingVolumeVertexInfo& MidAndExtent) :
//    BoundingVolume(MidAndExtent)
//{
//    VolumeType = BoundingVolumeType::Capsule;
//}

bool BoundingCapsule::Intersect(BoundingVolume& Other)
{
	bool RetVal = false;
	CHECK_VOLUMETYPE_AND_DO_COLLIDE(RetVal, Other);
	return RetVal;
}

////////////////////////////////////////////
// Bounding Capsule은 충돌처리 무시한다.
// Sphere들로 분할해서 충돌체크를 진행한다.
bool BoundingCapsule::Intersect(AABB& Other)
{
	return Other.Intersect(*this);
}

bool BoundingCapsule::Intersect(OBB& Other)
{
	return Other.Intersect(*this);
}

bool BoundingCapsule::Intersect(BoundingSphere& Other)
{
	return false;
}

bool BoundingCapsule::Intersect(BoundingCapsule& Other)
{
	return false;
}

bool BoundingCapsule::Intersect(Ray& Other, float* ContactDist /*= nullptr*/, float RayRadius /*= 0.f*/)
{
	return Other.Intersect(*this, ContactDist, RayRadius);
}

void BoundingCapsule::Resolve(float deltaTime)
{
	// Capulse이 가지고 있는 sphere들을 한꺼번에 검사한 후 가장 짧은 거리의 충돌을 처리한다.
	// Sphere와 비슷한 형식으로 처리
	std::vector<UID> ResolvedVolumeIds;
	bool Moved = false;
	Eigen::Vector3f CurrentLocation = LocalTransform.GetWorldTranslation();
	Eigen::Vector3f LastLocation = LastTranslation;
	Eigen::Vector3f MoveVector = CurrentLocation - LastLocation;
	Eigen::Vector3f MoveDirection = (MoveVector).normalized();
	Eigen::Vector3f PrevNormal;
	float MoveLength = MoveVector.norm();
	UID ClosestContactVolumeId = INVALID_UID;
	UID PrevContactVolumeId = INVALID_UID;

	// 무한루프, penetration 방지
	int Count = 0;
	bool EndLoop = false;
	bool FirstContact = false;
	Eigen::Vector3f FirstContactPoint;
	
	while (true)
	{
		// 무한루프 방지
		if (Count > 10)
		{
			LOG_INFO("Too many loops %f", Count);
			EndLoop = true;
			//////////////////////////////////
			CurrentLocation = FirstContactPoint;
			LastLocation = FirstContactPoint;
			//////////////////////////////////	
		}
		++Count;
		bool MultipleCollision = false;
		bool Collided = false;
		std::shared_ptr<BoundingSphere> ClosestSphere = nullptr;
		float ClosestContactDist = FLT_MAX;
		Eigen::Vector3f ClosestContactLocation;
		Eigen::Vector3f ClosestContactNormal;

		for (auto Sphere : Spheres)
		{
			Eigen::Vector3f SphereRelativeLocation = Sphere->GetTransform().GetTranslation();
			// Sphere들 중 가장 가까이에서 충돌한 sphere를 찾는다

			//TODO: 이 변수들은 Sphere에 특정되도록 만들어야 함.
			// 이전 위치에서 현재 위치로의 Ray
			Ray LastToCurrentRay;
			LastToCurrentRay.StartPoint = LastLocation + SphereRelativeLocation;
			//LastToCurrentRay.StartPoint = Sphere->GetLastTranslation();
			LastToCurrentRay.Direction = (CurrentLocation + SphereRelativeLocation - LastToCurrentRay.StartPoint).normalized();
			LastToCurrentRay.SetLength(MoveLength);
			//if(LastToCurrentRay.GetLength() < FLT_EPSILON)
			//	break;

			//TODO: 2. 계속해서 충돌하고 있을때

			// 1. 이전위치에서 다음 위치로 Ray로 CloseVolumes 모두 충돌체크 해보기
			for (auto Iter = CloseVolumes.begin(), Next = Iter; Next != CloseVolumes.end(); Iter = Next)
			{
				++Next;
				//LOG_INFO("Volume Num: %d", CloseVolumes.size());
				// 이전에 충돌처리했던 Id와는 바로 다시 검사하지 않는다.
				//if(std::find(ResolvedVolumeIds.begin(), ResolvedVolumeIds.end(), Id) != ResolvedVolumeIds.end())
				//	continue;
				if (*Iter == PrevContactVolumeId)
					continue;

				auto OtherVolume = Resolver->GetBoundingVolume(*Iter);
				if (OtherVolume == nullptr || OtherVolume->GetParentActor() == nullptr)
				{
					if (CloseVolumes.find(*Iter) != CloseVolumes.end())
						CloseVolumes.erase(Iter);
					continue;
				}

				// 충돌 지점까지의 거리
				float ContactDist;
				//TODO: 충돌 안했어도 어느정도 오차범위는 확인해야함.
				if (OtherVolume->Intersect(LastToCurrentRay, &ContactDist, Radius) == false)
					continue;

				//if(Id != PrevContactVolumeId)
				if (PrevContactVolumeId != INVALID_UID)
					MultipleCollision = true;

				//// StartPoint가 충돌할 경우 무시한다...?
				//if(ContactDist < FLT_EPSILON)
				//	continue;

				//LOG_INFO("%f", ContactDist);

				Collided = true;

				// 지금까지 최단 충돌거리보다 짧을 경우 ClosestContactVolumeId와 ClosestContactDist를 갱신시킨다.
				if (ContactDist < ClosestContactDist)
				{
					ClosestSphere = Sphere;
					ClosestContactVolumeId = *Iter;
					ClosestContactDist = ContactDist;
					//ContactLocation = LastLocation + LastToCurrentRay.Direction.normalized() * ContactDist;
					ClosestContactLocation = LastToCurrentRay.GetCollisionPoint();
					//LOG_INFO("Contact Location: %f %f %f", ClosestContactLocation.x(), ClosestContactLocation.y(), ClosestContactLocation.z());
					ClosestContactNormal = LastToCurrentRay.GetCollisionNormal();
					// 만약 Top이나 Bottom sphere가 아니라면 normal의 z성분은 0
					// Top이나 Bottom도 CollisionContactLocation위치에 따라 z성분을 지울지 말지 정해야 함
					//TODO: Capsule이 회전한다면 normal을 그냥 이렇게 변경하면 안될듯.
					// StartPoint가 Bottom인데 CollisionNormal의 z성분이 +
					// StartPoint가 Top인데 CollisionNormal의 z성분이 -
					// 가 아니라면 normal의 z성분은 모두 0으로 통일
					if (Sphere->IsTopSphereOfCapsule())
					{
						if (ClosestContactNormal.z() > -FLT_EPSILON)
						{
							ClosestContactNormal.z() = -FLT_EPSILON;
							ClosestContactNormal.normalize();
						}
					}
					else if (Sphere->IsBottomSphereOfCapsule())
					{
						if (ClosestContactNormal.z() < FLT_EPSILON)
						{
							ClosestContactNormal.z() = FLT_EPSILON;
							ClosestContactNormal.normalize();
						}
					}
					else
					{
						ClosestContactNormal.z() = 0.f;
						ClosestContactNormal.normalize();
					}

					//TEMP: Capsule끼리 충돌할 경우 z축 성분은 무시한다. (몬스터들끼리 위로 올라 타는 버그 방지용 임시방편)
					if(Resolver->GetBoundingVolume(*Iter)->GetVolumeType() == BoundingVolumeType::Capsule)
					{
						ClosestContactNormal.z() = 0.f;
						ClosestContactNormal.normalize();
					}
				}
			}
		}
		// 더이상 충돌이 없다면 종료한다.
		if (!Collided)
		{
			// 1. Ray (CurrentLocation -> Vector(0 0 -1) * 오차범위) 와 Actor에서 가져온 Platform과 충돌체크
			Ray PlatformCheckRay;
			//TODO: Bottom sphere의 transform으로 위치 조정
			// Ray의 시작 위치를 bottom sphere의 위치로 한다.
			PlatformCheckRay.StartPoint = /*LastLocation*/CurrentLocation + Eigen::Vector3f(0.f, 0.f, -1.f) * (HalfHeight - Radius);
			PlatformCheckRay.Direction = -Eigen::Vector3f::UnitZ();
			auto Parent = GetParentActor();
			auto LastPlatform = Parent->GetStandingPlatform();
			float PlatformDist;

			if (LastPlatform == nullptr)
			{
				auto Parent = GetParentActor();
				Parent->GetMovementComp()->SetGravity(true);
				Parent->SetActorStandingOnPlatform(false);
				Eigen::Vector3f InvalidNormal(0.f, 0.f, 0.f);
				Parent->SetPlatformNormal(InvalidNormal);
				Parent->SetStandingPlatform(nullptr);
			}

			// 이전 platform과 충돌검사
			else if (LastPlatform->Intersect(PlatformCheckRay, &PlatformDist, Radius))
			{
				// 오차범위 이내로 충돌한다면 오차범위를 줄여주고 Standing을 true로 set
				if (PlatformDist < StandingOnPlatformDistErrorRange)
				{
					// 오차범위만큼 재조정
					if (PlatformDist < 0.f)
					{
						CurrentLocation += PlatformCheckRay.Direction * (PlatformDist * 1.5f);
						//Moved = true;
					}
					//GetParentActor()->GetTransform().AddTranslation(CurrentLocation - LocalTransform.GetWorldTranslation());
					//break;
				}
				else
				{
					auto Parent = GetParentActor();
					Parent->GetMovementComp()->SetGravity(true);
					Parent->SetActorStandingOnPlatform(false);
					Eigen::Vector3f InvalidNormal(0.f, 0.f, 0.f);
					Parent->SetPlatformNormal(InvalidNormal);
					Parent->SetStandingPlatform(nullptr);
				}
			}
			else
			{
				auto Parent = GetParentActor();
				Parent->GetMovementComp()->SetGravity(true);
				Parent->SetActorStandingOnPlatform(false);
				Eigen::Vector3f InvalidNormal(0.f, 0.f, 0.f);
				Parent->SetPlatformNormal(InvalidNormal);
				Parent->SetStandingPlatform(nullptr);
			}

			// 종료하기 전에 변경된 위치는 적용시킨다.
			if (Moved)
			{
				//GetParentActor()->GetTransform().AddTranslation(CurrentLocation - LocalTransform.GetWorldTranslation());
				GetParentActor()->GetTransform().SetTranslation(CurrentLocation - Eigen::Vector3f(0.f, 0.f, HalfHeight));
			}
			break;
		}
		else
		{
			// 이번이 최초의 충돌이라면 저장해둔다. (무한루프에 빠질 가능성이 있을 때 이동시키지 않고 제일 처음 충돌한 지점으로 이동시켜놓는다.
			if(!FirstContact)
			{
				FirstContactPoint = ClosestContactLocation - ClosestSphere->GetTransform().GetTranslation() + ClosestContactNormal * 0.01f;;
				FirstContact = true;
			}
		}
		// 2. 가장 가까운 위치에서 충돌한 Volume 찾기


		// 4. SlidingVector 계산
		// (1) SlidingVector 구하는 함수 만들기
		MoveLength -= ClosestContactDist;
		//if(MoveLength < 0.f)
		//	MoveLength = 0.f;

		//TODO: Platform위에 있음을 체크할 actor들이 아니라면 체크하지 않음.
		// Parent가 nullptr일때가 있음.
		Eigen::Vector3f SlidingVector;
		////TODO: Normal로 충돌면의 각도를 얻고 -30 ~ 30도라면 MoveDirection을 그만큼 회전시켜준다.
		float FloorAngle = std::acosf(ClosestContactNormal.z()) * 180.f / 3.141592;
		if (FloorAngle < 40.f)
		{
			if (!GetParentActor()->IsStandingOnPlatform())
			{
				auto Parent = GetParentActor();
				Parent->SetActorStandingOnPlatform(true);
				Parent->SetPlatformNormal(ClosestContactNormal);
				Parent->SetStandingPlatform(Resolver->GetBoundingVolume(ClosestContactVolumeId));

				// 중력을 끈다
				GetParentActor()->GetMovementComp()->SetGravity(false);
			}

			SlidingVector = GetSlidingVector(MoveDirection * (MoveLength/* - ClosestContactDist*/), ClosestContactNormal);
			LastLocation = ClosestContactLocation - ClosestSphere->GetTransform().GetTranslation() + ClosestContactNormal * 0.01f;
			CurrentLocation = LastLocation + SlidingVector;
			PrevNormal = ClosestContactNormal;
			PrevContactVolumeId = ClosestContactVolumeId;
			Moved = true;
			continue;

			//Eigen::Vector3f TempMoveDirection = Transform::RotateVector(MoveDirection, Transform::GetFromToRotation(Eigen::Vector3f(0.f,0.f,1.f), ClosestContactNormal));
			//LOG_INFO("Normal: %f %f %f", ClosestContactNormal.x(), ClosestContactNormal.y(), ClosestContactNormal.z());
			//LOG_INFO("Before: %f %f %f", MoveDirection.x(), MoveDirection.y(), MoveDirection.z());
			//LOG_INFO("After: %f %f %f", TempMoveDirection.x(), TempMoveDirection.y(), TempMoveDirection.z());
			//SlidingVector = GetSlidingVector(TempMoveDirection * (MoveLength - ClosestContactDist), ClosestContactNormal);
		}

		if(!EndLoop)
		{
			SlidingVector = GetSlidingVector(MoveDirection * (MoveLength/* - ClosestContactDist*/), ClosestContactNormal);
		}
		else
		{
			SlidingVector.setZero();
		}

		//LOG_INFO("Normal vector: %f %f %f", ClosestContactNormal.x(), ClosestContactNormal.y(), ClosestContactNormal.z());
		//LOG_INFO("Contact Dist: %f", ClosestContactDist);
		//LOG_INFO("Sliding Vector: %f %f %f", SlidingVector.x(), SlidingVector.y(), SlidingVector.z());

		bool NoMovement = false;
		// SlidingVector가 0, 0, 0 일 경우엔 멈춘 상태이므로 남은 시간만큼 계산을 해줄 필요가 없다.
		if (!SlidingVector.isZero())
		{
			// 5. 해당 위치까지 걸린 deltaTime 빼주기
			//float VelocityMag = GetParentActor()->GetMovementComp()->GetCurrentVelocity().norm()/* - 0.1f*/;
			//float TimePassed = ClosestContactDist / VelocityMag;
			//if(TimePassed < 0.00016)
			//	NoMovement = true;
			//LOG_INFO("TimePassed: %f DeltaTime: %f", TimePassed, DeltaTime);
		}
		else
		{
			NoMovement = true;
		}

		// 3. 해당 위치로 이동
		LastLocation = ClosestContactLocation - ClosestSphere->GetTransform().GetTranslation() + ClosestContactNormal;
		if (MultipleCollision)
		{
			Eigen::Vector3f NewDir = PrevNormal.cross(ClosestContactNormal).normalized();
			float Dist = (CurrentLocation - LastLocation).dot(NewDir);
			Eigen::Vector3f Offset = NewDir * Dist;
			CurrentLocation = LastLocation + Offset + NewDir;
			//LOG_INFO("%f %f %f", Offset.x(), Offset.y(), Offset.z());
		}
		else
		{
			// 6. 남은 deltaTime만큼 sliding vector로 이동
			CurrentLocation = LastLocation + SlidingVector;
		}
		PrevNormal = ClosestContactNormal;
		PrevContactVolumeId = ClosestContactVolumeId;


		// 움직였음을 체크해준다.
		// 지금 충돌한 Volume을 제외한 다른 Volume들과 충돌이 없을 때 종료하기 전에 이동시켜주기 위함.
		Moved = true;

		// 7. 다시 Resolve(남은 deltaTime)

		// 탈출 조건
		// 1. DeltaTime이 -일때
		if (/*DeltaTime < 0.f || */NoMovement)
		{
			//CurrentLocation = ClosestContactLocation;

			// Actor를 이동시켜주고 loop를 종류한다.
			if (Collided)
				GetParentActor()->GetTransform().AddTranslation(CurrentLocation - LocalTransform.GetWorldTranslation());
			//GetParentActor()->Translate(CurrentLocation - LocalTransform.GetWorldTranslation());
			Eigen::Vector3f ResolvedLocation = LocalTransform.GetWorldTranslation();
			//LOG_INFO("Resolved location: %f %f %f", ResolvedLocation.x(), ResolvedLocation.y(), ResolvedLocation.z());
			break;
		}
	}
}

float BoundingCapsule::GetRadius() const
{
	return Radius;
}

float BoundingCapsule::GetHalfHeight() const
{
	return HalfHeight;
}

TheShift::BoundingCapsule::BoundingCapsule() : BoundingVolume(), HalfHeight(0), Radius(0)
{
	VolumeType = BoundingVolumeType::Capsule;
}

void TheShift::BoundingCapsule::SetRadius(float NewRadius)
{
	Radius = NewRadius;
}

void TheShift::BoundingCapsule::SetHalfHeight(float NewHalfHeight)
{
	HalfHeight = NewHalfHeight;
	CollisionRange = HalfHeight;
}

// 이 Capsule의 정보를 가지고 Sphere들을 생성한다. 이 함수를 실행한 후 vector에 담겨져 있는 Sphere들에 대해 SetParnet를 실행하여야 한다.
void TheShift::BoundingCapsule::CreateDerivedSpheres()
{
	// +/- Half Height의 위치에 Sphere 하나씩 추가
	auto NewTopSphere = std::make_shared<BoundingSphere>();
	NewTopSphere->SetAsDerivedSphere();
	NewTopSphere->SetAsTopSphereOfCapsule();
	//NewTopSphere->SetAsCollider();
	NewTopSphere->SetRadius(Radius);
	//Eigen::Vector3f TopLocation(0.f, 0.f, HalfHeight - Radius);
	//NewTopSphere->SetRelativeLocation(TopLocation);

	auto NewBottomSphere = std::make_shared<BoundingSphere>();
	NewBottomSphere->SetAsDerivedSphere();
	NewBottomSphere->SetAsBottomSphereOfCapsule();
	//NewBottomSphere->SetAsCollider();
	NewBottomSphere->SetRadius(Radius);
	//Eigen::Vector3f BottomLocation(0.f, 0.f, -(HalfHeight - Radius));
	//NewBottomSphere->SetRelativeLocation(BottomLocation);

	Spheres.emplace_back(NewTopSphere);
	Spheres.emplace_back(NewBottomSphere);

	// HalfHeight가 Radius를 포함한 값이 되므로 HalfHeight에서 Radius를 뺀 값으로 계산하는게 맞다.
	int Num = (int)((HalfHeight - Radius) / Radius);

	// Half Height를 Radius로 나누고 floor한 값이 1보다 크면
	if (Num >= 1)
	{
		// 제일 아래, 제일 위 두개의 Sphere 사이에
		// Num개 만큼 Sphere를 추가한다.
		// Num개를 추가한다는 것은 Num + 1 등분을 한다는 것.
		float Interval = (HalfHeight * 2.f) / (float)(Num + 1);

		for (int i = 0; i < Num; ++i)
		{
			auto NewSphere = std::make_shared<BoundingSphere>();
			NewSphere->SetAsDerivedSphere();
			//NewSphere->SetAsCollider();
			NewSphere->SetRadius(Radius);
			//Eigen::Vector3f NewLocation(0.f, 0.f, -HalfHeight + Interval * (1 + i));
			//NewSphere->SetRelativeLocation(NewLocation);

			// Setting한 sphere들을 vector에 추가.
			Spheres.emplace_back(NewSphere);
		}
	}
}

void TheShift::BoundingCapsule::CalculateAlignedExtent()
{
	Eigen::Vector3f Extent(Radius, Radius, HalfHeight);
	CalculatedAlignedExtent = AABB::CalculateExtent(Extent, LocalTransform.GetWorldRotation());
	ExtentUpdateNeeded = false;
}

BoundingSphere::BoundingSphere(Eigen::Vector3f& RelativeLocation, Eigen::Vector3f& RelativeRotation, float NewRadius) :
	BoundingVolume(RelativeLocation, RelativeRotation), Radius(NewRadius)
{
	VolumeType = BoundingVolumeType::Sphere;
}

bool BoundingSphere::Intersect(BoundingVolume& Other)
{
	bool RetVal = false;
	CHECK_VOLUMETYPE_AND_DO_COLLIDE(RetVal, Other);
	if (RetVal == false)
	{
		IntersectedLength = 0.f;
		CollisionPlaneNormalVector.setZero();
		CollisionPlaneLocation.setZero();
	}
	return RetVal;
}

bool BoundingSphere::Intersect(AABB& Other)
{
	return Other.Intersect(*this);
}

bool BoundingSphere::Intersect(OBB& Other)
{
	return Other.Intersect(*this);
}

bool BoundingSphere::Intersect(BoundingSphere& Other)
{
	auto MyTranslation = LocalTransform.GetWorldTranslation();
	auto OtherTranslation = Other.GetTransform().GetWorldTranslation();
	float OtherRadius = Other.GetRadius();

	bool DoesIntersect = (Radius + OtherRadius) * (Radius + OtherRadius) > (OtherTranslation - MyTranslation).squaredNorm();

	// Calculate CollisionNormVector
	if (DoesIntersect)
	{
		// 충돌면의 법선벡터
		Eigen::Vector3f Temp = MyTranslation - OtherTranslation;
		Temp.normalize();
		CollisionPlaneNormalVector = Temp;

		//TODO: Collision Plane의 Location저장
		// Static       vs      Static      : 아무것도 안함
		// Static       vs      Dynamic     : Static은 움직이지 않고 Dynamic이 겹친만큼 이동시켜줌.
		// Dynamic      vs      Dynamic     : 겹친만큼의 반만큼 동일하게 이동시켜줌.

		// IsMovable이 true면 Dynamic, false면 Static

		auto OtherParentActor = Other.GetParentActor();
		bool MyIsMovable = GetParentActor()->GetIsMovable();
		bool OtherIsMovable = OtherParentActor->GetIsMovable();

		// Dynamic vs Dynamic
		if (MyIsMovable == true && OtherIsMovable == true)
		{
			// 중간지점
			CollisionPlaneLocation = (MyTranslation + OtherTranslation) / 2.f;
		}

		// Dynamic vs Static
		else if (MyIsMovable == true && OtherIsMovable == false)
		{
			// Other의 접점
			// MyTranslation - OtherTranslation과 Other의 교차점을 찾는다. (길이를 MyRadius로 하면 됨)
			//Eigen::Vector3f OtherToMine = (MyTranslation - OtherTranslation);
			//OtherToMine.normalize();
			//OtherToMine *= Other.GetRadius();
			//CollisionPlaneLocation = OtherToMine;

			CollisionPlaneLocation = MyTranslation + CollisionPlaneNormalVector * (Radius - IntersectedLength);
		}

		// Static vs Dynamic
		else if (MyIsMovable == false && OtherIsMovable == true)
		{
			// this의 접점
			// OtherTranslation - MyTranslation과 this의 교차점을 찾는다. (길이를 OtherRadius로 하면 됨)
			//Eigen::Vector3f MineToOther = (OtherTranslation - OtherTranslation);
			//MineToOther.normalize();
			//MineToOther *= Radius;
			//CollisionPlaneLocation = MineToOther;

			CollisionPlaneLocation = MyTranslation + CollisionPlaneNormalVector * Radius;
		}

		// Static vs Static
		else if (MyIsMovable == false && OtherIsMovable == false)
		{
			// Do nothing
		}

		// 겹친 길이
		Eigen::Vector3f OtherCollisionPlaneNormalVector = -CollisionPlaneNormalVector;
		IntersectedLength = (Radius + OtherRadius) - (OtherTranslation - MyTranslation).norm();
		Other.SetLastIntersectedLength(IntersectedLength);
		Other.SetLastCollisionPlaneInfo(OtherCollisionPlaneNormalVector, CollisionPlaneLocation);
	}

	return DoesIntersect;
}

bool BoundingSphere::Intersect(BoundingCapsule& Other)
{
	return false;
}

bool BoundingSphere::Intersect(Ray& Other, float* ContactDist /*= nullptr*/, float RayRadius /*= 0.f*/)
{
	return Other.Intersect(*this, ContactDist, RayRadius);
}

void BoundingSphere::Resolve(float deltaTime)
{
	Eigen::Vector3f Location = LocalTransform.GetWorldTranslation();
	//LOG_INFO("Resolving sphere: %f %f %f", Location.x(), Location.y(), Location.z());
	//LOG_INFO("Last Location: %f %f %f", LastTranslation.x(), LastTranslation.y(), LastTranslation.z());

	bool Moved = false;
	UID ClosestContactVolumeId = INVALID_UID;
	Eigen::Vector3f CurrentLocation = LocalTransform.GetWorldTranslation();;
	Eigen::Vector3f LastLocation = LastTranslation;
	float DeltaTime = deltaTime;

	// 이전 위치에서 현재 위치로의 Ray
	Ray LastToCurrentRay;
	LastToCurrentRay.StartPoint = LastLocation;
	LastToCurrentRay.Direction = (CurrentLocation - LastLocation).normalized();
	LastToCurrentRay.SetEndPoint(CurrentLocation);
	if (LastToCurrentRay.GetLength() < FLT_EPSILON)
		return;

	while (true)
	{
		float ClosestContactDist = FLT_MAX;
		Eigen::Vector3f ClosestContactLocation;
		Eigen::Vector3f ClosestContactNormal;
		bool Collided = false;

		//TODO: 2. 계속해서 충돌하고 있을때

		// 1. 이전위치에서 다음 위치로 Ray로 CloseVolumes 모두 충돌체크 해보기
		for (TheShift::UID Id : CloseVolumes)
		{
			//LOG_INFO("Volume Num: %d", CloseVolumes.size());
			// 이전에 충돌처리했던 Id와는 바로 다시 검사하지 않는다.
			if (Id == ClosestContactVolumeId)
				continue;

			auto OtherVolume = Resolver->GetBoundingVolume(Id);

			// 충돌 지점까지의 거리
			float ContactDist;
			if (OtherVolume->Intersect(LastToCurrentRay, &ContactDist, Radius) == false)
				continue;

			//// StartPoint가 충돌할 경우 무시한다...?
			//if(ContactDist < FLT_EPSILON)
			//	continue;

			//LOG_INFO("%f", ContactDist);

			Collided = true;

			// 지금까지 최단 충돌거리보다 짧을 경우 ClosestContactVolumeId와 ClosestContactDist를 갱신시킨다.
			if (ContactDist < ClosestContactDist)
			{
				ClosestContactVolumeId = Id;
				ClosestContactDist = ContactDist;
				//ContactLocation = LastLocation + LastToCurrentRay.Direction.normalized() * ContactDist;
				ClosestContactLocation = LastToCurrentRay.GetCollisionPoint();
				//LOG_INFO("Contact Location: %f %f %f", ClosestContactLocation.x(), ClosestContactLocation.y(), ClosestContactLocation.z());
				ClosestContactNormal = LastToCurrentRay.GetCollisionNormal();
			}
		}



		// 더이상 충돌이 없다면 종료한다.
		if (!Collided)
		{
			// 종료하기 전에 변경된 위치는 적용시킨다.
			if (Moved)
			{
				Eigen::Vector3f MoveVector = CurrentLocation - LocalTransform.GetWorldTranslation();
				//GetParentActor()->Translate(CurrentLocation - LocalTransform.GetWorldTranslation());
				GetParentActor()->GetTransform().AddTranslation(CurrentLocation - LocalTransform.GetWorldTranslation());
				Eigen::Vector3f ResolvedLocation = LocalTransform.GetWorldTranslation();
				//LOG_INFO("Resolved location: %f %f %f", ResolvedLocation.x(), ResolvedLocation.y(), ResolvedLocation.z());
				//LOG_INFO("Move Vector: %f %f %f", MoveVector.x(), MoveVector.y(), MoveVector.z());
			}
			break;
		}

		// 2. 가장 가까운 위치에서 충돌한 Volume 찾기


		// 4. SlidingVector 계산
		// (1) SlidingVector 구하는 함수 만들기
		//LOG_INFO("Ray Length: %f", LastToCurrentRay.GetLength());
		Eigen::Vector3f SlidingVector = GetSlidingVector(LastToCurrentRay.Direction * LastToCurrentRay.GetLength(), ClosestContactNormal);
		//LOG_INFO("Sliding Vector: %f %f %f", SlidingVector.x(), SlidingVector.y(), SlidingVector.z());

		bool NoMovement = false;
		// SlidingVector가 0, 0, 0 일 경우엔 멈춘 상태이므로 남은 시간만큼 계산을 해줄 필요가 없다.
		if (!SlidingVector.isZero())
		{
			// 5. 해당 위치까지 걸린 deltaTime 빼주기
			float VelocityMag = GetParentActor()->GetMovementComp()->GetCurrentVelocity().norm()/* - 0.1f*/;
			float TimePassed = ClosestContactDist / VelocityMag;
			if (TimePassed < 0.00016)
				DeltaTime = -1.f;
			else
				DeltaTime -= TimePassed;
			//LOG_INFO("TimePassed: %f DeltaTime: %f", TimePassed, DeltaTime);
		}
		else
		{
			NoMovement = true;
		}

		// 3. 해당 위치로 이동
		LastLocation = ClosestContactLocation;
		// 6. 남은 deltaTime만큼 sliding vector로 이동
		CurrentLocation = LastLocation + SlidingVector/* * DeltaTime*/;


		// 움직였음을 체크해준다.
		// 지금 충돌한 Volume을 제외한 다른 Volume들과 충돌이 없을 때 종료하기 전에 이동시켜주기 위함.
		Moved = true;

		// 7. 다시 Resolve(남은 deltaTime)

		// 탈출 조건
		// 1. DeltaTime이 -일때
		if (DeltaTime < 0.f || NoMovement)
		{
			//CurrentLocation = ClosestContactLocation;

			// Actor를 이동시켜주고 loop를 종류한다.
			if (Collided)
				GetParentActor()->GetTransform().AddTranslation(CurrentLocation - LocalTransform.GetWorldTranslation());
			//GetParentActor()->Translate(CurrentLocation - LocalTransform.GetWorldTranslation());
			Eigen::Vector3f ResolvedLocation = LocalTransform.GetWorldTranslation();
			//LOG_INFO("Resolved location: %f %f %f", ResolvedLocation.x(), ResolvedLocation.y(), ResolvedLocation.z());
			break;
		}
	}
}

float BoundingSphere::GetRadius() const
{
	return Radius;
}

bool BoundingSphere::IsTopSphereOfCapsule() const
{
	return IsTopOfCapsule;
}

bool BoundingSphere::IsBottomSphereOfCapsule() const
{
	return IsBottomOfCapsule;
}

void BoundingSphere::SetAsTopSphereOfCapsule()
{
	IsTopOfCapsule = true;
}

void BoundingSphere::SetAsBottomSphereOfCapsule()
{
	IsBottomOfCapsule = true;
}

TheShift::BoundingSphere::BoundingSphere() : BoundingVolume(), Radius(0)
{
	VolumeType = BoundingVolumeType::Sphere;
}

void TheShift::BoundingSphere::SetRadius(float NewRadius)
{
	Radius = NewRadius;
	CollisionRange = Radius;
}

// Capsule에서 Derived된 sphere면 true를 return한다.
bool TheShift::BoundingSphere::IsPartOfCapsule() const
{
	return IsDerived;
}

void TheShift::BoundingSphere::SetAsDerivedSphere()
{
	IsDerived = true;
}

void TheShift::BoundingSphere::CalculateAlignedExtent()
{
	// Sphere는 회전이 되어 봤자 똑같으니까 그대로 return
	CalculatedAlignedExtent = Eigen::Vector3f(Radius, Radius, Radius);
	ExtentUpdateNeeded = false;
}

Ray::Ray()
{
	CollisionNormal = Eigen::Vector3f::Zero();
}

// Ray가 충돌한 면의 normal vector를 return한다.
const Eigen::Vector3f& Ray::GetCollisionNormal() const
{
	return CollisionNormal;
}

// Ray가 충돌한 위치를 return한다.
const Eigen::Vector3f& Ray::GetCollisionPoint() const
{
	return CollisionPoint;
}

float Ray::GetLength() const
{
	return Length;
}

bool Ray::Intersect(BoundingVolume* Other, float* ContactDist, float RayRadius, bool Precise /*=true*/)
{
	switch (Other->GetVolumeType())
	{
	case BoundingVolumeType::AABB:
		return Intersect(*reinterpret_cast<AABB*>(Other), ContactDist, RayRadius, Precise);

	case BoundingVolumeType::OBB:
		return Intersect(*reinterpret_cast<OBB*>(Other), ContactDist, RayRadius, Precise);

	case BoundingVolumeType::Capsule:
		return Intersect(*reinterpret_cast<BoundingCapsule*>(Other), ContactDist, RayRadius);

	case BoundingVolumeType::Sphere:
		return Intersect(*reinterpret_cast<BoundingSphere*>(Other), ContactDist, RayRadius);

	default:
		return false;
	}
}

// AABB와 intersect하는지 검사한다. intersect한다면 StartPoint로부터 충돌지점까지 길이를 ContactDist에 저장한다.
// Ray가 radius를 가진다면 인자로 전달한다.
bool Ray::Intersect(AABB& Other, float* ContactDist /*= nullptr*/, float RayRadius /*= 0.f*/, bool Precise /*=true*/)
{
	Eigen::Vector3f BoxLocation = Other.GetTransform().GetWorldTranslation();
	Eigen::Vector3f BoxMin = BoxLocation - Other.GetAlignedExtent();
	Eigen::Vector3f BoxMax = BoxLocation + Other.GetAlignedExtent();
	Eigen::Vector3f Min = BoxMin - Eigen::Vector3f(RayRadius, RayRadius, RayRadius);
	Eigen::Vector3f Max = BoxMax + Eigen::Vector3f(RayRadius, RayRadius, RayRadius);

	// r.dir is unit direction vector of ray
	Eigen::Vector3f DirFrac;
	DirFrac.x() = 1.f / Direction.x();
	DirFrac.y() = 1.f / Direction.y();
	DirFrac.z() = 1.f / Direction.z();

	// lb is the corner of AABB with minimal coordinates - left bottom, rt is maximal corner
	// r.org is origin of ray
	float t1 = (Min.x() - StartPoint.x()) * DirFrac.x();
	float t2 = (Max.x() - StartPoint.x()) * DirFrac.x();
	float t3 = (Min.y() - StartPoint.y()) * DirFrac.y();
	float t4 = (Max.y() - StartPoint.y()) * DirFrac.y();
	float t5 = (Min.z() - StartPoint.z()) * DirFrac.z();
	float t6 = (Max.z() - StartPoint.z()) * DirFrac.z();

	float tmin = std::max(std::max(std::min(t1, t2), std::min(t3, t4)), std::min(t5, t6));
	float tmax = std::min(std::min(std::max(t1, t2), std::max(t3, t4)), std::max(t5, t6));

	// if tmax < 0, ray (line) is intersecting AABB, but the whole AABB is behind us
	if (tmax < 0)
	{
		if (ContactDist != nullptr)
			*ContactDist = tmax;
		return false;
	}

	// if tmin > tmax, ray doesn't intersect AABB
	if (tmin > tmax)
	{
		if (ContactDist != nullptr)
			*ContactDist = tmax;
		return false;
	}

	float t;

	if (std::abs(tmin) < std::abs(tmax))
		t = tmin;
	else
		t = tmax;

	// Ray가 평행하고 교차점을 찾을 수 없을때.
	if (!_finitef(t))
	{
		return false;
	}

	if (HasEnd && !Precise)
	{
		// 충돌한 위치까지의 길이가 Ray의 길이보다 길면 return false
		if (std::abs(t) > Length)
		{
			return false;
		}
	}

	//t -= 0.1f;
	CollisionPoint = StartPoint + Direction * t;
	// Ray와 Box가 접촉한 Point를 구한다.
	Eigen::Vector3f BoxContactPoint;
	BoxContactPoint.x() = std::min(std::max(BoxMin.x(), CollisionPoint.x()), BoxMax.x());
	BoxContactPoint.y() = std::min(std::max(BoxMin.y(), CollisionPoint.y()), BoxMax.y());
	BoxContactPoint.z() = std::min(std::max(BoxMin.z(), CollisionPoint.z()), BoxMax.z());

	if (Precise)
	{
		int RoundOffAxisNum = 0;


		// BoxContactPoint랑 BoxLocation이랑 비교
		Eigen::Vector3f EdgeStart = BoxMin;
		Eigen::Vector3f EdgeEnd = BoxMax;
		if (CollisionPoint.x() < BoxMin.x())
		{
			EdgeStart.x() = BoxMin.x();
			EdgeEnd.x() = BoxMin.x();
			++RoundOffAxisNum;
		}
		else if (CollisionPoint.x() > BoxMax.x())
		{
			EdgeStart.x() = BoxMax.x();
			EdgeEnd.x() = BoxMax.x();
			++RoundOffAxisNum;
		}
		if (CollisionPoint.y() < BoxMin.y())
		{
			EdgeStart.y() = BoxMin.y();
			EdgeEnd.y() = BoxMin.y();
			++RoundOffAxisNum;
		}
		else if (CollisionPoint.y() > BoxMax.y())
		{
			EdgeStart.y() = BoxMax.y();
			EdgeEnd.y() = BoxMax.y();
			++RoundOffAxisNum;
		}
		if (CollisionPoint.z() < BoxMin.z() && RoundOffAxisNum < 2)
		{
			EdgeStart.z() = BoxMin.z();
			EdgeEnd.z() = BoxMin.z();
			++RoundOffAxisNum;
		}
		else if (CollisionPoint.z() > BoxMax.z() && RoundOffAxisNum < 2)
		{
			EdgeStart.z() = BoxMax.z();
			EdgeEnd.z() = BoxMax.z();
			++RoundOffAxisNum;
		}

		if (HasEnd)
		{
			// 충돌한 위치까지의 길이가 Ray의 길이보다 길면 return false
			if (std::abs(t) > Length && RoundOffAxisNum < 2)
			{
				return false;
			}
		}

		if (Precise)
		{
			if (RoundOffAxisNum >= 2)
			{
				// 2. Ray의 양 끝단을 구한다.
				// StartPoint - RayEnd
				// Line endpoint들 사이의 vector들
				Eigen::Vector3f RayEnd = StartPoint + Direction * Length;
				Eigen::Vector3f V0 = EdgeStart - StartPoint;
				Eigen::Vector3f V1 = EdgeEnd - StartPoint;
				Eigen::Vector3f V2 = EdgeStart - RayEnd;
				Eigen::Vector3f V3 = EdgeEnd - RayEnd;

				// Squared distance
				float D0 = V0.dot(V0);
				float D1 = V1.dot(V1);
				float D2 = V2.dot(V2);
				float D3 = V3.dot(V3);

				// 가능성 있는 endpoint를 고른다.
				Eigen::Vector3f BestA;
				if (D2 < D0 || D2 < D1 || D3 < D0 || D3 < D1)
				{
					BestA = RayEnd;
				}
				else
				{
					BestA = StartPoint;
				}

				// Edge에서 가능성있는 endpoint를 고른다.
				Eigen::Vector3f BestB = ClosestPointOnLineSegment(EdgeStart, EdgeEnd, BestA);

				if (IntersectSphere(BestB, RayRadius, ContactDist))
				{
					return true;
				}
				return false;
			}
		}
	}

	// t가 음수면 normal이 반대로 되어야 한다.
	//if(t < 0.f)
	//	CollisionNormal = (BoxContactPoint - CollisionPoint).normalized();
	//else
	CollisionNormal = (CollisionPoint - BoxContactPoint).normalized();
	*ContactDist = t;

	return true;

}

bool Ray::Intersect(OBB& Other, float* ContactDist /*= nullptr*/, float RayRadius /*= 0.f*/, bool Precise /*=true*/)
{
	// 1. OBB를 회전시켜 AABB로 만들고
	Eigen::Vector3f OBBLocation = Other.GetTransform().GetWorldTranslation();

	AABB ConvertedAABB;
	ConvertedAABB.SetExtent(Other.GetExtent());
	ConvertedAABB.SetAlignedExtent(Other.GetExtent());
	ConvertedAABB.SetIsMovable(Other.GetIsMovable());
	Eigen::Quaternionf MyRotation = Other.GetTransform().GetWorldRotation();
	Eigen::Quaternionf InversedRotation = MyRotation.inverse();
	//InversedRotation.z() *= -1.f;
	//InversedRotation.w() *= -1.f;
	//InversedRotation = InversedRotation.inverse();


	// 2. Ray를 동일한 만큼 회전시킴 (Direction, StartPoint)
	Ray NewRay;
	//Transform Temp;
	//Temp.Rotate(InversedRotation);
	//Temp.Translate(Direction);
	//NewRay.Direction = Temp.GetWorldTranslation();
	NewRay.Direction = Transform::RotateVector(Direction, InversedRotation);

	NewRay.StartPoint = Transform::RotateVector(StartPoint - OBBLocation, InversedRotation);
	//NewRay.Direction = InversedRotation * Direction;
	//NewRay.StartPoint = InversedRotation * (StartPoint - OBBLocation);

	if (HasEnd == true)
	{
		NewRay.SetLength(Length);
	}

	// 3. 1, 2에서 새로 만들어진 AABB, 회전된 Ray를 가지고 검사를 한다.
	bool ReturnValue = NewRay.Intersect(ConvertedAABB, ContactDist, RayRadius);

	// 4. Intersect의 결과를 다시 제자리로 이동시켜준다.
	CollisionPoint = Transform::RotateVector(NewRay.GetCollisionPoint(), MyRotation) + OBBLocation;
	CollisionNormal = Transform::RotateVector(NewRay.GetCollisionNormal(), MyRotation);
	//CollisionPoint = (MyRotation * NewRay.GetCollisionPoint()) + OBBLocation;
	//CollisionNormal = MyRotation * NewRay.GetCollisionNormal();

	//if(ReturnValue)
	//	LOG_INFO("World Direction: %f %f %f, Local Direction: %f %f %f", Direction.x(), Direction.y(), Direction.z(), NewRay.Direction.x(), NewRay.Direction.y(), NewRay.Direction.z());

	return ReturnValue;
}

bool Ray::Intersect(BoundingCapsule& Other, float* ContactDist /*= nullptr*/, float RayRadius /*= 0.f*/)
{
	// Other의 각각 Sphere와 intersect를 진행하고 dist가 가장 짧은애를 사용한다.
	bool EverIntersected = false;

	float TempDist = FLT_MAX;
	Eigen::Vector3f TempNormal;
	Eigen::Vector3f TempCollisionPoint;
	for (std::shared_ptr<BoundingSphere> Sphere : Other.Spheres)
	{
		if (Intersect(*Sphere, ContactDist, RayRadius) == true)
		{
			EverIntersected = true;

			if (*ContactDist < TempDist)
			{
				TempDist = *ContactDist;
				TempNormal = CollisionNormal;
				TempCollisionPoint = CollisionPoint;
			}
		}

	}

	// 충돌을 했다면 가장 짧은 위치에서 충돌한 정보를 저장한다.
	if (EverIntersected == true)
	{
		*ContactDist = TempDist;
		CollisionNormal = TempNormal;
		CollisionPoint = TempCollisionPoint;
	}

	return EverIntersected;
}

// Sphere와 intersect하는지 검사한다. intersect한다면 StartPoint로부터 충돌지점까지 길이를 ContactDist에 저장한다.
// Ray가 radius를 가진다면 인자로 전달한다.
bool Ray::Intersect(BoundingSphere& Other, float* ContactDist /*= nullptr*/, float RayRadius /*= 0.f*/)
{
	return IntersectSphere(Other.GetTransform().GetWorldTranslation(), Other.GetRadius() + RayRadius, ContactDist);
	//	Eigen::Vector3f OtherLocation = Other.GetTransform().GetWorldTranslation();
	//	// Ray와 Sphere의 교점까지의 거리
	//	float t0, t1;

	//#if false 
	//	// geometric solution
	//	Eigen::Vector3f L = OtherLocation - StartPoint;
	//	float tca = L.dot(Direction);
	//	// if (tca < 0) return false;
	//	float d2 = L.dot(L) - tca * tca;
	//	if(d2 > Other.GetRadius() + RayRadius) 
	//		return false;
	//	float thc = sqrt(Other.GetRadius() + RayRadius - d2);
	//	t0 = tca - thc;
	//	t1 = tca + thc;
	//#else 
	//	// analytic solution
	//	Eigen::Vector3f L = OtherLocation - StartPoint;
	//	float a = Direction.dot(Direction);
	//	float b = 2 * Direction.dot(L);
	//	float c = L.dot(L) - Other.GetRadius() - RayRadius;
	//	if(SolveQuadratic(a, b, c, t0, t1) == false) 
	//		return false;
	//#endif 
	//	if(t0 > t1) std::swap(t0, t1);

	//	if(t0 < 0)
	//	{
	//		// t0이 음수면 t1을 쓴다.
	//		t0 = t1;

	//		// t0과 t1이 모두 음수면 false를 return한다.
	//		if(t0 < 0) 
	//			return false;
	//	}

	//	// Ray가 평행하고 교차점을 찾을 수 없을때.
	//	if(std::isinf(t0))
	//	{
	//		return false;
	//	}

	//	if(HasEnd)
	//	{
	//		// 충돌 지점까지의 길이가 Ray의 길이보다 크면 return false
	//		if(t0 > Length)
	//			return false;
	//	}

	//	CollisionPoint = StartPoint + Direction * t0;
	//	CollisionNormal = (CollisionPoint - OtherLocation).normalized();

	//	if(ContactDist != nullptr)
	//		*ContactDist = t0;
	//	return true;
}

bool Ray::IntersectSphere(const Eigen::Vector3f& Location, float Radius, float* ContactDist /*= nullptr*/)
{
	// Ray와 Sphere의 교점까지의 거리
	float t0, t1;

	//#if false 
		// geometric solution
		//Eigen::Vector3f L = Location - StartPoint;
		//float tca = L.dot(Direction);
		//// if (tca < 0) return false;
		//float d2 = L.dot(L) - tca * tca;
		////if(d2 > Radius)
		////	return false;
		//float thc = sqrt(Radius - d2);
		//t0 = tca - thc;
		//t1 = tca + thc;
	//#else 
		// analytic solution
	Eigen::Vector3f L = StartPoint - Location;
	float a = Direction.dot(Direction);
	float b = 2 * Direction.dot(L);
	float c = L.dot(L) - (Radius * Radius);
	if (SolveQuadratic(a, b, c, t0, t1) == false)
		return false;
	//#endif 

	// 둘다 -값이면 Ray의 방향 뒤에 서 충돌한것
	if (t0 < 0.f && t1 < 0.f)
		return false;

	//if(t0 > t1) std::swap(t0, t1);

	if (std::abs(t0) > std::abs(t1))
		std::swap(t0, t1);

	if (t0 < 0)
	{
		// t0이 음수면 t1을 쓴다.
		//LOG_INFO("t0 %f t1 %f Length: %f", t0, t1, Length);
		//t0 = std::abs(t0);
	}

	// Ray가 평행하고 교차점을 찾을 수 없을때.
	if (!_finitef(t0))
	{
		return false;
	}

	if (HasEnd)
	{
		// 충돌 지점까지의 길이가 Ray의 길이보다 크면 return false
		if (std::abs(t0) > Length)
			return false;
	}

	CollisionPoint = StartPoint + Direction * t0;
	CollisionNormal = (CollisionPoint - Location).normalized();

	if (ContactDist != nullptr)
		*ContactDist = t0;
	return true;
}

//// Volume type에 맞춰 intersection 검사를 진행한다.
//bool Ray::Intersect(BoundingVolume* Other, float* ContactDist /*= nullptr*/, float Radius /*= 0.f*/)
//{
//	bool Result = false;

//	switch(Other->GetVolumeType())
//	{
//	case BoundingVolumeType::AABB:
//		return Intersect(*static_cast<AABB*>(Other), ContactDist, Radius);

//	case BoundingVolumeType::OBB:
//		return Intersect(*static_cast<OBB*>(Other), ContactDist, Radius);

//	case BoundingVolumeType::Capsule:
//		return Intersect(*static_cast<BoundingCapsule*>(Other), ContactDist, Radius);

//	case BoundingVolumeType::Sphere:
//		return Intersect(*static_cast<BoundingSphere*>(Other), ContactDist, Radius);

//	default:
//		return false;
//	}
//}

//bool Ray::Intersect(BoundingVolume& Other, float* ContactDist /*= nullptr*/, float Radius /*= 0.f*/)
//{
//	return false;
//}

void Ray::SetEndPoint(const Eigen::Vector3f& End)
{
	HasEnd = true;
	Length = (End - StartPoint).norm();
}

void Ray::SetLength(float NewLength)
{
	HasEnd = true;
	Length = NewLength;
}

bool Ray::IsEndless() const
{
	return !HasEnd;
}

bool SolveQuadratic(float a, float b, float c, float& x0, float& x1)
{
	float discr = b * b - 4 * a * c;
	if (discr < 0)
		return false;
	else if (discr == 0) x0 = x1 = -0.5 * b / a;
	else
	{
		float q = (b > 0) ?
			-0.5 * (b + sqrt(discr)) :
			-0.5 * (b - sqrt(discr));
		x0 = q / a;
		x1 = c / q;
	}
	if (x0 > x1) std::swap(x0, x1);

	return true;
}

Eigen::Vector3f ClosestPointOnLineSegment(Eigen::Vector3f& A, Eigen::Vector3f& B, Eigen::Vector3f& Point)
{
	Eigen::Vector3f AB = B - A;
	float t = (Point - A).dot(AB) / AB.dot(AB);
	return A + std::min(std::max(t, 0.f), 1.f) * AB;
}

// 입사벡터 p의 plane(normal vector n을 가진 평면)에 대한 sliding vector를 계산한다.
Eigen::Vector3f GetSlidingVector(const Eigen::Vector3f& p, const Eigen::Vector3f& n)
{
	return p - n * (p.dot(n));
}
}

