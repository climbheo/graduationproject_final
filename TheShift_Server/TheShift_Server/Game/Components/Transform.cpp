﻿#include "Transform.h"
#ifdef TSSERVER
#include "Vendor/include/spdlog/spdlog.h"
#endif
#include "Protocol/Protocol.h"
#include "Game/Components/BoundingVolume.h"

namespace TheShift
{
Transform::Transform()
{
	LocalTransform = Eigen::Transform<float, 3, Eigen::Affine>::Identity();
	WorldTransform = Eigen::Transform<float, 3, Eigen::Affine>::Identity();
	WorldTranslation.setZero();
	Scaling = 1.f;
}

Transform::Transform(Eigen::Vector3f& InitTranslation)
{
	LocalTransform = Eigen::Transform<float, 3, Eigen::Affine>::Identity();
	WorldTransform = Eigen::Transform<float, 3, Eigen::Affine>::Identity();
	Translate(InitTranslation);
	WorldTranslation = InitTranslation;
	Scaling = 1.f;
}

void Transform::Translate(Eigen::Vector3f& Translation)
{
	//LastTransform = WorldTransform;
	LocalTransform.translate(Translation);
	UpdateWorldTransform(false);
}

void Transform::Translate(TheShift::Vector3f& Translation)
{
	//LastTransform = WorldTransform;
	LocalTransform.translate(Eigen::Vector3f(Translation.X, Translation.Y, Translation.Z));
	UpdateWorldTransform(false);
}

void Transform::Translate(float x, float y, float z)
{
	//LastTransform = WorldTransform;
	LocalTransform.translate(Eigen::Vector3f(x, y, z));
	UpdateWorldTransform(false);
}

void Transform::Rotate(const Eigen::Vector3f& Euler)
{
	Eigen::Quaternionf Quat =
		Eigen::AngleAxisf(Euler.z(), Eigen::Vector3f::UnitZ()) *
		Eigen::AngleAxisf(-Euler.y(), Eigen::Vector3f::UnitY()) *
		Eigen::AngleAxisf(-Euler.x(), Eigen::Vector3f::UnitX());
	LocalTransform.rotate(Quat);
	UpdateWorldTransform(true);
}

void Transform::Rotate(const TheShift::Vector3f& Euler)
{
	Eigen::Quaternionf Quat =
		Eigen::AngleAxisf(Euler.Z, Eigen::Vector3f::UnitZ()) *
		Eigen::AngleAxisf(-Euler.Y, Eigen::Vector3f::UnitY()) *
		Eigen::AngleAxisf(-Euler.X, Eigen::Vector3f::UnitX());
	LocalTransform.rotate(Quat);
	UpdateWorldTransform(true);
}

void Transform::Rotate(const Eigen::Quaternionf& Quat)
{
	//Eigen::Vector3f Euler = Quat.toRotationMatrix().eulerAngles(2, 1, 0);
	//auto RotQuat =
	//    Eigen::AngleAxisf(Euler.x(), Eigen::Vector3f::UnitZ()) *
	//    Eigen::AngleAxisf(-Euler.y(), Eigen::Vector3f::UnitY()) *
	//    Eigen::AngleAxisf(-Euler.z(), Eigen::Vector3f::UnitX());
	auto RotQuat = Quat;
	RotQuat.z() *= -1.f;
	RotQuat.w() *= -1.f;
	LocalTransform.rotate(RotQuat);
	UpdateWorldTransform(true);
}

Eigen::Vector3f Transform::RotateVector(const Eigen::Vector3f& Vector, const Eigen::Quaternionf& Quat)
{
	auto RotQuat = Quat;
	RotQuat.z() *= -1.f;
	RotQuat.w() *= -1.f;
	return RotQuat * Vector;
}

Eigen::Quaternionf Transform::GetFromToRotation(const Eigen::Vector3f& From, const Eigen::Vector3f& To)
{
	Eigen::Quaternionf Result;
	Eigen::Vector3f H = From + To;
	H.normalize();
	Result.w() = From.dot(H);
	Result.x() = From.y() * H.z() - From.z() * H.y();
	Result.y() = From.z() * H.x() - From.x() * H.z();
	Result.z() = From.x() * H.y() - From.y() * H.x();
	return Result;
}

Eigen::Vector3f Transform::RotateVector(float x, float y, float z, const Eigen::Quaternionf& Quat)
{
	return RotateVector(Eigen::Vector3f(x, y, z), Quat);
}

Eigen::Vector3f Transform::RotateVector(const Eigen::Vector3f& Vector, float EulerX, float EulerY, float EulerZ)
{
	Eigen::Quaternionf Quat =
		Eigen::AngleAxisf(EulerZ * M_PI/180.f, Eigen::Vector3f::UnitZ())
		* Eigen::AngleAxisf(EulerY * M_PI / 180.f, Eigen::Vector3f::UnitY())
		* Eigen::AngleAxisf(EulerX * M_PI / 180.f, Eigen::Vector3f::UnitX());

	return RotateVector(Vector, Quat);
}

Eigen::Vector3f Transform::RotateVector(float x, float y, float z, float EulerX, float EulerY, float EulerZ)
{
	return RotateVector(Eigen::Vector3f(x,y,z), EulerX, EulerY, EulerZ);
}

Eigen::Vector3f& Transform::ScaleVector(Eigen::Vector3f& Vector, float x, float y, float z)
{
	Vector.x() *= x;
	Vector.y() *= y;
	Vector.z() *= z;

	return Vector;
}

void Transform::RotateWithDegrees(const Eigen::Vector3f& Rotation)
{
	RotateWithDegrees(Rotation.x(), Rotation.y(), Rotation.z());
}

void Transform::RotateWithDegrees(const TheShift::Vector3f& Rotation)
{
	RotateWithDegrees(Rotation.X, Rotation.Y, Rotation.Z);
}

void Transform::RotateWithDegrees(float x, float y, float z)
{
	Eigen::Quaternionf Quat =
		Eigen::AngleAxisf(z * M_PI / 180.f, Eigen::Vector3f::UnitZ()) *
		Eigen::AngleAxisf(-y * M_PI / 180.f, Eigen::Vector3f::UnitY()) *
		Eigen::AngleAxisf(-x * M_PI / 180.f, Eigen::Vector3f::UnitX());
	LocalTransform.rotate(Quat);
	UpdateWorldTransform(true);
}

void Transform::Scale(float NewScale)
{
	LocalTransform.scale(NewScale);
	Scaling *= NewScale;
}

void Transform::SetTranslation(const Eigen::Vector3f& Translation)
{
	LocalTransform.translation() = Translation;
	UpdateWorldTransform(false);
	//LastTransform = WorldTransform;
}

void Transform::SetTranslation(float x, float y, float z)
{
	LocalTransform.translation() = Eigen::Vector3f(x, y, z);
	UpdateWorldTransform(false);
	//LastTransform = WorldTransform;
}

void Transform::SetRotation(Eigen::Vector3f& Euler)
{
	auto PrevEuler = LocalTransform.rotation().matrix().eulerAngles(2, 1, 0);
	auto PrevQuat =
		Eigen::AngleAxisf(PrevEuler.x(), Eigen::Vector3f::UnitZ()) *
		Eigen::AngleAxisf(PrevEuler.y(), Eigen::Vector3f::UnitY()) *
		Eigen::AngleAxisf(PrevEuler.z(), Eigen::Vector3f::UnitX());

	auto TargetQuat =
		Eigen::AngleAxisf(Euler.z(), Eigen::Vector3f::UnitZ()) *
		Eigen::AngleAxisf(-Euler.y(), Eigen::Vector3f::UnitY()) *
		Eigen::AngleAxisf(-Euler.x(), Eigen::Vector3f::UnitX());
	auto OffsetQuat = PrevQuat.inverse() * TargetQuat;

	LocalTransform.rotate(OffsetQuat);
	UpdateWorldTransform(true);
}

// x, y축 각도가 반대인 Quaternion을 사용해야 함.
// MakeQuat() 함수 사용해서 Quaternion 만드세요.
void Transform::SetRotation(const Eigen::Quaternionf& Quat)
{
	auto PrevEuler = LocalTransform.rotation().matrix().eulerAngles(2, 1, 0);
	auto PrevQuat =
		Eigen::AngleAxisf(PrevEuler.x(), Eigen::Vector3f::UnitZ()) *
		Eigen::AngleAxisf(PrevEuler.y(), Eigen::Vector3f::UnitY()) *
		Eigen::AngleAxisf(PrevEuler.z(), Eigen::Vector3f::UnitX());

	auto InputEuler = Quat.toRotationMatrix().eulerAngles(2, 1, 0);

	auto TargetQuat =
		Eigen::AngleAxisf(InputEuler.x(), Eigen::Vector3f::UnitZ()) *
		Eigen::AngleAxisf(-InputEuler.y(), Eigen::Vector3f::UnitY()) *
		Eigen::AngleAxisf(-InputEuler.z(), Eigen::Vector3f::UnitX());
	auto OffsetQuat = PrevQuat.inverse() * TargetQuat;
	LocalTransform.rotate(OffsetQuat);

	UpdateWorldTransform(true);
}

void Transform::SetRotationWithDegrees(float x, float y, float z)
{
	auto PrevEuler = LocalTransform.rotation().matrix().eulerAngles(2, 1, 0);
	auto PrevQuat =
		Eigen::AngleAxisf(PrevEuler.x(), Eigen::Vector3f::UnitZ()) *
		Eigen::AngleAxisf(PrevEuler.y(), Eigen::Vector3f::UnitY()) *
		Eigen::AngleAxisf(PrevEuler.z(), Eigen::Vector3f::UnitX());
	Eigen::Quaternionf Quat =
		Eigen::AngleAxisf(z * M_PI / 180.f, Eigen::Vector3f::UnitZ()) *
		Eigen::AngleAxisf(-y * M_PI / 180.f, Eigen::Vector3f::UnitY()) *
		Eigen::AngleAxisf(-x * M_PI / 180.f, Eigen::Vector3f::UnitX());
	Eigen::Quaternionf OffsetQuat = PrevQuat.inverse() * Quat;

	LocalTransform.rotate(OffsetQuat);
	UpdateWorldTransform(true);
}

void Transform::SetRotationWithDegrees(const Eigen::Vector3f& Degrees)
{
	auto PrevEuler = LocalTransform.rotation().matrix().eulerAngles(2, 1, 0);
	auto PrevQuat =
		Eigen::AngleAxisf(PrevEuler.x(), Eigen::Vector3f::UnitZ()) *
		Eigen::AngleAxisf(PrevEuler.y(), Eigen::Vector3f::UnitY()) *
		Eigen::AngleAxisf(PrevEuler.z(), Eigen::Vector3f::UnitX());

	Eigen::Quaternionf TargetQuat =
		Eigen::AngleAxisf(Degrees.z() * M_PI / 180.f, Eigen::Vector3f::UnitZ()) *
		Eigen::AngleAxisf(-Degrees.y() * M_PI / 180.f, Eigen::Vector3f::UnitY()) *
		Eigen::AngleAxisf(-Degrees.x() * M_PI / 180.f, Eigen::Vector3f::UnitX());
	auto OffsetQuat = PrevQuat.inverse() * TargetQuat;
	LocalTransform.rotate(OffsetQuat);

	UpdateWorldTransform(true);
}

//TODO
void Transform::SetScale(float Value)
{}

void Transform::AddTranslation(const Eigen::Vector3f& Translation)
{
	//LastTransform = WorldTransform;
	LocalTransform.translation() = LocalTransform.translation() + Translation;
	UpdateWorldTransform(false);
}

Eigen::Vector3f Transform::GetTranslation() const
{
	return LocalTransform.translation();
}

Eigen::Quaternionf Transform::GetRotation() const
{
	auto Euler = LocalTransform.rotation().matrix().eulerAngles(2, 1, 0);
	auto NewQuat =
		Eigen::AngleAxisf(Euler.x(), Eigen::Vector3f::UnitZ()) *
		Eigen::AngleAxisf(-Euler.y(), Eigen::Vector3f::UnitY()) *
		Eigen::AngleAxisf(-Euler.z(), Eigen::Vector3f::UnitX());

	return NewQuat;
}

/// <return>
/// rotation을 euler각으로 return한다.
/// </return>
Eigen::Vector3f Transform::GetEuler() const
{
	auto Euler = LocalTransform.rotation().matrix().eulerAngles(2, 1, 0);
	Eigen::Vector3f RetVal;
	RetVal.x() = -Euler.z();
	RetVal.y() = -Euler.y();
	RetVal.z() = Euler.x();
	return RetVal;
}

/// <return>
/// rotation을 degree로 return한다. 
/// </return>
Eigen::Vector3f Transform::GetDegrees() const
{
	Eigen::Vector3f RetVal;
	auto Euler = LocalTransform.rotation().matrix().eulerAngles(2, 1, 0);
	RetVal.x() = -Euler.z() * (180.f / M_PI);
	RetVal.y() = -Euler.y() * (180.f / M_PI);
	RetVal.z() = Euler.x() * (180.f / M_PI);
	return RetVal;
}

float Transform::GetScale() const
{
	return Scaling;
}

Eigen::Vector3f Transform::GetLookVector() const
{
	Eigen::Vector3f LookAxis(1.f, 0.f, 0.f);
	return GetRotation() * LookAxis;
}

Eigen::Vector3f Transform::GetRightVector() const
{
	Eigen::Vector3f RightAxis(0.f, 1.f, 0.f);
	return GetRotation() * RightAxis;
}

const Eigen::Transform<float, 3, Eigen::Affine>& Transform::GetData() const
{
	return LocalTransform;
}

const Eigen::Transform<float, 3, Eigen::Affine>& Transform::GetWorldTransformData() const
{
	return WorldTransform;
}

Eigen::Vector3f Transform::GetWorldTranslation()
{
	//return WorldTransform.translation();
	return WorldTranslation;
}

//Eigen::Vector3f Transform::GetLastTranslation() const
//{
//    return LastTransform.translation();
//}

Eigen::Quaternionf Transform::GetWorldRotation()
{
	//auto Euler = WorldTransform.rotation().matrix().eulerAngles(2, 1, 0);
	//auto NewQuat1 =
	//    Eigen::AngleAxisf(Euler.x(), Eigen::Vector3f::UnitZ()) *
	//    Eigen::AngleAxisf(-Euler.y(), Eigen::Vector3f::UnitY()) *
	//    Eigen::AngleAxisf(-Euler.z(), Eigen::Vector3f::UnitX());

	//Eigen::Quaternionf NewQuat(WorldTransform.rotation());
	if(WorldRotationMatrixUpdateNeeded == true)
	{
		WorldRotationMatrix = WorldTransform.rotation();
		WorldRotationMatrixUpdateNeeded = false;
	}
	Eigen::Quaternionf NewQuat(WorldRotationMatrix);
	NewQuat.z() *= -1.f;
	NewQuat.w() *= -1.f;
	return NewQuat;
}

Eigen::Vector3f Transform::GetWorldEuler()
{
	//auto Euler = WorldTransform.rotation().matrix().eulerAngles(2, 1, 0);
	if(WorldRotationMatrixUpdateNeeded == true)
	{
		WorldRotationMatrix = WorldTransform.rotation();
		WorldRotationMatrixUpdateNeeded = false;
	}
	auto Euler = WorldRotationMatrix.eulerAngles(2, 1, 0);
	Eigen::Vector3f RetVal;
	RetVal.x() = -Euler.z();
	RetVal.y() = -Euler.y();
	RetVal.z() = Euler.x();
	return Euler;
}

Eigen::Vector3f Transform::GetWorldDegrees()
{
	Eigen::Vector3f RetVal;
	//auto Euler = WorldTransform.rotation().matrix().eulerAngles(2, 1, 0);
	if(WorldRotationMatrixUpdateNeeded == true)
	{
		WorldRotationMatrix = WorldTransform.rotation();
		WorldRotationMatrixUpdateNeeded = false;
	}
	auto Euler = WorldRotationMatrix.eulerAngles(2, 1, 0);

	RetVal.x() = -Euler.z() * (180.f / M_PI);
	RetVal.y() = -Euler.y() * (180.f / M_PI);
	RetVal.z() = Euler.x() * (180.f / M_PI);
	return RetVal;
}

Eigen::Vector3f Transform::ApplyTransform(Eigen::Vector3f& Origin) const
{
	float ArrVertices[3] = { Origin[0], Origin[1], Origin[2] };

	Eigen::MatrixXf Temp = ApplyTransform<1>(ArrVertices);
	Eigen::Vector3f RetVal;
	RetVal = Temp.col(0);

	return RetVal;
}

void Transform::SetTransform(const Eigen::Transform<float, 3, Eigen::Affine>& NewTransform)
{
	LocalTransform = NewTransform;
	//LastTransform = LocalTransform;
}

void Transform::SetParent(const Transform* Other)
{
	Parent = Other;
	UpdateWorldTransform(true);
}

void Transform::AddChild(Transform* Other)
{
	Childs.emplace_back(Other);
}

void Transform::UpdateWorldTransform(bool RotationIncluded)
{
	if(Parent != nullptr)
	{
		WorldTransform = (Parent->GetWorldTransformData() * LocalTransform);
		WorldTranslation = WorldTransform.translation();
	}
	else
	{
		WorldTransform = LocalTransform;
		WorldTranslation = WorldTransform.translation();
	}

	// 변환하였으면 BoundingVolume을 재정렬 시키고 Aligned Extent를 다시 계산한다.
	if(RotationIncluded)
	{
		WorldRotationMatrixUpdateNeeded = true;
		for(auto Volume : BoundingVolumes)
		{
			Volume->NotifyExtentUpdateNeeded();
			//        if(BoundingVolumeRearranger)
						//BoundingVolumeRearranger(Volume);
		}
	}
	//else
	//{
		// 회전하지 않았으면 재정렬만 시킨다.
		//for(auto Volume : BoundingVolumes)
		//{
			//if(BoundingVolumeRearranger)
				//BoundingVolumeRearranger(Volume);
		//}
	//}

	for(auto& Transform : Childs)
	{
		Transform->UpdateWorldTransform(RotationIncluded);
	}
}

void Transform::RegisterBoundingVolumeRearranger(std::function<void(BoundingVolume*)> Rearranger)
{
	BoundingVolumeRearranger = Rearranger;
}

void Transform::RegisterBoundingVolume(BoundingVolume* ChildVolume)
{
	BoundingVolumes.emplace_back(ChildVolume);
}
}

