﻿#pragma once

#include "Game/Components/Transform.h"
#include "Protocol/Serializables.h"


constexpr float COEFFICIENT = 0.01f;

namespace TheShift
{
class Movement
{
public:
	Movement(Transform* worldTransform, const StatusType* currentStatus, Stats* stat);
	Movement(const Movement& Other) = delete;

	Movement& operator=(const Movement& Other);

	Eigen::Vector3f GetCurrentVelocity() const;
	Eigen::Vector3f GetCurrentAcceleration() const;

	void Update(float deltaTime);
	Eigen::Quaternionf GetLastUpdateRotation() const;
	Eigen::Vector3f GetLastUpdateLocation() const;
	Eigen::Vector3f GetLastVelocity() const;
	Eigen::Vector3f GetInputDirection() const;

	//Unlock하는 순간 Pressing인게 있다면 Unlock후 초기화된 InputDirection에 이 함수를 통해 Pressing을 반영한다.
	void SetInputDirection(Eigen::Vector3f& NewDirection, Serializables::Input& RecentInput);
	///////////
	void SetVelocity(Eigen::Vector3f& NewVelocity);
	void ApplyInput(Serializables::Input Input, bool IsRotate = true);
	void AddImpulse(Eigen::Vector3f& impulse, float deltaTime);
	void AddForce(float x, float y, float z, float deltaTime);
	void SetDestination(Eigen::Vector3f& Target, bool IsChase = true);
	void SetGravity(bool Value);


	void LockInput();
	void UnlockInput();
	bool IsLocked() { return InputLocked; }

private:
	Eigen::Vector3f CurrentVelocity;
	Eigen::Vector3f CurrentAcceleration;
	const StatusType* CurrentStatus;
	Eigen::Vector3f CurrentMoveDirection;

	// Actor의 기본 능력치
	Stats* Stat;

	Transform* WorldTransform;
	Transform LastUpdatedTransform;
	Eigen::Vector3f LastVelocity;

	Eigen::Vector3f InputDirection;
	Eigen::Vector3f InputAcceleration;

	bool InputPressed = false;
	bool InputLocked = false;

	bool Gravity = true;
};
}

