﻿#include "ItemComponent.h"
#include "Game/Actors/Actor.h"
#include "Game/Components/InventoryComponent.h"


namespace TheShift
{
ItemComponent::ItemComponent()
{
}

ItemComponent::~ItemComponent()
{
}

std::shared_ptr<Actor> ItemComponent::GetOwnerActor()
{
	return OwnerActor;
}

std::shared_ptr<InventoryComponent> ItemComponent::GetOwnerInventoryComponent()
{
	return OwnerInventoryComponent;
}

int ItemComponent::GetItemType() const
{
	return 0;
}

void ItemComponent::SetOwnerActor(std::shared_ptr<Actor> NewOwnerActor)
{
	OwnerActor = NewOwnerActor;
}

void ItemComponent::SetOwnerInventory(std::shared_ptr<InventoryComponent> NewOwnerInventory)
{
	OwnerInventoryComponent = NewOwnerInventory;
}
}
