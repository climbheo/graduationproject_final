﻿#pragma once

#define _USE_MATH_DEFINES
#include "Protocol/Protocol.h"
#include <math.h>
#ifdef TSSERVER
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <Eigen/Dense>
#elif defined(TSCLIENT)
#include "../../Vendor/include/Eigen/Core"
#include "../../Vendor/include/Eigen/Geometry"
#include "../../Vendor/include/Eigen/Dense"
#endif

#include "Protocol/Types.h"

namespace TheShift
{
class BoundingVolume;

class Transform
{
public:
	Transform();
	Transform(Eigen::Vector3f& InitTranslation);
	Transform(Eigen::Vector3f& InitTranslation, Eigen::Vector3f& InitRotation);
	Transform(Eigen::Vector3f& InitTranslation, Eigen::Vector3f& InitRotation, float InitScale);

	void Translate(Eigen::Vector3f& Translation);
	void Translate(TheShift::Vector3f& Translation);
	void Translate(float x, float y, float z);
	void Rotate(const Eigen::Vector3f& Euler);
	void Rotate(const TheShift::Vector3f& Euler);
	void Rotate(const Eigen::Quaternionf& Quat);
	static Eigen::Vector3f RotateVector(const Eigen::Vector3f& Vector, const Eigen::Quaternionf& Quat);
	static Eigen::Quaternionf GetFromToRotation(const Eigen::Vector3f& From, const Eigen::Vector3f& To);
	static Eigen::Vector3f RotateVector(float x, float y, float z, const Eigen::Quaternionf& Quat);
	static Eigen::Vector3f RotateVector(const Eigen::Vector3f& Vector, float EulerX, float EulerY, float EulerZ);
	static Eigen::Vector3f RotateVector(float x, float y, float z, float EulerX, float EulerY, float EulerZ);
	static Eigen::Vector3f& ScaleVector(Eigen::Vector3f& Vector, float x, float y, float z);
	void RotateWithDegrees(const Eigen::Vector3f& Rotation);
	void RotateWithDegrees(const TheShift::Vector3f& Rotation);
	void RotateWithDegrees(float x, float y, float z);
	void Scale(float Scaling);

	void SetTranslation(const Eigen::Vector3f& Translation);
	void SetTranslation(float x, float y, float z);
	void SetRotation(Eigen::Vector3f& Euler);
	void SetRotation(const Eigen::Quaternionf& Quat);
	void SetRotationWithDegrees(float x, float y, float z);
	void SetRotationWithDegrees(const Eigen::Vector3f& Degrees);
	void SetScale(float Value);

	void AddTranslation(const Eigen::Vector3f& Translation);

	Eigen::Vector3f GetTranslation() const;
	//const Eigen::Vector3f& GetTranslation();
	Eigen::Quaternionf GetRotation() const;
	Eigen::Vector3f GetEuler() const;
	Eigen::Vector3f GetDegrees() const;
	float GetScale() const;
	Eigen::Vector3f GetLookVector() const;
	Eigen::Vector3f GetRightVector() const;
	const Eigen::Transform<float, 3, Eigen::Affine>& GetData() const;
	const Eigen::Transform<float, 3, Eigen::Affine>& GetWorldTransformData() const;

	Eigen::Vector3f GetWorldTranslation();
	//Eigen::Vector3f GetLastTranslation() const;
	Eigen::Quaternionf GetWorldRotation();
	Eigen::Vector3f GetWorldEuler();
	Eigen::Vector3f GetWorldDegrees();

	Eigen::Vector3f ApplyTransform(Eigen::Vector3f& Origin) const;
	void SetTransform(const Eigen::Transform<float, 3, Eigen::Affine>& NewTransform);

	void SetParent(const Transform* Other);
	void AddChild(Transform* Other);
	void UpdateWorldTransform(bool RotationIncluded);

	void RegisterBoundingVolumeRearranger(std::function<void(BoundingVolume*)> Rearranger);
	void RegisterBoundingVolume(BoundingVolume* ChildVolume);

private:
	float Scaling;
	Eigen::Transform<float, 3, Eigen::Affine> LocalTransform;
	Eigen::Transform<float, 3, Eigen::Affine> WorldTransform;
	//Eigen::Transform<float, 3, Eigen::Affine> LastTransform;
	Eigen::Vector3f WorldTranslation;
	Eigen::Matrix3f WorldRotationMatrix;
	bool WorldRotationMatrixUpdateNeeded = false;

	template <int VertexNum>
	Eigen::MatrixXf ApplyTransform(float* ArrVertices) const
	{
		Eigen::MatrixXf Vertices = Eigen::Map<Eigen::Matrix<float, 3, VertexNum>>(ArrVertices);
		return LocalTransform * Vertices.colwise().homogeneous();
	}

	const Transform* Parent = nullptr;
	std::vector<Transform*> Childs;
	std::vector<BoundingVolume*> BoundingVolumes;
	std::function<void(BoundingVolume*)> BoundingVolumeRearranger;
};
}

