﻿#include "Heightmap.h"
#include <fstream>
#include "Game/System/MapData.h"

namespace TheShift
{
Heightmap::Heightmap(const std::vector<std::vector<Eigen::Vector3f>>& verts) : Verts(verts)
{

}

const std::vector<std::vector<Eigen::Vector3f>>& Heightmap::GetVertices() const
{
	return Verts;
}
}

