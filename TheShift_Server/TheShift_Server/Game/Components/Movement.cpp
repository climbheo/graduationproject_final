﻿#include "Movement.h"
#include "Protocol/Log.h"
//#include "common.h"

namespace TheShift
{
Movement::Movement(Transform* worldTransform, const StatusType* currentStatus, Stats* stat) :
	WorldTransform(worldTransform), CurrentStatus(currentStatus), Stat(stat)
{
	CurrentAcceleration.setZero();
	CurrentVelocity.setZero();
	InputDirection.setZero();
}

Movement& Movement::operator=(const Movement& Other)
{
	if (this == &Other)
		return *this;
	CurrentVelocity = Other.CurrentVelocity;
	CurrentAcceleration = Other.CurrentAcceleration;
	CurrentStatus = Other.CurrentStatus;
	CurrentMoveDirection = Other.CurrentMoveDirection;
	//TODO: 자식, 부모정보는 옮기지 않고 행렬값만 옮긴다.
	WorldTransform->SetTransform(Other.WorldTransform->GetWorldTransformData());
	LastUpdatedTransform = Other.LastUpdatedTransform;
	LastVelocity = Other.LastVelocity;
	InputDirection = Other.InputDirection;
	InputAcceleration = Other.InputAcceleration;
	InputPressed = Other.InputPressed;
	InputLocked = Other.InputLocked;
	return *this;
}

Eigen::Vector3f Movement::GetCurrentVelocity() const
{
	return CurrentVelocity;
}

Eigen::Vector3f Movement::GetCurrentAcceleration() const
{
	return CurrentAcceleration;
}

void Movement::Update(float deltaTime)
{
	if(InputPressed)
	{
		CurrentAcceleration = InputAcceleration;
		if(CurrentAcceleration.z() > 1.f)
		{
			LOG_ERROR("InputAcceleration has z value!!!!!!!!!");
		}
	}

	CurrentVelocity += CurrentAcceleration * deltaTime;
	Eigen::Vector3f PrevVelocity = CurrentVelocity;
	
	float Friction = COEFFICIENT * Stat->Mass * 980.f;
	Eigen::Vector3f FrictionAcc = Eigen::Vector3f(CurrentVelocity.x(), CurrentVelocity.y(), 0.f);
	if(FrictionAcc.squaredNorm() > FLT_EPSILON)
	{
		FrictionAcc = (-FrictionAcc.normalized() * Friction);
	}
	FrictionAcc.z() = 0.f;

	// FrictionAcc 방향과 velocity의 방향이 같고
	// 
	//CurrentAcceleration += FrictionAcc;
	Eigen::Vector3f AfterVel = CurrentVelocity + FrictionAcc * deltaTime;
	//CurrentVelocity += CurrentAcceleration * deltaTime;

	
	Eigen::Vector3f LookVector = WorldTransform->GetLookVector();

	if(AfterVel.squaredNorm() < FLT_EPSILON)
	{
		CurrentVelocity = { 0.f, 0.f, 0.f };
	}
	else
	{
		if(AfterVel.x() * PrevVelocity.x() < 0.f)
		{
			AfterVel.x() = 0.f;
		}
		if(AfterVel.y() * PrevVelocity.y() < 0.f)
		{
			AfterVel.y() = 0.f;
		}

		CurrentVelocity = AfterVel;
	}

	if(CurrentVelocity.squaredNorm() > Stat->ForwardRunSpeed* Stat->ForwardRunSpeed)
	{
		CurrentVelocity.normalize();
		CurrentVelocity *= Stat->ForwardRunSpeed;
	}

	if(Gravity)
	{
		CurrentVelocity.z() -= 980.f * deltaTime;
	}
	else
	{
		CurrentVelocity.z() = 0.f;
	}
	Eigen::Vector3f TranslateValue = CurrentVelocity * deltaTime;

	WorldTransform->AddTranslation(TranslateValue);
	CurrentAcceleration.setZero();
}

Eigen::Quaternionf Movement::GetLastUpdateRotation() const
{
	return LastUpdatedTransform.GetRotation();
}

Eigen::Vector3f Movement::GetLastUpdateLocation() const
{
	return LastUpdatedTransform.GetTranslation();
}

Eigen::Vector3f Movement::GetLastVelocity() const
{
	return LastVelocity;
}

Eigen::Vector3f Movement::GetInputDirection() const
{
	return InputDirection;
}

void Movement::SetInputDirection(Eigen::Vector3f& NewDirection, Serializables::Input& RecentInput)
{
	//이 함수를 호출한다는건 InputPressed이란 소리므로 InputPressed도 true로 만들어준다.
	InputPressed = true;
	InputDirection = NewDirection;
	Serializables::Input input;
	input.CamRotation = RecentInput.CamRotation;
	input.ControlRotation = RecentInput.ControlRotation;
	if (InputDirection.y() > 0.f)
	{
		input.InputValue = InputType::Right_Pressed;
		ApplyInput(input,false);
	}
	else if (InputDirection.y() < 0.f)
	{
		input.InputValue = InputType::Left_Pressed;
		ApplyInput(input, false);
	}
	if (InputDirection.x() > 0.f)
	{
		input.InputValue = InputType::Forward_Pressed;
		ApplyInput(input, false);
	}
	else if (InputDirection.x() < 0.f)
	{
		input.InputValue = InputType::Back_Pressed;
		ApplyInput(input, false);
	}
}

void Movement::SetVelocity(Eigen::Vector3f& NewVelocity)
{
	CurrentVelocity = NewVelocity;
}

void Movement::ApplyInput(Serializables::Input Input, bool IsRotate)
{
	//input이 잠기면 들어온 InputDirection변수를 변경은 하지만 그 방향으로 움직이진 않는다.
	//잠겨있는 상태에선 Pressed는 반영이 되지만 Released는 반영되지 않는다
	Eigen::Vector3f LookAt = WorldTransform->GetLookVector();
	Eigen::Vector3f UpVector(0.f, 0.f, 1.f);

	if(Input.InputValue != InputType::Camera_Rotation)
	{
		switch(Input.InputValue)
		{
		case InputType::Left_Pressed:
			if (InputDirection.y() > -FLT_EPSILON)
			{
				LOG_DEBUG("LEFT_SEVER_RECEIVED");
				InputDirection.y() = -1.f;
				if (!InputLocked)
					InputPressed = true;
			}
			else
				LOG_DEBUG("LEFT_SERVER_AVOID");
			break;

		case InputType::Left_Released:
			LOG_DEBUG("LEFT_SEVER_RELEASED");
			if (InputDirection.y() < -FLT_EPSILON)
			{
				if (!InputLocked)
					InputDirection.y() = 0.f;
			}
			break;

		case InputType::Right_Pressed:

			if(InputDirection.y() < FLT_EPSILON)
			{
				LOG_DEBUG("RIGHT_SEVER_RECEIVED");
				InputDirection.y() = 1.f;
				if (!InputLocked)
					InputPressed = true;
			}
			else
				LOG_DEBUG("RIGHT_SERVER_AVOID");
			break;

		case InputType::Right_Released:
			LOG_DEBUG("RIGHT_SEVER_RELEASED");
			if (InputDirection.y() > FLT_EPSILON)
			{
				if (!InputLocked)
					InputDirection.y() = 0.f;
			}
			break;

		case InputType::Forward_Pressed:

			if(InputDirection.x() < FLT_EPSILON)
			{
				LOG_DEBUG("Forward_SEVER_RECEIVED");
				InputDirection.x() = 1.f;
				if (!InputLocked)
					InputPressed = true;
			}
			else
				LOG_DEBUG("Forward_SERVER_AVOID");
			break;

		case InputType::Forward_Released:
			LOG_DEBUG("FORWARD_SEVER_RELEASED");
			if (InputDirection.x() > FLT_EPSILON)
			{
				if (!InputLocked)
					InputDirection.x() = 0.f;
			}
			break;

		case InputType::Back_Pressed:
			if(InputDirection.x() > -FLT_EPSILON)
			{
				LOG_DEBUG("Back_SEVER_RECEIVED");
				InputDirection.x() = -1.f;
				if (!InputLocked)
					InputPressed = true;
			}
			else
				LOG_DEBUG("Back_SERVER_AVOID");
			break;

		case InputType::Back_Released:
			LOG_DEBUG("BACK_SEVER_RELEASED");
			if (InputDirection.x() < -FLT_EPSILON)
			{
				if (!InputLocked)
					InputDirection.x() = 0.f;
			}
			break;

		default:
			break;
		}
	}

	// 이동 방향이 zero vector면 input이 없는것으로 간주
	if(InputDirection.isZero() == true)
	{
		InputPressed = false;
		InputAcceleration.setZero();
	}

	// input이 있을때만 가속도를 설정한다.
	if(InputPressed == true)
	{
		if (IsRotate)
		{
			WorldTransform->SetRotationWithDegrees(0.f, 0.f, Input.ControlRotation.Z);
			//LOG_DEBUG("Input Rotating");
		}
		InputDirection.normalize();

		auto NewQuat =
			Eigen::AngleAxisf(0.f, Eigen::Vector3f::UnitX()) *
			Eigen::AngleAxisf(0.f, Eigen::Vector3f::UnitY()) *
			Eigen::AngleAxisf((Input.CamRotation.Z) * M_PI / 180.f, Eigen::Vector3f::UnitZ());
		//예전엔 Input.CamRotation.Z-360.f를 해줫는데 이유는 잘모르겠음 일단 주석만 남겨둠.
		InputAcceleration /*CurrentAcceleration*/ = NewQuat * InputDirection * Stat->ForwardRunAcceleration;
	}

	//if (InputDirection.x() > FLT_EPSILON)
	//{
	//    // if(CurrentStatus | Run)
	//    MoveDirection* Stat.ForwardRunSpeed;
	//}
	//else if (InputDirection.x() < FLT_EPSILON)
	//{
	//    MoveDirection * Stat.Back
	//}

}

void Movement::AddImpulse(Eigen::Vector3f& impulse, float deltaTime)
{
	//CurrentAcceleration = impulse * Stat.Mass;
	CurrentVelocity += impulse * Stat->Mass * deltaTime;

	/*CurrentVelocity += CurrentAcceleration * deltaTime;*/
}

void Movement::AddForce(float x, float y, float z, float deltaTime)
{}

// Target 방향으로 이동
void Movement::SetDestination(Eigen::Vector3f& Target, bool IsChase)
{
	// Input이 잠겨있으면 움직이지 않는다.
	if(InputLocked)
	{
		return;
	}

	Eigen::Vector3f CurrentLocation = WorldTransform->GetWorldTranslation();
	Eigen::Vector3f Dir = (Target - CurrentLocation).normalized();
	if(IsChase)
		CurrentAcceleration = Dir * Stat->ForwardRunAcceleration;

	// 해당 방향으로 z축 회전
	float Dot = Dir.dot(Eigen::Vector3f(1.f, 0.f, 0.f));
	// 0~180 도 사이의 값 0~1
	float AcosAngle = std::acos(Dot);

	Eigen::Vector3f Cross = Dir.cross(Eigen::Vector3f(1.f, 0.f, 0.f));
	float TurnAngle;

	if(Cross.z() > 0.f)
	{
		TurnAngle = -AcosAngle;
	}
	else
	{
		TurnAngle = AcosAngle;
	}

	Eigen::Vector3f Euler(0.f, 0.f, TurnAngle);
	WorldTransform->SetRotation(Euler);
}

void Movement::SetGravity(bool Value)
{
	Gravity = Value;
}

// 해당 Actor가 스스로 움직일 수 있도록 한다.
void Movement::LockInput()
{
	LOG_DEBUG("LOCKED!!");

	//Init
	InputDirection.x() = 0.f;
	InputDirection.y() = 0.f;
	InputPressed = false;
	InputAcceleration.setZero();

	InputLocked = true;
}

// 해당 Actor가 스스로 움직이는것을 제한한다.
void Movement::UnlockInput()
{
	InputDirection.x() = 0.f;
	InputDirection.y() = 0.f;
	InputPressed = false;
	InputAcceleration.setZero();

	InputLocked = false;
	LOG_DEBUG("UNLOCKED!!");
}

}

