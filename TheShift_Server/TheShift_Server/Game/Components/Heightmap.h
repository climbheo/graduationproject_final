﻿#pragma once

#include "Protocol/Types.h"

namespace TheShift
{
class Heightmap
{
public:
	Heightmap(const std::vector<std::vector<Eigen::Vector3f>>& verts);

	const std::vector<std::vector<Eigen::Vector3f>>& GetVertices() const;

private:
	const std::vector<std::vector<Eigen::Vector3f>>& Verts;
};
}

