﻿#include "CollisionResolver.h"
#include <cmath>
#include "Game/System/GameState.h"
#include "Game/System/MapData.h"
#include "Game/Actors/Actor.h"
#include "Game/Actors/Trigger.h"
#include "Protocol/Log.h"
#include "Game/Components/Heightmap.h"

namespace TheShift
{
CollisionResolver::CollisionResolver(GameState* state)
{
	State = state;
}

void CollisionResolver::ResolveCollisions(float deltaTime)
{
#ifdef TSSERVER
	if (VolumesToBeInitialized.empty() == false)
	{
		for (auto& Volume : VolumesToBeInitialized)
		{
			// 새로 생긴 Volume을 재정렬시켜준다.
			Volume->CalculateAlignedExtent();
			RearrangeVolume(Volume.get());
		}
		VolumesToBeInitialized.clear();
	}
#endif

	for (auto& ActorPair : *(State->GetActors()))
	{
		// 움직이는 객체만 Heightmap 충돌을 시킨다.
		Resolve(ActorPair.second, deltaTime);
	}

	//RearrangeByAxisX();
	//RearrangeByAxisY();
	//RearrangeByAxisZ();

	for (auto& VolumePair : BoundingVolumes)
	{
		if (VolumePair.second == nullptr)
		{
			//spdlog::debug("Volume is nullptr!!");
			LOG_DEBUG("Volume is nullptr!!");
			continue;
		}
		//ResolveBoundingVolume(Volume, deltaTime);
	}

#ifdef TSSERVER

#endif

	ResolveBoundingVolumes(deltaTime);

#ifdef TSSERVER

#endif
}

void CollisionResolver::CreateBoundingVolumes(std::shared_ptr<Actor> OwnerActor, std::vector<BoundingVolumeInfo>& BoundInfo)
{
	for (auto& Info : BoundInfo)
	{
		std::shared_ptr<BoundingVolume> Volume = nullptr;
		if (Info.BoundingVolumeType_ == BoundingVolumeType::OBB)
		{
			if (Info.RelativeRotation.IsZero())
			{
				if (OwnerActor->GetIsMovable() == false)
				{
					if (OwnerActor->GetTransform().GetWorldDegrees().isZero())
					{
						Info.BoundingVolumeType_ = BoundingVolumeType::AABB;
					}
				}
			}
		}

		switch (Info.BoundingVolumeType_)
		{
		case BoundingVolumeType::AABB:
		{
			auto AABBVolume = CreateCollider<AABB>();

			// Init
			AABBVolume->SetRelativeLocation(Info.RelativeLocation);
			AABBVolume->SetRelativeRotation(Info.RelativeRotation);
			AABBVolume->SetExtent(Info.ScaledExtent);
			AABBVolume->SetParent(OwnerActor);

			Volume = AABBVolume;
			break;
		}

		case BoundingVolumeType::OBB:
		{
			auto OBBVolume = CreateCollider<OBB>();

			// Init
			OBBVolume->SetRelativeLocation(Info.RelativeLocation);
			OBBVolume->SetRelativeRotation(Info.RelativeRotation);
			OBBVolume->SetExtent(Info.ScaledExtent);
			OBBVolume->SetParent(OwnerActor);

			Volume = OBBVolume;
			break;
		}

		case BoundingVolumeType::Capsule:
		{
			auto CapsuleVolume = CreateCollider<BoundingCapsule>();

			// Init
			CapsuleVolume->SetRelativeLocation(Info.RelativeLocation);
			CapsuleVolume->SetRelativeRotation(Info.RelativeRotation);
			CapsuleVolume->SetHalfHeight(Info.HalfHeight);
			CapsuleVolume->SetRadius(Info.Radius);
			CapsuleVolume->SetParent(OwnerActor);

			// 실질적 충돌체크를 담당 할 Sphere들을 생성한다.
			CapsuleVolume->CreateDerivedSpheres();

			// Capsule을 부모로 설정한다. 
			for (auto& Sphere : CapsuleVolume->Spheres)
			{
				Sphere->SetAsCollider();
				Sphere->SetParent(CapsuleVolume);
				RegisterCollider(Sphere);
			}

			InitDerivedSpheres(CapsuleVolume->Spheres, Info.HalfHeight, Info.Radius);

			Volume = CapsuleVolume;
			break;
		}

		case BoundingVolumeType::Sphere:
		{
			auto SphereVolume = CreateCollider<BoundingSphere>();

			// Init
			SphereVolume->SetRelativeLocation(Info.RelativeLocation);
			SphereVolume->SetRelativeRotation(Info.RelativeRotation);
			SphereVolume->SetRadius(Info.Radius);
			SphereVolume->SetParent(OwnerActor);

			Volume = SphereVolume;
			break;
		}

		default:
			break;
		}

		//TODO: 다른 Volume의 자식 Volume을 추가해야 하기 때문에 Json에 부모 정보도 필요하다.
		// Root volume이라면
	}
}

// 이미 생성된 Bounding Volume을 등록한다.
void CollisionResolver::RegisterTrigger(std::shared_ptr<BoundingVolume> NewTrigger)
{
	UID Id = GetAvailableUID();
	BoundingVolumes.insert(std::make_pair(Id, NewTrigger));
	NewTrigger->SetCollisionResolver(this);
	NewTrigger->SetId(Id);

	NewTrigger->SetAsTrigger();
	//SortedByAxisX.emplace_back(NewTrigger);
	//SortedByAxisY.emplace_back(NewTrigger);
	//SortedByAxisZ.emplace_back(NewTrigger);
	//SAPAxisXSets.insert(std::make_pair(NewTrigger, std::set<std::shared_ptr<BoundingVolume>>()));
	//SAPAxisYSets.insert(std::make_pair(NewTrigger, std::set<std::shared_ptr<BoundingVolume>>()));
	//SAPAxisZSets.insert(std::make_pair(NewTrigger, std::set<std::shared_ptr<BoundingVolume>>()));

	//NewTrigger->RegisterBoundingVolumeRearranger(std::bind(&CollisionResolver::RearrangeVolume, this, std::placeholders::_1));

#ifdef _DEBUG
   //LOG_DEBUG("%s Trigger registered.", typeid(*NewTrigger).name());
#endif

#ifdef TSCLIENT
   //switch(NewTrigger->GetVolumeType())
   //{
   //case BoundingVolumeType::AABB:
   //   LOG_DEBUG("AABB Trigger registered.");
   //   break;

   //case BoundingVolumeType::OBB:
   //   LOG_DEBUG("OBB Trigger registered.");
   //   break;

   //case BoundingVolumeType::Capsule:
   //   LOG_DEBUG("Capsule Trigger registered.");
   //   break;

   //case BoundingVolumeType::Sphere:
   //   LOG_DEBUG("Sphere Trigger registered.");
   //   break;

   //default:
   //   break;
   //}
#endif
}

// 이미 생성된 Bounding Volume을 등록한다.
void CollisionResolver::RegisterCollider(std::shared_ptr<BoundingVolume> NewCollider)
{
	UID Id = GetAvailableUID();
	BoundingVolumes.insert(std::make_pair(Id, NewCollider));
	NewCollider->SetCollisionResolver(this);
	NewCollider->SetId(Id);

	NewCollider->SetAsCollider();
	//SortedByAxisX.emplace_back(NewCollider);
	//SortedByAxisY.emplace_back(NewCollider);
	//SortedByAxisZ.emplace_back(NewCollider);
	//SAPAxisXSets.insert(std::make_pair(NewCollider, std::set<std::shared_ptr<BoundingVolume>>()));
	//SAPAxisYSets.insert(std::make_pair(NewCollider, std::set<std::shared_ptr<BoundingVolume>>()));
	//SAPAxisZSets.insert(std::make_pair(NewCollider, std::set<std::shared_ptr<BoundingVolume>>()));

	//NewCollider->RegisterBoundingVolumeRearranger(std::bind(&CollisionResolver::RearrangeVolume, this, std::placeholders::_1));

#ifdef _DEBUG
   //LOG_DEBUG("%s Collider registered.", typeid(*NewCollider).name());
#endif

#ifdef TSCLIENT
   // 클라이언트는 rtti옵션을 끈 상태 (/GR-)이기 때문에 typeid 못씀
	switch (NewCollider->GetVolumeType())
	{
	case BoundingVolumeType::AABB:
		LOG_DEBUG("AABB Collider registered.");
		break;

	case BoundingVolumeType::OBB:
		LOG_DEBUG("OBB Collider registered.");
		break;

	case BoundingVolumeType::Capsule:
		LOG_DEBUG("Capsule Collider registered.");
		break;

	case BoundingVolumeType::Sphere:
		LOG_DEBUG("Sphere Collider registered.");
		break;

	default:
		break;
	}
#endif
}

// 해당 Id를 가진 Volume의 shared_ptr을 return한다.
// 해당 Id를 가진 Volume이 없다면 nullptr을 return.
std::shared_ptr<BoundingVolume> CollisionResolver::GetBoundingVolume(UID Id)
{
	auto TargetVolumeIter = BoundingVolumes.find(Id);
	if (TargetVolumeIter != BoundingVolumes.end())
	{
		return TargetVolumeIter->second;
	}
	else
		return nullptr;
}

// BoundingVolume이 저장된 map에서 삭제 후 저장된 index들도 삭제한다.
void CollisionResolver::RemoveBoundingVolume(UID Id)
{
	BoundingVolumes.erase(Id);
}

// 저장된 바운딩 볼륨들을 모두 정리한다.
void CollisionResolver::Reset()
{
	IndexCount = 0;
	BoundingVolumes.clear();
	VolumesToBeInitialized.clear();
}

//// X의 최솟값의 오름차순으로 정렬한다.
//void CollisionResolver::RearrangeByAxisX()
//{
//   std::sort(SortedByAxisX.begin(), SortedByAxisX.end(), [](std::shared_ptr<BoundingVolume> a, std::shared_ptr<BoundingVolume> b) {
//      return a->GetMinPoint().x() < b->GetMinPoint().x();
//           });
//}
//
//// Y의 최솟값의 오름차순으로 정렬한다.
//void CollisionResolver::RearrangeByAxisY()
//{
//   std::sort(SortedByAxisY.begin(), SortedByAxisY.end(), [](std::shared_ptr<BoundingVolume> a, std::shared_ptr<BoundingVolume> b) {
//      return a->GetMinPoint().y() < b->GetMinPoint().y();
//           });
//}
//
//// Z의 최솟값의 오름차순으로 정렬한다.
//void CollisionResolver::RearrangeByAxisZ()
//{
//   std::sort(SortedByAxisZ.begin(), SortedByAxisZ.end(), [](std::shared_ptr<BoundingVolume> a, std::shared_ptr<BoundingVolume> b) {
//      return a->GetMinPoint().z() < b->GetMinPoint().z();
//           });
//}

// 해당 BoundingVolume을 재정렬한다.
void CollisionResolver::RearrangeVolume(BoundingVolume* TargetVolume)
{
	RearrangeVolumeInAxis(0, TargetVolume);
	RearrangeVolumeInAxis(1, TargetVolume);
	RearrangeVolumeInAxis(2, TargetVolume);
}

void CollisionResolver::RearrangeVolumeInAxis(int Axis, BoundingVolume* TargetVolume)
{
	//std::vector<std::shared_ptr<BoundingVolume>>* TargetVector = nullptr;
	//std::map<std::shared_ptr<BoundingVolume>, std::set<std::shared_ptr<BoundingVolume>>>* TargetSAPSets = nullptr;
	//// X축
	//if(Axis == 0)
	//{
	//   TargetVector = &SortedByAxisX;
	//   TargetSAPSets = &SAPAxisXSets;
	//}
	//// Y축
	//else if(Axis == 1)
	//{
	//   TargetVector = &SortedByAxisY;
	//   TargetSAPSets = &SAPAxisYSets;
	//}
	//// Z축
	//else if(Axis == 2)
	//{
	//   TargetVector = &SortedByAxisZ;
	//   TargetSAPSets = &SAPAxisZSets;
	//}
	//else
	//   return;

	//auto MyWorldTranslation = TargetVolume->GetTransform().GetWorldTranslation();
	//auto MyExtent = TargetVolume->GetAlignedExtent();
	//Eigen::Vector3f Min = MyWorldTranslation - MyExtent;
	//Eigen::Vector3f Max = MyWorldTranslation + MyExtent;
	//float MyMinValue;
	//float MyMaxValue;
	//if(Axis == 0)
	//{
	//   MyMinValue = Min.x();
	//   MyMaxValue = Max.x();
	//}
	//else if(Axis == 1)
	//{
	//   MyMinValue = Min.y();
	//   MyMaxValue = Max.y();
	//}
	//else if(Axis == 2)
	//{
	//   MyMinValue = Min.z();
	//   MyMaxValue = Max.z();
	//}

	//auto TargetIter = std::find_if(TargetVector->begin(), TargetVector->end(), [TargetVolume](std::shared_ptr<BoundingVolume> a) {return a.get() == TargetVolume; });
	//auto Iter = TargetIter;

	//// 다음 원소들
	//Iter++;
	//while(Iter != TargetVector->end())
	//{
	//   auto OtherTranslation = (*Iter)->GetTransform().GetWorldTranslation();
	//   auto OtherExtent = (*Iter)->GetAlignedExtent();
	//   auto OtherMin = OtherTranslation - OtherExtent;
	//   float OtherMinValue;

	//   if(Axis == 0)
	//   {
	//      OtherMinValue = OtherMin.x();
	//   }
	//   else if(Axis == 1)
	//   {
	//      OtherMinValue = OtherMin.y();
	//   }
	//   else if(Axis == 2)
	//   {
	//      OtherMinValue = OtherMin.z();
	//   }

	//   // 다음 Volume이 값이 더 작다면 바꿔준다.
	//   if(MyMinValue > OtherMinValue)
	//   {
	//      //// OtherMin보다 큰데 OtherMax보다 작으면 겹친것
	//      //if(MyMinValue < OtherMaxValue)
	//      //{
	//      //   // 없으면 추가해준다.
	//      //   // 단, 같은 Actor의 volume이 아니라면
	//      //   if((*TargetIter)->GetParentActor() != (*Iter)->GetParentActor())
	//      //   {
	//      //      if((*TargetSAPSets)[*TargetIter].count(*Iter) == 0)
	//      //      {
	//      //         (*TargetSAPSets)[*TargetIter].insert(*Iter);
	//      //         LOG_INFO("Volume added.");
	//      //      }
	//      //      if((*TargetSAPSets)[*Iter].count(*TargetIter) == 0)
	//      //      {
	//      //         (*TargetSAPSets)[*Iter].insert(*TargetIter);
	//      //         LOG_INFO("Volume added.");
	//      //      }
	//      //   }
	//      //}
	//      //// MyMin과 OtherMin과 OtherMax보다 크면 겹치지 않은것
	//      //else
	//      //{
	//      //   // 있으면 제거해준다.
	//      //   if((*TargetSAPSets)[*TargetIter].count(*Iter) != 0)
	//      //   {
	//      //      (*TargetSAPSets)[*TargetIter].erase(*Iter);
	//      //      LOG_INFO("Volume removed.");
	//      //   }
	//      //   if((*TargetSAPSets)[*Iter].count(*TargetIter) != 0)
	//      //   {
	//      //      (*TargetSAPSets)[*Iter].erase(*TargetIter);
	//      //      LOG_INFO("Volume removed.");
	//      //   }
	//      //}

	//      // 자리를 바꿔줌
	//      std::swap(*Iter, *TargetIter);
	//      std::swap(Iter, TargetIter);
	//   }
	//   else
	//      break;
	//   //else
	//   //{
	//   //   // OtherMin보다 작은데 MyMaxValue가 OtherMinValue보다 크면 겹친것
	//   //   if(MyMaxValue > OtherMinValue)
	//   //   {
	//   //      // 없으면 추가해준다.
	//   //      // 단, 같은 Actor의 volume이 아니라면
	//   //      if((*TargetIter)->GetParentActor() != (*Iter)->GetParentActor())
	//   //      {
	//   //         if((*TargetSAPSets)[*TargetIter].count(*Iter) == 0)
	//   //         {
	//   //            (*TargetSAPSets)[*TargetIter].insert(*Iter);
	//   //            LOG_INFO("Volume added.");
	//   //         }
	//   //         if((*TargetSAPSets)[*Iter].count(*TargetIter) == 0)
	//   //         {
	//   //            (*TargetSAPSets)[*Iter].insert(*TargetIter);
	//   //            LOG_INFO("Volume added.");
	//   //         }
	//   //      }
	//   //   }
	//   //   // MyMin과 MyMax가 모두 OtherMin보다 작으면 겹치지 않은것
	//   //   else
	//   //   {
	//   //      // 있으면 제거해준다.
	//   //      if((*TargetSAPSets)[*TargetIter].count(*Iter) != 0)
	//   //      {
	//   //         (*TargetSAPSets)[*TargetIter].erase(*Iter);
	//   //         LOG_INFO("Volume removed.");
	//   //      }
	//   //      if((*TargetSAPSets)[*Iter].count(*TargetIter) != 0)
	//   //      {
	//   //         (*TargetSAPSets)[*Iter].erase(*TargetIter);
	//   //         LOG_INFO("Volume removed.");
	//   //      }
	//   //   }
	//   //   break;
	//   //}

	//   // 다음 Iter 비교
	//   Iter = TargetIter;
	//   Iter++;
	//}

	//// 이전 원소들
	//Iter = TargetIter;
	//while(TargetIter != TargetVector->begin())
	//{
	//   Iter = TargetIter;
	//   Iter--;
	//   auto OtherTranslation = (*Iter)->GetTransform().GetWorldTranslation();
	//   auto OtherExtent = (*Iter)->GetAlignedExtent();
	//   auto OtherMin = OtherTranslation - OtherExtent;
	//   float OtherMinValue;

	//   if(Axis == 0)
	//   {
	//      OtherMinValue = OtherMin.x();
	//   }
	//   else if(Axis == 1)
	//   {
	//      OtherMinValue = OtherMin.y();
	//   }
	//   else if(Axis == 2)
	//   {
	//      OtherMinValue = OtherMin.z();
	//   }

	//   // 이전 Volume이 값이 더 크다면 바꿔준다.
	//   if(MyMinValue < OtherMinValue)
	//   {
	//      //// MyMin이 OtherMin보다 작은데 MyMax가 OtherMin보다 크면 겹친것
	//      //if(MyMaxValue > OtherMinValue)
	//      //{
	//      //   // 없으면 추가해준다.
	//      //   // 단, 같은 Actor의 volume이 아니라면
	//      //   if((*TargetIter)->GetParentActor() != (*Iter)->GetParentActor())
	//      //   {
	//      //      if((*TargetSAPSets)[*TargetIter].count(*Iter) == 0)
	//      //      {
	//      //         (*TargetSAPSets)[*TargetIter].insert(*Iter);
	//      //         LOG_INFO("Volume added.");
	//      //      }
	//      //      if((*TargetSAPSets)[*Iter].count(*TargetIter) == 0)
	//      //      {
	//      //         (*TargetSAPSets)[*Iter].insert(*TargetIter);
	//      //         LOG_INFO("Volume added.");
	//      //      }
	//      //   }
	//      //}
	//      //// MyMin과 MyMax모두 OtherMin보다 작으면 겹치지 않은것
	//      //else
	//      //{
	//      //   // 있으면 제거해준다.
	//      //   if((*TargetSAPSets)[*TargetIter].count(*Iter) != 0)
	//      //   {
	//      //      (*TargetSAPSets)[*TargetIter].erase(*Iter);
	//      //      LOG_INFO("Volume removed.");
	//      //   }
	//      //   if((*TargetSAPSets)[*Iter].count(*TargetIter) != 0)
	//      //   {
	//      //      (*TargetSAPSets)[*Iter].erase(*TargetIter);
	//      //      LOG_INFO("Volume removed.");
	//      //   }
	//      //}
	//      // 자리를 바꿔줌
	//      std::swap(*Iter, *TargetIter);
	//      std::swap(Iter, TargetIter);
	//   }
	//   else
	//      break;
	//   //else
	//   //{
	//   //   // MyMin이 OtherMin보다 큰데 MyMin이 OtherMax보다 작으면 겹친것
	//   //   if(OtherMaxValue > MyMinValue)
	//   //   {
	//   //      // 없으면 추가해준다.
	//   //      // 단, 같은 Actor의 volume이 아니라면
	//   //      if((*TargetIter)->GetParentActor() != (*Iter)->GetParentActor())
	//   //      {
	//   //         if((*TargetSAPSets)[*TargetIter].count(*Iter) == 0)
	//   //         {
	//   //            (*TargetSAPSets)[*TargetIter].insert(*Iter);
	//   //            LOG_INFO("Volume added.");
	//   //         }
	//   //         if((*TargetSAPSets)[*Iter].count(*TargetIter) == 0)
	//   //         {
	//   //            (*TargetSAPSets)[*Iter].insert(*TargetIter);
	//   //            LOG_INFO("Volume added.");
	//   //         }
	//   //      }
	//   //   }
	//   //   // MyMin이 OtherMin과 OtherMax 모두보다 크면 겹치지 않은것
	//   //   else
	//   //   {
	//   //      // 있으면 제거해준다.
	//   //      if((*TargetSAPSets)[*TargetIter].count(*Iter) != 0)
	//   //      {
	//   //         (*TargetSAPSets)[*TargetIter].erase(*Iter);
	//   //         LOG_INFO("Volume removed.");
	//   //      }
	//   //      if((*TargetSAPSets)[*Iter].count(*TargetIter) != 0)
	//   //      {
	//   //         (*TargetSAPSets)[*Iter].erase(*TargetIter);
	//   //         LOG_INFO("Volume removed.");
	//   //      }
	//   //   }
	//   //   break;
	//   //}
	//}

	//// 이후 Volume들
	//auto CompIter = TargetIter;
	//while(true)
	//{
	//   CompIter++;
	//   if(CompIter == TargetVector->end())
	//      break;

	//   auto OtherTranslation = (*CompIter)->GetTransform().GetWorldTranslation();
	//   auto OtherExtent = (*CompIter)->GetAlignedExtent();
	//   auto OtherMin = OtherTranslation - OtherExtent;
	//   auto OtherMax = OtherTranslation + OtherExtent;
	//   float OtherMinValue;
	//   float OtherMaxValue;

	//   if(Axis == 0)
	//   {
	//      OtherMinValue = OtherMin.x();
	//      OtherMaxValue = OtherMax.x();
	//   }
	//   else if(Axis == 1)
	//   {
	//      OtherMinValue = OtherMin.y();
	//      OtherMaxValue = OtherMax.y();
	//   }
	//   else if(Axis == 2)
	//   {
	//      OtherMinValue = OtherMin.z();
	//      OtherMaxValue = OtherMax.z();
	//   }

	//   if(MyMaxValue > OtherMinValue)
	//   {
	//      // 없으면 추가해준다.
	//      // 단, 같은 Actor의 volume이 아니라면
	//      if((*TargetIter)->GetParentActor() != (*CompIter)->GetParentActor())
	//      {
	//         if((*TargetSAPSets)[*TargetIter].count(*CompIter) == 0)
	//         {
	//            (*TargetSAPSets)[*TargetIter].insert(*CompIter);
	//            //LOG_INFO("Volume added.");
	//         }
	//         if((*TargetSAPSets)[*CompIter].count(*TargetIter) == 0)
	//         {
	//            (*TargetSAPSets)[*CompIter].insert(*TargetIter);
	//            //LOG_INFO("Volume added.");
	//         }
	//      }
	//   }
	//   else
	//   {
	//      // 겹치진 않지만 sap pair에 등록되어있다면 삭제해주고 continue
	//      if((*TargetSAPSets)[*TargetIter].count(*CompIter) != 0)
	//      {
	//         // 둘다 등록되어있으므로 둘다 삭제
	//         //LOG_INFO("Volume removed.");
	//         (*TargetSAPSets)[*TargetIter].erase(*CompIter);
	//         (*TargetSAPSets)[*CompIter].erase(*TargetIter);
	//         continue;
	//      }
	//      break;
	//   }
	//}

	//// 이전 Volume들
	//CompIter = TargetIter;
	//while(true)
	//{
	//   if(CompIter == (*TargetVector).begin())
	//      break;

	//   CompIter--;

	//   auto OtherTranslation = (*CompIter)->GetTransform().GetWorldTranslation();
	//   auto OtherExtent = (*CompIter)->GetAlignedExtent();
	//   auto OtherMin = OtherTranslation - OtherExtent;
	//   auto OtherMax = OtherTranslation + OtherExtent;
	//   float OtherMinValue;
	//   float OtherMaxValue;

	//   if(Axis == 0)
	//   {
	//      OtherMinValue = OtherMin.x();
	//      OtherMaxValue = OtherMax.x();
	//   }
	//   else if(Axis == 1)
	//   {
	//      OtherMinValue = OtherMin.y();
	//      OtherMaxValue = OtherMax.y();
	//   }
	//   else if(Axis == 2)
	//   {
	//      OtherMinValue = OtherMin.z();
	//      OtherMaxValue = OtherMax.z();
	//   }

	//   if(MyMinValue < OtherMaxValue)
	//   {
	//      // 없으면 추가해준다.
	//      // 단, 같은 Actor의 volume이 아니라면
	//      if((*TargetIter)->GetParentActor() != (*CompIter)->GetParentActor())
	//      {
	//         if((*TargetSAPSets)[*TargetIter].count(*CompIter) == 0)
	//         {
	//            (*TargetSAPSets)[*TargetIter].insert(*CompIter);
	//            //LOG_INFO("Volume added.");
	//         }
	//         if((*TargetSAPSets)[*CompIter].count(*TargetIter) == 0)
	//         {
	//            (*TargetSAPSets)[*CompIter].insert(*TargetIter);
	//            //LOG_INFO("Volume added.");
	//         }
	//      }
	//   }
	//   else
	//   {
	//      // 겹치진 않지만 sap pair에 등록되어있다면 삭제해주고 continue
	//      if((*TargetSAPSets)[*TargetIter].count(*CompIter) != 0)
	//      {
	//         // 둘다 등록되어있으므로 둘다 삭제
	//         //LOG_INFO("Volume removed.");
	//         (*TargetSAPSets)[*TargetIter].erase(*CompIter);
	//         (*TargetSAPSets)[*CompIter].erase(*TargetIter);
	//         continue;
	//      }
	//      break;
	//   }
	//}

	// 위에서 정렬시켜줄때 추가하면 될거같은데?
	// 이전꺼랑 이후꺼를 비교 위처럼 while로 안겹칠때까지?
	// 그리고 map에 추가?
	//for(auto& Pair : (*TargetSAPSets))
	//{
	//   auto OtherTranslation = Pair.second->GetTransform().GetWorldTranslation();
	//   auto OtherExtent = Pair.second->GetAlignedExtent();
	//   auto OtherMin = OtherTranslation - OtherExtent;
	//   auto OtherMax = OtherTranslation + OtherExtent;
	//   // 겹치는지 확인
	//}
}

float CollisionResolver::CalcHeightOfLandscape(const Eigen::Vector2f& Coord, std::shared_ptr<TheShift::Heightmap> Map)
{
	const auto& Vertices = Map->GetVertices();
	const Eigen::Vector2i Resolution = Eigen::Vector2i(Vertices[0].size(), Vertices.size());


	// TODO: 회전 먹이면 제대로 안돌아 감.
	// Heightmap내에서의 좌표를 구함.
	float LocalCoordXInLandscape = Coord.x() - Vertices[0][0].x();
	float LocalCoordYInLandscape = Coord.y() - Vertices[0][0].y();

	// Heightmap의 크기를 구함.
	float SizeX = Vertices[0][Resolution.x() - 1].x() - Vertices[0][0].x();
	float SizeY = Vertices[Resolution.y() - 1][0].y() - Vertices[0][0].y();
	assert(LocalCoordXInLandscape > -FLT_EPSILON &&
		   LocalCoordXInLandscape < SizeX + FLT_EPSILON &&
		   LocalCoordYInLandscape > -FLT_EPSILON &&
		   LocalCoordYInLandscape < SizeY + FLT_EPSILON);


	// Grid하나의 size를 구함.
	float GridSizeX = SizeX / (Resolution.x() - 1);
	float GridSizeY = SizeY / (Resolution.y() - 1);

	// 몇번째 grid에 있는지 계산함.
	int GridX = static_cast<int>(std::floor(LocalCoordXInLandscape / GridSizeX));
	int GridY = static_cast<int>(std::floor(LocalCoordYInLandscape / GridSizeY));

	// 해당 grid내에서 어느 위치에 있는지 계산함. [0~1]
	float CoordInGridX = (LocalCoordXInLandscape - GridSizeX * GridX) / GridSizeX;
	float CoordInGridY = (LocalCoordYInLandscape - GridSizeY * GridY) / GridSizeY;


	// 어느 삼각형에 있는지 구함.
	// 삼각형 어떻게 생겼는지 모름.
	// barycentric interpolation.
	float Height = 0.f;
	if (CoordInGridX < 1 - CoordInGridY)
	{
		Eigen::Vector3f Vert1(0.f, 0.f, Vertices[GridY][GridX].z());
		Eigen::Vector3f Vert2(0.f, 1.f, Vertices[GridY][GridX + 1].z());
		Eigen::Vector3f Vert3(1.f, 0.f, Vertices[GridY + 1][GridX].z());
		Eigen::Vector2f Coord(CoordInGridX, CoordInGridY);

		Height = BarryCentric(Vert1, Vert2, Vert3, Coord);
	}
	else
	{
		Eigen::Vector3f Vert1(0.f, 1.f, Vertices[GridY][GridX + 1].z());
		Eigen::Vector3f Vert2(1.f, 0.f, Vertices[GridY + 1][GridX].z());
		Eigen::Vector3f Vert3(1.f, 1.f, Vertices[GridY + 1][GridX + 1].z());
		Eigen::Vector2f Coord(CoordInGridX, CoordInGridY);

		Height = BarryCentric(Vert1, Vert2, Vert3, Coord);
	}

	return Height;
}

float CollisionResolver::CalcHeightOfLandscape(const Eigen::Vector3f& Coord, std::shared_ptr<TheShift::Heightmap> Map)
{
	Eigen::Vector2f Coord2f(Coord.x(), Coord.y());
	return CalcHeightOfLandscape(Coord2f, Map);
}

float CollisionResolver::BarryCentric(const Eigen::Vector3f& Vert1, const Eigen::Vector3f& Vert2, const Eigen::Vector3f& Vert3, const Eigen::Vector2f& Coord)
{
	float Det = (Vert2.x() - Vert3.x()) * (Vert1.y() - Vert3.y()) + (Vert3.y() - Vert2.y()) * (Vert1.x() - Vert3.x());
	float L1 = ((Vert2.x() - Vert3.x()) * (Coord.x() - Vert3.y()) + (Vert3.y() - Vert2.y()) * (Coord.y() - Vert3.x())) / Det;
	float L2 = ((Vert3.x() - Vert1.x()) * (Coord.x() - Vert3.y()) + (Vert1.y() - Vert3.y()) * (Coord.y() - Vert3.x())) / Det;
	float L3 = 1.f - L1 - L2;
	return L1 * Vert1.z() + L2 * Vert2.z() + L3 * Vert3.z();
}

/// <summary>
/// 현재 해당 position이 위치한 Heightmap을 return한다.
/// x, y좌표만 고려해서 return한다.
/// </summary>
const std::shared_ptr<TheShift::Heightmap> CollisionResolver::GetHeightmapCurrentlyOn(const Eigen::Vector3f& Position) const
{
	for (const auto Heightmap : State->GetHeightmaps())
	{
		const auto& Vertices = Heightmap->GetVertices();
		auto MinVertex = Vertices.front().front();
		auto MaxVertex = Vertices.back().back();
		float SizeX = Vertices[0][Vertices[0].size() - 1].x() - Vertices[0][0].x();
		float SizeY = Vertices[Vertices.size() - 1][0].y() - Vertices[0][0].y();

		if (Position.x() < MinVertex.x())
			continue;
		if (Position.x() > MaxVertex.x())
			continue;
		if (Position.y() < MinVertex.y())
			continue;
		if (Position.y() > MaxVertex.y())
			continue;

		return Heightmap;
	}
	return nullptr;
}

void CollisionResolver::Resolve(std::shared_ptr<Actor> Target, float deltaTime)
{
	if (Target->GetIsMovable())
	{
		ResolveHeightmap(Target);
	}
}

void CollisionResolver::ResolveHeightmap(std::shared_ptr<Actor> Target)
{
	auto Position = Target->GetTransform().GetWorldTranslation();
	const auto Heightmap = GetHeightmapCurrentlyOn(Target->GetTransform().GetWorldTranslation());

	// Heightmap 위에 있을때만
	if (Heightmap != nullptr)
	{
		float Height = CalcHeightOfLandscape(Position, Heightmap);
		if (Position.z() < Height)
		{
			// Heightmap 위로 올려준다.
			Position.z() = Height;
			Target->SetTranslation(Position.x(), Position.y(), Position.z());
			auto MovementComp = Target->GetMovementComp();
			auto Velocity = MovementComp->GetCurrentVelocity();
			Velocity.z() = 0.f;
			MovementComp->SetVelocity(Velocity);
		}
	}
}

void CollisionResolver::ResolveBoundingVolumes(float DeltaTime)
{
	int Count = 0;
	for (auto Iter = BoundingVolumes.begin(), Next = Iter; Next != BoundingVolumes.end(); Iter = Next)
	{
		++Next;
		auto Volume = Iter->second;

		// 움직이지 못하고 trigger가 아니라면 검사할 필요가 없으므로 바로 넘긴다.
		if (Volume->GetIsMovable() == false && Volume->IsTrigger() == false)
			continue;
		//if(Volume->GetVolumeType() == BoundingVolumeType::Capsule)
		//   continue;
		if (Volume->GetVolumeType() == BoundingVolumeType::Sphere)
		{
			// Capsule에 포함된 Sphere는 무시한다.
			if (std::static_pointer_cast<BoundingSphere>(Volume)->IsPartOfCapsule())
			{
				continue;
			}
		}

		auto MyActor = Volume->GetParentActor();

		// 클라이언트에서는 내 Actor만 처리한다.
#ifdef TSCLIENT
		if (State->GetMyId() != MyActor->GetId())
		{
			continue;
		}
#endif
		auto MyWorldTranslation = Volume->GetTransform().GetWorldTranslation();
		float MyX = MyWorldTranslation.x();
		float MyY = MyWorldTranslation.y();
		float MyZ = MyWorldTranslation.z();
		auto MyCollisionRange = Volume->GetCollisionRange();

		for (auto OtherIter = BoundingVolumes.begin(), OtherNext = OtherIter; OtherNext != BoundingVolumes.end(); OtherIter = OtherNext)
		{
			++OtherNext;
			//if(Volume->GetVolumeType() == BoundingVolumeType::Capsule)
			//   continue;
			if (OtherIter->second->GetVolumeType() == BoundingVolumeType::Sphere)
			{
				// Volume이 Trigger일때는 Capsule에 포함된 sphere이어도 무시하지 않는다.
				//if(!Volume->IsTrigger())
				//{
				   // Capsule에 포함된 Sphere는 무시한다.
				if (std::static_pointer_cast<BoundingSphere>(OtherIter->second)->IsPartOfCapsule())
				{
					continue;
				}
				//}
			}

			auto Other = OtherIter->second;

			if (Iter->first == OtherIter->first)
				continue;
			auto OtherActor = OtherIter->second->GetParentActor();
			if (MyActor == OtherActor)
				continue;
			if (Other->IsTrigger() == true)
				continue;

			auto OtherWorldTranslation = OtherIter->second->GetTransform().GetWorldTranslation();
			float OtherX = OtherWorldTranslation.x();
			float OtherY = OtherWorldTranslation.y();
			float OtherZ = OtherWorldTranslation.z();
			auto OtherCollisionRange = OtherIter->second->GetCollisionRange();
			//auto DistVector = MyWorldTranslation - OtherWorldTranslation;
			double SquaredDist = /*(MyWorldTranslation - OtherWorldTranslation).squaredNorm();*/
				(MyX - OtherX) * (MyX - OtherX)
				+ (MyY - OtherY) * (MyY - OtherY)
				+ (MyZ - OtherZ) * (MyZ - OtherZ);
			//double SquaredDist = DistVector.x() * DistVector.x() + DistVector.y() * DistVector.y() + DistVector.z() * DistVector.z();


			// 거리가 충분히 가까운지 체크하고 맞다면 RecognizedActor에 추가한다.
			// RecognizedActor는 AI에 쓰인다.
#ifdef TSSERVER
			if (OtherActor->GetType() == ActorType::Player && MyActor->GetIsMovable() && MyActor->GetType() != ActorType::Prop)
				MyActor->CheckRecognizeRangeAndAdd(OtherActor->GetId(), SquaredDist);
#endif


			// 길이비교 후 범위 내에 들어가면 추가
			if (SquaredDist > ((double)MyCollisionRange + (double)OtherCollisionRange) * ((double)MyCollisionRange + (double)OtherCollisionRange))
			{
				Volume->CloseVolumes.erase(OtherIter->first);
				continue;
			}
			else
			{
				Volume->CloseVolumes.insert(OtherIter->first);
			}

			++Count;

			// Trigger가 아니면 Intersect검사하지 않는다.
			// 충돌검사는 Intersect가 아니라 Resolve함수로 진행한다.
			if (!Volume->IsTrigger())
			{
				continue;
			}
			//if(!Other->IsTrigger())
			//{
			//   continue;
			//}

			if (OtherActor->GetType() == ActorType::Prop)
			{
				continue;
			}

			// 충돌 체크
			if (Volume->Intersect(*Other))
			{
				// Trigger인지 먼저 체크
				if (Volume->IsTrigger())
				{
					Volume->OnTriggerOverlap(Other);
					continue;
				}
				if (Other->IsTrigger())
				{
					Other->OnTriggerOverlap(Volume);
					continue;
				}

				// Trigger는 무시


				// 충돌 처리
				// BoundingVolume으로부터 충돌 법선벡터와 겹쳐진 길이를 얻어온다.
				Eigen::Vector3f CollisionPlaneNormalVector;
				Eigen::Vector3f CollisionPlaneLocation;
				Volume->GetLastCollisionPlaneInfo(CollisionPlaneNormalVector, CollisionPlaneLocation);

				float IntersectedLength = Volume->GetLastIntersectedLength();
				float DistanceToMove = IntersectedLength;

				bool MyIsMovable = MyActor->GetIsMovable();
				bool OtherIsMovable = OtherActor->GetIsMovable();

				// 두 Actor모두 Dynamic이라면 각각 겹친 반만큼만 이동하면 된다.
				if (MyIsMovable == true && OtherIsMovable == true)
					DistanceToMove *= 0.5f;

				// 겹친만큼 이동시켜주기.
				if (MyIsMovable == true)
				{
					// CollisionPlaneNormalVector의 방향을 알맞게 바꿔준다.
					auto MyTranslation = MyActor->GetTransform().GetWorldTranslation();
					auto LocationRelativeToPlane = (MyTranslation - CollisionPlaneLocation);

					auto MoveDirection = CollisionPlaneNormalVector;

					// CollisionPlaneNormalVector방향으로 IntersectedLength만큼 움직여준다.
					MyActor->GetTransform().AddTranslation(MoveDirection * DistanceToMove);

					// 충돌 시 입사벡터의 수평성분만 남긴다. (Sliding Vector)
					auto MyMovementComp = MyActor->GetMovementComp();

					// Actor로부터 Movement 방향을 얻어온다.
					auto MyVelocity = MyMovementComp->GetCurrentVelocity();

					// MyActor 속도 변경!
					Eigen::Vector3f MySlidingVector = GetSlidingVector(MyVelocity, CollisionPlaneNormalVector);
					//Eigen::Vector3f MySlidingVector = MyVelocity - MoveDirection * (MyVelocity.dot(MoveDirection));
					MyMovementComp->SetVelocity(MySlidingVector);
				}

				if (OtherIsMovable == true)
				{
					// CollisionPlaneNormalVector의 방향을 알맞게 바꿔준다.
					Eigen::Vector3f OtherTranslation = OtherActor->GetTransform().GetWorldTranslation();
					Eigen::Vector3f LocationRelativeToPlane = (OtherTranslation - CollisionPlaneLocation);

					Eigen::Vector3f MoveDirection = -CollisionPlaneNormalVector;

					// CollisionPlaneNormalVector방향으로 IntersectedLength만큼 움직여준다.
					OtherActor->GetTransform().AddTranslation(MoveDirection * DistanceToMove);

					// 충돌 시 입사벡터의 수평성분만 남긴다. (Sliding Vector)
					auto OtherMovementComp = OtherActor->GetMovementComp();

					// Actor로부터 Movement 방향을 얻어온다.
					auto OtherVelocity = OtherMovementComp->GetCurrentVelocity();

					// Other Actor 속도 변경!
					Eigen::Vector3f OtherSlidingVector = OtherVelocity - MoveDirection * (OtherVelocity.dot(MoveDirection));
					OtherMovementComp->SetVelocity(OtherSlidingVector);
				}
			}
		}

		//Volume->Resolve(DeltaTIme);

		// Trigger reset
		if (Volume->IsTrigger())
		{
			auto Parent = Volume->GetParentActor();
			auto TriggerActor = std::reinterpret_pointer_cast<Trigger>(Parent);
			// Trigger 범위 안에 있다가 나간 Actor가 있는지 check하고 있다면 함수를 실행한다.
			TriggerActor->TriggerLeaveCheck();
			// Overlap mark를 reset해준다.
			TriggerActor->ResetOverlapMark();

			float LifeTime = TriggerActor->GetLifeTime();
			float TimerLeftTime = TriggerActor->GetTimerLeftTime();

			// Timer가 아직 안됐다면 실행하지 않음.
			if (TimerLeftTime < 0.f)
			{
				// 이미 -값이면 LifeTime 무한
				if (LifeTime < 0.f)
				{

				}
				// 아니면 LifeTime check
				else
				{
					LifeTime -= (DeltaTime);

					// LifeTime 끝남. 삭제
					if (LifeTime < 0.f)
					{
						//if(Next == OtherIter)
						//   ++Next;

						State->DestroyActor(TriggerActor);
						//LOG_DEBUG("Trigger destroyed.");
					}
					else
					{
						TriggerActor->SetLifeTime(LifeTime);
					}
				}
			}
			else
			{
				// Timer 갱신
				TriggerActor->SetTimer(TimerLeftTime - DeltaTime);
			}
			continue;
		}

		if (Volume->GetIsMovable())
			Volume->Resolve(DeltaTime);

		// 다음 충돌처리 때 사용하기 위해 현재 위치를 저장해둔다.
		//MyActor->SetLastTranslation();
		Volume->SetLastTranslation();
	}
}

// Derived spheres 초기화
void CollisionResolver::InitDerivedSpheres(std::vector<std::shared_ptr<BoundingSphere>>& Spheres, float HalfHeight, float Radius)
{
	Eigen::Vector3f TopLocation(0.f, 0.f, HalfHeight - Radius);
	Spheres[0]->SetRelativeLocation(TopLocation);

	Eigen::Vector3f BottomLocation(0.f, 0.f, -(HalfHeight - Radius));
	Spheres[1]->SetRelativeLocation(BottomLocation);

	// HalfHeight가 Radius를 포함한 값이 되므로 HalfHeight에서 Radius를 뺀 값으로 계산하는게 맞다.
	int Num = Spheres.size() - 2;

	// Half Height를 Radius로 나누고 floor한 값이 1보다 크면
	if (Num >= 1)
	{
		// 제일 아래, 제일 위 두개의 Sphere 사이에
		// Num개 만큼 Sphere를 추가한다.
		// Num개를 추가한다는 것은 Num + 1 등분을 한다는 것.
		float Interval = (HalfHeight * 2.f) / (float)(Num + 1);

		for (int i = 0; i < Num; ++i)
		{
			Eigen::Vector3f NewLocation(0.f, 0.f, -HalfHeight + Interval * (1 + i));
			Spheres[2 + i]->SetRelativeLocation(NewLocation);
		}
	}
}

TheShift::UID CollisionResolver::GetAvailableUID()
{
	UID FreeUID = INVALID_UID;
	bool Found = false;
	while (!Found)
	{
		++FreeUID;
		if (BoundingVolumes.find(FreeUID) == BoundingVolumes.end())
		{
			break;
		}
		if (FreeUID >= MAX_UID)
			return INVALID_UID;
	}

	return FreeUID;
}
}
