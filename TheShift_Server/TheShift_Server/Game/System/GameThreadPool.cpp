﻿#include "GameThreadPool.h"

GameThreadPool::GameThreadPool()
{}

bool GameThreadPool::CreateGameThreads(ThreadIndex NumThreads)
{
	for(int8_t i = 0; i < NumThreads; ++i)
	{
		Threads.insert(std::make_pair(i, new GameThread{ i }));
	}

	for(int i = 0; i < NumThreads; ++i)
	{
		if(!Threads[i]->IsReady())
			return false;
	}

	return true;
}

void GameThreadPool::JoinAll()
{
	for(auto& Thread : Threads)
	{
		Thread.second->Suspend();
		Thread.second->Clear();
		Thread.second->Join();
	}
}

void GameThreadPool::RunAll()
{
	for(auto& Thread : Threads)
	{
		Thread.second->Run();
	}
}

void GameThreadPool::SuspendAll()
{
	for(auto& Thread : Threads)
	{
		Thread.second->Suspend();
	}
}

void GameThreadPool::AddPlayerToSession(std::shared_ptr<Avatar> client, SessionId sessionId)
{
	if(SessionThreadMapping.find(sessionId) != SessionThreadMapping.end())
	{
		if(Threads.find(SessionThreadMapping[sessionId]) != Threads.end())
		{
			Threads[SessionThreadMapping[sessionId]]->AddPlayerToSession(client, sessionId);
			client->SessionId_ = sessionId;
		}

	}
}

/// <summary>
/// Session이 사라졌으므로 SessionThreadMapping에서 해당 SessionId를 제거해준다.
/// </summary>
void GameThreadPool::RemoveSessionThreadMapping(SessionId sessionId)
{
	if(SessionThreadMapping.find(sessionId) != SessionThreadMapping.end())
	{
		SessionThreadMapping.erase(sessionId);
	}
}

/// <summary>
/// Game Thread로 Message를 보낸다.
/// </summary>
/// <param name="newMessage"> MemoryPool에서 할당되어야 한다. </param>
void GameThreadPool::DispatchMessageToThread(Message* newMessage)
{
	if(SessionThreadMapping.find(newMessage->Dest) != SessionThreadMapping.end())
	{
		if(MessageQueues.find(SessionThreadMapping[newMessage->Dest]) != MessageQueues.end())
		{
			MessageQueues[SessionThreadMapping[newMessage->Dest]].push(newMessage);
			if(Threads.find(SessionThreadMapping[newMessage->Dest]) != Threads.end())
			{
				// Suspended상태라면 다시 시작한다.
				if(Threads[SessionThreadMapping[newMessage->Dest]]->CheckSuspended())
					Threads[SessionThreadMapping[newMessage->Dest]]->WakeThread();
			}
		}
	}
}

// Session이 가장 적게 할당된 thread에 새로운 session을 할당한다.
void GameThreadPool::CreateSession(std::shared_ptr<SessionInfo> sessionInfo)
{
	// 가장 Session이 적게 할당된 thread를 고른다.
	ThreadIndex FreeThreadIndex = 0;
	int Least = 1000000;

	for(auto& it : Threads)
	{
		int SessionCount = it.second->GetSessionCount();
		if(SessionCount < Least)
		{
			Least = SessionCount;
			FreeThreadIndex = it.first;
		}
	}

	if(Threads.find(FreeThreadIndex) != Threads.end())
	{
		Threads[FreeThreadIndex]->CreateSession(sessionInfo->Id);
		SessionThreadMapping.insert(std::make_pair(sessionInfo->Id, FreeThreadIndex));

		// 로비에서 같은 session에 있던 player들을 모두 추가시켜준다.
		for(auto& Player : sessionInfo->Players)
		{
			AddPlayerToSession(Player.second, sessionInfo->Id);
		}

		Threads[FreeThreadIndex]->StartSession(sessionInfo->Id, sessionInfo->Map);
	}
}

// 해당 Game thread에 어떤 queue를 사용해야 할지 알려준다.
void GameThreadPool::SetMessageQueues(ThreadIndex NumThreads)
{
	for(ThreadIndex i = 0; i < NumThreads; ++i)
	{
		MessageQueues.insert(std::make_pair(i, tbb::concurrent_queue<Message*>{}));
	}

	for(int i = 0; i < Threads.size(); ++i)
	{
		Threads[i]->SetMessageQueue(&MessageQueues[i]);
	}
}

