﻿#pragma once

#include "Protocol/Types.h"
#include <map>

using namespace TheShift;

class MapData
{
private:
	MapData();
	~MapData();

public:
	static MapData& GetInstance()
	{
		if(!Instance_)
			Instance_ = new MapData;
		return *Instance_;
	}
	static void DestroyInstance()
	{
		if (Instance_)
		{
			delete Instance_;
			Instance_ = nullptr;
		}
	}

public:
	const MapInfo& operator[](MapType Type) const;
	const MapInfo& GetMapInfo(MapType Type) const;

private:
	static MapData* Instance_;
	static int HeightmapCount;
	std::map<MapType, MapInfo> Data;

	void ReadRawHeightmap(HeightmapInfo& Info);
	void ReadLandscape(MapInfo& Info);
};

