﻿#include "Session.h"

SessionInfo::SessionInfo() :
	Id(0)
{}

SessionInfo::SessionInfo(SessionId newId) : Id(newId)
{}

Session::Session()
{}

Session::Session(SessionId NewId)
{
	Id = NewId;
}

bool Session::UpdateGame(std::chrono::nanoseconds deltaTime)
{
	return State.Update(deltaTime);
}

void Session::ApplyMessage(Message* message)
{
	State.ApplyMessage(message);
	// Disconnect message였다면 Session에서 삭제
	if(message->Data->ClassId() == static_cast<uint16_t>(Serializables::Type::Disconnect))
	{
		Players.erase(message->From);
	}
}

void Session::AddNewPlayer(std::shared_ptr<Avatar> client)
{
	State.AddNewPlayer(client);
}

void Session::StartGame(MapType map)
{
	State.Init(map);
	State.StartGame();
}

void Session::SetUpdateTimestep(float Timestep)
{
	State.SetUpdateTimestep(Timestep);
}

std::map<ClientId, std::shared_ptr<Avatar>>* Session::GetPlayers()
{
	return State.GetPlayers();
}

std::map<UID, std::shared_ptr<Actor>>* Session::GetActors()
{
	return State.GetActors();
}

std::vector<std::shared_ptr<Serializables::SerializableData>>* Session::GetDataToBeSent()
{
	return State.GetDataToBeSent();
}

void Session::ClearDataToBeSent()
{
	State.ClearDataToBeSent();
}
