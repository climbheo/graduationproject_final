﻿#pragma once

//#include "common.h"
#include "GameThread.h"
#include <map>
#include "Protocol/Protocol.h"
//#include "Protocol/atomic_memory_pool.h"
#include "tbb/include/tbb/concurrent_queue.h"


// 모든 Game thread들을 저장하는 class
// 모든 thread를 한번에 실행시키거나 종료시킬 수 있다.
class GameThreadPool
{
public:
	GameThreadPool();

	//void SetMessagePool(MemoryPool<Message>* messagePool);
	//void SetSendRequirements(tbb::concurrent_queue<SocketInfo*>* sendQueue, MemoryPool<SocketInfo>* socketInfoPool,
	//                         HANDLE* sendEvent);

	// NumThreads 만큼 thread를 만들어준다.
	bool CreateGameThreads(ThreadIndex NumThreads);

	// Message queue를 등록하고
	// Queue의 index를 설정해준다.
	void SetMessageQueues(ThreadIndex NumThreads);



	// 서버 종료 전 불려야 한다.
	void JoinAll();

	// 모든 game thread를 실행시킨다.
	void RunAll();

	// 모든 game thread를 모두 멈춤
	// 서버 실행 도중 tickrate를 바꾸거나 할때 사용
	void SuspendAll();

	//ThreadIndex GetQueueIndexByClientId(ClientId id);
	void AddPlayerToSession(std::shared_ptr<Avatar> client, SessionId sessionId);
	void RemoveSessionThreadMapping(SessionId sessionId);
	void DispatchMessageToThread(Message* newMessage);


	// Game thread중 가장 일이 적은 thread에 Session을 할당한다.
	void CreateSession(std::shared_ptr<SessionInfo> sessionInfo);


private:
	std::map<ThreadIndex, GameThread*> Threads;
	std::map<ThreadIndex, tbb::concurrent_queue<Message*>> MessageQueues;
	std::map<SessionId, ThreadIndex> SessionThreadMapping;

};

