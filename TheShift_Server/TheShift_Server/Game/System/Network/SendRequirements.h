﻿#pragma once

//#include <apiquery2.h>
#include "tbb/include/tbb/concurrent_queue.h"
#include "tbb/include/tbb/concurrent_hash_map.h"
#include "tbb/include/tbb/spin_rw_mutex.h"
#include "spdlog/logger.h"
#include "Player.h"
#include "Protocol/MessageBuilder.h"
#include "Game/System/MemoryAllocators.h"
#include "Protocol/Log.h"

class Server;
//LPWSAOVERLAPPED_COMPLETION_ROUTINE CompletionRoutine;
void CALLBACK CompletionRoutine(DWORD Error, DWORD TransferredBytes, LPWSAOVERLAPPED LpOverlapped, DWORD Flags);

namespace TheShift
{
struct ClientIdHashCompare
{
	static size_t hash(const ClientId& x)
	{
		size_t h = x;
		return h;
	}
	static bool equal(const ClientId& x, const ClientId& y)
	{
		return x == y;
	}
};
typedef tbb::concurrent_hash_map<ClientId, Player*, ClientIdHashCompare> ClientIdTable;

//extern HANDLE SendEvent;
extern tbb::concurrent_hash_map<ClientId, Player*, ClientIdHashCompare> Players;
//extern tbb::concurrent_queue<SocketInfo*> SendQueue;

namespace Network
{
extern thread_local MessageBuilder MsgBuilder;

// Send functions
template <typename T>
typename std::enable_if<std::is_base_of<Serializables::SerializableData, T>::value>::type Send(ClientId id, T& data)
{
	SocketInfo* SendSocketInfo = MemoryAllocator::SocketInfoAllocator.alloc();
	SendSocketInfo->Io = IoType::Write;
	size_t Size = 0;
	MsgBuilder.BuildSerializedBuffer(data, SendSocketInfo->Buffer, Size);
	SendSocketInfo->WsaBuffer.len = Size;
	SendSocketInfo->PlayerIndex = id;

	ClientIdTable::const_accessor Accessor;
	if(!Players.find(Accessor, id))
	{
		LOG_WARNING("Can't find Player #%d", id);
		return;
	}

	SOCKET Socket = Accessor->second->GetSocketInfoPtr()->Socket;
	SendSocketInfo->Socket = Socket;
	Accessor.release();
	DWORD BytesSent = 0;
	WSASend(Socket, &SendSocketInfo->WsaBuffer, 1, &BytesSent, 0, reinterpret_cast<OVERLAPPED*>(SendSocketInfo), CompletionRoutine);
}
} // namespace Network
} // namespace TheShift
