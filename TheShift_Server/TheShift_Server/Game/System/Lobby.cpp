﻿#include "Lobby.h"

Lobby::Lobby()
{}

bool Lobby::IsPlayerInLobby(ClientId id) const
{
	return Players.find(id) != Players.end();
}

bool Lobby::IsSessionInLobby(SessionId id) const
{
	return Sessions.find(id) != Sessions.end();
}

bool Lobby::IsPlayerInSession(ClientId clientId, SessionId sessionId) const
{
	if(IsPlayerInLobby(clientId) && IsSessionInLobby(sessionId))
	{
		auto TargetSession = Sessions.find(sessionId);

		if(TargetSession->second->Players.find(clientId) != TargetSession->second->Players.end())
		{
			return true;
		}
	}

	return false;
}

bool Lobby::IsEveryoneReady(SessionId id) const
{
	if(IsSessionInLobby(id))
	{
		for(auto PlayerPair : Sessions.find(id)->second->Players)
		{
			if(Players.find(PlayerPair.second->ClientId_)->second->IsReady_ == false)
			{
				return false;
			}
		}
	}

	return true;
}

/// <summary>
/// 해당 Session이 현재 게임 중인지 확인한다.
/// </summary>
/// <returns> 게임중인지 여부 </returns>
bool Lobby::IsInGame(SessionId id) const
{
	if(IsSessionInLobby(id) == false)
	{
		return false;
	}
	return Sessions.find(id)->second->IsInGame;
}

/// <summary>
/// Lobby에 player를 추가한다.
/// </summary>
/// <returns> 추가에 성공했는지 여부를 return한다.</returns>
/// TODO: 이름 등 추가 정보도 받아서 Avatar에 채워줘야한다.
bool Lobby::Join(ClientId id)
{
	// 이미 해당 id가 존재한다면 return false
	if(Players.find(id) != Players.end())
	{
		LOG_WARNING("Failed to add player to lobby. Player Id: %d", id);
		return false;
	}
	Players.insert(std::make_pair(id, std::make_shared<Avatar>(id)));
	LOG_INFO("Player connected to lobby. Player Id: %d", id);
	return true;
}

/// <summary>
/// 이미 존재하는 세션에 clientId를 추가하거나 새로 session을 생성 후 추가한다.
/// </summary>
/// <returns> 추가에 성공했는지 여부를 return한다. </returns>
SessionId Lobby::SessionJoinOrCreate(ClientId clientId, SessionId sessionId)
{
	SessionId TargetSessionId = sessionId;
	// 해당 clientId가 존재하지 않음
	if(!IsPlayerInLobby(clientId))
	{
		return INVALID_SESSIONID;
	}
	// 해당 sessionId가 존재하지 않음
	if(!IsSessionInLobby(TargetSessionId))
	{
		// 새로운 Session 생성 요청이라면 새로 생성.
		if(TargetSessionId == NEW_SESSION)
		{
			TargetSessionId = GetAvailableSessionId();
			Sessions.insert(std::make_pair(TargetSessionId, std::make_shared<SessionInfo>(TargetSessionId)));
			// Host로 임명
			Sessions[TargetSessionId]->HostId = clientId;
			Sessions[TargetSessionId]->Players.insert(std::make_pair(clientId, Players[clientId]));
			Players[clientId]->SessionId_ = TargetSessionId;
		}
		else
			return INVALID_SESSIONID;
	}
	Sessions[TargetSessionId]->Players.insert(std::make_pair(clientId, GetPlayerInfo(clientId)));
	return TargetSessionId;
}

/// <summary>
/// Sessions에서 해당 Player가 있는 Session을 찾아 Id를 제거한다.
/// 해당 session의 모든 Player가 나갔다면 session을 제거한다.
/// </summary>
/// <returns> Session에서 나가졌는지 여부를 return한다. </returns>
bool Lobby::SessionExit(ClientId id)
{
	if(IsPlayerInLobby(id))
	{
		// Session Id check
		SessionId SId = Players[id]->SessionId_;

		// 해당 session이 이 Player의 id를 가지고 있을때만 삭제한다
		if(IsPlayerInSession(id, SId))
		{
			Sessions[SId]->Players.erase(id);
			// 해당 session의 모든 Player가 나갔다면 해당 session을 제거한다.
			if(Sessions[SId]->Players.empty())
			{
				Sessions.erase(SId);
			}

			// SessionId 초기화
			Players[id]->SessionId_ = INVALID_SESSIONID;
			return true;
		}
	}

	return false;
}

/// <summary>
/// Session에 접속중이라면 해당 session에서 삭제하고 Lobby에서도 삭제한다.
/// </summary>
void Lobby::DisconnectPlayer(ClientId id)
{
	SessionExit(id);
	Players.erase(id);
}

/// <summary>
/// 현재 Player가 session내에 있는지 확인하고
/// 게임중이 아닌지 확인한다.
/// 아니라면 ready상태로 바꾼다.
/// </summary>
/// <returns>
/// 해당 Player의 상태가 ready로 바뀌었는지 여부를 return한다.
/// 이미 ready상태여도 true를 return한다.
/// </returns>
/// <seealso cref="Lobby::PlayerUnready()"/>
bool Lobby::PlayerReady(ClientId id)
{
	if(IsPlayerInLobby(id))
	{
		if(IsSessionInLobby(Players[id]->SessionId_))
		{
			Players[id]->IsReady_ = true;
			return true;
		}
	}

	return false;
}

/// <summary>
/// PlayerReady의 반대 역할
/// </summary>
/// <see cref="Lobby::PlayerReady()"/>
bool Lobby::PlayerUnReady(ClientId id)
{
	if(IsPlayerInLobby(id))
	{
		if(IsSessionInLobby(Players[id]->SessionId_))
		{
			Players[id]->IsReady_ = false;
			return true;
		}
	}

	return false;
}

// 해당 Id를 가진 Session을 삭제한다.
void Lobby::DestroySession(SessionId id)
{
	auto TargetSession = Sessions.find(id);
	if(TargetSession != Sessions.end())
	{
		for(auto& Player : TargetSession->second->Players)
		{
			DisconnectPlayer(Player.first);
		}
	}
}

std::shared_ptr<Avatar> Lobby::GetPlayerInfo(ClientId id)
{
	if(IsPlayerInLobby(id))
	{
		return Players.find(id)->second;
	}
	return nullptr;
}

std::shared_ptr<SessionInfo> Lobby::GetSessionInfo(SessionId sessionId)
{
	if(IsSessionInLobby(sessionId))
	{
		return Sessions[sessionId];
	}
	return nullptr;
}

void Lobby::GetPlayers(std::vector<std::shared_ptr<Avatar>>& players)
{
	for(auto& PlayerPair : Players)
	{
		players.emplace_back(PlayerPair.second);
	}
}

void Lobby::GetSessions(std::vector<std::shared_ptr<SessionInfo>>& sessions)
{
	for(auto& SessionPair : Sessions)
	{
		sessions.emplace_back(SessionPair.second);
	}
}

/// <summary>
/// Build LobbyInfo.
/// Contains information of Players and sessions in the lobby
/// </summary>
void Lobby::GetLobbyInfo(Serializables::LobbyInfo& info)
{
	for(auto& Player : Players)
	{
		Serializables::PlayerInfo PInfo;
		PInfo.Id = Player.first;
		PInfo.Name = Player.second->Name_;
		info.Players.Array.emplace_back(PInfo);
	}
	for(auto& Session : Sessions)
	{
		Serializables::SessionInfo SInfo;
		SInfo.SessionIndex = Session.first;
		SInfo.SessionName = Session.second->Name;
		for(auto& Id : Session.second->Players)
		{
			SInfo.ClientIds.emplace_back(Id.second->ClientId_);
		}
		SInfo.IsInGame = Session.second->IsInGame;
		SInfo.Map = Session.second->Map;
		info.Sessions.Array.emplace_back(SInfo);
	}
}

/// <summary>
/// Lobby에 접속중인 Player들을 반환한다.
/// Session에 접속중인 Player들은 제외한다.
/// </summary>
void Lobby::GetPlayersInLobby(std::vector<std::shared_ptr<Avatar>>& players)
{
	// TODO: Lobby에 있는 Player들 중 session에 접속해있는 Player들은 제외해야함.
}

ClientId Lobby::GetHostId(SessionId sessionId)
{
	if(Sessions.find(sessionId) != Sessions.end())
	{
		return Sessions[sessionId]->HostId;
	}
	return INVALID_CLIENTID;
}

/// <summary>
/// 새로 생성 가능한 SessionId를 반환한다.
/// </summary>
SessionId Lobby::GetAvailableSessionId() const
{
	bool Found = false;
	SessionId FreeSessionId = 2;
	while(FreeSessionId <= MAX_SESSION + 1)
	{
		if(Sessions.find(FreeSessionId) == Sessions.end())
		{
			Found = true;
			break;
		}
		++FreeSessionId;
	}

	if(Found)
		return FreeSessionId;
	else
		return INVALID_SESSIONID;
}


