﻿#include "TimerCallee.h"
#include "Protocol/Log.h"

namespace TheShift
{
TimerCallee::TimerCallee() : TimeSetProperly(false)
{

}

TimerCallee::~TimerCallee()
{

}

CoolDown::CoolDown(bool* trigger) : Trigger(trigger)
{

}

CoolDown::CoolDown()
{

}

// Cool down을 사용 가능한 상태로 바꾼다.
void CoolDown::operator()()
{
	if(Trigger != nullptr)
		*Trigger = true;
	else
		LOG_WARNING("Timer couldn't trigger cooldown. bool variable not valid.");
}

CoolDown::operator bool() const
{
	return true;
}

void CoolDown::Register(bool* trigger)
{
	if(trigger != nullptr)
		Trigger = trigger;
	else
		LOG_WARNING("CoolDown::Register couldn't register bool variable. Not valid.");
}

Activator::Activator(std::function<void()> func)
{
	Behaviour = func;
}

Activator::Activator()
{

}

// 지정된 행동을 실행한다.
void Activator::operator()()
{
	if (Behaviour)
		Behaviour();
	else
		LOG_WARNING("Timer couldn't call Activator's behaviour. Not valid.");
}

Activator::operator bool() const
{
	if(Behaviour)
		return true;
	else
		return false;
}

void Activator::Register(std::function<void()> func)
{
	Behaviour = func;
}

////////////////////////////////////////////////


CheckActivator::operator bool() const
{
	return Activator::operator bool();
}

void CheckActivator::operator()()
{
	if (!isActivated)
	{
		Activator::operator()();
		isActivated = true;
	}
}

void CheckActivator::SetActivated(bool isActivated)
{
	this->isActivated = isActivated;
}

}