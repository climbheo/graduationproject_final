﻿#include "TimerQueue.h"
#include <cassert>
#include "Protocol/Log.h"

TheShift::TimerQueue::TimerQueue()
{

}

void TheShift::TimerQueue::Push(std::shared_ptr<TimerCallee>& Callee)
{
	assert(Callee->TimeSetProperly == true);
	Queue.push(Callee);
}

void TheShift::TimerQueue::Poll()
{
	while(true)
	{
		// Queue가 비어있으면 break
		if(Queue.empty())
			break;

		// 제일 앞 원소의 예정 시간이 아직 오지 않았으면 break
		auto Element = Queue.top();
		if(Element->TimePoint > std::chrono::high_resolution_clock::now())
			break;

		// Callable 일때 실행한다.
		if(*Element)
		{
			(*Element)();
			//spdlog::info("Callable object got called.");
		}

		Queue.pop();
	}
}

void TheShift::TimerQueue::Clear()
{
	while (!Queue.empty())
	{
		Queue.pop();
	}
}

//std::shared_ptr<TheShift::TimerCallee> TheShift::TimerQueue::Top()
//{
//	return Queue.top();
//}
//
//void TheShift::TimerQueue::Pop()
//{
//	Queue.pop();
//}
