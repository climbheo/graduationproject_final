﻿#pragma once

#include "TimerCallee.h"
#include <queue>
#include <memory>

namespace TheShift
{
class TimerQueue
{
public:
	TimerQueue();

	void Push(std::shared_ptr<TimerCallee>& Callee);
	//std::shared_ptr<TimerCallee> Top();
	//void Pop();
	void Poll();

	//For Anim Notify -> 애님상태가 중간에 변경되면 등록되었지만 실행되지 않은 모든 Callee를 삭제해야함.
	void Clear();

private:

	struct Cmp
	{
		bool operator()(std::shared_ptr<TimerCallee> a, std::shared_ptr<TimerCallee> b)
		{
			return a->TimePoint > b->TimePoint;
		}
	};
	std::priority_queue<std::shared_ptr<TimerCallee>, std::vector<std::shared_ptr<TimerCallee>>, Cmp> Queue;
};
}

