﻿#include "GameManager.h"
#include "Protocol/Log.h"
#include "MapData.h"
#include "AnimNotifyData.h"


GameManager::GameManager()
{
}

/// <summary>
/// GamePool의 thread들을 생성하고 MessageQueue를 map한다.
/// </summary>
void GameManager::SetGameThreads(ThreadIndex numThreads)
{
	GamePool.CreateGameThreads(numThreads);
	// Game thread와 queue를 1대1로 맞춰준다.
	GamePool.SetMessageQueues(numThreads);
}

/// <summary>
/// GamePool의 모든 thread를 실행한다.
/// </summary>
void GameManager::Run()
{
	// if (SendQueue != nullptr /*&& SocketInfoPool != nullptr*/)
	GamePool.RunAll();
	// else
	// spdlog::error("Can't Run() GamePool when SendQueue and SocketInfoPool are nullptr");
}


/// <summary>
///	게임과 관련된 모든 패킷을 처리한다.
/// </summary>
/// <param name="clientId"> 해당 패킷을 보낸 client의 id </param>
///	<param name="data"> Data가 담겨있는 buffer </param>
///	<param name="dataType"> 해당 buffer가 어떤 type의 data를 가지고 있는지 알려준다. </param>
void GameManager::ProcessPacketData(ClientId clientId, char* data, Serializables::Type dataType)
{
	InputStream.SetTargetBuffer(data);
	switch(dataType)
	{
	case Serializables::Type::Disconnect:
	{
		// 삭제하는 순서: GamePool->GameLobby->Server
		Serializables::Disconnect Data;
		auto Exit = std::make_shared<Serializables::SessionExit>();
		Data.Read(InputStream);
		std::shared_ptr<Avatar> TargetPlayer = GameLobby.GetPlayerInfo(clientId);
		if(TargetPlayer != nullptr)
		{
			Data.ActorId = TargetPlayer->ActorId_;
			Exit->CurrentSessionId = TargetPlayer->SessionId_;
		}

		DispatchMessageToThread(clientId, Exit);
		ProcessSessionExit(clientId, *Exit);
		ProcessLobbyExit(clientId);
		break;
	}

	case Serializables::Type::SessionExit:
	{
		auto Data = std::make_shared<Serializables::SessionExit>();
		Data->Read(InputStream);
		// Game중인 session에서도 나가게 한다.
		DispatchMessageToThread(clientId, Data);
		ProcessSessionExit(clientId, *Data);
	}

	case Serializables::Type::LobbyJoinReq:
	{
		Serializables::LobbyJoinReq Data;
		Data.Read(InputStream);
		ProcessLobbyJoinReq(clientId, Data);
		break;
	}

	case Serializables::Type::LobbyInfoReq:
	{
		// TODO: ProcessLobbyConnectionReq() 함수 만들기
		// 로비에 접속이 가능하다면 로비에 플레이어를 추가시키고 로비에 접속한 플레이어와 세션 목록 패킷을 보낸다.
		// 접속이 불가능하다면 실패 패킷을 보낸다.
		Serializables::LobbyInfoReq Data;
		Data.Read(InputStream);
		ProcessLobbyInfoReq(clientId, Data);
		break;
	}

	case Serializables::Type::SessionJoinReq:
	{
		// 1. 게임이 아직 시작되지 않았을 때:
		// 해당 세션이 존재한다면 해당 세션이 자리가 있는지 확인하고
		// 접속 가능하다면 해당 세션에 추가시킨다.
		// 해당 세션이 존재하지 않는다면 새로 추가시킨다.
		// 이후 게임이 시작되면 해당 세션 정보를 널널한 game thread로 전달한다.

		// 2. 게임이 진행중일 때
		// 빈 자리가 남아있다면 세션에 플레이어 추가 후 해당 세션이 할당된 thread에 message를 보낸다.
		// 빈 자리가 없다면 실패 패킷을 보낸다.
		Serializables::SessionJoinReq Data;
		Data.Read(InputStream);
		ProcessSessionJoinReq(clientId, Data);
		break;
	}

	case Serializables::Type::CreateAndStartSessionReq:
	{
		Serializables::CreateAndStartSessionReq Data;
		Data.Read(InputStream);
		ProcessCreateAndStartSessionReq(clientId, Data);
		break;
	}

	case Serializables::Type::Chat:
	{
		// TODO: ProcessChat() 함수 만들기
		// 해당 플레이어가 로비에 있는지, 대기중인 세션에 있는지, 진행중인 게임에 있는지 확인한다.
		// 로비에 있다면 로비에 있는 모든 플레이어에게 보내고 대기중인 세션에 있다면 해당 세션에만 보낸다.
		// 진행중인 게임에 있어도 해당 세션에만 보낸다.
		Serializables::Chat Data;
		Data.Read(InputStream);
		ProcessChat(clientId, Data);
		// NewMessage = MsgBuilder.BuildMessage<Serializables::Chat>(DataBuffer);
		break;
	}

	case Serializables::Type::Ready:
	{
		Serializables::Ready Data;
		Data.Read(InputStream);
		ProcessReady(clientId, Data);
		break;
	}

	case Serializables::Type::MapSelect:
	{
		Serializables::MapSelect Data;
		Data.Read(InputStream);
		ProcessMapSelect(clientId, Data);
	}

	case Serializables::Type::LoadCompleted:
	{
		auto Data = std::make_shared<Serializables::LoadCompleted>();
		Data->Read(InputStream);
		DispatchMessageToThread(clientId, Data);
		//ProcessLoadCompleted(clientId, Data);
		break;
	}

	case Serializables::Type::Input:
	{
		auto Data = std::make_shared<Serializables::Input>();
		Data->Read(InputStream);
		DispatchMessageToThread(clientId, Data);
		//ProcessInput(clientId, Data);
		break;
	}

	case Serializables::Type::ChangeUpdateTimestep:
	{
		auto Data = std::make_shared<Serializables::ChangeUpdateTimestep>();
		Data->Read(InputStream);
		DispatchMessageToThread(clientId, Data);
		break;
	}

	case Serializables::Type::SwapWeapon:
	{
		auto Data = std::make_shared<Serializables::SwapWeapon>();
		Data->Read(InputStream);
		DispatchMessageToThread(clientId, Data);
		break;
	}

	case Serializables::Type::SwapArmor:
	{
		auto Data = std::make_shared<Serializables::SwapArmor>();
		Data->Read(InputStream);
		DispatchMessageToThread(clientId, Data);
		break;
	}

	case Serializables::Type::LevelChangeReqDebug:
	{
		auto Data = std::make_shared<Serializables::LevelChangeReqDebug>();
		Data->Read(InputStream);
		DispatchMessageToThread(clientId, Data);
		break;
	}

	case Serializables::Type::ActWithTargets:
	{
		auto Data = std::make_shared<Serializables::ActWithTargets>();
		Data->Read(InputStream);
		DispatchMessageToThread(clientId, Data);
		break;
	}

	case Serializables::Type::DirectiveAct:
	{
		auto Data = std::make_shared<Serializables::DirectiveAct>();
		Data->Read(InputStream);
		DispatchMessageToThread(clientId, Data);
		break;
	}

	case Serializables::Type::TriggerNotify:
	{
		auto Data = std::make_shared<Serializables::TriggerNotify>();
		Data->Read(InputStream);
		DispatchMessageToThread(clientId, Data);
		break;
	}

	case Serializables::Type::InteractableAct:
	{
		auto Data = std::make_shared<Serializables::InteractableAct>();
		Data->Read(InputStream);
		DispatchMessageToThread(clientId, Data);
		break;
	}

	case Serializables::Type::ItemAcquireFromChestReq:
	{
		auto Data = std::make_shared<Serializables::ItemAcquireFromChestReq>();
		Data->Read(InputStream);
		DispatchMessageToThread(clientId, Data);
		break;
	}

	case Serializables::Type::ItemAcquire:
	{
		auto Data = std::make_shared<Serializables::ItemAcquire>();
		Data->Read(InputStream);
		DispatchMessageToThread(clientId, Data);
		break;
	}

	case Serializables::Type::ItemEquip:
	{
		auto Data = std::make_shared<Serializables::ItemEquip>();
		Data->Read(InputStream);
		DispatchMessageToThread(clientId, Data);
		break;
	}

	case Serializables::Type::ItemUnacquire:
	{
		auto Data = std::make_shared<Serializables::ItemUnacquire>();
		Data->Read(InputStream);
		DispatchMessageToThread(clientId, Data);
		break;
	}

	case Serializables::Type::ItemUnequip:
	{
		auto Data = std::make_shared<Serializables::ItemUnequip>();
		Data->Read(InputStream);
		DispatchMessageToThread(clientId, Data);
		break;
	}

	case Serializables::Type::Cheat:
	{
		auto Data = std::make_shared<Serializables::Cheat>();
		Data->Read(InputStream);
		DispatchMessageToThread(clientId, Data);
		break;
	}

	case Serializables::Type::Unhandled:
		LOG_WARNING("Unhandled Serializable type! From Client #%d", clientId);
		break;

	default:
		LOG_WARNING("Unhandled Serializable type! From Client #%d", clientId);
		break;
	}
}

// Client를 정리한다.
void GameManager::ClearClient(ClientId clientId)
{
	auto Exit = std::make_shared<Serializables::SessionExit>();
	std::shared_ptr<Avatar> TargetPlayer = GameLobby.GetPlayerInfo(clientId);
	if (TargetPlayer != nullptr)
	{
		Exit->CurrentSessionId = TargetPlayer->SessionId_;
		DispatchMessageToThread(clientId, Exit);
		ProcessSessionExit(clientId, *Exit);
		ProcessLobbyExit(clientId);
	}
}

/// <summary>
/// GamePool의 메모리를 해제하고 모든 Thread들을 join한다.
/// </summary>
void GameManager::ClearAll()
{
	MapData::DestroyInstance();
	AnimNotifyData::DestroyInstance();
	GamePool.JoinAll();
}

/// <summary>
/// Lobby에 입장시킨다.
/// 입장 완료 후 Lobby정보도 함께 보낸다.
/// </summary>
void GameManager::ProcessLobbyJoinReq(ClientId id, Serializables::LobbyJoinReq& data)
{
	if(GameLobby.Join(id))
	{
		Serializables::LobbyConnected Connected;
		Connected.Connected = true;

		GameLobby.GetPlayerInfo(id)->Name_ = data.Name;
		Serializables::LobbyInfo Info;
		GameLobby.GetLobbyInfo(Info);


		Network::Send(id, Connected);
		Network::Send(id, Info);
	}
}

/// <summary>
/// GameLobby의 정보를 읽어와 보낸다.
/// </summary>
void GameManager::ProcessLobbyInfoReq(ClientId id, Serializables::LobbyInfoReq& data)
{
	Serializables::LobbyInfo Info;
	GameLobby.GetLobbyInfo(Info);

	Network::Send(id, Info);
	LOG_INFO("Lobby info sent. Players: %d Sessions: %d", Info.Players.Array.size(), Info.Sessions.Array.size());
}

/// <summary>
/// Player를 요청받은 session에 접속시킨다.
/// </summary>
/// TODO: Session에 방 제목을 넣기 위해서는 SessionCreateReq를 만들어서 따로 처리해야 함.
void GameManager::ProcessSessionJoinReq(ClientId id, Serializables::SessionJoinReq& data)
{
	SessionId TargetSessionId = GameLobby.SessionJoinOrCreate(id, data.SessionIndex);

	// Session이 게임중일때 GamePool로 message 전달
	if(GameLobby.IsInGame(data.SessionIndex))
	{
		GamePool.AddPlayerToSession(GameLobby.GetPlayerInfo(id), data.SessionIndex);
		auto Req = std::make_shared<Serializables::SessionJoinReq>(data);
		DispatchMessageToThread(id, Req);
	}
	// Session이 게임중이 아닐떄 그냥 접속
	else
	{
		if(TargetSessionId != INVALID_SESSIONID)
		{
			Serializables::SessionConnected Data;
			Data.SessionIndex = TargetSessionId;
			Data.HostId = GameLobby.GetHostId(TargetSessionId);

			for(auto& Iter : GameLobby.GetSessionInfo(TargetSessionId)->Players)
			{
				Serializables::PlayerInfo Info;
				Info.Id = Iter.second->ClientId_;
				Info.Name = Iter.second->Name_;
				Data.PlayersInfo.Array.emplace_back(std::move(Info));
			}
			Network::Send(id, Data);
		}
	}
}

// 새로운 Session을 생성하고 바로 시작시킨다.
void GameManager::ProcessCreateAndStartSessionReq(ClientId id, Serializables::CreateAndStartSessionReq& data)
{
	// Lobby에 없는상태면 추가해준다.
	if(!GameLobby.IsPlayerInLobby(id))
	{
		GameLobby.Join(id);
	}
	
	SessionId NewSessionId = GameLobby.SessionJoinOrCreate(id, NEW_SESSION);
	if(NewSessionId == INVALID_SESSIONID)
	{
		LOG_ERROR("Failed to start session, INVALID_SESSIONID");
		return;
	}

	// Map 설정
	auto TargetSession = GameLobby.GetSessionInfo(NewSessionId);
	TargetSession->Map = data.Type;
	TargetSession->Name = data.SessionName;

	
	// Session 시작
	if (!StartSession(NewSessionId))
	{
		// Session 시작 실패
		LOG_WARNING("Failed to start session# %d", NewSessionId);
	}
}

/// <summary>
/// Player를 Lobby에서 접속 종료시킨다.
/// </summary>
void GameManager::ProcessLobbyExit(ClientId id)
{
	GameLobby.DisconnectPlayer(id);
}

/// <summary>
/// Session에서 해당 Player를 나가게 한다.
/// </summary>
void GameManager::ProcessSessionExit(ClientId id, Serializables::SessionExit& data)
{
	auto PlayerInfo = GameLobby.GetPlayerInfo(id);
	// nullptr check
	if(PlayerInfo == nullptr)
	{
		// Client가 접속하자마자 연결을 끊으면 등록된 정보가 없기 때문에 아무것도 하지 않는다.	
		return;
	}

	if(data.CurrentSessionId == PlayerInfo->SessionId_)
	{
		if(GameLobby.GetSessionInfo(data.CurrentSessionId) != nullptr)
		{
			GameLobby.SessionExit(id);
			if(GameLobby.GetSessionInfo(data.CurrentSessionId) == nullptr)
			{
				GamePool.RemoveSessionThreadMapping(data.CurrentSessionId);
			}
		}
	}
}

/// <summary>
/// Player가 Lobby에 있는지, session에 있는지 판단하고 그곳의 Player들에게 chat message를 보낸다.
/// </summary>
void GameManager::ProcessChat(ClientId id, Serializables::Chat& data)
{
	std::shared_ptr<SessionInfo> TargetSession = GameLobby.GetSessionInfo(GameLobby.GetPlayerInfo(id)->SessionId_);
	if(TargetSession == nullptr)
	{
		// Lobby에 있는 player들에게 chat 전달.
		if(data.To == LOBBY)
		{
			std::vector<std::shared_ptr<Avatar>> Players;

			GameLobby.GetPlayersInLobby(Players);
			for(auto& Player : Players)
			{
				Network::Send(Player->ClientId_, data);
			}
		}
		return;
	}
	// Game으로 전달
	if(TargetSession->IsInGame)
	{
		auto Chat = std::make_shared<Serializables::Chat>(data);
		DispatchMessageToThread(id, Chat);
		//Message* NewMessage = MemoryAllocator::MessageAllocator.alloc();
		//NewMessage->Data = Chat;
		//NewMessage->Dest = data.To;
		//NewMessage->From = id;
		//GamePool.DispatchMessageToThread(NewMessage);
	}
	// Session으로 전달
	else
	{
		for(auto& PlayerPair : TargetSession->Players)
		{
			Network::Send(PlayerPair.second->ClientId_, data);
		}
	}
}

/// <summary>
/// Player의 Ready상태를 바꾸고 해당 session에 접속한 Player들에게 상태변화를 보낸다.
/// </summary>
void GameManager::ProcessReady(ClientId id, Serializables::Ready& data)
{
	bool Success;
	bool Ready;
	if(data.IsReady)
	{
		Success = GameLobby.PlayerReady(id);
		Ready = true;
	}
	else
	{
		Success = GameLobby.PlayerUnReady(id);
		Ready = false;
	}
	if(Success)
	{
		Serializables::ReadyState NewState;
		// Ready의 상태를 같은 Session에 있는 Player들에게 보낸다.
		NewState.Id = id;
		NewState.IsReady = Ready;
		std::shared_ptr<Avatar> Player = GameLobby.GetPlayerInfo(id);
		if(Player != nullptr)
		{
			std::shared_ptr<SessionInfo> TargetSession = GameLobby.GetSessionInfo(Player->SessionId_);
			if(TargetSession != nullptr)
			{
				for(auto PlayerPair : TargetSession->Players)
				{
					// Ready 데이터를 보낸 Player를 제외한 Player들에게 상태변화를 보낸다.
					if(PlayerPair.second->ClientId_ != id)
						Network::Send(PlayerPair.second->ClientId_, NewState);
				}
			}
			// 모든 Player가 준비가 되었다면 GamePool에 Session을 추가하고 시작한다.
			if(GameLobby.IsEveryoneReady(Player->SessionId_) == true)
			{
				StartSession(Player->SessionId_);
			}
		}
	}
}

// Map을 변경해준다. 
void GameManager::ProcessMapSelect(ClientId id, Serializables::MapSelect& data)
{
	auto Player = GameLobby.GetPlayerInfo(id);
	if(Player != nullptr)
	{
		auto Info = GameLobby.GetSessionInfo(Player->SessionId_);
		if(Info != nullptr)
		{
			Info->Map = data.Map;
			LOG_INFO("Session #%d: Map changed to %d", Info->Id, data.Map);

			// 다른 플레이어들에게도 전달해준다.
			for(auto& PlayerPair : Info->Players)
			{
				Network::Send(PlayerPair.first, data);
			}
		}
		else
			LOG_WARNING("Can't find session #%d", Info->Id);
	}
	else
		LOG_WARNING("Can't find player #%d", Player->ClientId_);
}

// 해당 Id를 가진 Session을 시작한다.
bool GameManager::StartSession(SessionId id)
{
	// SessionInfo를 넘겨 그 정보를 기반으로 Session을 생성한다.
	std::shared_ptr<SessionInfo> Info = GameLobby.GetSessionInfo(id);
	if(Info != nullptr)
	{	
		if(!Info->IsInGame && !Info->Players.empty())
		{
			Info->IsInGame = true;
			GamePool.CreateSession(Info);
			return true;
		}
		else
		{
			if(Info->IsInGame)
			{
				LOG_WARNING("Failed to start Session# %d, session already in progress.", id);
				return false;
			}
			if(Info->Players.empty())
			{
				LOG_ERROR("Failed to start Session# %d, session is empty.", id);
				return false;
			}
		}
	}
	else
	{
		LOG_ERROR("Failed to start Session# %d, session ptr is nullptr.", id);
		return false;
	}

	return false;
}

