﻿#include "MapData.h"
#include "Protocol/Protocol.h"
#include "Protocol/Json_Formatter/JsonReader.h"
#include "Protocol/Log.h"
#include <string>
#include <cmath>

#ifdef TSCLIENT
#include "Misc/Paths.h"
#include "FileHelper.h"
#include "PlatformFileManager.h"
#endif

MapData* MapData::Instance_ = nullptr;
int MapData::HeightmapCount = 0;

// .raw, .r16 파일을 읽어 높이 정보를 저장한다.
// Scale이 적용된 정점 좌표를 저장한다.
void MapData::ReadRawHeightmap(HeightmapInfo& Info)
{
#ifdef TSCLIENT
	FString Path = (FPaths::ProjectContentDir() + HeightmapDir + Info.FileName.c_str());
	if(!FPlatformFileManager::Get().GetPlatformFile().FileExists(*Path))
		return;

	const int64 FileSize = FPlatformFileManager::Get().GetPlatformFile().FileSize(*Path);

	FString FileData;
	FFileHelper::LoadFileToString(FileData, *Path);
	IFileHandle* FileHandle = FPlatformFileManager::Get().GetPlatformFile().OpenRead(*Path);

	int RowSize = static_cast<int>(std::sqrt(FileSize / 2));
	int ColumnSize = RowSize;

	for(int i = 0; i < RowSize; ++i)
	{
		Info.Verts.emplace_back();
		for(int j = 0; j < ColumnSize; ++j)
		{
			Info.Verts[i].emplace_back();
			uint16_t uValue;
			FileHandle->Read(reinterpret_cast<uint8*>(&uValue), sizeof(uint16_t));
			// Z축 scale이 100%일 때 최소, 최대 Height는 +/- 256임
			Info.Verts[i][j].x() = j * Info.Scale.X + Info.Position.X;
			Info.Verts[i][j].y() = i * Info.Scale.Y + Info.Position.Y;
			Info.Verts[i][j].z() = (uValue / (float)(0xFFFF) - 0.5) * 2 * 256 * Info.Scale.Z + Info.Position.Z;
			//spdlog::info("{0}, {1}: {2}", Info.Verts[i][j].x(), Info.Verts[i][j].y(), Info.Verts[i][j].z());
		}
	}
#endif

#ifdef TSSERVER
	std::string Path = HeightmapDir + Info.FileName;
	std::ifstream Ifs;
	Ifs.open(Path.c_str(), std::fstream::in | std::fstream::binary);
	assert(Ifs.is_open());
	Ifs.seekg(0, std::ios::end);
	auto FileSize = Ifs.tellg();
	Ifs.seekg(0, std::ios::beg);


	int RowSize = static_cast<int>(std::sqrt(FileSize / 2));
	int ColumnSize = RowSize;

	for(int i = 0; i < RowSize; ++i)
	{
		Info.Verts.emplace_back();
		for(int j = 0; j < ColumnSize; ++j)
		{
			Info.Verts[i].emplace_back();
			uint16_t uValue;
			Ifs.read(reinterpret_cast<char*>(&uValue), sizeof(uint16_t));
			// Z축 scale이 100%일 때 최소, 최대 Height는 +/- 256임
			Info.Verts[i][j].x() = j * Info.Scale.X + Info.Position.X;
			Info.Verts[i][j].y() = i * Info.Scale.Y + Info.Position.Y;
			Info.Verts[i][j].z() = (uValue / (float)(0xFFFF) - 0.5) * 2 * 256 * Info.Scale.Z + Info.Position.Z;
			//spdlog::info("{0}, {1}: {2}", Info.Verts[i][j].x(), Info.Verts[i][j].y(), Info.Verts[i][j].z());
		}
	}
#endif
	Info.Id = ++HeightmapCount;

	//spdlog::info("Heightmap: {0} loaded!", Info.FileName);
	LOG_INFO("Heightmap: %s loaded!", Info.FileName.c_str());
}

void MapData::ReadLandscape(MapInfo& Info)
{
	for(auto& Element : Info.Heightmaps)
	{
		ReadRawHeightmap(Element);
	}
}

MapData::MapData()
{
	JsonFormatter::JsonReader Reader;

#ifdef TSSERVER
	std::string Path = MapDataDir;
#endif

#ifdef TSCLIENT
	std::string Path = TCHAR_TO_ANSI(*(FPaths::ProjectContentDir() + MapDataDir));
	LOG_INFO("Path: %s", Path.c_str());
	if (!ImportMapInfoAsString((Path + "LandScape.json").c_str(), MapType::Landscape))
		LOG_ERROR("Can't import LandScape.json");
	if (!ImportMapInfoAsString((Path + "Stage01.json").c_str(), MapType::Stage01))
		LOG_ERROR("Can't import Stage01.json");
	if (!ImportMapInfoAsString((Path + "Stage02.json").c_str(), MapType::Stage02))
		LOG_ERROR("Can't import Stage02.json");
	if (!ImportMapInfoAsString((Path + "Stage03.json").c_str(), MapType::Stage03))
		LOG_ERROR("Can't import Stage03.json");
#endif

#ifdef TSSERVER
	//Data.insert(std::make_pair(MapType::NewMap1, MapInfo()));
	//assert(Reader.ImportMapInfo((Path + "NewMap1.json").c_str(), Data[MapType::NewMap1]));
	Data.insert(std::make_pair(MapType::Landscape, MapInfo()));
	Reader.ImportMapInfo((Path + "LandScape.json").c_str(), Data[MapType::Landscape]);
	Data.insert(std::make_pair(MapType::Stage01, MapInfo()));
	Reader.ImportMapInfo((Path + "Stage01.json").c_str(), Data[MapType::Stage01]);
	Data.insert(std::make_pair(MapType::Stage02, MapInfo()));
	Reader.ImportMapInfo((Path + "Stage02.json").c_str(), Data[MapType::Stage02]);
	Data.insert(std::make_pair(MapType::Stage03, MapInfo()));
	Reader.ImportMapInfo((Path + "Stage03.json").c_str(), Data[MapType::Stage03]);
#endif

	//... add more

	//spdlog::info("Loading Heightmaps...");
	LOG_INFO("Loading Heightmaps... %d", Data.size());
	for(auto& Info : Data)
	{
		ReadLandscape(Info.second);
	}
	//spdlog::info("...Heightmaps loaded completely");
	LOG_INFO("... Heightmaps loaded completely.");
}

MapData::~MapData()
{
}

const TheShift::MapInfo& MapData::operator[](MapType Type) const
{
	return Data.at(Type);
}

const TheShift::MapInfo& MapData::GetMapInfo(MapType Type) const
{
	return Data.at(Type);
}
