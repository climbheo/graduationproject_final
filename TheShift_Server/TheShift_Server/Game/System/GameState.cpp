﻿#include "GameState.h"
#include "Game/System/MapData.h"
#include <functional>
#include "Protocol/Log.h"

#include "Game/Components/BoundingVolume.h"
#include "Game/Components/Heightmap.h"
#include "Game/Components/InventoryComponent.h"
#include "Game/Components/ItemComponent.h"
#include "Game/Components/ArmorItemComponent.h"
#include "Game/Components/WeaponItemComponent.h"
#include "Game/Actors/Character.h"
#include "Game/Actors/Trigger.h"
#include "Game/Actors/KnightMonster.h"
#include "Game/Actors/Spider.h"
#include "Game/Actors/FatMonster.h"
#include "Game/Actors/SwordMonster.h"
#include "Game/Actors/WereWolf.h"
#include "Game/System/CollisionResolver.h"

#ifdef TSCLIENT
#include "Network/Components/TSNetworkedComponent.h"
#include "EngineMinimal.h"
#endif

using namespace std::chrono_literals;

namespace TheShift
{

GameState::GameState() : TimePassed(0)
{
	CollisionResolver_ = new CollisionResolver(this);
}

/// <summary>
/// Game state를 update한다.
/// Player가 있으면 true를 return하고 없으면 false를 return한다.
/// </summary>
bool GameState::Update(std::chrono::nanoseconds deltaTime)
{
#ifdef TSSERVER
	// 남아있는 Player가 아무도 없다면 종료한다.
	if(Players.size() == 0)
	{
		return false;
	}

	if(AllPlayerLoaded)
	#endif

	#ifdef TSCLIENT
		if(MapLoaded)
		#endif

			if(true)
			{
				TimePassed += deltaTime;


			#ifdef TSSERVER
				auto UpdateData = std::make_shared<Serializables::SessionStateUpdate>();
				//auto CollisionDebugData = std::make_shared<Serializables::CollisionDebug>();
				bool Changed = false;
				JobTimer.Poll();
			#endif

				//bool SendCollisionDebug = false;
				//CollisionDebugDuration += deltaTime;
				//
				//if(CollisionDebugDuration > 16ms)
				//{
				//	CollisionDebugDuration = 0ns;
				//	SendCollisionDebug = true;
				//}
				//else
				//{
				//	SendCollisionDebug = false;
				//}
				for(UID Id : Garbage)
				{
					DestroyActor(Id);
				}

				Garbage.clear();
		

				for(auto& ActorPair : Actors)
				{
					ActorPair.second->Update(deltaTime.count() * 1e-9f);
				}

#ifdef TSCLIENT
				// Character위치 보간
				if (ReconciliationAvailable)
				{
					float ReconciliationAmount = deltaTime / ReconciliationDuration;
					if (ReconciliationAmount > 1.f)
					{
						ReconciliationAmount = 1.f;
					}
					ReconciliationDelta += deltaTime;
					Eigen::Vector3f ReconciliationTranslation = ReconciliationDirection * (ReconciliationLength * ReconciliationAmount);
					auto MyActor = Actors[MyActorId];
					// MyActor를 DummyActor에 대해 보간시켜준다.
					if (MyActor != nullptr)
					{
						//MyActor->SetLastTranslation();
						MyActor->Translate(ReconciliationTranslation);
					}
					// 내 Actor를 찾을 수 없음
					else
					{
						LOG_WARNING("Can't do reconciliation. Can't find my actor %d", MyActorId);
					}

					if (ReconciliationDelta > ReconciliationDuration)
					{
						ReconciliationAvailable = false;
					}
				}
#endif

				//auto CollisionStartTime = std::chrono::high_resolution_clock::now();
				// 충돌처리
				CollisionResolver_->ResolveCollisions(deltaTime.count() * 1e-9f);

				//auto CollisionDuration = std::chrono::high_resolution_clock::now() - CollisionStartTime;

				for(auto& ActorPair : Actors)
				{
					ActorType Type = ActorPair.second->GetType();
					if(Type == ActorType::Prop
					   || Type == ActorType::Interactable
					   || Type == ActorType::Destructible
					   || Type == ActorType::Trigger)
						continue;

					if(ActorPair.second->GetType() == ActorType::Player)
					{
						Serializables::MyCharacterSyncState Data;
						Data.LastProcessedInputId = std::reinterpret_pointer_cast<Character>(ActorPair.second)->GetLastProcessedInputId();
						Data.MyActorId = ActorPair.first;
						Serializables::ActorPosition PosData;
						PosData.Self = ActorPair.first;
						if (PosData.Self == 0)
						{
							LOG_WARNING("Actor UID is invalid.");
						}
						PosData.Position = ActorPair.second->GetTransform().GetWorldTranslation();
						PosData.Rotation = ActorPair.second->GetTransform().GetWorldDegrees();
						PosData.Speed = ActorPair.second->GetVelocity();
						Data.MyCharacterPosition = PosData;
						auto TargetPlayerPair = std::find_if(Players.begin(), Players.end(), [ActorPair](std::pair<ClientId, std::shared_ptr<Avatar>> Element)
									 {
										 return Element.second->ActorId_ == ActorPair.first;
									 });
						Network::Send(TargetPlayerPair->first, Data);
					}

				#ifdef TSCLIENT
					// 내 Character만 UE Actor의 Location을 set해준다.
					if(ActorPair.first == MyActorId)
					{
						auto Translation = ActorPair.second->GetTransform().GetWorldTranslation();
						LOG_INFO("Translation: %f %f %f", Translation.x(), Translation.y(), Translation.z());
						auto UEActor = GetUEActor(ActorPair.first);
						if(UEActor != nullptr)
							UEActor->SetActorLocation(FVector(Translation.x(), Translation.y(), Translation.z()));
					}
				#endif

				#ifdef TSSERVER
					Serializables::ActorPosition ActorPos;

					//Eigen::Vector3f Speed = ActorPair.second->GetVelocity();
					//if (!Speed.isZero())
					//    ActorPair.second->Translate(ActorPair.second->GetVelocity() * (deltaTime.count() / 1000.f));
					//TODO: 변화가 있는 Actor만 보내야 함.
					ActorPos.Self = ActorPair.first;
					if(ActorPos.Self == 0)
					{
						LOG_WARNING("Actor UID is invalid.");
					}
					ActorPos.Position = ActorPair.second->GetTransform().GetWorldTranslation();
					ActorPos.Rotation = ActorPair.second->GetTransform().GetWorldDegrees();
					ActorPos.Speed = ActorPair.second->GetVelocity();
					UpdateData->ActorPositions.Array.emplace_back(ActorPos);


					//// Collsion Debug
					//auto Volumes = ActorPair.second->GetBoundingVolumes();
					//for(auto Volume : Volumes)
					//{
					//	Serializables::BoundingVolumeDebug VolumeInfoForDebug;
					//	VolumeInfoForDebug.Position = Volume->GetTransform().GetWorldTranslation();
					//	VolumeInfoForDebug.Rotation = Volume->GetTransform().GetWorldDegrees();;
					//	VolumeInfoForDebug.BoundingVolumeType_ = Volume->GetVolumeType();
					//	VolumeInfoForDebug.ScaledExtent = Volume->GetRearrangedExtent();
					//	VolumeInfoForDebug.HalfHeight = Volume->GetHalfHeight();
					//	VolumeInfoForDebug.Radius = Volume->GetRadius();
					//	CollisionDebugData->Info.Array.push_back(VolumeInfoForDebug);

					//	//spdlog::debug("Volume Position: {0}, {1}, {2}", VolumeInfoForDebug.Position.X, VolumeInfoForDebug.Position.Y, VolumeInfoForDebug.Position.Z);
					//}

					//spdlog::info("Id: {0}", ActorPos.Self);
					//spdlog::info("Locataion: X: {0}, Y: {1}, Z: {2}", ActorPos.Position.X, ActorPos.Position.Y, ActorPos.Position.Z);
					//spdlog::info("Rotation: X: {0}, Y: {1}, Z: {2}", ActorPos.Rotation.X, ActorPos.Rotation.Y, ActorPos.Rotation.Z);
					//spdlog::info("Speed: X: {0}, Y: {1}, Z: {2}", ActorPos.Speed.X, ActorPos.Speed.Y, ActorPos.Speed.Z);
					Changed = true;
				#endif
				}

				//if(!Acts.empty())
				//	Changed = true;

				//while(!Acts.empty())
				//{
				//	Serializables::Act& Act = Acts.front();
				//	UpdateData->Acts.Array.emplace_back(Act);

				//	Acts.pop();
				//}

			#ifdef TSSERVER
				if(Changed)
				{
					DataToBeSent.emplace_back(UpdateData);

					//if(SendCollisionDebug)
					//{
					//	DataToBeSent.emplace_back(CollisionDebugData);
					//	//spdlog::debug("Collision Debug send");
					//}
				}

				//LOG_INFO("Collision Duration: \t%d", CollisionDuration.count());
			#endif
			}
	return true;
}

/// <summary>
/// 받은 Message를 처리한다.
/// </summary>
/// TODO: base class에서 ClassId를 불러서 type을 구분할 수 있으면 TypeOfMessage를 넣을 필요가 없잖아... Test해보기
#ifdef TSSERVER
void GameState::ApplyMessage(Message* message)
{
	switch(static_cast<Serializables::Type>(message->Data->ClassId()))
	{
	case Serializables::Type::SessionJoinReq:
		ProcessSessionJoinReq(message);
		break;

	case Serializables::Type::SessionExit:
		ProcessSessionExit(message);
		break;

	case Serializables::Type::LoadCompleted:
		ProcessLoadCompleted(message);
		break;

	case Serializables::Type::Input:
		ProcessInput(message);
		break;

	case Serializables::Type::SwapWeapon:
		ProcessSwapWeapon(message);
		break;

	case Serializables::Type::SwapArmor:
		ProcessSwapArmor(message);
		break;

	case Serializables::Type::ActWithTargets:
		ProcessActWithTargets(message);
		break;

	case Serializables::Type::LevelChangeReqDebug:
		ProcessLevelChangeReqDebug(message);
		break;

	case Serializables::Type::DirectiveAct:
		ProcessDirectiveAct(message);
		break;

	case Serializables::Type::InteractableAct:
		ProcessInteractableAct(message);
		break;

	case Serializables::Type::ItemAcquireFromChestReq:
		ProcessItemAcquireFromChestReq(message);
		break;

	case Serializables::Type::ItemAcquire:
		ProcessItemAcquire(message);
		break;

	case Serializables::Type::ItemEquip:
		ProcessItemEquip(message);
		break;

	case Serializables::Type::ItemUnacquire:
		ProcessItemUnacquire(message);
		break;

	case Serializables::Type::ItemUnequip:
		ProcessItemUnequip(message);
		break;
	case Serializables::Type::TriggerNotify:
		ProcessTriggerNotify(message);
		break;

	case Serializables::Type::Cheat:
		ProcessCheat(message);
		break;

	case Serializables::Type::Unhandled:
		LOG_WARNING("GameState: Unhandled message from %d", message->From);
		break;

	default:
		LOG_WARNING("GameState: Unhandled message from %d", message->From);
		break;
	}
}
#endif

#ifdef TSSERVER
std::vector<std::shared_ptr<Serializables::SerializableData>>* GameState::GetDataToBeSent()
{
	return &DataToBeSent;
}
#endif

#ifdef TSSERVER
void GameState::ClearDataToBeSent()
{
	DataToBeSent.clear();
}
#endif

#ifdef TSSERVER
void GameState::AddNewPlayer(std::shared_ptr<Avatar> player)
{
	Players.insert(std::make_pair(player->ClientId_, player));
}
#endif

#ifdef TSSERVER
std::map<ClientId, std::shared_ptr<Avatar>>* GameState::GetPlayers()
{
	return &Players;
}
#endif

std::map<UID, std::shared_ptr<Actor>>* GameState::GetActors()
{
	return &Actors;
}

std::list<std::shared_ptr<Actor>> GameState::GetActorsByType(ActorType Type)
{
	std::list< std::shared_ptr<Actor>> ActorLst;
	
	for (auto& iter : Actors)
	{
		if (iter.second->GetType() == Type)
			ActorLst.emplace_back(iter.second);
	}

	return ActorLst;
}

std::list<std::shared_ptr<Actor>> GameState::GetActorsByTypes(ActorType* Types, int TypeNum)
{
	std::list< std::shared_ptr<Actor>> ActorLst;
	
	for (auto& iter : Actors)
	{
		for (int i = 0; i < TypeNum; ++i)
		{
			if (iter.second->GetType() == Types[i])
				ActorLst.emplace_back(iter.second);
		}
	}

	return ActorLst;
}

std::shared_ptr<Actor> GameState::GetNearestActorByType(ActorType Type, Actor* SrcActor, std::list<std::shared_ptr<Actor>>* ActorsToExecpt)
{
	auto ActorLst = GetActorsByType(Type);
	
	return GetNearestActor(&ActorLst, SrcActor, ActorsToExecpt);
}

std::shared_ptr<Actor> GameState::GetNearestActorByTypes(ActorType* Types, int TypeNum, Actor* SrcActor, std::list<std::shared_ptr<Actor>>* ActorsToExecpt)
{
	auto ActorLst = GetActorsByTypes(Types, TypeNum);

	return GetNearestActor(&ActorLst, SrcActor, ActorsToExecpt);
}

/// <summary>
/// 저장된 Actor들의 map pointer를 return한다.
/// operator[] 대신 .at(keyType) 을 사용해주세요... const라 operator[] 안됨
/// </summary>
const std::map<UID, std::shared_ptr<Actor>>* GameState::GetActors() const
{
	return &Actors;
}

// 해당 Id의 Actor를 return한다.
std::shared_ptr<TheShift::Actor> GameState::GetActor(UID Id)
{
	if(Actors.find(Id) == Actors.end())
	{
		LOG_WARNING("Actor #%d does not exist.", Id);
		return nullptr;
	}
	else
		return Actors.at(Id);
}

const std::vector<std::shared_ptr<TheShift::Heightmap>>& GameState::GetHeightmaps() const
{
	return Heightmaps;
}

#ifdef TSSERVER
TheShift::MapType GameState::GetMapType() const
{
	return Map;
}
#endif

//#ifdef TSSERVER
//void GameState::AddAct(Serializables::Act& act)
//{
//	Acts.push(act);
//}
//#endif

/// <summary>
/// Timer에 Job을 추가합니다.
/// Callable 객체인 Callee는 시간이 member인 TimePoint를 지난 경우 자동으로 실행됩니다.
/// </summary>
#ifdef TSSERVER
void GameState::AddToTimer(std::shared_ptr<TimerCallee> Callee)
{
	JobTimer.Push(Callee);
}

void GameState::BroadcastPacket(Serializables::SerializableData& Data)
{
	for(auto PlayerPair : Players)
	{
		Network::Send(PlayerPair.first, Data);
	}
}

// 해당 Tag를 가진 Actor들을 spawn한다.
void GameState::SpawnActorsWithTagAndBroadcast(const char* Tag)
{
	auto RangePair = PlacementInfo.equal_range(Tag);
	for(auto Iter = RangePair.first; Iter != RangePair.second; ++Iter)
	{
		//TODO: 패킷 하나로 줄이는게 좋음. 한꺼번에 보내도록.
		Serializables::ActorCreated Packet;
		Packet.ActorId = SpawnActor(Iter->second.Type, Iter->second.ActorPosition);
		Packet.Position = Iter->second.ActorPosition;
		Packet.Type = Iter->second.Type;
		BroadcastPacket(Packet);
	}
}

void GameState::ProcessDieEvent(std::shared_ptr<Actor> DyingActor)
{
	LOG_INFO("Monster dead. Tag name: %s", DyingActor->GetTag());
	if(DyingActor->GetTag() == "Unlock_CellDoor3")
	{
		Serializables::Event Data;
		Data.Type = EventType::Stage01_CellDoor3Unlock;
		BroadcastPacket(Data);
	}
	else if(DyingActor->GetTag() == "Unlock_CellDoor3")
	{
		Serializables::Event Data;
		Data.Type = EventType::Stage01_CellDoor4Unlock;
		BroadcastPacket(Data);
	}
}

void GameState::SetUpdateTimestep(float NewTimestep)
{
	UpdateTimestep = NewTimestep;
}

#endif

TheShift::CollisionResolver* GameState::GetCollisionResolver()
{
	return CollisionResolver_;
}

/// <summary>
/// 해당 map을 불러온다.
/// </summary>
void GameState::Init(MapType map)
{
	Map = map;
	if(map != MapType::Unhandled)
		LoadMap(map);
	else
		LOG_ERROR("Map type is not valid. Can't Init.");

	// Unhandled이면 아무것도 하지 않음.
	// 나중에 게임이 시작 되었을 때 map이 전달되면 그때 초기화 함.
	// LoadMap을 부르면 됨
}

void GameState::LoadMap(MapType map)
{
#ifdef TSSERVER
	for(int i = 0; i < Acts.size(); ++i)
		Acts.pop();
#endif

	CollisionResolver_->Reset();
	PlacementInfo.clear();
	ActorStat.clear();
	Heightmaps.clear();
	StartPointIndices.clear();
	Actors.clear();
	Interactables.clear();

	Map = map;

	// MapInfo를 읽어온다.
	auto& CurrentMapInfo = MapData::GetInstance().GetMapInfo(map);

	// Heightmap들을 읽어온다.
	for(const auto& Heightmap : CurrentMapInfo.Heightmaps)
	{
		Heightmaps.emplace_back(std::make_shared<TheShift::Heightmap>(Heightmap.Verts));
	}

	// Actor들의 정보를 읽어온다.
	for(auto& ActorTypeInfo : CurrentMapInfo.ActorTypeInfo)
	{
		ActorStat.insert(std::make_pair(ActorTypeInfo.Type, ActorTypeInfo));
	}

	// Map의 배치 정보를 저장한다. 
	for(auto& ActorPlacement : CurrentMapInfo.ActorPlacements)
	{
		PlacementInfo.insert(std::make_pair(ActorPlacement.Tag, ActorPlacement));
	}

	if(PlacementInfo.empty())
	{
		LOG_ERROR("MapData is empty.");
	}
	else
	{
		LOG_INFO("MapData is not empty.");
	}

	// Start Point index들을 추가한다.
	for(int i = 0; i < PlacementInfo.count("Start Point"); ++i)
	{
		StartPointIndices.insert(std::make_pair(INVALID_CLIENTID, i));
	}


	// 처음 로드되어야 할 Actor들 CreateActor 하기
	// Tag(Key)가 ""이면 맵 로드할때 모두 생성한다.
	//auto ObjectsIter = PlacementInfo.lower_bound("");
	//auto ObjectsEnd = PlacementInfo.upper_bound("");
	//for(; ObjectsIter != ObjectsEnd; ObjectsIter++)
	for(auto& ObjectsIter : PlacementInfo)
	{
		// Tag가 비어있지 않고 Interactable이 아니라면 넘긴다.
		// 문과 같이 Tag가 있고 Interactable인 애들만 처음에 초기화 할 때 생성한다.
		// Destructible도 넘기지 않는다.
		//if (ObjectsIter.second.Type != ActorType::Interactable && !ObjectsIter.second.Tag.empty() && ObjectsIter.second.Type != ActorType::Destructible)
			//continue;
		if (ObjectsIter.second.Type == ActorType::Player)
			continue;

	#ifdef TSCLIENT
		if(ObjectsIter.second.Type != ActorType::Prop && ObjectsIter.second.Type != ActorType::Player)
		{
			continue;
		}
	#endif

		UID NewActorId = SpawnActor(ObjectsIter.second.Type, ObjectsIter.second.ActorPosition);
		if(NewActorId != INVALID_UID)
		{
			if(Actors.find(NewActorId) == Actors.end())
			{
				LOG_WARNING("Can't find actor. UID: %d", NewActorId);
				break;
			}
			else
			{
				Actors[NewActorId]->SetTag(ObjectsIter.second.Tag);
				Actors[NewActorId]->SetRotationWithDegrees(ObjectsIter.second.ActorRotation);
				if (ObjectsIter.second.Type == ActorType::Prop
					|| ObjectsIter.second.Type == ActorType::Interactable
					|| ObjectsIter.second.Type == ActorType::Destructible
					|| ObjectsIter.second.Type == ActorType::Trigger)
				{
					// Is this actor Movable?
					Actors[NewActorId]->SetIsMovable(ObjectsIter.second.IsMovable);
					CollisionResolver_->CreateBoundingVolumes(Actors[NewActorId], ObjectsIter.second.BoundInfo);
				}

				if(ObjectsIter.second.Type == ActorType::Prop || ObjectsIter.second.Type == ActorType::Interactable)
				{
					Actors[NewActorId]->SetIsMovable(ObjectsIter.second.IsMovable);
					//CollisionResolver_->CreateBoundingVolumes(Actors[NewActorId], ObjectsIter.second.BoundInfo);
				}

				// Tag가 있는 actor라면 Interactables에 추가한다.
				if (!ObjectsIter.first.empty())
				{
					Interactables.insert(std::make_pair(ObjectsIter.first, NewActorId));
					LOG_INFO("Interactable added. Id: %d Tag: %s", NewActorId, ObjectsIter.first);
				}

				// Destructible 설정
				if(ObjectsIter.second.Type == ActorType::Destructible)
				{
					LOG_INFO("Destructible created.");
					Actors[NewActorId]->SetActorType(ActorType::Destructible);
					Actors[NewActorId]->SetHp(200.f);
					Actors[NewActorId]->SetCanBeDamaged();
				}
				//CollisionResolver_->CreateBoundingVolumes(Actors[NewActorId], ObjectsIter.second.BoundInfo);

				// Item이 있다면 inventory를 추가해준다.
				if(!ObjectsIter.second.InventoryInfo.empty())
				{
					auto TargetActor = Actors[NewActorId];

					for(auto& ItemInfo : ObjectsIter.second.InventoryInfo)
					{
						std::shared_ptr<ItemComponent> NewItem;
						switch(static_cast<ItemType>(ItemInfo.ItemCategory))
						{
						case ItemType::Weapon:
							NewItem = std::make_shared<WeaponItemComponent>();
							std::reinterpret_pointer_cast<WeaponItemComponent>(NewItem)->WeaponType = static_cast<WeaponItemType>(ItemInfo.ItemType);
							break;

						case ItemType::Armor:
							NewItem = std::make_shared<ArmorItemComponent>();
							std::reinterpret_pointer_cast<ArmorItemComponent>(NewItem)->ArmorType = static_cast<ArmorItemType>(ItemInfo.ItemType);
							break;

						default:
							break;
						}

						NewItem->ItemCategory = static_cast<ItemType>(ItemInfo.ItemCategory);

						// Item을 추가해준다.
						TargetActor->MakeInventory();
						TargetActor->GetInventoryComp()->AcquireItem(NewItem);
					}
				}

			//#ifdef TSSERVER
			//	if(ObjectsIter->second.Type != ActorType::Prop && ObjectsIter->second.Type != ActorType::Player)
			//	{
			//		Serializables::ActorCreated Data;
			//		Data.ActorId = NewActorId;
			//		Data.Position = ObjectsIter->second.ActorPosition;
			//		Data.Type = ObjectsIter->second.Type;
			//		BroadcastPacket(Data);
			//	}
			//#endif
			}
		}

		
	}

	switch(map)
	{
	case MapType::TestMap:
		break;

	default:
		break;
	}

#ifdef TSSERVER
	// 마지막으로 Player들 초기화
	for(auto& PlayerPair : Players)
	{
		InitPlayer(ActorType::Player, PlayerPair.first);
	}
#endif

	// Map이 바뀌었으면 시간은 0으로 set해준다.
	TimePassed = std::chrono::nanoseconds::zero();

#ifdef TSCLIENT
	MapLoaded = true;
#endif

	LOG_INFO("Map: %d loaded.", map);
}

/// <summary> 
/// Game 시작에 대한 처리를 한다.
/// </summary>
//TODO: 다른 일도 동반해야 할 것...?
#ifdef TSSERVER
void GameState::StartGame()
{
	auto Data = BuildSessionStarted();
	//spdlog::info("Players Size: {0}", Data.Players.Array.size());
	LOG_INFO("Players Size: %d", Data.Players.Array.size());

	BroadcastPacket(Data);
}
#endif

#ifdef TSCLIENT
// GameState에서 UE Actor를 접근할 수 있도록 등록한다.
void GameState::RegisterNetworkComps(TMap<UID, UTSNetworkedComponent*>* Container)
{
	NetworkComps = Container;
	LOG_INFO("NetworkComps registered.");
}

// GameState에서 UE용 Actor에 접근해야할 때 사용.
// AActor의 pointer를 return한다.
AActor* GameState::GetUEActor(UID Id)
{
	auto Comp = NetworkComps->Find(Id);
	if(Comp != nullptr)
	{
		return (*Comp)->GetOwner();
	}
	return nullptr;
}

TheShift::UID GameState::GetMyId() const
{
	return MyActorId;
}

// UE에서 Actor의 정보가 필요할 경우 TheShift::Actor의 pointer를 return한다.
std::shared_ptr<TheShift::Actor> GameState::GetCorrespondingActor(UID Id)
{
	auto RetVal = Actors.find(Id);
	if(RetVal != Actors.end())
		return RetVal->second;
	return nullptr;
}

void GameState::SetMyActorId(UID Id)
{
	MyActorId = Id;
}

#endif


#ifdef TSSERVER
void GameState::ProcessSessionExit(Message* message)
{
	if(Players.find(message->From) != Players.end())
	{
		UID ActorId = Players.at(message->From)->ActorId_;
		Players.erase(message->From);
		//UID ActorId = std::reinterpret_pointer_cast<Serializables::Disconnect>(message->Data)->ActorId;
		DestroyActor(ActorId);

		// 남아있는 Player들에게 접속을 종료한 Player가 있음을 알린다.
		Serializables::PlayerDisconnect ToRemainings;
		ToRemainings.DisconnectingClientId = message->From;

		for(auto& Player : Players)
		{
			Network::Send(Player.first, ToRemainings);
		}

		if(StartPointIndices.find(message->From) != StartPointIndices.end())
		{
			int StartPointNum = StartPointIndices[message->From];
			StartPointIndices.erase(message->From);
			StartPointIndices.insert(std::make_pair(INVALID_CLIENTID, StartPointNum));
		}
		//spdlog::info("GameState: Player destroyed {0}", message->From);
		LOG_INFO("GameState: Player destroyed %d", message->From);
	}
}
#endif

/// <summary>
/// Player들로부터 Load가 완료되었다는 정보를 수신하면 Game update를 시작한다.
/// </summary>
#ifdef TSSERVER
void GameState::ProcessLoadCompleted(Message* message)
{
	if(!AllPlayerLoaded)
	{
		auto Data = std::reinterpret_pointer_cast<Serializables::LoadCompleted>(message->Data);

		if(Players.find(message->From) != Players.end())
		{
			Players[message->From]->IsLoaded_ = true;
		}
		else
		{
			return;
		}

		for(auto& PlayerPair : Players)
		{
			if(!PlayerPair.second->IsLoaded_)
			{
				return;
			}
		}

		Serializables::GameStarted GameStartData;
		GameStartData.Started = true;
		BroadcastPacket(GameStartData);
		
		AllPlayerLoaded = true;
	}
	// 이미 모두 준비가 된 상태에서 새로 들어온 상황이라면 해당 player에게만 보낸다.
	else
	{
		if (Players.find(message->From) != Players.end())
		{
			Players[message->From]->IsLoaded_ = true;
		}
		else
		{
			return;
		}

		Serializables::GameStarted GameStartData;
		GameStartData.Started = true;
		Network::Send(message->From, GameStartData);
	}
}
#endif

#ifdef TSSERVER
void GameState::ProcessInput(Message* message)
{
	auto InputData = std::reinterpret_pointer_cast<Serializables::Input>(message->Data);
	UID ActorId = 0;
	if(Players.find(message->From) != Players.end())
	{
		ActorId = Players[message->From]->ActorId_;
	}
	else
		return;
	std::shared_ptr<Actor> TargetActor = nullptr;
	if(Actors.find(ActorId) != Actors.end())
	{
		TargetActor = Actors[ActorId];
		//Actors[ActorId]->OnInput(InputData->InputValue);
	}
	else
		return;

	// Input 실행 (CanAct가 true일 경우에만 실행한다.)
	//TODO: Client측에서도 처리해주어야 한다.
	//if(TargetActor->GetCanAct())
		TargetActor->OnInput(*InputData);
}

// Player가 무기변경을 할 경우
void GameState::ProcessSwapWeapon(Message* message)
{
	// Player validity check
	if(Players.find(message->From) == Players.end())
		return;

	auto Data = std::reinterpret_pointer_cast<Serializables::SwapWeapon>(message->Data);

	// Actor id validity check
	UID ActorId = Players[message->From]->ActorId_;
	if(ActorId != Data->Id)
		return;

	// Actor validity check
	auto TargetActor = Actors.find(ActorId);
	if(TargetActor == Actors.end())
		return;

	// Swap 처리
	std::reinterpret_pointer_cast<Character>(TargetActor->second)->SwapWeapon(static_cast<WeaponType>(Data->SwapType));

	//TODO: 무기 변경에 성공 했다면 packet을 보낸다. 무기 변경을 할 수 없는 상황이라면 보내지 말아야 함.

	// 무기를 변경했음을 모든 client들에게 알린다.
	Serializables::ActWithValue SwapData;
	SwapData.Self = ActorId;
	SwapData.Type = ActType::WeaponSwap;
	SwapData.Value = Data->SwapType;
	BroadcastPacket(SwapData);

	LOG_INFO("Swap data sent.");
}

// Player가 방어구를 변경한 경우.
void GameState::ProcessSwapArmor(Message* message)
{
	// Player validity check
	if (Players.find(message->From) == Players.end())
		return;

	auto Data = std::reinterpret_pointer_cast<Serializables::SwapArmor>(message->Data);

	// Actor id validity check
	UID ActorId = Players[message->From]->ActorId_;
	if (ActorId != Data->Id)
		return;

	// Actor validity check
	auto TargetActor = Actors.find(ActorId);
	if (TargetActor == Actors.end())
		return;

	// Swap 처리
	std::reinterpret_pointer_cast<Character>(TargetActor->second)->SwapArmor(static_cast<ArmorItemType>(Data->SwapType));

	// 무기를 변경했음을 모든 client들에게 알린다.
	Serializables::ActWithValue SwapData;
	SwapData.Self = ActorId;
	SwapData.Type = ActType::ArmorSwap;
	SwapData.Value = static_cast<int>(Data->SwapType);
	BroadcastPacket(SwapData);

	LOG_INFO("Swap data sent: Armor type: %d", Data->SwapType);
}

#endif


#ifdef TSSERVER
void GameState::ProcessSessionJoinReq(Message* message)
{
	if(Players.find(message->From) == Players.end())
		return;

	// Character 생성 ( 생성 위치중 한 곳에 )
	UID NewPlayerActorUID = InitPlayer(ActorType::Player, message->From);
	if(NewPlayerActorUID == INVALID_UID)
		return;

	auto JoinReq = std::reinterpret_pointer_cast<Serializables::SessionJoinReq>(message->Data);


	// 새로 들어온 Player에게 현재 Game의 상태를 저장한다.
	Serializables::SessionStarted ToNewPlayer;
	for(auto& PlayerPair : Players)
	{
		Serializables::AvatarInfo Info;
		Info.Player = *PlayerPair.second;
		ToNewPlayer.Players.Array.emplace_back(Info);
	}
	ToNewPlayer.Map = Map;
	ToNewPlayer.ServerUpdateTimestep = UpdateTimestep;

	// 현재 맵에 배치된 Actor들의 정보를 저장한다.
	for(auto& ActorPair : Actors)
	{
		// Map에 배치되는 Prop들은 보내지 않는다.
		if(ActorPair.second->GetType() != ActorType::Prop /*&& ActorPair.second->GetType() != ActorType::Player*/)
		{
			Serializables::ActorInfo Info;

			Info.Type = ActorPair.second->GetType();
			Info.ActorId = ActorPair.second->GetId();
			Info.Hp = ActorPair.second->GetHp();
			Info.Position = ActorPair.second->GetTransform().GetTranslation();

			ToNewPlayer.Actors.Array.emplace_back(Info);
		}
	}

	// 새로 들어온 Player에게 Packet을 보낸다.
	Network::Send(message->From, ToNewPlayer);

	Serializables::ToNewPlayerOldPlayerWeapons ToNewPlayerWeaponData;
	// 기존 Player들의 무기 정보를 새로 들어온 player에게 보낸다.
	for(auto& OldPlayer : Players)
	{
		// 새로 들어온 player에 대해서는 처리하지 않는다.
		if(OldPlayer.first != message->From)
		{
			auto OldPlayerActorIter = Actors.find(OldPlayer.second->ActorId_);
			if (OldPlayerActorIter == Actors.end())
				continue;

			// 기존 player의 weapon을 swap시키는 형식으로 기존 player의 무기를 동기화시킨다.
			auto OldPlayerActor = std::reinterpret_pointer_cast<Character>(OldPlayerActorIter->second);
			Serializables::ActWithValue OldPlayerWeaponData;
			OldPlayerWeaponData.Self = OldPlayer.second->ActorId_;
			OldPlayerWeaponData.Type = ActType::WeaponSwap;
			OldPlayerWeaponData.Value = static_cast<int>(OldPlayerActor->GetCurrentWeaponType());

			ToNewPlayerWeaponData.SwapWeapons.Array.emplace_back(OldPlayerWeaponData);
		}
	}

	Network::Send(message->From, ToNewPlayerWeaponData);

	// 기존에 있던 Player들에겐 새로 들어온 Player의 상태를 보내준다.
	Serializables::NewPlayer ToOldPlayers;

	ToOldPlayers.Player = *Players[message->From];

	Serializables::ActorInfo NewActorInfo;
	NewActorInfo.ActorId = NewPlayerActorUID;
	NewActorInfo.Hp = Actors[NewPlayerActorUID]->GetHp();
	NewActorInfo.Position = Actors[NewPlayerActorUID]->GetTransform().GetTranslation();
	NewActorInfo.Type = Actors[NewPlayerActorUID]->GetType();
	ToOldPlayers.NewActor = NewActorInfo;

	for(auto& PlayerPair : Players)
	{
		if(PlayerPair.second->ClientId_ != message->From)
			Network::Send(PlayerPair.second->ClientId_, ToOldPlayers);
	}
}
void GameState::ProcessActWithTargets(Message* message)
{
	// Player validity check
	if (Players.find(message->From) == Players.end())
		return;

	auto Data = std::reinterpret_pointer_cast<Serializables::ActWithTargets>(message->Data);

	// Actor id validity check
	UID ActorId = Players[message->From]->ActorId_;
	if (ActorId != Data->Self)
		return;

	// Actor validity check
	auto TargetActor = Actors.find(ActorId);
	if (TargetActor == Actors.end())
		return;

	// Bow를 들고있는 플레이어의 화면상에 타겟팅된 액터 ID들을 넘겨준다.
	std::reinterpret_pointer_cast<Character>(TargetActor->second)->SetBowScreenTargets(&Data->IdsOfTargets);

	LOG_INFO("Act With Targets Received From Client");
}
#endif

#ifdef TSSERVER
void GameState::ProcessLevelChangeReqDebug(Message* message)
{
	if (Players.find(message->From) == Players.end())
		return;

	auto Data = std::reinterpret_pointer_cast<Serializables::LevelChangeReqDebug>(message->Data);
	
	std::vector<PlayerStoredDataForLevelChange> TempData;
	for(auto& Player : Players)
	{
		auto PlayerActorIter = Actors.find(Player.second->ActorId_);
		if (PlayerActorIter == Actors.end())
			continue;

		auto PlayerActor = std::reinterpret_pointer_cast<Character>(PlayerActorIter->second);
		PlayerStoredDataForLevelChange PlayerData;
		PlayerData.Id = Player.first;
		PlayerData.Weapon = PlayerActor->GetCurrentWeaponType();
		PlayerData.Armor = PlayerActor->GetCurrentArmorType();
		PlayerData.Hp = PlayerActor->GetHp();
		PlayerData.Stamina = PlayerActor->GetCurrentStamina();
		PlayerData.TeleportGage = PlayerActor->GetCurrentTeleportGage();
		for(auto ItemPair : PlayerActor->GetInventoryComp()->Items)
		{
			PlayerData.Items.emplace_back(std::make_pair(ItemPair.first, std::make_pair(static_cast<int>(ItemPair.second->ItemCategory), static_cast<int>(ItemPair.second->GetItemType()))));
		}

		TempData.emplace_back(PlayerData);
	}
	

	
	
	// State초기화
	Init(Data->Type);

	Serializables::LevelChanged LevelChangedData;
	LevelChangedData.Map = Data->Type;
	BroadcastPacket(LevelChangedData);

	Serializables::SessionStarted SessionStartedData = BuildSessionStarted();
	BroadcastPacket(SessionStartedData);


	// 정보를 이전 level과 동일하게 해준다.
	for(auto& PlayerTempData : TempData)
	{
		auto NewPlayerActorIter = Actors.find(Players[PlayerTempData.Id]->ActorId_);
		if (NewPlayerActorIter == Actors.end())
			continue;

		auto NewPlayerActor = std::reinterpret_pointer_cast<Character>(NewPlayerActorIter->second);
		NewPlayerActor->SwapWeapon(PlayerTempData.Weapon);
		NewPlayerActor->SwapArmor(PlayerTempData.Armor);
		NewPlayerActor->SetHp(PlayerTempData.Hp);
		NewPlayerActor->SetCurrentStamina(PlayerTempData.Stamina);
		NewPlayerActor->SetCurrentTeleportGage(PlayerTempData.TeleportGage);
		for(auto& ItemData : PlayerTempData.Items)
		{
			std::shared_ptr<ItemComponent> ItemComp;
			switch(static_cast<ItemType>(ItemData.second.first))
			{
			case ItemType::Weapon:
				ItemComp = std::make_shared<WeaponItemComponent>();
				std::reinterpret_pointer_cast<WeaponItemComponent>(ItemComp)->WeaponType = static_cast<WeaponItemType>(ItemData.second.second);
				break;

			case ItemType::Armor:
				ItemComp = std::make_shared<ArmorItemComponent>();
				std::reinterpret_pointer_cast<ArmorItemComponent>(ItemComp)->ArmorType = static_cast<ArmorItemType>(ItemData.second.second);
				break;

			default:
				continue;
			}
			ItemComp->Index = ItemData.first;
			ItemComp->ItemCategory = static_cast<ItemType>(ItemData.second.first);
			
			ItemComp->SetOwnerActor(NewPlayerActor);
			NewPlayerActor->GetInventoryComp()->Items.insert(std::make_pair(ItemData.first, ItemComp));
		}
	}
	
	Serializables::ToNewPlayerOldPlayerWeapons ItemData;
	// 기존 Player들의 무기 정보를 새로 들어온 player에게 보낸다.
	for (auto& OldPlayer : Players)
	{
		auto OldPlayerActorIter = Actors.find(OldPlayer.second->ActorId_);
		if (OldPlayerActorIter == Actors.end())
			continue;

		// 기존 player의 weapon을 swap시키는 형식으로 기존 player의 무기를 동기화시킨다.
		auto OldPlayerActor = std::reinterpret_pointer_cast<Character>(OldPlayerActorIter->second);
		Serializables::ActWithValue PlayerWeaponData;
		PlayerWeaponData.Self = OldPlayer.second->ActorId_;
		PlayerWeaponData.Type = ActType::WeaponSwap;
		PlayerWeaponData.Value = static_cast<int>(OldPlayerActor->GetCurrentWeaponType());

		Serializables::ActWithValue PlayerArmorData;
		PlayerArmorData.Self = OldPlayer.second->ActorId_;
		PlayerArmorData.Type = ActType::ArmorSwap;
		PlayerArmorData.Value = static_cast<int>(OldPlayerActor->GetCurrentArmorType());

		ItemData.SwapWeapons.Array.emplace_back(PlayerWeaponData);
		ItemData.SwapWeapons.Array.emplace_back(PlayerArmorData);
	}
	BroadcastPacket(ItemData);

	for(auto& Player : Players)
	{
		auto PlayerActorIter = Actors.find(Player.second->ActorId_);
		if (PlayerActorIter == Actors.end())
			continue;

		auto PlayerActor = std::reinterpret_pointer_cast<Character>(Actors[Player.second->ActorId_]);
		
		Serializables::StatChange StatData;
		StatData.Id = Player.second->ActorId_;
		StatData.Hp = PlayerActor->GetHp();
		StatData.Stamina = PlayerActor->GetCurrentStamina();
		StatData.TeleportGage = PlayerActor->GetCurrentTeleportGage();

		Network::Send(Player.first, StatData);

		// Item
		for(auto& Item : PlayerActor->GetInventoryComp()->Items)
		{
			Serializables::ItemAcquire ItemData;
			ItemData.Index = Item.first;
			ItemData.ItemCategory = static_cast<int>(Item.second->ItemCategory);
			ItemData.ItemType = Item.second->GetItemType();

			// Client측에서 Level travel scheduled가 true면 저장해놨다가 처리한다.
			Network::Send(Player.first, ItemData);
		}
	}
}

void GameState::ProcessDirectiveAct(Message* message)
{
	if (Players.find(message->From) == Players.end())
		return;

	auto Data = std::reinterpret_pointer_cast<Serializables::DirectiveAct>(message->Data);

	switch(Data->Type)
	{
		// Value가 열려 하는 문의 UID
	case InteractableActionType::NextLevel:
	{
		// Next레벨에 대한 요청을 받았음을 bool로 저장한다.
		// 모든 Player들에 대해서 수락 여부를 poll한다.
		// 모두 true라면 레벨 변경하고
		// 아니라면 가만히 있는다.
		// false를 수신한다면 제일 처음 set한 bool 변수를 다시 false로 바꾸어 검사하지 않도록 한다.
		break;
	}

	default:
		break;
	}
}

// Interactable act를 처리한다.
void GameState::ProcessInteractableAct(Message* message)
{
	if (Players.find(message->From) == Players.end())
		return;

	auto Data = std::reinterpret_pointer_cast<Serializables::InteractableAct>(message->Data);

	// 같은 tag를 가진 actor들에 대해 전달받은 type의 행동을 수행한다.
	auto ObjectsIter = Interactables.lower_bound(Data->Tag);
	auto ObjectsEndIter = Interactables.upper_bound(Data->Tag);
	for (; ObjectsIter != ObjectsEndIter; ObjectsIter++)
	{
		switch (Data->Type)
		{
		case InteractableActionType::OpenDoorRotateMinus:
		{
			LOG_INFO("OpenDoorRotateMinus");
			auto ActorIter = Actors.find(ObjectsIter->second);
			if (ActorIter != Actors.end())
			{
				auto Rotator = std::make_shared<Activator>();
				UID TargetId = ObjectsIter->second;

				//// 일정 시간동안 주어진 값만큼 서서히 회전시키라고 명령한다.
				//Serializables::RotateForTime RotateData;
				//// 밀리세컨드 단위
				//RotateData.Id = TargetId;
				//RotateData.DurationMilliseconds = 1000.f;
				//RotateData.RotateAmount = Vector3f(0.f, 0.f, -30.f);
				//BroadcastPacket(RotateData);
				BroadcastPacket(*Data);

				// 클라이언트에게 1초동안 문을 회전시키라 명령하고 서버에서는 1초 후에 회전을 시켜준다.
				Rotator->Register([TargetId, this]()
								  {
									  auto ActorIter = Actors.find(TargetId);
									  if (ActorIter != Actors.end())
									  {
										  ActorIter->second->RotateWithDegrees(0.f, 0.f, -30.f);
									  }
								  });
				Rotator->SetTimer(1s);
				AddToTimer(Rotator);
			}
			else
			{
				LOG_WARNING("Can't find door actor %d", ObjectsIter->second);
			}
		}
		break;

		case InteractableActionType::OpenDoorRotatePlus:
		{
			LOG_INFO("OpenDoorRotatePlus");
			auto ActorIter = Actors.find(ObjectsIter->second);
			if (ActorIter != Actors.end())
			{
				auto Rotator = std::make_shared<Activator>();
				UID TargetId = ObjectsIter->second;

				//// 일정 시간동안 주어진 값만큼 서서히 회전시키라고 명령한다.
				//Serializables::RotateForTime RotateData;
				//// 밀리세컨드 단위
				//RotateData.Id = TargetId;
				//RotateData.DurationMilliseconds = 1000.f;
				//RotateData.RotateAmount = Vector3f(0.f, 0.f, 30.f);
				//BroadcastPacket(RotateData);
				BroadcastPacket(*Data);

				// 클라이언트에게 1초동안 문을 회전시키라 명령하고 서버에서는 1초 후에 회전을 시켜준다.
				Rotator->Register([TargetId, this]()
								  {
									  auto ActorIter = Actors.find(TargetId);
									  if (ActorIter != Actors.end())
									  {
										  ActorIter->second->RotateWithDegrees(0.f, 0.f, 30.f);
									  }
								  });
				Rotator->SetTimer(1s);
				AddToTimer(Rotator);
			}
			else
			{
				LOG_WARNING("Can't find door actor %d", ObjectsIter->second);
			}
		}

		default:
			return;
		}
	}
}

void GameState::ProcessItemAcquireFromChestReq(Message* message)
{
	if (Players.find(message->From) == Players.end())
		return;

	auto Data = std::reinterpret_pointer_cast<Serializables::ItemAcquireFromChestReq>(message->Data);

	auto TargetActor = GetActor(Players[message->From]->ActorId_);
	if (TargetActor == nullptr)
		return;

	if(Interactables.find(Data->ChestTag) != Interactables.end())
	{
		// Chest가 있는지 확인
		auto Chest = GetActor(Interactables.lower_bound(Data->ChestTag)->second);
		if (Chest == nullptr)
		{
			LOG_WARNING("Chest doesn't exist. Chest tag: %s", Data->ChestTag);
			return;
		}

		// Chest에게 inventory가 있는지 확인
		auto ChestInventory = Chest->GetInventoryComp();
		if (ChestInventory == nullptr)
		{
			LOG_WARNING("Chest doesn't have a inventory component. Chest tag: %s", Data->ChestTag);
			return;
		}

		// Chest에 찾는 item이 있는지 확인
		auto ItemFromChest = ChestInventory->UnacquireItem(Data->Index);
		if (ItemFromChest == nullptr)
		{
			LOG_WARNING("Chest doesn't have valid item with item index: %d", Data->Index);
			return;
		}

		// Item을 주려는 actor가 inventory를 가지고 있는지 확인
		auto Inventory = TargetActor->GetInventoryComp();
		if (Inventory == nullptr)
		{
			LOG_WARNING("Actor without inventory is trying to acquire item. ActorId: %d", message->From);
			return;
		}

		Serializables::ItemRemoveFromChest ItemRemoveData;
		ItemRemoveData.Index = ItemFromChest->Index;
		ItemRemoveData.ChestTag = Data->ChestTag;
		
		LOG_INFO("Trying to acquire an item. Index: %d", ItemFromChest->Index);
		Inventory->AcquireItem(ItemFromChest);

		Serializables::ItemAcquire Data;
		Data.ItemCategory = static_cast<int>(ItemFromChest->ItemCategory);
		Data.Index = ItemFromChest->Index;
		switch (ItemFromChest->ItemCategory)
		{
		case ItemType::Weapon:
			Data.ItemType = static_cast<int>(std::reinterpret_pointer_cast<WeaponItemComponent>(ItemFromChest)->WeaponType);
			break;

		case ItemType::Armor:
			Data.ItemType = static_cast<int>(std::reinterpret_pointer_cast<ArmorItemComponent>(ItemFromChest)->ArmorType);
			break;

		default:
			LOG_WARNING("Invalid item type: %d", ItemFromChest->ItemCategory);
			return;
		}

		Network::Send(message->From, Data);
		BroadcastPacket(ItemRemoveData);
		
	}
	else
		LOG_WARNING("No actor found with tag: %s", Data->ChestTag);
}

void GameState::ProcessItemEquip(Message* message)
{
	if (Players.find(message->From) == Players.end())
		return;

	auto Data = std::reinterpret_pointer_cast<Serializables::ItemEquip>(message->Data);
}

void GameState::ProcessItemUnequip(Message* message)
{
	if (Players.find(message->From) == Players.end())
		return;

	auto Data = std::reinterpret_pointer_cast<Serializables::ItemUnequip>(message->Data);
}

void GameState::ProcessItemAcquire(Message* message)
{
	if (Players.find(message->From) == Players.end())
		return;

	auto Data = std::reinterpret_pointer_cast<Serializables::ItemAcquire>(message->Data);
}

void GameState::ProcessItemUnacquire(Message* message)
{
	if (Players.find(message->From) == Players.end())
		return;

	auto Data = std::reinterpret_pointer_cast<Serializables::ItemUnacquire>(message->Data);
}

void GameState::ProcessTriggerNotify(Message* message)
{
	if (Players.find(message->From) == Players.end())
		return;

	LOG_INFO("Trigger Notify received.");

	auto Data = std::reinterpret_pointer_cast<Serializables::TriggerNotify>(message->Data);
	switch(Data->Act)
	{
	case TriggerActionType::Stage01_PlayBossCutscene:
	case TriggerActionType::Stage02_PlayBossCutscene:
	case TriggerActionType::Stage03_PlayBossCutscene:
	{
		if(auto PlayerActor = Players[message->From])
		{
			Serializables::TriggerNotify Notify;
			Notify.Tag = Data->Tag;
			Notify.Act = Data->Act;
			BroadcastPacket(Notify);
			if (Data->Act == TriggerActionType::Stage01_PlayBossCutscene)
			{
				LOG_DEBUG("IN");
				for (auto& iter : Actors)
				{
					if (auto Boss_stage01 = dynamic_cast<SwordMonster*>(iter.second.get()))
					{
						Boss_stage01->SetCanActWithDelay(15.f);
						LOG_DEBUG("Activate Boss1");
						break;
					}
				}
			}
		}

		break;
	}

	default:
		break;
	}
}

void GameState::ProcessCheat(Message* message)
{
	if (Players.find(message->From) == Players.end())
		return;

	auto Data = std::reinterpret_pointer_cast<Serializables::Cheat>(message->Data);

	if (Actors.find(Data->Id) != Actors.end())
	{
		auto PlayerActor = std::reinterpret_pointer_cast<Character>(Actors[Data->Id]);
		if (PlayerActor == nullptr)
			return;
		
		switch (Data->Type)
		{
		case CheatType::MoveToStart:
		{
			auto Iter = Interactables.lower_bound("Cheat_Point_Start");
			if (Iter != Interactables.end())
			{
				if (Actors.find(Iter->second) != Actors.end())
				{
					PlayerActor->GetTransform().SetTranslation(Actors[Iter->second]->GetTransform().GetWorldTranslation());
					PlayerActor->SetLastTranslation();
					LOG_INFO("Cheat mode: Move to start.");
				}
			}
			break;
		}
			

		case CheatType::MoveToMiddle:
		{
			auto Iter = Interactables.lower_bound("Cheat_Point_Middle");
			if (Iter != Interactables.end())
			{
				if (Actors.find(Iter->second) != Actors.end())
				{
					PlayerActor->GetTransform().SetTranslation(Actors[Iter->second]->GetTransform().GetWorldTranslation());
					PlayerActor->SetLastTranslation();
					LOG_INFO("Cheat mode: Move to start.");
				}
			}
			break;
		}

		case CheatType::MoveToBoss:
		{
			auto Iter = Interactables.lower_bound("Cheat_Point_Boss");
			if (Iter != Interactables.end())
			{
				if (Actors.find(Iter->second) != Actors.end())
				{
					PlayerActor->GetTransform().SetTranslation(Actors[Iter->second]->GetTransform().GetWorldTranslation());
					PlayerActor->SetLastTranslation();
					LOG_INFO("Cheat mode: Move to start.");
				}
			}
			break;
		}

		case CheatType::MoveToEnd:
		{
			auto Iter = Interactables.lower_bound("Cheat_Point_End");
			if (Iter != Interactables.end())
			{
				if (Actors.find(Iter->second) != Actors.end())
				{
					PlayerActor->GetTransform().SetTranslation(Actors[Iter->second]->GetTransform().GetWorldTranslation());
					PlayerActor->SetLastTranslation();
					LOG_INFO("Cheat mode: Move to start.");
				}
			}
			break;
		}

		case CheatType::GodMode:
			PlayerActor->GodMode();
			if(PlayerActor->IsGod())
			{
				Serializables::StatChange Stat;
				Stat.Id = Data->Id;
				Stat.Hp = PlayerActor->GetMaxHp();
				Stat.Stamina = PlayerActor->GetMaxStamina();
				Stat.TeleportGage = PlayerActor->GetMaxTeleportGage();
				Network::Send(Data->Id, Stat);
			}
			break;

		default:
			break;
		}
	}
}
#endif

#ifdef TSSERVER
TheShift::Serializables::SessionStarted GameState::BuildSessionStarted() const
{
	Serializables::SessionStarted Data;
	Data.Map = Map;
	Data.ServerUpdateTimestep = UpdateTimestep;

	// Player정보 입력
	for(auto& PlayerPair : Players)
	{
		Serializables::AvatarInfo PlayerInfo;
		PlayerInfo.Player = *PlayerPair.second;
		Data.Players.Array.emplace_back(PlayerInfo);
	}

	// Actor정보 입력
	for(auto& ActorPair : Actors)
	{
		// 맵에 배치된 Prop들은 보내지 않는다. JSON에 어차피 다 저장되어 있음.
		// 생성되어있는 몬스터나 플레이어 액터만 보내주면 됨.
		auto Type = ActorPair.second->GetType();
		if(Type == ActorType::Prop
		   || Type == ActorType::Trigger)
			continue;

		SerializeActorInfo(Data.Actors, ActorPair.second);
		//Serializables::ActorInfo ObjectInfo;
		//ObjectInfo.Type = ActorPair.second->GetType();
		//ObjectInfo.Hp = ActorPair.second->GetHp();
		//ObjectInfo.ActorId = ActorPair.second->GetId();
		//auto Position = ActorPair.second->GetTransform().GetTranslation();
		//ObjectInfo.Position = Vector3f(Position.x(), Position.y(), Position.z());
		//Data.Actors.Array.emplace_back(ObjectInfo);
	}

	return Data;
}

void GameState::SerializeActorInfo(Serializables::SerializableVector<Serializables::ActorInfo>& Vector,
                                   std::shared_ptr<Actor> TargetActor) const
{
	Serializables::ActorInfo Info;
	Info.Type = TargetActor->GetType();
	Info.Hp = TargetActor->GetHp();
	Info.ActorId = TargetActor->GetId();
	auto Position = TargetActor->GetTransform().GetTranslation();
	Info.Position = Vector3f(Position.x(), Position.y(), Position.z());
	Vector.Array.emplace_back(Info);
}

#endif

/// <summary>
/// Player가 Session에 최초 접속 시 초기화를 해준다. 
/// </summary>
#ifdef TSSERVER
UID GameState::InitPlayer(ActorType Type, ClientId Id)
{
	//TODO: Player 초기화 작업
	return CreateCharacter(Type, Id);
}
#endif

TheShift::UID GameState::SpawnActor(ActorType Type, const Vector3f& Position)
{
	UID FreeUID = INVALID_UID;
	if (Type == ActorType::Prop)
		FreeUID = GetAvailableUID(true);
	else
		FreeUID = GetAvailableUID(false);
	
	if(FreeUID == INVALID_UID)
		return INVALID_UID;
	return CreateActor(Type, Position, FreeUID)->GetId();
}

TheShift::UID GameState::SpawnActor(ActorType Type, const Eigen::Vector3f& Position)
{
	UID FreeUID = INVALID_UID;
	if (Type == ActorType::Prop)
		FreeUID = GetAvailableUID(true);
	else
		FreeUID = GetAvailableUID(false);

	if(FreeUID == INVALID_UID)
		return INVALID_UID;
	return CreateActor(Type, Position, FreeUID)->GetId();
}

std::shared_ptr<Actor> GameState::CreateActor(ActorType Type, const Vector3f& Position, UID Id)
{
	std::shared_ptr<Actor> NewActor;

	switch(Type)
	{
	case ActorType::Player:
		NewActor = std::make_shared<Character>(Id);
		NewActor->GetInventoryComp()->SetOwnerActor(NewActor);
		break;

	case ActorType::Prop:
	case ActorType::Interactable:
	case ActorType::Destructible:
		NewActor = std::make_shared<Actor>(Id);
		break;

	case ActorType::Trigger:
		NewActor = std::make_shared<Trigger>(Id);
		break;

	case ActorType::KnightMonster:
		NewActor = std::make_shared<KnightMonster>(Id);
		break;

	case ActorType::Spider:
		NewActor = std::make_shared<Spider>(Id);
		break;

	case ActorType::SwordMonster:
		NewActor = std::make_shared<SwordMonster>(Id);
		break;
	case ActorType::BlackWareWolf:
	case ActorType::BrownWareWolf:
	case ActorType::WhiteWareWolf:
		NewActor = std::make_shared<WereWolf>(Id);
		if (auto Wolf = dynamic_cast<WereWolf*>(NewActor.get()))
			Wolf->SetWereWolfType(Type);

		break;

	case ActorType::FatMonster:
		NewActor = std::make_shared<FatMonster>(Id);
		break;

		//case ActorType::OgerMonster:
		//	NewActor = std::make_shared<OgerMonster>(Id);
		//	break;

			// EX) Add case state for every actor type like so
			//case ActorType::"ActorType":
			//	NewActor = std::make_shared<"ActorType">(FreeUID);
			//	break;

	default:
		return nullptr;
	}

	// return INVALID_UID if NewActor is nullptr
	if(NewActor == nullptr)
		return nullptr;


	// Init BoundingVolumes
	// Tag가 없는 Actor들은 Init에서 초기화
	if(Type != ActorType::Prop
	   && Type != ActorType::Interactable
	   && Type != ActorType::Destructible 
	   && Type != ActorType::Trigger)
	{
		// Is this actor Movable?
		NewActor->SetIsMovable(ActorStat[Type].IsMovable);
		CollisionResolver_->CreateBoundingVolumes(NewActor, ActorStat[Type].BoundInfo);
	}


	Actors.insert(std::make_pair(Id, NewActor));


	// Init Actor
	Actors[Id]->RegisterGameState(this);
	//#ifdef TSSERVER
	//	// Register func
	//	Actors[Id]->RegisterAddActFunc([this](Serializables::Act& act) {this->AddAct(act); });
	//#endif
		// Set Location
	Actors[Id]->SetTranslation(Position.X, Position.Y, Position.Z);
	Actors[Id]->SetRotation(Eigen::Quaternionf::Identity());
	Actors[Id]->SetLastTranslation();

	return NewActor;
}

std::shared_ptr<Actor> GameState::CreateActor(ActorType Type, const Eigen::Vector3f& Position, UID Id)
{
	return CreateActor(Type, TheShift::Vector3f(Position.x(), Position.y(), Position.z()), Id);
}

/// <summary>
/// 시작 포인트들 중 한 곳에 Character를 생성시킨다.
/// </summary>
//TODO: Character의 Type에 따라 초기화 해준다. (무기 종류? 클래스 종류?)
#ifdef TSSERVER
UID GameState::CreateCharacter(ActorType Type, ClientId id)
{
	if(Players.find(id) == Players.end())
		return INVALID_UID;

	bool NoAvailablePoints = false;

	// Player시작위치를 불러온다.
	// 빈 자리를 찾고 불가능하다면 마지막 위치에 추가한다.
	int StartPointNum = PlacementInfo.count("Start Point");
	auto It = PlacementInfo.lower_bound("Start Point");

	// 가능한 Start point가 있으면
	if(StartPointIndices.count(INVALID_CLIENTID) != 0)
	{
		// Key가 INVALID_CLIENTID이면 비어있는 Start point
		auto AvailableStartPointNumPair = StartPointIndices.lower_bound(INVALID_CLIENTID);
		for(int i = 0; i < AvailableStartPointNumPair->second; ++i)
		{
			It++;
		}

		// 해당 Start point에 해당 Player가 spawn됐으므로 Key를 해당 Player의 Id로 바꿔준다.
		StartPointIndices.insert(std::make_pair(id, AvailableStartPointNumPair->second));
		StartPointIndices.erase(AvailableStartPointNumPair);
	}
	// 가능한 Start point가 없으면 그냥 뺑뺑이 돌린다.
	else
	{
		It = PlacementInfo.lower_bound("Start Point");
		// 1, 2, 3번 자리가 있으면 123123 이런식으로 캐릭터를 배치시킨다.
		int Max = (Players.size() % StartPointNum) - 1;
		for(int i = 0; i < Max; ++i)
		{
			It++;
		}
	}

	UID CharacterId = SpawnActor(ActorType::Player, It->second.ActorPosition);
	if(CharacterId == INVALID_UID)
	{
		//spdlog::error("INVALID_UID while creating character. PlayerId: {0}", id);
		LOG_ERROR("INVALID_UID while creating character. PlayerId: %d", id);
	}
	else
	{
		Players[id]->ActorId_ = CharacterId;
	}

	return CharacterId;
}
#endif

/// <summary>
/// Import한 Actor의 정보에 따라 Bounding volume을 초기화 시켜준다.
/// </summary>
bool GameState::SetBoundingVolume(std::shared_ptr<Actor> TargetActor)
{
	// Import한 정보를 읽는다.
	ActorType Type = TargetActor->GetType();
	if(ActorStat.find(Type) == ActorStat.end())
	{
		//spdlog::warn("Actor Type: {0}, Can't find Actor Stat from imported data", Type);
		LOG_WARNING("Actor Type: %d, Can't find Actor Stat from imported data", static_cast<int>(Type));
		return false;
	}

	ActorInfo Info = ActorStat[Type];

	// 해당 정보대로 초기화 한다.
	TargetActor->Init(Info);
	return true;
}

void GameState::DestroyActor(UID actorId)
{
	if(Actors.find(actorId) != Actors.end())
	{
		// Actor가 가지고 있던 Volume들 일괄 삭제
		for(auto VolumePointer : Actors[actorId]->GetBoundingVolumes())
		{
			CollisionResolver_->RemoveBoundingVolume(VolumePointer->GetId());
		}
		Actors.erase(actorId);
	}
}

void GameState::DestroyActor(std::shared_ptr<Actor> ActorToDestroy)
{
	//DestroyActor(ActorToDestroy->GetId());
	PendDestroyActor(ActorToDestroy->GetId());
}

void GameState::PendDestroyActor(UID ActorId)
{
	Garbage.emplace_back(ActorId);
}

UID GameState::GetAvailableUID(bool Reverse/* = false*/)
{
	UID FreeUID = INVALID_UID;
	if(Reverse)
	{
		FreeUID = MAX_UID;
	}
	bool Found = false;
	while(!Found)
	{
		if (Reverse)
		{
			--FreeUID;
			if (FreeUID == INVALID_UID)
				return INVALID_UID;
		}
		else
		{
			++FreeUID;
			if (FreeUID >= MAX_UID)
				return INVALID_UID;
		}

		if(Actors.find(FreeUID) == Actors.end())
		{	
			break;
		}

	}

	return FreeUID;
}
std::shared_ptr<Actor> GameState::GetNearestActor(std::list<std::shared_ptr<Actor>>* Lst, Actor* SrcActor, std::list<std::shared_ptr<Actor>>* ActorsToExecpt)
{
	float Distance = 100000.f;

	Eigen::Vector3f SrcTranslation = SrcActor->GetTransform().GetTranslation();
	std::shared_ptr<Actor> NearestActor = nullptr;
	for (auto Actor : *Lst)
	{
		float CurDistance = (Actor->GetTransform().GetTranslation() - SrcTranslation).norm();
		if (CurDistance < Distance)
		{
			//만약 거르고 싶은 액터들의 리스트 안에 이 액터가 없다면 이 액터는 가까운 액터로 설정되어야 한다.
			if (ActorsToExecpt)
			{
				if (find_if(ActorsToExecpt->begin(), ActorsToExecpt->end(), [Actor](const std::shared_ptr<TheShift::Actor>& Src)
					{
						return Actor == Src;
					})
					== ActorsToExecpt->end())
				{
					NearestActor = Actor;
					Distance = CurDistance;
					LOG_DEBUG("DIstance : %f", Distance);
				}
			}
			else
			{
				NearestActor = Actor;
				Distance = CurDistance;
			}
		}
	}

	return NearestActor;
}
}

