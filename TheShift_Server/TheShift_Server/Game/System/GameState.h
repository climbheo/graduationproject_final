﻿#pragma once


#include "Protocol/Protocol.h"
#include "Protocol/Types.h"

#include <chrono>
#include <map>
#include <queue>

#ifdef TSSERVER
#include "Player.h"
#include "Game/System/Network/SendRequirements.h"
#include "Game/System/TimerQueue.h"
#include "Protocol/MessageBuilder.h"
#endif

#ifdef TSCLIENT
#include "Containers/Map.h"
#include "Misc/InputInfo.h"		// Input보간 관련
class UTSNetworkedComponent;
#endif

namespace TheShift
{
class BoundingVolume;
class Heightmap;
class Actor;
class CollisionResolver;
class EventComponent;

/// <summary>
/// Game simulation에 필요한 모든 정보를 가진다.
/// Update함수로 게임의 모든 정보를 업데이트한다.
/// </summary>
/// TODO: 생성시 JSON파일로부터 Actor들의 생성 좌표를 읽어와야 한다. Actor의 Type도.
class GameState
{
public:
	GameState();

	bool Update(std::chrono::nanoseconds deltaTime);

#ifdef TSSERVER
	void ApplyMessage(Message* message);
	std::vector<std::shared_ptr<Serializables::SerializableData>>* GetDataToBeSent();
	void ClearDataToBeSent();
	void AddNewPlayer(std::shared_ptr<Avatar> player);
	std::map<ClientId, std::shared_ptr<Avatar>>* GetPlayers();
	MapType GetMapType() const;
	void StartGame();
	void AddToTimer(std::shared_ptr<TimerCallee> Callee);
	void BroadcastPacket(Serializables::SerializableData& Data);
	void SpawnActorsWithTagAndBroadcast(const char* Tag);
	void ProcessDieEvent(std::shared_ptr<Actor> DyingActor);
	void SetUpdateTimestep(float NewTimestep);
#endif

	std::map<UID, std::shared_ptr<Actor>>* GetActors();
	std::list<std::shared_ptr<Actor>> GetActorsByType(ActorType Type);
	std::list<std::shared_ptr<Actor>> GetActorsByTypes(ActorType* Types, int TypeNum);
	std::shared_ptr<Actor> GetNearestActorByType(ActorType Type, Actor* SrcActor, std::list<std::shared_ptr<Actor>>* ActorsToExecpt = nullptr);
	std::shared_ptr<Actor> GetNearestActorByTypes(ActorType* Types,int TypeNum, Actor* SrcActor, std::list<std::shared_ptr<Actor>>* ActorsToExecpt = nullptr);
	std::shared_ptr<Actor> GetActor(UID Id);
	const std::vector<std::shared_ptr<Heightmap>>& GetHeightmaps() const;

	void Init(MapType map);
	void LoadMap(MapType map);

	const std::map<UID, std::shared_ptr<Actor>>* GetActors() const;
	CollisionResolver* GetCollisionResolver();

	UID SpawnActor(ActorType Type, const Vector3f& Position);
	UID SpawnActor(ActorType Type, const Eigen::Vector3f& Position);
	std::shared_ptr<Actor> CreateActor(ActorType Type, const Vector3f& Position, UID Id);
	std::shared_ptr<Actor> CreateActor(ActorType Type, const Eigen::Vector3f& Position, UID Id);

	void DestroyActor(UID ActorId);
	void DestroyActor(std::shared_ptr<Actor> ActorToDestroy);
	void PendDestroyActor(UID ActorId);

#ifdef TSCLIENT
	// GameState에서 UE Actor를 읽을 수 있도록 포인터로 가지고 있음.
	void RegisterNetworkComps(TMap<UID, UTSNetworkedComponent*>* Container);
	AActor* GetUEActor(UID Id);
	UID GetMyId() const;
	std::shared_ptr<TheShift::Actor> GetCorrespondingActor(UID Id);
	void SetMyActorId(UID Id);
	void ApplyUnprocessedInput(const Serializables::ActorPosition& PositionInfo, const InputInfo& UnprocessedInput, UID MyActorId);
#endif


private:
	std::chrono::nanoseconds TimePassed;
	MapType Map;

	//std::chrono::nanoseconds CollisionDebugDuration;


#ifdef TSSERVER
	bool AllPlayerLoaded = false;

	// Update interval
	float UpdateTimestep;

	// Game simulation에 필요한 모든 정보
	std::map<ClientId, std::shared_ptr<Avatar>> Players;
	std::queue<Serializables::Act> Acts;
	TimerQueue JobTimer;
	std::vector<std::shared_ptr<Serializables::SerializableData>> DataToBeSent;
#endif

#ifdef TSCLIENT
	UID MyActorId = INVALID_UID;
	bool MapLoaded = false;
	TMap<UID, UTSNetworkedComponent*>* NetworkComps;
#endif

	std::map<UID, std::shared_ptr<Actor>> Actors;
	std::multimap<std::string, UID> Interactables;

	std::multimap<std::string, ActorPlacementInfo> PlacementInfo;

	std::map<ActorType, ActorInfo> ActorStat;

	std::vector<std::shared_ptr<Heightmap>> Heightmaps;

	CollisionResolver* CollisionResolver_;

	std::vector<UID> Garbage;

	// 몇번째 Start point가 비어있는지 알기 위해
	// Key가 INVALID_CLIENTID이면 비어있는 것
	std::map<ClientId, int> StartPointIndices;

#ifdef TSSERVER
	void ProcessSessionExit(Message* message);
	void ProcessLoadCompleted(Message* message);
	void ProcessInput(Message* message);
	void ProcessSwapWeapon(Message* message);
	void ProcessSwapArmor(Message* message);
	void ProcessSessionJoinReq(Message* message);
	void ProcessActWithTargets(Message* message);
	void ProcessLevelChangeReqDebug(Message* message);
	void ProcessDirectiveAct(Message* message);
	void ProcessInteractableAct(Message* message);
	void ProcessItemAcquireFromChestReq(Message* message);
	void ProcessItemEquip(Message* message);
	void ProcessItemUnequip(Message* message);
	void ProcessItemAcquire(Message* message);
	void ProcessItemUnacquire(Message* message);
	void ProcessTriggerNotify(Message* message);
	void ProcessCheat(Message* message);

	Serializables::SessionStarted BuildSessionStarted() const;
	void SerializeActorInfo(Serializables::SerializableVector<Serializables::ActorInfo>& Vector, std::shared_ptr<Actor> TargetActor) const;
	UID InitPlayer(ActorType Type, ClientId Id);
#endif

	UID CreateCharacter(ActorType Type, ClientId id);
	bool SetBoundingVolume(std::shared_ptr<Actor> TargetActor);
	UID GetAvailableUID(bool Reverse = false);

	std::shared_ptr<Actor> GetNearestActor(std::list<std::shared_ptr<Actor>>* Lst, Actor* SrcActor, std::list<std::shared_ptr<Actor>>* ActorsToExecpt = nullptr);
};

struct PlayerStoredDataForLevelChange
{
	ClientId Id;
	WeaponType Weapon;
	ArmorItemType Armor;
	float Hp;
	int Stamina;
	int TeleportGage;
	// Index, Category, Type
	std::vector<std::pair<int, std::pair<int, int>>> Items;
};
}


