﻿#include "AnimNotifyData.h"
#include "Protocol/Protocol.h"
#include "Protocol/Json_Formatter/JsonReader.h"
#include "Protocol/Log.h"

AnimNotifyData* AnimNotifyData::Instance_ = nullptr;

AnimNotifyData::AnimNotifyData()
{
	JsonFormatter::JsonReader Reader;

#ifdef TSSERVER
	std::string Path = AnimNotifyDataDir;
#endif

#ifdef TSCLIENT
	std::string Path = TCHAR_TO_ANSI(*(FPaths::ProjectContentDir() + MapDataDir));
	LOG_INFO("Path: %s", Path.c_str());
#endif

	//Data.insert(std::make_pair(MapType::NewMap1, MapInfo()));
	//assert(Reader.ImportMapInfo((Path + "NewMap1.json").c_str(), Data[MapType::NewMap1]));
	Data.emplace("Player", std::set<AnimNotifyInfo>());
	Reader.ImportAnimNotifyInfo((Path + "PlayerNotifies.json").c_str(), Data["Player"]);

	Data.emplace("KnightMonster", std::set<AnimNotifyInfo>());
	Reader.ImportAnimNotifyInfo((Path + "KnightMonsterNotifies.json").c_str(), Data["KnightMonster"]);

	Data.emplace("FatMonster", std::set<AnimNotifyInfo>());
	Reader.ImportAnimNotifyInfo((Path + "FatMonsterNotifies.json").c_str(), Data["FatMonster"]);

	Data.emplace("Spider", std::set<AnimNotifyInfo>());
	Reader.ImportAnimNotifyInfo((Path + "SpiderNotifies.json").c_str(), Data["Spider"]);

	Data.emplace("SwordMonster", std::set<AnimNotifyInfo>());
	Reader.ImportAnimNotifyInfo((Path + "SwordMonsterNotifies.json").c_str(), Data["SwordMonster"]);

	Data.emplace("WereWolf", std::set<AnimNotifyInfo>());
	Reader.ImportAnimNotifyInfo((Path + "WereWolfNotifies.json").c_str(), Data["WereWolf"]);
	//... add more

	//spdlog::info("...Heightmaps loaded completely");
	LOG_INFO("... AnimNotifies loaded completely.");
}

const std::set<AnimNotifyInfo>* AnimNotifyData::GetAnimNotifyInfo(std::string ActorName) const
{
	auto iter = Data.find(ActorName);
	if (iter == Data.end())
		return nullptr;
	else
		return &iter->second;
}
