﻿#pragma once

#include "Protocol/atomic_memory_pool.h"
#include "Protocol/MessageBuilder.h"
#include "Player.h"


namespace MemoryAllocator
{

extern MemoryPool<SocketInfo> SocketInfoAllocator;
extern MemoryPool<TheShift::Message> MessageAllocator;

}
