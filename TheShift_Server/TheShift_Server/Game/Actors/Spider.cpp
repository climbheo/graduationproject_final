﻿#include "Spider.h"
#include "Game/System/GameState.h"
#include "Game/Actors/Trigger.h"
#ifdef TSSERVER
#include "Game/System/TimerCallee.h"
#endif
using namespace std::chrono_literals;
using namespace std;
const std::set<AnimNotifyInfo>* Spider::SpiderAnimNotifies = nullptr;

namespace TheShift
{
TheShift::Spider::Spider(UID Id) : Monster(Id)
{
	Type = ActorType::Spider;
#ifdef TSSERVER
	IsHitAnimPlay = true;
	SetRecognizeRange(500.f);
	SetContactRange(150.f);

	//Get Pointer of Player AnimData
	if (!SpiderAnimNotifies)
		SpiderAnimNotifies = AnimNotifyData::GetInstance().GetAnimNotifyInfo("Spider");
	else
		pMonsterAnimNotify = SpiderAnimNotifies;
#endif
}
#ifdef TSSERVER

void TheShift::Spider::OnPlayerContact(std::shared_ptr<Actor> Player)
{
	// Cool-Down, CanAct check
	if (AttackCoolDown && CanAct && !GetHit())
	{
		AttackCoolDown = false;
		CanChase = false;
		std::shared_ptr<CoolDown> AttackCoolDownTimer = std::make_shared<CoolDown>();
		AttackCoolDownTimer->SetTimer(4s);
		// 기본 공격 Cool down 등록
		AttackCoolDownTimer->Register(&AttackCoolDown);
		// Timer에 등록
		CurrentGameState->AddToTimer(AttackCoolDownTimer);

		// 공격 함을 알림
		Serializables::ActWithTarget AttackMessage;
		AttackMessage.Self = Id;
		AttackMessage.TargetId = Player->GetId();

		if (PrevAttackNum == 1)
		{
			AttackMessage.Type = ActType::BaseAttack_1;
			PrevAttackNum = 2;
		}
		else
		{
			AttackMessage.Type = ActType::BaseAttack_2;
			PrevAttackNum = 1;
		}
		if (SpiderAnimNotifies)
		{
			CurrentActivators.clear();

			//위의 animName에 해당하는 AnimNotifies를 찾고
			auto iter = SpiderAnimNotifies->find(string("Spider_Attack0") + to_string(static_cast<int>(AttackMessage.Type)));

			//모든 Notify를 실행한다.
			if (iter != SpiderAnimNotifies->end())
			{
				auto& Notifies = iter->AnimNotifies;
				CurrentActivators.reserve(Notifies.size());
				for (auto& Notify : Notifies)
				{
					if (Notify.first.find("HitCheck") != string::npos)
					{
						shared_ptr<CheckActivator> HitActivator = make_shared<CheckActivator>();
						float hitCheckTime = Notify.second;

						HitActivator->SetTimer(chrono::milliseconds(long long(hitCheckTime * 1000.f)));

						HitActivator->Register([this]()
							{
								LOG_DEBUG("Attack!");
								//Knight Monster 공격
								if (CurrentCombo == 1)
									CreateAttackOBB(-1.f, ActType::BaseAttack_1, 80.f, Eigen::Vector3f(75.f, 75.f, 75.f), Eigen::Vector3f(100.f, 0.f, 100.f), false);
								else
									CreateAttackOBB(-1.f, ActType::BaseAttack_1, 80.f, Eigen::Vector3f(100.f, 100.f, 100.f), Eigen::Vector3f(100.f, 0.f, 100.f), false);
							});

						CurrentGameState->AddToTimer(HitActivator);
						CurrentActivators.emplace_back(HitActivator);
					}
					else if (Notify.first.find("AttackEnd") != string::npos)
					{
						shared_ptr<CheckActivator> AttackEndActivator = make_shared<CheckActivator>();
						float hitCheckTime = Notify.second;

						AttackEndActivator->SetTimer(chrono::milliseconds(long long(hitCheckTime * 1000.f)));

						AttackEndActivator->Register([this]()
							{
								CanChase = true;
							});

						CurrentGameState->AddToTimer(AttackEndActivator);
					}
				}
			}
		}


		CurrentGameState->BroadcastPacket(AttackMessage);

	}
}

float TheShift::Spider::GetHitResetTime(bool IsBigHit)
{
	if (SpiderAnimNotifies)
	{
		auto iter = SpiderAnimNotifies->end();
		if (!IsBigHit)
			iter = SpiderAnimNotifies->find("Spider_Hit");
		else
			iter = SpiderAnimNotifies->find("Spider_Die02");
		if (iter != SpiderAnimNotifies->end())
		{
			auto& Notifies = iter->AnimNotifies;

			for (auto& Notify : Notifies)
			{
				if (Notify.first.find("HitReset") != std::string::npos)
				{ 
					if (!IsBigHit)
						return Notify.second;
					else
						return Notify.second + 0.2f;
				}
			}
		}


	}
	return 0.0f;
}

#endif
}