﻿#pragma once

#include "Game/Components/BoundingVolume.h"
#include "Protocol/Types.h"
#include "Protocol/Protocol.h"
#include "Protocol/Serializables.h"
#include "../Components/Movement.h"
#ifdef TSSERVER
#include "../System/TimerCallee.h"
#include "Game/System/AnimNotifyData.h"

#endif


/// <summary>
/// Actor의 스탯
/// 이동 속도, 공격력, 공격력 등을 저장한다.
/// 실시간으로 변화하는 값이 아닌 기본값을 저장한다.
/// </summary>
namespace TheShift
{
class GameState;
class InventoryComponent;
class ItemComponent;

class Actor
{
public:
	Actor(UID id);
	Actor(UID id, ActorType type);
	
	virtual ~Actor();
	virtual void OnInput(Serializables::Input inputValue);
#ifdef TSSERVER
	bool IsGod() const;
	virtual void OnHit(ActType AttackType);
	virtual void SwapWeapon(WeaponType Type);
	virtual float GetHitResetTime(bool IsBigHit = false);
	virtual void EquipItem(std::shared_ptr<ItemComponent> Item);
	virtual void UnequipItem(std::shared_ptr<ItemComponent> Item);
	virtual void GodMode();
	
#endif
	virtual void MovementCheck(Serializables::Input inputValue, bool IsRotate = true);
	virtual void Update(float DeltaTime);

	void Init(const ActorInfo& Info);

	UID GetId() const;
	bool GetHit() const;
	ActorType GetType() const;
	Transform& GetTransform();
	std::shared_ptr<Movement> GetMovementComp() const;
	std::shared_ptr<InventoryComponent> GetInventoryComp() const;
	float GetHp() const;
	//const Eigen::Vector3f& GetDirection() const;
	Eigen::Vector3f GetVelocity() const;
	Eigen::Vector3f GetAcceleration() const;
	bool GetIsMovable() const;
	bool GetBigHit() const;
#ifdef TSSERVER
	std::string GetTag() const;
	bool GetCanAct() const;
	bool GetCanBeDamaged() const;
	void ClearActivators();
	bool IsPlayHitAnim() const;
	void SetCanBeDamaged();
	void SetActorType(ActorType NewType);
#endif
	std::vector<BoundingVolume*>& GetBoundingVolumes();
	//std::vector<BoundingVolume*>& GetBoundingVolumes();

	void SetTranslation(float X, float Y, float Z);
	void SetRotation(const Eigen::Quaternionf& Rotation);
	void SetRotationWithDegrees(float X, float Y, float Z);
	void SetRotationWithDegrees(const TheShift::Vector3f& Degrees);
	void SetLastTranslation();
	void SetScale(float value);
	void SetIsMovable(bool Movable);
	void SetForwardRunSpeed(float NewSpeed);
	void SetHp(float NewHpValue);
	//void SetHit(bool Hit);
	bool ApplyDamage(float Damage, ActType AttackType = ActType::None, UID AttackerId = INVALID_UID, bool SetDead = false, bool HitAnimPlay = true);

	void Translate(float x, float y, float z);
	void Translate(Vector3f value);
	void Translate(Eigen::Vector3f value);
	void Rotate(const Eigen::Quaternionf& Rotation);
	void RotateWithDegrees(float x, float y, float z);
	void RotateWithDegrees(Vector3f value);
	void Scale(float value);

#ifdef TSCLIENT
	void RotateForTimeWithDegrees(Vector3f Rotation, float DurationMilliseconds);
	bool RotateForTimeOn = false;
	Vector3f AngularVelocity;
	float RotationDuration;
#endif
	
	void RegisterGameState(TheShift::GameState* BasedGameState);
	void RegisterBoundingVolume(BoundingVolume* NewVolume);

	// Platform 충돌 관련
	void SetStandingPlatform(std::shared_ptr<BoundingVolume> Platform);
	std::shared_ptr<BoundingVolume> GetStandingPlatform();
	void SetPlatformNormal(Eigen::Vector3f& PlatformSurfaceNormalVector);
	const Eigen::Vector3f& GetPlatformSurfaceNormal() const;
	bool IsStandingOnPlatform() const;
	void SetActorStandingOnPlatform(bool Value);
	void SetTag(std::string& NewTag);
	void MakeInventory();

#ifdef TSSERVER	 
	//void RegisterAddActFunc(std::function<void(Serializables::Act&)> Func);
	void SetRecognizeRange(float Range);
	void CheckRecognizeRangeAndAdd(UID OtherActorId, double SquaredDist);
	void SetCanAct(bool Value);
	void CreateAttackOBB(float Duration, TheShift::ActType AttackType = ActType::BaseAttack_1, float Damage = 80.f, 
						Eigen::Vector3f Extent = Eigen::Vector3f(100.f, 100.f, 100.f), Eigen::Vector3f RelativeLocation = Eigen::Vector3f(100.f,0.f,100.f),
						bool SetDead = false, bool IsKnockBack = true);

#endif

protected:
	UID Id;
	ActorType Type;
	StatusType CurrentStatus;
	float Hp;
	std::string Tag;
	
	//Eigen::Vector3f CurrentMoveDirection;

	// Actor의 기본 능력치
	Stats Stat;

	bool IsMovable = true;

	Transform WorldTransform;
	Eigen::Vector3f LastTranslation;

	TheShift::GameState* CurrentGameState = nullptr;
	std::shared_ptr<Movement> MovementComponent = nullptr;
	std::shared_ptr<InventoryComponent> Inventory = nullptr;
#ifdef TSSERVER
	bool God = false;
	float RecognizeRange;
	float SquaredRecognizeRange;

	// whetehr or not this actor can be damaged
	bool CanbeDamaged = true;

	// whether or not this actor can act
	bool CanAct = true;
	// For AnimNotify Activators
	std::vector<std::shared_ptr<CheckActivator>> CurrentActivators;
	std::vector<std::shared_ptr<CheckActivator>> BowChargeActivators;
	std::shared_ptr<CheckActivator> HitActivator;
#endif
	// whether or not this actor is hit at the moment
	bool IsHit = false;
	// whether or not this actor is Bighit at the moment
	bool IsBigHit = false;

	std::map<UID, double> RecognizedActors;

	std::vector<BoundingVolume*> BoundingVolumes;
	std::shared_ptr<BoundingVolume> PlatformVolume = nullptr;
	Eigen::Vector3f PlatformSurfaceNormal;
	bool StandingOnPlatform = false;

	//피격 시 피격애님을 재생하는가?->보스 몬스터는 평타몇대맞는다고 경직먹지않는다.(false이면 피격당해도 피만깎이고 상태가 취소되지않음)
	bool IsHitAnimPlay = false;
};
}
