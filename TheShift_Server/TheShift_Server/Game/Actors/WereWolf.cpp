﻿#include "WereWolf.h"
#include "Game/System/GameState.h"
#include "Game/Actors/Trigger.h"
#ifdef TSSERVER
#include "Game/System/TimerCallee.h"
#endif

using namespace std::chrono_literals;
using namespace std;
const std::set<AnimNotifyInfo>* WereWolf::WereWolfAnimNotifies = nullptr;

TheShift::WereWolf::WereWolf(UID Id) : Monster(Id)
{
	Hp = 500.f;
	Type = ActorType::BlackWareWolf;
#ifdef TSSERVER
	SetRecognizeRange(10000.f);
	SetContactRange(400.f);

	//Get Pointer of Player AnimData
	if (!WereWolfAnimNotifies)
		WereWolfAnimNotifies = AnimNotifyData::GetInstance().GetAnimNotifyInfo("WereWolf");

	pMonsterAnimNotify = WereWolfAnimNotifies;

	//플레이어보다 살짝 빠르게
	Stat.ForwardRunSpeed = 375.f;

#endif
}

void TheShift::WereWolf::OnPlayerContact(std::shared_ptr<Actor> Player)
{
	//처음으로 공격 가능한 거리가 될 시-> 보스 몬스터의 상태를 랜덤으로 결정한다.
	DecidePattern();

}

void TheShift::WereWolf::OnRecognizePlayer(std::shared_ptr<Actor> Player)
{
	Monster::OnRecognizePlayer(Player);
}

float TheShift::WereWolf::GetHitResetTime(bool IsBigHit)
{
	if (WereWolfAnimNotifies)
	{
		auto iter = WereWolfAnimNotifies->end();
		if (!IsBigHit)
		{
			iter = WereWolfAnimNotifies->find("werewolf_gethit");
			LOG_DEBUG("Wolf Hit!!");
		}
		else
		{ 
			iter = WereWolfAnimNotifies->find("werewolf_gethit2");
			LOG_DEBUG("Wolf BigHit!!");
		}
		if (iter != WereWolfAnimNotifies->end())
		{
			auto& Notifies = iter->AnimNotifies;

			for (auto& Notify : Notifies)
			{
				if (Notify.first.find("HitReset") != std::string::npos)
					return Notify.second;
			}
		}
	}

	return 0.0f;
}


//이 함수는 각 상태들의 마지막에 불려야한다.-> 특정 상태가 끝나면 다시 패턴을 결정하는 함수.
void TheShift::WereWolf::DecidePattern()
{
	if(Type != ActorType::WhiteWareWolf && (GetHit()))
		return;
	
	//각 공격이던 뭐던 상태던 끝나면 사거리 안에 플렝이어 있는지 결정
	if (ChasingActor)
	{
		if (GetDistanceToPlayer() > ContactRange)
		{
			LOG_DEBUG("Recahse Player");
			InitState();
			//StateStarted = false;
			Stat.ForwardRunSpeed = 375.f;
			ChasingActor = nullptr;
			return;
		}
	}
	if (Type == ActorType::WhiteWareWolf)
		DecideBossWolfPattern();
	//else if (Type == ActorType::BrownWareWolf)
	//	DecideArmorWolfPattern();
	else
		DecideNormalWolfPattern();

}

void TheShift::WereWolf::SetWereWolfType(ActorType Type)
{
	this->Type = Type;
	if (this->Type == ActorType::WhiteWareWolf)
	{
		IsHitAnimPlay = false;
		SetRecognizeRange(1000.f);
		Hp = 2500.f;
	}
	else
	{
		SetRecognizeRange(1000.f);
		IsHitAnimPlay = true;
	}
}

void TheShift::WereWolf::DecideBossWolfPattern()
{
	if (!StateStarted)
	{
		CurrentCombo = 0;
		MaxCombo = 1;
		LockonAttacking = true;

		/*	ComboAttacking = true;
			CurrentCombo = 0;
			MaxCombo = 3;*/
		return;
	}
	//특정 상태가 끝나고 다시 상태를 결정하러 왔다면 일단 콤보를 0으로 만들고 StateStarted를 false로 갱신한다.
	else
	{
		Stat.ForwardRunSpeed = 375.f;
		CurrentCombo = 0;
		StateStarted = false;
	}

	InitState();

	int randomPattern = rand() % 4;
	switch (randomPattern)
	{
	case 0:
	case 1:
		LockonAttacking = true;
		MaxCombo = rand() % 3 + 1;
		//CurrentCombo = 1;
		break;

	case 2:
		ComboAttacking = true;
		MaxCombo = rand() % 2 + 2;
		break;


		/*	Evading = true;
			CurrentCombo = rand() % 2 + 1;
			break;
	*/
	case 3:
		Curling = true;
		MaxCombo = rand() % 2 + 1;
		break;
	}
}

void TheShift::WereWolf::DecideNormalWolfPattern()
{

	if (!StateStarted)
	{
		ComboAttacking = true;
		CurrentCombo = 0;
		MaxCombo = 3;
		return;
	}
	//특정 상태가 끝나고 다시 상태를 결정하러 왔다면 일단 콤보를 0으로 만들고 StateStarted를 false로 갱신한다.
	else
	{
		Stat.ForwardRunSpeed = 375.f;
		CurrentCombo = 0;
		StateStarted = false;
	}

	InitState();

	int randomPattern = rand() % 4;
	switch (randomPattern)
	{
	case 0:
	case 1:
		LockonAttacking = true;
		MaxCombo = 3;
		//CurrentCombo = 1;
		break;

	case 2:
		ComboAttacking = true;
		MaxCombo = rand() % 2 + 2;
		break;

	case 3:
		Curling = true;
		MaxCombo = rand() % 2 + 1;
		break;
	}
}

void TheShift::WereWolf::DecideArmorWolfPattern()
{
	LOG_DEBUG("ARMOR");
	if (!StateStarted)
	{
		LockonAttacking = true;
		MaxCombo = 2;
		return;
	}
	//특정 상태가 끝나고 다시 상태를 결정하러 왔다면 일단 콤보를 0으로 만들고 StateStarted를 false로 갱신한다.
	else
	{
		Stat.ForwardRunSpeed = 375.f;
		CurrentCombo = 0;
		StateStarted = false;
	}

	InitState();

	int randomPattern = rand() % 4;
	switch (randomPattern)
	{
	case 0:
		LockonAttacking = true;
		MaxCombo = rand() % 1 + 2;
		//CurrentCombo = 1;
		break;

	case 1:
	case 2:
		ComboAttacking = true;
		MaxCombo = rand() % 2 + 2;
		break;

	case 3:
		Curling = true;
		MaxCombo = rand() % 2 + 1;
		break;
	}
}
