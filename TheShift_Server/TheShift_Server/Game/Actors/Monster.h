﻿#pragma once

#include "Game/Actors/Actor.h"
#ifdef TSCLIENT
#include "../../Actors/TSWeapon.h"
#endif

namespace TheShift
{

class Monster : public Actor
{
public:
	Monster(UID Id);

	virtual void Update(float DeltaTime) override;

#ifdef TSSERVER
	// 주위에 플레이어가 있는지 찾는다.
	virtual void LookForPlayer();
	void FindAndChaseTargetActor();
	//인식 범위 안의 플레이어를 랜덤으로 선택한다.
	void ChangeTargetActor();
	void FindNearestTargetActor();
	void ChaseTargetActor();

	//!! Client에선 ActorRecognizedAPlyer를 수신 시 RecognizedActors에 Actor를 추가해준다.
	// RecognizeRange보다 멀어질 경우 알아서 제거

	// LookForPlayer()가 INVALID_UID외의 UID를 return할 시 해당 Player를 따라간다.
	virtual void OnRecognizePlayer(std::shared_ptr<Actor> Player);

	// FollowPlayer중 Player와 일정 범위 내에 위치할 경우 행동을 정의한다. -> 보스몬스터의 패턴 진입함수.
	virtual void OnPlayerContact(std::shared_ptr<Actor> Player);

	// 일반몬스터가 아닌 특수몬스터들의 패턴 결정 함수
	virtual void DecidePattern() {}

	void SetContactRange(float Range);

	//모든 상태를 리셋한다.->이 함수를 호출하면 맹목적으로 플레이어를 추적만하는 상태가 된다.
	void InitState();

	//Time 뒤에행동가능하게하는 함수 
	void SetCanActWithDelay(float Duration);
#endif

protected:
	//현재 추적중인 플레이어와의 거리를 반환하는 함수
	float GetDistanceToPlayer();

	//현재 몬스터의 LookVector와 플레이어까지의 방향벡터의 사잇각.(작은각도를 반환한다).-> Z성분은 무시한다.
	float GetToPlayerAngleDegrees();

	//몬스터의 상태를 컨트롤하는 함수
	void RunMonsterAI();

	//맴돌때
	virtual void OnCurling();
	//연속공격할때
	virtual void OnComboAttack();
	//추적할떄
	virtual void OnChasing();
	//회피할때
	virtual void OnEvasion();
	//락온공격할때
	virtual void OnLockOnAttack();
	//뒤돌아서 공격할때
	virtual void OnTurnAttack();

	//어택 관련 함수
	void ManageAttackNotify(std::string AnimName);

	//점프 공격 Z값 컨트롤 함수
	void ControlZLocation();
protected:
#ifdef TSSERVER
	// Attack 또는 Skill을 사용하게 되는 거리
	// ex) Player와 이 거리보다 가까워지게 되면 공격
	// Player와 이 거리보다 가까워지게 되면 OnPlayerContact()가 불린다.
	float ContactRange;
	float SquaredContactRange;

	//현재 추적하고있는 액터의 정보
	std::shared_ptr<Actor> ChasingActor;

	//지금 추적(회전및이동) 가능한 상태인가?를 나타내는 변수->공격애님이 재생되고 있을 때는 추적(움직임)을 하면 안됨.
	bool CanChase = true;

	//지금 움직임중인가?
	bool Moving = false;

	//밑의 상태들 중 한개가 시작되어서 아직 끝나기 전인가?
	bool StateStarted = false;

	//지금 연속공격중인가
	bool ComboAttacking = false;
	
	//지금 플레이어를 맴돌고있는가
	bool Curling = false;
	
	//지금 플레이어를 추적중인가
	bool Chasing = false;

	//지금 회피액션중인가
	bool Evading = false;

	//지금 락온공격중인가
	bool LockonAttacking = false;

	//지금 뒤돌아 공격중인가
	bool TurnAttacking = false;

	//지금 콤보 카운트
	int CurrentCombo = 0;

	//맥스 콤보 카운트
	int MaxCombo = 0;

	//MoveStart와 HitNotify사이의 시간간격
	float MoveToHitTime = 0.f;

	//MoveStart한 실제 Tick
	unsigned long long MoveStartTime = 0;

	//Jump 했을 당시의 Z값.
	float OldZ = 0.f;

	//애님 노티파이 포인터
	const std::set<AnimNotifyInfo>* pMonsterAnimNotify;

	// 어그로가 너무 많이 끌린 캐릭터에게 어그로가 집중되는것을 막기 위한 어그로 제한
	// 5마리 이하의 monster에게 어그로가 끌린 플레이어는 피하고 다른 플레이어를 찾는다.
	// 아예 안따라오는건 아니고 다른 대체 플레이어가 있다면 대체 플레이어를 따라간다.
	int StableAggroCount = 5;
	bool AggroTooManyOnTarget = false;
#endif
};
}
