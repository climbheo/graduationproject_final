﻿#include "Game/Actors/Monster.h"

#include "Character.h"
#include "Game/System/GameState.h"


namespace TheShift
{
using namespace std;

Monster::Monster(UID Id) : Actor(Id)
{
	MovementComponent = std::make_shared<Movement>(&WorldTransform, &CurrentStatus, &Stat);
	IsHitAnimPlay = true;
}

void Monster::Update(float DeltaTime)
{
#ifdef TSSERVER
	// 행동을 할 수 있을때만 실행한다.
	if(CanAct && !IsHit)
		LookForPlayer();
#endif
	Actor::Update(DeltaTime);
}

#ifdef TSSERVER
// 일정 범위 안에 플레이어가 들어와있는지 확인하고 거리에 따른 행동을 한다.
// RecognizeRange	: Player를 발견함 (ex Player를 따라감)
// ContactRange		: Player와 접촉함 (ex Player를 공격함)
void Monster::LookForPlayer()
{
	RunMonsterAI();


	////현재 쫓고있는 플레이어가 없다면
	//else
	//{
	//	// 거리비교
	//	for (auto ActorDistPair : RecognizedActors)
	//	{
	//		// Player와 contact
	//		if (ActorDistPair.second < SquaredContactRange)
	//		{
	//			// Client는 Contact처리는 Packet을 수신해서 한다.
	//			// 알아서 계산해서 실행하지 않음.
	//			OnPlayerContact(ActorDistPair.first);
	//		}
	//		// Player를 발견
	//		else if (ActorDistPair.second < SquaredRecognizeRange)
	//		{
	//			OnRecognizePlayer(ActorDistPair.first);
	//		}
	//	}
	//}
}

void Monster::FindAndChaseTargetActor()
{
	//인식범위내에 액터가 없으면 행동 X.
	if (RecognizedActors.empty())
	{
		ChasingActor = nullptr;
	
		return;
	}
	//만약 쫓고있던 플레이어가 있었다면
	if (ChasingActor)
	{
		//쫓던 플레이어가 인식범위 안에 있는지 찾는다.
		auto iter_Actor = RecognizedActors.find(ChasingActor->GetId());

		//현재 인식범위 내에 없다면 추적 액터는 리셋->다시 탐색해야 함.
		if (iter_Actor == RecognizedActors.end())
		{
			auto TargetActor = CurrentGameState->GetActor(iter_Actor->first);
			if(TargetActor != nullptr)
			{
				if (TargetActor->GetType() == ActorType::Player)
				{
					// 목표 player의 chasing monster count를 1 줄인다.
					std::reinterpret_pointer_cast<Character>(TargetActor)->DecreaseChasingMonsterCount();
				}
			}
			FindNearestTargetActor();
		}

		// 현재 쫒고있는 플레이어가 aggro가 너무 많은 상태면 다시 찾아본다.
		if(std::reinterpret_pointer_cast<Character>(ChasingActor)->GetCurrentChasingMonstersCount() > StableAggroCount)
		{
			//LOG_INFO("Aggro too many on target. Searching for other player.");
			FindNearestTargetActor();
		}
	}
	else
		//없으면 무조건 대상을 찾는다.
		FindNearestTargetActor();

	//새로 찾은 대상을 추적, 공격한다.
	ChaseTargetActor();

}
void Monster::ChangeTargetActor()
{
	size_t RecognizedPlayerNum = RecognizedActors.size();
	int randomPlayerNum = rand() % RecognizedPlayerNum;

	auto iter = RecognizedActors.begin();
	for (int i = 0; i < randomPlayerNum; ++i)
		iter++;

	ChasingActor = CurrentGameState->GetActor(iter->first);
}
void Monster::FindNearestTargetActor()
{
	double finalDistance = SquaredRecognizeRange;
	UID	NearestActorID = INVALID_UID;
	double SecondFinalDistance = SquaredRecognizeRange;
	int MinimumAggroCount = INT_MAX;
	UID MinimumAggroActorID = INVALID_UID;
	for (auto& iter : RecognizedActors)
	{
		if (iter.second < finalDistance)
		{
			if(auto TargetActor = CurrentGameState->GetActor(iter.first))
			{
				if(TargetActor->GetType() == ActorType::Player)
				{
					// 상대의 어그로가 현재 monster의 어그로 적정선을 넘어섰다면 다른 대체 player를 찾는다.
					// 대체 player가 없다면 그대로 쫒는다.
					int AggroCount = std::reinterpret_pointer_cast<Character>(TargetActor)->GetCurrentChasingMonstersCount();
					if(StableAggroCount < AggroCount)
					{
						if (AggroCount < MinimumAggroCount)
						{
							MinimumAggroCount = AggroCount;
							MinimumAggroActorID = iter.first;
							SecondFinalDistance = iter.second;
						}
					}
					else
					{
						finalDistance = iter.second;
						NearestActorID = iter.first;
					}
				}
			}
		}
		//else if(iter.second < SquaredRecognizeRange)
		//{
		//	if (auto TargetActor = CurrentGameState->GetActor(iter.first))
		//	{
		//		int AggroCount = std::reinterpret_pointer_cast<Character>(TargetActor)->GetCurrentChasingMonstersCount();
		//		if (StableAggroCount < AggroCount)
		//		{
		//			if (MinimumAggroCount < AggroCount)
		//			{
		//				MinimumAggroCount = AggroCount;
		//				MinimumAggroActorID = iter.first;
		//				SecondFinalDistance = iter.second;
		//			}
		//		}
		//	}
		//}
	}

	// Aggro가 적정선 범위 내인 target actor가 없다면 aggro가 가장 적은 target을 목표로 한다.
	if(NearestActorID == INVALID_UID && MinimumAggroActorID != INVALID_UID)
	{
		NearestActorID = MinimumAggroActorID;
	}

	ChasingActor = std::reinterpret_pointer_cast<Character>(CurrentGameState->GetActor(NearestActorID));
	if (ChasingActor != nullptr)
	{
		if (ChasingActor->GetType() == ActorType::Player)
		{
			// 목표 player의 ChasingMonsterCount를 1 늘린다.
			std::reinterpret_pointer_cast<Character>(ChasingActor)->IncreaseChasingMonsterCount();
		}
	}
}
void Monster::ChaseTargetActor()
{
	//추적대상이 유효한 액터라면(nullptr가 아니라면)
	if (ChasingActor)
	{
		double Distance = RecognizedActors[ChasingActor->GetId()];
		//액터와의 거리가 Contact거리 안이면 Contact 액션을 취해라(ex, 공격)
		if (Distance < SquaredContactRange)
			OnPlayerContact(ChasingActor);
		//액터와의 거리가 Recognize거리 안이면 Recognize 액션을 취해라(ex, 추적)
		else if (Distance < SquaredRecognizeRange)
			OnRecognizePlayer(ChasingActor);
	}
}
#endif

#ifdef TSSERVER

// 이 Monster가 Player에게 공격과 같은 행동을 할 수 있게되는 거리를 set한다.
// Monster와 Player의 거리가 Range보다 가까워지게 되면 OnPlayerContact()가 불린다.
void Monster::SetContactRange(float Range)
{
	ContactRange = Range;
	SquaredContactRange = Range * Range;
}


float Monster::GetDistanceToPlayer()
{
	if (ChasingActor)
		return (GetTransform().GetTranslation() - ChasingActor->GetTransform().GetTranslation()).norm();

	return -1.f;
}

float Monster::GetToPlayerAngleDegrees()
{
	Eigen::Vector3f LookVector = WorldTransform.GetLookVector();
	Eigen::Vector3f RightVector = WorldTransform.GetRightVector();
	Eigen::Vector3f DirectionVector = (ChasingActor->GetTransform().GetTranslation() - WorldTransform.GetTranslation()).normalized();
	
	float DegreeDiff = acosf(LookVector.dot(DirectionVector)) * 180.f / M_PI;
	if (RightVector.dot(DirectionVector) < 0.f)
		DegreeDiff *= -1.f;

	return DegreeDiff;
}

void Monster::RunMonsterAI()
{
	if (Curling)
		OnCurling();
	else if (ComboAttacking)
		OnComboAttack();
	else if (Chasing)
		OnChasing();
	else if (Evading)
		OnEvasion();
	else if (LockonAttacking)
	{
		OnLockOnAttack();
		//LOG_DEBUG("Lock On Attack : %d", CurrentCombo);
	}
	else if (TurnAttacking)
		OnTurnAttack();
	//일반 몬스터는 밑의 함수만 거의불림. -> 접근, 추적, 공격
	else
	{
		//LOG_DEBUG("Default Moving");
		FindAndChaseTargetActor();
	}
}

void Monster::OnCurling()
{
	if (!StateStarted)
	{
		LOG_DEBUG("Curling Start");
		CanChase = true;
		CurrentActivators.clear();
		
		shared_ptr<CheckActivator> NewActivator = make_shared<CheckActivator>();
		int randomTime = rand() % 2 + 1;
		NewActivator->SetTimer(chrono::milliseconds(long long(randomTime * 1000.f)));

		NewActivator->Register([this]()
			{
				LOG_DEBUG("Curling End");
				DecidePattern();
			});

		CurrentGameState->AddToTimer(NewActivator);
		CurrentActivators.emplace_back(NewActivator);
			
		Stat.ForwardRunSpeed = 175.f;
		StateStarted = true;

		Serializables::ActWithValue Message;
		Message.Self = Id;
		Message.Type = ActType::Cam_Rotation;
		Message.Value = MaxCombo;
		CurrentGameState->BroadcastPacket(Message);
	}
	else
	{
		LOG_DEBUG("Curling");
		auto TargetPos = ChasingActor->GetTransform().GetTranslation();
		MovementComponent->SetDestination(TargetPos, false);
		float Direction = MaxCombo == 1 ? -1.f : 1.f; // 콤보가 1이면 왼쪽 2이면 오른쪽
		Eigen::Vector3f ScaledVector = WorldTransform.GetRightVector() * Direction;
		MovementComponent->AddImpulse(ScaledVector, 1.f);
	}
}

void Monster::OnComboAttack()
{
	if (!StateStarted)
	{
		ManageAttackNotify("Combo");

		Serializables::Act Message;
		Message.Self = Id;
		Message.Type = static_cast<ActType>(MaxCombo);
		CurrentGameState->BroadcastPacket(Message);
	}
	else
	{
		if (ChasingActor)
		{
			//공격 직전까지 플레이어를 바라본다.
			if (CanChase)
			{
				auto TargetPos = ChasingActor->GetTransform().GetTranslation();
				MovementComponent->SetDestination(TargetPos, false);
			}
			//움직이기 시작하면 룩벡터 방향으로 빠르게 움직인다.
			if (Moving)
			{
				float MoveScale = 0.f;
				float Distance = GetDistanceToPlayer();

				MoveScale = Stat.ForwardRunSpeed;
				//Stat.ForwardRunSpeed = Distance * 1.4f;

				Eigen::Vector3f ScaledLookVector = WorldTransform.GetLookVector() * MoveScale;
				MovementComponent->AddImpulse(ScaledLookVector, 1.f);
			}
			
		}

	}
}

void Monster::OnChasing()
{
	if (!StateStarted)
	{

	}
	else
	{

	}
}

void Monster::OnEvasion()
{
	if (!StateStarted)
	{
		CanChase = true;
	
		CurrentActivators.clear();
		if (pMonsterAnimNotify)
		{
			auto iter = pMonsterAnimNotify->find("Roll_L");

			if (iter != pMonsterAnimNotify->end())
			{
				auto& Notifies = iter->AnimNotifies;
				float MoveNotifyTime = 0.f;
				for (auto& Notify : Notifies)
				{
					float notifyTime = Notify.second;
					if (Notify.first.find("AttackEnd") != std::string::npos)
					{
						shared_ptr<CheckActivator> NewActivator = make_shared<CheckActivator>();
						NewActivator->SetTimer(chrono::milliseconds(long long(notifyTime * 1000.f)));
						MoveNotifyTime = notifyTime;
						NewActivator->Register([this]()
							{
								DecidePattern();
							});

						CurrentGameState->AddToTimer(NewActivator);
						CurrentActivators.emplace_back(NewActivator);
					}
				}
			}
		}
		StateStarted = true;

		Serializables::ActWithValue Message;
		Message.Self = Id;
		Message.Type = ActType::Dash;
		Message.Value = MaxCombo;
		CurrentGameState->BroadcastPacket(Message);
	}
	else
	{
		auto TargetPos = ChasingActor->GetTransform().GetTranslation();
		MovementComponent->SetDestination(TargetPos, false);
		float Direction = (MaxCombo == 1 ? -1.f : 1.f); // 콤보가 1이면 왼쪽구르기 2이면 오른쪽구르기
		Eigen::Vector3f ScaledVector = WorldTransform.GetRightVector() * Direction * Stat.ForwardRunSpeed;
		MovementComponent->AddImpulse(ScaledVector, 1.f);
	}
}

void Monster::OnLockOnAttack()
{
	if (!StateStarted)
	{
		ManageAttackNotify("_LockOn");
		Serializables::Act Message;
		Message.Self = Id;
		Message.Type = static_cast<ActType>(4 + MaxCombo);
		CurrentGameState->BroadcastPacket(Message);
		if (Type == ActorType::WhiteWareWolf)
		{
			if (MaxCombo == 1)
				MaxCombo = 8;
			else
				CurrentCombo = MaxCombo - 1;
		}
	}
	else
	{
		if (ChasingActor)
		{
			//공격 직전까지 플레이어를 바라본다.
			if (CanChase)
			{
				auto TargetPos = ChasingActor->GetTransform().GetTranslation();
				MovementComponent->SetDestination(TargetPos, false);
			}
			//움직이기 시작하면 룩벡터 방향으로 빠르게 움직인다.
			if (Moving)
			{
				float MoveScale = 0.f;
				float Distance = GetDistanceToPlayer();
				
				MoveScale = Distance * 1.4f;
				Stat.ForwardRunSpeed = Distance * 1.4f;

				Eigen::Vector3f ScaledLookVector = WorldTransform.GetLookVector()* MoveScale;
				MovementComponent->AddImpulse(ScaledLookVector, 1.f);
			}
			//점프 공격이면 z축 움직임도 컨트롤해주자.(솟아오름을 표현)
			if (MaxCombo == 2 && Moving)
				ControlZLocation();
		}

	}
}

void Monster::OnTurnAttack()
{
	if (!StateStarted)
	{

	}
	else
	{

	}
}

void Monster::InitState()
{
	CurrentActivators.clear();
	CanChase = true;
	Curling = false;
	Chasing = false;
	Evading = false;
	Moving = false;
	LockonAttacking = false;
	TurnAttacking = false;
	ComboAttacking = false;

	Serializables::Act Message;
	Message.Self = Id;
	Message.Type = ActType::None;
	CurrentGameState->BroadcastPacket(Message);
}

void Monster::SetCanActWithDelay(float Duration)
{
	shared_ptr<CheckActivator> NewActivator = make_shared<CheckActivator>();
	NewActivator->SetTimer(chrono::milliseconds(long long(Duration * 1000.f)));
	NewActivator->Register([this]()
		{
			CanAct = true;
			IsHit = false;
			IsBigHit = false;
			LOG_DEBUG("Actitvate Timer GOGO");
		});
	CurrentGameState->AddToTimer(NewActivator);
}

void Monster::ManageAttackNotify(std::string AnimName)
{
	CanChase = true;
	//현재 등록해두었던 액티베이터 삭제
	CurrentActivators.clear();
	if (pMonsterAnimNotify)
	{
		std::string animName = std::to_string(MaxCombo) + AnimName; //1_LockOn, 2_LockOn
		auto iter = pMonsterAnimNotify->find(animName);

		if (iter != pMonsterAnimNotify->end())
		{
			auto& Notifies = iter->AnimNotifies;
			float MoveNotifyTime = 0.f;
			for (auto& Notify : Notifies)
			{
				float notifyTime = Notify.second;
				if (Notify.first.find("MoveStart") != std::string::npos)
				{
					shared_ptr<CheckActivator> NewActivator = make_shared<CheckActivator>();
					NewActivator->SetTimer(chrono::milliseconds(long long(notifyTime * 1000.f)));
					MoveNotifyTime = notifyTime;
					NewActivator->Register([this]()
						{
							CanChase = true;
							//플레이어를 향해 움직이기 시작한다.
							Moving = true;
							MoveStartTime = GetTickCount();
							OldZ = WorldTransform.GetTranslation().z();
						});

					CurrentGameState->AddToTimer(NewActivator);
					CurrentActivators.emplace_back(NewActivator);
				}
				else if (Notify.first.find("ChasingStop") != std::string::npos)
				{
					shared_ptr<CheckActivator> NewActivator = make_shared<CheckActivator>();
					NewActivator->SetTimer(chrono::milliseconds(long long(notifyTime * 1000.f)));
					NewActivator->Register([this]()
						{
							//플레이어를 쳐다보는 건 그만둔다 -> 이제 플레이어방향이 아닌 직진만한다.
							CanChase = false;
						});

					CurrentGameState->AddToTimer(NewActivator);
					CurrentActivators.emplace_back(NewActivator);
				}
				else if (Notify.first.find("HitCheck") != std::string::npos)
				{
					MoveToHitTime = notifyTime - MoveNotifyTime;
					shared_ptr<CheckActivator> NewActivator = make_shared<CheckActivator>();
					NewActivator->SetTimer(chrono::milliseconds(long long(notifyTime * 1000.f)));
					NewActivator->Register([this]()
						{
							CurrentCombo++;
							//강한공격
							if (CurrentCombo == MaxCombo)
							{
								CreateAttackOBB(-1.f, ActType::Smash_1, 80.f, Eigen::Vector3f(250.f, 75.f, 75.f), Eigen::Vector3f(50.f, 0.f, 100.f));
								CurrentCombo = 0;
							}
							else
							{
								if (!LockonAttacking)
									CreateAttackOBB(-1.f, ActType::BaseAttack_1, 80.f, Eigen::Vector3f(250.f, 75.f, 75.f), Eigen::Vector3f(50.f, 0.f, 100.f));
								else
								{
									//웨어울프 워윅궁 스킬은 첫타때만 밀어낸다.
									if(CurrentCombo==1)
										CreateAttackOBB(-1.f, ActType::BaseAttack_1, 80.f, Eigen::Vector3f(250.f, 75.f, 75.f), Eigen::Vector3f(50.f, 0.f, 100.f));
									else
										CreateAttackOBB(-1.f, ActType::BaseAttack_1, 80.f, Eigen::Vector3f(250.f, 75.f, 75.f), Eigen::Vector3f(50.f, 0.f, 100.f), false, false);
								}
							}
							//공격을 끝마친순간 움직임은 멈춘다.
							if(LockonAttacking && MaxCombo == 2)
								ControlZLocation();
							Moving = false;
						});

					CurrentGameState->AddToTimer(NewActivator);
					CurrentActivators.emplace_back(NewActivator);
				}
				else if (Notify.first.find("AttackEnd") != std::string::npos)
				{
					shared_ptr<CheckActivator> NewActivator = make_shared<CheckActivator>();
					NewActivator->SetTimer(chrono::milliseconds(long long(notifyTime * 1000.f)));
					NewActivator->Register([this]()
						{
							////공격을 마쳤으면 플레이어를 다시 쳐다볼 수 있게 하면서 패턴 결정
							//CanChase = true;
							DecidePattern();
						});

					CurrentGameState->AddToTimer(NewActivator);
					CurrentActivators.emplace_back(NewActivator);
				}
			}
		}
	}
	else
		LOG_DEBUG("There is no notifyContainerPointer");
	StateStarted = true;
}

void Monster::ControlZLocation()
{
	float ActorNewZ = sinf(float(GetTickCount() - MoveStartTime) / 1000.f * 180.f / MoveToHitTime * M_PI / 180.f) * 300.f;
	Eigen::Vector3f CurrentPos = WorldTransform.GetTranslation();
	if (ActorNewZ <= 0.f)
		ActorNewZ = 0.f;
	CurrentPos.z() = OldZ + ActorNewZ;
	WorldTransform.SetTranslation(CurrentPos);
	//SetLastTranslation();
	//LOG_DEBUG("Degree : %f, Value : %f : ", float(GetTickCount() - MoveStartTime) / 1000.f * 180.f / MoveToHitTime, ActorNewZ);
}

#endif

#ifdef TSSERVER
// UID가 PlayerId인 Player를 발견했을때 하는 행동을 정의.
// Player와 이 거리보다 가까워지게 되면 불린다.
void Monster::OnRecognizePlayer(std::shared_ptr<Actor> Player)
{
	//추적할 수 없는 상태이면 쫓지마라(ex->공격 애님이 재생되고 있을 때)
	if (!CanChase)
		return;

	auto MyLocation = GetTransform().GetWorldTranslation();
	Eigen::Vector3f PlayerLocation;
	
	// Player를 향해 이동하게 한다.
	PlayerLocation = Player->GetTransform().GetWorldTranslation();
	MovementComponent->SetDestination(PlayerLocation);
}
#endif

#ifdef TSSERVER
// 어떤 Player와 일정 범위 안에 위치하게 되면 하는 행동을 정의한다.
// Player와 이 거리보다 가까워지게 되면 불린다.
// Client가 몬스터가 공격했다는 Packet수신 시 실행한다.
void Monster::OnPlayerContact(std::shared_ptr<Actor> Player)
{

}
#endif

}

