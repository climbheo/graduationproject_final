﻿#include "Game/Actors/Trigger.h"
#include "Game/System/GameState.h"
#include "Game/Components/BoundingVolume.h"
#include "Game/System/CollisionResolver.h"
#include "Game/Components/Movement.h"
#include "Game/Actors/Character.h"


namespace TheShift
{
Trigger::Trigger(UID Id) : Actor(Id)
{}

// Template function instances
template std::shared_ptr<AABB> TheShift::Trigger::AddTriggerVolume();
template std::shared_ptr<OBB> TheShift::Trigger::AddTriggerVolume();
template std::shared_ptr<BoundingSphere> TheShift::Trigger::AddTriggerVolume();
template std::shared_ptr<BoundingCapsule> TheShift::Trigger::AddTriggerVolume();

template<typename VolumeT>
typename std::shared_ptr<VolumeT> TheShift::Trigger::AddTriggerVolume()
{
	// Volume 생성
	auto MyId = GetId();
	std::shared_ptr<VolumeT> Volume = CurrentGameState->GetCollisionResolver()->CreateTrigger<VolumeT>();
	if((*CurrentGameState->GetActors()).find(MyId) != CurrentGameState->GetActors()->end())
	{
		Volume->SetParent((*CurrentGameState->GetActors())[MyId]);
	}

	// OnTriggerOverlap 설정
	Volume->SetOnTriggerOverlap([this](std::shared_ptr<BoundingVolume> Other) {
		// Overlap 이벤트는 매 프레임 불린다. 
		// 없던 Actor가 범위 안에 Enter하면
		if(std::find_if(OverlappedActors.begin(), OverlappedActors.end(),
						[Other](std::pair<std::shared_ptr<Actor>, bool> a) {
							return a.first == Other->GetParentActor();
						}) == OverlappedActors.end())
		{
			OverlappedActors.emplace_back(std::make_pair(Other->GetParentActor(), true));
			// OnTriggerEnter 실행
			if(OnTriggerEnterFunc)
			{
				OnTriggerEnterFunc(Other->GetParentActor());
			}
		}
						// 원래 범위 내에 있던 Actor이면
		else
		{
			auto Iter = std::find_if(OverlappedActors.begin(), OverlappedActors.end(), [Other](std::pair<std::shared_ptr<Actor>, bool> a) {
				return Other->GetParentActor() == a.first;
									 });
			// 해당 Actor가 Enter했음을 표기한다.
			// 나중에 Leave를 판별하기 위함. (true였는데 false면 Leave)
			// 해당 bool값은 충돌체크 완료 시 false로 set, overlap시 true로 set
			Iter->second = true;
		}
		// Overlap Func call
		if(OnTriggerOverlapFunc)
			OnTriggerOverlapFunc(Other);
								});

	// Volume 추가
	TriggerVolumes.emplace_back(Volume);

	return Volume;
}

void Trigger::SetOnTriggerEnterFunc(std::function<void(std::shared_ptr<Actor>)> Func)
{
	OnTriggerEnterFunc = Func;
}

void Trigger::SetOnTriggerLeaveFunc(std::function<void(std::shared_ptr<Actor>)> Func)
{
	OnTriggerLeaveFunc = Func;
}

void Trigger::SetOnTriggerOverlapFunc(std::function<void(std::shared_ptr<BoundingVolume>)> Func)
{
	OnTriggerOverlapFunc = Func;
}

// 이 Trigger에 Overlap
void Trigger::SetAsDamager(float Damage)
{
	SetOnTriggerEnterFunc([Damage, this](std::shared_ptr<Actor> Other) {
		// 데미지를 준 액터에 중복으로 데미지가 적용되는것을 막음
		if(Other->ApplyDamage(Damage))
		{

		}
						  });
	LifeTime = 0.01f;
}

// Trigger volume에 intersect하면 hp를 깎는다.
// 다른 actor의 공격에 의해 hp를 깎는 경우 이 함수 대신 SetAsAttack을 사용한다.
void Trigger::SetAsDamager(float Damage, float Timer)
{
	SetOnTriggerEnterFunc([Damage, this](std::shared_ptr<Actor> Other) {
		// 데미지를 준 액터에 중복으로 데미지가 적용되는것을 막음
		Other->ApplyDamage(Damage);
						  });
	LifeTime = 0.01f;
	TimerTime = Timer;
}

// Trigger volume에 intersect하면 hp를 깎는다.
// 공격자, 공격 종류에 대한 정보를 받아 클라이언트들에게 이를 알려준다.
void Trigger::SetAsAttack(float Damage, float Timer, ActType AttackType, std::shared_ptr<Actor> Attacker, bool SetDead, bool IsKnockBack)
{
	// 데미지를 준 액터에 중복으로 데미지가 적용되는것을 막음
	SetOnTriggerEnterFunc([Damage, AttackType, Attacker, this, SetDead, IsKnockBack](std::shared_ptr<Actor> Other) {
		//둘중하나라도 널포인터면 연산하지 않는다.
		if (!Attacker || !Other)
			return;
		
		// Client에서 debug용으로 쓰기 위한 공격 범위 데이터
		Serializables::AttackRangeDebug DebugData;
		for(auto& TriggerVolume : TriggerVolumes)
		{
			Serializables::BoundingVolumeDebug VolumeData;
			VolumeData.BoundingVolumeType_ = TriggerVolume->GetVolumeType();
			VolumeData.Position = TriggerVolume->GetTransform().GetWorldTranslation();
			VolumeData.Rotation = TriggerVolume->GetTransform().GetWorldDegrees();
			switch(VolumeData.BoundingVolumeType_)
			{
			case BoundingVolumeType::AABB:
				VolumeData.ScaledExtent = TriggerVolume->GetExtent();
				break;

			case BoundingVolumeType::OBB:
				VolumeData.ScaledExtent = TriggerVolume->GetExtent();
				break;

			case BoundingVolumeType::Capsule:
				VolumeData.Radius = TriggerVolume->GetRadius();
				VolumeData.HalfHeight = TriggerVolume->GetHalfHeight();
				break;

			case BoundingVolumeType::Sphere:
				VolumeData.Radius = TriggerVolume->GetRadius();
				break;
				
			case BoundingVolumeType::Unhandled:
				break;

			default:
				break;
			}

			DebugData.Info.Array.emplace_back(VolumeData);
		}
		CurrentGameState->BroadcastPacket(DebugData);


		ActorType AttackerType = Attacker->GetType();
		ActorType OtherType = Other->GetType();

		if(OtherType == ActorType::Destructible && AttackType == ActType::Teleport_End)
		{
			LOG_INFO("Destructible Destroy.");
		}

		// 둘중 하나가 Player고 나머지 하나가 Player가 아니라면 데미지 적용 (Player와 비 Player사이에서만 데미지가 오고감)
		// IsHit이 false일때만 데미지 적용
		if (/*!Other->GetHit() &&*/
			((AttackerType == ActorType::Player && OtherType != ActorType::Player) || (AttackerType != ActorType::Player && OtherType == ActorType::Player)))
		{
			if ((AttackerType != ActorType::Player && Other->GetBigHit()) || Other->GetHp() <= 0.f || !Other->GetCanBeDamaged())
			{
				//LOG_DEBUG("This Actor Already Hit or Dead or Can't be Damaged");
				return;
			}

			//피격 애니메이션이 재생되는 액터만 밀어낸다.(잡몹은 밀리나, 보스몹은 밀리지 않는다.
			//엎어지는 애님재생중인애들은 밀어내지않는다.
			if (Other->IsPlayHitAnim() && !Other->GetBigHit() && IsKnockBack)
			{
				auto HitLocation = GetTransform().GetTranslation();
				auto OtherLocation = Other->GetTransform().GetTranslation();

				OtherLocation.x() = HitLocation.x() - OtherLocation.x();
				OtherLocation.y() = HitLocation.y() - OtherLocation.y();
				OtherLocation.z() = HitLocation.z() - OtherLocation.z();

				OtherLocation.normalize();

				float MoveScale = AttackType > ActType::BaseAttack_4 ? 400.f : 100.f;
				Other->SetForwardRunSpeed(400.f);

				OtherLocation.x() *= -MoveScale;
				OtherLocation.y() *= -MoveScale;
				OtherLocation.z() *= -MoveScale;
				Other->GetMovementComp()->AddImpulse(OtherLocation, 1.f);
			}
			
			// Damage 적용
			bool IsDead = false;
			//파괴가능한오브젝트가 아니면 일반적 처리
			if (OtherType != ActorType::Destructible)
			{
				bool FinalDeadApply = (Other->GetType() == ActorType::WhiteWareWolf || Other->GetType() == ActorType::FatMonster || Other->GetType() == ActorType::SwordMonster)? false : SetDead;
				IsDead = Other->ApplyDamage(Damage, AttackType, Attacker->GetId(), FinalDeadApply);
			
			}
			else
			{
				//만약 텔레포트 어택이 아니라면 대미지 0
				if (AttackType != ActType::Teleport_Attack_1)
					IsDead = Other->ApplyDamage(0.f, AttackType, Attacker->GetId(), false);
				//텔레포트 어택이면 대미지와 상관없이 파괴.
				else
					IsDead = Other->ApplyDamage(Damage, AttackType, Attacker->GetId(), true);
			}
			if (IsDead && OtherType == ActorType::Player)
			{
				//플레이어가 죽었을때는 삭제하지 않고 키만 잠군다.
				if (Type == ActorType::Player)
					MovementComponent->LockInput();
			
			}
			else if(IsDead)
			{
				LOG_DEBUG("NonPlayerDead");
				CurrentGameState->DestroyActor(Other);
			}
		}

	});
	LifeTime = 0.01f;
	TimerTime = Timer;
}

// Trigger의 LifeTime을 설정한다.
// -값이면 무한지속
void Trigger::SetLifeTime(float Duration)
{
	LifeTime = Duration;
}

// Duration만큼 지난 후에 발동할수 있도록 한다.
void Trigger::SetTimer(float Duration)
{
	TimerTime = Duration;
}

float Trigger::GetLifeTime()
{
	return LifeTime;
}

// 발동까지 남은 시간을 return한다.
float Trigger::GetTimerLeftTime()
{
	return TimerTime;
}

void Trigger::ResetOverlapMark()
{
	for(auto& OverlappedActor : OverlappedActors)
	{
		OverlappedActor.second = false;
	}
}

// Trigger범위 내에 있다가 밖으로 나간 Actor가 있는지 check하고 
// OnTriggerLeave()가 valid하다면 실행하고 Actor list에서 삭제한다.
void Trigger::TriggerLeaveCheck()
{
	for(auto Iter = OverlappedActors.begin(); Iter != OverlappedActors.end();)
	{
		if(Iter->second == false)
		{
			if(OnTriggerLeaveFunc)
				OnTriggerLeaveFunc(Iter->first);

			Iter = OverlappedActors.erase(Iter);
			continue;
		}
		Iter++;

	}
}
}
