﻿#include "Game/Actors/Character.h"
#include "Game/System/GameState.h"
#include "Game/System/CollisionResolver.h"
#include "Game/Actors/Trigger.h"
#include "Game/Components/InventoryComponent.h"
#include "Game/Components/ItemComponent.h"
#include "Game/Components/WeaponItemComponent.h"
#include "Game/Components/ArmorItemComponent.h"

#include "Protocol/Log.h"

const std::set<AnimNotifyInfo>* Character::PlayerAnimNotifies = nullptr;

namespace TheShift
{
Character::Character(UID id) : Actor(id, ActorType::Player)
{
	IsHitAnimPlay = true;
	MovementComponent = std::make_shared<Movement>(&WorldTransform, &CurrentStatus, &Stat);
	Inventory = std::make_shared<InventoryComponent>();
	NonAccumulatedInputDirection.setZero();

	//Default Weapon Hand (MaxCombo 3)
	MaxCombo = 3;

	Hp = 2000.f;

	//Get Pointer of Player AnimData
	if (!PlayerAnimNotifies)
		PlayerAnimNotifies = AnimNotifyData::GetInstance().GetAnimNotifyInfo("Player");
}

void Character::OnInput(Serializables::Input inputValue)
{
	Actor::OnInput(inputValue);

	if(inputValue.InputValue ==InputType::Cheat_Pressed)
	{
		AttackEndComboState();
		IsHit = false;
		IsBigHit = false;
		Aiming = false;
		ReleasedAim = false;
		Dash = false;
		Teleport = false;

		MovementComponent->UnlockInput();

		NonAccumulatedInputDirection.x() = 0.f;
		NonAccumulatedInputDirection.y() = 0.f;
		
		RecentInput = inputValue;
		SetRotationWithDegrees(0.f, 0.f, inputValue.ControlRotation.Z);
		return;
	}

	//누적되지 않는 현재 캐릭터의 키 입력 상태를 계산한다.
	CalculateInputDirection(inputValue);

	if (GetHp() <= 0.f)
		MovementComponent->LockInput();
	
	//Movement Apply -> 이 Movement의 Input은 때론 입력이 누적된다(움직임이 잠겼을때 ex->공격중)
	//활이 조준중일땐 클라에서 보내주는 카메라 회전값에 따라 회전해야 한다.
	bool IsRotate = (CurrentWeaponType == WeaponType::BOW && Aiming) ? false : true;

	if (!Dash && !Teleport && !GetHit())
	{
		MovementCheck(inputValue, IsRotate);
		//LOG_DEBUG("Moevement");
	}
	switch (inputValue.InputValue)
	{
	case InputType::Shift_Pressed:
		ShiftPressing = true;
		break;

	case InputType::Shift_Released:
		ShiftPressing = false;
	
		break;

	case InputType::Camera_Rotation:
	{
		RecentInput.CamRotation = inputValue.CamRotation;
		if (CurrentWeaponType == WeaponType::BOW)
		{
			SetRotationWithDegrees(0.f, 0.f, inputValue.ControlRotation.Z);
			Serializables::ActWithVector PlayerAct;
			PlayerAct.Self = Id;
			PlayerAct.Vector = WorldTransform.GetWorldDegrees();
			PlayerAct.Type = ActType::Cam_Rotation;
			CurrentGameState->BroadcastPacket(PlayerAct);
		}
		break;
	}
	case InputType::MouseLeft_Pressed:
	{
		LOG_DEBUG("MouseLeft Pressed");
		if (!GetHit() && CurrentCombo < MaxCombo && !Dash && !Teleport)
		{
			if (!IsAttacking)
			{
				IsAttacking = true;
				
				RecentInput = inputValue;
				if (CurrentWeaponType != WeaponType::BOW)
				{
					LOG_DEBUG("--------------firstAttack------------------");
					RotateByRecentInput();
					AttackStartComboState();
				}
				else
				{ 
					CurrentCombo++;
					LOG_DEBUG("--------------Bow firstAttack------------------");
					SetRotationWithDegrees(0.f, 0.f, inputValue.ControlRotation.Z);
					Aiming = true;

					ChargeArrow();
				}
				//클라이언트로 보내야 할 정보는? -> CurrentCombo
#ifdef TSSERVER
				Serializables::ActWithVector PlayerAct;
				PlayerAct.Self = Id;
				PlayerAct.Vector = WorldTransform.GetWorldDegrees();
				PlayerAct.Type = static_cast<TheShift::ActType>(CurrentCombo);
				CurrentGameState->BroadcastPacket(PlayerAct);
#endif
				//spdlog::info("Attack Start : {0}", CurrentCombo);
			}
			//공격중이라면
			else
			{
				RecentInput = inputValue;

				if (CurrentWeaponType != WeaponType::BOW)
				{
					//다음 콤보가 나갈 수 있게 함.
					if (CanNextCombo)
						IsComboInputOn = true;
				}
				else
				{
					LOG_DEBUG("Bow Combo Input On!!");
					IsComboInputOn = true;
					ReleasedAim = false;
				}
				//spdlog::info("Input While Attacking");
			}

		}
		break;
	}
	case InputType::MouseRight_Pressed:
	{
		if (CurrentWeaponType != WeaponType::BOW)
		{
			if (!GetHit() && IsAttacking && CurrentCombo <= MaxCombo)
			{
#ifdef TSSERVER
				LOG_DEBUG("General Smash");
				RecentInput = inputValue;
				if (CurrentCombo)
				{
					CurrentCombo += int(TheShift::ActType::BaseAttack_4);
					IsComboInputOn = true;
				}
			}
#endif
		}
		else
		{
#ifdef TSSERVER
			if (!GetHit() && !Dash && !Teleport && !IsAttacking && CurrentCombo < MaxCombo && !Aiming && !ReleasedAim)
			{
				RecentInput = inputValue;
				RotateByRecentInput();
				LOG_DEBUG("------------BowSmash----------------------");
				Serializables::ActWithVector PlayerAct;
				PlayerAct.Self = Id;
				PlayerAct.Vector = WorldTransform.GetWorldDegrees();
				PlayerAct.Type = ActType::Smash_1;
				CurrentGameState->BroadcastPacket(PlayerAct);
				CurrentCombo = MaxCombo;

				MovementComponent->LockInput();

				if (PlayerAnimNotifies)
				{
					//위의 animName에 해당하는 AnimNotifies를 찾고
					auto iter = PlayerAnimNotifies->find("Archer_Smash");

					//모든 Notify를 실행한다.
					if (iter != PlayerAnimNotifies->end())
					{
						auto& Notifies = iter->AnimNotifies;
						CurrentActivators.reserve(Notifies.size());
						for (auto& Notify : Notifies)
						{
							if (Notify.first.find("HitCheck") != std::string::npos)
							{
								std::shared_ptr<CheckActivator> HitActivator = std::make_shared<CheckActivator>();
								float hitCheckTime = Notify.second;

								HitActivator->SetTimer(chrono::milliseconds(long long(hitCheckTime * 1000.f)));

								HitActivator->Register([this]()
									{
										if (God)
											CreateAttackOBB(-1.f, static_cast<TheShift::ActType>(CurrentCombo), 300.f, Eigen::Vector3f(100.f, 100.f, 100.f), Eigen::Vector3f(100.f, 100.f, 100.f), false);
										else
										CreateAttackOBB(-1.f,ActType::Smash_1);
									});

								CurrentGameState->AddToTimer(HitActivator);
								CurrentActivators.emplace_back(HitActivator);
							}
							else if (Notify.first.find("NextAttackCheck") != std::string::npos)
							{
								std::shared_ptr<CheckActivator> ComboCheckActivator = std::make_shared<CheckActivator>();
								float comboCheckTime = Notify.second;

								ComboCheckActivator->SetTimer(chrono::milliseconds(long long(comboCheckTime * 1000.f)));
								ComboCheckActivator->Register([this]()
									{
										InitAttackState();
									});
								CurrentGameState->AddToTimer(ComboCheckActivator);
								CurrentActivators.emplace_back(ComboCheckActivator);
							}
						}
					}
				}
			}
#endif
		}
		break;
	}
	case InputType::V_Pressed:
	{
		// Teleport gage가 없으면 아예 처리하지 않는다.
		if (CurrentTeleportGage < TeleportGageConsumption)
			break;
		
		//상태제약은나중에
		if (!Dash && !IsAttacking && !Teleport&& !GetHit())
		{
			LOG_DEBUG("V_RECEIVED");
			//RotateBeforState();
			SetRotationWithDegrees(0.f, 0.f, inputValue.ControlRotation.Z);
			MovementComponent->LockInput();
			InitAllState();
			
			Teleport = true;
			CanbeDamaged = false;
			Serializables::Act PlayerAct;
			PlayerAct.Self = Id;
			PlayerAct.Type = ActType::Teleport_Casting;
			CurrentGameState->BroadcastPacket(PlayerAct);

			
			if (PlayerAnimNotifies)
			{
				CurrentActivators.clear();

				if (CurrentWeaponType == WeaponType::HAND)
				{
					//제일 먼저 Casting찾기
					auto iter = PlayerAnimNotifies->find("Grappler_T_Casting");

					//첫 TeleportMove Notify 등록
					if (iter != PlayerAnimNotifies->end())
					{
						auto& Notifies = iter->AnimNotifies;
						CurrentActivators.reserve(Notifies.size());
						for (auto& Notify : Notifies)
						{
							if (Notify.first.find("TeleportMove") != std::string::npos)
							{
								std::shared_ptr<CheckActivator> TeleportMoveActivator = std::make_shared<CheckActivator>();
								float TeleportMoveTime = Notify.second;

								TeleportMoveActivator->SetTimer(chrono::milliseconds(long long(TeleportMoveTime * 1000.f)));

								TeleportMoveActivator->Register([&]()
									{
										LOG_DEBUG("TeleportMove!!");
										TeleportMove();
									});

								CurrentGameState->AddToTimer(TeleportMoveActivator);
								CurrentActivators.emplace_back(TeleportMoveActivator);
							}
						}
					}
				}
				else if (CurrentWeaponType == WeaponType::SWORD)
				{
					auto iter = PlayerAnimNotifies->find("GreatSword_TeleportAttack");
					
					//첫 TeleportMove Notify 등록
					if (iter != PlayerAnimNotifies->end())
					{
						SpendTeleportGage(200);
						
						float addTime = 0.666f * 2.f / 3.f + 1.f;
						auto& Notifies = iter->AnimNotifies;
						CurrentActivators.reserve(Notifies.size());
						for (auto& Notify : Notifies)
						{
							if (Notify.first.find("HitCheck") != std::string::npos)
							{
								std::shared_ptr<CheckActivator> TeleportMoveActivator = std::make_shared<CheckActivator>();
								float TeleportMoveTime = Notify.second;

								TeleportMoveActivator->SetTimer(chrono::milliseconds(long long((addTime+TeleportMoveTime) * 1000.f)));

								TeleportMoveActivator->Register([&]()
									{
										if (God)
											CreateAttackOBB(-1.f, static_cast<TheShift::ActType>(CurrentCombo), 300.f, Eigen::Vector3f(100.f, 100.f, 100.f), Eigen::Vector3f(100.f, 100.f, 100.f), false);
										else
										CreateAttackOBB(-1.f, ActType::Teleport_Attack_1,80.f,Eigen::Vector3f(300.f,300.f,50.f),Eigen::Vector3f(100.f,0.f,100.f),true);
									});

								CurrentGameState->AddToTimer(TeleportMoveActivator);
								CurrentActivators.emplace_back(TeleportMoveActivator);
							}
							else if (Notify.first.find("NextAttackCheck") != std::string::npos)
							{
								std::shared_ptr<CheckActivator> ComboCheckActivator = std::make_shared<CheckActivator>();
								float comboCheckTime = Notify.second;

								ComboCheckActivator->SetTimer(chrono::milliseconds(long long((addTime + comboCheckTime) * 1000.f)));
								ComboCheckActivator->Register([this]()
									{
										RotateBeforState();
										MovementComponent->UnlockInput();
										Teleport = false;
										CanbeDamaged = true;
										Serializables::ActWithVector PlayerAct;
										PlayerAct.Self = Id;
										PlayerAct.Vector = WorldTransform.GetWorldDegrees();
										PlayerAct.Type = ActType::Attack_End;//이 메세지를 받으면 공격을 끝내라는 의미
										CurrentGameState->BroadcastPacket(PlayerAct);
									});
								CurrentGameState->AddToTimer(ComboCheckActivator);
								CurrentActivators.emplace_back(ComboCheckActivator);
							}
							
						}

					}
				}
			}

		}

		break;
	}

	case InputType::SpaceBar_Pressed:
	{
		if (!Dash && !GetHit())
		{
			for (auto& oldActivator : CurrentActivators)
				oldActivator->SetActivated(true);
			CurrentActivators.clear();

			Stat.ForwardRunSpeed = DashForce;
			
			bool IsAttacked = IsAttacking;

			//현재 회전상태를 반영한다.-> 누적 X
			if(!(CurrentWeaponType == WeaponType::HAND && IsAttacked))
				RotateByRecentInput(false);

			//현재 눌려있는 키가 있다면 MovementComp에도 현재 눌려있는 방향을 반영. -> 누적 X
			if (NonAccumulatedInputDirection.norm() > 0.f)
				MovementComponent->SetInputDirection(NonAccumulatedInputDirection, RecentInput);

			//대쉬 제외하고 모든 상태 Reset
			InitAllState();
			Dash = true;
			CanbeDamaged = false;

			Serializables::ActWithValue PlayerAct;
			PlayerAct.Self = Id;
			PlayerAct.Type = ActType::Dash;

			Eigen::Vector3f LookVector = WorldTransform.GetLookVector();

			MovementComponent->LockInput();
			////대쉬 시 콤보 리셋 -> 밑의 함수 안에 MovementComponent연관 함수가있기에 여기서 호출해야함.
			//AttackEndComboState();
			//뒤로 백스텝
			string animName = "";
			if (NonAccumulatedInputDirection.norm() <= FLT_EPSILON)
			{
				LOG_DEBUG("Back!!!!!!!!!1");
				if (CurrentWeaponType == WeaponType::HAND)
					animName = "BS";
				else if (CurrentWeaponType == WeaponType::SWORD)
				{
					animName = "GreatSword_Dash";
					RotateByRecentInput(false, 180.f);
				}
				else
					animName = "Archer_BS";
				//LookVector *= -200.f;
				Transform::ScaleVector(LookVector, -DashForce, -DashForce, -DashForce);
				MovementComponent->AddImpulse(LookVector, 1.f);
				PlayerAct.Value = 1;
			}
			//나머지 방향 컨트롤
			else
			{
				if (!(CurrentWeaponType == WeaponType::HAND && IsAttacked))
				{
					LOG_DEBUG("Forward");
					Transform::ScaleVector(LookVector, DashForce, DashForce, DashForce);
					MovementComponent->AddImpulse(LookVector, 1.f);
					PlayerAct.Value = 0;

					if (CurrentWeaponType == WeaponType::BOW)
						animName = "Archer_Dash";
					else if(CurrentWeaponType == WeaponType::SWORD)
						animName = "GreatSword_Dash";
					else
						animName = "FS";
				}
				//맨손 공격중 대쉬는 적용방식이 다르다.
				else
				{
					if (NonAccumulatedInputDirection.x() > 0.f)
					{
						RotateByRecentInput(false);
						LookVector = WorldTransform.GetLookVector();
						Transform::ScaleVector(LookVector, DashForce, DashForce, DashForce);
						MovementComponent->AddImpulse(LookVector, 1.f);
						
						PlayerAct.Value = 0;
						animName = "FS";
					}
					else if(NonAccumulatedInputDirection.x() < 0.f)
					{
						//뒤왼
						if (NonAccumulatedInputDirection.y() < 0.f)
						{
							RotateByRecentInput(false, 180.f);
							LookVector = -WorldTransform.GetRightVector() - WorldTransform.GetLookVector();
							LookVector.normalize();
							Transform::ScaleVector(LookVector, DashForce, DashForce, DashForce);
							MovementComponent->AddImpulse(LookVector, 1.f);

							PlayerAct.Value = 2;
							animName = "LS";
						}
						//뒤오
						else if (NonAccumulatedInputDirection.y() > 0.f)
						{
							RotateByRecentInput(false, -180.f);
							LookVector = WorldTransform.GetRightVector() - WorldTransform.GetLookVector();
							LookVector.normalize();
							Transform::ScaleVector(LookVector, DashForce, DashForce, DashForce);
							MovementComponent->AddImpulse(LookVector, 1.f);

							PlayerAct.Value = 3;
							animName = "RS";
						}
						//뒤
						else
						{
							RotateByRecentInput(false, 180.f);
							LookVector = WorldTransform.GetLookVector();
							Transform::ScaleVector(LookVector, -DashForce, -DashForce, -DashForce);
							MovementComponent->AddImpulse(LookVector, 1.f);

							PlayerAct.Value = 1;
							animName = "BS";
						}
					}
					else
					{
						//왼
						if (NonAccumulatedInputDirection.y() < 0.f)
						{
							RotateByRecentInput(false, 135.f);
							LookVector = -WorldTransform.GetRightVector() - WorldTransform.GetLookVector();
							LookVector.normalize();
							Transform::ScaleVector(LookVector, DashForce, DashForce, DashForce);
							MovementComponent->AddImpulse(LookVector, 1.f);

							PlayerAct.Value = 2;
							animName = "LS";
						}
						//오
						else if (NonAccumulatedInputDirection.y() > 0.f)
						{
							RotateByRecentInput(false, -135.f);
							LookVector = WorldTransform.GetRightVector() - WorldTransform.GetLookVector();
							LookVector.normalize();
							Transform::ScaleVector(LookVector, DashForce, DashForce, DashForce);
							MovementComponent->AddImpulse(LookVector, 1.f);

							PlayerAct.Value = 3;
							animName = "RS";
						}
					}

				}
			}
			
			if (PlayerAnimNotifies)
			{
				//위의 animName에 해당하는 AnimNotifies를 찾고
				auto iter = PlayerAnimNotifies->find(animName);

				//모든 Notify를 실행한다.
				if (iter != PlayerAnimNotifies->end())
				{
					auto& Notifies = iter->AnimNotifies;
				
					for (auto& Notify : Notifies)
					{
						if (Notify.first.find("DashEnd") != std::string::npos)
						{
							std::shared_ptr<CheckActivator> DashEndActivator = std::make_shared<CheckActivator>();
							float DashEndTime = Notify.second;

							DashEndActivator->SetTimer(chrono::milliseconds(long long(DashEndTime * 1000.f)));

							DashEndActivator->Register([&]()
													   {
														   Stat.ForwardRunSpeed = 300.f;
														   LOG_DEBUG("DASHEND!!!!!!!!!!1");
														   Dash = false;
														   CanbeDamaged = true;
														   AttackEndComboState();
														   Eigen::Vector3f newVelocity(0.f, 0.f, 0.f);
														   MovementComponent->SetVelocity(newVelocity);
														  
														   Serializables::ActWithVector PlayerAct;
														   PlayerAct.Self = Id;
														   PlayerAct.Vector = WorldTransform.GetWorldDegrees();
														   PlayerAct.Type = ActType::Dash_End;
														   CurrentGameState->BroadcastPacket(PlayerAct);
													   });

							CurrentGameState->AddToTimer(DashEndActivator);
							
						}
					}
				}
			}
			CurrentGameState->BroadcastPacket(PlayerAct);
		}
		break;
	}
	case InputType::MouseLeft_Released:
	{
		if (GetHit() || TeleportAttacking)
			break;
		if (CurrentWeaponType == WeaponType::BOW)
		{
			if (Aiming)
			{
				//이미 ReleasedAim이 아니라면
				if (!ReleasedAim)
				{
					auto iter = PlayerAnimNotifies->find("Aim_Recoil");

					//모든 Notify를 실행한다.
					if (iter != PlayerAnimNotifies->end())
					{
						auto& Notifies = iter->AnimNotifies;
						CurrentActivators.reserve(Notifies.size());
						for (auto& Notify : Notifies)
						{
							if (Notify.first.find("NextAttackCheck") != std::string::npos)
							{
								std::shared_ptr<CheckActivator> NewActivator = std::make_shared<CheckActivator>();
								float DashEndTime = Notify.second;

								NewActivator->SetTimer(chrono::milliseconds(long long(DashEndTime * 1000.f)));

								NewActivator->Register([&]()
									{
										LOG_DEBUG("AttackCheckNotify_Bow");
										if (IsComboInputOn)
										{
											Serializables::ActWithVector PlayerAct;
											PlayerAct.Self = Id;
											PlayerAct.Vector = WorldTransform.GetWorldDegrees();
											PlayerAct.Type = ActType::BaseAttack_4; //이메세지를 받으면 다시 공격을 이어가라는 의미

											IsComboInputOn = false;
											ReleasedAim = false;
											Aiming = true;
											CurrentCombo = 1;
											CurrentGameState->BroadcastPacket(PlayerAct);
											LOG_DEBUG("AttackComboNotify_Bow");

											ChargeArrow();
										}
										else
										{
											LOG_DEBUG("AttackEndNotify_Bow");
											IsAttacking = false;
											IsComboInputOn = false;
											ReleasedAim = false;
											Aiming = false;
											CurrentCombo = 0;

											RotateByRecentInput(false);

											//현재 눌려있는 키가 있다면 MovementComp에도 현재 눌려있는 방향을 반영. -> 누적 X
											if (NonAccumulatedInputDirection.norm() > 0.f)
												MovementComponent->SetInputDirection(NonAccumulatedInputDirection, RecentInput);

											Serializables::ActWithVector PlayerAct;
											PlayerAct.Self = Id;
											PlayerAct.Vector = WorldTransform.GetWorldDegrees();
											PlayerAct.Type = ActType::Attack_End;//이 메세지를 받으면 공격을 끝내라는 의미
											CurrentGameState->BroadcastPacket(PlayerAct);
										}

									});
								CurrentActivators.emplace_back(NewActivator);
								CurrentGameState->AddToTimer(NewActivator);
							
							}
						}
					}
					Aiming = false;
					if(!Teleport)
						ReleasedAim = true;
					
					Serializables::ActWithVector PlayerAct;
					PlayerAct.Self = Id;
					PlayerAct.Vector = WorldTransform.GetWorldDegrees();
					PlayerAct.Type = ActType::Attack_End;
					CurrentGameState->BroadcastPacket(PlayerAct);
					LOG_DEBUG("Aim END!!_Bow1");
				}
				FireArrow();
				
			}
			else
			{
				if (!IsComboInputOn)
				{
					Serializables::ActWithVector PlayerAct;
					PlayerAct.Self = Id;
					PlayerAct.Vector = WorldTransform.GetWorldDegrees();
					PlayerAct.Type = ActType::Attack_End;
					CurrentGameState->BroadcastPacket(PlayerAct);
					LOG_DEBUG("Aim END!!_Bow2");
				}
				IsComboInputOn = false;
				LOG_DEBUG("MouseLeft_Released!!");
			}
		}

		break;
	}

	
	default:
		break;
	}

	LastProcessedInputId = inputValue.InputId;
}

void Character::OnHit(ActType AttackType)
{
	Actor::OnHit(AttackType);
	// 맞는 중엔 스스로 움직일 수 없도록 한다.
	if (MovementComponent != nullptr)
		MovementComponent->LockInput();

	InitAllState();
}

void Character::Update(float DeltaTime)
{
	Actor::Update(DeltaTime);

	//LOG_DEBUG("current hp : %f", Hp);

	if (ShiftPressing)
	{
		if (!IsAttacking && !Aiming && !Teleport && !GetHit() && !ReleasedAim && !TeleportAttacking && !Dash)
			Stat.ForwardRunSpeed = 450.f;
	}
	else
	{
		if (!IsAttacking && !Aiming && !Teleport && !ReleasedAim && !TeleportAttacking && !Dash)
			Stat.ForwardRunSpeed = 300.f;
	}


	//LOG_DEBUG("Player Hp : %f", GetHp());

	/*LOG_DEBUG("InputDirection : %f, %f", MovementComponent->GetInputDirection().x(), MovementComponent->GetInputDirection().y());
	
	LOG_DEBUG("NonAccumulatedInputDirection : %f, %f", NonAccumulatedInputDirection.x(), NonAccumulatedInputDirection.y());*/
}

#ifdef TSSERVER
// 새로운 Type의 무기로 바꾼다.
void Character::SwapWeapon(WeaponType Type)
{
	CurrentWeaponType = Type;
	switch (CurrentWeaponType)
	{
	case WeaponType::SWORD:
		MaxCombo = 4;
		DashForce = 650.f;
		break;

	case WeaponType::HAND:
		DashForce = 600.f;
		MaxCombo = 3;
		break;
	case WeaponType::BOW:
		DashForce = 900.f;
		MaxCombo = 3;
		break;

	}
}

// 새로운 Type의 방어구로 바꾼다.
void Character::SwapArmor(ArmorItemType Type)
{
	CurrentArmorType = Type;
	switch(CurrentArmorType)
	{
	case ArmorItemType::None:
		break;

	case ArmorItemType::HeavyArmor:
		break;

	case ArmorItemType::LightArmor:
		break;

	default:
		break;
	}
}

// Teleport가능한 위치를 return한다.
// TargetActor를 향해 이동, TargetActor의 위치에서 OffsetPositionAfterTeleport만큼 떨어진 곳이 목적지
// Teleport후 충돌하는 위치에 있다면 충돌을 계산한 위치를 return한다.
Eigen::Vector3f Character::CalculateSafeTeleportDestination(std::shared_ptr<Actor> TargetActor,
															Eigen::Vector3f OffsetPositionAfterTeleport)
{
	// 1. TargetActor의 CloseVolumes를 가져온다.
	Eigen::Vector3f TargetActorPosition = TargetActor->GetTransform().GetWorldTranslation();
	auto TargetActorBoundingVolumes = TargetActor->GetBoundingVolumes();

	// Target actor의 capsule volume을 찾는다.
	//TODO: Actor의 가장 최상위 volume을 따로 저장하도록하는게 맞나...?
	auto TargetActorCapsuleIter = std::find_if(TargetActorBoundingVolumes.begin(), TargetActorBoundingVolumes.end(), [](BoundingVolume* Element)
										   {
											   return Element->GetVolumeType() == BoundingVolumeType::Capsule;
										   });

	// Target actor에 Capsule이 없다면 제자리 위치를 return한다.
	if(TargetActorCapsuleIter == TargetActorBoundingVolumes.end())
	{
		return GetTransform().GetWorldTranslation();
	}

	// Target actor의 주변 volume들
	auto TargetActorCloseVolumes = (*TargetActorCapsuleIter)->CloseVolumes;

	// 내 BoundingVolume중 Capsule을 찾는다.
	auto CapsuleIter = std::find_if(BoundingVolumes.begin(), BoundingVolumes.end(), [](BoundingVolume* Element)
									{
										return Element->GetVolumeType() == BoundingVolumeType::Capsule;
									});

	// Capsule이 없다면 제자리 위치를 return한다.
	if(CapsuleIter == BoundingVolumes.end())
	{
		return GetTransform().GetWorldTranslation();
	}
	
	BoundingCapsule* MyCapsule = reinterpret_cast<BoundingCapsule*>(*CapsuleIter);
	float MyRadius = MyCapsule->GetRadius();
	float MyHalfHeight = MyCapsule->GetHalfHeight();

	if(TargetActor->IsStandingOnPlatform())
	{
		Eigen::Quaternionf PlatformQuat = Transform::GetFromToRotation(Eigen::Vector3f::UnitZ(), TargetActor->GetPlatformSurfaceNormal());
		OffsetPositionAfterTeleport = PlatformQuat * OffsetPositionAfterTeleport;
		//OffsetPositionAfterTeleport = Transform::RotateVector(OffsetPositionAfterTeleport, PlatformQuat);
	}
	float OffsetLength = OffsetPositionAfterTeleport.norm();
	Eigen::Vector3f NormalizedOffset = OffsetPositionAfterTeleport.normalized();

	// 내 capsule의 하위 sphere들에서 출발하는 ray를 만들어 target actor에서 목표 위치까지 충돌하는 volume이 있나 확인한다.
	float ClosestContactDist = OffsetLength;
	for (auto& Sphere : MyCapsule->Spheres)
	{
		// 3. TargetActor -> 2번의 결과값을 ray로 만들고 CloseVolumes와 Intersect검사를 한다.
		Ray FromTargetToTeleportOffsetRay;
		// 벽에 너무 붙어서 Ray를 쏠 경우 Ray의 시작이 박스 안에서 시작할 가능성을 배제시키기 위해
		Eigen::Vector3f SphereHeightOffset(TargetActorPosition.x(), TargetActorPosition.y(), TargetActorPosition.z() + Sphere->GetRelativeLocation().z() + MyHalfHeight/* + 1.f*/);
		FromTargetToTeleportOffsetRay.StartPoint = SphereHeightOffset + TargetActor->GetPlatformSurfaceNormal();// - NormalizedOffset/* * 5.f*/;
		FromTargetToTeleportOffsetRay.Direction = NormalizedOffset;
		FromTargetToTeleportOffsetRay.SetLength(OffsetLength);

		// Target actor의 주변에 있는 volume들과 검사를 한다.
		for (auto CloseVolumeId : TargetActorCloseVolumes)
		{
			auto Volume = CurrentGameState->GetCollisionResolver()->GetBoundingVolume(CloseVolumeId);
			// 움직이지 않는 벽들과 같은 volume에 대해서만 체크한다.
			if (Volume->GetParentActor()->GetIsMovable())
				continue;
			float ContactDist;
			bool DoesIntersect = FromTargetToTeleportOffsetRay.Intersect(Volume.get(), &ContactDist, MyRadius, false);

			// Intersect하고 ContactDist가 ClosestContactDist보다 작으면 값을 바꿔 저장한다.
			if (DoesIntersect && ContactDist < ClosestContactDist)
			{
				ClosestContactDist = ContactDist;
			}
		}
	}

	// 4. 가장 가까운 값을 Ray의 direction으로 해당 값만큼 이동된 vector를 return한다.
	//return TargetActorPosition + NormalizedRotatedOffset * ClosestContactDist;
	return TargetActorPosition + NormalizedOffset * (ClosestContactDist/* - 1.f*/);
}

WeaponType Character::GetCurrentWeaponType() const
{
	return CurrentWeaponType;
}

ArmorItemType Character::GetCurrentArmorType() const
{
	return CurrentArmorType;
}

void Character::SetBowScreenTargets(std::vector<UID>* TargetIds)
{
	if (TeleportAttacking)
		return;
	BowScreenTargets = *TargetIds;
	LOG_DEBUG("Target num : %d", TargetIds->size());
	
	if (Teleport && CurrentWeaponType == WeaponType::BOW && BowScreenTargets.empty())
	{
		if (MovementComponent->IsLocked())
			MovementComponent->UnlockInput();
		Teleport = false;
		CanbeDamaged = true;
		//CurrentCombo = 0;
		ReleasedAim = false;
		CurrentCombo = 0;

		Serializables::ActWithVector PlayerAct;
		PlayerAct.Self = Id;
		PlayerAct.Vector = WorldTransform.GetWorldDegrees();
		PlayerAct.Type = ActType::Attack_End;
		CurrentGameState->BroadcastPacket(PlayerAct);
	}
	else if (Teleport && CurrentWeaponType == WeaponType::BOW)
	{
		TeleportAttacking = true;
		LOG_DEBUG("TeleportMove_Bow");
		std::shared_ptr<Activator> TeleportActivator = std::make_shared<Activator>();
		TeleportActivator->SetTimer(chrono::milliseconds(long long(0.3f * 1000.f)));

		TeleportActivator->Register([this]()
			{
				TeleportMove_Bow();

			});

		CurrentGameState->AddToTimer(TeleportActivator);
	}
}
float Character::GetHitResetTime(bool IsBigHit)
{

	if (PlayerAnimNotifies)
	{
		if (!IsBigHit)
		{
			string animName = "";
			switch (CurrentWeaponType)
			{
			case WeaponType::HAND:
				animName = "Grappler_Hit";
				break;

			case WeaponType::SWORD:
				animName = "GreatSword_Hit";
				break;

			case WeaponType::BOW:
				animName = "Archer_Hit";
				break;
			}
			auto iter = PlayerAnimNotifies->find(animName);

			if (iter != PlayerAnimNotifies->end())
			{
				auto& Notifies = iter->AnimNotifies;

				for (auto& Notify : Notifies)
				{
					if (Notify.first.find("HitReset") != std::string::npos)
						return Notify.second;
				}
			}
		}
		else
			return 2.75f;
	}

	return 0.0f;
}

// 현재 이 player를 쫒고있는 몬스터 수를 1 늘린다.
void Character::IncreaseChasingMonsterCount()
{
	++CurrentChasingMonstersCount;
}

// 현재 이 player를 쫒고있는 몬스터 수를 1 줄인다.
void Character::DecreaseChasingMonsterCount()
{
	--CurrentChasingMonstersCount;
}

// 현재 남아있는 teleport gage를 return한다.
int Character::GetCurrentTeleportGage() const
{
	return CurrentTeleportGage;
}

void Character::SetCurrentTeleportGage(int NewValue)
{
	CurrentTeleportGage = NewValue;
}

// Teleport 사용이 가능한지 확인하고 가능하다면 gage를 깎는다.
bool Character::SpendTeleportGage(int Consumption)
{
	if (God)
		return true;
	
	if(CurrentTeleportGage >= Consumption)
	{
		CurrentTeleportGage -= Consumption;
		Serializables::TeleportGageChange Data;
		Data.Id = Id;
		Data.Gage = CurrentTeleportGage;
		CurrentGameState->BroadcastPacket(Data);

		return true;
	}

	return false;
}

// Consumption만큼의 teleport gage를 소모할 수 있는 상황인지 여부를 return한다.
bool Character::DoesHaveEnoughTeleportGage(int Consumption)
{
	if (CurrentTeleportGage >= Consumption)
		return true;
	else
		return false;
}

int Character::GetCurrentStamina() const
{
	return CurrentStamina;
}

void Character::SetCurrentStamina(int NewValue)
{
	CurrentStamina = NewValue;
}

bool Character::SpendStamina(int Consumption)
{
	if(CurrentStamina >= Consumption)
	{
		CurrentStamina -= Consumption;
		Serializables::StaminaChange Data;
		Data.Id = Id;
		Data.NewStaminaValue = CurrentStamina;
		CurrentGameState->BroadcastPacket(Data);

		return true;
	}

	return false;
}

// Consumption만큼의 stamina를 사용할 수 있는 상황인지 여부를 return한다.
bool Character::DoesHaveEnoughStamina(int Consumption)
{
	if (CurrentStamina >= Consumption)
		return true;
	else
		return false;
}

// 현재 이 player를 쫒고있는 몬스터의 수를 return한다.
int Character::GetCurrentChasingMonstersCount()
{
	return CurrentChasingMonstersCount;
}
#endif

uint32_t Character::GetLastProcessedInputId() const
{
	return LastProcessedInputId;
}

float Character::GetMaxHp() const
{
	return MaxHp;
}


void Character::InitAttackState(bool SendPacket)
{
	//여기서 보내야함. Attack이 끝낫다는것을
	Aiming = false;
	ReleasedAim = false;
	AttackEndComboState();
	Stat.ForwardRunSpeed = 300.f;
#ifdef TSSERVER
	if (SendPacket)
	{
		LOG_DEBUG("----------Attack End Packet Send!!!---------");
		Serializables::ActWithVector PlayerAct;
		PlayerAct.Self = Id;
		PlayerAct.Vector = WorldTransform.GetWorldDegrees();
		PlayerAct.Type = ActType::Attack_End;
		CurrentGameState->BroadcastPacket(PlayerAct);
	}
#endif
}


void Character::AttackComboCheck(bool IsSmash)
{
	//BaseAttack은 MaxCombo가 최댓값, 지금 로직대로면 밑을 타지않음.
	//스매쉬와 평타의 조건을 나눈다.
	if ((IsSmash && CurrentCombo <= MaxCombo) || (!IsSmash && CurrentCombo < MaxCombo))
	{
		CanNextCombo = false;

		if (IsComboInputOn && !Dash)
		{
			RotateByRecentInput();
			AttackStartComboState();
			//LOG_DEBUG("------------ComboCheck----------------------");
#ifdef TSSERVER
			Serializables::ActWithVector PlayerAct;
			PlayerAct.Self = Id;
			PlayerAct.Vector = WorldTransform.GetWorldDegrees();
			PlayerAct.Type = static_cast<TheShift::ActType>(CurrentCombo);
			CurrentGameState->BroadcastPacket(PlayerAct);
#endif
			//spdlog::info("Notify : {0}", CurrentCombo);
		}
		else
			InitAttackState(); //콤보가 눌리지 않았다면
	}
	else
	{
		//콤보가 끝낫다면

		//스매쉬는 무조건 종료
		if (IsSmash  || Dash)
			InitAttackState();
		else
		{
			//평타중에 스매쉬를 쳤다면? 이 노티파이는 발동되면안됨.
			if (!IsComboInputOn || Dash)
				InitAttackState();
			else
			{
				RotateByRecentInput();

				Serializables::ActWithVector PlayerAct;
				PlayerAct.Self = Id;
				PlayerAct.Vector = WorldTransform.GetWorldDegrees();
				PlayerAct.Type = static_cast<TheShift::ActType>(CurrentCombo);
				CurrentGameState->BroadcastPacket(PlayerAct);

				auto lookVector = WorldTransform.GetLookVector();

				AttackStartComboState(true);

			}
		}
	}
}

int Character::GetMaxTeleportGage() const
{
	return MaxTeleportGage;
}

int Character::GetMaxStamina() const
{
	return MaxStamina;
}

void Character::AttackStartComboState(bool IsSmash)
{
	if (CurrentCombo != (MaxCombo - 1))
		CanNextCombo = true;
	IsComboInputOn = false;

	MovementComponent->LockInput();

	int32_t IncreasedCombo = 0;
	if (IsSmash)
		IncreasedCombo = CurrentCombo;
	else
		IncreasedCombo = (CurrentCombo >= 0 && CurrentCombo < MaxCombo) ? (CurrentCombo + 1) : MaxCombo;

	/*LOG_DEBUG("CurrentCombo : %d, ControlRotation.X,Y,Z : %f, %f, %f", CurrentCombo, inputValue.ControlRotation.X, inputValue.ControlRotation.Y, inputValue.ControlRotation.Z);
	LOG_DEBUG("CurrentCombo : %d, CamRotation.X,Y,Z : %f, %f, %f", CurrentCombo, inputValue.CamRotation.X, inputValue.CamRotation.Y, inputValue.CamRotation.Z);
*/
	if (IncreasedCombo != CurrentCombo || IsSmash)
	{
		/*	for (auto& oldActivator : CurrentActivators)
				oldActivator->SetActivated(true);*/

				//현재 상태의 Activator만 저장하기 위해 기존 Activator는 지운다.
		CurrentActivators.clear();

		CurrentCombo = IncreasedCombo;
		auto lookVector = WorldTransform.GetLookVector();

		//Smash  Anim Notify Name -> (WeaponType)_Smash0(n)  ex) GreatSword_Smash01
		//Attack Anim Notify Name -> (WeaponType)_Attack0(n) ex) Grappler_Attack1 -> Attack은 0이빠져있음 주의!
		string animName = "";
		switch (CurrentWeaponType)
		{
		case WeaponType::HAND:
			animName = "Grappler_";
			break;

		case WeaponType::SWORD:
			animName = "GreatSword_";
			break;
		}

		//For Attack
		if (!IsSmash)
			animName += ("Attack" + to_string(IncreasedCombo));
		//For Smash
		else
		{
			IncreasedCombo -= int(TheShift::ActType::BaseAttack_4);
			animName += ("Smash0" + to_string(IncreasedCombo));
			//LOG_DEBUG("SMash");
		}

		if (PlayerAnimNotifies)
		{
			//위의 animName에 해당하는 AnimNotifies를 찾고
			auto iter = PlayerAnimNotifies->find(animName);

			//모든 Notify를 실행한다.
			if (iter != PlayerAnimNotifies->end())
			{
				auto& Notifies = iter->AnimNotifies;
				CurrentActivators.reserve(Notifies.size());
				for (auto& Notify : Notifies)
				{
					if (Notify.first.find("HitCheck") != std::string::npos)
					{
						std::shared_ptr<CheckActivator> HitActivator = std::make_shared<CheckActivator>();
						float hitCheckTime = Notify.second;

						HitActivator->SetTimer(chrono::milliseconds(long long(hitCheckTime * 1000.f)));

						HitActivator->Register([this]()
											   {
													if(God)
														CreateAttackOBB(-1.f, static_cast<TheShift::ActType>(CurrentCombo),300.f, Eigen::Vector3f(100.f,100.f,100.f), Eigen::Vector3f(100.f, 100.f, 100.f),false);
													else
														CreateAttackOBB(-1.f, static_cast<TheShift::ActType>(CurrentCombo));
											   });

						CurrentGameState->AddToTimer(HitActivator);
						CurrentActivators.emplace_back(HitActivator);
					}
					else if (Notify.first.find("NextAttackCheck") != std::string::npos)
					{
						std::shared_ptr<CheckActivator> ComboCheckActivator = std::make_shared<CheckActivator>();
						float comboCheckTime = Notify.second;

						ComboCheckActivator->SetTimer(chrono::milliseconds(long long(comboCheckTime * 1000.f)));
						ComboCheckActivator->Register([this, IsSmash]()
													  {
														  AttackComboCheck(IsSmash);
													  });
						CurrentGameState->AddToTimer(ComboCheckActivator);
						CurrentActivators.emplace_back(ComboCheckActivator);
					}
				}
			}
		}
		//LOG_DEBUG("-----------------------------------");
		/*lookVector *= 100.f;*/
		float moveScale = 0.f;
		switch (CurrentWeaponType)
		{
		case WeaponType::SWORD:
			moveScale = 450.f;
			break;
		case WeaponType::HAND:
			moveScale = 300.f;
			break;
		}
		Stat.ForwardRunSpeed = moveScale;
		Transform::ScaleVector(lookVector, moveScale, moveScale, moveScale);
		//LOG_DEBUG("lookVector : %f, %f, %f", lookVector.x(), lookVector.y(), lookVector.z());
		//LOG_DEBUG("ATTATCKSTARTCOMBOSTATE : ADDFORCE");
		MovementComponent->AddImpulse(lookVector, 1.f);
	}
}

void Character::AttackEndComboState()
{
	
	//현재 회전상태를 반영한다.-> 누적 X
	RotateByRecentInput(false);
	//MovementComp Lock을 풀어준다
	MovementComponent->UnlockInput();
	//현재 눌려있는 키가 있다면 MovementComp에도 현재 눌려있는 방향을 반영. -> 누적 X
	if (NonAccumulatedInputDirection.norm() > 0.f)
		MovementComponent->SetInputDirection(NonAccumulatedInputDirection, RecentInput);

	IsComboInputOn = false;
	CanNextCombo = false;
	CurrentCombo = 0;
	IsAttacking = false;
}

void Character::RotateBeforState()
{
	RotateByRecentInput(false);

	//현재 눌려있는 키가 있다면 MovementComp에도 현재 눌려있는 방향을 반영. -> 누적 X
	if (NonAccumulatedInputDirection.norm() > 0.f)
		MovementComponent->SetInputDirection(NonAccumulatedInputDirection, RecentInput);
}

void Character::ChargeArrow()
{
	for (auto& iter : BowChargeActivators)
		iter->SetActivated(true);
	BowChargeActivators.clear();

	for (int i = 0; i < 2; ++i)
	{
		std::shared_ptr<CheckActivator> BowComboActivator = std::make_shared<CheckActivator>();

		BowComboActivator->SetTimer(chrono::milliseconds(long long((1.f + 1.f * float(i)) * 1000.f)));

		BowComboActivator->Register([&]()
			{
				if (IsAttacking && !ReleasedAim)
				{
					CurrentCombo++;
					Serializables::ActWithVector PlayerAct;
					PlayerAct.Self = Id;
					PlayerAct.Vector = WorldTransform.GetWorldDegrees();
					PlayerAct.Type = static_cast<TheShift::ActType>(CurrentCombo);
					CurrentGameState->BroadcastPacket(PlayerAct);
					if (CurrentCombo == 3)
						CurrentCombo = 0;
				}
			});
		CurrentGameState->AddToTimer(BowComboActivator);
		BowChargeActivators.emplace_back(BowComboActivator);
	}
}

void Character::TeleportMove()
{
	//여기서 가장가까운 몬스터를 찾는다. 
	//만약 찾는다면 콤보 증가
	//못찾는다면 그대로 콤보 0으로만들고 텔레포트기술 종료

	auto NearestMonster = CurrentGameState->GetNearestActorByType(ActorType::KnightMonster, this, &TargetedActors);//나중에 TypeLst넣어주기.
	if (NearestMonster && CurrentCombo != MaxCombo)
	{
		SpendTeleportGage(200);
		
		NearestMonster->SetCanAct(false);
		TargetedActors.emplace_back(NearestMonster);
		auto& TargetTransform = NearestMonster->GetTransform();
		Eigen::Vector3f MonsterPos = TargetTransform.GetTranslation();
		//WorldTransform.SetTranslation(MonsterPos + TargetTransform.GetLookVector() * -250.f);
		Eigen::Vector3f TargetPosition = CalculateSafeTeleportDestination(NearestMonster, -TargetTransform.GetLookVector() * 150.f);
		WorldTransform.SetTranslation(TargetPosition);
		SetLastTranslation();

		//그 몬스터의 뒤로 이동시키고 이동시킨다.->이동은 매프레임
		Eigen::Vector3f TargetVector = (MonsterPos - WorldTransform.GetTranslation());

		LOG_DEBUG("TargetVector : %f, %f, %f", TargetVector.x(), TargetVector.y(), TargetVector.z());
		LOG_DEBUG("TargetVector's Size : %f", TargetVector.norm());
		TargetVector.normalize();


		//Eigen::Vector3f RelativeDirection = TargetVector.normalized();
		//WorldTransform.SetRotation(Transform::GetFromToRotation(Eigen::Vector3f::UnitX(), RelativeDirection));
		//MovementComponent->SetDestination(MonsterPos);
		Eigen::Vector3f Dir = TargetVector.normalized();

		// 해당 방향으로 z축 회전
		float Dot = Dir.dot(Eigen::Vector3f(1.f, 0.f, 0.f));
		// 0~180 도 사이의 값 0~1
		float AcosAngle = std::acos(Dot);

		Eigen::Vector3f Cross = Dir.cross(Eigen::Vector3f(1.f, 0.f, 0.f));
		float TurnAngle;

		if (Cross.z() > 0.f)
		{
			TurnAngle = -AcosAngle;
		}
		else
		{
			TurnAngle = AcosAngle;
		}

		Eigen::Vector3f Euler(0.f, 0.f, TurnAngle);
		GetTransform().SetRotation(Euler);

		Transform::ScaleVector(TargetVector, 150.f, 150.f, 150.f);
		MovementComponent->AddImpulse(TargetVector, 1.f);
		LOG_DEBUG("TargetVector : %f, %f, %f", TargetVector.x(), TargetVector.y(), TargetVector.z());

		CurrentCombo++;

		Serializables::ActWithVector PlayerAct;
		PlayerAct.Self = Id;
		PlayerAct.Vector = WorldTransform.GetWorldDegrees();
		PlayerAct.Type = static_cast<ActType>(static_cast<int>(ActType::Teleport_Casting) + CurrentCombo);
		CurrentGameState->BroadcastPacket(PlayerAct);

		if (PlayerAnimNotifies)
		{
			string WeaponString = "";
			switch (CurrentWeaponType)
			{
			case WeaponType::HAND:
				WeaponString = "Grappler";
				break;
			case WeaponType::SWORD:
				WeaponString = "Sword";
				break;
			case WeaponType::BOW:
				WeaponString = "Bow";
				break;
			}
			//Attack_T_H2
			std::string animName = WeaponString + "_T_Attack" + to_string(CurrentCombo);
			LOG_DEBUG(animName.c_str());

			//위의 animName에 해당하는 AnimNotifies를 찾고
			auto iter = PlayerAnimNotifies->find(animName);

			//모든 Notify를 실행한다.
			if (iter != PlayerAnimNotifies->end())
			{
				auto& Notifies = iter->AnimNotifies;
				CurrentActivators.reserve(Notifies.size());
				for (auto& Notify : Notifies)
				{
					if (Notify.first.find("HitCheck") != std::string::npos)
					{
						std::shared_ptr<CheckActivator> HitActivator = std::make_shared<CheckActivator>();
						float hitCheckTime = Notify.second;

						HitActivator->SetTimer(chrono::milliseconds(long long(hitCheckTime * 1000.f)));

						HitActivator->Register([this]()
											   {
								if (God)
									CreateAttackOBB(-1.f, static_cast<TheShift::ActType>(CurrentCombo), 300.f, Eigen::Vector3f(100.f, 100.f, 100.f), Eigen::Vector3f(100.f, 100.f, 100.f), false);
								else
												   CreateAttackOBB(-1.f, ActType::Smash_1);
											   });

						CurrentGameState->AddToTimer(HitActivator);
						CurrentActivators.emplace_back(HitActivator);
					}
					else if (Notify.first.find("TeleportMove") != std::string::npos)
					{
						std::shared_ptr<CheckActivator> TeleportActivator = std::make_shared<CheckActivator>();
						float comboCheckTime = Notify.second;

						TeleportActivator->SetTimer(chrono::milliseconds(long long(comboCheckTime * 1000.f)));
						TeleportActivator->Register([this]()
													{
														TeleportMove();
													});
						CurrentGameState->AddToTimer(TeleportActivator);
						CurrentActivators.emplace_back(TeleportActivator);
					}
				}
			}
		}
	}
	else
	{
		//Init State
		TargetedActors.clear();
		CurrentTargetingActor = nullptr;
		CurrentCombo = 0;
		if(MovementComponent->IsLocked())
			MovementComponent->UnlockInput();
		Teleport = false;
		CanbeDamaged = true;
		Serializables::ActWithVector PlayerAct;
		PlayerAct.Self = Id;
		PlayerAct.Vector = WorldTransform.GetWorldDegrees();
		PlayerAct.Type = ActType::Teleport_End;
		CurrentGameState->BroadcastPacket(PlayerAct);
	}
}

void Character::TeleportMove_Bow()
{
	//단 한명의 타겟을 받는다.
	auto TargetID = BowScreenTargets.front();
	auto Target = CurrentGameState->GetActor(TargetID);
	if (Target == nullptr)
		return;

	SpendTeleportGage(200);
	
	auto MonsterPos = Target->GetTransform().GetTranslation();
	Target->SetCanAct(false);

	auto& TargetTransform = Target->GetTransform();

	Eigen::Vector3f TargetPosition = CalculateSafeTeleportDestination(Target, -TargetTransform.GetLookVector() * 150.f);
	WorldTransform.SetTranslation(TargetPosition);
	SetLastTranslation();


	//그 몬스터의 뒤로 이동시키고 이동시킨다.->이동은 매프레임
	Eigen::Vector3f TargetVector = (MonsterPos - WorldTransform.GetTranslation());

	LOG_DEBUG("TargetVector : %f, %f, %f", TargetVector.x(), TargetVector.y(), TargetVector.z());
	LOG_DEBUG("TargetVector's Size : %f", TargetVector.norm());
	TargetVector.normalize();

	Transform::ScaleVector(TargetVector, 150.f, 150.f, 150.f);
	MovementComponent->AddImpulse(TargetVector, 1.f);
	LOG_DEBUG("TargetVector : %f, %f, %f", TargetVector.x(), TargetVector.y(), TargetVector.z());

	Eigen::Vector3f Dir = TargetVector.normalized();

	// 해당 방향으로 z축 회전
	float Dot = Dir.dot(Eigen::Vector3f(1.f, 0.f, 0.f));
	// 0~180 도 사이의 값 0~1
	float AcosAngle = std::acos(Dot);

	Eigen::Vector3f Cross = Dir.cross(Eigen::Vector3f(1.f, 0.f, 0.f));
	float TurnAngle;

	if (Cross.z() > 0.f)
	{
		TurnAngle = -AcosAngle;
	}
	else
	{
		TurnAngle = AcosAngle;
	}

	Eigen::Vector3f Euler(0.f, 0.f, TurnAngle);
	GetTransform().SetRotation(Euler);


	CurrentActivators.clear();

	//Attack_T_H2
	std::string animName ="Archer_Teleport";
	LOG_DEBUG(animName.c_str());

	//위의 animName에 해당하는 AnimNotifies를 찾고
	auto iter = PlayerAnimNotifies->find(animName);

	//모든 Notify를 실행한다.
	if (iter != PlayerAnimNotifies->end())
	{
		auto& Notifies = iter->AnimNotifies;
		CurrentActivators.reserve(Notifies.size());

		for (auto& Notify : Notifies)
		{
			if (Notify.first.find("HitCheck") != std::string::npos)
			{
				std::shared_ptr<CheckActivator> HitActivator = std::make_shared<CheckActivator>();
				float hitCheckTime = Notify.second;

				HitActivator->SetTimer(chrono::milliseconds(long long(hitCheckTime * 1000.f)));

				HitActivator->Register([this, Target, TargetID]()
					{
						if (!this->CurrentCombo)
						{
							Target->ApplyDamage(0.f, ActType::BaseAttack_1, GetId());
							this->CurrentCombo++;

							Serializables::ActWithVector PlayerAct;
							PlayerAct.Self = Id;
							PlayerAct.Vector = WorldTransform.GetWorldDegrees();
							PlayerAct.Type = ActType::BaseAttack_1;
							CurrentGameState->BroadcastPacket(PlayerAct);
						}
						else
						{
							if(Target->GetType()==ActorType::WhiteWareWolf || Target->GetType()==ActorType::FatMonster || Target->GetType()==ActorType::SwordMonster)
								Target->ApplyDamage(150.f, ActType::BaseAttack_2, GetId(), false);
							else
								Target->ApplyDamage(0.f, ActType::BaseAttack_2, GetId(), true);
							CurrentGameState->DestroyActor(TargetID);

							Serializables::ActWithVector PlayerAct;
							PlayerAct.Self = Id;
							PlayerAct.Vector = WorldTransform.GetWorldDegrees();
							PlayerAct.Type = ActType::BaseAttack_2;
							CurrentGameState->BroadcastPacket(PlayerAct);
							if (MovementComponent->IsLocked())
								MovementComponent->UnlockInput();

							Teleport = false;
							CanbeDamaged = true;
							TeleportAttacking = false;
						}
					});

				CurrentGameState->AddToTimer(HitActivator);
				CurrentActivators.emplace_back(HitActivator);
			}

		}
	}
	BowScreenTargets.clear();
}

void Character::RotateByRecentInput(bool IsApplyAttackInput, float DegreeDiff)
{
	//공격체크전까지의 Input이 누적된 방향을 알아야함.
	//그 정보는 InputDirection에 있음
	//최근 인풋이 들어왓을때의 카메라 방향에 따른 다음 방향을 결정해야 한다.
	//마지막으로 Pressing도 반영해준다.
	//만약 인자가 true이면 누적된 인풋도 반영
	Eigen::Vector3f InputDirection = NonAccumulatedInputDirection;
	//if(!IsApplyAttackInput)
	if (IsApplyAttackInput)
		InputDirection += MovementComponent->GetInputDirection();
	InputDirection.normalize();
	//LOG_DEBUG("--------At RotateBy RecentInput: %f, %f------------", InputDirection.x(), InputDirection.y());

	//Forward 벡터(1,0,0)를 카메라 회전행렬에 곱해서 최종 목표 방향벡터를 찾는다.
	//LOG_DEBUG("Cam Rotation Z : %f", RecentInput.CamRotation.Z);

	Eigen::Vector3f GoalVector = Transform::RotateVector(1.f, 0.f, 0.f, 0.f, 0.f, RecentInput.CamRotation.Z);
	//LOG_DEBUG("Goal Vector: %f, %f, %f", GoalVector.x(), GoalVector.y(), GoalVector.z());
	Eigen::Vector3f ActorForwardVector = WorldTransform.GetLookVector();
	ActorForwardVector.z() = 0.f;
	ActorForwardVector.normalize();
	//LOG_DEBUG("Actor Forward Vector: %f, %f, %f", ActorForwardVector.x(), ActorForwardVector.y(), ActorForwardVector.z());

	Eigen::Vector3f ActorRightVector = WorldTransform.GetRightVector();
	ActorRightVector.z() = 0.f;
	ActorRightVector.normalize();

	if (InputDirection.x() > 0.f)
	{
		//LOG_DEBUG("x PLUS APPLY");
		if (InputDirection.y() > 0.f)
		{
			GoalVector = Transform::RotateVector(GoalVector, 0.f, 0.f, 45.f);
			//LOG_DEBUG("Y PLUS APPLY");
		}
		else if (InputDirection.y() < 0.f)
		{
			GoalVector = Transform::RotateVector(GoalVector, 0.f, 0.f, -45.f);
			//LOG_DEBUG("Y MINUS APPLY");
		}
		else
			GoalVector = GoalVector;
	}
	else if (InputDirection.x() < 0.f)
	{
		//LOG_DEBUG("x MINUS APPLY");
		if (InputDirection.y() > 0.f)
		{
			GoalVector = Transform::RotateVector(GoalVector, 0.f, 0.f, 135.f);
			//LOG_DEBUG("Y PLUS APPLY");
		}
		else if (InputDirection.y() < 0.f)
		{
			GoalVector = Transform::RotateVector(GoalVector, 0.f, 0.f, -135.f);
			//LOG_DEBUG("Y MINUS APPLY");
		}
		else
			GoalVector = -GoalVector;
	}
	else
	{
		//LOG_DEBUG("x ZERO APPLY");
		if (InputDirection.y() > 0.f)
		{
			GoalVector = Transform::RotateVector(GoalVector, 0.f, 0.f, 90.f);
			//LOG_DEBUG("Y PLUS APPLY");
		}
		else if (InputDirection.y() < 0.f)
		{
			GoalVector = Transform::RotateVector(GoalVector, 0.f, 0.f, -90.f);
			//LOG_DEBUG("Y MINUS APPLY");
		}
		else
		{
			GoalVector = ActorForwardVector;
			//LOG_DEBUG("NOINPUT!!!");
		}
	}
	//LOG_DEBUG("Rotated Goal Vector: %f, %f, %f", GoalVector.x(), GoalVector.y(), GoalVector.z());
	float RadDiff = acosf(GoalVector.dot(ActorForwardVector));
	if (_isnanf(GoalVector.dot(ActorForwardVector)))
	{
		//LOG_DEBUG("DOT NAN!!!");
	}
	if (_isnanf(RadDiff))
	{
		LOG_DEBUG("Nan!!");
		RadDiff = 0.f;
	}

	if (GoalVector.dot(ActorRightVector) > 0.f)
		RadDiff *= -1.f;

	WorldTransform.Rotate(Eigen::Vector3f(0.f, 0.f, -RadDiff + (DegreeDiff * M_PI / 180.f)));
	//LOG_DEBUG("Final Rotation : %f", RadDiff * 180.f / M_PI);
}

// 아이템을 장착한다.
void Character::EquipItem(std::shared_ptr<ItemComponent> Item)
{
	switch (Item->ItemCategory) 
	{
	case ItemType::Weapon:
		break;

	case ItemType::Armor:
		break;

	default:
		break;
	}
}

// 아이템 장착을 해제한다.
void Character::UnequipItem(std::shared_ptr<ItemComponent> Item)
{
	switch (Item->ItemCategory) 
	{
	case ItemType::Weapon:
		break;

	case ItemType::Armor:
		break;

	default:
		break;
	}
}

void Character::GodMode()
{
	if (God) 
	{
		God = false;
		TeleportGageConsumption = 200;
	}
	else 
	{
		God = true;
		Hp = MaxHp;
		CurrentStamina = MaxStamina;
		CurrentTeleportGage = MaxTeleportGage;
		TeleportGageConsumption = 0;
	}
}

void Character::CalculateInputDirection(Serializables::Input inputValue)
{
	switch (inputValue.InputValue)
	{
	case InputType::Left_Pressed:
		LOG_DEBUG("Non_Left_PRESSED");
		if (NonAccumulatedInputDirection.y() > -FLT_EPSILON)
			NonAccumulatedInputDirection.y() = -1.f;
		break;

	case InputType::Left_Released:
		LOG_DEBUG("Non_Left_RELEASED");
		if (NonAccumulatedInputDirection.y() < -FLT_EPSILON)
			NonAccumulatedInputDirection.y() = 0.f;
		break;

	case InputType::Right_Pressed:
		LOG_DEBUG("Non_Right_PRESSED");
		if (NonAccumulatedInputDirection.y() < FLT_EPSILON)
			NonAccumulatedInputDirection.y() = 1.f;

		break;

	case InputType::Right_Released:
		LOG_DEBUG("Non_Right_RELEASED");
		if (NonAccumulatedInputDirection.y() > FLT_EPSILON)
			NonAccumulatedInputDirection.y() = 0.f;
		break;

	case InputType::Forward_Pressed:
		LOG_DEBUG("Non_Forward_PRESSED");
		if (NonAccumulatedInputDirection.x() < FLT_EPSILON)
			NonAccumulatedInputDirection.x() = 1.f;

		break;

	case InputType::Forward_Released:
		LOG_DEBUG("Non_Forward_RLEASED");
		if (NonAccumulatedInputDirection.x() > FLT_EPSILON)
			NonAccumulatedInputDirection.x() = 0.f;
		break;

	case InputType::Back_Pressed:
		LOG_DEBUG("Non_BACK_PRESSED");
		if (NonAccumulatedInputDirection.x() > -FLT_EPSILON)
			NonAccumulatedInputDirection.x() = -1.f;

		break;

	case InputType::Back_Released:
		LOG_DEBUG("Non_BACK_RELEASED");
		if (NonAccumulatedInputDirection.x() < -FLT_EPSILON)
			NonAccumulatedInputDirection.x() = 0.f;
		break;

	default:
		break;
	}
}
void Character::FireArrow()
{
	//화살 맞히는 구문-> 클라에서 에임에 제일 가까운 친구들 받아와서(클라한테받음) 피깎기(이건 서버에서)
	//이 함수는 노티파이를 등록하는 함수임(즉, 바로 발동하지 않음)
	auto iter = PlayerAnimNotifies->find("Aim_Recoil");

	if (iter != PlayerAnimNotifies->end())
	{
		auto& Notifies = iter->AnimNotifies;
		CurrentActivators.reserve(Notifies.size());
		for (auto& Notify : Notifies)
		{
			if (Notify.first.find("HitCheck") != std::string::npos)
			{
				std::shared_ptr<CheckActivator> FireArrowActivator = std::make_shared<CheckActivator>();
				float FireArrowTime = Notify.second;

				FireArrowActivator->SetTimer(chrono::milliseconds(long long(FireArrowTime * 1000.f)));
				FireArrowActivator->Register([&]()
					{
						//ID받아온애들 피깎는구문들어가야함.	
						for (auto ID : BowScreenTargets)
						{
							auto Target = CurrentGameState->GetActor(ID).get();
							if (Target)
							{
								if (CurrentCombo == 1)
								{
									if (Target->ApplyDamage(350.f, ActType::BaseAttack_1, GetId(),false,false))
										CurrentGameState->DestroyActor(ID);
								}
								else
								{
									if (Target->ApplyDamage(350.f, ActType::BaseAttack_1, GetId(), false, true))
										CurrentGameState->DestroyActor(ID);
								}
							}
						}

						LOG_DEBUG("Fire Arrow!!");
					});

				CurrentGameState->AddToTimer(FireArrowActivator);
				CurrentActivators.emplace_back(FireArrowActivator);
			}
		}
	}
	BowScreenTargets.clear();

	/////클라에서 이 메시지를 받으면(노티파이가 발동하면) 해당 애들 피격 모션 시키면서 피격 이펙트 출력까지!
	
}
void Character::InitAllState()
{
	ClearActivators();
	IsAttacking = false;
	CurrentCombo = 0;
	Aiming = false;
	ReleasedAim = false;
	CanNextCombo = false;
	IsComboInputOn = false;
	Dash = false;
}
void Character::LockInput()
{
	MovementComponent->LockInput();
}
void Character::UnlockInput()
{
	MovementComponent->UnlockInput();
}
}
