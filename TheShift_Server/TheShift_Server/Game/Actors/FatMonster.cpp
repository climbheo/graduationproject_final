﻿#include "FatMonster.h"
#include "Game/System/GameState.h"
#include "Game/Actors/Trigger.h"
#ifdef TSSERVER
#include "Game/System/TimerCallee.h"
#endif

using namespace std::chrono_literals;
using namespace std;
const std::set<AnimNotifyInfo>* FatMonster::FatMonsterAnimNotifies = nullptr;

TheShift::FatMonster::FatMonster(UID Id) : Monster(Id)
{
	Hp = 2500.f;
	Type = ActorType::FatMonster;
#ifdef TSSERVER
	SetRecognizeRange(500.f);
	SetContactRange(400.f);

	//Get Pointer of Player AnimData
	if (!FatMonsterAnimNotifies)
		FatMonsterAnimNotifies = AnimNotifyData::GetInstance().GetAnimNotifyInfo("FatMonster");

	pMonsterAnimNotify = FatMonsterAnimNotifies;

	//기본적으로 슈퍼아머상태
	IsHitAnimPlay = false;

	//플레이어보다 살짝 빠르게
	Stat.ForwardRunSpeed = 450.f;

#endif
}

void TheShift::FatMonster::OnPlayerContact(std::shared_ptr<Actor> Player)
{
	//처음으로 공격 가능한 거리가 될 시-> 보스 몬스터의 상태를 랜덤으로 결정한다.
	DecidePattern();
	
}

void TheShift::FatMonster::OnRecognizePlayer(std::shared_ptr<Actor> Player)
{
	Monster::OnRecognizePlayer(Player);
}

float TheShift::FatMonster::GetHitResetTime(bool IsBigHit)
{
	if (FatMonsterAnimNotifies)
	{
		auto iter = FatMonsterAnimNotifies->find("FatMonster_Hit");

		if (iter != FatMonsterAnimNotifies->end())
		{
			auto& Notifies = iter->AnimNotifies;

			for (auto& Notify : Notifies)
			{
				if (Notify.first.find("HitReset") != std::string::npos)
					return Notify.second;
			}
		}
	}

	return 0.0f;
}


//이 함수는 각 상태들의 마지막에 불려야한다.-> 특정 상태가 끝나면 다시 패턴을 결정하는 함수.
void TheShift::FatMonster::DecidePattern()
{
	//각 상태 진입할때 CanChase = true;해주기

	//각 공격이던 뭐던 상태던 끝나면 사거리 안에 플렝이어 있는지 결정
	if (ChasingActor)
	{
		if (GetDistanceToPlayer() > ContactRange)
		{
			LOG_DEBUG("Recahse Player");
			InitState();
			//StateStarted = false;
			Stat.ForwardRunSpeed = 375.f;
			ChasingActor = nullptr;
			return;
		}
	}

	//아무 상태도 아니라면 일단 포스있게 락온어택 박고 시작하자.
	if (!StateStarted)
	{
		CurrentCombo = 0;
		MaxCombo = 2;
		LockonAttacking = true;
		/*
		ComboAttacking = true;
		CurrentCombo = 3;
		MaxCombo = 3;
	*/	return;
	}
	//특정 상태가 끝나고 다시 상태를 결정하러 왔다면 일단 콤보를 0으로 만들고 StateStarted를 false로 갱신한다.
	else
	{
		Stat.ForwardRunSpeed = 450.f;
		CurrentCombo = 0;
		StateStarted = false;
	}
	
	InitState();

	int randomPattern = rand() % 4;
	switch (randomPattern)
	{
	case 0:
		LockonAttacking = true;
		CurrentCombo = rand() % 2 + 1;
		break;

	case 1:
		ComboAttacking = true;
		CurrentCombo = rand() % 2 + 2;
		break;

	case 2:
		Evading = true;
		CurrentCombo = rand() % 2 + 1;
		break;

	case 3:
		Curling = true;
		CurrentCombo = rand() % 2 + 1;
		break;
	}

	////락온 어택이 끝났다면
	//if (LockonAttacking)
	//{
	//	LockonAttacking = true;
	//	CurrentCombo = 1;
	//}
	////콤보 어택이 끝났다면
	//else if (ComboAttacking)
	//{

	//}
	////회피가 끝났다면
	//else if (Evading)
	//{
	//	LockonAttacking = true;
	//	CurrentCombo = 2;



	//	Evading = false;
	//}
	////돌아서 공격이 끝났다면
	//else if (TurnAttacking)
	//{

	//}
	////꼬라보기가 끝났다면
	//else if (Curling)
	//{
	//	CurrentCombo = rand() % 2 + 1;
	//}
}
