﻿#pragma once

#ifndef TS_CHARACTER_H
#define TS_CHARACTER_H

#include "Game/Actors/Actor.h"

#ifdef TSSERVER
#include <set>
#include <stdarg.h>  
using namespace std;
#endif

#ifdef TSCLIENT
#include "../../Actors/TSWeapon.h"
#endif

namespace TheShift
{
class Character : public Actor
{
public:
	Character(UID id);
	void OnInput(Serializables::Input inputValue) override;
	virtual void OnHit(ActType AttackType) override;
	virtual void Update(float DeltaTime);

	void InitAttackState(bool SendPacket = true);

	//눌려잇는 인풋을 반영하고 공격상태들을 Reset하는 함수.
	void AttackEndComboState();

	//공격중에 들어온 인풋값에 기반해 다음 공격으로넘어갈때 액터를 회전시키는 함수
	void RotateByRecentInput(bool IsApplyAttackInput = true, float DegreeDiff = 0.f);

	// 새로운 Type의 무기로 바꾼다.
#ifdef TSSERVER
	virtual void EquipItem(std::shared_ptr<ItemComponent> Item) override;
	virtual void UnequipItem(std::shared_ptr<ItemComponent> Item) override;
	void GodMode() override;
	
	void SwapWeapon(WeaponType Type) override;
	void SwapArmor(ArmorItemType Type);
	Eigen::Vector3f CalculateSafeTeleportDestination(std::shared_ptr<Actor> TargetActor, Eigen::Vector3f OffsetPositionAfterTeleport = Eigen::Vector3f(50.f, 50.f, 0.f));
	WeaponType GetCurrentWeaponType() const;
	ArmorItemType GetCurrentArmorType() const;
	void SetBowScreenTargets(std::vector<UID>* TargetIds);

	virtual float GetHitResetTime(bool IsBigHit) override;
	int GetCurrentChasingMonstersCount();
	void IncreaseChasingMonsterCount();
	void DecreaseChasingMonsterCount();

	// Teleport gage
	int GetCurrentTeleportGage() const;
	void SetCurrentTeleportGage(int NewValue);
	bool SpendTeleportGage(int Consumption);
	bool DoesHaveEnoughTeleportGage(int Consumption);

	// Stamina
	int GetCurrentStamina() const;
	void SetCurrentStamina(int NewValue);
	bool SpendStamina(int Consumption);
	bool DoesHaveEnoughStamina(int Consumption);
	
#endif
	uint32_t GetLastProcessedInputId() const;
	float GetMaxHp() const;
	int GetMaxStamina() const;
	int GetMaxTeleportGage() const;

private:
	void AttackStartComboState(bool IsSmash = false);
	void AttackComboCheck(bool IsSmash = false);
	void RotateBeforState();

	//활 차지 함수
	void ChargeArrow();

	//텔레포트 움직임 컨트롤 함수
	void TeleportMove();

	//활 텔레포트 움직임 컨트롤 함수.
	void TeleportMove_Bow();

	//누적되지 않는 현상태의 InputDirection을 계산하는 함수
	void CalculateInputDirection(Serializables::Input inputValue);

	//화살 발사하는함수
	void FireArrow();

	//모든 상태를 리셋하는 함수
	void InitAllState();

	//Input Lock
	void LockInput();
	void UnlockInput();

private:
	//Current Weapon Type
	WeaponType CurrentWeaponType = WeaponType::HAND;

	// Current Armor Type
	ArmorItemType CurrentArmorType = ArmorItemType::None;

	//Current Combo
	int32_t CurrentCombo = 0;

	//Is Attack?		
	bool IsAttacking = false;
	//Max Combo
	int32_t MaxCombo = 3;

	//CanNextCombo? -> CurrentCombo < MaxCombo?
	bool CanNextCombo = false;

	//Is Input incomed while Attacking? 
	bool IsComboInputOn = false;

	//Is Dashing?
	bool Dash = false;

	//Is Teleporting?
	bool Teleport = false;

	// Client에선 GameState에서 굳이 Teleport랑 Stamina건들일 일 없으니 Server에서만 빌드
#ifdef TSSERVER
	// Teleport gage
	float MaxHp = 2000;
	int MaxTeleportGage = 2000;
	int CurrentTeleportGage = 2000;
	// Teleport하는데 사용되는 게이지
	int TeleportGageConsumption = 200;

	//TODO: 현재 임시로 2000으로 크게 올려놨음. 적정선을 찾아 낮춰야 함.
	// Stamina
	int MaxStamina = 2000;
	int CurrentStamina = 2000;
#endif

	//Is TeleportAttacking?
	bool TeleportAttacking = false;

	//Is Aiming?
	bool Aiming = false;

	//Is Released Aiming
	bool ReleasedAim = false;

	//Dash Force
	float DashForce = 450.f;

	//Is Shift pressing
	bool ShiftPressing = false;

	//////

	//Recent InputValue
	Serializables::Input RecentInput;
	//누적되지 않는 키의 상태를 반영 -> MovementComponent의 InputDirection은 때론 누적되곤함(공격중일떄)
	Eigen::Vector3f NonAccumulatedInputDirection;

	uint32_t LastProcessedInputId = 0;

private:
	//Player AnimData
	static const std::set<AnimNotifyInfo>* PlayerAnimNotifies;
	std::list<std::shared_ptr<Actor>> TargetedActors;
	std::shared_ptr<Actor> CurrentTargetingActor;
	std::vector<UID> BowScreenTargets;

	int CurrentChasingMonstersCount = 0;
};
}
#endif

