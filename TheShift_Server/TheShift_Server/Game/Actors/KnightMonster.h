﻿#pragma once

#include "Game/Actors/Monster.h"


namespace TheShift
{
class KnightMonster : public Monster
{
public:
	KnightMonster(UID Id);

#ifdef TSSERVER
	void OnPlayerContact(std::shared_ptr<Actor> Player) override;
	
	virtual float GetHitResetTime(bool IsBigHit = false) override;
#endif

private:
#ifdef TSSERVER
	bool AttackCoolDown = true;
	int  PrevAttackNum = 1;

	static const std::set<AnimNotifyInfo>* KnightMonsterAnimNotifies;
#endif
};
}

