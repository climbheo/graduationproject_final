#include "server.h"
#include "Game/System/MapData.h"
#include "Game/System/AnimNotifyData.h"
#include "Protocol/Log.h"


Server TheShiftServer(54000);

void CALLBACK CompletionRoutine(DWORD Error, DWORD TransferredBytes, LPWSAOVERLAPPED LpOverlapped, DWORD Flags)
{
	int Retval = 0;

	// 클라이언트 정보 얻기
	SocketInfo* SockInfo = reinterpret_cast<SocketInfo*>(LpOverlapped);
	sockaddr_in ClientAddr;
	int AddrLen = sizeof(ClientAddr);
	getpeername(SockInfo->Socket, reinterpret_cast<sockaddr*>(&ClientAddr), &AddrLen);

	if(Error != NO_ERROR)
	{
		LOG_ERROR("CompletionRoutine error!! Error: %d", WSAGetLastError());
		closesocket(SockInfo->Socket);
		TheShiftServer.DisconnectPlayer(SockInfo->PlayerIndex);
	}
	//spdlog::trace("CompletionRoutine completed on Player #{0}, Socket #{1}", SockInfo->PlayerIndex, SockInfo->Socket);

	// Error check
	if(Error != NO_ERROR)
		LOG_ERROR("CompletionRoutine: Io Error %d", WSAGetLastError());
	//spdlog::error("CompletionRoutine: Io Error {0}", WSAGetLastError());

	if(TransferredBytes == 0)
	{
		// TODO: 클라이언트 접속 종료
		closesocket(SockInfo->Socket);
		char StrAddr[INET_ADDRSTRLEN];
		inet_ntop(AF_INET, &ClientAddr.sin_addr, StrAddr, INET_ADDRSTRLEN);
		LOG_INFO("Client disconnect. IP: %s, Port: %d", StrAddr, ntohs(ClientAddr.sin_port));
		//spdlog::info("Client disconnect. IP: {0}, Port: {1}", StrAddr, ntohs(ClientAddr.sin_port));
		// Player 해제
		TheShiftServer.DisconnectPlayer(SockInfo->PlayerIndex);
		return;
	}

	// Io처리
	TheShiftServer.ProcessIo(SockInfo->PlayerIndex, SockInfo, TransferredBytes);
}

Server::Server() : Port(DEFAULT_PORT)
{
	InitServer();
}

//Server::Server(uint32_t AddrValue, uint16_t PortValue) : Address(AddrValue), Port(PortValue)
//{
//    InitServer();
//}

Server::Server(uint16_t PortValue) : Port(PortValue)
{
	SetAddrPort(Port);
	InitServer();
}

Server::~Server()
{
	Join();

	Players.clear();
	PlayerPool.clear();
}

void Server::Run()
{
	GameMngr.Run();
	DoAccept();
}

void Server::DoAccept()
{
	NewPlayerEvent = CreateEvent(nullptr, FALSE, FALSE, nullptr);

	if(NewPlayerEvent == nullptr)
	{
		LOG_ERROR("DoAccept Event was NULL");
		//spdlog::error("DoAccept Event was NULL");
		return;
	}

	// Thread 생성
	ReceiverThread = std::thread(&Server::ReceiverThreadFunc, this);

	while(true)
	{
		sockaddr_in clientAddr;
		char cstrAddr[INET_ADDRSTRLEN];
		int addrSize = sizeof(clientAddr);
		SOCKET clientSocket = INVALID_SOCKET;

		// Accept
		ClientSocket = WSAAccept(ListenSocket, reinterpret_cast<sockaddr*>(&clientAddr), &addrSize, nullptr, 0);
		int nValue = 1;
		if(setsockopt(ClientSocket, IPPROTO_TCP, TCP_NODELAY, (const char*)&nValue, sizeof(nValue)) != NO_ERROR)
		{
			LOG_ERROR("Can't set socket option with TCP_NODELAY. Error: %d", WSAGetLastError());
		}

		if(ClientSocket == INVALID_SOCKET)
		{
			LOG_ERROR("DoAccept(ClientSocket was INVALID_SOCKET");
			//spdlog::error("DoAccept(ClientSocket was INVALID_SOCKET");
			continue;
		}

		if(!SetEvent(NewPlayerEvent))
		{
			LOG_ERROR("Can't signal Receiver thread");
			//spdlog::error("Can't signal Receiver thread");
			break;
		}
	}
}


/// <summary>
/// Network IO를 담당.
/// 새로운 Client가 Accept되었을 때, Send요청이 있을  aleratble wait상태에서 깨어난다.
/// Alertable wait상태에서는 완료된 IO에 대한 처리를 진행한다.
/// </summary>
void Server::ReceiverThreadFunc()
{
	// TODO: overflow 일어나기 쉬움. 수정해야함
	ClientId PlayerIndex = 0;

	while(true)
	{
		// Alertable wait
		ResetEvent(NewPlayerEvent);
		DWORD Result = WaitForSingleObjectEx(NewPlayerEvent, INFINITE, TRUE);
		switch(Result)
		{
			////////////////
			///// Recv /////
			////////////////
		case WAIT_OBJECT_0:
		{
			// 접속한 클라이언트 정보 출력
			sockaddr_in ClientAddr;
			int AddrLen = sizeof(ClientAddr);
			getpeername(ClientSocket, reinterpret_cast<sockaddr*>(&ClientAddr), &AddrLen);
			char StrAddr[INET_ADDRSTRLEN];
			inet_ntop(AF_INET, &ClientAddr, StrAddr, INET_ADDRSTRLEN);
			LOG_INFO("클라이언트 접속: IP: %s, Port: %d", StrAddr, ntohs(ClientAddr.sin_port));
			//spdlog::info("클라이언트 접속: IP: {0}, Port: {1}", StrAddr, ntohs(ClientAddr.sin_port));

			// Player 할당
			Player* NewPlayer = PlayerPool.alloc();

			NewPlayer->GetSocketInfoPtr()->Io = IoType::Read;
			NewPlayer->GetSocketInfoPtr()->Socket = ClientSocket;

			if(NewPlayer == nullptr)
			{
				LOG_ERROR("Can't allocate SocketInfo: Memory is full.");
				//spdlog::error("Can't allocate SocketInfo: Memory is full.");
				continue;
			}

			PlayerIndex++;
			NewPlayer->GetSocketInfoPtr()->PlayerIndex = PlayerIndex;
			// TODO: 동기화 괜찮나?
			// Players에 저장
			ClientIdTable::accessor Accessor;
			ClientIdTable::value_type NewPlayerPair(std::make_pair(PlayerIndex, NewPlayer));
			if(!Players.insert(Accessor, NewPlayerPair))
			{
				continue;
			}
			Serializables::Connected ConnectedData;
			ConnectedData.MyId = PlayerIndex;
			Accessor.release();

			Network::Send(PlayerIndex, ConnectedData);

			int RetVal = Recv(PlayerIndex);

			if(RetVal == SOCKET_ERROR)
			{
				int ErrorCode = WSAGetLastError();
				if(ErrorCode != WSA_IO_PENDING)
				{
					LOG_ERROR("WSASend(): ErrorCode wsa not WSA_IO_PENDING: %d", ErrorCode);
					//spdlog::error("WSASend(): ErrorCode wsa not WSA_IO_PENDING: {0}", ErrorCode);
					return;
				}
			}
			break;
		}

		case STATUS_USER_APC:
			break;

		default:
			LOG_ERROR("Error in WaitForMultipleObjectEx(), %d", Result);
			//spdlog::error("Error in WaitForMultipleObjectEx(), {0}", Result);
			break;
		}
	}
}

void Server::SetAddrPort(uint16_t PortValue)
{
	//inet_pton(AF_INET, StrAddrValue, &Address);
	//Address = ntohl(Address);
	Port = PortValue;
}

void Server::InitServer()
{
	int ErrorCode = NO_ERROR;
	ErrorCode = InitWinsockAndListen();
	if(ErrorCode != NO_ERROR)
	{
		LOG_ERROR("Winsock초기화 실패! ErrorCode: %d", ErrorCode);
		//spdlog::error("Winsock초기화 실패! ErrorCode: {0}", ErrorCode);
	}
	else
	{
		LOG_INFO("Winsock초기화 완료!");
		//spdlog::info("Winsock초기화 완료!");
	}

	GameMngr.SetGameThreads(std::thread::hardware_concurrency());

	//Load Data;
	MapData::GetInstance();
	AnimNotifyData::GetInstance();
}

int Server::InitWinsockAndListen()
{
	WSAData WsaData;
	if(WSAStartup(MAKEWORD(2, 2), &WsaData) != NO_ERROR)
	{
		return WSAGetLastError();
	}

	ListenSocket = WSASocketW(AF_INET, SOCK_STREAM, IPPROTO_TCP, nullptr, 0, WSA_FLAG_OVERLAPPED);
	ZeroMemory(&SockAddr, sizeof(SockAddr));
	SockAddr.sin_addr.S_un.S_addr = htonl(INADDR_ANY);
	SockAddr.sin_family = AF_INET;
	SockAddr.sin_port = htons(Port);

	// bind
	if(bind(ListenSocket, reinterpret_cast<sockaddr*>(&SockAddr), sizeof(SockAddr)) == SOCKET_ERROR)
	{
		return WSAGetLastError();
	}
	// listen
	if(listen(ListenSocket, SOMAXCONN) == SOCKET_ERROR)
	{
		return WSAGetLastError();
	}

	return NO_ERROR;
}

int Server::Recv(ClientId Id)
{
	int ErrorCode = NO_ERROR;
	DWORD Flag = 0;

	ClientIdTable::const_accessor Accessor;
	if(!Players.find(Accessor, Id))
	{
		//spdlog::warn("Can't find Player #{0}", Id);
		LOG_WARNING("Can't find Player #%d", Id);
		return -1;
	}
	Player* User = Accessor->second;
	SocketInfo* SockInfo = User->GetSocketInfoPtr();
	SockInfo->Io = IoType::Read;

	if(WSARecv(SockInfo->Socket, &SockInfo->WsaBuffer, 1, &SockInfo->RecvBytes, &Flag,
			   reinterpret_cast<OVERLAPPED*>(SockInfo), CompletionRoutine) == SOCKET_ERROR)
	{
		ErrorCode = WSAGetLastError();
		if(ErrorCode != ERROR_IO_PENDING)
		{
			Accessor.release();
			LOG_ERROR("WSARecv Error! Not IO_PENDING: {0}", ErrorCode);
			//spdlog::error("WSARecv Error! Not IO_PENDING: {0}", ErrorCode);
			return ErrorCode;
		}
		ErrorCode = NO_ERROR;
	}
	Accessor.release();
	return ErrorCode;
}

void Server::DisconnectPlayer(ClientId PlayerIndex)
{
	// TODO: Game thread한테도 알려야 함
	// Message로 전달해주면 될듯

	ClientIdTable::accessor Accessor;
	if(!Players.find(Accessor, PlayerIndex))
	{
		return;
	}
	Player* ToBeDisconnected = Accessor->second;
	shutdown(ToBeDisconnected->GetSocketInfoPtr()->Socket, SD_SEND);
	closesocket(ToBeDisconnected->GetSocketInfoPtr()->Socket);
	PlayerPool.dealloc(ToBeDisconnected);
	Accessor.release();
	GameMngr.ClearClient(PlayerIndex);
	Players.erase(PlayerIndex);
	LOG_INFO("Player #%d disconnected.", PlayerIndex);
	//spdlog::info("Player #{0} disconnected.", PlayerIndex);
}

void Server::ProcessIo(ClientId Id, SocketInfo* Info, uint32_t BytesTransferred)
{
	switch(Info->Io)
	{
	case IoType::Read:
		//spdlog::debug("ProcessIo: Read!! {0} bytes transferred.", BytesTransferred);
		AssembleAndProcessPacket(Id, BytesTransferred);
		Recv(Id);
		break;

	case IoType::Write:
		//spdlog::debug("Packet of Size: {0} sent to Client Id #{1}", BytesTransferred, Id);
		// 메모리 정리
		if(Info != nullptr)
			MemoryAllocator::SocketInfoAllocator.dealloc(Info);
		break;

	case IoType::None:
		LOG_WARNING("Unhandled IoType from Client Id #%d", Id);
		//spdlog::warn("Unhandled IoType from Client Id #{0}", Id);
		break;

	default:
		LOG_WARNING("Unhandled IoType from Client Id #%d", Id);
		//spdlog::warn("Unhandled IoType from Client Id #{0}", Id);
		break;
	}
}

void Server::AssembleAndProcessPacket(ClientId Id, uint32_t BytesTransferred)
{
	// Recv 처리 도중에 이 Player에 접근 할 경우는 없기 때문에 바로 release함
	ClientIdTable::accessor Accessor;
	if(!Players.find(Accessor, Id))
	{
		LOG_WARNING("Can't find Player #%d", Id);
		//spdlog::warn("Can't find Player #{0}", Id);
		return;
	}
	Player* User = Accessor->second;
	uint32_t Rest = BytesTransferred;
	char* RecvBufferPtr = User->GetRecvBuffer();
	char* PacketBuffer = User->GetPacketBuffer();
	uint32_t PacketSize = 0;
	uint32_t PrevSize = User->GetPrevSize();
	Accessor.release();

	// 전에 받아놓은 패킷의 제일 앞 사이즈를 읽는다.
	if(PrevSize >= sizeof(uint32_t))
	{
		PacketSize = *reinterpret_cast<uint32_t*>(PacketBuffer);
	}
	// 데이터의 길이도 안왔을 수 있음.
	else
	{
		uint32_t AdditionalDataSizeLength = sizeof(uint32_t) - PrevSize;

		// 새로 온 데이터로도 덜 온 데이터 길이를 만들 수 없다면 온 만큼 복사해주고 패킷을 만들 수 없으니 return한다.
		if(Rest < AdditionalDataSizeLength)
		{
			memcpy(PacketBuffer + PrevSize, RecvBufferPtr, Rest);
			PrevSize += Rest;
			User->SetPrevSize(PrevSize);
			return;
		}

		// PrevSize부터 4바이트 까지만 복사해준다.
		memcpy(&PacketBuffer[PrevSize], RecvBufferPtr, AdditionalDataSizeLength);
		// 데이터 길이를 복사해준 만큼 ptr을 옮겨준다.
		RecvBufferPtr += AdditionalDataSizeLength;
		PrevSize += AdditionalDataSizeLength;
		Rest -= AdditionalDataSizeLength;
		PacketSize = *reinterpret_cast<uint32_t*>(PacketBuffer);
	}

	// 패킷을 만든다.
	while(Rest > 0)
	{
		if(PacketSize == 0)
		{
			// 패킷 처리하고 남아있는 데이터 처리할 때 데이터 사이즈를 읽을 수 있으면 계속 처리.
			if(Rest >= sizeof(uint32_t))
			{
				PacketSize = *reinterpret_cast<uint32_t*>(RecvBufferPtr);
			}
			// 데이터 사이즈를 읽을 수 없으면 복사해주고 break;
			else
			{
				memcpy(PacketBuffer + PrevSize, RecvBufferPtr, Rest);
				PrevSize = Rest;
				break;
			}
		}
		uint32_t Required = PacketSize - PrevSize;
		if(Rest >= Required)
		{
			// 패킷 생성 가능.
			memcpy(PacketBuffer + PrevSize, RecvBufferPtr, Required);

			///////////////
			// 패킷 처리 //
			// ////////////
			uint32_t DataSize = *(reinterpret_cast<uint32_t*>(User->GetPacketBuffer()));
			Serializables::Type MsgType =
				*reinterpret_cast<Serializables::Type*>(&User->GetPacketBuffer()[sizeof(DataSize)]);
			char* DataBuffer = User->GetPacketBuffer() + sizeof(DataSize) + sizeof(Serializables::Type);

			GameMngr.ProcessPacketData(Id, DataBuffer, MsgType);

			// 접속 종료
			if(MsgType == Serializables::Type::Disconnect)
			{
				DisconnectPlayer(Id);
			}
			////////////////////////////////////////////////////////////////////////

			Rest -= Required;
			RecvBufferPtr += Required;
			PacketSize = 0;
			PrevSize = 0;
		}
		else
		{
			// 패킷 생성 불가능.
			// 복사해둔다.
			memcpy(PacketBuffer + PrevSize, RecvBufferPtr, Rest);
			PrevSize += Rest;
			Rest = 0;
		}
	}
	User->SetPrevSize(PrevSize);
}

void Server::Join()
{
	// TODO: 이 전에 메모리가 모두 해제가 되었는지 확인해야함
	// 모두 Suspend시켜놓고 메모리 모두 해제하고 다시 Suspend 풀기
	// 메모리 해제가 모두 되었으면 Join()

	ReceiverThread.join();
	GameMngr.ClearAll();
}
