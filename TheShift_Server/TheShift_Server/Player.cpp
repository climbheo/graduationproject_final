﻿#include "Player.h"

//Player::Player(SocketInfo* Info) : SockInfo(Info)
//{
//}

Player::Player() :
	PrevSize(0)
{
	ZeroMemory(PacketBuffer, BUFFERSIZE);
}

Player::~Player()
{}

int8_t Player::GetThreadIndex() const
{
	return ThreadIndex;
}

SocketInfo* Player::GetSocketInfoPtr()
{
	return &SockInfo;
}

char* Player::GetRecvBuffer()
{
	return SockInfo.Buffer;
}

char* Player::GetPacketBuffer()
{
	return PacketBuffer;
}

uint32_t Player::GetPrevSize() const
{
	return PrevSize;
}

ClientId Player::GetId() const
{
	return Id;
}

void Player::SetThreadIndex(int8_t index)
{
	ThreadIndex = index;
}

void Player::SetPrevSize(uint32_t size)
{
	PrevSize = size;
}

void Player::SetSocketInfo(SocketInfo& info)
{
	SockInfo = info;
}


