﻿#include "MemoryStream.h"

InputMemoryStream::InputMemoryStream() :
	Buffer(nullptr),
	BitHead(0),
	BitCapacity(0)
{}

InputMemoryStream::InputMemoryStream(uint32_t inBitCount) :
	Buffer(nullptr),
	BitHead(0),
	BitCapacity(inBitCount)
{}

// use this to get data from recvPost()
char* InputMemoryStream::GetBufferPtr()
{
	//return buffer.data();
	return Buffer;
}

void InputMemoryStream::SetTargetBuffer(char* targetBuffer)
{
	Buffer = targetBuffer;
	Reset();
}

uint32_t InputMemoryStream::GetBitLength() const
{
	return BitHead;
}

uint32_t InputMemoryStream::GetByteLength() const
{
	return (BitHead + 7) >> 3;
}

void InputMemoryStream::Reset()
{
	BitHead = 0;
}

void InputMemoryStream::ReadBits(uint8_t& outData, uint32_t inBitCount)
{
	// calculate the byteOffset of the buffer
	// by dividing bitHead by 8
	// and the bitOffset by taking the last 3 bits
	uint32_t byteOffset = BitHead >> 3;
	uint32_t bitOffset = BitHead & 0x7;

	outData = static_cast<uint8_t>(Buffer[byteOffset] >> bitOffset);

	// read next byte if needed
	uint8_t bitsRead = 8 - bitOffset;
	if(inBitCount > bitsRead)
	{
		outData |= static_cast<uint8_t>(Buffer[byteOffset + 1] << bitsRead);
	}

	// discard other bits
	outData &= (~(0x00ff << inBitCount));

	BitHead += inBitCount;
}

void InputMemoryStream::ReadBits(void* outData, uint32_t inBitCount)
{
	uint32_t bitCount = inBitCount;
	uint8_t* destByte = static_cast<uint8_t*>(outData);

	// read all the bytes
	while(bitCount > 8)
	{
		ReadBits(*destByte, 8);
		++destByte;
		bitCount -= 8;
	}

	// read anything left
	if(bitCount > 0)
	{
		ReadBits(*destByte, bitCount);
	}
}

/// <summary>
/// Reads chat chatMessage.
/// </summary>
//void InputMemoryStream::ReadChat(Protocol::Chat& chatMessage)
//{
//	Read(chatMessage.To);
//	Read(chatMessage.From);
//	Read(chatMessage.Data);
//}

//void InputMemoryStream::Read(bool& outData)
//{
//    ReadBits(&outData, 1);
//}

/// <summary>
/// Reads string from input stream.
/// Read length of string and then read string.
/// </summary>
void InputMemoryStream::Read(std::string& outData)
{
	uint32_t len = 0;
	Read(len);
	outData.resize(len);

	for(auto& element : outData)
	{
		Read(element);
	}
}

///// <summary>
///// Reads glm::ivec2 from buffer.
///// </summary>
//void InputMemoryStream::read(glm::ivec2& ivec2)
//{
//	read(ivec2.x);
//	read(ivec2.y);
//}
///// <summary>
///// Reads glm::vec3 from buffer.
///// </summary>
//void InputMemoryStream::read(glm::vec3& vec3)
//{
//	read(vec3.x);
//	read(vec3.y);
//	read(vec3.z);
//}
//
///// <summary>
///// Reads glm::quat from buffer.
///// </summary>
//void InputMemoryStream::read(glm::quat& quaternion)
//{
//	read(quaternion.w);
//	read(quaternion.x);
//	read(quaternion.y);
//	read(quaternion.z);
//}

//void InputMemoryStream::Read(SerializableData& data)
//{
//	data.Read(*this);
//}
//
//void InputMemoryStream::Read(std::shared_ptr<SerializableData> data)
//{
//	(*data.get()).Read(*this);
//}

//void InputMemoryBitStream::read(Network::GameObjectData& data)
//{
//	data.read(*this);
//}

//void InputMemoryBitStream::read(std::vector<Network::MessageData>& vector)
//{
//	uint32_t size;
//	read(size);
//	vector.resize(size);
//	for(int i=0; i<size; ++i) {
//		read(vector[i]);
//	}
//}

// re-size buffer with number of bits
// this won't change the size buffer if inSize is smaller than current size
void OutputMemoryStream::ResizeBuffer(uint32_t inBitCapacity)
{
	if(Buffer.size() * 8 < inBitCapacity)
	{
		size_t bytes = inBitCapacity / 8;
		if(inBitCapacity % 8 != 0)
			++bytes;

		Buffer.resize(bytes);
		BitCapacity = static_cast<uint32_t>(Buffer.size() * 8);
	}
}

OutputMemoryStream::OutputMemoryStream() :
	BitHead(0),
	BitCapacity(0)
{
	ResizeBuffer(32);
}

const char* OutputMemoryStream::GetBufferPtr() const
{
	return Buffer.data();
}

uint32_t OutputMemoryStream::GetBitLength() const
{
	return BitHead;
}

uint32_t OutputMemoryStream::GetByteLength() const
{
	return (BitHead + 7) >> 3;
}

void OutputMemoryStream::Reset()
{
	BitHead = 0;
}

// writes less than a byte
void OutputMemoryStream::WriteBits(uint8_t inData, uint32_t inBitCount)
{
	uint32_t newHead = BitHead + static_cast<uint32_t>(inBitCount);
	if(newHead > BitCapacity)
	{
		ResizeBuffer(std::max(BitCapacity * 2, newHead));
	}

	// calculate the byteOffset of the buffer
	// by dividing bitHead by 8
	// and the bitOffset by taking the last 3 bits
	uint32_t byteOffset = BitHead >> 3;
	uint32_t bitOffset = BitHead & 0x7;

	// make sure wrong bit is not inserted.
	uint8_t mask = ~(0xff << bitOffset);
	Buffer[byteOffset] = (Buffer[byteOffset] & mask) | (inData << bitOffset);

	uint8_t bitsInserted = 8 - bitOffset;
	if(bitsInserted < inBitCount)
	{
		Buffer[byteOffset + 1] = inData >> bitsInserted;
	}

	BitHead = newHead;
}

void OutputMemoryStream::WriteBits(const void* inData, uint32_t inBitCount)
{
	size_t bitCount = inBitCount;
	const char* srcBytes = static_cast<const char*>(inData);

	// writeCurLog all the bytes
	while(bitCount > 8)
	{
		WriteBits(*srcBytes, 8);
		++srcBytes;
		bitCount -= 8;
	}

	// writeCurLog anything left
	if(bitCount > 0)
		WriteBits(*srcBytes, static_cast<uint32_t>(bitCount));
}

//void OutputMemoryStream::Write(bool inData)
//{
//	WriteBits(&inData, 1);
//}

//void OutputMemoryStream::Push(bool inData)
//{
//	//TODO: 이게 맞는거야??
//	Write(inData);
//	*reinterpret_cast<uint32_t*>(Buffer.data() + 4) = (BitHead - 32 + 7) >> 3;
//}

/// <summary>
/// Writes the specified string.
/// </summary>
/// <data order>
/// 1. Length of string
/// ~2. string
/// </data order>
//void OutputMemoryStream::Push(const std::string& inData)
//{
//	// need to count elements.
//	auto elementCount = static_cast<uint32_t>(inData.size());
//	Push(elementCount);
//
//	for (const auto& element : inData) {
//		Push(element);
//	}
//}

///// <summary>
///// Writes the specified string.
///// </summary>
///// <data order>
///// 1. Length of string
///// ~2. string
///// </data order>
//void OutputMemoryStream::push(const std::string_view& inData)
//{
//	// need to count elements.
//	auto elementCount = static_cast<uint32_t>(inData.size());
//	Write(elementCount);
//
//	for (const auto& element : inData) {
//		Write(element);
//	}
//}
//
///// <summary>
///// Writes the specified ivec2.
///// </summary>
//void OutputMemoryStream::push(const glm::ivec2& ivec2)
//{
//	push(ivec2.x);
//	push(ivec2.y);
//}
//
///// <summary>
///// Writes the specified vec3.
///// </summary>
//void OutputMemoryStream::push(const glm::vec3& vec3)
//{
//	push(vec3.x);
//	push(vec3.y);
//	push(vec3.z);
//}
//
///// <summary>
///// Writes the specified quaternion.
///// </summary>
//void OutputMemoryStream::push(const glm::quat& quat)
//{
//	push(quat.w);
//	push(quat.x);
//	push(quat.y);
//	push(quat.z);
//}

//void OutputMemoryStream::Push(const SerializableData& data)
//{
//	data.Push(*this);
//}

/// <summary>
/// Write a header of a network message.
/// </summary>
/// <data order>
/// 1. param1:messageType
/// 2. param2:dataSize
///.</data order>
//void OutputMemoryStream::WriteHeader(Protocol::MessageType messageType)
//{
//	Reset();
//	if(BitHead != 0) {
//		//std::cout << "OutputStream was not cleared before use." << std::endl;
//		Reset();
//	}
//	Push(uint32_t(0));
//	Push(messageType);
//}

///// <summary>
///// Write a header of a network message.
///// Supposed to contain session or lobby data at the end.
///// </summary>
///// <data order>
///// 1. MessageType::Connection
///// 2. Data size (sizeof(ConnectionType) + param2:dataSize)
///// 3. param1:connectionType
///// ~4. depend on ConnectionType
///// </data order>
//void OutputMemoryStream::WriteHeader(Network::ConnectionType connectionType)
//{
//	if (BitHead != 0) {
//		//std::cout << "OutputStream was not cleared before use." << std::endl;
//		Reset();
//	}
//	push(uint32_t(0));
//	push(Network::MessageType::Connection);
//	push(connectionType);
//}

///// <summary>
///// Write chat message.
///// </summary>
///// <data order>
///// 1. Header
///// 2. (param1:message).sender
///// 3. (param1:message).receiver
///// 4. (param1:message).string
///// </data order>
//void OutputMemoryStream::pushChat(const Network::ChatMessage& message)
//{
//	writeHeader(Network::MessageType::Chat);
//	push(message.to);
//	push(message.from);
//	push(message.message);
//}
