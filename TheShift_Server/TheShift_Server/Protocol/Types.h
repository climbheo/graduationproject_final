﻿#pragma once

#include "Protocol.h"
#include "../Vendor/include/Eigen/Eigen"

namespace TheShift
{
// 게임 내에서 Player가 가지는 정보
// 이름, 캐릭터 정보 등
struct Avatar
{
	Avatar()
	{
		ClientId_ = INVALID_CLIENTID;
		Name_ = "";
		SessionId_ = INVALID_SESSIONID;
		IsReady_ = false;
		IsLoaded_ = false;
	}
	Avatar(ClientId playerId)
	{
		Name_ = "";
		SessionId_ = INVALID_SESSIONID;
		IsReady_ = false;
		IsLoaded_ = false;
		ClientId_ = playerId;
	}

	ClientId ClientId_;
	SessionId SessionId_;

	UID ActorId_;
	std::string Name_;
	bool IsReady_;
	bool IsLoaded_;
};

struct Vector3f
{
	Vector3f() {}
	Vector3f(float x, float y, float z) : X(x), Y(y), Z(z)
	{}

	Vector3f operator*(float value) const
	{
		return Vector3f(X * value, Y * value, Z * value);
	}

	Vector3f& operator=(const Eigen::Vector3f& other)
	{
		X = other[0];
		Y = other[1];
		Z = other[2];
		return *this;
	}

	bool IsZero() const
	{
		if(X > FLT_EPSILON || X < -FLT_EPSILON) return false;
		if(Y > FLT_EPSILON || Y < -FLT_EPSILON) return false;
		if(Z > FLT_EPSILON || Z < -FLT_EPSILON) return false;
		return true;
	}

	float X;
	float Y;
	float Z;
};

struct Vector2f
{
	Vector2f() {}
	Vector2f(float x, float y) : X(x), Y(y) {}

	Vector2f operator*(float value) const
	{
		return Vector2f(X * value, Y * value);
	}

	Vector2f& operator=(const Eigen::Vector2f& other)
	{
		X = other.x();
		Y = other.y();
		return *this;
	}

	bool IsZero() const
	{
		if(X > FLT_EPSILON || X < -FLT_EPSILON) return false;
		if(Y > FLT_EPSILON || Y < -FLT_EPSILON) return false;
		return true;
	}

	float X;
	float Y;
};

enum class ActorType : uint16_t
{
	Prop = 0,
	MyCharacter,
	Player,
	Trigger,
	TestActor,
	KnightMonster,
	FatMonster,
	Spider,
	Bat,
	SwordMonster,
	BlackWareWolf,
	BrownWareWolf,
	WhiteWareWolf,
	Destructible,
	Interactable,
};

enum class CollisionType : uint8_t
{
	Unhandled = 0,

};

enum class BoundingVolumeType : uint8_t
{
	Unhandled = 0,
	Sphere,
	AABB,
	OBB,
	Capsule,
	//Heightmap = 5,
};


enum class InputType : uint16_t
{
	// Error
	Unhandled = 0,

	// X Y Axes Pressed
	Forward_Pressed,
	Back_Pressed,
	Left_Pressed,
	Right_Pressed,

	// X Y Axes Released
	Forward_Released,
	Back_Released,
	Left_Released,
	Right_Released,

	// No Input But Camera Rotated.
	// 이건 보내도 의미없는 input
	Camera_Rotation,

	// Mouse Left Right Button Pressed
	MouseLeft_Pressed,
	MouseRight_Pressed,

	// Mouse Left Right Button Released
	MouseLeft_Released,
	MouseRight_Released,

	// Combo_Check Notify
	Combo_Check,
	Combo_End,

	// Hit_Check Notify
	Hit_Check,
	Hit_Reset,

	//Move Notify
	Notify_Move,

	// Space Bar Pressed
	SpaceBar_Pressed,

	// V Key Pressed(Teleport)
	V_Pressed,

	//Shift Key (Increase Velocity)
	Shift_Pressed,
	Shift_Released,

	Cheat_Pressed
};

enum class MapType : uint16_t
{
	Unhandled = ConstHash("Unhandled"),
	TestMap = ConstHash("TestMap"),
	NewMap1 = ConstHash("NewMap1"),
	Landscape = ConstHash("Landscape"),

	MainMenu = ConstHash("MainMenu"),
	Stage01 = ConstHash("Stage01"),
	Stage02 = ConstHash("Stage02"),
	Stage03 = ConstHash("Stage03"),
	Stage04 = ConstHash("Stage04"),

	// ... 똑같이 추가하고 아래 GetMapType 함수의 case문도 추가해주세요~
};

namespace LevelName
{
const char* const MainMenu = "MainMenu";
const char* const Stage01 = "Stage01";
const char* const Stage02 = "Stage02";
const char* const Stage03 = "Stage03";
const char* const Stage04 = "Stage04";
const char* const TestLandscape = "Landscape";
}

inline MapType GetMapType(const char* MapName)
{
	MapType RetVal = MapType::Unhandled;
	bool NotAMapName = false;
	switch(static_cast<MapType>(ConstHash(MapName)))
	{
	case MapType::TestMap:
		break;

		// ... 맵 이름 추가해주세요.

	case MapType::NewMap1:
		break;

	case MapType::Landscape:
		break;

	case MapType::Stage01:
		break;

	default:
		NotAMapName = true;
		break;
	}

	if(NotAMapName)
		return MapType::Unhandled;
	else
		return static_cast<MapType>(ConstHash(MapName));
}

struct HeightmapInfo
{
	int Id;
	std::string FileName;
	Vector3f Position;
	Vector3f Rotation;
	Vector3f Scale;
	std::vector<std::vector<Eigen::Vector3f>> Verts;
};

enum class ActType : uint16_t
{
	None = 0,

	///여기 Enum은 "전"에 뭐넣지말것
	// 기본 공격
	BaseAttack_1,
	BaseAttack_2,
	BaseAttack_3,
	BaseAttack_4,

	//스매쉬
	Smash_1,
	Smash_2,
	Smash_3,
	Smash_4,

	//텔레포트 공격 관련 변수
	Teleport_Casting,
	Teleport_Attack_1,
	Teleport_Attack_2,
	Teleport_Attack_3,
	Teleport_Attack_4,
	///여기 Enum은 "전"에 뭐넣지말것

	//대쉬, 대쉬 End
	Dash,
	Dash_End,

	// 무기 변경
	WeaponSwap,

	//Attack End
	Attack_End,

	//Teleport End
	Teleport_End,

	//활 상태일때 Camera 회전은 클라에 전해져야함.
	Cam_Rotation,
	
	//Hit 상태 Reset
	Hit_Reset,

	//Player Death 상태로
	Player_Death,

	//Player Live 상태로
	Player_ReSpawn,

	// 아머 변경
	ArmorSwap,
};

// 특정 actor와 interact하는 경우 해당 act의 type
enum class InteractableActionType : uint16_t
{
	ActivateActors,
	DeactivateActors,
	OpenDoorRotateMinus,
	OpenDoorRotatePlus,
	NextLevel,
	OpenChest,
	DoorUnlock,
};

enum class TriggerActionType : uint16_t
{
	None,
	Stage01_PlayBossCutscene,
	Stage02_PlayBossCutscene,
	Stage03_PlayBossCutscene,
};

enum class EventType : uint16_t
{
	Stage01_CellDoor3Unlock,
	Stage01_CellDoor4Unlock,
};

enum class CheatType : uint16_t
{
	MoveToStart,
	MoveToMiddle,
	MoveToBoss,
	MoveToEnd,
	GodMode,
};

// Item 종류
enum class ItemType
{
	Weapon,
	Armor,
};

// 무기 종류
enum class WeaponItemType
{
	GreatSword,
	Box,
};

// 방어구 종류
enum class ArmorItemType
{
	None = 0,
	HeavyArmor,
	LightArmor,
};

enum class StatusType : uint16_t
{
	Idle = 1,
	MoveLeft = 2,
	MoveRight = 3,
	MoveUp = 4,
	MoveDown = 5,
};

#ifdef TSSERVER
// Client측에선 UENUM을 사용하므로 Type 순서를 동일하게 맞춰주어야 함
enum class WeaponType : uint8_t
{
	EQUIP_NONE = 0,
	HAND,
	SWORD,
	DAGGER,
	BOW,
	WEAPON_END
};
#endif

struct Stats
{
	float Mass = 100;
	float ForwardRunSpeed = 1000;
	float SideRunSpeed;
	float BackRunSpeed;

	float ForwardWalkSpeed;
	float SideWalkSpeed;
	float BackWalkSpeed;

	float ForwardRunAcceleration = 2000;
	float SideRunAcceleration;
	float BackRunAcceleration;

	float ForwardWalkAcceleration;
	float SideWalkAcceleration;
	float BackWalkAcceleration;
};

struct BoundingVolumeInfo
{
	BoundingVolumeType BoundingVolumeType_;
	Vector3f ScaledExtent;
	Vector3f RelativeLocation;
	Vector3f RelativeRotation;
	float HalfHeight;
	float Radius;
};

/// <summary>
/// Actor class별 고유한 정보를 저장한다.
/// ex) 속도, Bounding volume 정보
/// </summary>
struct ActorInfo
{
	ActorType Type;
	Stats Stat;
	std::vector<BoundingVolumeInfo> BoundInfo;
	bool IsMovable;
};

// Inventory에 들어가있는 item의 정보를 export할때 사용하는 구조체
struct ItemInfo
{
	int Index;
	int ItemCategory;
	int ItemType;
};

/// <summary>
/// Map에 배치된 Actor의 상태를 저장한다.
/// ex) Map에 (0.f, 0.f, 0.f)위치에 (0.f, 0.f, 0.f) (degrees) 만큼 회전된 상태
/// </summary>
struct ActorPlacementInfo
{
	std::string Tag;
	ActorType Type;
	Vector3f ActorPosition;
	// In degrees
	Vector3f ActorRotation;
	std::vector<BoundingVolumeInfo> BoundInfo;
	std::vector<ItemInfo> InventoryInfo;
	// Is this actor movable?
	bool IsMovable;
};

struct MapInfo
{
	MapType Type;
	std::vector<HeightmapInfo> Heightmaps;
	std::vector<ActorInfo> ActorTypeInfo;
	std::vector<ActorPlacementInfo> ActorPlacements;
};

struct AnimNotifyInfo
{
	bool operator==(const AnimNotifyInfo& other) const
	{
		return AnimName == other.AnimName;
	}
	bool operator<(const AnimNotifyInfo& other) const
	{
		return AnimName < other.AnimName;
	}



	AnimNotifyInfo() {}
	AnimNotifyInfo(const char* pName) { AnimName = pName; }
	AnimNotifyInfo(std::string name) { AnimName = name; }

	std::string AnimName;
	std::vector<std::pair<std::string, float>> AnimNotifies;
};

} // namespace TheShift
