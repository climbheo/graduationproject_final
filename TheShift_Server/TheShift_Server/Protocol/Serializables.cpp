﻿#include "Serializables.h"

namespace TheShift
{
namespace Serializables
{
void Add(const Vector3f& Vector, OutputMemoryStream& OutputStream)
{
	OutputStream.Push(Vector.X);
	OutputStream.Push(Vector.Y);
	OutputStream.Push(Vector.Z);
}

void Add(const std::string& Str, OutputMemoryStream& OutputStream)
{
	OutputStream.Push(Str);
}

void Add(const ActorInfo& Actor, OutputMemoryStream& OutputStream)
{
	Actor.Push(OutputStream);
}

void Get(Vector3f& Vector, InputMemoryStream& InputStream)
{
	InputStream.Read(Vector.X);
	InputStream.Read(Vector.Y);
	InputStream.Read(Vector.Z);
}

void Get(std::string& Str, InputMemoryStream& InputStream)
{
	InputStream.Read(Str);
}

void Add(const Avatar& avatar, OutputMemoryStream& OutputStream)
{
	OutputStream.Push(avatar.ClientId_);
	OutputStream.Push(avatar.IsReady_);
	OutputStream.Push(avatar.IsLoaded_);
	OutputStream.Push(avatar.SessionId_);
	OutputStream.Push(avatar.ActorId_);
	OutputStream.Push(avatar.Name_);
}

void Get(Avatar& avatar, InputMemoryStream& InputStream)
{
	InputStream.Read(avatar.ClientId_);
	InputStream.Read(avatar.IsReady_);
	InputStream.Read(avatar.IsLoaded_);
	InputStream.Read(avatar.SessionId_);
	InputStream.Read(avatar.ActorId_);
	InputStream.Read(avatar.Name_);
}

void Get(ActorInfo& Actor, InputMemoryStream& InputStream)
{
	Actor.Read(InputStream);
}

void Chat::Push(OutputMemoryStream& OutputStream) const
{}

void Chat::Read(InputMemoryStream& InputStream)
{}

uint16_t Chat::ClassId() const
{
	return static_cast<uint16_t>(Type::Chat);
}

void Connected::Push(OutputMemoryStream& OutputStream) const
{
	Add(MyId, OutputStream);
}

void Connected::Read(InputMemoryStream& InputStream)
{
	Get(MyId, InputStream);
}

uint16_t Connected::ClassId() const
{
	return static_cast<uint16_t>(Type::Connected);
}

void SessionCreateReq::Push(OutputMemoryStream& OutputStream) const
{
	Add(MyId, OutputStream);
	Add(Map, OutputStream);
	Add(SessionName, OutputStream);
}

void SessionCreateReq::Read(InputMemoryStream& InputStream)
{
	Get(MyId, InputStream);
	Get(Map, InputStream);
	Get(SessionName, InputStream);
}

uint16_t SessionCreateReq::ClassId() const
{
	return static_cast<uint16_t>(Type::SessionCreateReq);
}

uint16_t SerializableData::ClassId() const
{
	return ConstHash("SerializableData");
}

void PlayerInfo::Push(OutputMemoryStream& OutputStream) const
{
	Add(Id, OutputStream);
	Add(Name, OutputStream);
}

void PlayerInfo::Read(InputMemoryStream& InputStream)
{
	Get(Id, InputStream);
	Get(Name, InputStream);
}

void SessionJoinReq::Push(OutputMemoryStream& OutputStream) const
{
	Add(SessionIndex, OutputStream);
}

void SessionJoinReq::Read(InputMemoryStream& InputStream)
{
	Get(SessionIndex, InputStream);
}

uint16_t SessionJoinReq::ClassId() const
{
	return static_cast<uint16_t>(Type::SessionJoinReq);
}

void CreateAndStartSessionReq::Push(OutputMemoryStream& OutputStream) const
{
	Add(Type, OutputStream);
	Add(SessionName, OutputStream);
}

void CreateAndStartSessionReq::Read(InputMemoryStream& InputStream)
{
	Get(Type, InputStream);
	Get(SessionName, InputStream);
}

uint16_t CreateAndStartSessionReq::ClassId() const
{
	return static_cast<uint16_t>(Type::CreateAndStartSessionReq);
}

void SessionInfo::Push(OutputMemoryStream& OutputStream) const
{
	Add(SessionIndex, OutputStream);
	Add(SessionName, OutputStream);
	Add(ClientIds, OutputStream);
	Add(IsInGame, OutputStream);
	Add(Map, OutputStream);
}

void SessionInfo::Read(InputMemoryStream& InputStream)
{
	Get(SessionIndex, InputStream);
	Get(SessionName, InputStream);
	Get(ClientIds, InputStream);
	Get(IsInGame, InputStream);
	Get(IsInGame, InputStream);
}

void AvatarInfo::Push(OutputMemoryStream& OutputStream) const
{
	Add(Player, OutputStream);
}

void AvatarInfo::Read(InputMemoryStream& InputStream)
{
	Get(Player, InputStream);
}

void ActorInfo::Push(OutputMemoryStream& OutputStream) const
{
	Add(ActorId, OutputStream);
	Add(Type, OutputStream);
	Add(Position, OutputStream);
	Add(Hp, OutputStream);
}

void ActorInfo::Read(InputMemoryStream& InputStream)
{
	Get(ActorId, InputStream);
	Get(Type, InputStream);
	Get(Position, InputStream);
	Get(Hp, InputStream);
}

//void SimpleAct::Push(OutputMemoryStream& OutputStream) const
//{
//    Add(Self, OutputStream);
//    Add(Type, OutputStream);
//}
//
//void SimpleAct::Read(InputMemoryStream& InputStream)
//{
//    Get(Self, InputStream);
//    Get(Type, InputStream);
//}

//void ActWithTargetVector::Push(OutputMemoryStream& OutputStream) const
//{
//    //Add(Self, OutputStream);
//    //Add(Type, OutputStream);
//    Act::Push(OutputStream);
//    Add(TargetVector, OutputStream);
//}
//
//void ActWithTargetVector::Read(InputMemoryStream& InputStream)
//{
//    //Get(Self, InputStream);
//    //Get(Type, InputStream);
//    Act::Read(InputStream);
//    Get(TargetVector, InputStream);
//}
//
//uint16_t ActWithTargetVector::ClassId()
//{
//    return static_cast<uint16_t>(Type::ActWithTargetVector);
//}
//
//void ActWithTargetActor::Push(OutputMemoryStream& OutputStream) const
//{
//    //Add(Self, OutputStream);
//    //Add(Type, OutputStream);
//    Act::Push(OutputStream);
//    Add(TargetActorId, OutputStream);
//}
//
//void ActWithTargetActor::Read(InputMemoryStream& InputStream)
//{
//    //Get(Self, InputStream);
//    //Get(Type, InputStream);
//    Act::Read(InputStream);
//    Get(TargetActorId, InputStream);
//}
//
//uint16_t ActWithTargetActor::ClassId()
//{
//    return static_cast<uint16_t>(Type::ActWithTargetActor);
//}

void ActorPosition::Push(OutputMemoryStream& OutputStream) const
{
	Add(Self, OutputStream);
	Add(Position, OutputStream);
	Add(Rotation, OutputStream);
	Add(Speed, OutputStream);
}

void ActorPosition::Read(InputMemoryStream& InputStream)
{
	Get(Self, InputStream);
	Get(Position, InputStream);
	Get(Rotation, InputStream);
	Get(Speed, InputStream);
}

void Disconnect::Push(OutputMemoryStream& OutputStream) const
{
	Add(ActorId, OutputStream);
}

void Disconnect::Read(InputMemoryStream& InputStream)
{
	Get(ActorId, InputStream);
}

uint16_t Disconnect::ClassId() const
{
	return static_cast<uint16_t>(Type::Disconnect);
}

void LobbyJoinReq::Push(OutputMemoryStream& OutputStream) const
{
	Add(MyId, OutputStream);
	Add(Name, OutputStream);
}

void LobbyJoinReq::Read(InputMemoryStream& InputStream)
{
	Get(MyId, InputStream);
	Get(Name, InputStream);
}

uint16_t LobbyJoinReq::ClassId() const
{
	return static_cast<uint16_t>(Type::LobbyJoinReq);
}

void LobbyConnected::Push(OutputMemoryStream& OutputStream) const
{
	Add(Connected, OutputStream);
}

void LobbyConnected::Read(InputMemoryStream& InputStream)
{
	Get(Connected, InputStream);
}

uint16_t LobbyConnected::ClassId() const
{
	return static_cast<uint16_t>(Type::LobbyConnected);
}

void LobbyInfoReq::Push(OutputMemoryStream& OutputStream) const
{
	Add(MyId, OutputStream);
}

void LobbyInfoReq::Read(InputMemoryStream& InputStream)
{
	Get(MyId, InputStream);
}

uint16_t LobbyInfoReq::ClassId() const
{
	return static_cast<uint16_t>(Type::LobbyInfoReq);
}

void LobbyInfo::Push(OutputMemoryStream& OutputStream) const
{
	Add(Players, OutputStream);
	Add(Sessions, OutputStream);
}

void LobbyInfo::Read(InputMemoryStream& InputStream)
{
	Get(Players, InputStream);
	Get(Sessions, InputStream);
}

uint16_t LobbyInfo::ClassId() const
{
	return static_cast<uint16_t>(Type::LobbyInfo);
}

void SessionConnected::Push(OutputMemoryStream& OutputStream) const
{
	Add(SessionIndex, OutputStream);
	Add(SessionName, OutputStream);
	Add(PlayersInfo, OutputStream);
	Add(HostId, OutputStream);
}

void SessionConnected::Read(InputMemoryStream& InputStream)
{
	Get(SessionIndex, InputStream);
	Get(SessionName, InputStream);
	Get(PlayersInfo, InputStream);
	Get(HostId, InputStream);
}

uint16_t SessionConnected::ClassId() const
{
	return static_cast<uint16_t>(Type::SessionConnected);
}

void MapSelect::Push(OutputMemoryStream& OutputStream) const
{
	Add(SessionIndex, OutputStream);
	Add(Map, OutputStream);
}

void MapSelect::Read(InputMemoryStream& InputStream)
{
	Get(SessionIndex, InputStream);
	Get(Map, InputStream);
}

uint16_t MapSelect::ClassId() const
{
	return static_cast<uint16_t>(Type::MapSelect);
}

void LevelChanged::Push(OutputMemoryStream& OutputStream) const
{
	Add(Map, OutputStream);
}

void LevelChanged::Read(InputMemoryStream& InputStream)
{
	Get(Map, InputStream);
}

uint16_t LevelChanged::ClassId() const
{
	return static_cast<uint16_t>(Type::LevelChanged);
}

void ActorCreated::Push(OutputMemoryStream& OutputStream) const
{
	Add(ActorId, OutputStream);
	Add(Type, OutputStream);
	Add(Position, OutputStream);
}

void ActorCreated::Read(InputMemoryStream& InputStream)
{
	Get(ActorId, InputStream);
	Get(Type, InputStream);
	Get(Position, InputStream);
}

uint16_t ActorCreated::ClassId() const
{
	return static_cast<uint16_t>(Type::ActorCreated);
}

void SessionStarted::Push(OutputMemoryStream& OutputStream) const
{
	Add(Map, OutputStream);
	Add(Actors, OutputStream);
	Add(Players, OutputStream);
	Add(ServerUpdateTimestep, OutputStream);
}

void SessionStarted::Read(InputMemoryStream& InputStream)
{
	Get(Map, InputStream);
	Get(Actors, InputStream);
	Get(Players, InputStream);
	Get(ServerUpdateTimestep, InputStream);
}

uint16_t SessionStarted::ClassId() const
{
	return static_cast<uint16_t>(Type::SessionStarted);
}

void NewPlayer::Push(OutputMemoryStream& OutputStream) const
{
	Add(Player, OutputStream);
	Add(NewActor, OutputStream);
}

void NewPlayer::Read(InputMemoryStream& InputStream)
{
	Get(Player, InputStream);
	Get(NewActor, InputStream);
}

uint16_t NewPlayer::ClassId() const
{
	return static_cast<uint16_t>(Type::NewPlayer);
}

void Ready::Push(OutputMemoryStream& OutputStream) const
{
	Add(IsReady, OutputStream);
}

void Ready::Read(InputMemoryStream& InputStream)
{
	Get(IsReady, InputStream);
}

uint16_t Ready::ClassId() const
{
	return static_cast<uint16_t>(Type::Ready);
}

void LoadCompleted::Push(OutputMemoryStream& OutputStream) const
{
	Add(Completed, OutputStream);
}

void LoadCompleted::Read(InputMemoryStream& InputStream)
{
	Get(Completed, InputStream);
}

uint16_t LoadCompleted::ClassId() const
{
	return static_cast<uint16_t>(Type::LoadCompleted);
}

void GameStarted::Push(OutputMemoryStream& OutputStream) const
{
	Add(Started, OutputStream);
}

void GameStarted::Read(InputMemoryStream& InputStream)
{
	Get(Started, InputStream);
}

uint16_t GameStarted::ClassId() const
{
	return static_cast<uint16_t>(Type::GameStarted);
}

void ReadyState::Push(OutputMemoryStream& OutputStream) const
{
	Add(Id, OutputStream);
	Add(IsReady, OutputStream);
}

void ReadyState::Read(InputMemoryStream& InputStream)
{
	Get(Id, InputStream);
	Get(IsReady, InputStream);
}

uint16_t ReadyState::ClassId() const
{
	return static_cast<uint16_t>(Type::ReadyState);
}

void Input::Push(OutputMemoryStream& OutputStream) const
{
	Add(InputValue, OutputStream);
	Add(InputId, OutputStream);
	Add(CamRotation, OutputStream);
	Add(ControlRotation, OutputStream);
}

void Input::Read(InputMemoryStream& InputStream)
{
	Get(InputValue, InputStream);
	Get(InputId, InputStream);
	Get(CamRotation, InputStream);
	Get(ControlRotation, InputStream);
}

uint16_t Input::ClassId() const
{
	return static_cast<uint16_t>(Type::Input);
}

void DirectiveAct::Push(OutputMemoryStream& OutputStream) const
{
	Add(Type, OutputStream);
	Add(Value, OutputStream);
}

void DirectiveAct::Read(InputMemoryStream& InputStream)
{
	Get(Type, InputStream);
	Get(Value, InputStream);
}

uint16_t DirectiveAct::ClassId() const
{
	return static_cast<uint16_t>(Type::DirectiveAct);
}

void InteractableAct::Push(OutputMemoryStream& OutputStream) const
{
	Add(Tag, OutputStream);
	Add(Type, OutputStream);
}

void InteractableAct::Read(InputMemoryStream& InputStream)
{
	Get(Tag, InputStream);
	Get(Type, InputStream);
}

uint16_t InteractableAct::ClassId() const
{
	return static_cast<uint16_t>(Type::InteractableAct);
}

void MountWeapon::Push(OutputMemoryStream& OutputStream) const
{
	Add(Weapon, OutputStream);
}

void MountWeapon::Read(InputMemoryStream& InputStream)
{
	Get(Weapon, InputStream);
}

uint16_t MountWeapon::ClassId() const
{
	return static_cast<uint16_t>(Type::MountWeapon);
}

void SessionStateUpdate::Push(OutputMemoryStream& OutputStream) const
{
	Add(ActorPositions, OutputStream);
}

void SessionStateUpdate::Read(InputMemoryStream& InputStream)
{
	Get(ActorPositions, InputStream);
}

uint16_t SessionStateUpdate::ClassId() const
{
	return static_cast<uint16_t>(Type::SessionStateUpdate);
}

void MyCharacterSyncState::Push(OutputMemoryStream& OutputStream) const
{
	Add(MyActorId, OutputStream);
	Add(LastProcessedInputId, OutputStream);
	MyCharacterPosition.Push(OutputStream);
}

void MyCharacterSyncState::Read(InputMemoryStream& InputStream)
{
	Get(MyActorId, InputStream);
	Get(LastProcessedInputId, InputStream);
	MyCharacterPosition.Read(InputStream);
}

uint16_t MyCharacterSyncState::ClassId() const
{
	return static_cast<uint16_t>(Type::MyCharacterSyncState);
}

void SessionExit::Push(OutputMemoryStream& OutputStream) const
{
	Add(CurrentSessionId, OutputStream);
}

void SessionExit::Read(InputMemoryStream& InputStream)
{
	Get(CurrentSessionId, InputStream);
}

uint16_t SessionExit::ClassId() const
{
	return static_cast<uint16_t>(Type::SessionExit);
}

void RotateForTime::Push(OutputMemoryStream& OutputStream) const
{
	Add(Id, OutputStream);
	Add(DurationMilliseconds, OutputStream);
	Add(RotateAmount, OutputStream);
}

void RotateForTime::Read(InputMemoryStream& InputStream)
{
	Get(Id, InputStream);
	Get(DurationMilliseconds, InputStream);
	Get(RotateAmount, InputStream);
}

uint16_t RotateForTime::ClassId() const
{
	return static_cast<uint16_t>(Type::RotateForTime);
}

void PlayerDisconnect::Push(OutputMemoryStream& OutputStream) const
{
	Add(DisconnectingClientId, OutputStream);
}

void PlayerDisconnect::Read(InputMemoryStream& InputStream)
{
	Get(DisconnectingClientId, InputStream);
}

uint16_t PlayerDisconnect::ClassId() const
{
	return static_cast<uint16_t>(Type::PlayerDisconnect);
}

void CollisionDebug::Push(OutputMemoryStream& OutputStream) const
{
	Add(Info, OutputStream);
}

void CollisionDebug::Read(InputMemoryStream& InputStream)
{
	Get(Info, InputStream);
}

uint16_t CollisionDebug::ClassId() const
{
	return static_cast<uint16_t>(Type::CollisionDebug);
}

void AttackRangeDebug::Push(OutputMemoryStream& OutputStream) const
{
	Add(Info, OutputStream);
}

void AttackRangeDebug::Read(InputMemoryStream& InputStream)
{
	Get(Info, InputStream);
}

uint16_t AttackRangeDebug::ClassId() const
{
	return static_cast<uint16_t>(Type::AttackRangeDebug);
}

void BoundingVolumeDebug::Push(OutputMemoryStream& OutputStream) const
{
	Add(BoundingVolumeType_, OutputStream);
	Add(Position, OutputStream);
	Add(Rotation, OutputStream);
	Add(ScaledExtent, OutputStream);
	Add(HalfHeight, OutputStream);
	Add(Radius, OutputStream);
}

void BoundingVolumeDebug::Read(InputMemoryStream& InputStream)
{
	Get(BoundingVolumeType_, InputStream);
	Get(Position, InputStream);
	Get(Rotation, InputStream);
	Get(ScaledExtent, InputStream);
	Get(HalfHeight, InputStream);
	Get(Radius, InputStream);
}

void Act::Push(OutputMemoryStream& OutputStream) const
{
	Add(Self, OutputStream);
	Add(Type, OutputStream);
}

void Act::Read(InputMemoryStream& InputStream)
{
	Get(Self, InputStream);
	Get(Type, InputStream);
}

uint16_t Act::ClassId() const
{
	return static_cast<uint16_t>(Type::Act);
}

void Hit::Push(OutputMemoryStream& OutputStream) const
{
	Add(Self, OutputStream);
	Add(Hp, OutputStream);
	Add(Damage, OutputStream);
	Add(IsDead, OutputStream);
	Add(AttackerId, OutputStream);
	Add(AttackType, OutputStream);
}

void Hit::Read(InputMemoryStream& InputStream)
{
	Get(Self, InputStream);
	Get(Hp, InputStream);
	Get(Damage, InputStream);
	Get(IsDead, InputStream);
	Get(AttackerId, InputStream);
	Get(AttackType, InputStream);
}

uint16_t Hit::ClassId() const
{
	return static_cast<uint16_t>(Type::Hit);
}

void DestructibleHit::Push(OutputMemoryStream& OutputStream) const
{
	Add(Tag, OutputStream);
	Add(Hp, OutputStream);
	Add(Damage, OutputStream);
	Add(IsDead, OutputStream);
	Add(AttackerId, OutputStream);
	Add(AttackType, OutputStream);
}

void DestructibleHit::Read(InputMemoryStream& InputStream)
{
	Get(Tag, InputStream);
	Get(Hp, InputStream);
	Get(Damage, InputStream);
	Get(IsDead, InputStream);
	Get(AttackerId, InputStream);
	Get(AttackType, InputStream);
}

uint16_t DestructibleHit::ClassId() const
{
	return static_cast<uint16_t>(Type::DestructibleHit);
}

void ActorRecognizedAPlayer::Push(OutputMemoryStream& OutputStream) const
{
	Add(Self, OutputStream);
	Add(TargetId, OutputStream);
}

void ActorRecognizedAPlayer::Read(InputMemoryStream& InputStream)
{
	Get(Self, InputStream);
	Get(TargetId, InputStream);
}

uint16_t ActorRecognizedAPlayer::ClassId() const
{
	return static_cast<uint16_t>(Type::ActorRecognizedAPlayer);
}

void ActWithVector::Push(OutputMemoryStream& OutputStream) const
{
	Act::Push(OutputStream);
	Add(Vector, OutputStream);
}

void ActWithVector::Read(InputMemoryStream& InputStream)
{
	Act::Read(InputStream);
	Get(Vector, InputStream);
}

uint16_t ActWithVector::ClassId() const
{
	return static_cast<uint16_t>(Type::ActWithVector);
}

void ActWithTarget::Push(OutputMemoryStream& OutputStream) const
{
	Act::Push(OutputStream);
	Add(TargetId, OutputStream);
}

void ActWithTarget::Read(InputMemoryStream& InputStream)
{
	Act::Read(InputStream);
	Get(TargetId, InputStream);
}

uint16_t ActWithTarget::ClassId() const
{
	return static_cast<uint16_t>(Type::ActWithTarget);
}

void ActWithTargets::Push(OutputMemoryStream& OutputStream) const
{
	Act::Push(OutputStream);
	Add(IdsOfTargets, OutputStream);
}

void ActWithTargets::Read(InputMemoryStream& InputStream)
{
	Act::Read(InputStream);
	Get(IdsOfTargets, InputStream);
}

uint16_t ActWithTargets::ClassId() const
{
	return static_cast<uint16_t>(Type::ActWithTargets);
}

void ChangeUpdateTimestep::Push(OutputMemoryStream& OutputStream) const
{
	Add(NewTimestep, OutputStream);
}

void ChangeUpdateTimestep::Read(InputMemoryStream& InputStream)
{
	Get(NewTimestep, InputStream);
}

uint16_t ChangeUpdateTimestep::ClassId() const
{
	return static_cast<uint16_t>(Type::ChangeUpdateTimestep);
}

void UpdateTimestepChanged::Push(OutputMemoryStream& OutputStream) const
{
	Add(NewTimestep, OutputStream);
}

void UpdateTimestepChanged::Read(InputMemoryStream& InputStream)
{
	Get(NewTimestep, InputStream);
}

uint16_t UpdateTimestepChanged::ClassId() const
{
	return static_cast<uint16_t>(Type::UpdateTimestepChanged);
}

void ToSword::Push(OutputMemoryStream& OutputStream) const
{
	Add(Id, OutputStream);
}

void ToSword::Read(InputMemoryStream& InputStream)
{
	Get(Id, InputStream);
}

uint16_t ToSword::ClassId() const
{
	return static_cast<uint16_t>(Type::ToSword);
}

void SwapWeapon::Push(OutputMemoryStream& OutputStream) const
{
	Add(Id, OutputStream);
	Add(SwapType, OutputStream);
}

void SwapWeapon::Read(InputMemoryStream& InputStream)
{
	Get(Id, InputStream);
	Get(SwapType, InputStream);
}

uint16_t SwapWeapon::ClassId() const
{
	return static_cast<uint16_t>(Type::SwapWeapon);
}

void SwapArmor::Push(OutputMemoryStream& OutputStream) const
{
	Add(Id, OutputStream);
	Add(SwapType, OutputStream);
}

void SwapArmor::Read(InputMemoryStream& InputStream)
{
	Get(Id, InputStream);
	Get(SwapType, InputStream);
}

uint16_t SwapArmor::ClassId() const
{
	return static_cast<uint16_t>(Type::SwapArmor);
}

void ToNewPlayerOldPlayerWeapons::Push(OutputMemoryStream& OutputStream) const
{
	Add(SwapWeapons, OutputStream);
}

void ToNewPlayerOldPlayerWeapons::Read(InputMemoryStream& InputStream)
{
	Get(SwapWeapons, InputStream);
}

uint16_t ToNewPlayerOldPlayerWeapons::ClassId() const
{
	return static_cast<uint16_t>(Type::ToNewPlayerOldPlayerWeapons);
}

void StatChange::Push(OutputMemoryStream& OutputStream) const
{
	Add(Id, OutputStream);
	Add(Hp, OutputStream);
	Add(Stamina, OutputStream);
	Add(TeleportGage, OutputStream);
}

void StatChange::Read(InputMemoryStream& InputStream)
{ 
	Get(Id, InputStream);
	Get(Hp, InputStream);
	Get(Stamina, InputStream);
	Get(TeleportGage, InputStream);
}

uint16_t StatChange::ClassId() const
{
	return static_cast<uint16_t>(Type::StatChange);
}

void Event::Push(OutputMemoryStream& OutputStream) const
{
	Add(Type, OutputStream);
}

void Event::Read(InputMemoryStream& InputStream)
{
	Get(Type, InputStream);
}

uint16_t Event::ClassId() const
{
	return static_cast<uint16_t>(Type::Event);
}

void TeleportGageChange::Push(OutputMemoryStream& OutputStream) const
{
	Add(Id, OutputStream);
	Add(Gage, OutputStream);
}

void TeleportGageChange::Read(InputMemoryStream& InputStream)
{
	Get(Id, InputStream);
	Get(Gage, InputStream);
}

uint16_t TeleportGageChange::ClassId() const
{
	return static_cast<uint16_t>(Type::TeleportGageChange);
}

void MaxTeleportGageChange::Push(OutputMemoryStream& OutputStream) const
{
	Add(Id, OutputStream);
	Add(NewMaxValue, OutputStream);
}

void MaxTeleportGageChange::Read(InputMemoryStream& InputStream)
{
	Get(Id, InputStream);
	Get(NewMaxValue, InputStream);
}

uint16_t MaxTeleportGageChange::ClassId() const
{
	return static_cast<uint16_t>(Type::MaxTeleportGageChange);
}

void StaminaChange::Push(OutputMemoryStream& OutputStream) const
{
	Add(Id, OutputStream);
	Add(NewStaminaValue, OutputStream);
}

void StaminaChange::Read(InputMemoryStream& InputStream)
{
	Get(Id, InputStream);
	Get(NewStaminaValue, InputStream);
}

uint16_t StaminaChange::ClassId() const
{
	return static_cast<uint16_t>(Type::StaminaChange);
}

void MaxStaminaChange::Push(OutputMemoryStream& OutputStream) const
{
	Add(Id, OutputStream);
	Add(NewMaxStaminaValue, OutputStream);
}

void MaxStaminaChange::Read(InputMemoryStream& InputStream)
{
	Get(Id, InputStream);
	Get(NewMaxStaminaValue, InputStream);
}

uint16_t MaxStaminaChange::ClassId() const
{
	return static_cast<uint16_t>(Type::MaxStaminaChange);
}

void ItemAcquire::Push(OutputMemoryStream& OutputStream) const
{
	Add(Index, OutputStream);
	Add(ItemCategory, OutputStream);
	Add(ItemType, OutputStream);
}

void ItemAcquire::Read(InputMemoryStream& InputStream)
{
	Get(Index, InputStream);
	Get(ItemCategory, InputStream);
	Get(ItemType, InputStream);
}

uint16_t ItemAcquire::ClassId() const
{
	return static_cast<uint16_t>(Type::ItemAcquire);
}

void ItemUnacquire::Push(OutputMemoryStream& OutputStream) const
{
	Add(OwnerId, OutputStream);
	Add(Index, OutputStream);
}

void ItemUnacquire::Read(InputMemoryStream& InputStream)
{
	Get(OwnerId, InputStream);
	Get(Index, InputStream);
}

uint16_t ItemUnacquire::ClassId() const
{
	return static_cast<uint16_t>(Type::ItemUnacquire);
}

void ItemEquip::Push(OutputMemoryStream& OutputStream) const
{
	Add(OwnerActorId, OutputStream);
	Add(ItemCategory, OutputStream);
	Add(ItemType, OutputStream);
}

void ItemEquip::Read(InputMemoryStream& InputStream)
{
	Get(OwnerActorId, InputStream);
	Get(ItemCategory, InputStream);
	Get(ItemType, InputStream);
}

uint16_t ItemEquip::ClassId() const
{
	return static_cast<uint16_t>(Type::ItemEquip);
}

void ItemUnequip::Push(OutputMemoryStream& OutputStream) const
{
	Add(OwnerActorId, OutputStream);
	Add(Index, OutputStream);
}

void ItemUnequip::Read(InputMemoryStream& InputStream)
{
	Get(OwnerActorId, InputStream);
	Get(Index, InputStream);
}

uint16_t ItemUnequip::ClassId() const
{
	return static_cast<uint16_t>(Type::ItemUnequip);
}

void ItemAcquireFromChestReq::Push(OutputMemoryStream& OutputStream) const
{
	Add(ChestTag, OutputStream);
	Add(Index, OutputStream);
}

void ItemAcquireFromChestReq::Read(InputMemoryStream& InputStream)
{
	Get(ChestTag, InputStream);
	Get(Index, InputStream);
}

uint16_t ItemAcquireFromChestReq::ClassId() const
{
	return static_cast<uint16_t>(Type::ItemAcquireFromChestReq);
}

void TriggerNotify::Push(OutputMemoryStream& OutputStream) const
{
	Add(Tag, OutputStream);
	Add(Act, OutputStream);
}

void TriggerNotify::Read(InputMemoryStream& InputStream)
{
	Get(Tag, InputStream);
	Get(Act, InputStream);
}

uint16_t TriggerNotify::ClassId() const
{
	return static_cast<uint16_t>(Type::TriggerNotify);
}

void Cheat::Push(OutputMemoryStream& OutputStream) const
{
	Add(Id, OutputStream);
	Add(Type, OutputStream);
}

void Cheat::Read(InputMemoryStream& InputStream)
{
	Get(Id, InputStream);
	Get(Type, InputStream);
}

uint16_t Cheat::ClassId() const
{
	return static_cast<uint16_t>(Type::Cheat);
}

void ItemRemoveFromChest::Push(OutputMemoryStream& OutputStream) const
{
	Add(ChestTag, OutputStream);
	Add(Index, OutputStream);
}

void ItemRemoveFromChest::Read(InputMemoryStream& InputStream)
{
	Get(ChestTag, InputStream);
	Get(Index, InputStream);
}

uint16_t ItemRemoveFromChest::ClassId() const
{
	return static_cast<uint16_t>(Type::ItemRemoveFromChest);
}

void LevelChangeReqDebug::Push(OutputMemoryStream& OutputStream) const
{
	Add(Type, OutputStream);
}

void LevelChangeReqDebug::Read(InputMemoryStream& InputStream)
{
	Get(Type, InputStream);
}

uint16_t LevelChangeReqDebug::ClassId() const
{
	return static_cast<uint16_t>(Type::LevelChangeReqDebug);
}

void ActWithValue::Push(OutputMemoryStream& OutputStream) const
{
	Act::Push(OutputStream);
	Add(Value, OutputStream);
}

void ActWithValue::Read(InputMemoryStream& InputStream)
{
	Act::Read(InputStream);
	Get(Value, InputStream);
}

uint16_t ActWithValue::ClassId() const
{
	return static_cast<uint16_t>(Type::ActWithValue);
}

} // namespace Serializables
} // namespace TheShift
