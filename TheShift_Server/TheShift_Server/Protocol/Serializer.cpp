﻿#include "Serializer.h"

namespace TheShift
{
const char* Serializer::GetBuffer() const
{
	return OutputStream.GetBufferPtr();
}

char* Deserializer::GetBuffer()
{
	return InputStream.GetBufferPtr();
}
} // namespace TheShift
