﻿#pragma once

#include <stdint.h>

#define TSSERVER
//#define TSCLIENT

namespace TheShift
{
using SessionId = uint16_t;
using ClientId = uint16_t;
using UID = uint32_t;
using ThreadIndex = int8_t;

constexpr uint16_t DEFAULT_PORT = 54000;
constexpr int BUFFERSIZE = 2048;
constexpr ThreadIndex INVALID_THREADINDEX = -1;
constexpr ClientId INVALID_CLIENTID = 0;
constexpr SessionId INVALID_SESSIONID = 0;
constexpr SessionId NEW_SESSION = 1;
constexpr SessionId LOBBY = 1;
constexpr SessionId MAX_SESSION = 50000;
// Session당 Actor 개수 제한 10만
constexpr UID MAX_UID = 100000;
constexpr UID INVALID_UID = 0;

const char* const MapDataDir = "Protocol/Json/Maps/";
const char* const AnimNotifyDataDir = "Protocol/Json/Notifies/";
const char* const HeightmapDir = "Protocol/Heightmap/";

inline constexpr uint16_t ConstHash_(const char* Name, uint32_t NumChar)
{
	return *Name ? static_cast<uint32_t>(Name[0])* NumChar + 33 + ConstHash_(Name + 1, NumChar + 1) : 5381;
}

inline constexpr uint16_t ConstHash(const char* Name)
{
	return static_cast<uint32_t>(Name[0]) + 33 + ConstHash_(Name + 1, 1);
}
}
