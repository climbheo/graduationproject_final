﻿#pragma once

#include "common.h"
//#include "Protocol/Serializables.h"

using namespace TheShift;

enum class IoType
{
	Read = 1,
	Write = 2,
	None = 3
};

struct SocketInfo : WSAOVERLAPPED
{
	//WSAOVERLAPPED Overlapped;
	SOCKET Socket;
	char Buffer[BUFFERSIZE];
	DWORD RecvBytes;
	DWORD SendBytes;
	WSABUF WsaBuffer;
	IoType Io = IoType::None;
	ClientId PlayerIndex;

	SocketInfo() :
		Socket(INVALID_SOCKET), RecvBytes(0), SendBytes(0), PlayerIndex(0)
	{
		ZeroMemory(Buffer, BUFFERSIZE);
		Internal = InternalHigh = 0;
		Offset = OffsetHigh = 0;
		hEvent = nullptr;

		WsaBuffer.buf = Buffer;
		WsaBuffer.len = BUFFERSIZE;
	}
};

// Player의 정보를 담는다.
// 할당할때 SocketInfo를 지정해 줘야함.
class Player
{
public:
	Player();
	//Player(SocketInfo* Info);
	~Player();

	// WSAOVERLAPPED, SOCKET등 SOCKET IO 관련 정보

	// 해당 Player가 속한 Thread의 index를 return한다.
	// -1이 return된 경우 아직 아무 thread에도 속하지 않은것임.
	int8_t GetThreadIndex() const;
	SocketInfo* GetSocketInfoPtr();
	char* GetRecvBuffer();
	char* GetPacketBuffer();
	uint32_t GetPrevSize() const;
	ClientId GetId() const;

	// 해당 Player가 Game에 입장해 캐릭터가 Threads[Index]에 할당된 경우 해당 Player의 packet을 Threads[Index]로 dispatch해주기 위해 index를 설정한다.
	void SetThreadIndex(int8_t index);
	void SetPrevSize(uint32_t size);
	void SetSocketInfo(SocketInfo& info);

private:
	SocketInfo SockInfo;
	char PacketBuffer[BUFFERSIZE];
	uint32_t PrevSize;

	int8_t ThreadIndex = -1;
	ClientId Id;
};


