﻿#pragma once

#include "common.h"
#include <thread>
//#include <functional>
#include "tbb/include/tbb/concurrent_queue.h"
#include "tbb/include/tbb/spin_rw_mutex.h"
#include "Protocol/MemoryStream.h"
#include "Protocol/atomic_memory_pool.h"
#include "Protocol/MessageBuilder.h"
#include "Protocol/Protocol.h"
#include "Game/System/GameThreadPool.h"
#include "Protocol/Serializables.h"
#include "Player.h"
#include "Game/System/GameManager.h"
#include "Game/System/Network/SendRequirements.h"
#include "Protocol/Json_Formatter/JsonWriter.h"
#include "Protocol/Json_Formatter/JsonReader.h"

#pragma comment(lib, "ws2_32.lib")


using namespace TheShift;
//void CALLBACK CompletionRoutine1(DWORD Error, DWORD ReceivedBytes, LPWSAOVERLAPPED LpOverlapped, DWORD Flags);
//LPWSAOVERLAPPED_COMPLETION_ROUTINE CompletionRoutine = CompletionRoutine1;

class Server
{
public:
	Server();
	Server(uint16_t Port);
	~Server();
	Server(const Server&) = delete;
	Server& operator=(const Server&) = delete;

	void Run();

	// 들어온 데이터로 Packet을 만든다. 그후 Dispatch message
	void ProcessIo(ClientId Id, SocketInfo* Info, uint32_t BytesTransferred);
	void DisconnectPlayer(ClientId PlayerIndex);

private:
	HANDLE NewPlayerEvent;
	SOCKET ListenSocket;
	SOCKET ClientSocket;
	sockaddr_in SockAddr;
	//uint32_t Address;
	uint16_t Port;
	bool Running;
	std::thread ReceiverThread;

	// Packet을 보내거나 해석할 때 사용한다.
	// Header 이후에 데이터가 쌓인다.
	OutputMemoryStream OutputStream;
	InputMemoryStream InputStream;

	//TODO: Fix Queue to non-blocking??

	// Deals with every game process
	GameManager GameMngr;

	MemoryPool<Player> PlayerPool;

	JsonFormatter::JsonWriter JsonWriter_;
	JsonFormatter::JsonReader JsonReader_;

	void Join();
	void DoAccept();

	// Common network functions
	void ReceiverThreadFunc();

	void SetAddrPort(uint16_t PortValue);
	void InitServer();
	int InitWinsockAndListen();

	int Recv(ClientId Id);

	void AssembleAndProcessPacket(ClientId Id, uint32_t BytesTransferred);
	//void ProcessPacket(ClientId Id);
};


extern Server TheShiftServer;


